package com.bjxczy.onepark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * AttendanceRules
 */
@EnableCaching
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class RecordApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecordApplication.class, args);
        System.err.println("------------------------出入记录模块运行成功-------------------");
    }

}
