package com.bjxczy.onepark.checkattendancemodule.service.impl;


import com.alibaba.nacos.common.utils.UuidUtils;
import com.bjxczy.onepark.checkattendancemodule.entity.AttendanceRules;
import com.bjxczy.onepark.checkattendancemodule.entity.RulesAndStaff;
import com.bjxczy.onepark.checkattendancemodule.entity.StaffCheckAttendance;
import com.bjxczy.onepark.checkattendancemodule.entity.WeeksRules;
import com.bjxczy.onepark.checkattendancemodule.mapper.AttendanceRulesMapper;
import com.bjxczy.onepark.checkattendancemodule.mapper.RulesAndStaffMapper;
import com.bjxczy.onepark.checkattendancemodule.mapper.WeeksRulesMapper;
import com.bjxczy.onepark.checkattendancemodule.pojo.dto.AttendanceRulesDto;
import com.bjxczy.onepark.checkattendancemodule.service.AttendanceRulesService;
import com.bjxczy.onepark.checkattendancemodule.service.QueryStaffServer;
import com.bjxczy.onepark.common.utils.PageUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.*;

/*
 *@ClassName AttendanceRulesServiceImpl
 *@Author 温良伟
 *@Date 2023/3/29 17:57
 *@Version 1.0
 */
@Service
public class AttendanceRulesServiceImpl implements AttendanceRulesService {

    @Resource
    private AttendanceRulesMapper mapper;

    @Resource
    private WeeksRulesMapper weeksRulesMapper;

    @Resource
    private QueryStaffServer queryStaffServer;


    @Resource
    private RulesAndStaffMapper rulesAndStaffMapper;


    @Override
    public Object getListPage(AttendanceRulesDto dto) {
        List<AttendanceRules> list = mapper.selectByDto(dto);
        list.forEach(el -> {
            StringBuilder str = new StringBuilder();
            List<WeeksRules> weeksRules = weeksRulesMapper.selectByRulesId(el.getRulesId());
            for (WeeksRules weeksRule : weeksRules) {
                if (weeksRule.getIsCheck()) {
                    str.append(weeksRule.getDisplay()).append(" - ");
                }
            }
            char[] chars = str.toString().toCharArray();
            str = new StringBuilder();
            chars[chars.length - 1] = ' ';
            chars[chars.length - 2] = ' ';
            for (int i = 0; i < chars.length; i++) {
                char aChar = chars[i];
                str.append(aChar);
            }
            el.setWeeks(weeksRules);
            el.setWeeksLabel(str.toString().trim());
            el.setOfficeHoursLabel(el.getOfficeHours() + " - " + el.getOffDutyTime());
            el.setIds(rulesAndStaffMapper.selectByRulesId(el.getRulesId()));
        });
        return PageUtils.Page(dto, list);
    }

    /**
     * 生成 考勤规则对象
     *
     * @param rules
     * @return
     */
    private AttendanceRules generateAttendanceRules(AttendanceRules rules) {
        rules.setRulesId(UuidUtils.generateUuid());//生成id
        rules.setPeopleNumber(rules.getIds().size());// 使用改考勤的人数
        rules.setCreateTime(new Date());
        rules.setRulesName(rules.getRulesName().trim());
        rules.setDelFlag(0);
        return rules;
    }

   /* public void setStaffCheckAttendance(AttendanceRules rules) {
        rules.getIds().forEach(el -> {
            // 获取员工
            StaffInfoFine staff = queryStaffServer.getStaffById(el);
            encapsulationStaffCheckAttendance(staff, rules);

        });
    }*/

    /**
     * 考勤记录 依赖于 通行记录 此方法作废
     *
     * @param staff
     * @param rules
     */
    /*private void encapsulationStaffCheckAttendance(StaffInfoFine staff, AttendanceRules rules) {
        StaffCheckAttendance attendance = new StaffCheckAttendance();
        List<DeptNameInfo> deptNameInfos = StrUtils.DeptNameInfo(staff.getDeptNames());
        // 本月共几周
        int weeks = DateUtil.getWeeks(DateUtil.getYearNum(), DateUtil.getMonthNum());
        System.err.println(weeks);

        Date month = DateUtil.getFirstDayOfMonth(DateUtil.getYearNum(), DateUtil.getMonthNum());
        Integer date = DateUtil.getWeekOfDate(month);
        System.err.println("本月第一天是周" + date);

        Date ofMonth = DateUtil.getLastdayDayOfMonth(DateUtil.getYearNum(), DateUtil.getMonthNum());
        Integer datezu = DateUtil.getWeekOfDate(ofMonth);
        System.err.println("本月最后一天是周" + datezu);

        System.out.println(month);
        System.out.println(ofMonth);

    }*/
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insert(AttendanceRules rul) throws Exception {
        // 封装考勤规则
        AttendanceRules rules = generateAttendanceRules(rul);
        if (nameCheck(rules)) {
            throw new Exception("考勤名称相同 考勤添加失败");
        }
        // 绑定人员和规则
        insertRulesAndStaff(rules, "insert");
        // 周规则入库
        setWeeksRules(rules);
        // 规则入库
        return mapper.insert(rules);
    }

    @Override
    public Object update(AttendanceRules rules) throws Exception {
        if (nameCheck(rules)) {
            throw new Exception("考勤名称相同 考勤更新失败");
        }
        // 绑定人员和规则
        insertRulesAndStaff(rules, "insert");
        // 周规则入库
        setWeeksRules(rules);
        return mapper.updateById(rules);
    }


    /**
     * 插入员工绑定
     *
     * @param rules
     */
    private void insertRulesAndStaff(AttendanceRules rules, String msg) throws Exception {
        if (msg.equals("insert")) {
            if (staffCheck(rules)) {
                throw new Exception("考勤员工已经在其他考勤规则中 考勤添加失败");
            }
        }
        if (msg.equals("update")) {
            rulesAndStaffMapper.deleteById(rules.getRulesId());
        }
        rules.getIds().forEach(el -> {
            rulesAndStaffMapper.insert(new RulesAndStaff(rules.getRulesId(), el.toString()));
        });
    }


    /**
     * 周的信息规则
     *
     * @param rules
     */
    public void setWeeksRules(AttendanceRules rules) {
        weeksRulesMapper.deleteById(rules.getRulesId());
        rules.getWeeks().forEach(el -> {
            el.setRulesId(rules.getRulesId());
            // 周规则入库
            weeksRulesMapper.insert(el);
        });
    }

    private Boolean staffCheck(AttendanceRules rules) {
        List<RulesAndStaff> list = rulesAndStaffMapper.selectByMap(null);
        if (list.size() == 0) {
            return false;
        }
        List<Integer> ids = rules.getIds();
        for (RulesAndStaff el : list) {
            for (Integer id : ids) {
                if (el.getStaffId().equals(id.toString())) {
                    return true;
                }
            }
        }
        return false;
    }

    private Boolean nameCheck(AttendanceRules rules) {
        List<AttendanceRules> select = mapper.selectByMap(null);
        Integer index = null;
        // 清除自己后在比较
        if (rules.getRulesName() != null && !rules.getRulesName().equals("")) {
            for (int i = 0; i < select.size(); i++) {
                if (select.get(i).getRulesId().equals(rules.getRulesId())) {
                    index = i;
                }
            }
        }
        if (index != null) {
            int i = index;
            select.remove(i);
        }
        if (select.size() == 0) {
            return false;
        }
        for (AttendanceRules el : select) {
            assert rules.getRulesName() != null;
            String trim = rules.getRulesName().trim();
            if (el.getRulesName().equals(trim)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public Object delete(String rulesId) {
        weeksRulesMapper.deleteById(rulesId);
        rulesAndStaffMapper.deleteById(rulesId);
        return mapper.deleteById(rulesId);
    }
}
