package com.bjxczy.onepark.checkattendancemodule.service;


import com.bjxczy.onepark.checkattendancemodule.entity.AttendanceRules;
import com.bjxczy.onepark.checkattendancemodule.pojo.dto.AttendanceRulesDto;

/*
 *@ClassName AttendanceRulesService
 *@Description TODO
 *@Author 温良伟
 *@Date 2023/3/29 17:56
 *@Version 1.0
 */
public interface AttendanceRulesService {
    int insert(AttendanceRules rules) throws Exception;

    Object getListPage(AttendanceRulesDto dto);

    Object update(AttendanceRules rules)  throws Exception;

    Object delete(String rulesId);
}
