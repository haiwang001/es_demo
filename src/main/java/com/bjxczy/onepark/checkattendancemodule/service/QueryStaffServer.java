package com.bjxczy.onepark.checkattendancemodule.service;


import com.bjxczy.core.feign.client.organization.BaseStaffFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

/*
 *@ClassName QueryStaffServer
 *@Description TODO
 *@Author 温良伟
 *@Date 2023/3/30 10:05
 *@Version 1.0
 */
@FeignClient("organization")
public interface QueryStaffServer extends BaseStaffFeignClient {
}
