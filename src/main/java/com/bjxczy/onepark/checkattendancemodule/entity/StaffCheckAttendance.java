package com.bjxczy.onepark.checkattendancemodule.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/*
 *@ClassName StaffCheckAttendance
 *@Author 温良伟
 *@Date 2023/3/30 9:56
 *@Version 1.0   待定 员工考勤总览
 */
@Data
@TableName("k_staff_check_attendance")
public class StaffCheckAttendance {

    @TableId(type = IdType.AUTO)
    private Long id; // 记录id
    private String rulesId; // 规则id 绑定 rules_id
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime; // create_time
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag; // del_flag

    private String staffName; // 员工姓名 staff_name
    private String staffCode; // 员工编号  staff_code
    private String deptId; // 员工部门id dept_id
    private String deptName; // 员工部门名称  dept_name
    private String shouldAttendanceDays; // 应该出勤的天数 should_attendance_days
    private String beLateNumber; // 迟到数   be_late_number
    private String leaveEarlyNumber; // 早退数   leave_early_number
    private String uncLockedNumber; // 未打卡数   unc_locked_number
    private String forAttendanceNumber; // 未出勤数   for_attendance_number

}
