package com.bjxczy.onepark.checkattendancemodule.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/*
 *@ClassName RulesAndStaff
 *@Author 温良伟
 *@Date 2023/3/30 14:22
 *@Version 1.0  考勤规则和员工中间表
 */
@Data
@TableName("k_rules_staff")
public class RulesAndStaff {
    @TableId
    private String rulesId; // 规则id rules_id
    private String staffId; // 员工id staff_id

    public RulesAndStaff(String rulesId, String staffId) {
        this.rulesId = rulesId;
        this.staffId = staffId;
    }
}
