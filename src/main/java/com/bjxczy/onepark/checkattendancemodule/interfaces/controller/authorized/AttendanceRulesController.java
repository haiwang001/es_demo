package com.bjxczy.onepark.checkattendancemodule.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.checkattendancemodule.entity.AttendanceRules;
import com.bjxczy.onepark.checkattendancemodule.pojo.dto.AttendanceRulesDto;
import com.bjxczy.onepark.checkattendancemodule.service.AttendanceRulesService;
import com.bjxczy.onepark.common.resp.R;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/*
 *@ClassName LogController
 *@Author 温良伟
 *@Date 2023/2/1 9:38
 *@Version 1.0 考勤模块
 */
@Log4j2
@ApiResourceController
@RequestMapping("/attendanceRules")
public class AttendanceRulesController {
    @Resource
    private AttendanceRulesService service;

    @GetMapping(value = "/list", name = "考勤列表")
    public R<?> getListPage(AttendanceRulesDto dto) {
        return R.success(service.getListPage(dto));
    }

    @PostMapping(value = "/insert", name = "增加考勤规则")
    public R<?> insert(@RequestBody AttendanceRules Rules) {
        try {
            return R.success(service.insert(Rules));
        } catch (Exception e) {
            return R.success(el -> el.set("code", 500).set("msg", e.getMessage()));
        }
    }


    @PutMapping(value = "/update", name = "修改考勤规则")
    public R<?> update(@RequestBody AttendanceRules Rules) {
        try {
            return R.success(service.update(Rules));
        } catch (Exception e) {
            return R.success(el -> el.set("code", 500).set("msg", e.getMessage()));
        }
    }

    @DeleteMapping(value = "/delete/{rulesId}", name = "删除考勤规则")
    public R<?> delete(@PathVariable String rulesId) {
        try {
            return R.success(service.delete(rulesId));
        } catch (Exception e) {
            return R.success(el -> el.set("code", 500).set("msg", e.getMessage()));
        }
    }

}
