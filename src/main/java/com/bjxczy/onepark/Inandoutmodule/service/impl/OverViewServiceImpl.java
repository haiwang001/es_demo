package com.bjxczy.onepark.Inandoutmodule.service.impl;


import com.bjxczy.onepark.common.utils.DateUtil;
import com.bjxczy.onepark.Inandoutmodule.entity.RecordDoor;
import com.bjxczy.onepark.Inandoutmodule.pojo.vo.DateNodeVo;
import com.bjxczy.onepark.Inandoutmodule.pojo.vo.VisitorVo;
import com.bjxczy.onepark.Inandoutmodule.repository.EsRecordRepository;
import com.bjxczy.onepark.Inandoutmodule.repository.OverViewMapper;
import com.bjxczy.onepark.Inandoutmodule.service.OverViewService;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/*
 *@ClassName OverViewService
 *@Author 温良伟
 *@Date 2023/3/24 10:32
 *@Version 1.0
 */
@Service
public class OverViewServiceImpl implements OverViewService {

    @Resource
    private OverViewMapper mapper;

    @Resource
    private EsRecordRepository Repository;

    @Override
    public Object numberOfPeople(String id) {
        HashMap<String, Object> returnMap = new HashMap<>();
        ArrayList<RecordDoor> RecordList = new ArrayList<>();
        Iterable<RecordDoor> all = null;
        if (id == null || "".equals(id.trim())) {
            all = Repository.findAll();// 全部记录
        } else {
            BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
            BoolQueryBuilder orBuilder = new BoolQueryBuilder();
            orBuilder.should(QueryBuilders.wildcardQuery("tenantId", id)); //  通过局址id查询
            queryBuilder.must(orBuilder);
            all = Repository.search(queryBuilder);
        }
        all.forEach(RecordList::add);
        //最新记录时间
        if (RecordList.size() >= 0 || all != null) {
            String LatestTime = LatestTime(RecordList);
            returnMap.put("datetime", LatestTime);
        } else {
            returnMap.put("datetime", null);
        }
        // 通过 名称分组去重  识别不同的人数
        Map<String, List<RecordDoor>> map = RecordList.stream().collect(Collectors.groupingBy(RecordDoor::getStaffName));
        returnMap.put("discernCount", map.size());
        // 找到所有访客人数
        List<VisitorVo> visitorAll = mapper.getVisitorAll();
        returnMap.put("visitorCount", visitorAll.size());
        // 拥有凭证数总人数
        List<Integer> num = mapper.getCertificateNum();
        returnMap.put("interiorCount", num.size());
        // 临时
        List<VisitorVo> temporary = new ArrayList<>(visitorAll.size());
        // 常驻三方
        List<VisitorVo> resident = new ArrayList<>(visitorAll.size());

        visitorAll.forEach(el -> {
            if (el.getType().equals("0")) {
                resident.add(el);
            }
            if (el.getType().equals("2")) {
                temporary.add(el);
            }
        });
        returnMap.put("temporaryVisitorCount", temporary.size());
        returnMap.put("residentVisitorCount", resident.size());
        return returnMap;
    }

    private String LatestTime(ArrayList<RecordDoor> recordList) {
        HashMap<Long, String> map = new HashMap<>(recordList.size());
        ArrayList<Long> list = new ArrayList<>(recordList.size());
        recordList.forEach(el -> {
            try {
                long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(el.getStartTime()).getTime();
                list.add(time);
                map.put(time, el.getStartTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        if (list.size() <= 0) {
            return "";
        }
        list.sort(Collections.reverseOrder());//排序后 第1个元素为最大值
        Long aLong = list.get(0);
        return map.get(aLong);
    }


    @Override
    public Object accumulativeFlow() {
        Iterable<RecordDoor> all = Repository.findAll();// 全部记录
        ArrayList<RecordDoor> RecordList = new ArrayList<>(); // 这周的全部数据
        List<DateNodeVo> list = DateUtil.LastWeek();//获取一周的节点 yyyy-mm-dd 节点
        all.forEach(rel -> {
            list.forEach(el -> {
                if (rel.getYearsMonthDay().equals(el.getX())) {
                    RecordList.add(rel);
                }
            });
        });
        return DataReturnDay(RecordList);
    }

    private Object DataReturnDay(ArrayList<RecordDoor> list) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("count", list.size());
        // 进入方向的设备查询
        List<String> enter = mapper.selectEnter();
        ArrayList<RecordDoor> enterList = new ArrayList<>();
        enter.forEach(El -> {
            list.forEach(el -> {
                if (El.equals(el.getDeviceCode())) {
                    enterList.add(el);
                }
            });
        });

        List<DateNodeVo> listNode1 = DateUtil.LastWeek();//获取一周的节点 yyyy-mm-dd 节点
        List<DateNodeVo> listNode2 = DateUtil.LastWeek();//获取一周的节点 yyyy-mm-dd 节点

        if (list.size() != 0) {
            // 开门方向的记录
            Map<String, List<RecordDoor>> map1 = enterList.stream().collect(Collectors.groupingBy(RecordDoor::getYearsMonthDay));
            listNode1.forEach(el -> {
                map1.forEach((k, v) -> {
                    System.err.println(el.getX().split(":")[0]);
                    System.err.println(k);
                    if (el.getX().split(":")[0].equals(k)) {
                        el.setY((long) v.size());
                    }
                });
            });
            list.removeAll(enterList);
            // 反方向
            Map<String, List<RecordDoor>> map2 = list.stream().collect(Collectors.groupingBy(RecordDoor::getYearsMonthDay));
            listNode2.forEach(el -> {
                map2.forEach((k, v) -> {
                    if (el.getX().split(":")[0].equals(k)) {
                        el.setY((long) v.size());
                    }
                });
            });
        }

        map.put("enterCount", enterList.size());
        map.put("goOutCount", list.size());
        map.put("enterNode", listNode1);
        map.put("goOutNode", listNode2);
        return map;
    }

    private HashMap<String, Object> DataReturnHour(List<RecordDoor> list) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("count", list.size());
        // 进入方向的设备查询
        List<String> enter = mapper.selectEnter();
        ArrayList<RecordDoor> enterList = new ArrayList<>();
        enter.forEach(El -> {
            list.forEach(el -> {
                if (El.equals(el.getDeviceCode())) {
                    enterList.add(el);
                }
            });
        });

        ArrayList<DateNodeVo> dateNodeVos1 = DateUtil.node24();//展开所有节点 用于对比
        ArrayList<DateNodeVo> dateNodeVos2 = DateUtil.node24();//展开所有节点 用于对比
        if (list.size() != 0) {
            // 开门方向的记录
            Map<String, List<RecordDoor>> map1 = enterList.stream().collect(Collectors.groupingBy(RecordDoor::getHour));
            dateNodeVos1.forEach(el -> {
                map1.forEach((k, v) -> {
                    if (el.getX().split(":")[0].equals(k)) {
                        el.setY((long) v.size());
                    }
                });
            });
            list.removeAll(enterList);
            // 反方向
            Map<String, List<RecordDoor>> map2 = list.stream().collect(Collectors.groupingBy(RecordDoor::getHour));
            dateNodeVos2.forEach(el -> {
                map2.forEach((k, v) -> {
                    if (el.getX().split(":")[0].equals(k)) {
                        el.setY((long) v.size());
                    }
                });
            });
        }
        map.put("enterCount", enterList.size());
        map.put("goOutCount", list.size());
        map.put("enterNode", dateNodeVos1);
        map.put("goOutNode", dateNodeVos2);
        return map;
    }

    @Override
    public Object toDayFlow() {
        Iterable<RecordDoor> all = Repository.findAll();// 全部记录
        String day = DateUtil.getYearAndMonthAndDay();// yyyy-mm-dd 当前日期
        ArrayList<RecordDoor> list = new ArrayList<>();
        all.forEach(el -> {
            System.err.println(el.getYearsMonthDay());
            System.err.println(day);
            if (el.getYearsMonthDay().equals(day)) {
                list.add(el);
            }
        });
        // 进入方向的设备查询
        return DataReturnHour(list);
    }


    @Override
    public Object entranceGuardOverview(String id) {
        HashMap<String, Object> map = new HashMap<>();
        Integer All = mapper.selectAll(id);
        Integer Online = mapper.selectOnlineAll(id);
        Integer NoOnline = mapper.selectNoOnlineAll(id);
        map.put("All", All); // 全部设备
        map.put("Online", Online); // 在线设备
        map.put("NoOnline", NoOnline); // 不在线设备
        return map;
    }
}
