package com.bjxczy.onepark.Inandoutmodule.service.impl;


import com.alibaba.nacos.common.utils.StringUtils;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.utils.ExcelUtils;
import com.bjxczy.onepark.common.utils.PageUtils;
import com.bjxczy.onepark.Inandoutmodule.entity.RecordDoor;
import com.bjxczy.onepark.Inandoutmodule.pojo.dto.RecordDoorDto;
import com.bjxczy.onepark.Inandoutmodule.repository.EsRecordRepository;
import com.bjxczy.onepark.Inandoutmodule.service.MovementRecordService;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Array;
import java.util.*;

/*
 *@ClassName IoLogServiceImpl
 *@Author 温良伟
 *@Date 2023/2/1 10:58
 *@Version 1.0
 */
@Service
public class MovementRecordServiceImpl implements MovementRecordService {
    /**
     * mysql版本
     */
   /* @Autowired
    private RecordRepository repository;*/
    /**
     * es版本
     */
    @Resource
    private EsRecordRepository repository;

    @Override
    public Boolean exportExcel(HttpServletResponse response,
                               String fileName, RecordDoorDto dto,
                               UserInformation sysUser) {
        try {
            if ("".equals(fileName) || fileName == null)
                fileName = "通行记录信息";
            ExcelUtils.export(response,
                    fileName.trim(),
                    fileName.trim(),
                    this.queryList(dto), RecordDoor.class);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 查询
     *
     * @param dto
     * @param sysUser
     * @return
     */
    @Override
    public Object queryPage(RecordDoorDto dto, UserInformation sysUser) {
        ArrayList<RecordDoor> list = queryList(dto);
        return PageUtils.Page(dto, list);
    }

    private ArrayList<RecordDoor> queryList(RecordDoorDto dto) {

        //查询对象
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        //模糊搜索对象
        BoolQueryBuilder keyBuilder = QueryBuilders.boolQuery();
        //分类查询对象
        BoolQueryBuilder orBuilder = QueryBuilders.boolQuery();
        //拼接模糊搜索条件
        if (StringUtils.isNotBlank(dto.getMerge())) {
            //这边主要用的是should，也就是相当于mysql的or
            // title like concat('%keyword%')
            // or content like concat('%keyword%')
            //wildcardQuery可以用于带分词的模糊搜索，如果要分词的话，那么字段的type应该是text，
            // 假如在用wildcardQuery而不想分词的话，可以查.keyword
            //例如title.keyword，不过我这边title的type已经定了是keyword类型，所以我就直接做不分词的模糊查询，精确查询的话就用matchQuery
            keyBuilder.should(QueryBuilders.wildcardQuery("staffName", "*" + dto.getMerge() + "*"));
            keyBuilder.should(QueryBuilders.wildcardQuery("misCode", "*" + dto.getMerge() + "*"));
            keyBuilder.should(QueryBuilders.wildcardQuery("mobile", "*" + dto.getMerge() + "*"));
            queryBuilder.must(keyBuilder);
        }
        // 部门 id
        if (dto.getDeptId() != null && !"".equals(dto.getDeptId())) {
            orBuilder.should(QueryBuilders.matchQuery("deptId", dto.getDeptId()));
            queryBuilder.must(orBuilder);
        }
        // 局址 id
        if (dto.getTenantId() != null && !"".equals(dto.getTenantId())) {
            orBuilder.should(QueryBuilders.wildcardQuery("tenantId", dto.getTenantId()));
            queryBuilder.must(orBuilder);
        }
        // 楼层查询
        if (dto.getFloorName() != null && !"".equals(dto.getFloorName())) {
            orBuilder.should(QueryBuilders.wildcardQuery("floorName", "*" + dto.getFloorName() + "*"));
            queryBuilder.must(orBuilder);
        }
        // 凭证
        if (dto.getCardNo() != null && !"".equals(dto.getCardNo())) {
            orBuilder.should(QueryBuilders.wildcardQuery("cardNo", "*" + dto.getCardNo() + "*"));
            queryBuilder.must(orBuilder);
        }
        // 设备名称
        if (dto.getDeviceName() != null && !"".equals(dto.getDeviceName())) {
            orBuilder.should(QueryBuilders.wildcardQuery("deviceName", "*" + dto.getDeviceName() + "*"));
            queryBuilder.must(orBuilder);
        }
        // 开门时间
        if (dto.getStartTime() != null && !"".equals(dto.getStartTime())) {
            orBuilder.should(QueryBuilders.termQuery("YearsMonthDay", dto.getStartTime()));
            queryBuilder.must(orBuilder);
        }
        // 开门方向
        if (dto.getInOrOut() != null) {
            orBuilder.should(QueryBuilders.matchQuery("inOrOut", dto.getInOrOut()));
            queryBuilder.must(orBuilder);
        }
        // 开门方式
        if (dto.getAuthType() != null) {
            orBuilder.should(QueryBuilders.matchQuery("authType", dto.getAuthType()));
            queryBuilder.must(orBuilder);
        }
        Iterable<RecordDoor> search = repository.search(queryBuilder);
        ArrayList<RecordDoor> list = new ArrayList<>();
        search.forEach(list::add);
        list = reverse(list);
        System.err.println(list);
        return list;
    }

    private ArrayList<RecordDoor> reverse(ArrayList<RecordDoor> list) {
        ArrayList<RecordDoor> door = new ArrayList();
        for (int i = list.size() - 1; i >= 0; i--) {
            door.add(list.get(i));
        }
        return door;
    }


}
