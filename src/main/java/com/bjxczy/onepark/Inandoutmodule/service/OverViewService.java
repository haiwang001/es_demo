package com.bjxczy.onepark.Inandoutmodule.service;

public interface OverViewService {
    Object entranceGuardOverview(String id);

    Object toDayFlow();

    Object accumulativeFlow();

    Object numberOfPeople(String id);
}
