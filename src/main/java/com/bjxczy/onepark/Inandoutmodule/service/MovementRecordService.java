package com.bjxczy.onepark.Inandoutmodule.service;

import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.Inandoutmodule.pojo.dto.RecordDoorDto;

import javax.servlet.http.HttpServletResponse;

public interface MovementRecordService {
    Object queryPage(RecordDoorDto dto, UserInformation sysUser);

    Boolean exportExcel(HttpServletResponse response, String fileName, RecordDoorDto dto, UserInformation sysUser);
}
