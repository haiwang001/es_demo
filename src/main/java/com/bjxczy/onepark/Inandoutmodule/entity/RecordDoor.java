package com.bjxczy.onepark.Inandoutmodule.entity;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;

/*
 *@ClassName RecordDoor
 *@Author 温良伟
 *@Date 2023/1/31 1140
 *@Version 1.0
 *
 * 出入记录表
 */
@Data
@Document(indexName = "record_door")
public class RecordDoor implements Serializable {
    private static final long serialVersionUID = 4762215939785321492L;
    @javax.persistence.Id
    @Id
    private String id; //id编号

    /*
    index： 是否创建索引。作为搜索条件时index必须为true
    analyzer：指定分词器类型。
    */
    @Excel(name = "员工编号", width = 18, replace = {"_null"})
    @Field(type = FieldType.Keyword, index = false)
    private String misCode; //员工编号
    @Excel(name = "姓名", width = 18, replace = {"_null"})
    @Field(type = FieldType.Keyword, index = false)
    private String staffName; //姓名
    @Excel(name = "手机号", width = 18, replace = {"_null"})
    @Field(type = FieldType.Keyword, index = false)
    private String mobile; //手机号
    @Field(type = FieldType.Keyword, index = false)
    @Excel(name = "部门", width = 18, replace = {"_null"})
    private String deptName; //部门名称
    @Field(type = FieldType.Keyword, index = false)
    @Excel(name = "接访人", width = 18, replace = {"_null"})
    private String interName; //接访人
    @Excel(name = "空间", width = 18, replace = {"_null"})
    @Field(type = FieldType.Keyword, index = false)
    private String address; //局址

    @Field(type = FieldType.Keyword, index = false)
    private String tenantId; //局址id
    @Excel(name = "楼层", width = 18, replace = {"_null"})
    @Field(type = FieldType.Keyword, index = false)
    private String floorName; //楼层
    @Excel(name = "人员类型", width = 18, replace = {"_null"})
    @Field(type = FieldType.Keyword, index = false)
    private String peopleType; //人员类型
    /*  汉王无下列字段 */
    @Field(type = FieldType.Keyword, index = false)
    @Excel(name = "凭证号展示", width = 18, replace = {"_null"})
    private String cardNo; //凭证

    @Field(type = FieldType.Keyword, index = false)
    private String peopleTypeLabel; //人员类型描述

    @Field(type = FieldType.Keyword, index = false)
    private Integer authType; //开门方式
    @Excel(name = "开门方式", width = 18, replace = {"_null"})
    private String authTypeLabel; //开门方式描述

    @Field(type = FieldType.Keyword, index = false)
    private Integer inOrOut; //开门方向
    @Excel(name = "开门方向", width = 18, replace = {"_null"})
    private String inOrOutLabel; //开门方向描述
    @Field(type = FieldType.Keyword, index = false)
    @Excel(name = "体温检测", width = 18, replace = {"_null"})
    private String temperatureLabel; //体温状态展示

    @Field(type = FieldType.Keyword, index = false)
    private Integer mask; //口罩状态
    @Excel(name = "口罩状态", width = 18, replace = {"_null"})
    @Field(type = FieldType.Keyword, index = false)
    private String maskLabel; //口罩状态展示
    @Excel(name = "开门时间", width = 18, replace = {"_null"})
    @Field(type = FieldType.Keyword, index = false)
    private String startTime; //开门时间展示
    @Excel(name = "设备名称", width = 18, replace = {"_null"})
    @Field(type = FieldType.Keyword, index = false)
    private String deviceName; //设备名称
    @Field(type = FieldType.Keyword, index = false)
    private String deviceCode; //设备编号

    @Field(type = FieldType.Keyword, index = false)
    private String Years; // yyyy  年
    @Field(type = FieldType.Keyword, index = false)
    private String YearsMonth; // yyyy-mm  月
    @Field(type = FieldType.Keyword , index = false)
    private String YearsMonthDay; //yyyy-mm-dd
    @Field(type = FieldType.Keyword, index = false)
    private String hour; // 小时
    @Field(type = FieldType.Keyword, index = false)
    private Integer temperature; //体温状态
    @Field(type = FieldType.Keyword, index = false)
    private String deptId; //部门id
}
