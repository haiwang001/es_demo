package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

/*
 *@ClassName Visitor
 *@Author 温良伟
 *@Date 2023/3/27 14:35
 *@Version 1.0
 */
@Data
public class VisitorVo {
    private String name;
    private String type;
}
