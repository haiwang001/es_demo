package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/*
 *@ClassName VisitorInfoExcelVo
 *@Author 温良伟
 *@Date 2023/1/30 14:03
 *@Version 1.0
 */
@Data
public class RecordDoorExcelVo {
    @Excel(name = "员工编号", width = 18, replace = {"_null"})
    private String misCode;//访客姓名 Y
    @Excel(name = "姓名", width = 18, replace = {"_null"})
    private String staffName;//访客姓名 Y
}
