package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

/*
 *@ClassName DateNodeVo
 *@Author 温良伟
 *@Date 2023/3/2 12:55
 *@Version 1.0
 */
@Data
public class DateNodeVo {
    private String x;
    private Long y;

    public DateNodeVo(String x, Long y) {
        this.x = x;
        this.y = y;
    }
}
