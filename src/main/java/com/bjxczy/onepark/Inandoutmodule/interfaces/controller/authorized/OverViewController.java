package com.bjxczy.onepark.Inandoutmodule.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.Inandoutmodule.service.OverViewService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/*
 *@ClassName OverViewController
 *@Author 温良伟
 *@Date 2023/3/24 10:20
 *@Version 1.0
 * 通行概览
 */
@Log4j2
@ApiResourceController
@RequestMapping("/OverView")
public class OverViewController {

    @Resource
    private OverViewService service;

    /**
     * 设备总览
     *
     * @return
     */
    @GetMapping("/guardOverview")
    public R<?> entranceGuardOverview(String id) {
        return R.success(service.entranceGuardOverview(id));
    }

    /**
     * 今日流量统计
     *
     * @return
     */
    @GetMapping("/toDayFlow")
    public R<?> toDayFlow() {
        return R.success(service.toDayFlow());
    }

    /**
     * 累计人流量统计
     *
     * @return
     */
    @GetMapping("/accumulativeFlow")
    public R<?> accumulativeFlow() {
        return R.success(service.accumulativeFlow());
    }

    /**
     * 人数概览
     *
     * @return
     */
    @GetMapping("/numberOfPeople")
    public R<?> numberOfPeople(String id) {
        return R.success(service.numberOfPeople(id));
    }
}
