package com.bjxczy.onepark.Inandoutmodule.repository;

import com.bjxczy.onepark.Inandoutmodule.entity.RecordDoor;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import javax.annotation.Resource;

@Resource
public interface EsRecordRepository extends ElasticsearchRepository<RecordDoor, Integer> {

}
