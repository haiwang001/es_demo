package com.bjxczy.onepark.Inandoutmodule.repository;


import com.bjxczy.onepark.Inandoutmodule.pojo.vo.VisitorVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/*
 *@ClassName OverViewMapper
 *@Description TODO
 *@Author 温良伟
 *@Date 2023/3/24 10:37
 *@Version 1.0
 */
@Mapper
public interface OverViewMapper {

    // 设备数
    @Select("select count(*)\n" +
            "from voucher.guard_info\n" +
            "where del_flag = 0 and\n" +
            "      if(#{id} is not null or #{id} != '',area_id = #{id},true)")
    Integer selectAll(@Param("id") String id);


    // 设备在线数
    @Select("select count(*)\n" +
            "from voucher.guard_info\n" +
            "where del_flag = 0  and is_on_line = 1  and \n" +
            "      if(#{id} is not null or #{id} != '',area_id = #{id},true)")
    Integer selectOnlineAll(@Param("id") String id);

    // 设备不在线数
    @Select("select count(*)\n" +
            "from voucher.guard_info\n" +
            "where del_flag = 0  and is_on_line = 0  and \n" +
            "      if(#{id} is not null or #{id} != '',area_id = #{id},true)")
    Integer selectNoOnlineAll(@Param("id") String id);

    @Select("select guard_code\n" +
            "from voucher.guard_info\n" +
            "where del_flag = 0\n" +
            "  and guard_direction = 1")
    List<String> selectEnter();

    @Select("select visitor_name name, visitor_type type\n" +
            "from visitor_info\n" +
            "where del_flag = 0\n" +
            "group by visitor_name")
    List<VisitorVo> getVisitorAll();

    @Select("select count(*)\n" +
            "from evidence_record\n" +
            "where voucher_num > 0\n" +
            "group by fld_name")
    List<Integer> getCertificateNum();
}
