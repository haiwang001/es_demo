## 二、接口

### 1. 组织架构

#### 1.1 员工查询

| 地址     | /api/organization/staff/findStaff |
| -------- | --------------------------------- |
| 请求方式 | GET                               |

请求参数：

| 参数名   | 类型    | 位置  | 说明             |
| -------- | ------- | ----- | ---------------- |
| unifyParams  | Integer | query | 员工编号/姓名/手机号 |
| orgId | Integer | query | 部门ID |
| queryOrg | String  | query | 是否返回部门数据 |

请求示例：
```
GET /api/organization/staff/findStaff?unifyParams=&orgId=&queryOrg=true
```

返回示例：
```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"organizationTree": [{
			"delFlag": 0,
			"createTime": "2022-12-02 14:45:04",
			"updateTime": "2022-12-02 14:45:04",
			"operator": null,
			"id": 1517,
			"fldName": "北京XXX网络科技有限公司",
			"description": null,
			"parentId": -1,
			"sort": 6.0,
			"orgSource": "default",
			"uuid": "105d4a50b07d45f3951f055381fa8e2f",
			"showFlag": 1,
			"fldCode": "105d4a50b07d45f3951f055381fa8e2f",
			"children": [{
				"delFlag": 0,
				"createTime": "2022-12-02 14:45:21",
				"updateTime": "2022-12-02 14:45:21",
				"operator": null,
				"id": 1518,
				"fldName": "测试部门一",
				"description": null,
				"parentId": 1517,
				"sort": 1.0,
				"orgSource": "default",
				"uuid": "48b86e68b6424ed4ba16fd66bf76a99f",
				"showFlag": 1,
				"fldCode": "48b86e68b6424ed4ba16fd66bf76a99f",
				"children": [],
				"path": [{
					"delFlag": 0,
					"createTime": "2022-12-02 14:45:04",
					"updateTime": "2022-12-02 14:45:04",
					"operator": null,
					"id": 1517,
					"fldName": "北京XXX网络科技有限公司",
					"description": null,
					"parentId": -1,
					"sort": 6.0,
					"orgSource": "default",
					"uuid": "105d4a50b07d45f3951f055381fa8e2f",
					"showFlag": 1
				}, {
					"delFlag": 0,
					"createTime": "2022-12-02 14:45:21",
					"updateTime": "2022-12-02 14:45:21",
					"operator": null,
					"id": 1518,
					"fldName": "测试部门一",
					"description": null,
					"parentId": 1517,
					"sort": 1.0,
					"orgSource": "default",
					"uuid": "48b86e68b6424ed4ba16fd66bf76a99f",
					"showFlag": 1
				}]
			}, {
				"delFlag": 0,
				"createTime": "2023-02-09 09:17:08",
				"updateTime": "2023-02-09 09:17:08",
				"operator": "郑悦来",
				"id": 1531,
				"fldName": "测试部门二",
				"description": null,
				"parentId": 1517,
				"sort": 2.0,
				"orgSource": "default",
				"uuid": "7e6dbf33217249d3829876a3a633357c",
				"showFlag": 1,
				"fldCode": "7e6dbf33217249d3829876a3a633357c",
				"children": [],
				"path": [{
					"delFlag": 0,
					"createTime": "2022-12-02 14:45:04",
					"updateTime": "2022-12-02 14:45:04",
					"operator": null,
					"id": 1517,
					"fldName": "北京XXX网络科技有限公司",
					"description": null,
					"parentId": -1,
					"sort": 6.0,
					"orgSource": "default",
					"uuid": "105d4a50b07d45f3951f055381fa8e2f",
					"showFlag": 1
				}, {
					"delFlag": 0,
					"createTime": "2023-02-09 09:17:08",
					"updateTime": "2023-02-09 09:17:08",
					"operator": "郑悦来",
					"id": 1531,
					"fldName": "测试部门二",
					"description": null,
					"parentId": 1517,
					"sort": 2.0,
					"orgSource": "default",
					"uuid": "7e6dbf33217249d3829876a3a633357c",
					"showFlag": 1
				}]
			}],
			"path": [{
				"delFlag": 0,
				"createTime": "2022-12-02 14:45:04",
				"updateTime": "2022-12-02 14:45:04",
				"operator": null,
				"id": 1517,
				"fldName": "北京XXX网络科技有限公司",
				"description": null,
				"parentId": -1,
				"sort": 6.0,
				"orgSource": "default",
				"uuid": "105d4a50b07d45f3951f055381fa8e2f",
				"showFlag": 1
			}]
		}],
		"staffList": [{
			"delFlag": 0,
			"createTime": "2022-12-02 14:23:29",
			"updateTime": "2023-03-06 11:30:02",
			"operator": "wlm",
			"id": 13155,
			"fldName": "XXX",
			"mobile": "18012342055",
			"uuid": "372ab62396144d7f8dda42df2ebff00c",
			"gender": "MALE",
			"email": "xxx@xxx.com",
			"employeeNo": "E00001234536",
			"identityCard": "4302XXXXXXXXXXX924",
			"photo": [],
			"hasAccount": 0,
			"organizationId": 1501,
			"positionId": 3,
			"rankId": 1,
			"remark": "",
			"dimission": 1,
			"comeFrom": 1,
			"positionName": null,
			"rankName": null,
			"deptNames": [{
				"id": 1500,
				"deptName": "XXXX"
			}, {
				"id": 1501,
				"deptName": "XXX"
			}],
			"userSourceName": null,
			"deptName": null,
			"misCode": null
		}]
	},
	"success": true
}
```

#### 1.2 部门员工懒加载

| 地址     | /api/organization/staff/lazyTree |
| -------- | --------------------------------- |
| 请求方式 | GET                               |

请求参数：

| 参数名   | 类型    | 位置  | 说明             |
| -------- | ------- | ----- | ---------------- |
| organizationId  | Integer | query | 部门ID |

请求示例：
```
GET /api/organization/staff/lazyTree?organizationId=1500
```
返回参数：
| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | Integer | ID |
| name | String | 名称 |
| tyoe | String | 类型：organization-部门；staff-员工 |

返回示例：

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": [
        {
            "id": 1500,
            "name": "昌平中心",
            "type": "organization"
        },
        {
            "id": 1517,
            "name": "北京XXX网络科技有限公司",
            "type": "organization"
        },
        {
            "id": 1528,
            "name": "昌平区",
            "type": "organization"
        }
    ],
    "success": true
}
```

#### 1.3 职级查询

| 地址     | /api/organization/rank/list |
| -------- | --------------------------------- |
| 请求方式 | GET                               |

请求参数：
无

返回示例：
```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [{
		"delFlag": 0,
		"createTime": "2022-11-25 14:33:19",
		"updateTime": "2023-03-20 15:17:32",
		"operator": "郭思伽",
		"id": 1,
		"fldName": "普通员工",
		"fldLevel": 1
	}, {
		"delFlag": 0,
		"createTime": "2022-11-29 09:58:35",
		"updateTime": "2022-11-29 09:58:35",
		"operator": null,
		"id": 2,
		"fldName": "主管",
		"fldLevel": 2
	}, {
		"delFlag": 0,
		"createTime": "2022-11-29 09:58:57",
		"updateTime": "2022-11-29 09:58:57",
		"operator": null,
		"id": 3,
		"fldName": "副职经理",
		"fldLevel": 3
	}, {
		"delFlag": 0,
		"createTime": "2022-11-29 09:59:01",
		"updateTime": "2022-11-29 09:59:01",
		"operator": null,
		"id": 4,
		"fldName": "正职经理",
		"fldLevel": 4
	}, {
		"delFlag": 0,
		"createTime": "2022-11-29 14:59:05",
		"updateTime": "2022-11-29 14:59:05",
		"operator": null,
		"id": 5,
		"fldName": "公司领导",
		"fldLevel": 5
	}],
	"success": true
}
```

#### 1.4 岗位查询

| 地址     | /api/organization/position/list |
| -------- | --------------------------------- |
| 请求方式 | GET                               |

请求参数：
无

返回示例：
```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [{
		"delFlag": 0,
		"createTime": "2022-11-25 14:33:12",
		"updateTime": "2022-11-25 14:33:12",
		"operator": null,
		"id": 1,
		"fldName": "默认岗位"
	}, {
		"delFlag": 0,
		"createTime": "2022-12-02 14:17:05",
		"updateTime": "2022-12-02 14:17:05",
		"operator": null,
		"id": 3,
		"fldName": "组织人事部经理"
	}, {
		"delFlag": 0,
		"createTime": "2022-12-02 14:17:19",
		"updateTime": "2022-12-02 14:17:19",
		"operator": null,
		"id": 4,
		"fldName": "薪酬福利主管"
	}, {
		"delFlag": 0,
		"createTime": "2022-12-02 14:17:27",
		"updateTime": "2022-12-02 14:17:27",
		"operator": null,
		"id": 5,
		"fldName": "客服专员"
	}, {
		"delFlag": 0,
		"createTime": "2022-12-02 14:17:39",
		"updateTime": "2022-12-02 14:17:39",
		"operator": null,
		"id": 7,
		"fldName": "干部人事主管"
	}, {
		"delFlag": 0,
		"createTime": "2022-12-02 14:17:50",
		"updateTime": "2022-12-02 14:17:50",
		"operator": null,
		"id": 9,
		"fldName": "培训专员"
	}, {
		"delFlag": 0,
		"createTime": "2022-12-02 14:17:57",
		"updateTime": "2022-12-02 14:17:57",
		"operator": null,
		"id": 10,
		"fldName": "人事专员"
	}, {
		"delFlag": 0,
		"createTime": "2022-12-02 14:18:14",
		"updateTime": "2022-12-02 14:18:14",
		"operator": null,
		"id": 11,
		"fldName": "楼宇自控工程师"
	}, {
		"delFlag": 0,
		"createTime": "2023-01-06 10:17:52",
		"updateTime": "2023-01-06 10:17:52",
		"operator": "郑悦来",
		"id": 14,
		"fldName": "信息中心经理"
	}, {
		"delFlag": 0,
		"createTime": "2023-01-06 10:18:17",
		"updateTime": "2023-01-06 10:18:17",
		"operator": "郑悦来",
		"id": 15,
		"fldName": "智慧信息部负责人"
	}, {
		"delFlag": 0,
		"createTime": "2023-01-06 10:20:00",
		"updateTime": "2023-01-06 10:20:00",
		"operator": "郑悦来",
		"id": 16,
		"fldName": "信息化平台管理员"
	}, {
		"delFlag": 0,
		"createTime": "2023-01-06 10:20:16",
		"updateTime": "2023-01-06 10:20:16",
		"operator": "郑悦来",
		"id": 17,
		"fldName": "园区智能化集成工程师"
	}],
	"success": true
}
```
#### 1.5 部门查询

| 地址     | /api/organization/organization/treesVague |
| -------- | ----------------------------------------- |
| 请求方式 | GET                                       |

请求参数：
无

```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [{
		"delFlag": 0,
		"createTime": "2022-12-02 14:31:56",
		"updateTime": "2022-12-02 14:31:56",
		"operator": null,
		"id": 1500,
		"fldName": "昌平中心",
		"description": null,
		"parentId": -1,
		"sort": 5.0,
		"orgSource": "default",
		"uuid": "305b38396e0e40038b7440e3abb33805",
		"showFlag": 1,
		"fldCode": "305b38396e0e40038b7440e3abb33805",
		"children": [{
			"delFlag": 0,
			"createTime": "2022-12-02 14:32:16",
			"updateTime": "2022-12-02 14:32:16",
			"operator": null,
			"id": 1501,
			"fldName": "组织人事部",
			"description": null,
			"parentId": 1500,
			"sort": 1.0,
			"orgSource": "default",
			"uuid": "b4bd1909dff949e894bc49d200d0d355",
			"showFlag": 1,
			"fldCode": "b4bd1909dff949e894bc49d200d0d355",
			"children": [],
			"path": [{
				"delFlag": 0,
				"createTime": "2022-12-02 14:31:56",
				"updateTime": "2022-12-02 14:31:56",
				"operator": null,
				"id": 1500,
				"fldName": "昌平中心",
				"description": null,
				"parentId": -1,
				"sort": 5.0,
				"orgSource": "default",
				"uuid": "305b38396e0e40038b7440e3abb33805",
				"showFlag": 1
			}, {
				"delFlag": 0,
				"createTime": "2022-12-02 14:32:16",
				"updateTime": "2022-12-02 14:32:16",
				"operator": null,
				"id": 1501,
				"fldName": "组织人事部",
				"description": null,
				"parentId": 1500,
				"sort": 1.0,
				"orgSource": "default",
				"uuid": "b4bd1909dff949e894bc49d200d0d355",
				"showFlag": 1
			}],
			"staffs": null
		}
	}],
	"success": true
}
```