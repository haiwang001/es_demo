package com.bjxczy.onepark.domain.model.organization.service;


import java.util.List;
import java.util.Optional;

import com.bjxczy.onepark.interfaces.vo.OrganizationTwoTreeVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import com.bjxczy.onepark.domain.model.organization.repository.OrganizationRepository;
import com.bjxczy.onepark.domain.model.organization.valueobject.ParentId;
import com.bjxczy.onepark.domain.valueobject.Uuid;
import com.bjxczy.onepark.interfaces.service.OrganizationQueryService;
import com.bjxczy.onepark.interfaces.vo.OrganizationTreeVO;

@SpringBootTest
public class OrganizationDomainServcieTest {
	
	@Autowired
	private OrganizationDomainService organizationDomainService;
	@Autowired
	private OrganizationQueryService organizationQueryService;
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Test
	public void createOrganizationTest() {
		organizationDomainService.createOrganization(new Organization("测试部门", null, new ParentId(null), "default", new Uuid()));
	}
	
	@Test
	public void removeTest() {
		organizationDomainService.remove(1485);
	}
	
	@Test
	public void listTreeTest() {
		List<OrganizationTreeVO> tree = organizationQueryService.listTree();
		System.out.println(tree);
	}

	@Test
	public void listTwoTreeTest() {
		List<OrganizationTwoTreeVO> tree = organizationQueryService.listTwoTree();
		System.out.println(tree);
	}
	
	@Test
	public void validateTest() {
		organizationRepository.findById(1483).ifPresent(model -> {
			model.setFldName("");
			model.setDescription("222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222");
			model.validate();
		});
	}

}
