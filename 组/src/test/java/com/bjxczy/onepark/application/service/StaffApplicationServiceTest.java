package com.bjxczy.onepark.application.service;


import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.application.command.CreateStaffCommand;
import com.bjxczy.onepark.application.command.RemoveStaffCommand;
import com.bjxczy.onepark.application.command.UpdateStaffCommand;
import com.bjxczy.onepark.domain.model.staff.entity.Position;
import com.bjxczy.onepark.domain.model.staff.entity.Rank;
import com.bjxczy.onepark.domain.model.staff.respository.PositionRepository;
import com.bjxczy.onepark.domain.model.staff.respository.RankRepository;
import com.bjxczy.onepark.interfaces.dto.QueryStaffDTO;
import com.bjxczy.onepark.interfaces.service.StaffQueryService;
import com.bjxczy.onepark.interfaces.vo.StaffVO;

@SpringBootTest
public class StaffApplicationServiceTest {

	@Autowired
	private StaffApplicationService staffApplicationService;
	@Autowired
	private RankRepository rankRepository;
	@Autowired
	private PositionRepository positionRepository;
	@Autowired
	private StaffQueryService staffQueryService;

	@Test
	public void createOther() {
		// 创建职级
		Position position = new Position();
		position.setFldName("默认职级");
		positionRepository.save(position);
		// 创建岗位
		Rank rank = new Rank();
		rank.setFldName("默认岗位");
		rankRepository.save(rank);
	}

	@Test
	public void createStaffTest() {
		CreateStaffCommand command = new CreateStaffCommand();
		command.setFldName("");
		command.setMobile("15510524531");
		command.setOrganizationId(1483);
		staffApplicationService.createStaff(command);
	}

	@Test
	public void updateStaffTest() {
		UpdateStaffCommand command = new UpdateStaffCommand();
		command.setId(13151);
		command.setFldName("员工1改");
		command.setGender(2);
		command.setEmail("zhengyuelai@bjxczy.com");
		command.setEmployeeNo("E0000123456");
		command.setIdentityCard("110101201808185430");
		List<String> photo = new ArrayList<>();
		photo.add("http://baidu.com");
		command.setPhoto(photo);
		command.setOrganizationId(1483);
		command.setPositionIds("1");
		command.setRankId(1);
		command.setRemark("备注");
		staffApplicationService.updateStaff(command);
	}

	@Test
	public void removeStaffTest() {
		RemoveStaffCommand command = new RemoveStaffCommand(13151);
		staffApplicationService.remove(command);;
	}

	@Test
	public void listStaffTest() {
		IPage<StaffVO> data = staffQueryService.listPageStaff(new QueryStaffDTO());
		System.out.println(data);
	}

}
