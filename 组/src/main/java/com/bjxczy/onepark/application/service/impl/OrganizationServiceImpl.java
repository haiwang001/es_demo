package com.bjxczy.onepark.application.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjxczy.onepark.application.command.ChangeOrgNameCommand;
import com.bjxczy.onepark.application.command.CreateOrganizationCommand;
import com.bjxczy.onepark.application.command.RemoveOrganizationCommand;
import com.bjxczy.onepark.application.service.OrganizationService;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.utils.OptionalUtils;
import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import com.bjxczy.onepark.domain.model.organization.repository.OrganizationRepository;
import com.bjxczy.onepark.domain.model.organization.service.OrganizationDomainService;
import com.bjxczy.onepark.domain.model.organization.valueobject.ParentId;
import com.bjxczy.onepark.domain.valueobject.Uuid;

@Service
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	private OrganizationDomainService organizationDomainService;
	@Autowired
	private OrganizationRepository organizationRepository;

	@Override
	public void createOrganzation(CreateOrganizationCommand command) {
		organizationDomainService.createOrganization(new Organization(command.getFldName(), command.getDescription(),
				new ParentId(command.getParentId()), "default", new Uuid()));
	}

	@Override
	public void removeOrganization(RemoveOrganizationCommand command) {
		organizationDomainService.remove(command.getId());
	}

	@Override
	public void changeOrgName(ChangeOrgNameCommand command) {
		OptionalUtils.ifPresentOrElse(organizationRepository.findById(command.getId()), model -> {
			model.changeName(command.getName(),command.getId());
			organizationRepository.saveModel(model);
		}, () -> new ResultException("部门不存在！"));
	}

}
