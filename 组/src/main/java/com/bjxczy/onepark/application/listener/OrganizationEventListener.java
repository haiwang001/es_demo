package com.bjxczy.onepark.application.listener;

import com.bjxczy.onepark.application.publish.OrganizationEventPublish;
import com.bjxczy.onepark.domain.model.organization.event.CreateOrganizationEvent;
import com.bjxczy.onepark.domain.model.organization.event.RemoveOrganizationEvent;
import com.bjxczy.onepark.domain.model.organization.event.UpdateOranizationEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName OrganizationEventListener
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/31 17:06
 * @Version 1.0
 */
@Component
@Slf4j
public class OrganizationEventListener {

    @Autowired
    private OrganizationEventPublish organizationEventPublish;

    @EventListener
    public void createOrganizationEventHandler(CreateOrganizationEvent event) {
        log.info("[OrganizationEvent] createOrganizationEvent: {}", event);
        organizationEventPublish.createOrganizationEventPublish(event);
    }

    @EventListener
    public void updateOranizationEventHandler(UpdateOranizationEvent event) {
        log.info("[OrganizationEvent] updateOranizationEvent: {}", event);
        organizationEventPublish.updateOranizationEventPublish(event);
    }

    @EventListener
    public void removeOrganizationEventHandler(RemoveOrganizationEvent event) {
        log.info("[OrganizationEvent] removeOrganizationEvent: {}", event);
        organizationEventPublish.removeOrganizationEventPublish(event);
    }

}
