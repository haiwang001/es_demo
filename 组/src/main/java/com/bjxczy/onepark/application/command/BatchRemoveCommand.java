package com.bjxczy.onepark.application.command;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BatchRemoveCommand {
	
	private List<Integer> idList;

}
