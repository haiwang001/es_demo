package com.bjxczy.onepark.application.service;

import com.bjxczy.onepark.application.command.ChangeOrgNameCommand;
import com.bjxczy.onepark.application.command.CreateOrganizationCommand;
import com.bjxczy.onepark.application.command.RemoveOrganizationCommand;

public interface OrganizationService {

	void createOrganzation(CreateOrganizationCommand command);

	void removeOrganization(RemoveOrganizationCommand removeOrganizationCommand);

	void changeOrgName(ChangeOrgNameCommand command);

}
