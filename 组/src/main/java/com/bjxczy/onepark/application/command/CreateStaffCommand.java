package com.bjxczy.onepark.application.command;

import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import lombok.Data;

@Data
public class CreateStaffCommand {

	private String fldName;

	private String mobile;

	private Integer gender;

	private String Email;

	private String employeeNo;

	private String identityCard;

	private List<String> photo;

	private Integer organizationId;

	private String positionIds;

	private Integer rankId;

	private String misCode;

	private String remark;

}
