package com.bjxczy.onepark.application.command;

import lombok.Data;

@Data
public class RemoveStaffCommand {
	
	private Integer id;
	
	private String uuid;
	
	public RemoveStaffCommand(Integer id) {
		this.id = id;
	}

	public RemoveStaffCommand(String uuid) {
		this.uuid = uuid;
	}
	
	public boolean isInternal() {
		return this.id != null;
	}
}
