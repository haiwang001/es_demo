package com.bjxczy.onepark.application.publish;

import com.bjxczy.onepark.domain.model.staff.event.CreateStaffEvent;
import com.bjxczy.onepark.domain.model.staff.event.RemoveStaffEvent;
import com.bjxczy.onepark.domain.model.staff.event.UpdateStaffEvent;

public interface StaffEventPublish {

	void updateStaffEventPublish(UpdateStaffEvent event);

	void deleteStaffEventPublish(RemoveStaffEvent event);

	void createStaffEventPublish(CreateStaffEvent event);
}
