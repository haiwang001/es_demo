package com.bjxczy.onepark.application.publish;

import com.bjxczy.onepark.domain.model.organization.event.CreateOrganizationEvent;
import com.bjxczy.onepark.domain.model.organization.event.RemoveOrganizationEvent;
import com.bjxczy.onepark.domain.model.organization.event.UpdateOranizationEvent;

public interface OrganizationEventPublish {
    void createOrganizationEventPublish(CreateOrganizationEvent event);

    void updateOranizationEventPublish(UpdateOranizationEvent event);

    void removeOrganizationEventPublish(RemoveOrganizationEvent event);
}
