package com.bjxczy.onepark.application.service;

import com.bjxczy.onepark.application.command.BatchRemoveCommand;
import com.bjxczy.onepark.application.command.CreateStaffCommand;
import com.bjxczy.onepark.application.command.RemoveStaffCommand;
import com.bjxczy.onepark.application.command.UpdateStaffCommand;

public interface StaffApplicationService {
	
	void createStaff(CreateStaffCommand command);

	void updateStaff(UpdateStaffCommand command);

	void remove(RemoveStaffCommand removeStaffCommand);

	void batchRemove(BatchRemoveCommand batchRemoveCommand);

}
