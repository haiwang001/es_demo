package com.bjxczy.onepark.application.command;

import lombok.Data;

@Data
public class ChangeOrgNameCommand {
	
	private String id;
	
	private String name;

}
