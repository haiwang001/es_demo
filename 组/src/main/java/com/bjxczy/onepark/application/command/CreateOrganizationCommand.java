package com.bjxczy.onepark.application.command;

import lombok.Data;

@Data
public class CreateOrganizationCommand {
	
	private Integer parentId;
	
	private String fldName;
	
	private String description;

}
