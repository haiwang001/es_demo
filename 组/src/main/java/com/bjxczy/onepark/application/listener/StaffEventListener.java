package com.bjxczy.onepark.application.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.bjxczy.onepark.application.publish.StaffEventPublish;
import com.bjxczy.onepark.domain.model.staff.event.CreateStaffEvent;
import com.bjxczy.onepark.domain.model.staff.event.RemoveStaffEvent;
import com.bjxczy.onepark.domain.model.staff.event.UpdateStaffEvent;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StaffEventListener {

	@Autowired
	private StaffEventPublish staffEventPublish;

	@EventListener
	public void createStaffEventHandler(CreateStaffEvent event) {
		log.info("[StaffEvent] createStaffEvent: {}", event);
		staffEventPublish.createStaffEventPublish(event);
	}

	@EventListener
	public void updateStaffEventHandler(UpdateStaffEvent event) {
		log.info("[StaffEvent] updateStaffEvent: {}", event);
		staffEventPublish.updateStaffEventPublish(event);
	}

	@EventListener
	public void removeStaffEventHandler(RemoveStaffEvent event) {
		log.info("[StaffEvent] removeStaffEvent: {}", event);
		staffEventPublish.deleteStaffEventPublish(event);
	}

}
