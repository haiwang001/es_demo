package com.bjxczy.onepark.application.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RemoveOrganizationCommand {
	
	private Integer id;

}
