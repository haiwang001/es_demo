package com.bjxczy.onepark.application.service.impl;

import java.util.HashSet;
import java.util.Optional;

import com.bjxczy.core.web.log.LogContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjxczy.onepark.application.command.BatchRemoveCommand;
import com.bjxczy.onepark.application.command.CreateStaffCommand;
import com.bjxczy.onepark.application.command.RemoveStaffCommand;
import com.bjxczy.onepark.application.command.UpdateStaffCommand;
import com.bjxczy.onepark.application.service.StaffApplicationService;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.utils.OptionalUtils;
import com.bjxczy.onepark.domain.model.organization.repository.OrganizationRepository;
import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;
import com.bjxczy.onepark.domain.model.staff.respository.StaffRepository;
import com.bjxczy.onepark.domain.model.staff.service.StaffDomainService;
import com.bjxczy.onepark.domain.model.staff.valueobject.Email;
import com.bjxczy.onepark.domain.model.staff.valueobject.EmployeeNo;
import com.bjxczy.onepark.domain.model.staff.valueobject.IdentityCard;
import com.bjxczy.onepark.domain.model.staff.valueobject.Mobile;
import com.bjxczy.onepark.domain.model.staff.valueobject.Photo;
import com.bjxczy.onepark.domain.valueobject.GenderHepler;
import com.bjxczy.onepark.domain.valueobject.Uuid;

@Service
public class StaffApplicationServcieImpl implements StaffApplicationService {

	@Autowired
	private StaffDomainService staffDomainService;
	@Autowired
	private StaffRepository staffRepository;
	@Autowired
	private OrganizationRepository organizationRepository;

	@Override
	public void createStaff(CreateStaffCommand command) {
		organizationRepository.findById(command.getOrganizationId()).
				orElseThrow(() -> new ResultException("部门不存在！"));
		Staff staff = new Staff(command.getFldName(),
				new Mobile(command.getMobile()),
				new Uuid(),
				GenderHepler.ofValue(command.getGender()),
				new Email(command.getEmail()),
				new EmployeeNo(command.getMisCode()),
				new IdentityCard(command.getIdentityCard()),
				new Photo(command.getPhoto()),
				command.getOrganizationId(),
				command.getPositionIds(),
				command.getRankId(),
				command.getRemark());
		staffDomainService.createStaff(staff);
	}

	@Override
	public void updateStaff(UpdateStaffCommand command) {
		OptionalUtils.ifPresentOrElse(staffRepository.findById(command.getId()), staff -> {
			organizationRepository.findById(command.getOrganizationId())
					.orElseThrow(() -> new ResultException("部门不存在！"));
			staffRepository.findByEmployeeNo(new EmployeeNo(command.getMisCode())).ifPresent(s -> {
				if (!s.getId().equals(command.getId())){
					throw new ResultException("员工编号已存在！");
				}
			});
			staff.changeStaffInfo(command.getFldName(),
					GenderHepler.ofValue(command.getGender()),
					new Email(command.getEmail()),
					new EmployeeNo(command.getMisCode()),
					new IdentityCard(command.getIdentityCard()),
					new Photo(command.getPhoto()),
					command.getOrganizationId(),
					command.getPositionIds(),
					command.getRankId(),
					command.getRemark(),
					command.getDimission());
			staffRepository.saveModel(staff);
			LogContextHolder.put("fldName",command.getFldName());
		}, () -> new ResultException("员工不存在！"));
	}

	@Override
	public void remove(RemoveStaffCommand command) {
		Optional<Staff> optional = command.isInternal()
		? staffRepository.findById(command.getId()) :
				staffRepository.findByUuid(new Uuid(command.getUuid()));

		OptionalUtils.ifPresentOrElse(optional, staff -> {
			LogContextHolder.put("fldName",staff.getFldName());
			staffRepository.remove(staff);
			LogContextHolder.put("fldName",staff.getFldName());
		}, () -> new ResultException("员工不存在！"));
	}

	@Override
	public void batchRemove(BatchRemoveCommand command) {
		//删除的员工名
		HashSet<String> names = new HashSet<>();
		// 临时处理，后续优化
		for (Integer id : command.getIdList()) {
			OptionalUtils.ifPresentOrElse(staffRepository.findById(id), model -> {
				names.add(model.getFldName());
			}, () -> new ResultException("员工不存在！"));
			this.remove(new RemoveStaffCommand(id));
		}
		String fldNames = String.join(",", names);
		LogContextHolder.put("fldNames",fldNames);
	}
}
