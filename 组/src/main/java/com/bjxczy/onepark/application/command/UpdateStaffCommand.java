package com.bjxczy.onepark.application.command;

import java.util.List;
import java.util.Set;

import lombok.Data;

@Data
public class UpdateStaffCommand {

	private Integer id;

	private String uuid;

	private String fldName;

	private Integer gender;

	private String email;

	private String employeeNo;

	private String identityCard;

	private List<String> photo;

	private Integer organizationId;

	private String positionIds;

	private Integer rankId;

	private String remark;

	private Integer dimission;

	private String misCode;

}
