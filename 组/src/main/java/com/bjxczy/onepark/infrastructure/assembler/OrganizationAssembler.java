package com.bjxczy.onepark.infrastructure.assembler;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import com.bjxczy.onepark.infrastructure.po.OrganizationPO;
import com.bjxczy.onepark.interfaces.vo.OrganizationTreeVO;

@Mapper(componentModel = "spring")
public interface OrganizationAssembler {
	
	@Mapping(source = "parentId.value", target = "parentId")
	@Mapping(source = "sort.value", target = "sort")
	@Mapping(source = "uuid.value", target = "uuid")
	OrganizationPO toPO(Organization model);
	
	@Mapping(source = "parentId", target = "parentId.value")
	@Mapping(source = "sort", target = "sort.value")
	@Mapping(source = "uuid", target = "uuid.value")
	Organization toModel(OrganizationPO po);
	
	@Mapping(source = "uuid", target = "fldCode")
	OrganizationTreeVO toTreeVO(OrganizationPO po);

}
