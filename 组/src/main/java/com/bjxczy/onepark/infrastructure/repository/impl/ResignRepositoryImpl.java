package com.bjxczy.onepark.infrastructure.repository.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;
import com.bjxczy.onepark.infrastructure.mapper.StaffMapper;
import com.bjxczy.onepark.infrastructure.po.StaffPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResignRepositoryImpl {

    @Autowired
    private StaffMapper staffMapper;
    public void delete(Integer id ){
        UpdateWrapper<StaffPO> staffUpdateWrapper = new UpdateWrapper<>();
        staffUpdateWrapper.eq("id",id);
        staffMapper.delete(staffUpdateWrapper);
    }
}
