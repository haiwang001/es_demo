package com.bjxczy.onepark.infrastructure.publish.impl;

import com.bjxczy.core.rabbitmq.supports.OrganizationBaseListener;
import com.bjxczy.onepark.application.publish.OrganizationEventPublish;
import com.bjxczy.onepark.common.model.organization.OrganizationInformation;
import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import com.bjxczy.onepark.domain.model.organization.event.CreateOrganizationEvent;
import com.bjxczy.onepark.domain.model.organization.event.RemoveOrganizationEvent;
import com.bjxczy.onepark.domain.model.organization.event.UpdateOranizationEvent;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName OrganizationEventPublishImpl
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/31 17:16
 * @Version 1.0
 */
@Component
public class OrganizationEventPublishImpl extends OrganizationBaseListener implements OrganizationEventPublish {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Override
    public void createOrganizationEventPublish(CreateOrganizationEvent event) {
        Organization organization = event.getOrganization();
        OrganizationInformation information = new OrganizationInformation();
        information.setId(organization.getId());
        information.setDescription(organization.getDescription());
        information.setFldName(organization.getFldName());
        information.setParentId(organization.getParentId().getValue());
        rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME,CREATE_ORGANIZATION_EVENT,information);
    }

    @Override
    public void updateOranizationEventPublish(UpdateOranizationEvent event) {
        Organization organization = event.getOrganization();
        OrganizationInformation information = new OrganizationInformation();
        information.setId(organization.getId());
        information.setDescription(organization.getDescription());
        information.setFldName(organization.getFldName());
        information.setParentId(organization.getParentId().getValue());
        rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME,UPDATE_ORGANIZATION_EVENT,information);
    }

    @Override
    public void removeOrganizationEventPublish(RemoveOrganizationEvent event) {
        Organization organization = event.getOrganization();
        OrganizationInformation information = new OrganizationInformation();
        information.setId(organization.getId());
        information.setDescription(organization.getDescription());
        information.setFldName(organization.getFldName());
        information.setParentId(organization.getParentId().getValue());
        rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME,REMOVE_ORGANIZATION_EVENT,information);
    }
}
