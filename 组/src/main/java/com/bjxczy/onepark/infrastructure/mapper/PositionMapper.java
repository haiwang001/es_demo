package com.bjxczy.onepark.infrastructure.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.domain.model.staff.entity.Position;

@Mapper
public interface PositionMapper extends BaseMapper<Position> {

}
