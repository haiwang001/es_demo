package com.bjxczy.onepark.infrastructure.po;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@TableName("tbl_organization")
@EqualsAndHashCode(callSuper = false)
public class OrganizationPO extends UserOperator implements Serializable {
	
	private static final long serialVersionUID = 1299335090198471852L;

	@TableId(type = IdType.AUTO)
	private Integer id;
	
	private String fldName;
	
	private String description;
	
	private Integer parentId;
	
	private Float sort;
	
	private String orgSource;
	
	private String uuid;
	
	private Integer showFlag;

}
