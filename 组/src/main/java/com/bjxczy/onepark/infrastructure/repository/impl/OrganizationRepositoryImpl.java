package com.bjxczy.onepark.infrastructure.repository.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bjxczy.onepark.infrastructure.mapper.StaffMapper;
import com.bjxczy.onepark.infrastructure.po.StaffPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.core.web.base.MyBatisPlusRepositoryImpl;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.OrganizationInformation;
import com.bjxczy.onepark.common.model.organization.OrganizationTree;
import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import com.bjxczy.onepark.domain.model.organization.repository.OrganizationRepository;
import com.bjxczy.onepark.domain.model.organization.valueobject.ParentId;
import com.bjxczy.onepark.infrastructure.assembler.OrganizationAssembler;
import com.bjxczy.onepark.infrastructure.mapper.OrganizationMapper;
import com.bjxczy.onepark.infrastructure.po.OrganizationPO;
import com.bjxczy.onepark.interfaces.service.OrganizationQueryService;
import com.bjxczy.onepark.interfaces.vo.OrganizationTreeVO;
import com.bjxczy.onepark.interfaces.vo.OrganizationTwoTreeVO;

@Service
public class OrganizationRepositoryImpl
		extends MyBatisPlusRepositoryImpl<OrganizationMapper, OrganizationPO, Organization>
		implements OrganizationRepository, OrganizationQueryService {

	@Autowired
	private OrganizationAssembler organizationAssembler;
	@Autowired
	private OrganizationMapper OrganizationMapper;
	@Autowired
	private StaffMapper staffMapper;
	@Override
	public List<Organization> findByParentId(ParentId parentId) {
		return listToModel(lambdaQuery().eq(OrganizationPO::getParentId, parentId.getValue()).list());
	}

	@CacheEvict(value = { "organizationTree", "orgParentList", "orgChildrenIdList" }, allEntries = true)
	@Override
	public Boolean saveModel(Organization model) {
		return super.defaultSaveModel(model);
	}

	@CacheEvict(value = { "organizationTree", "orgParentList", "orgChildrenIdList" }, allEntries = true)
	@Override
	public Boolean remove(Organization model) {
		return super.defaultRemove(model);
	}

	@Override
	public OrganizationPO toPO(Organization source) {
		return organizationAssembler.toPO(source);
	}

	@Override
	public Organization toModel(OrganizationPO source) {
		return organizationAssembler.toModel(source);
	}

	@Cacheable("organizationTree")
	@Override
	public List<OrganizationTreeVO> listTree() {
		return listTree(parentId -> lambdaQuery().eq(OrganizationPO::getParentId, parentId)
				.orderByAsc(OrganizationPO::getSort).list());
	}

	private List<OrganizationTreeVO> listTree(Function<Integer, List<OrganizationPO>> func) {
		List<OrganizationPO> list = func.apply(-1);
		return setChildren(list, null, func);
	}
	private List<OrganizationTreeVO> treeStaffs(Function<Integer, List<OrganizationPO>> func) {
		List<OrganizationPO> list = func.apply(-1);
		return setStaffChildren(list, null, func);
	}
	private List<OrganizationTreeVO> setStaffChildren(List<OrganizationPO> list, OrganizationTreeVO parent,
												 Function<Integer, List<OrganizationPO>> func) {
		List<OrganizationTreeVO> result = new ArrayList<>();
		for (OrganizationPO item : list) {
			OrganizationTreeVO vo = organizationAssembler.toTreeVO(item);
			System.out.println("-:" + item.getId());
			List<OrganizationPO> childrenList = func.apply(item.getId());
			List<OrganizationPO> path = new ArrayList<>();
			List<StaffPO> staffPOS = staffMapper
					.selectList(new LambdaQueryWrapper<StaffPO>().eq(StaffPO::getOrganizationId, item.getId()));
			vo.setStaffs(staffPOS);

			if (parent != null) {
				path.addAll(parent.getPath());
			}
			path.add(item);
			vo.setPath(path);
			List<OrganizationTreeVO> voChildrenList = setChildren(childrenList, vo, func);
			vo.setChildren(voChildrenList);
			result.add(vo);
		}
		return result;
	}
	private List<OrganizationTreeVO> setChildren(List<OrganizationPO> list, OrganizationTreeVO parent,
			Function<Integer, List<OrganizationPO>> func) {
		List<OrganizationTreeVO> result = new ArrayList<>();
		for (OrganizationPO item : list) {
			OrganizationTreeVO vo = organizationAssembler.toTreeVO(item);
			System.out.println("-:" + item.getId());
			List<OrganizationPO> childrenList = func.apply(item.getId());
			List<OrganizationPO> path = new ArrayList<>();
			if (parent != null) {
				path.addAll(parent.getPath());
			}
			path.add(item);
			vo.setPath(path);
			List<OrganizationTreeVO> voChildrenList = setChildren(childrenList, vo, func);
			vo.setChildren(voChildrenList);
			result.add(vo);
		}
		return result;
	}

	@Override
	@Cacheable("orgParentList")
	public List<DeptNameInfo> getParentList(Integer organizationId) {
		List<DeptNameInfo> list = new ArrayList<>();
		do {
			OrganizationPO po = getById(organizationId);
			if (po == null) {
				break;
			}
			list.add(new DeptNameInfo(po.getId(), po.getFldName()));
			organizationId = po.getParentId();
		} while (organizationId != -1);
		Collections.reverse(list);
		return list;
	}

	@Override
	public List<DeptNameInfo> getParentArray(Integer organizationId) {
		List<DeptNameInfo> list = new ArrayList<>();
		do {
			OrganizationPO po = OrganizationMapper.selectByorganizationId(organizationId);
			if (po == null) {
				break;
			}
			list.add(new DeptNameInfo(po.getId(), po.getFldName()));
			organizationId = po.getParentId();
		} while (organizationId != -1);
		Collections.reverse(list);
		return list;
	}

	@Override
	public List<OrganizationTreeVO> deptTrees(Integer id) {
		return listTree(parentId -> lambdaQuery().eq(OrganizationPO::getParentId, parentId)
				.ne(OrganizationPO::getId, id).orderByAsc(OrganizationPO::getSort).list());
	}

	@Override
	public void changeParent(Integer id, Integer parentId) {
		this.update(Wrappers.<OrganizationPO>lambdaUpdate().eq(OrganizationPO::getId, id)
				.set(OrganizationPO::getParentId, parentId));
	}

	@Override
	public List<OrganizationTree> findTree() {
		List<OrganizationTree> list1 = new ArrayList<>();
		List<OrganizationTreeVO> organizationTreeVOS = listTree(parentId -> lambdaQuery().eq(OrganizationPO::getParentId, parentId)
				.orderByAsc(OrganizationPO::getSort).list());

		for (OrganizationTreeVO organizationTreeVO : organizationTreeVOS) {

			OrganizationTree organizationTree = new OrganizationTree();

			BeanUtils.copyProperties(organizationTreeVO,organizationTree);

			List<OrganizationPO> path = organizationTreeVO.getPath();

			List<OrganizationInformation> organizationInformations = new ArrayList<>();
			for (OrganizationPO organizationPO : path) {
				OrganizationInformation organizationInformation = new OrganizationInformation();
				BeanUtils.copyProperties(organizationPO,organizationInformation);
				organizationInformations.add(organizationInformation);
			}
			organizationTree.setPath(organizationInformations);

			List<OrganizationTreeVO> children = organizationTreeVO.getChildren();
			List<OrganizationTree> organizationTrees = new ArrayList<>();
			for (OrganizationTreeVO child : children) {
				OrganizationTree organizationTreess = new OrganizationTree();
				BeanUtils.copyProperties(child,organizationTreess);
				organizationTreess.setStaffs(new ArrayList<>());
				organizationTrees.add(organizationTreess);
			}
			organizationTree.setChildren(organizationTrees);
			organizationTree.setStaffs(new ArrayList<>());
			list1.add(organizationTree);
		}
		return list1;
	}

	@Override
	public List<OrganizationTwoTreeVO> listTwoTree() {
		List<OrganizationTwoTreeVO> result=new ArrayList<>();
		List<OrganizationPO> poList = lambdaQuery().eq(OrganizationPO::getParentId, -1)
				.orderByAsc(OrganizationPO::getSort).list();
		poList.forEach(e->{
			OrganizationTwoTreeVO organizationTwoTree =new OrganizationTwoTreeVO();
			organizationTwoTree.setId(e.getId());
			organizationTwoTree.setParentId(e.getParentId());
			organizationTwoTree.setDeptName(e.getFldName());
			result.add(organizationTwoTree);
			List<OrganizationPO> childrenList = lambdaQuery().eq(OrganizationPO::getParentId, e.getId())
					.orderByAsc(OrganizationPO::getSort).list();
			childrenList.forEach(c->{
				OrganizationTwoTreeVO organizationTwoTreeVO = new OrganizationTwoTreeVO();
				organizationTwoTreeVO.setId(c.getId());
				organizationTwoTreeVO.setParentId(c.getParentId());
				organizationTwoTreeVO.setDeptName(c.getFldName());
				result.add(organizationTwoTreeVO);
			});
		});
		return result;
	}

	@Override
	@Cacheable("orgTreeStaffs")
	public List<OrganizationTreeVO> treeStaffs() {

		return treeStaffs(parentId -> lambdaQuery().eq(OrganizationPO::getParentId, parentId)
				.orderByAsc(OrganizationPO::getSort).list());
	}

	@Override
	@Cacheable("orgChildrenIdList")
	public List<Integer> getChildrenIdList(Integer organizationId) {
		List<Integer> result = new ArrayList<>();
		List<OrganizationPO> list = lambdaQuery().eq(OrganizationPO::getParentId, organizationId).list();
		// 添加子部门ID
		addChildrenId(result, list);
		// 包含父ID
		result.add(organizationId);
		return result;
	}

	private void addChildrenId(List<Integer> result, List<OrganizationPO> list) {
		for (OrganizationPO item : list) {
			result.add(item.getId());
			List<OrganizationPO> childList = lambdaQuery().eq(OrganizationPO::getParentId, item.getId()).list();
			addChildrenId(result, childList);
		}
	}

	@Override
	public List<OrganizationTreeVO> listTreeByIds(Set<Integer> organizationIds) {
		if (null == organizationIds || organizationIds.size() == 0) {
			return new ArrayList<>();
		}
		return listTree(parentId -> lambdaQuery().eq(OrganizationPO::getParentId, parentId)
				.orderByAsc(OrganizationPO::getSort).in(OrganizationPO::getId, organizationIds).list());
	}

	@Override
	public List<Integer> getParentIds(Integer organizationId) {
		List<Integer> list = new ArrayList<>();
		do {
			OrganizationPO po = OrganizationMapper.selectByorganizationId(organizationId);
			if (po == null) {
				break;
			}
			list.add(organizationId);
			organizationId = po.getParentId();
		} while (organizationId != -1);
		return list;
	}

	@Override
	public List<OrganizationPO> listByParentId(Integer parentId) {
		return lambdaQuery().eq(OrganizationPO::getParentId, parentId).list();
	}

}
