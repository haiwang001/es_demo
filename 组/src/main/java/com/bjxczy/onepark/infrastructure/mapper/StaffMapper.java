package com.bjxczy.onepark.infrastructure.mapper;

import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;
import com.bjxczy.onepark.infrastructure.po.StaffPO;
import com.bjxczy.onepark.interfaces.dto.QueryStaffDTO;
import com.bjxczy.onepark.interfaces.vo.StaffVO;

@Mapper
public interface StaffMapper extends BaseMapper<StaffPO> {

	IPage<StaffVO> listPage(Page<Staff> page, @Param(Constants.WRAPPER) QueryWrapper<StaffPO> queryWrapper, @Param("dto") QueryStaffDTO dto);

	StaffVO findById(@Param("id") Integer id);

	StaffInfoFine StaffInfoFine(@Param("id") Integer id);

}
