package com.bjxczy.onepark.infrastructure.publish.impl;

import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.domain.model.staff.event.CreateStaffEvent;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjxczy.core.rabbitmq.supports.OrganizationBaseListener;
import com.bjxczy.onepark.application.publish.StaffEventPublish;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.domain.model.staff.event.RemoveStaffEvent;
import com.bjxczy.onepark.domain.model.staff.event.UpdateStaffEvent;
import com.bjxczy.onepark.infrastructure.repository.impl.StaffReposiotryImpl;

@Component
public class StaffEventPublishImpl implements StaffEventPublish {

	@Autowired
	private StaffReposiotryImpl staffReposiotryImpl;
	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Override
	public void updateStaffEventPublish(UpdateStaffEvent event) {
		StaffInformation info = staffReposiotryImpl.findInfoById(event.getStaff().getId());
		rabbitTemplate.convertAndSend(
				OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME,
				OrganizationBaseListener.UPDATE_STAFF_EVENT, info);
	}

	@Override
	public void deleteStaffEventPublish(RemoveStaffEvent event) {
		rabbitTemplate.convertAndSend(
				OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME,
				OrganizationBaseListener.REMOVE_STAFF_EVENT,
				new RemoveStaffInfo(event.getId(),
						event.getUuid(),
						event.getMobile()));
	}

	@Override
	public void createStaffEventPublish(CreateStaffEvent event) {
		StaffInformation info = staffReposiotryImpl.findInfoById(event.getStaff().getId());
		rabbitTemplate.convertAndSend(
				OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME,
				OrganizationBaseListener.CREATE_STAFF_EVENT, info);
	}

}
