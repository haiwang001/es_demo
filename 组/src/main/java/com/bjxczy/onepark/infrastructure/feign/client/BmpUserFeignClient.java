package com.bjxczy.onepark.infrastructure.feign.client;

import org.springframework.cloud.openfeign.FeignClient;

import com.bjxczy.core.feign.client.user.BaseUserFeignClient;
import com.bjxczy.onepark.common.constant.ServiceNameConstant;

@FeignClient(ServiceNameConstant.USER)
public interface BmpUserFeignClient extends BaseUserFeignClient {

}
