package com.bjxczy.onepark.infrastructure.repository.impl;

import com.alibaba.nacos.common.utils.Objects;
import com.bjxczy.onepark.common.model.organization.RankInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.domain.model.staff.entity.Position;
import com.bjxczy.onepark.infrastructure.po.StaffPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.domain.model.staff.entity.Rank;
import com.bjxczy.onepark.domain.model.staff.respository.RankRepository;
import com.bjxczy.onepark.infrastructure.mapper.RankMapper;
import com.bjxczy.onepark.interfaces.dto.QueryRankDTO;
import com.bjxczy.onepark.interfaces.dto.SaveRankDTO;
import com.bjxczy.onepark.interfaces.service.RankService;
import com.bjxczy.onepark.interfaces.service.StaffQueryService;
import com.bjxczy.onepark.interfaces.vo.RankVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RankRepositoryImpl extends ServiceImpl<RankMapper, Rank> implements RankRepository, RankService {
	
	@Autowired
	private StaffQueryService staffQueryService;

	@Override
	public void saveRank(SaveRankDTO dto) {
		Rank entity = new Rank();
		BeanUtils.copyProperties(dto, entity);
		this.saveOrUpdate(entity);
	}

	@Override
	public IPage<RankVO> listPageRank(QueryRankDTO dto) {
		QueryWrapper<Rank> query = new QueryWrapper<>();
		query.orderByAsc("fld_level");
		query.orderByDesc("update_time");
		Page<Rank> data = this.page(dto.toPage(), query);
		return data.convert(e -> {
			RankVO vo = new RankVO();
			BeanUtils.copyProperties(e, vo);
			vo.setStaffCount(staffQueryService.countByRankId(e.getId()));
			return vo;
		});
	}

	@Override
	public List<RankInfo> getRankLists() {
		List<Rank> ranks = baseMapper.selectList(new QueryWrapper<Rank>());
		List<RankInfo> rankInfos = new ArrayList<>();
		for (Rank rank : ranks) {
			rankInfos.add(toInfo(rank));
		}
		return rankInfos;
	}

	private RankInfo toInfo(Rank rank) {
		if (rank == null) {
			return null;
		}
		RankInfo info = new RankInfo();
		BeanUtils.copyProperties(rank, info);
		return info;
	}

}
