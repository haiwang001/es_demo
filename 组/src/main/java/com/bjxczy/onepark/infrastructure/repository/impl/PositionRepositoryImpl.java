package com.bjxczy.onepark.infrastructure.repository.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.domain.model.staff.entity.Position;
import com.bjxczy.onepark.domain.model.staff.respository.PositionRepository;
import com.bjxczy.onepark.infrastructure.mapper.PositionMapper;
import com.bjxczy.onepark.interfaces.dto.QueryPositionDTO;
import com.bjxczy.onepark.interfaces.dto.SavePositionDTO;
import com.bjxczy.onepark.interfaces.service.PositionService;
import com.bjxczy.onepark.interfaces.service.StaffQueryService;
import com.bjxczy.onepark.interfaces.vo.PositionVO;

@Service
public class PositionRepositoryImpl extends ServiceImpl<PositionMapper, Position> implements PositionRepository, PositionService {
	
	@Autowired
	private StaffQueryService staffQueryService;

	@Override
	public void savePosition(SavePositionDTO dto) {
		Position entity = new Position();
		BeanUtils.copyProperties(dto, entity);
		this.saveOrUpdate(entity);
	}

	@Override
	public void delPosition(Integer id) {
		this.removeById(id);
	}

	@Override
	public IPage<PositionVO> listPagePosition(QueryPositionDTO dto) {
		QueryWrapper<Position> query = new QueryWrapper<>();
		query.orderByDesc("update_time");
		Page<Position> data = this.page(dto.toPage(), query);
		return data.convert(e -> {
			PositionVO vo = new PositionVO();
			BeanUtils.copyProperties(e, vo);
			vo.setStaffCount(staffQueryService.countByPositionId(e.getId()));
			return vo;
		});
	}

}
