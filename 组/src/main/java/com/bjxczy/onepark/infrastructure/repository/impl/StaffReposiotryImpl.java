package com.bjxczy.onepark.infrastructure.repository.impl;

import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.nacos.common.utils.Objects;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.bjxczy.core.web.base.MyBatisPlusRepositoryImpl;
import com.bjxczy.onepark.car.common.utils.ExcelUtil;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.QueryStaffParams;
import com.bjxczy.onepark.common.model.organization.QueryTopStaffParams;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;
import com.bjxczy.onepark.domain.model.staff.entity.Position;
import com.bjxczy.onepark.domain.model.staff.entity.Rank;
import com.bjxczy.onepark.domain.model.staff.respository.PositionRepository;
import com.bjxczy.onepark.domain.model.staff.respository.RankRepository;
import com.bjxczy.onepark.domain.model.staff.respository.StaffRepository;
import com.bjxczy.onepark.domain.model.staff.valueobject.EmployeeNo;
import com.bjxczy.onepark.domain.model.staff.valueobject.IdentityCard;
import com.bjxczy.onepark.domain.model.staff.valueobject.Mobile;
import com.bjxczy.onepark.domain.valueobject.Gender;
import com.bjxczy.onepark.domain.valueobject.Uuid;
import com.bjxczy.onepark.infrastructure.assembler.StaffAssembler;
import com.bjxczy.onepark.infrastructure.mapper.StaffMapper;
import com.bjxczy.onepark.infrastructure.po.OrganizationPO;
import com.bjxczy.onepark.infrastructure.po.StaffPO;
import com.bjxczy.onepark.interfaces.dto.FindStaffDTO;
import com.bjxczy.onepark.interfaces.dto.QueryStaffDTO;
import com.bjxczy.onepark.interfaces.service.OrganizationQueryService;
import com.bjxczy.onepark.interfaces.service.StaffQueryService;
import com.bjxczy.onepark.interfaces.vo.DeptStaffCountVO;
import com.bjxczy.onepark.interfaces.vo.LazyTreeVO;
import com.bjxczy.onepark.interfaces.vo.OrgStaffVO;
import com.bjxczy.onepark.interfaces.vo.OrganizationTreeVO;
import com.bjxczy.onepark.interfaces.vo.StaffVO;

import cn.hutool.core.map.MapBuilder;

@Service
public class StaffReposiotryImpl extends MyBatisPlusRepositoryImpl<StaffMapper, StaffPO, Staff>
		implements StaffRepository, StaffQueryService {

	@Autowired
	private StaffAssembler staffAssemler;
	@Autowired
	private OrganizationQueryService organizationQueryService;
	@Autowired
	private RankRepository rankRepository;
	@Autowired
	private PositionRepository positionRepository;

	@Override
	public StaffPO toPO(Staff source) {
		return staffAssemler.toPO(source);
	}

	@Override
	public Staff toModel(StaffPO source) {
		return staffAssemler.toModel(source);
	}

	@Override
	public Optional<Staff> findByUuid(Uuid uuid) {
		StaffPO po = lambdaQuery().eq(StaffPO::getUuid, uuid.getValue()).one();
		return Optional.ofNullable(staffAssemler.toModel(po));
	}

	@Override
	public Optional<Staff> findByMobile(Mobile mobile) {
		StaffPO po = lambdaQuery().eq(StaffPO::getMobile, mobile.getValue()).one();
		return Optional.ofNullable(staffAssemler.toModel(po));
	}

	@Override
	public Optional<Staff> findByIdentityCard(IdentityCard identifyCard) {
		StaffPO po = lambdaQuery().eq(StaffPO::getIdentityCard, identifyCard.getValue()).one();
		return Optional.ofNullable(staffAssemler.toModel(po));
	}

	@Override
	public Optional<Staff> findByEmployeeNo(EmployeeNo employeeNo) {
		StaffPO po = lambdaQuery().eq(StaffPO::getEmployeeNo, employeeNo.getValue()).one();
		return Optional.ofNullable(staffAssemler.toModel(po));
	}

	@Override
	public IPage<StaffVO> listPageStaff(QueryStaffDTO dto) {
		QueryWrapper<StaffPO> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("del_flag",0);
		// 获取所有组织ID
		queryWrapper.and(StringUtils.isNotBlank(dto.getUnifyParams()), w -> {
			w.like("fld_name", dto.getUnifyParams()).or().like("mobile", dto.getUnifyParams()).or();
		});
		queryWrapper.in(null != dto.getOrganizationId(), "organization_id",
				organizationQueryService.getChildrenIdList(dto.getOrganizationId()));
		queryWrapper.eq(null != dto.getRankId(), "rank_id", dto.getRankId());
		queryWrapper.eq(null != dto.getDimission(), "dimission", dto.getDimission());
		// queryWrapper.in(null!=dto.getPositionId(),"position_ids",dto.getPositionId());
		Optional.ofNullable(dto.getPositionId()).ifPresent(positionId->{
			String sql=String.format("and find_in_set(%s,position_ids)",positionId);
			queryWrapper.last(sql);
		});
		IPage<StaffVO> data = getBaseMapper().listPage(dto.toPage(), queryWrapper, dto);
		data.getRecords().forEach(e -> {
			e.setDeptNames(organizationQueryService.getParentList(e.getOrganizationId()));
			e.setMisCode(e.getEmployeeNo());
		});
		return data;
	}

	@Override
	public Integer countByRankId(Integer id) {
		Integer cnt = lambdaQuery().eq(StaffPO::getRankId, id).count();
		return cnt;
	}

	@Override
	public Integer countByPositionId(Integer id) {
		// Integer cnt = lambdaQuery().in(StaffPO::getPositionIds, id).count();
		String sql=String.format("and find_in_set(%s,position_ids)",id);
		Integer cnt = lambdaQuery().last(null!=id,sql).count();
		return cnt;
	}

	@Override
	public DeptStaffCountVO countByDepartment(Integer organizationId) {
		Integer curCount = lambdaQuery().eq(StaffPO::getOrganizationId, organizationId).count(),
				sumCount = lambdaQuery()
						.in(StaffPO::getOrganizationId, organizationQueryService.getChildrenIdList(organizationId))
						.count();
		return new DeptStaffCountVO(curCount, sumCount - curCount, sumCount);
	}

	@Override
	public StaffInformation findInfoById(Integer id) {
		return toInfo(getById(id));
	}

	@Override
	public StaffInformation findInfoByMobile(String mobile) {
		return toInfo(lambdaQuery().eq(StaffPO::getMobile, mobile).one());
	}

	@Override
	public StaffInformation findInfoByUuid(String uuid) {
		return toInfo(lambdaQuery().eq(StaffPO::getUuid, uuid).one());
	}

	private StaffInformation toInfo(StaffPO po) {
		if (po == null) {
			return null;
		}
		StaffInformation info = new StaffInformation();
		BeanUtils.copyProperties(po, info);
		if (Objects.nonNull(po.getRankId())) {
			Rank rank = rankRepository.getById(po.getRankId());
			info.setRankName(rank.getFldName());
		}
		if (Objects.nonNull(po.getPositionIds())) {
			HashSet<String> positions = new HashSet<>();
			for (String positionId : po.getPositionIds().split(",")) {
				Position position = positionRepository.getById(Integer.valueOf(positionId));
				positions.add(position.getFldName());
			}
			info.setPositionName(String.join(",",positions));
		}
		info.setDeptNames(organizationQueryService.getParentList(po.getOrganizationId()));
		return info;
	}

	@Override
	public OrgStaffVO findStaff(FindStaffDTO dto) {
		List<StaffVO> staffList = new ArrayList<>();
		List<OrganizationTreeVO> trees = null;
		// 查询员工数据
		QueryWrapper<StaffPO> staffQuery = new QueryWrapper<>();
		staffQuery.and(StringUtils.isNotBlank(dto.getUnifyParams()), w -> {
			w.like("fld_name", dto.getUnifyParams()).or().like("mobile", dto.getUnifyParams()).or().like("employee_no",
					dto.getUnifyParams());
		});
		staffQuery.eq(dto.getOrgId() != null, "organization_id", dto.getOrgId());
		staffQuery.last("limit 0, 50");
		List<StaffPO> poList = list(staffQuery);
		staffList = poList.stream().map(e -> {
			StaffVO vo = new StaffVO();
			BeanUtils.copyProperties(e, vo);
			vo.setDeptNames(organizationQueryService.getParentList(e.getOrganizationId()));
			return vo;
		}).collect(Collectors.toList());
		// 查询组织数据
		if (dto.getQueryOrg()) {
			if (StringUtils.isBlank(dto.getUnifyParams())) {
				trees = organizationQueryService.listTree();
			} else {
				Set<Integer> organizationIds = staffList.stream().flatMap(e -> e.getDeptNames().stream())
						.map(e -> e.getId()).distinct().collect(Collectors.toSet());
				trees = organizationQueryService.listTreeByIds(organizationIds);
			}
		}
		return new OrgStaffVO(trees, staffList);
	}

	@Override
	public StaffVO findStaffById(Integer staffId) {
		StaffVO staffVO = getBaseMapper().findById(staffId);
		Integer organizationId = staffVO.getOrganizationId();
		List<DeptNameInfo> parentList = organizationQueryService.getParentList(organizationId);
		staffVO.setDeptNames(parentList);
		return staffVO;
	}

	@Override
	public StaffInfoFine StaffInfoFine(Integer staffId) {
		StaffInfoFine staffInfoFine = getBaseMapper().StaffInfoFine(staffId);
		Integer organizationId = staffInfoFine.getOrganizationId();
		List<DeptNameInfo> parentList = organizationQueryService.getParentArray(organizationId);
		staffInfoFine.setDeptNames(parentList);
		staffInfoFine.setGender(Gender.valueOf(staffInfoFine.getGender()).getValue().toString());
		return staffInfoFine;
	}

	@Override
	public List<StaffInformation> getStaffByPersonName(String name) {
		QueryWrapper<StaffPO> queryWrapper = new QueryWrapper<StaffPO>();
		queryWrapper.like("fld_name", name);
		queryWrapper.and(wrp -> wrp.eq("del_flag", 0).or().isNull("del_flag"));
		queryWrapper.orderByAsc("if(isnull(rank_id), 99999, cast(rank_id as char))");
		List<StaffPO> list = this.list(queryWrapper);
		List<StaffInformation> lists = new ArrayList<>();
		for (StaffPO staffPO : list) {
			lists.add(toInfo(staffPO));
		}
		return lists;
	}

	@Override
	public IPage<StaffVO> listResignListPage(QueryStaffDTO dto) {
		QueryWrapper<StaffPO> queryWrapper = new QueryWrapper<>();
		// 获取所有组织ID
		queryWrapper.and(StringUtils.isNotBlank(dto.getUnifyParams()), w -> {
			w.like("fld_name", dto.getUnifyParams()).or().like("mobile", dto.getUnifyParams()).or()
					.like("employee_no", dto.getUnifyParams()).or();
		});
		queryWrapper.in(null != dto.getOrganizationId(), "organization_id",
				organizationQueryService.getChildrenIdList(dto.getOrganizationId()));
		queryWrapper.eq(null != dto.getRankId(), "rank_id", dto.getRankId());
		queryWrapper.eq("dimission", 1);
		IPage<StaffVO> data = getBaseMapper().listPage(dto.toPage(), queryWrapper, dto);
		data.getRecords().forEach(e -> e.setDeptNames(organizationQueryService.getParentList(e.getOrganizationId())));
		return data;
	}

	@Override
	public void export(QueryStaffDTO dto, HttpServletResponse resp) {
		QueryWrapper<StaffPO> queryWrapper = new QueryWrapper<>();
		// 获取所有组织ID
		queryWrapper.and(StringUtils.isNotBlank(dto.getUnifyParams()), w -> {
			w.like("fld_name", dto.getUnifyParams()).or().like("mobile", dto.getUnifyParams()).or()
					.like("employee_no", dto.getUnifyParams()).or();
		});
		queryWrapper.in(null != dto.getOrganizationId(), "organization_id",
				organizationQueryService.getChildrenIdList(dto.getOrganizationId()));
		queryWrapper.eq(null != dto.getRankId(), "rank_id", dto.getRankId());
		queryWrapper.eq("dimission", 1);
		IPage<StaffVO> data = getBaseMapper().listPage(dto.toPage(), queryWrapper, dto);
		data.getRecords().forEach(e -> {
			e.setDeptNames(organizationQueryService.getParentList(e.getOrganizationId()));
			e.setDeptName(e.getDeptNames().stream().map(DeptNameInfo::getDeptName).collect(Collectors.joining("/")));
			e.setUserSourceName("手动录入");
		});
		try {
			ExcelUtil.exportXlsxByBean(resp, "离职人员列表", getResignExportHead(), data.getRecords());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<StaffInformation> getStaffs(Integer organizationId) {
		List<Integer> ids = organizationQueryService.getChildrenIdList(organizationId);
		LambdaQueryWrapper<StaffPO> staffLambdaQueryWrapper = new LambdaQueryWrapper<>();
		if (!ids.isEmpty()) {
			staffLambdaQueryWrapper.in(StaffPO::getOrganizationId, ids);
		}
		List<StaffPO> staffs = list(staffLambdaQueryWrapper);
		List<StaffInformation> lists = new ArrayList<>();
		for (StaffPO staffPO : staffs) {
			lists.add(toInfo(staffPO));
		}
		return lists;
	}

	private Map<String, String> getResignExportHead() {
		return MapBuilder.create(new LinkedHashMap<String, String>()).put("employeeNo", "员工编号").put("fidName", "姓名")
				.put("mobile", "手机号").put("deptName", "部门").put("rankName", "职级").put("email", "邮箱")
				.put("positionName", "岗位").put("userSourceName", "数据来源").put("createTime", "创建时间")
				.put("operator", "操作人").put("updateTime", "更新时间").build();
	}

	@Override
	public List<StaffInformation> getStaffBatch(List<Integer> ids) {
		List<StaffPO> list = lambdaQuery().in(StaffPO::getId, ids).list();
		return list.stream().map(e -> toInfo(e)).collect(Collectors.toList());
	}

	@Override
	public List<StaffInformation> listStaffByParams(QueryStaffParams params) {
		LambdaQueryWrapper<StaffPO> wrapper = new LambdaQueryWrapper<>();
		//主要是为了构建where，防止后面的sql拼接没有where
		wrapper.eq(StaffPO::getDelFlag,0);
		wrapper.in(params.getOrganizationIds() != null && params.getOrganizationIds().size() > 0,
						StaffPO::getOrganizationId, params.getOrganizationIds())
				.in(params.getRankIds() != null && params.getRankIds().size() > 0, StaffPO::getRankId,
						params.getRankIds());
		if (params.getPositionIds() != null && params.getPositionIds().size() > 0){
			HashSet<String> set = new HashSet<>();
			for (Integer positionId : params.getPositionIds()) {
				set.add(String.format(" find_in_set(%s,position_ids) ",positionId));
			}
			String sql = String.join("or", set);
			wrapper.last("and("+sql+")");
		}
		List<StaffPO> list = this.baseMapper.selectList(wrapper);
		return list.stream().map(e -> toInfo(e)).collect(Collectors.toList());
	}

	@Override
	public List<StaffInformation> listTopDeptStaff(Integer staffId, QueryTopStaffParams params) {
		StaffPO staff = getById(staffId);
		if (staff != null) {
			List<Integer> orgIds = organizationQueryService.getParentIds(staff.getOrganizationId());
			if (null != orgIds && orgIds.size() > 0) {
				return listStaffByParams(new QueryStaffParams(orgIds, params.getRankIds(), params.getPositionIds()));
			}
		}
		return new ArrayList<>();
	}

	@Override
	public List<LazyTreeVO> lazyQueryStaff(Integer organizationId) {
		organizationId = organizationId == null ? -1 : organizationId;
		List<OrganizationPO> orgList = organizationQueryService.listByParentId(organizationId);
		List<StaffPO> staffList = lambdaQuery().eq(StaffPO::getOrganizationId, organizationId).list();
		List<LazyTreeVO> data = new ArrayList<>();
		data.addAll(orgList.stream().map(e -> new LazyTreeVO(e.getId(), e.getFldName(), "organization"))
				.collect(Collectors.toList()));
		data.addAll(staffList.stream().map(e -> new LazyTreeVO(e.getId(), e.getFldName(), "staff"))
				.collect(Collectors.toList()));
		return data;
	}
}
