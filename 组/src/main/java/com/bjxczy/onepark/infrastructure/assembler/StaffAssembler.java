package com.bjxczy.onepark.infrastructure.assembler;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;
import com.bjxczy.onepark.infrastructure.po.StaffPO;

@Mapper(componentModel = "spring")
public interface StaffAssembler {
	
	@Mapping(source = "mobile.value", target = "mobile")
	@Mapping(source = "uuid.value", target = "uuid")
	@Mapping(source = "email.value", target = "email")
	@Mapping(source = "employeeNo.value", target = "employeeNo")
	@Mapping(source = "identityCard.value", target = "identityCard")
	@Mapping(source = "photo.value", target = "photo")
	@Mapping(target = "hasAccount", expression = "java(staff.getHasAccount() ? 1 : 0)")
	StaffPO toPO(Staff staff);
	
	@Mapping(target = "mobile.value", source = "mobile")
	@Mapping(target = "uuid.value", source = "uuid")
	@Mapping(target = "email.value", source = "email")
	@Mapping(target = "employeeNo.value", source = "employeeNo")
	@Mapping(target = "identityCard.value", source = "identityCard")
	@Mapping(target = "photo.value", source = "photo")
	@Mapping(target = "hasAccount", expression = "java(po.getHasAccount() == 1)")
	Staff toModel(StaffPO po);

}
