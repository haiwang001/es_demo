package com.bjxczy.onepark.infrastructure.po;

import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.bjxczy.core.web.base.UserOperator;
import com.bjxczy.onepark.domain.valueobject.Gender;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "tbl_staff", autoResultMap = true)
public class StaffPO extends UserOperator {

	private static final long serialVersionUID = 2023622219128139102L;

	@TableId(type = IdType.AUTO)
	private Integer id;

	private String fldName;

	private String mobile;

	private String uuid;

	private Gender gender;

	private String email;

	private String employeeNo;

	private String identityCard;

	@TableField(typeHandler = FastjsonTypeHandler.class)
	private List<String> photo;

	private Integer hasAccount;

	private Integer organizationId;

	private String positionIds;

	private Integer rankId;

	private String remark;

	private Integer dimission;

	private Integer comeFrom;

}
