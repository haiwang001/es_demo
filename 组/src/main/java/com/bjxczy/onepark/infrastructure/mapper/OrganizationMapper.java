package com.bjxczy.onepark.infrastructure.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.infrastructure.po.OrganizationPO;

@Mapper
public interface OrganizationMapper extends BaseMapper<OrganizationPO> {
    OrganizationPO selectByorganizationId(Integer organizationId);
}
