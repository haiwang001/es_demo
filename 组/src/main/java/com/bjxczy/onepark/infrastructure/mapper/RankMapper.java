package com.bjxczy.onepark.infrastructure.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.domain.model.staff.entity.Rank;

@Mapper
public interface RankMapper extends BaseMapper<Rank> {

}
