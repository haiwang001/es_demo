package com.bjxczy.onepark.interfaces.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：yubaowang
 * @date ：2023/2/15 13:46
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationTwoTreeVO {
    private Integer id;
    private Integer parentId;
    private String deptName;
}
