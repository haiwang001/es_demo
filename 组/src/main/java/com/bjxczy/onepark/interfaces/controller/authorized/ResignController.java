package com.bjxczy.onepark.interfaces.controller.authorized;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import com.bjxczy.core.web.privacy.MaskingOperation;
import com.bjxczy.core.web.privacy.ResponseMasking;
import com.bjxczy.core.web.privacy.handler.EmployeeNoMaskingHandler;
import com.bjxczy.core.web.privacy.handler.IdCardMaskingHandler;
import com.bjxczy.core.web.privacy.handler.MaskingField;
import com.bjxczy.core.web.privacy.handler.MobileMaskingHandler;
import com.bjxczy.core.web.privacy.iterator.IPageMaskingIterator;
import com.bjxczy.onepark.application.command.BatchRemoveCommand;
import com.bjxczy.onepark.application.command.RemoveStaffCommand;
import com.bjxczy.onepark.application.service.StaffApplicationService;
import com.bjxczy.onepark.interfaces.dto.QueryStaffDTO;
import com.bjxczy.onepark.interfaces.service.StaffQueryService;
import com.bjxczy.onepark.interfaces.vo.StaffVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@ApiResourceController
@RequestMapping("/resign")
public class ResignController {
    @Autowired
    private StaffApplicationService staffApplicationService;

    @Autowired
    private StaffQueryService staffQueryService;

    @GetMapping(value = "/page", name = "分页查询离职人员")
    @ResponseMasking(configId = "baseEngine.staffManage",operation = MaskingOperation.QUERY,
            fields = {@MaskingField(name = "employeeNo",typeHandler = EmployeeNoMaskingHandler.class),
                    @MaskingField(name = "mobile",typeHandler = MobileMaskingHandler.class),
                    @MaskingField(name = "identityCard",typeHandler = IdCardMaskingHandler.class)},
            iterator = IPageMaskingIterator.class)
    public IPage<StaffVO> listPageStaff(QueryStaffDTO dto) {
        return staffQueryService.listResignListPage(dto);
    }


    @SysLog(operationType = OperationType.DELETE,operation = "删除离职人员：${ctx.fldName}")
    @DeleteMapping(value = "/del/{id}", name = "删除离职人员")
    public void del(@PathVariable("id") Integer id) {
        staffApplicationService.remove(new RemoveStaffCommand(id));
    }

    @SysLog(operationType = OperationType.DELETE,operation = "批量删除离职人员：${ctx.fldNames}")
    @DeleteMapping(value = "/batchDel", name = "批量删除离职人员")
    public void batchRemoveStaff(@RequestBody List<Integer> idList) {
        staffApplicationService.batchRemove(new BatchRemoveCommand(idList));
    }
    @GetMapping(value = "/export", name = "导出离职人员")
    public void export(QueryStaffDTO dto, HttpServletResponse resp) {
         staffQueryService.export(dto,resp);
    }
}
