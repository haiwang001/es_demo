package com.bjxczy.onepark.interfaces.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.common.model.organization.QueryStaffParams;
import com.bjxczy.onepark.common.model.organization.QueryTopStaffParams;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.interfaces.dto.FindStaffDTO;
import com.bjxczy.onepark.interfaces.dto.QueryStaffDTO;
import com.bjxczy.onepark.interfaces.vo.DeptStaffCountVO;
import com.bjxczy.onepark.interfaces.vo.LazyTreeVO;
import com.bjxczy.onepark.interfaces.vo.OrgStaffVO;
import com.bjxczy.onepark.interfaces.vo.StaffVO;

public interface StaffQueryService {

    IPage<StaffVO> listPageStaff(QueryStaffDTO dto);

    Integer countByRankId(Integer id);

    Integer countByPositionId(Integer id);

    DeptStaffCountVO countByDepartment(Integer organizationId);

    StaffInformation findInfoById(Integer id);

    StaffInformation findInfoByMobile(String mobile);

    StaffInformation findInfoByUuid(String uuid);

    OrgStaffVO findStaff(FindStaffDTO dto);

    StaffVO findStaffById(Integer staffId);

    /**
     * 远程获取查询
     *
     * @param staffId id
     * @return 返回信息
     */
    StaffInfoFine StaffInfoFine(Integer staffId);

    List<StaffInformation> getStaffByPersonName(String name);

    IPage<StaffVO> listResignListPage(QueryStaffDTO dto);

    void export(QueryStaffDTO dto, HttpServletResponse resp);

    List<StaffInformation> getStaffs(Integer organizationId);

	List<StaffInformation> getStaffBatch(List<Integer> ids);

	List<StaffInformation> listStaffByParams(QueryStaffParams params);

	List<StaffInformation> listTopDeptStaff(Integer staffId, QueryTopStaffParams params);

	/**
	 * 懒加载查询组织架构员工
	 * @param organizationId
	 * @return
	 */
	List<LazyTreeVO> lazyQueryStaff(Integer organizationId);
}
