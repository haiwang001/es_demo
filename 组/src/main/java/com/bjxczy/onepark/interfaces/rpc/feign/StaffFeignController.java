package com.bjxczy.onepark.interfaces.rpc.feign;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bjxczy.core.feign.client.organization.BaseStaffFeignClient;
import com.bjxczy.onepark.common.model.organization.QueryStaffParams;
import com.bjxczy.onepark.common.model.organization.QueryTopStaffParams;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.interfaces.service.OrganizationQueryService;
import com.bjxczy.onepark.interfaces.service.StaffQueryService;

@Controller
@ResponseBody
public class StaffFeignController implements BaseStaffFeignClient {

	@Autowired
	private StaffQueryService staffQueryService;
	@Autowired
	private OrganizationQueryService organizationQueryService;

	@Override
	public StaffInformation findById(@PathVariable("id") Integer id) {
		return staffQueryService.findInfoById(id);
	}

	@Override
	public StaffInfoFine getStaffById(@PathVariable("id") Integer integer) {
		return staffQueryService.StaffInfoFine(integer);
	}

	@Override
	public StaffInformation findByMobile(@PathVariable("mobile") String mobile) {
		return staffQueryService.findInfoByMobile(mobile);
	}

	@Override
	public StaffInformation findByUuid(@PathVariable("uuid") String uuid) {
		return staffQueryService.findInfoByUuid(uuid);
	}

	@Override
	public List<StaffInformation> getStaffByPersonName(@PathVariable("name") String name) {
		return staffQueryService.getStaffByPersonName(name);
	}

	@Override
	public List<Integer> listOrgChildrenId(@RequestParam("organizationId") Integer organizationId) {
		return organizationQueryService.getChildrenIdList(organizationId);
	}

	@Override
	public List<StaffInformation> getStaffs(Integer integer) {
		return staffQueryService.getStaffs(integer);
	}

	@Override
	public List<StaffInformation> getStaffBatch(@RequestBody List<Integer> ids) {
		return staffQueryService.getStaffBatch(ids);
	}

	@Override
	public List<StaffInformation> listStaffByParams(@RequestBody QueryStaffParams params) {
		return staffQueryService.listStaffByParams(params);
	}

	@Override
	public List<StaffInformation> listTopDeptStaff(@RequestParam("staffId") Integer staffId,
			@RequestBody QueryTopStaffParams params) {
		return staffQueryService.listTopDeptStaff(staffId, params);
	}

}
