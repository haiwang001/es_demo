package com.bjxczy.onepark.interfaces.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class SavePositionDTO {
	
	private Integer id;
	
	@NotBlank(message = "名称不能为空！")
	private String fldName;

}
