package com.bjxczy.onepark.interfaces.controller.authorized;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.util.ReflectUtil;
import com.bjxczy.core.web.log.LogParam;
import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import com.bjxczy.core.web.privacy.MaskingOperation;
import com.bjxczy.core.web.privacy.ResponseMasking;
import com.bjxczy.core.web.privacy.handler.EmployeeNoMaskingHandler;
import com.bjxczy.core.web.privacy.handler.IdCardMaskingHandler;
import com.bjxczy.core.web.privacy.handler.MaskingField;
import com.bjxczy.core.web.privacy.handler.MobileMaskingHandler;
import com.bjxczy.core.web.privacy.iterator.IPageMaskingIterator;
import com.bjxczy.core.web.privacy.iterator.PageWrapperMaskingIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.application.command.BatchRemoveCommand;
import com.bjxczy.onepark.application.command.CreateStaffCommand;
import com.bjxczy.onepark.application.command.RemoveStaffCommand;
import com.bjxczy.onepark.application.command.UpdateStaffCommand;
import com.bjxczy.onepark.application.service.StaffApplicationService;
import com.bjxczy.onepark.interfaces.dto.FindStaffDTO;
import com.bjxczy.onepark.interfaces.dto.QueryStaffDTO;
import com.bjxczy.onepark.interfaces.service.StaffQueryService;
import com.bjxczy.onepark.interfaces.vo.DeptStaffCountVO;
import com.bjxczy.onepark.interfaces.vo.OrgStaffVO;
import com.bjxczy.onepark.interfaces.vo.StaffVO;

@ApiResourceController
@RequestMapping("/staff")
public class StaffController {

	@Autowired
	private StaffApplicationService staffApplicationService;
	@Autowired
	private StaffQueryService staffQueryService;

	@SysLog(operationType = OperationType.INSERT,operation = "创建员工：${command.fldName}")
	@PostMapping(name = "创建员工")
	public void createUser(@RequestBody @LogParam CreateStaffCommand command) {
		staffApplicationService.createStaff(command);
	}

	@SysLog(operationType = OperationType.UPDATE,operation = "编辑员工：${ctx.fldName}")
	@PutMapping(value = "/{id}", name = "编辑")
	public void updateUser(@PathVariable("id") Integer id, @RequestBody UpdateStaffCommand command) {
		command.setId(id);
		staffApplicationService.updateStaff(command);
	}

	@SysLog(operationType = OperationType.DELETE,operation = "删除员工：${ctx.fldName}")
	@DeleteMapping(value = "/{id}", name = "删除")
	public void removeStaff(@PathVariable("id") Integer id) {
		staffApplicationService.remove(new RemoveStaffCommand(id));
	}

	@SysLog(operationType = OperationType.DELETE,operation = "删除员工：${ctx.fldNames}")
	@DeleteMapping(value = "/batch", name = "批量删除")
	public void batchRemoveStaff(@RequestBody List<Integer> idList) {
		staffApplicationService.batchRemove(new BatchRemoveCommand(idList));
	}

	@GetMapping(value = "/staffPage", name = "分页查询")
	@ResponseMasking(configId = "baseEngine.staffManage",operation = MaskingOperation.QUERY,
			fields = {@MaskingField(name = "employeeNo",typeHandler = EmployeeNoMaskingHandler.class),
					@MaskingField(name = "mobile",typeHandler = MobileMaskingHandler.class),
					@MaskingField(name = "identityCard",typeHandler = IdCardMaskingHandler.class)},
			iterator = IPageMaskingIterator.class)
	public IPage<StaffVO> listPageStaff(QueryStaffDTO dto) {
		return staffQueryService.listPageStaff(dto);
	}

	@GetMapping(value = "/countByDepartment", name = "部门人数统计")
	public DeptStaffCountVO countByDepartment(Integer organizationId) {
		return staffQueryService.countByDepartment(organizationId);
	}

	@GetMapping(value = "/findStaff", name = "查找员工")
	public OrgStaffVO findStaff(FindStaffDTO dto) {
		return staffQueryService.findStaff(dto);
	}

	@GetMapping(value = "/getStaffById", name = "获取员工详情")
	public StaffVO getStaffById(@RequestParam("id") Integer id) {
		return staffQueryService.findStaffById(id);
	}

}
