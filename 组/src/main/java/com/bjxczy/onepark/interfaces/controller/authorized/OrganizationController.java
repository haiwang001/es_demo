
package com.bjxczy.onepark.interfaces.controller.authorized;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.application.command.ChangeOrgNameCommand;
import com.bjxczy.onepark.application.command.CreateOrganizationCommand;
import com.bjxczy.onepark.application.command.RemoveOrganizationCommand;
import com.bjxczy.onepark.application.service.OrganizationService;
import com.bjxczy.onepark.interfaces.service.OrganizationQueryService;
import com.bjxczy.onepark.interfaces.vo.OrganizationTreeVO;
import com.bjxczy.onepark.interfaces.vo.OrganizationTwoTreeVO;

@ApiResourceController
@RequestMapping("/organization")
public class OrganizationController {

	@Autowired
	private OrganizationService organizationService;
	@Autowired
	private OrganizationQueryService organizationQueryService;

	@PostMapping(name = "新建部门")
	public void addOrganizaton(@RequestBody CreateOrganizationCommand command) {
		organizationService.createOrganzation(command);
	}

	@PutMapping(value = "/{id}", name = "更新部门")
	public void updateOranization(@RequestBody ChangeOrgNameCommand command,@PathVariable("id") Integer id) {
		command.setId(String.valueOf(id));
		organizationService.changeOrgName(command);
	}

	@DeleteMapping(value = "/{id}", name = "删除部门")
	public void removeOrganization(@PathVariable("id") Integer id) {
		organizationService.removeOrganization(new RemoveOrganizationCommand(id));
	}

	@GetMapping(value = "/treesVague", name = "查询部门")
	public List<OrganizationTreeVO> listTree() {
		return organizationQueryService.listTree();
	}
	@GetMapping(value = "/treeStaffs", name = "获取包含人员结构的部门树")
	public List<OrganizationTreeVO> treesVague() {
		return organizationQueryService.treeStaffs();
	}
	@GetMapping(value = "/deptTrees/{id}",name = "查询上级部门")
	public List<OrganizationTreeVO>deptTrees(@PathVariable("id") Integer id){
		return organizationQueryService.deptTrees(id);
	}
	
	@PutMapping(value = "/changeParent/{id}",name = "更换上级部门")
	public void changeParent(@PathVariable("id") Integer id, @RequestParam(value = "parentId")Integer parentId){
		organizationQueryService.changeParent(id,parentId);
	}

	@GetMapping(value = "/treesTwoVague", name = "查询二级部门")
	public List<OrganizationTwoTreeVO> listTwoTree() {
		return organizationQueryService.listTwoTree();
	}

}
