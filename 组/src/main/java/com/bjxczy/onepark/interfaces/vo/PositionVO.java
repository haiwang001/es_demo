package com.bjxczy.onepark.interfaces.vo;

import com.bjxczy.onepark.domain.model.staff.entity.Position;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class PositionVO extends Position {
	
	private static final long serialVersionUID = 3578492012588862249L;
	
	private Integer staffCount;

}
