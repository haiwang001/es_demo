package com.bjxczy.onepark.interfaces.vo;

import com.bjxczy.onepark.domain.model.staff.entity.Rank;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class RankVO extends Rank {
	
	private static final long serialVersionUID = -7761196146834468273L;
	
	private Integer staffCount;

}
