package com.bjxczy.onepark.interfaces.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeptStaffCountVO {
	
	private Integer curCount;
	
	private Integer subCount;
	
	private Integer sumCount;

}
