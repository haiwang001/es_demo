package com.bjxczy.onepark.interfaces.service;

import java.util.List;
import java.util.Set;

import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.OrganizationTree;
import com.bjxczy.onepark.infrastructure.po.OrganizationPO;
import com.bjxczy.onepark.interfaces.vo.OrganizationTreeVO;
import com.bjxczy.onepark.interfaces.vo.OrganizationTwoTreeVO;

public interface OrganizationQueryService {

	List<OrganizationTreeVO> listTree();

	List<DeptNameInfo> getParentList(Integer organizationId);

	List<Integer> getChildrenIdList(Integer organizationId);

	List<OrganizationTreeVO> listTreeByIds(Set<Integer> organizationIds);

	List<DeptNameInfo> getParentArray(Integer organizationId);

    List<OrganizationTreeVO> deptTrees(Integer id);

	void changeParent(Integer id, Integer parentId);

	List<OrganizationTree> findTree();

    List<OrganizationTwoTreeVO> listTwoTree();

	List<OrganizationTreeVO> treeStaffs();

	List<Integer> getParentIds(Integer organizationId);

	List<OrganizationPO> listByParentId(Integer organizationId);
}
