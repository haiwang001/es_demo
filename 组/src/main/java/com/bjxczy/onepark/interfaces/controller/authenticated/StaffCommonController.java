package com.bjxczy.onepark.interfaces.controller.authenticated;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.interfaces.service.StaffQueryService;
import com.bjxczy.onepark.interfaces.vo.LazyTreeVO;
import com.bjxczy.onepark.interfaces.vo.StaffVO;

@ApiResourceController
public class StaffCommonController {

	@Autowired
	private StaffQueryService staffQueryService;

	@GetMapping("/staff/loginStaff")
	public StaffVO ofLoginStaff(@LoginUser(isFull = true) UserInformation user) {
		return staffQueryService.findStaffById(user.getStaffId());
	}

	@GetMapping("/staff/lazyTree")
	public List<LazyTreeVO> lazyQueryStaff(
			@RequestParam(value = "organizationId", required = false) Integer organizationId) {
		return staffQueryService.lazyQueryStaff(organizationId);
	}

}
