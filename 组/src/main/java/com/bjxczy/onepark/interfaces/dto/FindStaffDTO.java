package com.bjxczy.onepark.interfaces.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FindStaffDTO {

	private String unifyParams;

	private Long orgId;

	private Boolean queryOrg;

}
