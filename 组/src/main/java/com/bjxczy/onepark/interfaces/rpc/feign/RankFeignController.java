package com.bjxczy.onepark.interfaces.rpc.feign;

import com.bjxczy.core.feign.client.organization.BaseRankFeignClient;
import com.bjxczy.onepark.common.model.organization.OrganizationTree;
import com.bjxczy.onepark.common.model.organization.RankInfo;
import com.bjxczy.onepark.interfaces.service.OrganizationQueryService;
import com.bjxczy.onepark.interfaces.service.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
public class RankFeignController implements BaseRankFeignClient {

    @Autowired
    private RankService rankService;
    @Autowired
    private OrganizationQueryService organizationQueryService;

    @Override
    public List<RankInfo> getRankLists() {
        return rankService.getRankLists();
    }

    @Override
    public List<OrganizationTree> findOrgTree() {
        return organizationQueryService.findTree();
    }
}
