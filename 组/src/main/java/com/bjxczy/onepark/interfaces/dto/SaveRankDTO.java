package com.bjxczy.onepark.interfaces.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class SaveRankDTO {
	
	private Integer id;
	
	@NotBlank(message = "名称不能为空！")
	private String fldName;
	
	@NotNull(message = "职级大小不能为空！")
	private Integer fldLevel;

}
