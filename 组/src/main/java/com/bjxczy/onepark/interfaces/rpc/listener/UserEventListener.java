package com.bjxczy.onepark.interfaces.rpc.listener;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.core.rabbitmq.supports.UserBaseListener;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.infrastructure.mapper.StaffMapper;
import com.bjxczy.onepark.infrastructure.po.StaffPO;
import com.rabbitmq.client.Channel;

/**
 * @ClassName UserEventListener
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/1 14:24
 * @Version 1.0
 */
@Component
public class UserEventListener extends UserBaseListener {

    @Autowired
    private StaffMapper staffMapper;

    @RabbitListener(bindings = { @QueueBinding(value =
    @Queue, // 临时队列
            key = CREATE_USER_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) // 绑定的交换机
    )})
    @Override
    public void createUserEventHandler(UserInformation information, Channel channel, Message message) {
        //将tbl_staff的has_account字段为1
        Integer staffId = information.getStaffId();
        StaffPO staffPO = new StaffPO();
        int update = staffMapper.update(staffPO, Wrappers.<StaffPO>lambdaUpdate().eq(StaffPO::getId, staffId).set(StaffPO::getHasAccount, 1));
        if (update>0){
            this.confirm(()->true,channel,message);
        }
    }

    @RabbitListener(bindings = { @QueueBinding(value =
    @Queue, // 临时队列
            key = REMOVE_USER_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) // 绑定的交换机
    )})
    @Override
    public void removeUserEventHandler(UserInformation information, Channel channel, Message message) {
        //将tbl_staff的has_account字段为0
        Integer staffId = information.getStaffId();
        StaffPO staffPO = new StaffPO();
        int update = staffMapper.update(staffPO, Wrappers.<StaffPO>lambdaUpdate().eq(StaffPO::getId, staffId).set(StaffPO::getHasAccount, 0));
        if (update>0){
            this.confirm(()->true,channel,message);
        }
    }
}
