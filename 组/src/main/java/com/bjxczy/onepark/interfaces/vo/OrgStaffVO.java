package com.bjxczy.onepark.interfaces.vo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrgStaffVO {
	
	private List<OrganizationTreeVO> organizationTree;
	
	private List<StaffVO> staffList;

}
