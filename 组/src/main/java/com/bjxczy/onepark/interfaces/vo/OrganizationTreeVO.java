package com.bjxczy.onepark.interfaces.vo;

import java.io.Serializable;
import java.util.List;

import com.bjxczy.onepark.infrastructure.po.OrganizationPO;

import com.bjxczy.onepark.infrastructure.po.StaffPO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class OrganizationTreeVO extends OrganizationPO implements Serializable {
	
	private static final long serialVersionUID = -5305906349277519206L;
	
	private String fldCode;
	
	private List<OrganizationTreeVO> children;
	
	private List<OrganizationPO> path;
	private List<StaffPO> staffs;


}
