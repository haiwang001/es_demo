package com.bjxczy.onepark.interfaces.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.common.model.organization.RankInfo;
import com.bjxczy.onepark.interfaces.dto.QueryRankDTO;
import com.bjxczy.onepark.interfaces.dto.SaveRankDTO;
import com.bjxczy.onepark.interfaces.vo.RankVO;

public interface RankService {

	void saveRank(SaveRankDTO dto);

	IPage<RankVO> listPageRank(QueryRankDTO dto);

	List<RankInfo> getRankLists();

}
