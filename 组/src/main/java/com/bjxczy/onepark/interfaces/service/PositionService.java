package com.bjxczy.onepark.interfaces.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.interfaces.dto.QueryPositionDTO;
import com.bjxczy.onepark.interfaces.dto.SavePositionDTO;
import com.bjxczy.onepark.interfaces.vo.PositionVO;

public interface PositionService {

	void savePosition(SavePositionDTO dto);

	void delPosition(Integer id);

	IPage<PositionVO> listPagePosition(QueryPositionDTO dto);

}
