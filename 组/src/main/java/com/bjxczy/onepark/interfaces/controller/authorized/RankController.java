package com.bjxczy.onepark.interfaces.controller.authorized;

import javax.validation.Valid;

import com.bjxczy.core.web.log.LogContextHolder;
import com.bjxczy.core.web.log.LogParam;
import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.base.AbstractRestController;
import com.bjxczy.onepark.domain.model.staff.entity.Rank;
import com.bjxczy.onepark.domain.model.staff.respository.RankRepository;
import com.bjxczy.onepark.interfaces.dto.QueryRankDTO;
import com.bjxczy.onepark.interfaces.dto.SaveRankDTO;
import com.bjxczy.onepark.interfaces.service.RankService;
import com.bjxczy.onepark.interfaces.vo.RankVO;

import java.util.Optional;

@ApiResourceController
@RequestMapping(value = "/rank", name = "职级")
@Validated
public class RankController extends AbstractRestController<Rank, Integer> {

	@Autowired
	private RankService rankService;
	@Autowired
	private RankRepository rankRepository;

	@Override
	public IService<Rank> getService() {
		return this.rankRepository;
	}

	@SysLog(operationType = OperationType.INSERT,operation = "新增职级：${dto.fldName}")
	@PostMapping(name = "新增{0}")
	public void addRank(@RequestBody @Valid @LogParam SaveRankDTO dto) {
		rankService.saveRank(dto);
	}

	@SysLog(operationType = OperationType.DELETE,operation = "删除职级：${ctx.fldName}")
	@Override
	@DeleteMapping(value = "/{id}", name = "删除{0}")
	public void removeById(@PathVariable("id") Integer id) {
		Optional.ofNullable(rankRepository.getById(id)).ifPresent(rank -> {
			LogContextHolder.put("fldName",rank.getFldName());
		});
		super.removeById(id);
	}

	@SysLog(operationType = OperationType.UPDATE,operation = "编辑职级：${dto.fldName}")
	@PutMapping(value = "/{id}", name = "编辑{0}")
	public void editRank(@PathVariable("id") Integer id, @RequestBody @Valid @LogParam SaveRankDTO dto) {
		dto.setId(id);
		rankService.saveRank(dto);
	}

	@GetMapping(value = "/page", name = "分页查询{0}")
	public IPage<RankVO> listPageRank(QueryRankDTO dto) {
		return rankService.listPageRank(dto);
	}
}
