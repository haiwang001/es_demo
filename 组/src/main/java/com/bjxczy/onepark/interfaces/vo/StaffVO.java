package com.bjxczy.onepark.interfaces.vo;

import java.util.List;

import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.infrastructure.po.StaffPO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class StaffVO extends StaffPO {

	private static final long serialVersionUID = 3695563047918075672L;

	private String positionName;

	private String rankName;

	private List<DeptNameInfo> deptNames;

	private String userSourceName;
	private String deptName;

	private String misCode;
}

