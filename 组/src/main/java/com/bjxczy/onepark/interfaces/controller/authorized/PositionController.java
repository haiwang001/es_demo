package com.bjxczy.onepark.interfaces.controller.authorized;

import javax.validation.Valid;

import com.bjxczy.core.web.log.LogContextHolder;
import com.bjxczy.core.web.log.LogParam;
import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.base.AbstractRestController;
import com.bjxczy.onepark.domain.model.staff.entity.Position;
import com.bjxczy.onepark.domain.model.staff.respository.PositionRepository;
import com.bjxczy.onepark.interfaces.dto.QueryPositionDTO;
import com.bjxczy.onepark.interfaces.dto.SavePositionDTO;
import com.bjxczy.onepark.interfaces.service.PositionService;
import com.bjxczy.onepark.interfaces.vo.PositionVO;

import java.util.Optional;

@ApiResourceController
@RequestMapping(value = "/position", name = "岗位")
@Validated
public class PositionController extends AbstractRestController<Position, Integer> {

	@Autowired
	private PositionService positionService;
	@Autowired
	private PositionRepository positionRepository;

	@Override
	public IService<Position> getService() {
		return positionRepository;
	}

	@SysLog(operationType = OperationType.DELETE,operation = "删除岗位：${ctx.fldName}")
	@DeleteMapping(value = "/{id}", name = "删除{0}")
	@Override
	public void removeById(@PathVariable("id")Integer id) {
		Optional.ofNullable(positionRepository.getById(id)).ifPresent(position -> {
			LogContextHolder.put("fldName",position.getFldName());
		});
		super.removeById(id);
	}

	@SysLog(operationType = OperationType.INSERT,operation = "新增岗位：${dto.fldName}")
	@PostMapping(name = "新增{0}")
	public void addPosition(@RequestBody @Valid @LogParam SavePositionDTO dto) {
		positionService.savePosition(dto);
	}

	@SysLog(operationType = OperationType.UPDATE,operation = "编辑岗位：${dto.fldName}")
	@PutMapping(value = "/{id}", name = "编辑{0}")
	public void updatePosition(@PathVariable("id") Integer id, @RequestBody @Valid @LogParam SavePositionDTO dto) {
		dto.setId(id);
		positionService.savePosition(dto);
	}

	@GetMapping(value = "/page", name = "分页查询{0}")
	public IPage<PositionVO> listPagePosition(QueryPositionDTO dto) {
		return positionService.listPagePosition(dto);
	}

}
