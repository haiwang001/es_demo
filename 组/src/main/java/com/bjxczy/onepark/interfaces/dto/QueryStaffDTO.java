package com.bjxczy.onepark.interfaces.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryStaffDTO extends BasePageDTO {

	private String unifyParams;

	private Integer organizationId;

	private Integer positionId;

	private Integer rankId;

	private Integer dimission;

}
