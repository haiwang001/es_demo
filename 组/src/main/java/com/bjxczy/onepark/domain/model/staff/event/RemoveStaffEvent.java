package com.bjxczy.onepark.domain.model.staff.event;

import org.springframework.context.ApplicationEvent;

import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class RemoveStaffEvent extends ApplicationEvent {

	private static final long serialVersionUID = -3051115738705046000L;
	
	private Integer id;
	
	private String uuid;
	
	private String mobile;
	
	public RemoveStaffEvent(Object source) {
		super(source);
		Staff staff = (Staff) source;
		this.id = staff.getId();
		this.uuid = staff.getUuid().getValue();
		this.mobile = staff.getMobile().getValue();
	}

}
