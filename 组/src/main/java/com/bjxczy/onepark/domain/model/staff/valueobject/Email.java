package com.bjxczy.onepark.domain.model.staff.valueobject;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.bjxczy.core.web.base.ValueObject;

import cn.hutool.core.lang.Validator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Email extends ValueObject<Email> {

	private static final long serialVersionUID = -5380502502148279349L;
	
	private String value;

	public Email(String value) {
		if (StringUtils.isNotBlank(value)) {
			if (!Validator.isEmail(value)) {
				throw new IllegalArgumentException("邮箱格式不正确！");
			}
		}
		this.value = value;
	}

	@Override
	protected boolean equalsTo(Email other) {
		return getValue().equals(other.getValue());
	}

}
