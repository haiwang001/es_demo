package com.bjxczy.onepark.domain.model.staff.valueobject;

import javax.validation.constraints.NotBlank;

import com.bjxczy.core.web.base.ValueObject;
import com.bjxczy.onepark.common.exception.ResultException;

import cn.hutool.core.lang.Validator;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Mobile extends ValueObject<Mobile> {
	
	private static final long serialVersionUID = 2263298335601286713L;
	
	@NotBlank(message = "手机号不能为空！")
	private String value;
	
	public Mobile(String value) {
		if (!Validator.isMobile(value)) {
			throw new ResultException("手机号格式错误！");
		}
		this.value = value;
	}
	
	@Override
	protected boolean equalsTo(Mobile other) {
		return getValue().equals(other.getValue());
	}

}
