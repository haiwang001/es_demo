package com.bjxczy.onepark.domain.model.organization.repository;

import java.util.List;

import com.bjxczy.core.web.base.AbstractRepository;
import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import com.bjxczy.onepark.domain.model.organization.valueobject.ParentId;

public interface OrganizationRepository extends AbstractRepository<Organization> {

	List<Organization> findByParentId(ParentId parentId);

}
