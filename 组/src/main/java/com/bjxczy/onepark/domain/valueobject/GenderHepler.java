package com.bjxczy.onepark.domain.valueobject;

public class GenderHepler {
	
	public static Gender ofValue(Integer value) {
		for (Gender item :Gender.values()) {
			while(item.getValue() == value) {
				return item;
			}
		}
		return null;
	}

}
