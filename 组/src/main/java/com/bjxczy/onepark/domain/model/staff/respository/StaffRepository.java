package com.bjxczy.onepark.domain.model.staff.respository;

import java.util.Optional;

import com.bjxczy.core.web.base.AbstractRepository;
import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;
import com.bjxczy.onepark.domain.model.staff.valueobject.EmployeeNo;
import com.bjxczy.onepark.domain.model.staff.valueobject.IdentityCard;
import com.bjxczy.onepark.domain.model.staff.valueobject.Mobile;
import com.bjxczy.onepark.domain.valueobject.Uuid;

public interface StaffRepository extends AbstractRepository<Staff> {

	Optional<Staff> findByUuid(Uuid uuid);

	Optional<Staff> findByMobile(Mobile mobile);

	Optional<Staff> findByIdentityCard(IdentityCard identifyCard);

    Optional<Staff> findByEmployeeNo(EmployeeNo employeeNo);
}
