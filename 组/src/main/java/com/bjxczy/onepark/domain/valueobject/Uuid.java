package com.bjxczy.onepark.domain.valueobject;

import javax.validation.constraints.NotBlank;

import com.bjxczy.core.web.base.ValueObject;

import cn.hutool.core.lang.UUID;
import lombok.Getter;
import lombok.Setter;

public class Uuid extends ValueObject<Uuid> {
	
	private static final long serialVersionUID = 3669836782315745781L;
	
	@Getter
	@Setter
	private String value;
	
	public Uuid() {
		this.value = UUID.fastUUID().toString().replace("-", "");
	}
	
	public Uuid(@NotBlank String uuid) {
		this.value = uuid;
	}

	@Override
	protected boolean equalsTo(Uuid other) {
		return getValue().equals(other.getValue());
	}
	
}
