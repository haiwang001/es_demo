package com.bjxczy.onepark.domain.model.organization.aggregate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bjxczy.onepark.domain.model.organization.event.CreateOrganizationEvent;
import com.bjxczy.onepark.domain.model.organization.event.RemoveOrganizationEvent;
import com.bjxczy.onepark.domain.model.organization.event.UpdateOranizationEvent;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;

import com.bjxczy.core.web.base.AbstractAggregateRoot;
import com.bjxczy.onepark.domain.model.organization.valueobject.ParentId;
import com.bjxczy.onepark.domain.model.organization.valueobject.Sort;
import com.bjxczy.onepark.domain.valueobject.Uuid;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class Organization extends AbstractAggregateRoot<Organization> {

	@Id
	private Integer id;

	@NotBlank(message = "部门名称不能为空！")
	private String fldName;

	@Length(min = 0, max = 200, message = "描述长度0-200字")
	private String description;

	@NotNull
	private ParentId parentId;

	private Sort sort;

	@NotBlank
	private String orgSource;

	@NotNull
	private Uuid uuid;

	private Integer showFlag;

	public Organization(String fldName, String description, ParentId parentId, String orgSource, Uuid uuid) {
		this.fldName = fldName;
		this.description = description;
		this.parentId = parentId;
		this.orgSource = orgSource;
		this.uuid = uuid;
		this.showFlag = 1;
		andEvent(new CreateOrganizationEvent(this));
	}

	public void changeName(String name,String id) {
		super.validate(); // 校验变更后字段
		this.fldName = name;
		this.id=Integer.valueOf(id);
		andEvent(new UpdateOranizationEvent(this));
	}

	@Override
	protected Object newRemoveEvent() {
		return new RemoveOrganizationEvent(this);
	}
}
