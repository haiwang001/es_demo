package com.bjxczy.onepark.domain.model.staff.valueobject;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.bjxczy.core.web.base.ValueObject;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Photo extends ValueObject<Photo> {
	
	private static final long serialVersionUID = -3941971918425357312L;
	
	private List<String> value;
	
	public Photo(@NotNull List<String> photo) {
		if (null != photo && photo.size() > 3) {
			throw new IllegalArgumentException("照片最多上传3条！");
		}
		this.value = photo;
	}

	@Override
	protected boolean equalsTo(Photo other) {
		return false;
	}

}
