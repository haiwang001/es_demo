package com.bjxczy.onepark.domain.model.staff.respository;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.domain.model.staff.entity.Rank;

public interface RankRepository extends IService<Rank> {

}
