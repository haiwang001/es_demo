package com.bjxczy.onepark.domain.model.organization.event;

import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @ClassName RemoveOrganizationEvent
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/31 16:50
 * @Version 1.0
 */
@Getter
@EqualsAndHashCode(callSuper = false)
public class RemoveOrganizationEvent extends ApplicationEvent {

    private Organization organization;

    public RemoveOrganizationEvent(Object source) {
        super(source);
        this.organization=(Organization) source;
    }
}
