package com.bjxczy.onepark.domain.model.staff.valueobject;

import javax.validation.constraints.NotBlank;

import com.bjxczy.core.web.base.ValueObject;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class MisCode extends ValueObject<MisCode> {
	
	private static final long serialVersionUID = -722081929249520038L;
	
	private String value;
	
	public MisCode(@NotBlank String value) {
		this.value = value;
	}

	@Override
	protected boolean equalsTo(MisCode other) {
		return getValue().equals(other.getValue());
	}


}
