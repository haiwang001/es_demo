package com.bjxczy.onepark.domain.model.organization.valueobject;

import java.util.List;

import com.bjxczy.core.web.base.ValueObject;
import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Sort extends ValueObject<Sort> {
	
	private static final long serialVersionUID = 1431496970328412747L;

	private static final Float DEFAULT_SORT_VALUE = 1.0F;
	
	@Getter
	@Setter
	private Float value;

	public Sort(List<Organization> list) {
		if (null == list || list.isEmpty()) {
			this.value = DEFAULT_SORT_VALUE;
		} else {
			Organization t = list.get(list.size() - 1);
			this.value = t.getSort().getValue() + 1;
		}
	}

	@Override
	protected boolean equalsTo(Sort other) {
		return false;
	}
	
}
