package com.bjxczy.onepark.domain.model.organization.event;

import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @ClassName CreateOrganizationEvetn
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/31 16:45
 * @Version 1.0
 */
@Getter
@EqualsAndHashCode(callSuper = false)
public class CreateOrganizationEvent extends ApplicationEvent {

    private Organization organization;

    public CreateOrganizationEvent(Object source) {
        super(source);
        this.organization=(Organization) source;

    }
}
