package com.bjxczy.onepark.domain.model.staff.valueobject;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.bjxczy.core.web.base.ValueObject;

import cn.hutool.core.util.IdcardUtil;
import com.bjxczy.onepark.common.exception.ResultException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class IdentityCard extends ValueObject<IdentityCard> {

	private static final long serialVersionUID = 2682822357136296439L;

	private String value;

	public IdentityCard(String value) {
		if (StringUtils.isNotBlank(value)) {
			if(!IdcardUtil.isValidCard(value)) {
				throw new ResultException("身份证号格式不正确！");
			}
		}
		this.value = value;
	}

	@Override
	protected boolean equalsTo(IdentityCard other) {
		return getValue().equals(other.getValue());
	}

}
