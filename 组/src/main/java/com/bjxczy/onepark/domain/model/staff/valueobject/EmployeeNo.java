package com.bjxczy.onepark.domain.model.staff.valueobject;

import com.bjxczy.core.web.base.ValueObject;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class EmployeeNo extends ValueObject<EmployeeNo> {
	
	private static final long serialVersionUID = 1895293514487950213L;
	
	private String value;
	
	public EmployeeNo(String value) {
		super();
		this.value = value;
	}

	@Override
	protected boolean equalsTo(EmployeeNo other) {
		return getValue().equals(other.getValue());
	}

}
