package com.bjxczy.onepark.domain.model.staff.aggregate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;

import com.bjxczy.core.web.base.AbstractAggregateRoot;
import com.bjxczy.onepark.domain.model.staff.event.CreateStaffEvent;
import com.bjxczy.onepark.domain.model.staff.event.RemoveStaffEvent;
import com.bjxczy.onepark.domain.model.staff.event.UpdateStaffEvent;
import com.bjxczy.onepark.domain.model.staff.valueobject.Email;
import com.bjxczy.onepark.domain.model.staff.valueobject.EmployeeNo;
import com.bjxczy.onepark.domain.model.staff.valueobject.IdentityCard;
import com.bjxczy.onepark.domain.model.staff.valueobject.Mobile;
import com.bjxczy.onepark.domain.model.staff.valueobject.Photo;
import com.bjxczy.onepark.domain.valueobject.Gender;
import com.bjxczy.onepark.domain.valueobject.Uuid;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Staff extends AbstractAggregateRoot<Staff> {

	@Id
	private Integer id;

	@NotBlank(message = "员工姓名不能为空！")
	private String fldName;

	private Mobile mobile;

	@NotNull
	private Uuid uuid;

	private Gender gender;

	private Email email;

	private EmployeeNo employeeNo;

	private IdentityCard identityCard;

	private Photo photo;

	private Boolean hasAccount;

	private Integer organizationId;

	private String positionIds;

	private Integer rankId;

	@Length(min = 0, max = 200, message = "描述长度0-200字")
	private String remark;

	private Integer dimission;

	private Integer comeFrom;
	public Staff(String fldName, Mobile mobile, Uuid uuid, Gender gender, Email email, EmployeeNo employeeNo,
				 IdentityCard identityCard, Photo photo, Integer organizationId, String positionIds, Integer rankId,
				 String remark) {
		super();
		this.fldName = fldName;
		this.mobile = mobile;
		this.uuid = uuid;
		this.gender = gender;
		this.email = email;
		this.employeeNo = employeeNo;
		this.identityCard = identityCard;
		this.photo = photo;
		this.hasAccount = false;
		this.organizationId = organizationId;
		this.positionIds = positionIds;
		this.rankId = rankId;
		this.remark = remark;
		andEvent(new CreateStaffEvent(this));
	}

	public void changeStaffInfo(@NotBlank(message = "员工姓名不能为空！") String fldName, Gender gender, Email email, EmployeeNo employeeNo,
			IdentityCard identityCard, Photo photo, Integer organizationId, String positionIds,
			Integer rankId, @Length(min = 0, max = 200, message = "描述长度0-200字") String remark,Integer dimission) {
		this.fldName = fldName;
		this.gender = gender;
		this.email = email;
		this.employeeNo = employeeNo;
		this.identityCard = identityCard;
		this.photo = photo;
		this.organizationId = organizationId;
		this.positionIds = positionIds;
		this.rankId = rankId;
		this.remark = remark;
		this.dimission=dimission;
		andEvent(new UpdateStaffEvent(this));
	}

	@Override
	protected Object newRemoveEvent() {
		return new RemoveStaffEvent(this);
	}

}
