package com.bjxczy.onepark.domain.model.staff.event;

import org.springframework.context.ApplicationEvent;

import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CreateStaffEvent extends ApplicationEvent {
	
	private static final long serialVersionUID = 5964811200710507662L;
	
	private Staff staff;

	public CreateStaffEvent(Object source) {
		super(source);
		this.staff = (Staff) source;
	}

}
