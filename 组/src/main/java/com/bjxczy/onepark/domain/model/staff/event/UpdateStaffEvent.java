package com.bjxczy.onepark.domain.model.staff.event;

import org.springframework.context.ApplicationEvent;

import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateStaffEvent extends ApplicationEvent {

	private static final long serialVersionUID = -8311836605835344759L;
	
	private Staff staff;

	public UpdateStaffEvent(Object source) {
		super(source);
		this.staff = (Staff) source;
	}

}
