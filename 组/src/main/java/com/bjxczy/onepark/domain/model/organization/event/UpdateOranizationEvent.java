package com.bjxczy.onepark.domain.model.organization.event;

import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @ClassName UpdateOranizationEvent
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/31 16:48
 * @Version 1.0
 */
@Getter
@EqualsAndHashCode(callSuper = false)
public class UpdateOranizationEvent extends ApplicationEvent {

    private Organization organization;

    public UpdateOranizationEvent(Object source) {
        super(source);
        this.organization=(Organization) source;
    }
}
