package com.bjxczy.onepark.domain.model.staff.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@TableName("tbl_position")
@EqualsAndHashCode(callSuper = false)
public class Position extends UserOperator {
	
	private static final long serialVersionUID = 8808366761690801912L;

	@TableId(type = IdType.AUTO)
	private Integer id;
	
	private String fldName; 

}
