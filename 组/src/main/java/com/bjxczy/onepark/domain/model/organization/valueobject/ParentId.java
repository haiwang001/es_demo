package com.bjxczy.onepark.domain.model.organization.valueobject;

import com.bjxczy.core.web.base.ValueObject;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ParentId extends ValueObject<ParentId> {
	
	private static final long serialVersionUID = -1775513392533405804L;

	private static final Integer DEFAULT_PARENT_ID = -1;
	
	private Integer value;

	public ParentId(Integer parentId) {
		this.value = parentId == null ? DEFAULT_PARENT_ID : parentId;
	}
	
	public boolean isRoot() {
		return this.value != null && this.value == DEFAULT_PARENT_ID;
	}

	@Override
	protected boolean equalsTo(ParentId other) {
		return getValue().equals(other.getValue());
	}

}
