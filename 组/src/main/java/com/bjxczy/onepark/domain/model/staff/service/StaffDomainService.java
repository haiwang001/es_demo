package com.bjxczy.onepark.domain.model.staff.service;

import javax.validation.Valid;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.utils.OptionalUtils;
import com.bjxczy.onepark.domain.model.staff.aggregate.Staff;
import com.bjxczy.onepark.domain.model.staff.respository.StaffRepository;

import java.util.Optional;

@Service
@Validated
public class StaffDomainService {

	@Autowired
	private StaffRepository staffRepository;

	public void createStaff(@Valid Staff staff) {
		staffRepository.findByMobile(staff.getMobile()).ifPresent(s -> {
			throw new ResultException("手机号已存在！");
		});
		staffRepository.findByEmployeeNo(staff.getEmployeeNo()).ifPresent(s -> {
			throw new ResultException("员工编号已存在！");
		});
		if (StrUtil.isNotEmpty(staff.getIdentityCard().getValue())){
			staffRepository.findByIdentityCard(staff.getIdentityCard()).ifPresent(s -> {
				throw new ResultException("身份证号已存在！");
			});
		}
		staffRepository.saveModel(staff);
	}

	public void removeStaff(Integer id) {
		OptionalUtils.ifPresentOrElse(staffRepository.findById(id), model -> {
			staffRepository.remove(model);
		}, () -> new ResultException("员工不存在！"));
	}

}
