package com.bjxczy.onepark.domain.model.organization.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.domain.model.organization.aggregate.Organization;
import com.bjxczy.onepark.domain.model.organization.repository.OrganizationRepository;
import com.bjxczy.onepark.domain.model.organization.valueobject.ParentId;
import com.bjxczy.onepark.domain.model.organization.valueobject.Sort;

@Service
@Validated
public class OrganizationDomainService {

	@Autowired
	private OrganizationRepository organizationRepository;

	public void createOrganization(@Valid Organization model) {
		if (!model.getParentId().isRoot()) {
			organizationRepository.findById(model.getParentId().getValue()).orElseThrow(() -> new ResultException("上级部门不存在！"));
		}
		// 设置排序
		List<Organization> list = organizationRepository.findByParentId(model.getParentId());
		model.setSort(new Sort(list));
		this.organizationRepository.saveModel(model);
	}

	public void remove(Integer id) {
		Organization model = organizationRepository.findById(id).orElseThrow(() -> new ResultException("部门不存在！"));
		removeChild(model);
	}

	private void removeChild(Organization model) {
		List<Organization> list = organizationRepository.findByParentId(new ParentId(model.getId()));
		for (Organization item : list) {
			removeChild(item);
		}
		organizationRepository.remove(model);
	}

}
