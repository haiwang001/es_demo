# EO单模块-API开发文档

## 字典数据

| group_code      | label          | value | extra |
| --------------- | -------------- | ----- | ----- |
| eoTaskStatus    | 待审批         | 0     |       |
| eoTaskStatus    | 已通过         | 1     |       |
| eoTaskStatus    | 已驳回         | 2     |       |
| eoTaskStatus    | 已执行         | 3     |       |
| eoTaskStatus    | 已完成         | 4     |       |
| park            | 东区           | 1     |       |
| park            | 西区           | 2     |       |
| roomType        | 大床498元/间夜 | 1     |       |
| roomType        | 午休房200元/间 | 2     |       |
| diningForm      | 自助餐         | 1     |       |
| diningForm      | 桌餐           | 2     |       |
| diningForm      | 冷餐           | 3     |       |
| diningForm      | 盒餐           | 4     |       |
| diningForm      | 其他           | 5     |       |
| eoType          | 酒店           | 1     |       |
| eoType          | 餐饮           | 2     |       |
| eoType          | 会议           | 3     |       |
| mealType        | 早餐           | 1     |       |
| mealType        | 午餐           | 2     |       |
| mealType        | 晚餐           | 3     |       |
| certificateType | 身份证         | 1     |       |
| certificateType | 驾驶证         | 2     |       |
| certificateType | 学生证         | 3     |       |
| certificateType | 护照           | 4     |       |
| certificateType | 士官证         | 5     |       |
| certificateType | 港澳通行证     | 6     |       |

## 一、表结构	

### eo_template（消费模板表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | varchar(32) | Y | 模板Id |
| name | varchar(50) | Y | 模板名称 |
| catering | varchar(60) | Y | 餐饮 |
| remark | varchar(255) | N | 备注 |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 更新时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |

### eo_task（EO单任务表）

| 字段          | 数据类型    | 是否必填 | 描述                |
| ------------- | ----------- | -------- | ------------------- |
| id            | varchar(32) | Y        | EO单Id              |
| name          | varchar(50) | Y        | EO单名称            |
| account       | varchar(50) | Y        | 主账账号            |
| start_time    | varchar(64) | Y        | 开始日期            |
| end_time      | varchar(64) | Y        | 结束日期            |
| person_count  | int   | Y        | 人数                |
| sign_name     | varchar(32) | Y        | 联系及签单人 姓名   |
| sign_mobile   | varchar(11) | Y        | 联系及签单人 手机号 |
| dept          | varchar(50) | Y        | 归属公司及部门      |
| proj_name     | varchar(32) | Y        | 项目负责人 姓名     |
| proj_mobile   | varchar(11) | Y        | 项目负责人 手机号   |
| eo_type | varchar(10) | Y | 种类（字典：eoType） |
| template_id   | varchar(32) | N       | 收费标准（模板Id）  |
| accommodation | text        | N        | 住宿                |
| catering | text | N | 餐饮 |
| rent          | text        | N        | 场租                |
| remark        | text        | N        | 备注                |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 更新时间 |
| operator | varchar(50) | N | 操作人 |
| status | varchar(10) | Y | EO单状态（字典：eoTaskStatus） |
| del_flag | tinyint | Y | 是否删除 |
| reject_reason | text | N | 驳回原因 |
| initiator_id | int | Y | 发起人 |
### eo_hotel（酒店EO单表）

| 字段   | 数据类型    | 是否必填 | 描述                   |
| ------ | ----------- | -------- | ---------------------- |
| id     | int         | Y        | 主键id                 |
| eo_id  | varchar(32) | Y        | EO单Id                 |
| park   | vatchar(10) | Y        | 选择园区（字典：park） |
| remark | text        | N        | 备注                   |

### eo_accommodation（酒店EO单-住宿表）

| 字段       | 数据类型    | 是否必填 | 描述                   |
| ---------- | ----------- | -------- | ---------------------- |
| id         | int         | Y        | 主键id                 |
| hotel_id   | int         | Y        | 酒店EO单Id             |
| room_type  | varchar(10) | Y        | 房型（字典：roomType） |
| type_count | int         | Y        | 房型数量               |

### eo_metting（酒店EO单-会议表）

| po          | 数据类型    | 是否必填 | 描述         |
| ----------- | ----------- | -------- | ------------ |
| id          | int         | Y        | 主键Id       |
| hotel_id    | int         | Y        | 酒店EO单Id   |
| topic       | varchar(64) | Y        | 会议主题     |
| start_time  | varchar(64) | Y        | 会议开始时间 |
| end_time    | varchar(64) | Y        | 会议结束时间 |
| met_room_id | varchar(32) | Y        | 会议室Id     |

### eo_metting_goods（会议-物品表）

| 字段       | 数据类型    | 是否必填 | 描述     |
| ---------- | ----------- | -------- | -------- |
| id         | int         | Y        | 主键id   |
| metting_id | int         | Y        | 会议Id   |
| go_name    | varchar(64) | Y        | 物品名称 |
| go_count   | int         | Y        | 物品数量 |

### eo_catering（餐饮EO单表）

| 字段           | 数据类型     | 是否必填 | 描述                           |
| -------------- | ------------ | -------- | ------------------------------ |
| id             | int          | Y        | 主键Id                         |
| eo_id          | varchar(32)  | Y        | EO单Id                         |
| meal_type      | varchar(10)  | Y        | 用餐类型（字典表：mealType）   |
| breakfast_time | varchar(50)  | N        | 早餐时间                       |
| lunch_time     | varchar(50)  | N        | 午餐时间                       |
| dinner_time    | varchar(50)  | N        | 晚餐时间                       |
| dining_place   | varchar(100) | Y        | 用餐地点                       |
| dining_form    | varchar(10)  | Y        | 用餐形式（字典表：diningForm） |
| remark         | text         | N        | 备注                           |

### eo_catering_schedule（餐饮时刻表）

| 字段           | 数据类型    | 是否必填 | 描述                         |
| -------------- | ----------- | -------- | ---------------------------- |
| id             | int         | Y        | 主键Id                       |
| meal_type      | varchar(10) | Y        | 用餐类型（字典表：mealType） |
| catering_id    | int         | Y        | 餐饮EO单Id                   |
| catering_date  | varchar(50) | Y        | 日期                         |
| breakfast_time | varchar(50) | N        | 早餐时间                     |
| lunch_time     | varchar(50) | N        | 午餐时间                     |
| dinner_time    | varchar(50) | N        | 晚餐时间                     |

### eo_vehicle（车辆EO单表）

| 字段      | 数据类型    | 是否必填 | 描述     |
| --------- | ----------- | -------- | -------- |
| id        | int         | Y        | 主键Id   |
| eo_id     | varchar(32) | Y        | EO单Id   |
| car_count | int         | Y        | 车辆数量 |
| remark    | text        | N        | 备注     |

### eo_vehicle_info（车辆信息表）

| 字段       | 数据类型    | 是否必填 | 描述       |
| ---------- | ----------- | -------- | ---------- |
| id         | int         | Y        | 主键Id     |
| vehicle_id | int         | Y        | 车辆Eo单Id |
| park       | varchar(64) | Y        | 停车场     |
| car_number | varchar(32) | Y        | 车牌号     |
| name       | varchar(32) | Y        | 姓名       |
| car_color  | varchar(32) | Y        | 车辆颜色   |
| mobile     | varchar(32) | Y        | 手机号     |

### eo_visit_info（来访人员信息表）

| 字段             | 数据类型     | 是否必填 | 描述     |
| ---------------- | ------------ | -------- | -------- |
| id               | int          | Y        | 主键Id   |
| eo_id            | varchar(32)  | Y        | EO单Id   |
| name             | varchar(32)  | Y        | 访客姓名 |
| certificate_type | varchar(10)  | Y        | 证件类型 |
| certificate_no   | varchar(64)  | Y        | 证件编号 |
| mobile           | varchar(32)  | Y        | 访客手机 |
| photo_url        | text         | N        | 照片地址 |
| visit_reason     | varchar(255) | N        | 到访事由 |
| area_id          | int          | Y        | 到访园区 |
| remark           | varchar(255) | N        | 备注     |
| interviewee      | int          | Y        | 接访人   |

### eo_settlement（结算信息表）

| 字段              | 数据类型    | 是否必填 | 描述         |
| ----------------- | ----------- | -------- | ------------ |
| id                | int         | Y        | 主键Id       |
| eo_id             | varchar(32) | Y        | EO单Id       |
| fee_accommodation | int         | Y        | 住宿实付金额 |
| fee_catering      | int         | Y        | 餐饮实付金额 |
| fee_metting       | int         | Y        | 会议实付金额 |
| fee_total         | int         | Y        | 实付总计     |

## 二、管理端接口

### 1、消费模板

#### （1）新增消费模板

请求地址：

| URL    | api/bmpworkflow/eo/addEoTemplate |
| ------ | -------------------------------- |
| Method | POST                             |

请求参数：

| 参数     | 类型   | 位置 | 说明     |
| -------- | ------ | ---- | -------- |
| name     | String | body | 模板名称 |
| catering | String | body | 餐饮     |
| remark   | String | body | 备注     |

请求实例：

```json
{
  "catering": "150元/人/天",
  "name": "A消费模板",
  "remark": "A消费模板备注信息"
}
```

#### （2）编辑消费模板

请求地址：

| URL    | /api/bmpworkflow/eo/editEoTemplate |
| ------ | ---------------------------------- |
| Method | PUT                                |

请求参数：

| 参数     | 类型   | 位置 | 说明     |
| -------- | ------ | ---- | -------- |
| id       | String | body | 模板Id   |
| name     | String | body | 模板名称 |
| catering | String | body | 餐饮     |
| remark   | String | body | 备注     |

#### （3）删除消费模板

请求地址：

| URL    | /api/bmpworkflow/eo/delEoTemplate/{id} |
| ------ | -------------------------------------- |
| Method | DELETE                                 |

请求参数：

| 参数 | 类型   | 位置  | 说明       |
| ---- | ------ | ----- | ---------- |
| id   | String | query | 消费模板Id |

请求实例：

```
/api/bmpworkflow/eo/delEoTemplate/51ba343b6be193b2255c2587df7ffd8e
```

#### （4）查询消费模板-分页

| URL    | /api/bmpworkflow/eo/getEoTemplatesByPage |
| ------ | ---------------------------------------- |
| Method | GET                                      |

请求参数：

| 参数     | 类型    | 位置  | 说明         |
| -------- | ------- | ----- | ------------ |
| pageNum  | Integer | query | 页数         |
| pageSize | Integer | query | 每页条数     |
| name     | String  | query | 消费模板名称 |

返回参数：

| 参数     | 类型   | 说明     |
| -------- | ------ | -------- |
| id       | String | 模板ID   |
| operator | String | 操作人   |
| name     | String | 模板名称 |
| catering | String | 餐饮     |
| remark   | String | 备注     |

#### （5）收费标准下拉

| URL    | /api/bmpworkflow/eo/getEoTemplatesPullList |
| ------ | ------------------------------------------ |
| Method | GET                                        |

请求参数：无

### 2、新增EO单

请求地址：

| URL    | api/bmpworkflow/eo/addEoTask |
| ------ | ---------------------------- |
| Method | POST                         |

请求参数：

| 参数                       | 类型    | 位置 | 说明                         |
| -------------------------- | ------- | ---- | ---------------------------- |
| name                       | String  | body | EO单名称                     |
| account                    | String  | body | 主账账号                     |
| startTime                  | String  | body | 开始日期                     |
| endTime                    | String  | body | 结束日期                     |
| personCount                | Integer | body | 人数                         |
| signMobile                 | String  | body | 联系及签单人 手机号          |
| signName                   | String  | body | 联系及签单人 姓名            |
| dept                       | String  | body | 归属公司及部门               |
| projMobile                 | String  | body | 项目负责人 手机号            |
| projName                   | String  | body | 项目负责人 姓名              |
| templateId                 | String  | body | 收费标准（模板Id）           |
| accommodation              | String  | body | 住宿                         |
| catering                   | String  | body | 餐饮                         |
| rent                       | String  | body | 场租                         |
| remark                     | String  | body | 备注                         |
| eoType                     | String  | body | 种类（字典：eoType）         |
| -- eoHotelDTO              | object  | body | 酒店相关信息                 |
| ---- park                  | String  | body | 园区（字典：park）           |
| ---- remark                | String  | body | 备注                         |
| ---- eoAccommodationPOS    | List    | body | 住宿房型信息添加             |
| ------ roomType            | String  | body | 房型（字典：roomType）       |
| ------ typeCount           | Integer | body | 房型数量                     |
| ---- eoMettingPOS          | List    | body | 会议相关信息                 |
| ------ topic               | String  | body | 会议主题                     |
| ------ startTime           | String  | body | 会议开始时间                 |
| ------ endTime             | String  | body | 会议结束时间                 |
| ------ metRoomId           | String  | body | 会议室Id                     |
| -- eoCateringDTO           | object  | body | 餐饮相关信息（统一配置）     |
| ---- mealType              | String  | body | 用餐类型（字典表：mealType） |
| ---- breakfastTime         | String  | body | 早餐                         |
| ---- lunchTime             | String  | body | 午餐                         |
| ---- dinnerTime            | String  | body | 晚餐                         |
| ---- diningPlace           | String  | body | 用餐地点                     |
| ---- diningForm            | String  | body | 用餐形式（字典：diningForm） |
| ---- remark                | String  | body | 备注                         |
| ---- eoCateringSchedulePOS | List    | body | 时刻表相关                   |
| ---- mealType              | String  | body | 用餐类型（字典表：mealType） |
| ------ cateringDate        | String  | body | 日期                         |
| ------ breakfastTime       | String  | body | 早餐时间                     |
| ------ lunchTime           | String  | body | 午餐时间                     |
| ------ dinnerTime          | String  | body | 晚餐时间                     |
| -- eoVehicleDTO            | object  | body | 车辆相关信息                 |
| ---- carCount              | Integer | body | 车辆数量                     |
| ---- remark                | String  | body | 备注                         |

### 3、查询EO单-分页

| URL    | /api/bmpworkflow/eo/getEoTaskByPage |
| ------ | ----------------------------------- |
| Method | GET                                 |

请求参数：

| 参数     | 类型    | 位置  | 说明                |
| -------- | ------- | ----- | ------------------- |
| pageNum  | Integer | query | 页数                |
| pageSize | Integer | query | 每页条数            |
| name     | String  | query | EO单名称            |
| sSTime   | String  | query | 开始日期 - 开始时间 |
| eSTime   | String  | query | 开始日期 - 结束时间 |
| sETime   | String  | query | 结束日期 - 开始时间 |
| eETime   | String  | query | 结束日期 - 结束时间 |

### 4、审批EO单

请求地址：

| URL    | /api/bmpworkflow/eo/checkEoTask |
| ------ | ------------------------------- |
| Method | PUT                             |

请求参数：

| 参数         | 类型   | 位置 | 说明                                                     |
| ------------ | ------ | ---- | -------------------------------------------------------- |
| id           | String | body | EO单Id                                                   |
| status       | String | body | 审批状态（关联字典表：eoTaskStatus 已通过：1 已驳回：2） |
| rejectReason | String | body | 驳回原因                                                 |

### 5、详情-新增访客人员信息

请求地址：

| URL    | api/bmpworkflow/eo/addVisits |
| ------ | ---------------------------- |
| Method | POST                         |

请求参数：

| 参数            | 类型    | 位置 | 说明                            |
| --------------- | ------- | ---- | ------------------------------- |
| eoId            | String  | body | Eo单Id                          |
| name            | String  | body | 访客姓名                        |
| certificateType | String  | body | 证件类型  字典：certificateType |
| certificateNo   | String  | body | 证件编号                        |
| mobile          | String  | body | 访客手机                        |
| photoUrl        | String  | body | 照片地址                        |
| visitReason     | String  | body | 到访事由                        |
| areaId          | Integer | body | 到访园区                        |
| remark          | String  | body | 备注                            |
| interviewee     | Integer | body | 接访人                          |

### 6、详情-新增车辆信息

请求地址：

| URL    | api/bmpworkflow/eo/addVehicleInfo |
| ------ | --------------------------------- |
| Method | POST                              |

请求参数：

| 参数      | 类型   | 位置 | 说明     |
| --------- | ------ | ---- | -------- |
| eoId      | String | body | Eo单Id   |
| park      | String | body | 停车场   |
| carNumber | String | body | 车牌号   |
| name      | String | body | 姓名     |
| mobile    | String | body | 手机号   |
| carColor  | String | body | 车辆颜色 |

### 7、详情-新增会议物品信息

请求地址：

| URL    | api/bmpworkflow/eo/addMettingGoods |
| ------ | ---------------------------------- |
| Method | POST                               |

请求参数：

| 参数      | 类型   | 位置 | 说明     |
| --------- | ------ | ---- | -------- |
| mettingId | String | body | 会议Id   |
| goName    | String | body | 物品名称 |
| goCount   | String | body | 物品数量 |



### 8、详情-删除访客人员信息

| 地址     | /api/bmpworkflow/eo/delVisitById/{id} |
| -------- | ------------------------------------- |
| 请求方式 | DELETE                                |

参数：

| 参数名 | 类型    | 位置 | 说明           |
| ------ | ------- | ---- | -------------- |
| id     | Integer | path | 访客人员信息Id |

### 9、详情-删除车辆信息

| 地址     | /api/bmpworkflow/eo/delVehicleById/{id} |
| -------- | --------------------------------------- |
| 请求方式 | DELETE                                  |

参数：

| 参数名 | 类型    | 位置 | 说明       |
| ------ | ------- | ---- | ---------- |
| id     | Integer | path | 车辆信息Id |

### 10、详情-删除会议物品

| 地址     | /api/bmpworkflow/eo/delMettingGoodsById/{id} |
| -------- | -------------------------------------------- |
| 请求方式 | DELETE                                       |

参数：

| 参数名 | 类型    | 位置 | 说明       |
| ------ | ------- | ---- | ---------- |
| id     | Integer | path | 会议物品Id |

### 11、下载人员信息模板

| URL    | /api/bmpworkflow/eo/downloadVisitTemplate |
| ------ | ----------------------------------------- |
| Method | GET                                       |

请求参数：无

### 12、下载车辆信息模板

| URL    | /api/bmpworkflow/eo/downloadVehicleTemplate |
| ------ | ------------------------------------------- |
| Method | GET                                         |

请求参数：无

### 13、下载会议物品模板

| URL    | /api/bmpworkflow/eo/downloadMettingGoodsTemplate |
| ------ | ------------------------------------------------ |
| Method | GET                                              |

请求参数：无

### 14、详情-查询人员信息分页

| URL    | /api/bmpworkflow/eo/getVisitInfoByPage |
| ------ | -------------------------------------- |
| Method | GET                                    |

请求参数：

| 参数     | 类型    | 位置  | 说明     |
| -------- | ------- | ----- | -------- |
| pageNum  | Integer | query | 页数     |
| pageSize | Integer | query | 每页条数 |
| eoId     | String  | query | eo单Id   |

### 15、详情-查询车辆信息分页

| URL    | /api/bmpworkflow/eo/getVehicleInfoByPage |
| ------ | ---------------------------------------- |
| Method | GET                                      |

请求参数：

| 参数     | 类型    | 位置  | 说明     |
| -------- | ------- | ----- | -------- |
| pageNum  | Integer | query | 页数     |
| pageSize | Integer | query | 每页条数 |
| eoId     | String  | query | eo单Id   |

### 16、详情-查询会议物品信息

| URL    | /api/bmpworkflow/eo/getMettingGoodsInfoByPage |
| ------ | --------------------------------------------- |
| Method | GET                                           |

请求参数：

| 参数      | 类型    | 位置  | 说明     |
| --------- | ------- | ----- | -------- |
| pageNum   | Integer | query | 页数     |
| pageSize  | Integer | query | 每页条数 |
| mettingId | Integer | query | 会议Id   |

### 17、删除EO单

| 地址     | /api/bmpworkflow/eo//delEoTask/{id} |
| -------- | ----------------------------------- |
| 请求方式 | DELETE                              |

参数：

| 参数名 | 类型   | 位置 | 说明     |
| ------ | ------ | ---- | -------- |
| id     | String | path | 删除EO单 |

### 18、编辑EO单

| 地址     | /api/bmpworkflow/eo/editEoTask |
| -------- | ------------------------------ |
| 请求方式 | PUT                            |

请求参数：

| 参数                       | 类型    | 位置 | 说明                         |
| -------------------------- | ------- | ---- | ---------------------------- |
| id                         | String  | body | EO单Id                       |
| name                       | String  | body | EO单名称                     |
| account                    | String  | body | 主账账号                     |
| startTime                  | String  | body | 开始日期                     |
| endTime                    | String  | body | 结束日期                     |
| personCount                | Integer | body | 人数                         |
| signMobile                 | String  | body | 联系及签单人 手机号          |
| signName                   | String  | body | 联系及签单人 姓名            |
| dept                       | String  | body | 归属公司及部门               |
| projMobile                 | String  | body | 项目负责人 手机号            |
| projName                   | String  | body | 项目负责人 姓名              |
| templateId                 | String  | body | 收费标准（模板Id）           |
| accommodation              | String  | body | 住宿                         |
| catering                   | String  | body | 餐饮                         |
| rent                       | String  | body | 场租                         |
| remark                     | String  | body | 备注                         |
| eoType                     | String  | body | 种类（字典：eoType）         |
| -- eoHotelDTO              | object  | body | 酒店相关信息                 |
| ---- park                  | String  | body | 园区（字典：park）           |
| ---- remark                | String  | body | 备注                         |
| ---- eoAccommodationPOS    | List    | body | 住宿房型信息添加             |
| ------ roomType            | String  | body | 房型（字典：roomType）       |
| ------ typeCount           | Integer | body | 房型数量                     |
| ---- eoMettingPOS          | List    | body | 会议相关信息                 |
| ------ topic               | String  | body | 会议主题                     |
| ------ startTime           | String  | body | 会议开始时间                 |
| ------ endTime             | String  | body | 会议结束时间                 |
| ------ metRoomId           | String  | body | 会议室Id                     |
| -- eoCateringDTO           | object  | body | 餐饮相关信息（统一配置）     |
| ---- mealType              | String  | body | 用餐类型（字典表：mealType） |
| ---- breakfastTime         | String  | body | 早餐                         |
| ---- lunchTime             | String  | body | 午餐                         |
| ---- dinnerTime            | String  | body | 晚餐                         |
| ---- diningPlace           | String  | body | 用餐地点                     |
| ---- diningForm            | String  | body | 用餐形式（字典：diningForm） |
| ---- remark                | String  | body | 备注                         |
| ---- eoCateringSchedulePOS | List    | body | 时刻表相关                   |
| ---- mealType              | String  | body | 用餐类型（字典表：mealType） |
| ------ cateringDate        | String  | body | 日期                         |
| ------ breakfastTime       | String  | body | 早餐时间                     |
| ------ lunchTime           | String  | body | 午餐时间                     |
| ------ dinnerTime          | String  | body | 晚餐时间                     |
| -- eoVehicleDTO            | object  | body | 车辆相关信息                 |
| ---- carCount              | Integer | body | 车辆数量                     |
| ---- remark                | String  | body | 备注                         |

### 19、导入会议物品数据

请求地址：

| URL    | api/bmpworkflow/eo/importMettingGoodsData |
| ------ | ----------------------------------------- |
| Method | POST                                      |

请求参数：

| 参数      | 类型          | 位置  | 说明          |
| --------- | ------------- | ----- | ------------- |
| mettingId | Integer       | param | 会议ID        |
| file      | MultipartFile | param | 会议物品Excel |

### 20、执行EO单-联动工单

请求地址：

| URL    | api/bmpworkflow/eo/execute |
| ------ | -------------------------- |
| Method | POST                       |

请求参数：

| 参数       | 类型   | 位置 | 说明               |
| ---------- | ------ | ---- | ------------------ |
| mettingIds | String | body | 会议Id（逗号分隔） |
| eoId       | String | body | EO单ID             |

### 21、导入车辆数据

请求地址：

| URL    | api/bmpworkflow/eo/importVehicleData |
| ------ | ------------------------------------ |
| Method | POST                                 |

请求参数：

| 参数           | 类型   | 位置 | 说明         |
| -------------- | ------ | ---- | ------------ |
| file     | MultipartFile | param | 车辆excel文件   |
| vehicleId | Integer | param | 车辆EO单ID |

### 22、导入访客模板

请求地址：

| URL    | api/bmpworkflow/eo/importVisitData |
| ------ | ---------------------------------- |
| Method | POST                               |

请求参数：

| 参数 | 类型          | 位置  | 说明          |
| ---- | ------------- | ----- | ------------- |
| file | MultipartFile | param | 访客excel文件 |
| eoId | String        | param | EO单Id        |

### 23、EO单-结算EO单

请求地址：

| URL    | api/bmpworkflow/eo/settlement |
| ------ | ----------------------------- |
| Method | POST                          |

请求参数：

| 参数             | 类型    | 位置 | 说明         |
| ---------------- | ------- | ---- | ------------ |
| eoId             | String  | body | eo单Id       |
| feeTotal         | Integer | body | 实付总计     |
| feeMetting       | Integer | body | 会议实付金额 |
| feeCatering      | Integer | body | 餐饮实付金额 |
| feeAccommodation | Integer | body | 住宿实付金额 |

### 24、Eo单-根据eoId查询结算信息

| URL    | /api/bmpworkflow/eo/getSettlementInfo |
| ------ | ------------------------------------- |
| Method | GET                                   |

请求参数：

| 参数 | 类型   | 位置  | 说明   |
| ---- | ------ | ----- | ------ |
| eoId | String | query | EO单Id |

## 三、移动端接口

### 1、酒店EO单-查看详情

| URL    | /api/bmpworkflow/eo/getCateringInfo |
| ------ | ----------------------------------- |
| Method | GET                                 |

请求参数：

| 参数 | 类型   | 位置  | 说明   |
| ---- | ------ | ----- | ------ |
| eoId | String | query | eo单Id |

返回参数：

| 参数                       | 类型    | 说明                         |
| -------------------------- | ------- | ---------------------------- |
| name                       | String  | EO单名称                     |
| account                    | String  | 主账账号                     |
| startTime                  | String  | 开始日期                     |
| endTime                    | String  | 结束日期                     |
| personCount                | Integer | 人数                         |
| signMobile                 | String  | 联系及签单人 手机号          |
| signName                   | String  | 联系及签单人 姓名            |
| dept                       | String  | 归属公司及部门               |
| projMobile                 | String  | 项目负责人 手机号            |
| projName                   | String  | 项目负责人 姓名              |
| templateId                 | String  | 收费标准（模板Id）           |
| accommodation              | String  | 住宿                         |
| catering                   | String  | 餐饮                         |
| rent                       | String  | 场租                         |
| remark                     | String  | 备注                         |
| eoType                     | String  | 种类（字典：eoType）         |
| -- eoHotelVO               | object  | 酒店相关信息                 |
| ---- park                  | String  | 园区（字典：park）           |
| ---- remark                | String  | 备注                         |
| ---- eoAccommodationVOS    | List    | 住宿房型信息添加             |
| ------ roomType            | String  | 房型（字典：roomType）       |
| ------ typeCount           | Integer | 房型数量                     |

### 2、会议EO单-查看详情

| URL    | /api/bmpworkflow/eo/getCateringInfo |
| ------ | ----------------------------------- |
| Method | GET                                 |

请求参数：

| 参数 | 类型   | 位置  | 说明   |
| ---- | ------ | ----- | ------ |
| eoId | String | query | eo单Id |

返回参数：

| 参数                       | 类型    | 说明                         |
| -------------------------- | ------- | ---------------------------- |
| name                       | String  | EO单名称                     |
| account                    | String  | 主账账号                     |
| startTime                  | String  | 开始日期                     |
| endTime                    | String  | 结束日期                     |
| personCount                | Integer | 人数                         |
| signMobile                 | String  | 联系及签单人 手机号          |
| signName                   | String  | 联系及签单人 姓名            |
| dept                       | String  | 归属公司及部门               |
| projMobile                 | String  | 项目负责人 手机号            |
| projName                   | String  | 项目负责人 姓名              |
| templateId                 | String  | 收费标准（模板Id）           |
| accommodation              | String  | 住宿                         |
| catering                   | String  | 餐饮                         |
| rent                       | String  | 场租                         |
| remark                     | String  | 备注                         |
| eoType                     | String  | 种类（字典：eoType）         |
| -- eoHotelVO               | object  | 酒店相关信息                 |
| ---- park                  | String  | 园区（字典：park）           |
| ---- remark                | String  | 备注                         |
| ---- eoMettingVOS          | List    | 会议相关信息                 |
| ------ topic               | String  | 会议主题                     |
| ------ startTime           | String  | 会议开始时间                 |
| ------ endTime             | String  | 会议结束时间                 |
| ------ metRoomId           | String  | 会议室Id                     |

### 3、餐饮EO单-查看详情
| URL    | /api/bmpworkflow/eo/getCateringInfo |
| ------ | ----------------------------------- |
| Method | GET                                 |

请求参数：

| 参数 | 类型   | 位置  | 说明   |
| ---- | ------ | ----- | ------ |
| eoId | String | query | eo单Id |

返回参数：

| 参数                       | 类型    | 说明                         |
| -------------------------- | ------- | ---------------------------- |
| name                       | String  | EO单名称                     |
| account                    | String  | 主账账号                     |
| startTime                  | String  | 开始日期                     |
| endTime                    | String  | 结束日期                     |
| personCount                | Integer | 人数                         |
| signMobile                 | String  | 联系及签单人 手机号          |
| signName                   | String  | 联系及签单人 姓名            |
| dept                       | String  | 归属公司及部门               |
| projMobile                 | String  | 项目负责人 手机号            |
| projName                   | String  | 项目负责人 姓名              |
| templateId                 | String  | 收费标准（模板Id）           |
| accommodation              | String  | 住宿                         |
| catering                   | String  | 餐饮                         |
| rent                       | String  | 场租                         |
| remark                     | String  | 备注                         |
| eoType                     | String  | 种类（字典：eoType）         |
| -- eoCateringVO           | object  | 餐饮相关信息（统一配置）     |
| ---- mealType              | String  | 用餐类型（字典表：mealType） |
| ---- breakfastTime         | String  | 早餐                         |
| ---- lunchTime             | String  | 午餐                         |
| ---- dinnerTime            | String  | 晚餐                         |
| ---- diningPlace           | String  | 用餐地点                     |
| ---- diningForm            | String  | 用餐形式（字典：diningForm） |
| ---- remark                | String  | 备注                         |
| ---- eoCateringScheduleVOS | List    | 时刻表相关                   |
| ---- mealType              | String  | 用餐类型（字典表：mealType） |
| ------ cateringDate        | String  | 日期                         |
| ------ breakfastTime       | String  | 早餐时间                     |
| ------ lunchTime           | String  | 午餐时间                     |
| ------ dinnerTime          | String  | 晚餐时间                     |
| -- eoVehicleVO            | object  | 车辆相关信息                 |
| ---- carCount              | Integer | 车辆数量                     |
| ---- remark                |         |                              |

### 4、车辆EO单-查看详情
| URL    | /api/bmpworkflow/eo/getCateringInfo |
| ------ | ----------------------------------- |
| Method | GET                                 |

请求参数：

| 参数 | 类型   | 位置  | 说明   |
| ---- | ------ | ----- | ------ |
| eoId | String | query | eo单Id |

返回参数：

| 参数                       | 类型    | 说明                         |
| -------------------------- | ------- | ---------------------------- |
| name                       | String  | EO单名称                     |
| account                    | String  | 主账账号                     |
| startTime                  | String  | 开始日期                     |
| endTime                    | String  | 结束日期                     |
| personCount                | Integer | 人数                         |
| signMobile                 | String  | 联系及签单人 手机号          |
| signName                   | String  | 联系及签单人 姓名            |
| dept                       | String  | 归属公司及部门               |
| projMobile                 | String  | 项目负责人 手机号            |
| projName                   | String  | 项目负责人 姓名              |
| templateId                 | String  | 收费标准（模板Id）           |
| accommodation              | String  | 住宿                         |
| catering                   | String  | 餐饮                         |
| rent                       | String  | 场租                         |
| remark                     | String  | 备注                         |
| eoType                     | String  | 种类（字典：eoType）         |
| -- eoVehicleVO            | object  | 车辆相关信息                 |
| ---- carCount              | Integer | 车辆数量                     |
| ---- remark                |         |                              |

### 5、财务EO单-查看详情
| URL    | /api/bmpworkflow/eo/getCateringInfo |
| ------ | ----------------------------------- |
| Method | GET                                 |

请求参数：

| 参数 | 类型   | 位置  | 说明   |
| ---- | ------ | ----- | ------ |
| eoId | String | query | eo单Id |

返回参数：

| 参数                       | 类型    | 说明                         |
| -------------------------- | ------- | ---------------------------- |
| name                       | String  | EO单名称                     |
| account                    | String  | 主账账号                     |
| startTime                  | String  | 开始日期                     |
| endTime                    | String  | 结束日期                     |
| personCount                | Integer | 人数                         |
| signMobile                 | String  | 联系及签单人 手机号          |
| signName                   | String  | 联系及签单人 姓名            |
| dept                       | String  | 归属公司及部门               |
| projMobile                 | String  | 项目负责人 手机号            |
| projName                   | String  | 项目负责人 姓名              |
| templateId                 | String  | 收费标准（模板Id）           |
| accommodation              | String  | 住宿                         |
| catering                   | String  | 餐饮                         |
| rent                       | String  | 场租                         |
| remark                     | String  | 备注                         |
| eoType                     | String  | 种类（字典：eoType）         |












