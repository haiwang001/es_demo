# 工作流引擎-API开发文档

## 字典数据
| group_code | label | value | extra |
| - | - | - | - |
| applyType | 审核中                  | 0 | |
| applyType | 通过 | 1 | |
| applyType | 关闭 | 2 | |
| candidateType | 指定人 | fixed | |
| candidateType | 发起人自选 | self_select | |
| candidateType | 系统角色 | role | |
| candidateType | 园区主管 | supervisor | |
| candidateType | 职级 | rank | |
| candidateType | 员工所在部门某职级 | self_dept_rank | |
| candidateType | 员工上级部门某职级 | higher_dept_rank | |
| assigneeIsNull | 自动通过 | auto_complete | |
| assigneeIsNull | 自动驳回 | auto_reject | |
| assigneeIsNull | 转交指定人员 | forward | |
| rejectType | 结束流程 | close | |
| rejectType | 驳回至申请人 | to_initiator | |
| rejectType | 驳回至上级节点 | to_source | |
| rejectType | 审批人自选节点 | custom | |
| conditionType | 表单字段条件 | formItem | |
| conditionType | 发起人 属于某职级 | belong_rank | |
| conditionType | 发起人 属于某部门 | belong_dept | |
| conditionType | 发起人 是某些人其中之一 | belong_some_one | |
| conditionType | 其它条件都不满足 | else | |
| formItemOper | 等于 | == ||
| formItemOper | 大于 | > | |
| formItemOper | 大于等于 | >= | |
| formItemOper | 小于 | < | |
| formItemOper | 大于等于 | <= | |

## 一、表结构

### wf_dict（字典表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | 主键ID，自增长 |
| parent_id | int | Y | 父ID |
| group_code | varchar(50) | Y | 字典分组 |
| dict_code | varchar(50) | Y | 类型值 |
| dict_label | varchar(50) | Y | 类型标签 |
| extra | json | N | 额外字段 |
| del_flag | tinyint | Y | 是否删除 |

### wf_formdef（表单表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | varchar(32) | Y | uuid |
| name | varchar(50) | Y | 表单名称 |
| remark | varchar(100) | N | 备注 |
| enable | tinyint | N | 是否启用：0-禁用；1-启用 |
| form_type | tinyint  | Y | 表单类型（1-业务表单，2-自定义表单） |
| extra | json | N | 表单配置 |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 删除时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |

### wf_form_item（表单字段表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | varchar(32) | Y | uuid |
| form_id | varchar(32) | Y | 表单ID |
| form_key | varchar(100) | Y | 表单字段KEY |
| label | varchar(100) | Y | 标题 |
| type | varchar(100) | Y | 表单类型 |
| required | tinyint | Y | 是否必填：0-否，1-是 |
| fixed | tinyint | Y | 是否固定（无法删除）：0-否，1-是 |
| extra | json | Y | 额外字段 |
| del_flag | tinyint | Y | 是否删除 |

### wf_re_procdef（流程定义表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | varchar(32) | Y | uuid |
| deploy_id | varchar(100) | Y | activiti部署ID |
| deploy_type | varchar(50) | Y | 流程类型（1-业务流程，2-自定义流程） |
| name | varchar(50) | Y | 名称 |
| remark | varchar(255) | N | 备注 |
| apply_range | tinyint | Y | 申请人范围：0-全体员工，1-指定员工 |
| icon | varchar(100) | N | 图标 |
| enable | tinyint | Y | 是否启用：0-未启用，1-启用 |
| act_version | varchar(100) | Y | 激活版本 |
| wo_id | uuid | N | 联动工单ID |
| wo_name | varchar(100) | N | 联动工单名称 |
| service_name | varchar(100) | N | 服务名 |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 删除时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |

### wf_re_procdef_staff（流程员工表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| procdef_id | varchar(32) | Y | 流程定义ID |
| staff_id | int | Y | 员工ID |

### wf_re_procdef_version（流程版本表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | varchar(32) | Y | uuid |
| tag_id | varchar(32) | Y | 节点ID |
| procdef_id | varchar(32) | Y | 流程定义ID |
| form_id | varchar(32) | Y | 表单ID |
| version | varchar(20) | Y | 版本号 |
| remark | varchar(100) | N |备注 |
| source_code | longtext | Y | BPMN源码 |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 删除时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |

### wf_re_procdef_node（流程节点表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | varchar(32) | Y | uuid |
| tag_id | varchar(32) | Y | 节点ID |
| version_id | varchar(32) | Y |版本ID|
| name | varchar(50) | N | 节点名称 |
| documentation | varchar(255) | N | 节点描述 |
| tagName | varchar(50) | N | 节点描述 |
| default_flow | varchar(32) | N | 连线默认分支 |
| candidate_type | varchar(20) | N | 候选类型 |
| candidate_value | json | N | 候选值 |
| assignee_is_null | varchar(20) | N | 审批人为空处理（字典数据） |
| assignee_value | varchar(20) | N | 转交指定人员 |
| expire_time | int | N | 审批时限（分） |
| reject_type | varchar(20) | N | 如果审批被驳回 |

### wf_re_procdef_line（流程连线表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | varchar(32) | Y | uuid |
| version_id | varchar(32) | Y |版本ID|
| name | varchar(50) | Y | 节点名称 |
| documentation | varchar(255) | Y | 节点描述 |
| tagName | varchar(50) | Y | 节点描述 |
| source_ref | varchar(32) | Y | 来源节点ID |
| target_ref | varchar(32) | Y | 目标节点ID |
| condition_type | varhcar(20) | Y | 条件类型 |
| expression | json | N | 表达式 |

### wf_ru_process（流程运行表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | 主键ID，自增长 |
| staff_id | int | Y | 申请人id |
| employee_no | varchar(30) | Y | 员工编号 |
| staff_name | varchar(50) | Y | 员工姓名 |
| mobile | varchar(50) | Y | 手机号 |
| dept_id | int | Y | 部门ID |
| dept_name | varchar(255) | Y | 部门 |
| rank_id | int | Y | 职级ID |
| rank_name | varchar(20) | Y | 职级名称 | 
| proc_def_key | varchar(100) | Y | 流程定义KEY |
| proc_def_name | varchar(100) | Y | 流程名称 |
| proc_version | varchar(20) | Y | 流程版本 |
| proc_inst_id | varchar(100) | Y | 流程实例ID |
| form_id | varchar(32) | Y | 流程实例ID |
| apply_status | int | Y | 申请状态（0-审核中、1-通过、2-关闭） |
| close_message | varchar(255) | N | 关闭消息 |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 删除时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |

### wf_hi_task（流程运行记录表）
| id | int | Y | 主键ID，自增长 |
| - | - | - | - |
| process_id | int | Y | 流程ID |
| task_id | int | Y | 流程ID |
| staff_id | int | Y | 审批人ID |
| mis_code | varchar(30) | Y | 审批人mis_code |
| category | varchar(30) | Y | 节点类型 |
| approval_name | varchar(30) | Y | 审批人姓名 |
| approval_status | int | Y | 审批状态 1同意 0 拒绝 |
| approval_text | varchar(255) | Y | 审批意见 |
| create_time | datetime | Y | 审批时间 |

## 二、接口

### 1. 表单管理

#### 1.1 查询表单（分页）

| 地址 | /api/bmpworkflow/form/page |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| pageNum | Integer | query | 页数 |
| pageSize | Integer | query | 每页条数 |
| name | String | query | 名称（模糊查询） |

请求示例：
```
GET /api/bmpworkflow/form/page?pageNum=1&pageSize=10
```

返回数据：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | String | 表单ID |
| name | String | 表单名称 |
| remark | String | 备注 |
| createTime | String | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime | String | 更新时间（yyyy-MM-dd HH:mm:ss） |
| operator | String | 操作人 |
| del_flag | Integer | 删除标记 |

#### 1.2 创建表单

| 地址 | /api/bmpworkflow/form |
| - | - |
| 请求方式 | POST |

参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| name | String | body | 页数 |
| remark | Integer | body | 每页条数 |

请求示例：
```
POST /api/bmpworkflow/form

{
	"name": "test",
	"remark" : "test"
}
```

#### 1.3 删除表单

| 地址 | /api/bmpworkflow/form/{id} |
| - | - |
| 请求方式 | DELETE |

参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| id | String | path | 表单ID |

请求示例：
```
DELETE /api/bmpworkflow/form/165d3bb58d164b43d3b191e3d609e4b4
```

#### 1.4 查询表单详情

| 地址 | /api/bmpworkflow/form/{id} |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| id | String | path | 表单ID |

请求示例：
```
GET /api/bmpworkflow/form/165d3bb58d164b43d3b191e3d609e4b4
```

返回数据：
| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | String | 表单ID |
| name | String | 表单名称 |
| remark | String | 备注 |
| extra | json | 表单配置 |
| formItem | List | 表单字段 |
| -- id | String | uuid |
| -- formId | String | 表单ID |
| -- formKey | String | 表单KEY |
| -- label | String | label |
| -- type | String | 类型 |
| -- required | Integer | 是否必填 |
| -- fixed | Integer | 是否固定 |
| -- extra | Object | 额外字段 |

#### 1.5 编辑表单

| 地址 | /api/bmpworkflow/form/{formId} |
| - | - |
| 请求方式 | PUT |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| formId | String | path | 表单ID |
| name | String | body | 表单名称 |
| remark | String | body | 备注 |
| extra | json | body |表单配置|
| formItem | List | body | 表单字段 |
| -- formKey | String | formItem | 表单KEY |
| -- category | String | formItem | 种类 |
| -- label | String | formItem | label |
| -- type | String  | formItem | 类型 |
| -- required | Integer | formItem | 是否必填：0-否，1-是 |
| -- extra | Object  | formItem | 额外字段 |

请求示例：

```json
POST /api/bmpworkflow/form/165d3bb58d164b43d3b191e3d609e4b4

{
	"name": "test",
	"remark": "test",
    "config": "",
	"formItem": [
		{
			"formKey": "f_hq4f49af5",
			"label": "姓名",
			"type": "input",
			"required": 1,
			"extra": {}
		}
	]
}
```

#### 1.6 查询表单列表

| 地址 | /api/bmpworkflow/form |
| - | - |
| 请求方式 | GET |

请求参数：
| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| enable | Integer | query | 是否启用：0-未启用；1-启用 |

返回数据：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | String | 表单ID |
| name | String | 表单名称 |
| remark | String | 备注 |
| createTime | String | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime | String | 更新时间（yyyy-MM-dd HH:mm:ss） |
| operator | String | 操作人 |
| del_flag | Integer | 删除标记 |

#### 1.7 启用/禁用表单

| 地址 | /api/bmpworkflow/form/{id}/status |
| - | - |
| 请求方式 | PUT |

请求参数：
| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| id | String | path | 表单ID |
| enable | Integer | query | 是否启用：0-未启用；1-启用 |

请求示例：
```
PUT /api/bmpworkflow/form/165d3bb58d164b43d3b191e3d609e4b4/status?enable=1
```

### 2. 流程管理

#### 2.1 流程定义查询（分页）

| 地址 | /api/bmpworkflow/process/def/page |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| pageNum | Integer | query | 页数 |
| pageSize | Integer | query | 每页条数 |
| deployType | String | query | 名称（模糊查询） |

请求示例：
```
GET /api/bmpworkflow/form/process/def?pageNum=1&pageSize=10
```

返回数据：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | String | 表单ID |
| deployId | String | 部署ID |
| deployType | Integer | 流程类型 |
| deployTypeName | String | 流程类型名称 |
| name | String | 流程名称 | 
| remark | String | 流程备注 |
| applyRange | Integer | 申请人范围 |
| icon | String | 图标 |
| enable | Integer | 是否启用：0-未启用，1-启用 |
| versionId | String | 激活版本ID |
| versionName | String | 激活版本名称 |
| wo_id | String | 工单ID |
| woName | String | 工单名称 |
| createTime | String | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime | String | 更新时间（yyyy-MM-dd HH:mm:ss） |
| operator | String | 操作人 |
| del_flag | Integer | 删除标记 |

#### 2.2 创建流程

| 地址 | /api/bmpworkflow/process/def |
| - | - |
| 请求方式 | POST |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| name | String | body | 流程名称 |
| remark | String | body | 流程备注 |
| icon | String | body |图标|
| applyRange | Integer | body | 申请人范围：0-全体员工，1-指定员工 |
| staffIds | List | body | 申请人ID |

请求示例：
```json
POST /api/bmpworkflow/process/def

{
	"name": "test",
	"remark": "test",
	"icon": "edit",
	"applyRange": 1,
	"staffIds": [123, 124, 125, 126]
}
```
#### 2.3 查询流程详情

| 地址 | /api/bmpworkflow/process/def/{id} |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| name | String | body | 流程名称 | 

请求示例：

```
GET /api/bmpworkflow/process/def/8FB7595B395D032F5925CB154EE3A62D
```

返回数据：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | String | 表单ID |
| deployId | String | 部署ID |
| deployType | Integer | 流程类型 |
| deployTypeName | String | 流程类型名称 |
| name | String | 流程名称 | 
| remark | String | 流程备注 |
| applyRange | Integer | 申请人范围 |
| icon | String | 图标 |
| enable | Integer | 是否启用：0-未启用，1-启用 |
| versionId | String | 激活版本ID |
| versionName | String | 激活版本名称 |
| wo_id | String | 工单ID |
| woName | String | 工单名称 |
| createTime | String | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime | String | 更新时间（yyyy-MM-dd HH:mm:ss） |
| operator | String | 操作人 |
| del_flag | Integer | 删除标记 |
| allowStaffList | List | 允许申请员工 |
| -- id | Integer | 员工ID |
| -- fldName | String | 员工姓名 |
| -- employeeNo | String | 员工编号 |
| -- mobile | String | 手机号 |
| -- deptNames  | List | 部门 | 

#### 2.4 编辑流程信息

| 地址 | /api/bmpworkflow/process/def/{id} |
| - | - |
| 请求方式 | PUT |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| id | String | path | 流程ID |
| name | String | body | 流程名称 |
| remark | String | body | 流程备注 |
| icon | String | body |图标|
| applyRange | Integer | body | 申请人范围：0-全体员工，1-指定员工 |
| staffIds | List | body | 申请人ID |
| enable | Integer | body |是否启用：0-未启用，1-启用|

请求示例：
```json
POST /api/bmpworkflow/process/def/8FB7595B395D032F5925CB154EE3A62D

{
	"name": "test",
	"remark": "test",
	"icon": "edit",
	"applyRange": 1,
	"staffIds": [123, 124, 125, 126],
	"enable": 1
}
```

#### 2.5 删除流程

| 地址 | /api/bmpworkflow/process/def/{id} |
| - | - |
| 请求方式 | DELETE |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| id | String | path | id |

请求示例：
```
DELETE /api/bmpworkflow/process/def/8FB7595B395D032F5925CB154EE3A62D
```

#### 2.6 查询版本列表

| 地址 | /api/bmpworkflow/process/{processId}/version |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| processId | String | path | 流程ID |

请求示例：
```
GET /api/bmpworkflow/process/8FB7595B395D032F5925CB154EE3A62D/version
```

返回数据：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | String | 版本ID |
| version | String | 版本号 |
| remark | String | 备注 |
| createTime | String | 创建时间（yyyy-MM-dd HH:mm:ss）|
| updateTime | String |更新时间（yyyy-MM-dd HH:mm:ss）|
| operator | String |操作人|

#### 2.7 创建版本

| 地址 | /api/bmpworkflow/process/{processId}/version |
| - | - |
| 请求方式 | POST |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| processId | String | path | 流程ID |
| formId | String | body | 表单ID |
| version | String | body | 版本号 |
| remark | String | body | 备注 |

请求示例：
```json
POST /api/bmpworkflow/process/8FB7595B395D032F5925CB154EE3A62D/version

{
    "formId": "F5925395D032",
	"version": "0.0.1",
	"remark": "默认版本",
}
```

#### 2.8 查询版本详情

| 地址 | /api/bmpworkflow/process/{processId}/version/{versionId} |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| processId | String | path | 流程ID |
| versionId | String | path | 版本ID |

请求示例：
```
GET /api/bmpworkflow/process/8FB7595B395D032F5925CB154EE3A62D/version/8FB7595B395D032F5925CB154EE3A62D
```
返回数据：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | String | uuid |
| procdef_id | String | 流程定义ID |
| version | String | 版本号 |
| remark | String | N |
| source_code | String | BPMN源码 |
| create_time | String | 创建时间（yyyy-MM-dd HH:mm:ss） |
| update_time | String | 删除时间（yyyy-MM-dd HH:mm:ss） |
| operator | String | 操作人 |
| del_flag | tinyint | 是否删除 |

#### 2.9 编辑版本信息

| 地址 | /api/bmpworkflow/process/{processId}/version/{versionId} |
| - | - |
| 请求方式 | PUT |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| processId | String | path | 流程ID |
| versionId | String | path | 版本ID |
| version | String | body | 版本号 |
| remark | String | body | 备注 |
| formId | String | body | 关联表单ID |
| nodes | List | body | 节点 |
| -- tagId | String | nodes | 节点ID |
| -- name | String | nodes | 节点名称 |
| -- documentation | String | nodes | 节点描述 |
| -- tagName | String | nodes | 节点tag |
| -- defaultFlow | String | nodes | 默认分支 |
| -- candidateType | String | nodes | 审批人候选类型（字典数据） |
| -- candidateValue | String | nodes | 审批人候选值（字典数据） |
| -- assigneeIsNull | String | nodes | 审批人为空（字典数据） |
| -- assigneeValue | String | nodes | 转交指定人员 |
| -- expireTime | String | nodes | 审批时限（字典数据） |
| -- rejectType | String | nodes | 如果审批被驳回（字典数据） |
| lines | List | body | 连线 |
| -- tagId | String | lines | 节点ID |
| -- name | String | lines | 节点名称 |
| -- documentation | lines | nodes | 节点描述 |
| -- tagName | String | lines | 节点tag |
| -- sourceRef | String | lines | 来源节点ID |
| -- targetRef | String | lines | 目标节点ID |
| -- conditionType | String | lines | 条件类型（字典数据） |
| -- expression | String | lines | 表达式 |
| sourceCode | String | body | 流程源码 |

请求示例：
```json
PUT /api/bmpworkflow/process/8FB7595B395D032F5925CB154EE3A62D/version/8FB7595B395D032F5925CB154EE3A62D

{
	"version": "0.0.1",
	"remark": "默认版本",
    "formId": "8FB7595B395D032F5925CB154EE3A62D",
	"sourceCode": "xml",
	"nodes": [{
		"tagId": "usertask1",
		"name": "审批节点1",
		"documentation": "",
		"tagName": "userTask",
		"candidateType": "fixed",
		"candidateValue": "[{\"id\":13164,\"label\":\"郑悦来\"}]",
		"assigneeIsNull": "auto_reject",
		"expireTime": 60,
		"rejectType": "close"
	}],
	"lines": [{
		"tagId": "flow1",
		"name": "审批节点1",
		"documentation": "",
		"tagName": "squenceFlow",
		"sourceRef": "startevent1",
		"targetRef": "usertask1",
		"conditionType": "formItem",
		"expression": "{\"oper\":\"==\",\"formKey\":\"fd_202031321516\",\"value\":10}"
	}]
}
```

candidateValue审批人候选类型请求样例

```javascript
// 指定人
candidateType = "fixed"
candidateValue = "[{\"id\":13164,\"label\":\"郑悦来\"}]", // 员工ID数组
// 发起人自选
candidateType = "self_select"
candidateValue = null
// 系统角色
candidateType = "role"
candidateValue = ["EJIDLbojDLFJdfkld"] // 角色ID数组
// 园区主管
candidateType = "supervisor"
candidateValue = "fd_20303151613213" // 表单字段ID
// 职级
candidateType = "rank"
candidateValue = [1,2,3,4] // 职级ID数组
// 员工所在部门某职级
candidateType = "self_dept_rank"
candidateValue = [1,2,3,4] // 职级ID数组
// 员工上级部门某职级
candidateType = "top_dept_rank"
candidateValue = [1,2,3,4] // 职级ID数组
```

expression条件类型请求样例

```javascript
// 表单字段类型
conditionType = "formItem"
expression = "{\"oper\":\"==\",\"formKey\":\"fd_202031321516\",\"value\":10}" // oper-运算符；formKey-表单字段KEY
// 发起人 属于某职级
conditionType = "belong_rank"
expression = [1,2] // 职级ID数组
// 发起人 属于某部门
conditionType = "belong_dept"
expression = [1,2] // 部门ID数组
// 发起人 属于某些人其中之一
conditionType = "belong_some_one"
expression = [1,2,3] // 员工ID数组
// 其它条件都不满足
conditionType = "else"
exporession = null
```



#### 2.10 删除版本

| 地址 | /api/bmpworkflow/process/{processId}/version/{versionId} |
| - | - |
| 请求方式 | DELETE |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| processId | String | path | 流程ID |
| versionId | String | path | 版本ID |

请求示例：
```
DELETE /api/bmpworkflow/process/8FB7595B395D032F5925CB154EE3A62D/version/8FB7595B395D032F5925CB154EE3A62D
```

#### 2.11 部署版本

| 地址 | /api/bmpworkflow/process/{processId}/version/{versionId}/deploy |
| - | - |
| 请求方式 | PUT |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| processId | String | path | 流程ID |
| versionId | String | path | 版本ID |

请求示例：
```
PUT /api/bmpworkflow/process/8FB7595B395D032F5925CB154EE3A62D/version/8FB7595B395D032F5925CB154EE3A62D/deploy
```
#### 2.12 查询流程列表
| 地址 | /api/bmpworkflow/process/def |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名    | 类型   | 位置 | 说明   |
| --------- | ------ | ---- | ------ |
| enable | Integer | query | 是否启用：0-未启用；1-启用 |

请求示例：
```
GET /api/bmpworkflow/process/def?enable=1
```

返回示例：
```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": [
        {
            "delFlag": 0,
            "createTime": "2023-04-14 09:44:00",
            "updateTime": "2023-04-14 09:48:43",
            "operator": "张元",
            "id": "4f1a33965195a054fc9876a9a206220f",
            "deployId": "Proc_4f1a33965195a054fc9876a9a206220f",
            "deployType": "2",
            "name": "办理凭证",
            "remark": "",
            "applyRange": 0,
            "icon": "",
            "enable": 0,
            "actVersion": "5aa0f295d7bc8d1dcfe02e13aa9393aa",
            "woId": null,
            "woName": null,
            "serviceName": null
        }
    ],
    "success": true
}
```
#### 2.13 启用/禁用流程

| 地址 | /api/bmpworkflow/process/def/{id}/status |
| - | - |
| 请求方式 | PUT |

请求参数：
| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| id | String | path | 流程ID |
| enable | Integer | query | 是否启用：0-未启用；1-启用 |

请求示例：
```
PUT /api/bmpworkflow/process/def/165d3bb58d164b43d3b191e3d609e4b4/status?enable=1
```


### 3. 申请&处理

#### 3.1 查询流程定义

| 地址 | /api/bmpworkflow/process/def/{procdefId}/active |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| procdefId | String | path | 流程ID |

请求示例：
```
GET /api/bmpworkflow/process/def/8FB7595B395D032F5925CB154EE3A62D/active
```
返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | String | 表单ID |
| deployId | String | 部署ID |
| deployType | Integer | 流程类型 |
| deployTypeName | String | 流程类型名称 |
| name | String | 流程名称 |
| remark | String | 流程备注 |
| applyRange | Integer | 申请人范围 |
| icon | String | 图标 |
| enable | Integer | 是否启用：0-未启用，1-启用 |
| versionId | String | 激活版本ID |
| versionName | String | 激活版本名称 |
| wo_id | String | 工单ID |
| woName | String | 工单名称 |
| createTime | String | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime | String | 更新时间（yyyy-MM-dd HH:mm:ss） |
| operator | String | 操作人 |
| del_flag | Integer | 删除标记 |
| fromdef | Object | 表单信息 |
| -- id | String | 表单ID |
| -- name | String | 表单名称 |
| -- remark | String | 备注 |
| -- extra | json | 表单配置 |
| formItem | List | 表单字段 |
| -- id | String | uuid |
| -- formId | String | 表单ID |
| -- formKey | String | 表单KEY |
| -- label | String | label |
| -- type | String | 类型 |
| -- required | Integer | 是否必填 |
| -- fixed | Integer | 是否固定 |
| -- extra | Object | 额外字段 |

返回示例：
```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "delFlag": 0,
        "createTime": "2023-03-22 17:39:28",
        "updateTime": "2023-03-22 17:41:58",
        "operator": "张元",
        "id": "d501796275a9b9412f24292788bee1ce",
        "deployId": "myProcess",
        "deployType": "2",
        "name": "默认测试流程",
        "remark": null,
        "applyRange": 0,
        "icon": null,
        "enable": 0,
        "actVersion": "04563d50982be4fe3993bc408c933d29",
        "woId": null,
        "woName": null,
        "formdef": {
            "delFlag": 0,
            "createTime": "2023-03-27 15:45:08",
            "updateTime": "2023-03-28 17:29:26",
            "operator": "张元",
            "id": "756b977e45f029208acc1a8011bd295d",
            "name": "测试工单表单",
            "remark": "",
            "enable": 0,
            "formType": 2,
            "extra": {
                "gutter": 15,
                "formRef": "elForm",
                "size": "medium",
                "formRules": "rules",
                "labelPosition": "right",
                "dynamicTableAllowed": true,
                "rules": "rules",
                "disabled": false,
                "labelWidth": 80,
                "formModel": "form",
                "version": "1.10"
            },
            "formTypeName": null
        },
        "formItems": [
            {
                "delFlag": 0,
                "id": "e4c3da818e7804848473aa3e5bfd0e0e",
                "formId": "756b977e45f029208acc1a8011bd295d",
                "formKey": "fd_1679903111832",
                "label": "单行文本",
                "type": "el-input",
                "required": 0,
                "fixed": null,
                "sort": 0,
                "extra": {
                    "prepend": "",
                    "rulesType": "default",
                    "rules": [],
                    "labelWidth": 80,
                    "compName": "单行文本",
                    "prefix-icon": "",
                    "required": false,
                    "showLabel": true,
                    "compType": "input",
                    "readonly": false,
                    "disabled": false,
                    "id": "fd_1679903111832",
                    "placeholder": "请输入文本",
                    "value": "",
                    "compIcon": "input",
                    "suffix-icon": "",
                    "ele": "el-input",
                    "clearable": true,
                    "label": "单行文本",
                    "layout": "colItem",
                    "gutter": 15,
                    "width": "100%",
                    "viewType": "text",
                    "_id": "fd_1679903111832",
                    "config": true,
                    "append": "",
                    "maxLength": 50,
                    "span": 24,
                    "status": "normal"
                }
            }]
    },
    "success": true
}
```

#### 3.2 查询审批人

| 地址 | /api/bmpworkflow/task/candidate/{key} |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| key | String | path | 流程DeployId |
| processId | Integer | Query | 流程ID（非必填） |
| variables | Object | Body | 表单变量 |

请求示例：
```json
GET /api/bmpworkflow/task/candidate/myProcess

{
	"fd_1679903117649": 1, // key: formKey, value: 表单字段值
	...
}
```
返回示例：
```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": [
        {
            "name": "主管审批", // 节点名称
            "formKey": "usertask2_assignee", // 变量KEY
            "candidates": [  // 候选人
                {
                    "id": 13164,  // 员工ID
                    "staffName": "郑悦来",  // 员工名称
                    "deptName": "北京XXX网络科技有限公司"  // 员工部门
                }
            ]
        }
    ],
    "success": true
}
```
#### 3.3 提交申请

| 地址 | /api/bmpworkflow/task/submit/{key} |
| - | - |
| 请求方式 | POST |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| key | String | path | 流程DeployId |
| variables | Object | Body | 表单变量 |

请求示例：
```json
POST /api/bmpworkflow/task/submit/myProcess

{
	"fd_1679903117649": 1, // key: formKey, value: 表单字段值
    "fd_1679903117649_label": "姓名" // {formKey}_label，表单字段描述
	...
}
```

#### 3.4 完成任务

| 地址 | /api/bmpworkflow/task/complete/{taskId} |
| - | - |
| 请求方式 | POST |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| taskId | String | path | 任务ID |
| approvalText | String | Body | 审批描述 |
| variables | Object | Body | 表单变量 |

请求示例：
```json
POST /api/bmpworkflow/task/complete/14025

{} // 暂时传空
```

#### 3.4 驳回审批

| 地址 | /api/bmpworkflow/task/reject/{taskId} |
| - | - |
| 请求方式 | POST |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| taskId | String | path | 任务ID |
| approvalText | String | Body | 审批描述 |
| rejectType | String | Body | 驳回类型（字典：rejectType） |
| targetId | String | Body | 目标节点ID，非必填，当rejectType为自选节点时需传递 |

请求示例：
```javascript
POST /api/bmpworkflow/task/reject/14025

{
    "approvalText": "驳回请求"
	"rejectType": "close",
	"targetId": null
}
```

#### 3.5 任务详情

| 地址 | /api/bmpworkflow/task/variables/{taskId} |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| taskId | String | path | 任务ID |


请求示例：
```
POST /api/bmpworkflow/task/variables/14052
```
返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| formValues | Object | 表单字段值：key-formKey；value-表单字段值 |
| taskdef | Object | 任务定义（仅执行中任务存在此内容） |
| -- id | String | 节点UUID |
| -- tagId | String | 节点ID |
| -- versionId | String | 版本ID |
| -- name | String | 节点名称 |
| -- documentation | String | 描述 |
| -- tagName | String | 节点类型 |
| -- defaultFlow | String | 默认分支 |
| -- candidateType | String | 候选类型 |
| -- candidateValue | String | 候选值 |
| -- assigneeIsValue | String | 审批人为空 |
| -- expireTime | int | 审批时限（分） |
| -- rejectType | String | 如果审批被驳回 |
| fromdef | Object | 表单信息 |
| -- id | String | 表单ID |
| -- name | String | 表单名称 |
| -- remark | String | 备注 |
| -- extra | json | 表单配置 |
| formItem | List | 表单字段 |
| -- id | String | uuid |
| -- formId | String | 表单ID |
| -- formKey | String | 表单KEY |
| -- label | String | label |
| -- type | String | 类型 |
| -- required | Integer | 是否必填 |
| -- fixed | Integer | 是否固定 |
| -- extra | Object | 额外字段 |
| tasks | List | 任务执行列表 |
| -- id | Integer | 记录ID |
| -- processId | Integer | 流程ID |
| -- taskId | String | 任务ID |
| -- staffId | Integer | 员工ID |
| -- employeeNo | String | 员工编号 |
| -- category | String | 节点类型 |
| -- approvalName | String | 节点名称 |
| -- approvalStatus | Integer | 状态：-1: 审批中；0: 拒绝；1: 同意 |
| -- approvalText | String | 审批意见 |
| -- createTime | String | 操作时间（yyyy-MM-dd HH:mm:ss） |
| -- operator | String | 操作人 |

返回示例：

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "formValues": {
            "fd_1679903117649": "22",
            "fd_1679903127477": [],
            "fd_1679972677316": "12",
            "fd_1679903124172": "",
            "fd_1679903115718": [],
            "fd_1679903120312": "00:03",
            "fd_1679903113280": 1,
            "fd_1679903114569": 1,
            "fd_1679903111832": "111"
        },
        "formItems": [
            {
                "delFlag": 0,
                "id": "e4c3da818e7804848473aa3e5bfd0e0e",
                "formId": "756b977e45f029208acc1a8011bd295d",
                "formKey": "fd_1679903111832",
                "label": "单行文本",
                "type": "el-input",
                "required": 0,
                "fixed": null,
                "sort": 0,
                "extra": {
                    "prepend": "",
                    "rulesType": "default",
                    "rules": [],
                    "labelWidth": 80,
                    "compName": "单行文本",
                    "prefix-icon": "",
                    "required": false,
                    "showLabel": true,
                    "compType": "input",
                    "readonly": false,
                    "disabled": false,
                    "id": "fd_1679903111832",
                    "placeholder": "请输入文本",
                    "value": "",
                    "compIcon": "input",
                    "suffix-icon": "",
                    "ele": "el-input",
                    "clearable": true,
                    "label": "单行文本",
                    "layout": "colItem",
                    "gutter": 15,
                    "width": "100%",
                    "viewType": "text",
                    "_id": "fd_1679903111832",
                    "config": true,
                    "append": "",
                    "maxLength": 50,
                    "span": 24,
                    "status": "normal"
                }
            }
        ],
        "formdef": {
            "delFlag": 0,
            "createTime": "2023-03-27 15:45:08",
            "updateTime": "2023-03-28 17:29:26",
            "operator": "张元",
            "id": "756b977e45f029208acc1a8011bd295d",
            "name": "测试工单表单",
            "remark": "",
            "enable": 0,
            "formType": 2,
            "extra": {
                "gutter": 15,
                "formRef": "elForm",
                "size": "medium",
                "formRules": "rules",
                "labelPosition": "right",
                "dynamicTableAllowed": true,
                "rules": "rules",
                "disabled": false,
                "labelWidth": 80,
                "formModel": "form",
                "version": "1.10"
            },
            "formTypeName": null
        },
        "tasks": [
            {
                "delFlag": 0,
                "id": 16,
                "processId": 9,
                "taskId": null,
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "category": "start",
                "approvalName": "提交",
                "approvalStatus": 1,
                "approvalText": null,
                "createTime": "2023-03-29 11:34:39",
                "operator": "郑悦来"
            },
            {
                "delFlag": null,
                "id": null,
                "processId": null,
                "taskId": "47543",
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "category": null,
                "approvalName": "主管审批",
                "approvalStatus": -1,
                "approvalText": null,
                "createTime": null,
                "operator": "郑悦来"
            }
        ]
    },
    "success": true
}
```

### 4. 我的申请

#### 4.1 我的申请

| 地址 | /api/bmpworkflow/process/run/page |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| key | String | query | 流程DeployId |
| pageNum | Integer | query  | 页数 |
| pageSize | String | query  | 每页条数 |

返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | Integer | ID |
| staffId | Integer | 员工ID |
| employeeNo | String | 员工编号 |
| staffName | String | 员工姓名 |
| mobile | String | 手机号 |
| deptId | Integer | 部门ID |
| deptName | String | 部门名称 |
| rankId | Integer | 职级ID |
| rankName | String | 职级名称 |
| procDefKey | String | 申请流程KEY |
| procDefName | String| 申请流程名称 |
| procVersion | String | 申请流程版本 |
| procInstId | String | 流程实例ID |
| formId | String | 表单ID |
| applyStatus | Integer | 申请状态 |
| applyStatusName | String | 申请状态描述 |
| closeMessage | String | 流程结束描述 |
| createTime | String | 申请时间（yyyy-MM-dd HH:mm:ss） |
| updateTime | string | 更新时间（yyyy-MM-dd HH:mm:ss） |


返回示例：
```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "records": [
            {
                "delFlag": 0,
                "createTime": "2023-03-28 15:26:22",
                "updateTime": "2023-03-28 15:26:22",
                "operator": "郑悦来",
                "id": 2,
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "staffName": "郑悦来",
                "mobile": "15510524531",
                "deptId": 1517,
                "deptName": "北京XXX网络科技有限公司",
                "rankId": 1,
                "rankName": "普通员工",
                "procDefKey": "myProcess",
                "procDefName": "默认测试流程",
                "procVersion": null,
                "procInstId": "20025",
                "formId": "756b977e45f029208acc1a8011bd295d",
                "applyStatus": 0,
                "closeMessage": null,
                "applyStatusName": null
            }
        ],
        "total": 6,
        "size": 10,
        "current": 1,
        "orders": [],
        "optimizeCountSql": true,
        "hitCount": false,
        "countId": null,
        "maxLimit": null,
        "searchCount": true,
        "pages": 1
    },
    "success": true
}
```

#### 4.2 申请详情

| 地址 | /api/bmpworkflow/process/run/{id} |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| id | String | path | 流程ID |

请求示例：
```
GET  /api/bmpworkflow/process/run/8FB7595B395D032F5925CB154EE3A62D
```

返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| formValues | Object | 表单字段值：key-formKey；value-表单字段值 |
| fromdef | Object | 表单信息 |
| -- id | String | 表单ID |
| -- name | String | 表单名称 |
| -- remark | String | 备注 |
| -- extra | json | 表单配置 |
| formItem | List | 表单字段 |
| -- id | String | uuid |
| -- formId | String | 表单ID |
| -- formKey | String | 表单KEY |
| -- label | String | label |
| -- type | String | 类型 |
| -- required | Integer | 是否必填 |
| -- fixed | Integer | 是否固定 |
| -- extra | Object | 额外字段 |
| tasks | List | 任务执行列表 |
| -- id | Integer | 记录ID |
| -- processId | Integer | 流程ID |
| -- taskId | String | 任务ID |
| -- staffId | Integer | 员工ID |
| -- employeeNo | String | 员工编号 |
| -- category | String | 节点类型 |
| -- approvalName | String | 节点名称 |
| -- approvalStatus | Integer | 状态：-1: 审批中；0: 拒绝；1: 同意 |
| -- approvalText | String | 审批意见 |
| -- createTime | String | 操作时间（yyyy-MM-dd HH:mm:ss） |
| -- operator | String | 操作人 |

返回示例：
```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "formValues": {
            "fd_1679903117649": "22",
            "fd_1679903127477": [],
            "fd_1679972677316": "12",
            "fd_1679903124172": "",
            "fd_1679903115718": [],
            "fd_1679903120312": "00:03",
            "fd_1679903113280": 1,
            "fd_1679903114569": 1,
            "fd_1679903111832": "111"
        },
        "process": {
            "delFlag": 0,
            "createTime": "2023-03-29 13:29:26",
            "updateTime": "2023-03-29 13:29:27",
            "operator": "郑悦来",
            "id": 10,
            "staffId": 13164,
            "employeeNo": "E0000123456456645",
            "staffName": "郑悦来",
            "mobile": "15510524531",
            "deptId": 1517,
            "deptName": "北京XXX网络科技有限公司",
            "rankId": 1,
            "rankName": "普通员工",
            "procDefKey": "myProcess",
            "procDefName": "默认测试流程",
            "procVersion": "04563d50982be4fe3993bc408c933d29",
            "procInstId": "50001",
            "formId": "756b977e45f029208acc1a8011bd295d",
            "applyStatus": 0,
            "closeMessage": null,
            "applyStatusName": null
        },
        "formItems": [],
        "formdef": {
            "delFlag": 0,
            "createTime": "2023-03-27 15:45:08",
            "updateTime": "2023-03-28 17:29:26",
            "operator": "张元",
            "id": "756b977e45f029208acc1a8011bd295d",
            "name": "测试工单表单",
            "remark": "",
            "enable": 0,
            "formType": 2,
            "extra": {
                "gutter": 15,
                "formRef": "elForm",
                "size": "medium",
                "formRules": "rules",
                "labelPosition": "right",
                "dynamicTableAllowed": true,
                "rules": "rules",
                "disabled": false,
                "labelWidth": 80,
                "formModel": "form",
                "version": "1.10"
            },
            "formTypeName": null
        },
        "tasks": [
            {
                "delFlag": 0,
                "id": 17,
                "processId": 10,
                "taskId": null,
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "category": "start",
                "approvalName": "提交",
                "approvalStatus": 1,
                "approvalText": null,
                "createTime": "2023-03-29 13:29:27",
                "operator": "郑悦来"
            },
            {
                "delFlag": 0,
                "id": 18,
                "processId": 10,
                "taskId": "50043",
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "category": null,
                "approvalName": "主管审批",
                "approvalStatus": 1,
                "approvalText": null,
                "createTime": "2023-03-29 14:37:46",
                "operator": "郑悦来"
            },
            {
                "delFlag": null,
                "id": null,
                "processId": null,
                "taskId": "50045",
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "category": null,
                "approvalName": "领导审批",
                "approvalStatus": -1,
                "approvalText": null,
                "createTime": null,
                "operator": "郑悦来"
            }
        ]
    },
    "success": true
}
```
#### 4.3 取消申请

| 地址 | /api/bmpworkflow/process/run/{id} |
| - | - |
| 请求方式 | PUT |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| id | String | path | 流程ID |

请求示例：
```
PUT  /api/bmpworkflow/process/run/8FB7595B395D032F5925CB154EE3A62D
```

### 5. 我的审批

#### 5.1 待办审批

| 地址 | /api/bmpworkflow/task/my |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| deployKey | String | query | 流程DeployId |
| unifyParams | String | query | 员工编码/姓名/手机号 |
| pageNum | Integer | query  | 页数 |
| pageSize | String | query  | 每页条数 |

请求示例：
```
GET /api/bmpworkflow/task/my?deployKey=myProcess&unifyParams=123&pageNum=1&pageSize=10
```
返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| taskId | Integer | ID |
| taskName | Integer | 节点名称                            |
| taskTime | String | 任务创建时间（yyyy-MM-dd HH:mm:ss） |
| staffId | Integer | 申请员工ID |
| employeeNo | String | 申请员工编号 |
| staffName | String | 申请员工姓名 |
| mobile | String | 手机号 |
| deptName | String | 部门名称 |
| submitTime | String | 申请提交时间（yyyy-MM-dd HH:mm:ss） |
| procdefName | String | 申请流程名称 |

返回示例：

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "records": [
            {
                "taskId": "50045",
                "taskName": "领导审批",
                "taskTime": "2023-03-29 14:37:45",
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "staffName": "郑悦来",
                "mobile": "15510524531",
                "deptName": "北京XXX网络科技有限公司",
                "submitTime": "2023-03-29 13:29:26",
                "procdefName": "默认测试流程"
            },
            {
                "taskId": "47543",
                "taskName": "主管审批",
                "taskTime": "2023-03-29 11:34:38",
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "staffName": "郑悦来",
                "mobile": "15510524531",
                "deptName": "北京XXX网络科技有限公司",
                "submitTime": "2023-03-29 11:34:39",
                "procdefName": "默认测试流程"
            }
        ],
        "total": 2,
        "size": 10,
        "current": 1,
        "orders": [],
        "optimizeCountSql": true,
        "hitCount": false,
        "countId": null,
        "maxLimit": null,
        "searchCount": true,
        "pages": 1
    },
    "success": true
}
```

#### 5.2 历史审批

| 地址 | /api/bmpworkflow/task/history |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| deployKey | String | query | 流程DeployId |
| unifyParams | String | query | 员工编码/姓名/手机号 |
| pageNum | Integer | query  | 页数 |
| pageSize | String | query  | 每页条数 |

请求示例：
```
GET /api/bmpworkflow/task/history?deployKey=myProcess&unifyParams=123&pageNum=1&pageSize=10
```

返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| taskId | Integer | ID |
| approvalName | Integer | 节点名称                            |
| taskTime | String | 任务创建时间（yyyy-MM-dd HH:mm:ss） |
| staffId | Integer | 申请员工ID |
| employeeNo | String | 申请员工编号 |
| staffName | String | 申请员工姓名 |
| mobile | String | 手机号 |
| deptName | String | 部门名称 |
| submitTime | String | 申请提交时间（yyyy-MM-dd HH:mm:ss） |
| procdefName | String | 申请流程名称 |
| approvalStatus | Integer | 审批状态 |
| approvalText | String | 审批意见 |

返回示例：
```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "records": [
            {
                "taskId": "50043",
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "staffName": "郑悦来",
                "mobile": "15510524531",
                "deptId": 1517,
                "deptName": "北京XXX网络科技有限公司",
                "approvalName": "主管审批",
                "approvalStatus": "1",
                "approvalText": null,
                "taskTime": "2023-03-29 14:37:46",
                "submitTime": "2023-03-29 13:29:26"
            }
        ],
        "total": 9,
        "size": 10,
        "current": 1,
        "orders": [],
        "optimizeCountSql": true,
        "hitCount": false,
        "countId": null,
        "maxLimit": null,
        "searchCount": true,
        "pages": 1
    },
    "success": true
}
```

### 6. 字典

#### 6.1 字典查询

| URL    | /api/bmpworkflow/dict/list/{groupCode} |
| ------ | -------------------------------------- |
| Method | GET                                    |

请求参数：

| 参数      | 类型   | 说明     |
| --------- | ------ | -------- |
| groupCode | string | 类型编码 |

返回的参数：

| 参数  | 类型   | 说明     |
| ----- | :----- | -------- |
| label | string | 字典名称 |
| value | string | 字典值   |

```json
响应
{
    "code": 0,
    "message": "请求成功！",
    "data": [
        {
        "label": "",
        "value": ""
    	},
        {
        "label": "",
        "value": ""
    	}
    ]
}
```

### 7. 审批监控

#### 7.1 审批列表查询

| 地址 | /api/bmpworkflow/process/run/monitor |
| - | - |
| 请求方式 | GET |

请求参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| key | String | query | 流程DeployId |
| unifyParams | String | query | 流程DeployId |
| status | Integer | path | 审批状态 |
| pageNum | Integer | query  | 页数 |
| pageSize | String | query  | 每页条数 |

返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| id | Integer | ID |
| staffId | Integer | 员工ID |
| employeeNo | String | 员工编号 |
| staffName | String | 员工姓名 |
| mobile | String | 手机号 |
| deptId | Integer | 部门ID |
| deptName | String | 部门名称 |
| rankId | Integer | 职级ID |
| rankName | String | 职级名称 |
| procDefKey | String | 申请流程KEY |
| procDefName | String| 申请流程名称 |
| procVersion | String | 申请流程版本 |
| procInstId | String | 流程实例ID |
| formId | String | 表单ID |
| applyStatus | Integer | 申请状态 |
| applyStatusName | String | 申请状态描述 |
| closeMessage | String | 流程结束描述 |
| createTime | String | 申请时间（yyyy-MM-dd HH:mm:ss） |
| updateTime | string | 更新时间（yyyy-MM-dd HH:mm:ss） |


返回示例：
```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "records": [
            {
                "delFlag": 0,
                "createTime": "2023-03-28 15:26:22",
                "updateTime": "2023-03-28 15:26:22",
                "operator": "郑悦来",
                "id": 2,
                "staffId": 13164,
                "employeeNo": "E0000123456456645",
                "staffName": "郑悦来",
                "mobile": "15510524531",
                "deptId": 1517,
                "deptName": "北京XXX网络科技有限公司",
                "rankId": 1,
                "rankName": "普通员工",
                "procDefKey": "myProcess",
                "procDefName": "默认测试流程",
                "procVersion": null,
                "procInstId": "20025",
                "formId": "756b977e45f029208acc1a8011bd295d",
                "applyStatus": 0,
                "closeMessage": null,
                "applyStatusName": null
            }
        ],
        "total": 6,
        "size": 10,
        "current": 1,
        "orders": [],
        "optimizeCountSql": true,
        "hitCount": false,
        "countId": null,
        "maxLimit": null,
        "searchCount": true,
        "pages": 1
    },
    "success": true
}
```