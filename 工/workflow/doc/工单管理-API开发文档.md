# 工单模块-API开发文档

## 字典数据
| group_code | label | value | extra |
| - | - | - | - |
| workOrderType | 服务运营 | 1 | |
| workOrderType | 日常巡检 | 2 | |
| workOrderType | 故障报修 | 3 | |
| workOrderType | 告警处置 | 4 | |
| workOrderType | 行政管理 | 5 | |
| workOrderType | 投诉工单 | 6 | |
| workOrderLevel | 特急 | 1 | |
| workOrderLevel | 紧急 | 2 | |
| workOrderLevel | 一般 | 3 | |
| woBusinessName | 智慧安防 | 1 | |
| woBusinessName | 智慧酒店 | 2 | |
| woBusinessName | 智慧餐饮 | 3 | |
| woBusinessName | 智慧管理 | 4 | |
| woBusinessName | 智慧设备设施 | 5 | |
| woTaskType | 抢单 | 1 | |
| woTaskType | 派单 | 2 | |
| woTaskRepeatType | 仅一次 | 1 | |
| woTaskRepeatType | 每天定时 | 2 | |
| woTaskRepeatType | 起止时间 | 3 | |
| woTaskAssigneeStatus | 已分配 | 1 | |
| woTaskAssigneeStatus | 已接单 | 2 | |
| woTaskAssigneeStatus | 转单中       | 3 | |
| woTaskAssigneeStatus | 已拒单 | 4 | |
| woTaskStatus | 已创建 | 0 | |
| woTaskStatus | 执行中 | 1 | |
| woTaskStatus | 已完成 | 2 | |
| woTaskStatus | 已验收 | 3 | |
| woTaskStatus | 指派已过期 | 4 | |
| woTaskStatus | 已取消 | 5 | |
| woTaskStatus | 已驳回 | 6 | |
| woTaskStatus | 工单异常 | 7 | |
| eodType | 休息 | 0 | |
| eodType | 白班 | 1 | |
| eodType | 夜班 | 2 | |
| woComplainStatus | 待反馈 | 0 | |
| woComplainStatus | 已反馈 | 1 | |
| strategyType | 用户自选 | 1 | |
| strategyType | 自动触发 | 2 | |

## 一、表结构

### wo_orderdef（工单配置表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | varchar(32) | Y | uuid |
| sequence_no | varchar(50) | Y | 工单编号 |
| dept_id | int | Y | 部门ID |
| dept_name | varchar(255) | Y | 部门名称（多级） |
| owner_id | int | Y | 部门主管员工ID |
| owner_name | varchar(100) | Y | 部门主管姓名 |
| name | varchar(100) | Y | 工单名称 |
| description | varchar(255) | N | 工单详述 |
| order_type | varchar(10) | Y | 工单类型（字典：workOrderType） |
| business_type | varchar(10) | Y | 业务模块（字典：woBusinessName） |
| order_level | varchar(10) | Y | 紧急程度（字典：workOrderLevel） |
| task_type | varchar(10) | Y | 接单类型（字典：woTaskType） |
| expire_time | int | Y | 过期时间（分钟）|
| form_id | varchar(32) | N | 表单ID |
| link_strategy | varchar(10) | Y | 联动策略（字典：strategyType） |
| trigger_id | varhcar(32) | N | 完成后触发工单ID |
| enable | tinyint | Y | 是否启用：0-禁用；1-启用 |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 更新时间 |
| operator | varchar(50) | N | 操作人 |
| switch | tinyint | Y | 开关（开：1 关：0） |
| del_flag | tinyint | Y | 是否删除 |

### wo_taskdef（任务定义表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | 任务实例ID（自增） |
| name | varchar(32) | Y | 工单任务名称 |
| wo_id | varchar(32) | Y | 工单配置ID |
| sequence_no | varchar(50) | Y | 工单编号 |
| order_type | varchar(10) | Y | 工单类型（字典：workOrderType） |
| business_type | varchar(10) | Y | 业务模块（字典：woBusinessName） |
| order_level | varchar(10) | Y | 紧急程度（字典：workOrderLevel） |
| task_type | varchar(10) | Y | 接单类型（字典：woTaskType） |
| assign_dept_id | int | Y | 部门ID |
| assign_dept_name | varchar(255) | Y | 部门名称（多级） |
| description | varchar(255) | N | 工单详述 |
| repeat_type | varchar(10) | Y | 重复类型（字典：woTaskRepeatType） |
| start_time | dateTime | N | 开始时间（当重复类型为重复类型时） |
| end_time | dateTime | N | 结束时间（当重复类型为重复类型时） |
| expire_time | int | Y | 过期时间（分钟）|
| initiator_id | int | N | 发起员工ID |
| initiator_name | varchar(100) | N | 发起员工姓名 |
| area_id   | int | N | 区域ID                      |
| tenant_id  | varchar(32) | N | 局址Id                      |
| space_id  | varchar(32)  | N | 空间Id                      |
| position | varchar(100) | N | 位置 |
| device_code | varchar(100) | N | 报修设备编码 |
| category | varchar(100) | N | 报修设备类型 |
| remark | varchar(255) | N | 报修备注 |
| repair_picture | json | N | 报修图片 |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 更新时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |

### wo_taskdef_repeat（任务定义-重复执行关联表）
> 当wo_taskinst.repeat_type = 2/3时，关联此表

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | ID（自增） |
| task_def_id | int | Y | 任务定义ID |
| exec_time | varchar(20) | Y | 执行时间：HH:mm:ss |

### wo_task（任务执行表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | ID（自增） |
| task_def_id | int | N | 任务定义ID |
| wo_id | varchar(32) | Y | 工单配置ID |
| trigger_task_id | int | N | 触发任务ID |
| sequence_no | varchar(50) | Y | 工单编号 |
| order_type | varchar(10) | Y | 工单类型（字典：workOrderType） |
| business_type | varchar(10) | Y | 业务模块（字典：woBusinessName） |
| order_level | varchar(10) | Y | 紧急程度（字典：workOrderLevel） |
| task_type | varchar(10) | Y | 接单类型（字典：woTaskType） |
| assign_dept_id | int | Y | 部门ID |
| assign_dept_name | varchar(255) | Y | 部门名称（多级） |
| expire_time | int | Y | 过期时间（分钟）|
| initiator_id | int | N | 发起员工ID |
| initiator_name | varchar(100) | N | 发起员工姓名 |
| mobile | varchar(32) | N | 发起员工手机号 |
| owner_id | int | Y | 部门主管员工ID |
| owner_name | varchar(100) | Y | 部门主管姓名 |
| task_status | varchar(10) | Y | 任务状态（字典：woTaskStatus） |
| reject_reason | varchar(255) | N | 驳回原因 |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 更新时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |

### wo_task_describe（任务触发描述表）

| 字段        | 数据类型    | 是否必填 | 描述               |
| - | - | - | - |
| id          | varchar(32) | Y        | UUID               |
| task_exec_id | int         | Y        | 任务执行ID    |
| label | varchar(50) | Y | label |
| type | varchar(50) | Y | 字段类型 |
| describe_value | text | Y | 字段值 |

### wo_task_instance（任务受理实例表）

| 字段        | 数据类型    | 是否必填 | 描述               |
| ----------- | ----------- | -------- | ------------------ |
| id          | varchar(32) | Y        | UUID               |
| assignee_id | int | N | 受理人ID |
| assignee_name | varchar(100) | N | 受理人名称 |
| task_exec_id | int         | Y        | 任务执行ID    |
| inst_status | varchar(10) | Y | 任务状态（字典：woTaskStatus，1：进行中 2：已完成  3：已验收：4： 已过期 5：已取消） |
| inst_desc | varchar(255) | N       | 描述（完成） |
| inst_pictures | json | N | 图片（完成） |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 更新时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |
| reject_picture | json | N | 驳回图片 |
| reject_reason | varchar(255) | N | 驳回原因 |
| trigger_id | varhcar(32) | N | 关联工单Id |

如果驳回的话 那就把任务状态直接改为进行中1、通过的话将任务状态改为已验收3

### wo_task_assignee（任务委派记录表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | ID（自增）|
| task_id | int | Y | 任务ID |
| inst_id | varchar(32) | Y | 实例ID |
| initiator_id | int | Y | 发起员工ID |
| initiator_name | varchar(100) | Y | 发起员工姓名 |
| assignee_id | int | N | 受理人ID |
| assignee_name | varchar(100) | N | 受理人名称 |
| assignee_status | varchar(10) | N | 委派状态（字典：woTaskAssigneeStatus） |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 更新时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |

### wo_task_variables（任务变量表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | ID（自增）|
| form_key | varchar(100) | Y | 表单字段 |
| task_instance_id | varchar(32) | Y | 任务实例ID |
| label | varchar(255) | Y | 表单描述 |
| value | varchar(255) | Y | 表单值 |
| value_desc | varchar(255) | N | 表单值描述 |
| extra | json | N | 额外字段 |

### wo_eod_group（部门员工班组表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | 班组ID |
| dept_id | int | Y | 部门ID |
| dept_name | varchar(255) | Y | 部门名称 |
| name | varchar(100) | Y | 班组名称 |
| eod_name | varchar(100) | Y | 值班标题 |
| eod_describe | varchar(100) | N | 值班描述 |
| area_id | int  | Y | 区域ID |
| tenant_id | varchar(32) | Y | 局址ID |
| space_id | varchar(32) | Y | 空间ID |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 更新时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |
| eod_area | varchar(255) | Y | 值班区域 |
| status | tinyint | Y | 班组状态（启用：1 停用：0） |

### wo_eod_staff（班组员工表）
> 订阅组织架构事件，处理增删改查，班组成员不足4个时，自动停用班组

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | ID（自增） |
| eod_id | int | Y | 班组ID |
| staff_id | int | Y        |员工ID|
| staff_name | varchar(100) | Y        |员工姓名|
| sort | int | Y |值班排序|

### wo_eod_staff_schedule（班组排班表）
> 每月生成一次数据，员工间可相互交换排班

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | ID（自增） |
| eod_date | String | Y | 值班日期 |
| eod_time | String | Y | 值班时间 |
| staff_id | int | Y | 员工ID |
| staff_name | varchar(100) | Y | 员工姓名 |
| mobile | varchar(50) | Y | 电话 |
| dept_name | varchar(100) | Y        | 部门名称 |
| position_name | varchar(50) | Y | 岗位名称 |
| eod_id | varchar(50) | Y | 班组Id |
| start_time | datetime | N | 开始时间 |
| end_time | datetime | N | 结束时间 |

### wo_complain（投诉表）

| 字段 | 数据类型 | 是否必填 | 描述 |
| - | - | - | - |
| id | int | Y | ID（自增） |
| sequence_no | varchar(50) | Y | 投诉编号 |
| dept_id | int | Y | 投诉部门ID |
| dept_name | varchar(255) | Y | 部门名称（多级） |
| initiator_id | int | Y | 发起员工ID |
| initiator_name | varchar(100) | Y | 发起员工姓名 |
| operator_id | int |N|操作人Id|
| title | varchar(100) | Y | 投诉标题 |
| content | text | Y | 投诉内容 |
| feedback | text | N | 反馈内容 |
| status | varchar(10) | Y | 投诉状态（字典：woComplainStatus） |
| create_time | datetime | Y | 创建时间 |
| update_time | datetime | Y | 更新时间 |
| operator | varchar(50) | N | 操作人 |
| del_flag | tinyint | Y | 是否删除 |

## 二、管理端接口

### 1. 工单配置管理

#### （1）pc端-新增工单配置

请求地址：

| URL    | api/bmpworkflow/order/add |
| ------ | ------------------------- |
| Method | POST                      |

请求参数：

| 参数         | 类型        | 位置 | 说明                           |
| ------------ | ----------- | ---- | ------------------------------ |
| deptId       | Integer     | body | 部门Id                         |
| deptName     | String      | body | 部门名称                       |
| businessType | String      | body | 业务模块                       |
| description  | String      | body | 工单描述                       |
| expireTime   | Integer     | body | 过期时间                       |
| name         | String      | body | 工单名称                       |
| formId       | String      | body | 表单Id                         |
| orderLevel   | String      | body | 工单等级                       |
| orderType    | String      | body | 工单类型                       |
| enable       | Integer     | body | 是否启用：0-禁用；1-启用       |
| triggerIds   | Set<String> | body | 关联工单                       |
| linkStrategy | String      | body | 联动策略（字典：strategyType） |


请求实例：

```
{
  "businessType": "智慧安防",
  "deptId": 1501,
  "deptName": "组织人事部",
  "description": "工单描述",
  "expireTime": 130,
  "formId": "0fc9a6f08d54d90dadf9932904f347ad",
  "name": "工单配置名称4",
  "orderLevel": "一般",
  "orderType": "服务运营",
  "ownerId": 13239,
  "ownerName": "张洪国",
  "enable":1,
  "triggerIds":["5c9c6fecd4a8884c19d2bfa8d15522fb"],
  "taskType": "0"
}
```

#### （2）pc端-查询工单配置列表（分页）

描述：查询工单配置列表，时间降序

| URL    | /api/bmpworkflow/order/list |
| ------ | --------------------------- |
| Method | GET                         |

请求参数：

| 参数         | 类型    | 位置  | 说明     |
| ------------ | ------- | ----- | -------- |
| pageNum      | Integer | query | 页数     |
| pageSize     | Integer | query | 每页条数 |
| name         | String  | query | 工单名称 |
| deptName     | String  | query | 部门名称 |
| orderType    | String  | query | 工单类型 |
| businessType | String  | query | 业务模块 |

返回参数：

| 参数         | 类型    | 说明                            |
| ------------ | ------- | ------------------------------- |
| createTime   | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime   | String  | 更新时间（yyyy-MM-dd HH:mm:ss） |
| sequenceNo   | String  | 工单配置编号                    |
| ownerId      | String  | 主管Id                          |
| ownerName    | String  | 主管名称                        |
| name         | String  | 工单名称                        |
| description  | String  | 工单描述                        |
| orderType    | Integer | 工单类型                        |
| businessType | String  | 业务模块                        |
| deptName     | String  | 部门名称                        |
| deptId       | Integer | 部门Id                          |
| taskType     | String  | 接单类型                        |
| expireTime   | Integer | 过期时间（分钟）                |
| formId       | String  | 表单Id                          |
| triggerId    | String  | 关联表单Id                      |
| enable       | Integer | 是否启用：0-禁用；1-启用        |

#### （3）pc端-编辑工单配置

请求地址：

| URL    | /api/bmpworkflow/order/edit |
| ------ | --------------------------- |
| Method | PUT                         |

请求参数：

| 参数         | 类型        | 位置 | 说明                           |
| ------------ | ----------- | ---- | ------------------------------ |
| deptId       | Integer     | body | 部门Id                         |
| deptName     | String      | body | 部门名称                       |
| businessType | String      | body | 业务模块                       |
| description  | String      | body | 工单描述                       |
| expireTime   | Integer     | body | 过期时间                       |
| name         | String      | body | 工单名称                       |
| formId       | String      | body | 表单Id                         |
| orderLevel   | String      | body | 工单等级                       |
| orderType    | String      | body | 工单类型                       |
| enable       | Integer     | body | 是否启用：0-禁用；1-启用       |
| triggerIds   | Set<String> | body | 关联工单                       |
| linkStrategy | String      | body | 联动策略（字典：strategyType） |


请求实例：

```
{
  "id":"cd596f10ef89bc963215461a262d35e2",
  "businessType": "智慧安防",
  "deptId": 1501,
  "deptName": "组织人事部",
  "description": "工单描述",
  "expireTime": 130,
  "formId": "0fc9a6f08d54d90dadf9932904f347ad",
  "name": "工单配置名称4",
  "orderLevel": "一般",
  "orderType": "服务运营",
  "ownerId": 13239,
  "ownerName": "张洪国",
  "enable":1
  "taskType": "派单"
}
```

#### （4）删除工单配置（批量）

请求地址：

| URL    | /api/bmpworkflow/order/delByIds |
| ------ | ------------------------------- |
| Method | DELETE                          |

请求参数：

| 参数 | 类型         | 位置 | 说明       |
| ---- | ------------ | ---- | ---------- |
| ids  | List<String> | body | 工单配置Id |

请求实例：

```
[
  "586386ee09d88df4941077f5cf444747","a0e210182e9d923fd24eb3e2cf49a9f4"
]
```

#### （5）pc端-导出工单配置数据

请求地址：

| URL    | /api/bmpworkflow/order/export |
| ------ | ----------------------------- |
| Method | POST                          |

请求参数：

| 参数         | 类型   | 位置  | 说明     |
| ------------ | ------ | ----- | -------- |
| name         | String | query | 工单名称 |
| deptName     | String | query | 部门名称 |
| orderType    | String | query | 工单类型 |
| businessType | String | query | 业务模块 |

#### （6）pc端-工单配置下拉框

描述：查询关联工单

| URL    | /api/bmpworkflow/order/pullList |
| ------ | ------------------------------- |
| Method | GET                             |

请求参数：

​	无

返回参数：

| 参数         | 类型    | 说明                            |
| ------------ | ------- | ------------------------------- |
| createTime   | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime   | String  | 更新时间（yyyy-MM-dd HH:mm:ss） |
| sequenceNo   | String  | 工单配置编号                    |
| ownerId      | String  | 主管Id                          |
| ownerName    | String  | 主管名称                        |
| name         | String  | 工单名称                        |
| description  | String  | 工单描述                        |
| orderType    | Integer | 工单类型                        |
| businessType | String  | 业务模块                        |
| deptName     | String  | 部门名称                        |
| deptId       | Integer | 部门Id                          |
| taskType     | String  | 接单类型                        |
| expireTime   | Integer | 过期时间（分钟）                |
| formId       | String  | 表单Id                          |
| triggerId    | String  | 关联表单Id                      |
| enable       | Integer | 是否启用：0-禁用；1-启用        |

### 2. 工单任务管理

#### （1）pc端-新增工单任务

请求地址：

| URL    | api/bmpworkflow/task/pc/create |
| ------ | ------------------------------ |
| Method | POST                           |

请求参数：

| 参数           | 类型    | 位置 | 说明         |
| -------------- | ------- | ---- | ------------ |
| assignDeptId   | Integer | body | 部门Id       |
| assignDeptName | String  | body | 部门名称     |
| businessType   | String  | body | 业务模块     |
| description    | String  | body | 工单描述     |
| expireTime     | Integer | body | 过期时间     |
| name           | String  | body | 工单任务名称 |
| areaId         | Integer | body | 区域Id       |
| tenantId       | String  | body | 局址Id       |
| spaceId        | String  | body | 空间Id       |
| orderLevel     | String  | body | 工单等级     |
| description    | String  | body | 任务描述     |
| orderType      | String  | body | 工单类型     |
| ownerId        | Integer | body | 主管ID       |
| ownerName      | String  | body | 主管名称     |
| repeatType     | String  | body | 重复类型     |
| sequenceNo     | String  | body | 工单编号     |
| taskType       | String  | body | 任务类型     |
| woId           | String  | body | 工单配置ID   |


请求实例：

```
{
  "areaId": 78,
  "assignDeptId": 1515,
  "assignDeptName": "智慧信息部",
  "businessType": "5",
  "description": "任务描述",
  "execTimes": [
    "15:00:00"
  ],
  "expireTime": 10,
  "name": "工单任务名称1",
  "orderLevel": "1",
  "orderType": "1",
  "ownerId": 13185,
  "ownerName": "栗寅",
  "repeatType": "2",
  "sequenceNo": "DBGZ20230331165533",
  "spaceId": "9c0b18242e9e4828bbc004a4f4522d39",
  "taskType": "1",
  "tenantId": "1629013398000795649",
  "woId": "78f41292ccfb21efc9199c0611d8aaad"
}
```

#### （2）pc端-查询工单任务列表（分页）

描述：查询工单任务列表，时间降序

| URL    | /api/bmpworkflow/task/def/listPage |
| ------ | ---------------------------------- |
| Method | GET                                |

请求参数：

| 参数          | 类型    | 位置  | 说明         |
| ------------- | ------- | ----- | ------------ |
| pageNum       | Integer | query | 页数         |
| pageSize      | Integer | query | 每页条数     |
| initiatorName | String  | query | 发起人姓名   |
| assignDeptId  | Integer | query | 部门ID       |
| sequenceNo    | String  | query | 工单编码     |
| orderLevel    | String  | query | 工单紧急程度 |

返回参数：

| 参数             | 类型    | 说明                            |
| ---------------- | ------- | ------------------------------- |
| createTime       | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime       | String  | 更新时间（yyyy-MM-dd HH:mm:ss） |
| sequenceNo       | String  | 工单配置编号                    |
| ownerId          | String  | 主管Id                          |
| ownerName        | String  | 主管名称                        |
| name             | String  | 工单任务名称                    |
| description      | String  | 任务描述                        |
| woId             | String  | 工单配置ID                      |
| orderType        | Integer | 工单类型                        |
| orderTypeName    | String  | 工单类型名称                    |
| repeatType       |         |                                 |
| repeatTypeName   |         |                                 |
| orderLevel       |         |                                 |
| orderLevelName   |         |                                 |
| businessType     | String  | 业务模块                        |
| businessTypeName | String  | 业务模块名称                    |
| assignDeptName   | String  | 部门名称                        |
| assignDeptId     | Integer | 部门ID                          |
| repeatType       | String  | 重复类型                        |
| taskType         | String  | 接单类型                        |
| expireTime       | Integer | 过期时间（分钟）                |
| initiatorId      | Integer | 发起人ID                        |
| initiatorName    | String  | 发起人姓名                      |
| areaId           | Integer | 区域ID                          |
| tenantId         | String  | 局址ID                          |
| spaceId          | String  | 空间ID                          |
| taskStatus       | String  | 状态                            |
| position         | String  | 位置                            |
| startTime        | String  | 开始时间                        |
| endTime          | String  | 结束时间                        |
| woTaskdefRepeats | List    | 重复时间                        |
| -- execTime      | String  | 执行时间                        |

#### （3）pc端-编辑工单任务

请求地址：

| URL    | /api/bmpworkflow/task/def/edit |
| ------ | ------------------------------ |
| Method | PUT                            |

请求参数：

| 参数           | 类型    | 说明         |
| -------------- | ------- | ------------ |
| id             | Integer | 工单任务ID   |
| assignDeptId   | Integer | 部门Id       |
| assignDeptName | String  | 部门名称     |
| businessType   | String  | 业务模块     |
| description    | String  | 工单描述     |
| expireTime     | Integer | 过期时间     |
| name           | String  | 工单任务名称 |
| areaId         | Integer | 区域Id       |
| tenantId       | String  | 局址Id       |
| spaceId        | String  | 空间Id       |
| orderLevel     | String  | 工单等级     |
| description    | String  | 任务描述     |
| orderType      | String  | 工单类型     |
| ownerId        | Integer | 主管ID       |
| ownerName      | String  | 主管名称     |
| repeatType     | String  | 重复类型     |
| sequenceNo     | String  | 工单编号     |
| taskType       | String  | 任务类型     |
| woId           | String  | 工单配置ID   |




请求实例：

```
{
  "id":1,
  "areaId": 78,
  "assignDeptId": 1515,
  "assignDeptName": "智慧信息部",
  "businessType": "5",
  "description": "任务描述",
  "execTimes": [
    "15:00:00"
  ],
  "expireTime": 10,
  "name": "工单任务名称1",
  "orderLevel": "1",
  "orderType": "1",
  "ownerId": 13185,
  "ownerName": "栗寅",
  "repeatType": "2",
  "sequenceNo": "DBGZ20230331165533",
  "spaceId": "9c0b18242e9e4828bbc004a4f4522d39",
  "taskType": "1",
  "tenantId": "1629013398000795649",
  "woId": "78f41292ccfb21efc9199c0611d8aaad"
}
```

#### （4）pc端-导出工单任务列表数据

请求地址：

| URL    | /api/bmpworkflow/task/def/export |
| ------ | -------------------------------- |
| Method | POST                             |

请求参数：

| 参数          | 类型   | 位置 | 说明         |
| ------------- | ------ | ---- | ------------ |
| initiatorName | String | body | 发起人姓名   |
| assignDeptId  | String | body | 部门ID       |
| sequenceNo    | String | body | 工单配置编号 |
| orderLevel    | String | body | 工单等级     |

### 3. 工单执行管理

#### （1）pc端-执行工单列表（分页）

描述：查询工单执行任务列表，时间降序

| URL    | /api/bmpworkflow/task/execute/listPage |
| ------ | -------------------------------------- |
| Method | GET                                    |

请求参数：

| 参数          | 类型    | 位置  | 说明         |
| ------------- | ------- | ----- | ------------ |
| pageNum       | Integer | query | 页数         |
| pageSize      | Integer | query | 每页条数     |
| initiatorName | String  | query | 发起人姓名   |
| assigneeName  | String  | query | 受理人姓名   |
| assignDeptId  | Integer | query | 部门ID       |
| sequenceNo    | String  | query | 工单编码     |
| orderLevel    | String  | query | 工单紧急程度 |

返回参数：

| 参数                 | 类型    | 说明                            |
| -------------------- | ------- | ------------------------------- |
| createTime           | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime           | String  | 更新时间（yyyy-MM-dd HH:mm:ss） |
| sequenceNo           | String  | 工单配置编号                    |
| ownerId              | String  | 主管Id                          |
| ownerName            | String  | 主管名称                        |
| name                 | String  | 工单任务名称                    |
| description          | String  | 任务描述                        |
| woId                 | String  | 工单配置ID                      |
| orderType            | Integer | 工单类型                        |
| businessType         | String  | 业务模块                        |
| assignDeptName       | String  | 部门名称                        |
| assignDeptId         | Integer | 部门ID                          |
| repeatType           | String  | 重复类型                        |
| taskType             | String  | 接单类型                        |
| expireTime           | Integer | 过期时间（分钟）                |
| initiatorId          | Integer | 发起人ID                        |
| initiatorName        | String  | 发起人姓名                      |
| areaId               | Integer | 区域ID                          |
| tenantId             | String  | 局址ID                          |
| spaceId              | String  | 空间ID                          |
| position             | String  | 位置                            |
| startTime            | String  | 开始时间                        |
| endTime              | String  | 结束时间                        |
| woTaskInstanceVOList | List    | 详情                            |
| - assigneeId         | Integer | 受理人Id                        |
| - assigneeName       | String  | 受理人姓名                      |
| - instPictures       | Set     | 完成图片                        |
| - mobile             | String  | 手机号                          |
| - instDesc           | String  | 完成备注                        |
| - updateTime         | date    | 更新时间                        |
| - formExtra          | List    | 表单数据                        |
| -- formKey           | String  | 字段key                         |
| -- label             | String  | 字段描述                        |
| -- value             | String  | 字段值（正常：1 异常：0）       |

#### （2）pc端-编辑执行工单任务

请求地址：

| URL    | /api/bmpworkflow/task/execute/edit |
| ------ | ---------------------------------- |
| Method | PUT                                |

请求参数：

| 参数       | 类型    | 说明             |
| ---------- | ------- | ---------------- |
| id         | Integer | 工单执行任务ID   |
| taskStatus | String  | 工单执行任务状态 |




请求实例：

```
{
  "id":1,
  "taskStatus": "2"
}
```

#### （3）pc端-执行任务工单验收审批

请求地址：

| URL    | /api/bmpworkflow/task/execute/check |
| ------ | ----------------------------------- |
| Method | PUT                                 |

请求参数：

| 参数          | 类型    | 说明                            |
| ------------- | ------- | ------------------------------- |
| instStatus    | String  | 执行状态（验收通过：4 驳回：2） |
| taskExecId    | Integer | 工单执行任务Id                  |
| rejectReason  | String  | 驳回原因                        |
| rejectPicture | Set     | 驳回图片                        |




请求实例：

```
{
  "taskExecId":1,
  "taskStatus": "4",
  "rejectReason":"检验不合格",
  "rejectPicture":["http://192.165.1.62:9881/group1/M00/00/22/wKUBPmQX9_GAZnUBAAAHoQzUBGI151.png","http://192.165.1.62:9881/group1/M00/00/22/wKUBPmQa0VaAbQClAAAHmoip2js516.png"]
}
```

#### （4）pc端-删除执行工单列表（支持批量）

请求地址：

| URL    | /api/bmpworkflow/task/execute/delByIds |
| ------ | -------------------------------------- |
| Method | DELETE                                 |

请求参数：

| 参数 | 类型          | 位置 | 说明       |
| ---- | ------------- | ---- | ---------- |
| ids  | List<Integer> | body | 工单配置Id |

请求实例：

```
[
  1
]
```



### 4. 工单报表

#### （1）pc端-查询任务填报表（分页）

| URL    | /api/bmpworkflow/task/page/taskForm |
| ------ | ----------------------------------- |
| Method | GET                                 |

请求参数：

| 参数           | 类型    | 位置  | 说明       |
| -------------- | ------- | ----- | ---------- |
| pageNum        | Integer | query | 页数       |
| pageSize       | Integer | query | 每页条数   |
| formName       | String  | query | 任务表名称 |
| sequenceNo     | String  | query | 编号       |
| orderType      | String  | query | 工单类型   |
| businessType   | String  | query | 业务模块   |
| assignDeptName | String  | query | 部门名称   |
| orderName      | String  | query | 工单名称   |

返回参数：

| 参数               | 类型    | 说明           |
| ------------------ | ------- | -------------- |
| total              | Integer | 记录条数       |
| records            | List    | 记录           |
| - taskExecId       | Integer | 执行任务Id     |
| - sequenceNo       | String  | 编号           |
| - formName         | String  | 任务填报表名称 |
| - taskType         | Strng   | 任务类型       |
| - taskTypeName     | String  | 任务类型名称   |
| - businessType     | String  | 业务模块       |
| - businessTypeName | String  | 业务模块名称   |
| - assignDeptId     | String  | 部门ID         |
| - assignDeptName   | String  | 部门名称       |
| - dataSize         | Integer | 数据条数       |
| - orderName        | String  | 工单名称       |
| - orderType        | String  | 工单类型       |
| - orderTypeName    | String  | 工单类型名称   |
| - details          | List    | 详情           |
| -- time            | String  | 时间           |
| -- assigneeName    | String  | 受理人         |
| -- woTaskVariables | List    | 表单字段       |
| --- formKey        | String  | 字段Key        |
| --- label          | String  | 字段名         |
| --- value          | String  | 字段值         |

#### （2）pc端-导出任务填报表数据

请求地址：

| URL    | /api/bmpworkflow/task/taskForm/export |
| ------ | ------------------------------------- |
| Method | POST                                  |

请求参数：

| 参数           | 类型    | 位置  | 说明       |
| -------------- | ------- | ----- | ---------- |
| pageNum        | Integer | query | 页数       |
| pageSize       | Integer | query | 每页条数   |
| formName       | String  | query | 任务表名称 |
| sequenceNo     | String  | query | 编号       |
| orderType      | String  | query | 工单类型   |
| businessType   | String  | query | 业务模块   |
| assignDeptName | String  | query | 部门名称   |
| orderName      | String  | query | 工单名称   |

### 5. 工单统计

#### （1）pc端-工单数统计

| URL    | /api/bmpworkflow//data/overview/count/list |
| ------ | ------------------------------------------ |
| Method | GET                                        |

请求参数：

| 参数          | 类型          | 位置  | 说明                               |
| ------------- | ------------- | ----- | ---------------------------------- |
| dateType      | String        | query | 时间维度（y:1 m:2 d:3）必传        |
| startTime     | String        | query | 开始时间（格式与时间维度联动）必传 |
| endTime       | String        | query | 结束时间（格式与时间维度联动）必传 |
| assignDeptIds | List<Integer> | query | 部门id                             |
| woIds         | List<String>  | query | 工单id                             |
| assigneeIds   | List<Integer> | query | 员工ID                             |

返回参数：

| 参数            | 类型    | 说明     |
| --------------- | ------- | -------- |
| unCompleteCount | Integer | 未完成数 |
| comleteCount    | String  | 已完成数 |
| total           | String  | 派单数   |
| expireCount     | String  | 过期数   |



#### （2）pc端-工单数柱状图统计

| URL    | /api/bmpworkflow//data/overview/chart/list |
| ------ | ------------------------------------------ |
| Method | GET                                        |

请求参数：

| 参数          | 类型          | 位置  | 说明                               |
| ------------- | ------------- | ----- | ---------------------------------- |
| dateType      | String        | query | 时间维度（y:1 m:2 d:3）必传        |
| startTime     | String        | query | 开始时间（格式与时间维度联动）必传 |
| endTime       | String        | query | 结束时间（格式与时间维度联动）必传 |
| assignDeptIds | List<Integer> | query | 部门id                             |
| woIds         | List<String>  | query | 工单id                             |
| assigneeIds   | List<Integer> | query | 员工ID                             |

返回参数：

| 参数           | 类型 | 说明                     |
| -------------- | ---- | ------------------------ |
| unCompleteList | List | 按时间范围统计未完成List |
| expireList     | List | 按时间范围统计过期List   |
| completeList   | List | 按时间范围统计完成List   |

#### （3）pc端-部门完成工单数统计

| URL    | /api/bmpworkflow//data/overview/count/listByDepart |
| ------ | -------------------------------------------------- |
| Method | GET                                                |

请求参数：

| 参数          | 类型          | 位置  | 说明                               |
| ------------- | ------------- | ----- | ---------------------------------- |
| dateType      | String        | query | 时间维度（y:1 m:2 d:3）必传        |
| startTime     | String        | query | 开始时间（格式与时间维度联动）必传 |
| endTime       | String        | query | 结束时间（格式与时间维度联动）必传 |
| assignDeptIds | List<Integer> | query | 部门id                             |
| woIds         | List<String>  | query | 工单id                             |
| assigneeIds   | List<Integer> | query | 员工ID                             |

返回参数：

| 参数     | 类型    | 说明   |
| -------- | ------- | ------ |
| deptName | String  | 部门名 |
| count    | Integer | 数量   |

#### （4）pc端-部门完成工单数柱状图统计

| URL    | /api/bmpworkflow//data/overview/chart/listByDepart |
| ------ | -------------------------------------------------- |
| Method | GET                                                |

请求参数：

| 参数          | 类型          | 位置  | 说明                               |
| ------------- | ------------- | ----- | ---------------------------------- |
| dateType      | String        | query | 时间维度（y:1 m:2 d:3）必传        |
| startTime     | String        | query | 开始时间（格式与时间维度联动）必传 |
| endTime       | String        | query | 结束时间（格式与时间维度联动）必传 |
| assignDeptIds | List<Integer> | query | 部门id                             |
| woIds         | List<String>  | query | 工单id                             |
| assigneeIds   | List<Integer> | query | 员工ID                             |

返回参数：

| 参数       | 类型    | 说明   |
| ---------- | ------- | ------ |
| 智慧信息部 | String  | 部门名 |
| time       | String  | 时间   |
| value      | Integer | 数量   |

#### （5）pc端-部门工单人员下拉联动

| URL    | /api/bmpworkflow/data/overview/pullList |
| ------ | --------------------------------------- |
| Method | POST                                    |

请求参数：

| 参数     | 类型          | 位置 | 说明   |
| -------- | ------------- | ---- | ------ |
| deptIds  | List<Integer> | body | 部门ID |
| orderIds | List<String>  | body | 工单ID |

返回参数：

| 参数         | 类型    | 说明       |
| ------------ | ------- | ---------- |
| woId         | String  | 工单Id     |
| woName       | String  | 工单名称   |
| assigneeId   | Integer | 受理人Id   |
| assigneeName | String  | 受理人姓名 |

### 6. 班组管理

#### （1）pc端-新增班组

请求地址：

| URL    | api/bmpworkflow/eod/create |
| ------ | -------------------------- |
| Method | POST                       |

请求参数：

| 参数        | 类型    | 位置 | 说明                        |
| ----------- | ------- | ---- | --------------------------- |
| deptId      | Integer | body | 部门Id                      |
| deptName    | String  | body | 部门名称                    |
| areaId      | Integer | body | 区域ID                      |
| tenantId    | String  | body | 局址Id                      |
| spaceId     | String  | body | 空间Id                      |
| name        | String  | body | 班组名称                    |
| eodName     | String  | body | 值班标题                    |
| eodDescribe | String  | body | 值班详述                    |
| staffIds    | List    | body | 人员值班排序                |
| status      | Integer | body | 班组状态（1：正常 0：停用） |


请求实例：

```
{
  "areaId": 79,
  "deptId": 1501,
  "eodDescribe": "值班描述1",
  "eodName": "值班标题1",
  "name": "班组名1",
  "spaceId": "03ee76766b1840de85720bbd0a193acb",
  "staffIds": [
    13239
  ],
  "status": 0,
  "deptName":"组织人事部",
  "tenantId": "1629051291050188801"
}
```

#### （2）pc端-班组列表（分页）

描述：查询班组列表，时间降序

| URL    | /api/bmpworkflow/eod/list |
| ------ | ------------------------- |
| Method | GET                       |

请求参数：

| 参数     | 类型    | 位置  | 说明                        |
| -------- | ------- | ----- | --------------------------- |
| pageNum  | Integer | query | 页数                        |
| pageSize | Integer | query | 每页条数                    |
| name     | String  | query | 班组名称                    |
| deptName | String  | query | 部门名称                    |
| status   | Integer | query | 班组状态（1：正常 0：停用） |

返回参数：

| 参数             | 类型    | 说明                            |
| ---------------- | ------- | ------------------------------- |
| createTime       | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime       | String  | 更新（yyyy-MM-dd HH:mm:ss）     |
| eodName          | String  | 值班标题                        |
| eodDescribe      | String  | 值班描述                        |
| eodArea          | String  | 值班区域                        |
| woEodStaffPOList | List    | 班组人员                        |
| -- staffName     | String  | 员工姓名                        |
| -- staffId       | Integer | 员工Id                          |
| -- sort          | String  | 排序（默认按升序返回）          |
| deptName         | String  | 部门名称                        |
| deptId           | Integer | 部门Id                          |
| status           | Integer | 班组状态（1：正常 0：停用）     |

#### （3）pc端-班组管理

请求地址：

| URL    | /api/bmpworkflow/eod/update |
| ------ | --------------------------- |
| Method | PUT                         |

请求参数：

| 参数        | 类型    | 说明                        |
| ----------- | ------- | --------------------------- |
| deptId      | Integer | 部门Id                      |
| deptName    | String  | 部门名称                    |
| areaId      | Integer | 区域ID                      |
| tenantId    | String  | 局址Id                      |
| spaceId     | String  | 空间Id                      |
| name        | String  | 班组名称                    |
| eodName     | String  | 值班标题                    |
| eodDescribe | String  | 值班详述                    |
| staffIds    | List    | 人员值班排序                |
| status      | Integer | 班组状态（1：正常 0：停用） |


请求实例：

```
{
  "id":1,
  "areaId": 79,
  "deptId": 1501,
  "eodDescribe": "值班描述1",
  "eodName": "值班标题1",
  "name": "班组名1",
  "spaceId": "03ee76766b1840de85720bbd0a193acb",
  "staffIds": [
    13239
  ],
  "status": 0,
  "deptName":"组织人事部",
  "tenantId": "1629051291050188801"
}
```

#### （4）pc端-值班管理-值班排序

描述：每次选完班组人员后 调用接口返回排班列表进行渲染

| URL    | /api/bmpworkflow/eod/schedule/sort |
| ------ | ---------------------------------- |
| Method | POST                               |

请求参数：

| 参数       | 类型         | 位置 | 说明         |
| ---------- | ------------ | ---- | ------------ |
| staffNames | List<string> | body | 员工姓名数组 |

请求示例：

```java
[
  "张三","李四","王五","赵六"
]
```

返回示例：

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": {
    "2023-03-07": [
      "李四",
      "张三",
      "赵六",
      "王五"
    ],
    "2023-03-29": [
      "赵六",
      "王五",
      "李四",
      "张三"
    ],
    "2023-03-06": [
      "张三",
      "赵六",
      "王五",
      "李四"
    ],
    "2023-03-28": [
      "王五",
      "李四",
      "张三",
      "赵六"
    ],
    "2023-03-05": [
      "赵六",
      "王五",
      "李四",
      "张三"
    ],
    "2023-03-27": [
      "李四",
      "张三",
      "赵六",
      "王五"
    ],
    "2023-03-04": [
      "王五",
      "李四",
      "张三",
      "赵六"
    ],
    "2023-03-26": [
      "张三",
      "赵六",
      "王五",
      "李四"
    ],
    "2023-03-09": [
      "赵六",
      "王五",
      "李四",
      "张三"
    ],
    "2023-03-08": [
      "王五",
      "李四",
      "张三",
      "赵六"
    ],
    "2023-03-10": [
      "张三",
      "赵六",
      "王五",
      "李四"
    ],
    "2023-03-31": [
      "李四",
      "张三",
      "赵六",
      "王五"
    ],
    "2023-03-30": [
      "张三",
      "赵六",
      "王五",
      "李四"
    ],
    "2023-03-14": [
      "张三",
      "赵六",
      "王五",
      "李四"
    ],
    "2023-03-13": [
      "赵六",
      "王五",
      "李四",
      "张三"
    ],
    "2023-03-12": [
      "王五",
      "李四",
      "张三",
      "赵六"
    ],
    "2023-03-11": [
      "李四",
      "张三",
      "赵六",
      "王五"
    ],
    "2023-03-18": [
      "张三",
      "赵六",
      "王五",
      "李四"
    ],
    "2023-03-17": [
      "赵六",
      "王五",
      "李四",
      "张三"
    ],
    "2023-03-16": [
      "王五",
      "李四",
      "张三",
      "赵六"
    ],
    "2023-03-15": [
      "李四",
      "张三",
      "赵六",
      "王五"
    ],
    "2023-03-19": [
      "李四",
      "张三",
      "赵六",
      "王五"
    ],
    "2023-03-21": [
      "赵六",
      "王五",
      "李四",
      "张三"
    ],
    "2023-03-20": [
      "王五",
      "李四",
      "张三",
      "赵六"
    ],
    "2023-03-03": [
      "李四",
      "张三",
      "赵六",
      "王五"
    ],
    "2023-03-25": [
      "赵六",
      "王五",
      "李四",
      "张三"
    ],
    "2023-03-02": [
      "张三",
      "赵六",
      "王五",
      "李四"
    ],
    "2023-03-24": [
      "王五",
      "李四",
      "张三",
      "赵六"
    ],
    "2023-03-01": [
      "赵六",
      "王五",
      "李四",
      "张三"
    ],
    "2023-03-23": [
      "李四",
      "张三",
      "赵六",
      "王五"
    ],
    "2023-03-22": [
      "张三",
      "赵六",
      "王五",
      "李四"
    ]
  },
  "success": true
}
```

#### （5）查询部门员工是否已为班组成员

描述：当新建班组 选择班组成员时，需要校验是否已为班组成员，如果是 ：需要弹框提示；否：正常选择

| URL    | /api/bmpworkflow/eod/isEodStaff |
| ------ | ------------------------------- |
| Method | GET                             |

请求参数：

| 参数     | 类型    | 位置  | 说明                        |
| -------- | ------- | ----- | --------------------------- |
| staffId  | Integer | query | 员工Id                        |


返回参数：

| 参数             | 类型    | 说明                            |
| ---------------- | ------- | ------------------------------- |
| data           | Boolean | 是否为班组成员（是：true 否：false） |

#### （6）删除班组（批量）

请求地址：

| URL    | /api/bmpworkflow/eod/delByIds |
| ------ | ----------------------------- |
| Method | DELETE                        |

请求参数：

| 参数 | 类型          | 位置 | 说明   |
| ---- | ------------- | ---- | ------ |
| ids  | List<Integer> | body | 班组Id |

请求实例：

```
[
  1,2
]
```

#### （7）pc端-导出班组列表数据

请求地址：

| URL    | /api/bmpworkflow/eod/export |
| ------ | --------------------------- |
| Method | POST                        |

请求参数：无

### 7. 投诉管理

#### （1）pc端-投诉列表（分页）

描述：查询投诉列表，时间降序

| URL    | /api/bmpworkflow/complain/list |
| ------ | ------------------------------ |
| Method | GET                            |

请求参数：

| 参数      | 类型    | 位置  | 说明                     |
| --------- | ------- | ----- | ------------------------ |
| pageNum   | Integer | query | 页数                     |
| pageSize  | Integer | query | 每页条数                 |
| deptId | Integer  | query | 部门Id |

返回参数：

| 参数          | 类型    | 说明                            |
| ------------- | ------- | ------------------------------- |
| status        | Integer | 投诉状态                        |

#### （2）pc端-投诉反馈

请求地址：

| URL    | /api/bmpworkflow/complain/feedback |
| ------ | ---------------------------------- |
| Method | PUT                                |

请求参数：

| 名称     | 类型    | 说明     |
| -------- | ------- | -------- |
| id       | Integer | 行Id     |
| feedback | String  | 反馈内容 |

请求示例：

```json
{
  "feedback": "反馈内容",
  "id": 1
}
```

#### （3）删除投诉

请求地址：

| URL    | /api/bmpworkflow/complain/{id} |
| ------ | ------------------------------ |
| Method | DELETE                         |

请求参数：

| 参数 | 类型    | 位置  | 说明   |
| ---- | ------- | ----- | ------ |
| id   | Integer | query | 投诉Id |

请求实例：

```
/api/bmpworkflow/complain/1
```

#### （4）pc端-导出投诉列表数据

请求地址：

| URL    | /api/bmpworkflow/complain/export |
| ------ | -------------------------------- |
| Method | POST                             |

请求参数：无

## 三、移动端接口

### 1. 工单

#### （1）移动端-发起执行工单任务

请求地址：

| URL    | /api/bmpworkflow/task/mob/create |
| ------ | -------------------------------- |
| Method | POST                             |

请求参数：

| 参数           | 类型    | 位置 | 说明         |
| -------------- | ------- | ---- | ------------ |
| assignDeptId   | Integer | body | 部门Id       |
| assignDeptName | String  | body | 部门名称     |
| businessType   | String  | body | 业务模块     |
| description    | String  | body | 工单描述     |
| expireTime     | Integer | body | 过期时间     |
| name           | String  | body | 工单任务名称 |
| areaId         | Integer | body | 区域Id       |
| tenantId       | String  | body | 局址Id       |
| spaceId        | String  | body | 空间Id       |
| orderLevel     | String  | body | 工单等级     |
| orderType      | String  | body | 工单类型     |
| ownerId        | Integer | body | 主管ID       |
| ownerName      | String  | body | 主管名称     |
| repeatType     | String  | body | 重复类型     |
| sequenceNo     | String  | body | 工单编号     |
| taskType       | String  | body | 任务类型     |
| woId           | String  | body | 工单配置ID   |
| repairPicture  | Set     | body | 报修图片     |
| deviceCode     | String  | body | 设备编码     |
| category       | String  | body | 设备类型     |
| remark         | String  | body | 报修备注     |


请求实例：

```
{
  "areaId": 78,
  "assignDeptId": 1515,
  "assignDeptName": "智慧信息部",
  "businessType": "5",
  "description": "任务描述",
  "taskPicture": [
    "15:00:00"
  ],
  "expireTime": 10,
  "name": "工单任务名称1",
  "orderLevel": "1",
  "orderType": "1",
  "ownerId": 13185,
  "ownerName": "栗寅",
  "repeatType": "2",
  "sequenceNo": "DBGZ20230331165533",
  "spaceId": "9c0b18242e9e4828bbc004a4f4522d39",
  "taskType": "1",
  "tenantId": "1629013398000795649",
  "woId": "78f41292ccfb21efc9199c0611d8aaad"
}
```

#### （2）移动端-查询派单列表（主管）

| URL    | /api/bmpworkflow/task/mob/assign/list |
| ------ | ------------------------------------- |
| Method | GET                                   |

请求参数：

| 参数       | 类型   | 位置  | 说明                                 |
| ---------- | ------ | ----- | ------------------------------------ |
| taskStatus | String | query | 工单任务状态（0：待指派  已指派：1） |

返回参数：

| 参数             | 类型    | 说明                            |
| ---------------- | ------- | ------------------------------- |
| createTime       | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime       | String  | 更新时间（yyyy-MM-dd HH:mm:ss） |
| sequenceNo       | String  | 工单配置编号                    |
| ownerId          | String  | 主管Id                          |
| ownerName        | String  | 主管名称                        |
| name             | String  | 工单任务名称                    |
| description      | String  | 任务描述                        |
| woId             | String  | 工单配置ID                      |
| orderLevelName   | String  | 紧急程度名称                    |
| orderType        | Integer | 工单类型                        |
| orderTypeName    | String  | 工单类型名称                    |
| businessType     | String  | 业务模块                        |
| businessTypeName | String  | 业务模块名称                    |
| assignDeptName   | String  | 部门名称                        |
| assignDeptId     | Integer | 部门ID                          |
| repeatType       | String  | 重复类型                        |
| taskType         | String  | 接单类型                        |
| taskTypeName     | String  | 接单类型名称                    |
| expireTime       | Integer | 过期时间（分钟）                |
| initiatorId      | Integer | 发起人ID                        |
| initiatorName    | String  | 发起人姓名                      |
| areaId           | Integer | 区域ID                          |
| tenantId         | String  | 局址ID                          |
| spaceId          | String  | 空间ID                          |
| taskStatus       | String  | 状态                            |
| position         | String  | 位置                            |
| startTime        | String  | 开始时间                        |
| endTime          | String  | 结束时间                        |

#### （3）移动端-执行工单任务指派（主管）

请求地址：

| URL    | /api/bmpworkflow/task/mob/assignTask |
| ------ | ------------------------------------ |
| Method | POST                                 |

请求参数：

| 参数           | 类型    | 位置 | 说明         |
| -------------- | ------- | ---- | ------------ |
| taskExecId     | Integer | body | 执行工单任务Id     |
| woEodStaffs      | List  | body | 指派人     |
| -- staffId     | Integer  | body | 员工Id     |
| -- staffName     | String  | body | 员工编号     |


请求实例：

```
{
  "taskExecId": 1,
  "woEodStaffs": [
    {
      "staffId": 13239,
      "staffName": "张洪国"
    },
    {
      "staffId": 13240,
      "staffName": "李晨博"
    }
  ]
}
```

#### （4）移动端-执行工单任务驳回(主管)

请求地址：

| URL    | /api/bmpworkflow/task/mob/reject |
| ------ | -------------------------------- |
| Method | PUT                              |

请求参数：

| 参数       | 类型    | 位置 | 说明           |
| ---------- | ------- | ---- | -------------- |
| id         | Integer | body | 执行工单任务Id |
| taskStatus | String       | body | 已驳回：6         |
| rejectReason | String | body | 驳回原因 |


请求实例：

```
{
  "id": 1,
  "rejectReason":"驳回原因"
  "taskStatus": "6"
}
```

#### （5）移动端-查询任务实例列表（普通员工）

| URL    | /api/bmpworkflow/task/mob/instance/list |
| ------ | --------------------------------------- |
| Method | GET                                     |

请求参数：

| 参数       | 类型   | 位置  | 说明                                                |
| ---------- | ------ | ----- | --------------------------------------------------- |
| instStatus | String | query | 实例工单任务状态（ 已指派：1 已完成：3 已取消 : 6） |

返回参数：

| 参数             | 类型    | 说明                            |
| ---------------- | ------- | ------------------------------- |
| assigneeId       | Integer | 受理人ID                        |
| assigneeName     | String  | 受理人姓名                      |
| instStatus       | String  | 实例工单任务状态                |
| createTime       | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime       | String  | 更新时间（yyyy-MM-dd HH:mm:ss） |
| woTaskdefPO      | Object  | 工单定义对象                    |
| - sequenceNo     | String  | 工单配置编号                    |
| - ownerId        | String  | 主管Id                          |
| - ownerName      | String  | 主管名称                        |
| - name           | String  | 工单任务名称                    |
| - description    | String  | 任务描述                        |
| - woId           | String  | 工单配置ID                      |
| - orderType      | Integer | 工单类型                        |
| - businessType   | String  | 业务模块                        |
| - assignDeptName | String  | 部门名称                        |
| - assignDeptId   | Integer | 部门ID                          |
| - repeatType     | String  | 重复类型                        |
| - taskType       | String  | 接单类型                        |
| - expireTime     | Integer | 过期时间（分钟）                |
| - initiatorId    | Integer | 发起人ID                        |
| - initiatorName  | String  | 发起人姓名                      |
| - areaId         | Integer | 区域ID                          |
| - tenantId       | String  | 局址ID                          |
| - spaceId        | String  | 空间ID                          |
| - taskStatus     | String  | 状态                            |
| - position       | String  | 位置                            |
| - startTime      | String  | 开始时间                        |
| - endTime        | String  | 结束时间                        |
| formExtra        | List    | 表单相关字段                    |
| formId           | String  | 表单Id                          |
| formKey          | String  | 字段key                         |
| value            | String  | 字段值                          |
| label            | String  | 字段描述                        |
| type             | String  | 输入框类型                      |

#### （6）移动端-我发起的工单任务列表查询

| URL    | /api/bmpworkflow/task/mob/initiate/list |
| ------ | --------------------------------------- |
| Method | GET                                     |

请求参数：

| 参数       | 类型   | 位置  | 说明     |
| ---------- | ------ | ----- | -------- |
| taskStatus | String | query | 任务状态 |

返回参数：

| 参数           | 类型    | 说明                            |
| -------------- | ------- | ------------------------------- |
| createTime     | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime     | String  | 更新时间（yyyy-MM-dd HH:mm:ss） |
| sequenceNo     | String  | 工单配置编号                    |
| ownerId        | String  | 主管Id                          |
| ownerName      | String  | 主管名称                        |
| name           | String  | 工单任务名称                    |
| description    | String  | 任务描述                        |
| woId           | String  | 工单配置ID                      |
| orderType      | Integer | 工单类型                        |
| businessType   | String  | 业务模块                        |
| assignDeptName | String  | 部门名称                        |
| assignDeptId   | Integer | 部门ID                          |
| repeatType     | String  | 重复类型                        |
| taskType       | String  | 接单类型                        |
| expireTime     | Integer | 过期时间（分钟）                |
| initiatorId    | Integer | 发起人ID                        |
| initiatorName  | String  | 发起人姓名                      |
| areaId         | Integer | 区域ID                          |
| tenantId       | String  | 局址ID                          |
| spaceId        | String  | 空间ID                          |
| position       | String  | 位置                            |
| startTime      | String  | 开始时间                        |
| endTime        | String  | 结束时间                        |

#### （7）移动端-取消我发起的工单任务

请求地址：

| URL    | /api/bmpworkflow/task/mob/cancel/{id} |
| ------ | ------------------------------------- |
| Method | PUT                                   |

请求参数：

| 参数 | 类型    | 位置 | 说明 |
| ---- | ------- | ---- | ---- |
| id   | Integer | path |      |

请求实例

```sh
/api/bmpworkflow/task/mob/cancel/1
```

#### （8）移动端-转单

请求地址：

| URL    | /api/bmpworkflow/task/mob/transfer |
| ------ | ---------------------------------- |
| Method | POST                               |

请求参数：

| 参数         | 类型    | 位置 | 说明           |
| ------------ | ------- | ---- | -------------- |
| taskId       | Integer | body | 执行工单任务Id |
| instId       | String  | body | 工单实例Id     |
| assigneeId   | Integer | body | 员工Id         |
| assigneeName | String  | body | 员工姓名       |


请求实例：

```
{
  "assigneeId": 13239,
  "assigneeName": "张洪国",
  "instId": "861d9fb10ca1d9bedeb245b91a0ab773",
  "taskId": 1
}
```

#### （9）移动端-查询转单列表

| URL    | /api/bmpworkflow/task/mob/transfer/list |
| ------ | --------------------------------------- |
| Method | GET                                     |

请求参数：无

返回参数：

| 参数           | 类型    | 说明                            |
| -------------- | ------- | ------------------------------- |
| createTime     | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime     | String  | 更新时间（yyyy-MM-dd HH:mm:ss） |
| sequenceNo     | String  | 工单配置编号                    |
| ownerId        | String  | 主管Id                          |
| ownerName      | String  | 主管名称                        |
| name           | String  | 工单任务名称                    |
| description    | String  | 任务描述                        |
| woId           | String  | 工单配置ID                      |
| orderType      | Integer | 工单类型                        |
| businessType   | String  | 业务模块                        |
| assignDeptName | String  | 部门名称                        |
| assignDeptId   | Integer | 部门ID                          |
| repeatType     | String  | 重复类型                        |
| taskType       | String  | 接单类型                        |
| expireTime     | Integer | 过期时间（分钟）                |
| initiatorId    | Integer | 发起人ID                        |
| initiatorName  | String  | 发起人姓名                      |
| areaId         | Integer | 区域ID                          |
| tenantId       | String  | 局址ID                          |
| spaceId        | String  | 空间ID                          |
| taskStatus     | String  | 状态                            |
| position       | String  | 位置                            |
| startTime      | String  | 开始时间                        |
| endTime        | String  | 结束时间                        |

#### （10）移动端-查询抢单列表

| URL    | /api/bmpworkflow/task/mob/grab/list |
| ------ | ----------------------------------- |
| Method | GET                                 |

请求参数：无

返回参数：

| 参数             | 类型    | 说明                            |
| ---------------- | ------- | ------------------------------- |
| woTaskdefPO      | object  | 工单定义对象                    |
| - createTime     | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| - updateTime     | String  | 更新时间（yyyy-MM-dd HH:mm:ss） |
| - sequenceNo     | String  | 工单配置编号                    |
| - ownerId        | String  | 主管Id                          |
| - ownerName      | String  | 主管名称                        |
| - name           | String  | 工单任务名称                    |
| - description    | String  | 任务描述                        |
| - woId           | String  | 工单配置ID                      |
| - orderType      | Integer | 工单类型                        |
| - businessType   | String  | 业务模块                        |
| - assignDeptName | String  | 部门名称                        |
| - assignDeptId   | Integer | 部门ID                          |
| - repeatType     | String  | 重复类型                        |
| - taskType       | String  | 接单类型                        |
| - expireTime     | Integer | 过期时间（分钟）                |
| - initiatorId    | Integer | 发起人ID                        |
| - initiatorName  | String  | 发起人姓名                      |
| - areaId         | Integer | 区域ID                          |
| - tenantId       | String  | 局址ID                          |
| - spaceId        | String  | 空间ID                          |
| - taskStatus     | String  | 状态                            |
| - position       | String  | 位置                            |
| - startTime      | String  | 开始时间                        |
| - endTime        | String  | 结束时间                        |

#### （11）移动端-抢单

请求地址：

| URL    | /api/bmpworkflow/task/mob/grab |
| ------ | ------------------------------ |
| Method | POST                           |

请求参数：

| 参数       | 类型    | 位置 | 说明           |
| ---------- | ------- | ---- | -------------- |
| taskExecId | Integer | body | 执行工单任务Id |


请求实例：

```
{
  "taskExecId": 1
}
```

#### （12）移动端-接单（他人转的单）

请求地址：

| URL    | /api/bmpworkflow/task/mob/receive |
| ------ | --------------------------------- |
| Method | POST                              |

请求参数：

| 参数         | 类型    | 位置 | 说明           |
| ------------ | ------- | ---- | -------------- |
| taskExecId   | Integer | body | 执行工单任务Id |
| id           | String  | body | 工单实例ID     |
| assigneeId   | Integer | body | 受理人Id       |
| assigneeName | String  | body | 受理人姓名     |


请求实例：

```
{
  "taskExecId": 1,
  "id":"861d9fb10ca1d9bedeb245b91a0ab343",
  "assigneeId":13239,
  "assigneeName":"张洪国"
}
```

#### （13）移动端-拒单（他人转的单）

请求地址：

| URL    | /api/bmpworkflow/task/mob/rejectTask |
| ------ | ------------------------------------ |
| Method | POST                                 |

请求参数：

| 参数         | 类型    | 位置 | 说明           |
| ------------ | ------- | ---- | -------------- |
| taskExecId   | Integer | body | 执行工单任务Id |
| id           | String  | body | 工单实例ID     |
| assigneeId   | Integer | body | 受理人Id       |
| assigneeName | String  | body | 受理人姓名     |


请求实例：

```
{
  "taskExecId": 1,
  "id":"861d9fb10ca1d9bedeb245b91a0ab343",
  "assigneeId":13239,
  "assigneeName":"张洪国"
}
```

#### （14）移动端-完成工单

请求地址：

| URL    | /api/bmpworkflow/task/mob/complete |
| ------ | ---------------------------------- |
| Method | POST                               |

请求参数：

| 参数         | 类型   | 位置 | 说明          |
| ------------ | ------ | ---- | ------------- |
| formExtra    | Set    | body | 表单          |
| -- key       | String | body | 表单字段key   |
| -- value     | String | body | 表单字段value |
| id           | String | body | 工单实例ID    |
| instPictures | List   | body | 完成图片      |
| instDesc     | String | body | 完成描述      |
| triggerId    | String | body | 关联工单      |


请求实例：

```
{
    "formExtra": {
        "fd_1681365242202": 1,
        "fd_1681365244173": 0,
        "fd_1681365245758": 0
    },
    "instPictures": [
        "http://192.165.1.62:9881/group1/M00/00/2A/wKUBPmQ3spyANPm8AAAGHnj3Yng817.png"
    ],
    "instDesc": "121212",
    "id": "044c91189942256b27f94cdea6db8872"
}
```

#### （15）移动端-查询转单+抢单数

| URL    | /api/bmpworkflow/task/mob/tranAndGrapCount |
| ------ | ------------------------------------------ |
| Method | GET                                        |

请求参数：无

返回参数：

| 参数           | 类型    | 说明                            |
| -------------- | ------- | ------------------------------- |
| data      | Integer | 转单+抢单数  |

#### （16）移动端-关联工单下拉框

描述：查询关联工单

| URL    | /api/bmpworkflow/order/mob/pullList/{id} |
| ------ | ---------------------------------------- |
| Method | GET                                      |

请求参数：

| 参数 | 类型   | 位置 | 说明       |
| ---- | ------ | ---- | ---------- |
| id   | String | path | 工单定义Id |

返回参数：

| 参数      | 类型   | 说明         |
| --------- | ------ | ------------ |
| triggerId | String | 关联工单Id   |
| name      | String | 关联工单名称 |

### 2. 投诉

#### （1）移动端-投诉列表

描述：查询投诉列表，时间降序

| URL    | /api/bmpworkflow/complain/mob/list |
| ------ | ---------------------------------- |
| Method | GET                                |

请求参数：

| 参数   | 类型   | 位置  | 说明                        |
| ------ | ------ | ----- | --------------------------- |
| status | String | query | 状态（已反馈：1 待反馈：0） |

返回参数：

| 参数          | 类型    | 说明                            |
| ------------- | ------- | ------------------------------- |
| createTime    | String  | 创建时间（yyyy-MM-dd HH:mm:ss） |
| updateTime    | String  | 反馈时间（yyyy-MM-dd HH:mm:ss） |
| title         | String  | 投诉标题                        |
| content       | String  | 投诉内容                        |
| initiatorName | String  | 投诉人姓名                      |
| operator      | String  | 反馈人姓名                      |
| sequenceNo    | String  | 投诉编号                        |
| deptName      | String  | 部门名称                        |
| deptId        | Integer | 部门Id                          |
| feedback      | String  | 反馈内容                        |
| status        | Integer | 投诉状态                        |

#### （2）发起投诉

请求地址：

| URL    | /api/bmpworkflow/complain/create |
| ------ | -------------------------------- |
| Method | POST                             |

请求参数：

| 参数           | 类型   | 位置 | 说明         |
| -------------- | ------ | ---- | ------------ |
| deptId     | Integer | body | 部门Id     |
| deptName     | String | body | 部门名称     |
| title | String | body | 投诉标题     |
| content     | String | body | 投诉内容     |


请求实例：

```
{
  "content": "投诉内容3",
  "deptId": 1503,
  "deptName": "财务管理部",
  "title": "投诉标题3"
}
```

### 3、班组

#### （1）移动端-值班人员

| URL    | /api/bmpworkflow/eod/mob/list |
| ------ | ----------------------------- |
| Method | GET                           |

请求参数：

| 参数      | 类型    | 位置  | 说明                      |
| --------- | ------- | ----- | ------------------------- |
| status    | Integer | query | 值班状态：1值班中 0休息中 |
| staffName | String  | query | 姓名                      |
| mobile    | String  | query | 电话                      |
| eodDate   | String  | query | 值班日期（yyyy-MM-dd）    |



