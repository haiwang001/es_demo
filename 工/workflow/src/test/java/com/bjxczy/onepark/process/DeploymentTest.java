package com.bjxczy.onepark.process;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bjxczy.onepark.WorkflowApplication;
import com.bjxczy.onepark.workflow.process.core.bpmn.BpmnModelConvert;

@SpringBootTest(classes = { WorkflowApplication.class })
public class DeploymentTest {
	
	@Autowired
	private RepositoryService repositoryService;
	
	@Test
	public void deploy() {
		Deployment deploy = repositoryService.createDeployment().addClasspathResource("activiti/MyProcess.bpmn").deploy();
		ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
//		log.info("部署ID：{}", deploy.getId());
//		log.info("流程Key：{}", pd.getKey());
		System.out.println("流程ID："+deploy.getId());
		System.out.println("流程Key："+pd.getKey());
	}
	
	@Test
	public void modelDeployTest() {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:activiti=\"http://activiti.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://www.activiti.org/test\"><process id=\"modelProcess\" name=\"modelProcess\" isExecutable=\"true\"><startEvent id=\"startevent1\" name=\"开始\"><documentation>MyStart</documentation></startEvent><exclusiveGateway id=\"exclusivegateway1\" name=\"Exclusive Gateway\"></exclusiveGateway><userTask id=\"usertask1\" name=\"User Task\"></userTask><userTask id=\"usertask2\" name=\"User Task\"><documentation>1111</documentation></userTask><endEvent id=\"endevent1\" name=\"End\"></endEvent><sequenceFlow id=\"flow1\" sourceRef=\"startevent1\" targetRef=\"exclusivegateway1\"></sequenceFlow><sequenceFlow id=\"flow2\" sourceRef=\"exclusivegateway1\" targetRef=\"usertask1\"><documentation>请假大于3</documentation><conditionExpression xsi:type=\"tFormalExpression\"><![CDATA[${num} > 3]]></conditionExpression></sequenceFlow><sequenceFlow id=\"flow3\" sourceRef=\"exclusivegateway1\" targetRef=\"usertask2\"></sequenceFlow><sequenceFlow id=\"flow4\" sourceRef=\"usertask1\" targetRef=\"endevent1\"></sequenceFlow><sequenceFlow id=\"flow5\" sourceRef=\"usertask2\" targetRef=\"endevent1\"></sequenceFlow></process><bpmndi:BPMNDiagram id=\"BPMNDiagram_myProcess\"><bpmndi:BPMNPlane bpmnElement=\"myProcess\" id=\"BPMNPlane_myProcess\"><bpmndi:BPMNShape bpmnElement=\"startevent1\" id=\"BPMNShape_startevent1\"><omgdc:Bounds height=\"35.0\" width=\"35.0\" x=\"170.0\" y=\"303.0\"></omgdc:Bounds></bpmndi:BPMNShape><bpmndi:BPMNShape bpmnElement=\"exclusivegateway1\" id=\"BPMNShape_exclusivegateway1\"><omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"290.0\" y=\"300.0\"></omgdc:Bounds></bpmndi:BPMNShape><bpmndi:BPMNShape bpmnElement=\"usertask1\" id=\"BPMNShape_usertask1\"><omgdc:Bounds height=\"55.0\" width=\"105.0\" x=\"460.0\" y=\"220.0\"></omgdc:Bounds></bpmndi:BPMNShape><bpmndi:BPMNShape bpmnElement=\"usertask2\" id=\"BPMNShape_usertask2\"><omgdc:Bounds height=\"55.0\" width=\"105.0\" x=\"460.0\" y=\"380.0\"></omgdc:Bounds></bpmndi:BPMNShape><bpmndi:BPMNShape bpmnElement=\"endevent1\" id=\"BPMNShape_endevent1\"><omgdc:Bounds height=\"35.0\" width=\"35.0\" x=\"670.0\" y=\"320.0\"></omgdc:Bounds></bpmndi:BPMNShape><bpmndi:BPMNEdge bpmnElement=\"flow1\" id=\"BPMNEdge_flow1\"><omgdi:waypoint x=\"205.0\" y=\"320.0\"></omgdi:waypoint><omgdi:waypoint x=\"290.0\" y=\"320.0\"></omgdi:waypoint></bpmndi:BPMNEdge><bpmndi:BPMNEdge bpmnElement=\"flow2\" id=\"BPMNEdge_flow2\"><omgdi:waypoint x=\"310.0\" y=\"300.0\"></omgdi:waypoint><omgdi:waypoint x=\"310.0\" y=\"247.0\"></omgdi:waypoint><omgdi:waypoint x=\"460.0\" y=\"247.0\"></omgdi:waypoint></bpmndi:BPMNEdge><bpmndi:BPMNEdge bpmnElement=\"flow3\" id=\"BPMNEdge_flow3\"><omgdi:waypoint x=\"310.0\" y=\"340.0\"></omgdi:waypoint><omgdi:waypoint x=\"310.0\" y=\"407.0\"></omgdi:waypoint><omgdi:waypoint x=\"460.0\" y=\"407.0\"></omgdi:waypoint></bpmndi:BPMNEdge><bpmndi:BPMNEdge bpmnElement=\"flow4\" id=\"BPMNEdge_flow4\"><omgdi:waypoint x=\"565.0\" y=\"247.0\"></omgdi:waypoint><omgdi:waypoint x=\"687.0\" y=\"247.0\"></omgdi:waypoint><omgdi:waypoint x=\"687.0\" y=\"320.0\"></omgdi:waypoint></bpmndi:BPMNEdge><bpmndi:BPMNEdge bpmnElement=\"flow5\" id=\"BPMNEdge_flow5\"><omgdi:waypoint x=\"565.0\" y=\"407.0\"></omgdi:waypoint><omgdi:waypoint x=\"687.0\" y=\"407.0\"></omgdi:waypoint><omgdi:waypoint x=\"687.0\" y=\"355.0\"></omgdi:waypoint></bpmndi:BPMNEdge></bpmndi:BPMNPlane></bpmndi:BPMNDiagram></definitions>";
		BpmnModel model = BpmnModelConvert.xml2Model(xml, null, null);
		Deployment deploy = repositoryService.createDeployment()
				.name("modelProcess")
				.addBpmnModel("modelProcess.bpmn", model).deploy();
		System.out.println("流程ID：" + deploy.getId());
	}

}
