package com.bjxczy.onepark.process;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
public class RestTemplateTest {
	
	@Autowired
	@Qualifier("rpcRestTemplate")
	private RestTemplate restTemplate;
	
	@Test
	public void test() {
		String url = "http://organization/rpc/staff/byId/13164";
		ResponseEntity<Object> obj = restTemplate.getForEntity(url, Object.class);
		System.out.println(obj);
	}

}
