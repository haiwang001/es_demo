package com.bjxczy.onepark.process.facade;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;
import com.bjxczy.onepark.workflow.form.mapper.FormItemMapper;
import com.bjxczy.onepark.workflow.process.facade.IFormFacade;

@SpringBootTest
public class FormFacadeTest {
	
	@Autowired
	private IFormFacade formFacade;
	@Autowired
	private FormItemMapper formItemMapper;
	
	@Test
	public void igoreDeleteTest() {
		String formId = "756b977e45f029208acc1a8011bd295d";
		FormdefPO form = formFacade.getFormdefById(formId);
		System.out.println(form);
		List<FormItemPO> list = formFacade.listByFormId(formId);
		System.out.println(list);
	}
	
	@Test
	public void logicDeleteTest() {
		String formId = "756b977e45f029208acc1a8011bd295d";
		formItemMapper.deleteByFormId(formId);
	}

}
