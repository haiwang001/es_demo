package com.bjxczy.onepark.process;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bjxczy.onepark.workflow.process.core.engine.ProcessPreEngine;
import com.bjxczy.onepark.workflow.process.core.engine.ProcessPreEngine.Line;
import com.bjxczy.onepark.workflow.process.core.engine.ProcessPreEngine.Node;
import com.bjxczy.onepark.workflow.process.core.engine.ProcessPreEngineFactory;

@SpringBootTest
public class ProcessPreEngineTest {

	@Autowired
	private ProcessPreEngineFactory processPreEngineFactory;

	@Test
	public void preTest() {
		Node n0 = new Node("start1", "startEvent", "line1"), n1 = new Node("task1", "userTask", null),
				n2 = new Node("task2", "userTask", null), n3 = new Node("end1", "endEvent", null);
		Line l0 = new Line("line0", "start1", "task1", "${testExpressionHandler.isBlank(str)}"),
				l1 = new Line("line1", "start1", "task2", null),
				l2 = new Line("line2", "task1", "end1", null), l3 = new Line("line3", "task2", "end1", null);
		Map<String, Object> variables = new HashMap<>();
		variables.put("num", 3);
		variables.put("str", "111");
		ProcessPreEngine instance = processPreEngineFactory.createProcessPreInstance(Arrays.asList(n0, n1, n2, n3),
				Arrays.asList(l0, l1, l2, l3));
		List<Node> history = instance.startProcess(variables);
		System.out.println(history);
	}

}
