package com.bjxczy.onepark.process;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProcessTest {
	
	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private TaskService taskService;
	
	@Test
	public void startProcess() {
		Map<String, Object> variables = new HashMap<>();
		variables.put("num", 5);
		ProcessInstance instance = runtimeService.startProcessInstanceByKey("modelProcess", variables);
		System.out.println(instance.getId());
	}
	
	@Test
	public void taskComplete() {
		taskService.complete("57509");
	}
	

}
