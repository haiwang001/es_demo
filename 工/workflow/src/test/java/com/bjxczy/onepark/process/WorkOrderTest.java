package com.bjxczy.onepark.process;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bjxczy.core.web.workflow.WorkOrderService;
import com.bjxczy.onepark.common.model.workflow.DescribeItem;
import com.bjxczy.onepark.common.model.workflow.ImageDescribeValue;
import com.bjxczy.onepark.common.model.workflow.PushOrderPayload;
import com.bjxczy.onepark.common.model.workflow.TextDescribeValue;

import cn.hutool.core.collection.CollectionUtil;

@SpringBootTest
public class WorkOrderTest {
	
	@Autowired
	private WorkOrderService workOrderService;
	
	 // 工单配置ID，根据业务在工单管理模块定义
    private String id = "78f41292ccfb21efc9199c0611d8aaad";
    
    @Test
    public void pushOrder() {
        // 简单工单
        workOrderService.pushOrder(id, "测试工单");
    }
    
    @Test
    public void pushOrder2() {
        // 复杂描述工单
        DescribeItem d_0 = new DescribeItem("申请人", new TextDescribeValue("李梦"));
        List<String> urls = CollectionUtil.newArrayList("https://dev.xconepark.com/images/xxx.jpg");
        DescribeItem d_1 = new DescribeItem("照片", new ImageDescribeValue(urls));
        List<DescribeItem> items = CollectionUtil.newArrayList(d_0, d_1);
        workOrderService.pushOrder(id, "申请已通过，请根据信息创建", items);
    }
    
    @Test
    public void pushOrder3() {
    	// 设备报修工单
    	PushOrderPayload payload = new PushOrderPayload(id, "XXX设备报修");
    	payload.setDeviceId("efdicialdfqoinvadsf");
    	workOrderService.pushOrder(payload);
    }

}
