package com.bjxczy.onepark.process;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bjxczy.core.redisson.delayed.RDelayedPayload;
import com.bjxczy.onepark.workflow.process.core.listener.UserTaskDelayedQueueImpl;

@SpringBootTest
public class UserTaskDelayedQueueTest {
	
	@Autowired
	private UserTaskDelayedQueueImpl impl;
	
	@Test
	public void offerTest() throws InterruptedException {
		System.out.println("启动时间：" + new Date());
		RDelayedPayload payload = new RDelayedPayload("110");
		impl.offer(payload, 30, TimeUnit.SECONDS);
//		impl.removeById(payload);
		TimeUnit.SECONDS.sleep(40);
	}

}
