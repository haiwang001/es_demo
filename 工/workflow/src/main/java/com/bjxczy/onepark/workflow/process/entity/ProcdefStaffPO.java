package com.bjxczy.onepark.workflow.process.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@TableName("wf_re_procdef_staff")
public class ProcdefStaffPO {
	
	@TableId(type = IdType.ASSIGN_UUID)
	private String id;
	
	private String procdefId;
	
	private Integer staffId;

}
