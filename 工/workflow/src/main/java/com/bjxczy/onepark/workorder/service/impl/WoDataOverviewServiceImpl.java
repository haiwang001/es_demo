package com.bjxczy.onepark.workorder.service.impl;

import com.bjxczy.onepark.workorder.mapper.WoTaskInstanceMapper;
import com.bjxczy.onepark.workorder.mapper.WoTaskMapper;
import com.bjxczy.onepark.workorder.pojo.dto.WoDataOverviewDTO;
import com.bjxczy.onepark.workorder.service.WoDataOverviewService;
import com.bjxczy.onepark.workorder.utils.DateTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName WoDataOverviewServiceImpl
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/4 17:49
 * @Version 1.0
 */
@Service
public class WoDataOverviewServiceImpl implements WoDataOverviewService {

    @Autowired
    private WoTaskInstanceMapper woTaskInstanceMapper;

    @Autowired
    private WoTaskMapper woTaskMapper;

    @Override
    public Map taskCounts(WoDataOverviewDTO woDataOverviewDTO) {
        //派发工单数
        woDataOverviewDTO.setStatusList(Arrays.asList("1", "2", "3", "5","4"));
        Integer total = woTaskInstanceMapper.selectcounts(woDataOverviewDTO);
        //完成工单数 （已验收 3）
        woDataOverviewDTO.setStatusList(Arrays.asList("3"));
        Integer comleteCount = woTaskInstanceMapper.selectcounts(woDataOverviewDTO);
        //未完成工单数 （进行中1 已完成2）
        woDataOverviewDTO.setStatusList(Arrays.asList("1", "2"));
        Integer unCompleteCount = woTaskInstanceMapper.selectcounts(woDataOverviewDTO);
        //已过期 4
        woDataOverviewDTO.setStatusList(Arrays.asList("4"));
        Integer expireCount = woTaskInstanceMapper.selectcounts(woDataOverviewDTO);
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        map.put("comleteCount", comleteCount);
        map.put("unCompleteCount", unCompleteCount);
        map.put("expireCount", expireCount);
        return map;
    }

    @Override
    public Map<String, Object> chartList(WoDataOverviewDTO woDataOverviewDTO) {
        String dateType = woDataOverviewDTO.getDateType();
        String startTime = woDataOverviewDTO.getStartTime();
        String endTime = woDataOverviewDTO.getEndTime();
        if ("1".equals(dateType)) {//y
            woDataOverviewDTO.setDateList(DateTools.getYearBetweenDate(startTime, endTime));
        } else if ("2".equals(dateType)) {//m
            woDataOverviewDTO.setDateList(DateTools.getMonthBetweenDate(startTime, endTime));
        } else {
            //d
            woDataOverviewDTO.setDateList(DateTools.getDaysBetweenDate(startTime, endTime));
        }
        //已完成 1
        woDataOverviewDTO.setStatus("1");
        List<Map<String, Object>> completeList = woTaskInstanceMapper.chartList(woDataOverviewDTO);
        //未完成 0
        woDataOverviewDTO.setStatus("0");
        List<Map<String, Object>> unCompleteList = woTaskInstanceMapper.chartList(woDataOverviewDTO);
        //已过期 2
        woDataOverviewDTO.setStatus("2");
        List<Map<String, Object>> expireList = woTaskInstanceMapper.chartList(woDataOverviewDTO);
        Map<String, Object> map = new HashMap<>();
        map.put("completeList", completeList);
        map.put("unCompleteList", unCompleteList);
        map.put("expireList", expireList);
        return map;
    }

    @Override
    public List<Map<String, Object>> listByDepart(WoDataOverviewDTO woDataOverviewDTO) {
        //统计完成工单 按部门统计（已验收 3）
        return woTaskInstanceMapper.listByDepart(woDataOverviewDTO);
    }

    @Override
    public Map<String, Object> chartListByDepart(WoDataOverviewDTO woDataOverviewDTO) {
        HashMap<String, Object> map = new HashMap<>();
        String dateType = woDataOverviewDTO.getDateType();
        String startTime = woDataOverviewDTO.getStartTime();
        String endTime = woDataOverviewDTO.getEndTime();
        if ("1".equals(dateType)) {//y
            woDataOverviewDTO.setDateList(DateTools.getYearBetweenDate(startTime, endTime));
        } else if ("2".equals(dateType)) {//m
            woDataOverviewDTO.setDateList(DateTools.getMonthBetweenDate(startTime, endTime));
        } else {
            //d
            woDataOverviewDTO.setDateList(DateTools.getDaysBetweenDate(startTime, endTime));
        }
        if (null == woDataOverviewDTO.getAssignDeptIds()) {
            List<Integer> deptIds = woTaskInstanceMapper.listDeptId(woDataOverviewDTO);
            woDataOverviewDTO.setAssignDeptIds(deptIds);
        }
        for (Integer assignDeptId : woDataOverviewDTO.getAssignDeptIds()) {
            if (null == assignDeptId) {
                return null;
            } else {
                woDataOverviewDTO.setAssignDeptIds(Arrays.asList(assignDeptId));
                String deptName = woTaskMapper.selDeptName(assignDeptId);
                map.put(deptName, woTaskInstanceMapper.chartListByDepart(woDataOverviewDTO));
            }
        }
        if (map.isEmpty()){
            return null;
        }
        return map;
    }
}
