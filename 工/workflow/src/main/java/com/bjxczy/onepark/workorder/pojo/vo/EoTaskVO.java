package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoTaskPO;
import lombok.Data;

/**
 * @ClassName EoTaskVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 15:28
 * @Version 1.0
 */
@Data
public class EoTaskVO extends EoTaskPO {
    // 1、酒店
    private EoHotelVO eoHotelVO;
    // 2、餐饮
    private EoCateringVO eoCateringVO;
    // 3、安全管理部
    private EoVehicleVO eoVehicleVO;
}
