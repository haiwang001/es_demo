package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoVisitInfoPO;
import lombok.Data;

/**
 * @ClassName EoVisitInfoVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/25 14:14
 * @Version 1.0
 */
@Data
public class EoVisitInfoVO extends EoVisitInfoPO {

    private String intervieweeName;

    private String areaName;
}
