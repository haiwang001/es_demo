package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.WoOrderdefPO;
import lombok.Data;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @ClassName WoOrderdefVo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/30 15:02
 * @Version 1.0
 */
@Data
public class WoOrderdefVO extends WoOrderdefPO {
    //工单类型名
    private String orderTypeName;
    //业务模块
    private String businessTypeName;
    //紧急程度
    private String orderLevelName;
    //表单
    private String formName;

    private String taskTypeName;

    //关联表单
    private LinkedHashSet<String> triggerNames;

    //联动策略名称
    private String linkStrategyName;
}
