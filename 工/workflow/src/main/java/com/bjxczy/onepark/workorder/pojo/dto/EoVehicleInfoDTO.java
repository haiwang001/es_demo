package com.bjxczy.onepark.workorder.pojo.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.po.EoVehicleInfoPO;
import lombok.Data;

/**
 * @ClassName EoVehicleInfoDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/24 16:47
 * @Version 1.0
 */
@Data
public class EoVehicleInfoDTO extends EoVehicleInfoPO {

    /**
     * EO单Id
     */
    @ExcelIgnore
    private String eoId;

    @ExcelIgnore
    private Integer pageNum = 1;

    @ExcelIgnore
    private Integer pageSize = 10;

    public <T> Page<T> toPage() {
        return new Page<>(pageNum, pageSize);
    }
}
