package com.bjxczy.onepark.workflow.process.core.listener;

import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.excel.util.StringUtils;
import com.bjxczy.core.web.message.MessageService;
import com.bjxczy.onepark.common.model.workbench.TemplateMessagePayload;
import com.bjxczy.onepark.workflow.process.core.constant.ProcessConstant;
import com.bjxczy.onepark.workflow.process.core.constant.TaskListenerType;
import com.bjxczy.onepark.workflow.process.core.constant.TaskVariables;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.entity.RuProcessPO;
import com.bjxczy.onepark.workflow.process.facade.ISubmodulesFacade;
import com.bjxczy.onepark.workflow.process.service.HiTaskService;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;
import com.bjxczy.onepark.workflow.process.service.RuProcessService;
import com.bjxczy.onepark.workflow.process.util.ProcessUtils;

import lombok.extern.slf4j.Slf4j;

@Component(TaskListenerType.PROCESS_END_LISTENER)
@Slf4j
public class ProcessEndListener implements ExecutionListener {

	@Autowired
	private RuProcessService ruProcessService;
	@Autowired
	private ProcessDefinitionService procdefService;
	@Autowired
	private HiTaskService hiTaskService;
	@Autowired
	private MessageService messageService;
	@Autowired
	private ISubmodulesFacade submodulesFacade;

	@Override
	public void notify(DelegateExecution execution) {
		RuProcessPO process = ruProcessService.getByInstId(execution.getProcessInstanceId());
		Map<String, Object> variables = execution.getVariables();
		boolean earlyEnd = variables.containsKey(TaskVariables.VAR_EARLY_END);
		String earlyEndMessage = (String) variables.get(TaskVariables.VAR_EARLY_END_MESSAGE);
		if (process != null) {
			ProcdefPO def = procdefService.getByDeployId(process.getProcDefKey());
			// 调用子模块处理业务流程
			if (!earlyEnd && ProcessUtils.isBusinessProcess(def)) {
				submodulesFacade.approved(def.getServiceName(), def.getDeployId(), variables, process.getStaffId());
			}
			// 更新流程状态
			process.setApplyStatus(earlyEnd ? ProcessConstant.APPLY_STATUS_CLOSE : ProcessConstant.APPLY_STATUS_PASS);
			if (StringUtils.isNotBlank(earlyEndMessage)) {
				process.setCloseMessage(earlyEndMessage);
			}
			ruProcessService.updateById(process);
			// 保存流程日志
			hiTaskService.saveOnEnd(process);
			// 向申请人发送消息
			variables.put("procdefName", process.getProcDefName()); // 设置流程名称
			variables.put("applyStatusName",
					process.getApplyStatus() == ProcessConstant.APPLY_STATUS_PASS ? "通过" : "关闭");
			String staffId = (String) variables.get(TaskVariables.VAR_INITIATOR_KEY);
			String eventCode = ProcessUtils.getEventCode(def, ProcessConstant.EVENT_END_SUFFIX);
			TemplateMessagePayload payload = new TemplateMessagePayload(Integer.parseInt(staffId),
					String.valueOf(process.getId()), eventCode, variables);
			messageService.pushTemplateMessage(payload);
		} else {
			log.error("[ProcessEnd] 未知流程:{}", execution);
		}
	}

	private static final long serialVersionUID = 5901123413224845492L;

}
