package com.bjxczy.onepark.workflow.process.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@TableName("wf_re_procdef_version")
public class ProcdefVersionPO extends UserOperator {
	
	private static final long serialVersionUID = -1078618914943396907L;

	@TableId(type = IdType.ASSIGN_UUID)
	private String id;
	
	private String procdefId;
	
	private String versionName;
	
	private String remark;
	
	private String formId;
	
	private String sourceCode;

	public ProcdefVersionPO(String procdefId, String versionName, String remark, String formId) {
		super();
		this.procdefId = procdefId;
		this.versionName = versionName;
		this.remark = remark;
		this.formId = formId;
	}
	
	public void edit(String versionName, String remark, String formId, String sourceCode) {
		this.versionName = versionName;
		this.remark = remark;
		this.formId = formId;
		this.sourceCode = sourceCode;
	}
	
}
