package com.bjxczy.onepark.workflow.process.core.condition;

import lombok.Data;

@Data
public class FormItemExpression {

	private String oper;

	private String formKey;

	private String value;

	public String toConditionExpression() {
		return "${" + formKey + oper + value + "}";
	}

}
