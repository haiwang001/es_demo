package com.bjxczy.onepark.workflow.process.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefStaffPO;

public interface ProcdefStaffService extends IService<ProcdefStaffPO> {

	void saveByProcess(ProcdefPO po, List<Integer> staffIds);

	List<StaffInformation> listStaffInfo(String processId);

	void removeByProcdefId(String procdefId);

	List<ProcdefStaffPO> listByDefId(String id);

}
