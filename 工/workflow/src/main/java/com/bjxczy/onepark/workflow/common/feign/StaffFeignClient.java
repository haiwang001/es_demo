package com.bjxczy.onepark.workflow.common.feign;

import org.springframework.cloud.openfeign.FeignClient;

import com.bjxczy.core.feign.client.organization.BaseStaffFeignClient;

@FeignClient("organization")
public interface StaffFeignClient extends BaseStaffFeignClient {

}
