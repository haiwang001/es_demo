package com.bjxczy.onepark.workflow.process.pojo.vo;

import org.springframework.beans.BeanUtils;

import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProcdefVO extends ProcdefPO {
	
	private static final long serialVersionUID = 8743129490431739036L;

	private String deployTypeName;
	
	private String applyRangeName;
	
	private String versionName;
	
	private String woName;
	
	public ProcdefVO(ProcdefPO po) {
		BeanUtils.copyProperties(po, this);
	}

}
