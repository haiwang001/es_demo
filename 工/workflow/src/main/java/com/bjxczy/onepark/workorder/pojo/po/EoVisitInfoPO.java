package com.bjxczy.onepark.workorder.pojo.po;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName EoVisitInfoPO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/24 17:05
 * @Version 1.0
 */
@Data
@TableName("eo_visit_info")
public class EoVisitInfoPO {

    @ExcelIgnore
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ExcelIgnore
    @ApiModelProperty(value = "EO单Id")
    private String eoId;

    @ExcelProperty("访客名称")
    @ApiModelProperty(value = "访客名称")
    private String name;

    @ExcelProperty("证件类型")
    @ApiModelProperty(value = "证件类型")
    private String certificateType;

    @ExcelProperty("访客证件")
    @ApiModelProperty(value = "访客证件")
    private String certificateNo;

    @ExcelProperty("访客手机")
    @ApiModelProperty(value = "访客手机")
    private String mobile;

    @ExcelIgnore
    @ApiModelProperty(value = "访客照片地址")
    private String photoUrl;

    @ExcelProperty("到访事由")
    @ApiModelProperty(value = "到访事由")
    private String visitReason;

    @ExcelIgnore
    @ApiModelProperty(value = "到访园区")
    private Integer areaId;

    @ExcelProperty("备注")
    @ApiModelProperty(value = "备注")
    private String remark;

    @ExcelIgnore
    @ApiModelProperty(value = "接访人")
    private Integer interviewee;
}
