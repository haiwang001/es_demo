package com.bjxczy.onepark.workflow.process.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName("wf_re_procdef_node")
public class ProcdefNodePO extends BaseBpmnElement {
	
	private String defaultFlow;
	
	private String candidateType;
	
	private String candidateValue;
	
	private String assigneeIsNull;
	
	private String assigneeValue;
	
	private Integer expireTime;
	
	private String rejectType;

}
