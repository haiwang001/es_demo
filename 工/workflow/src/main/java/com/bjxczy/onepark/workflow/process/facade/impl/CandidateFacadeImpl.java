package com.bjxczy.onepark.workflow.process.facade.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.common.model.organization.QueryStaffParams;
import com.bjxczy.onepark.common.model.organization.QueryTopStaffParams;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workflow.process.facade.ICandidateFacade;
import com.bjxczy.onepark.workflow.process.pojo.dto.CandidateStaff;
import com.bjxczy.onepark.workflow.process.pojo.dto.LabelDTO;

@Component
public class CandidateFacadeImpl implements ICandidateFacade {

	@Autowired
	private StaffFeignClient staffFeignClient;

	@Override
	public List<CandidateStaff> invoke(String key, UserInformation loginUser, String candidateValue) {
		List<CandidateStaff> result = null;
		switch (key) {
		case "fixed":
			result = getFixed(candidateValue);
			break;
		case "self_select":
			// 发起人自选的方式，候选人为全部员工，前端实现
			break;
		case "role":
			// TODO 查询角色候选
			break;
		case "supervisor":
			// TODO 园区主管
			break;
		case "rank": // 指定职级
			result = getByRank(candidateValue);
			break;
		case "self_dept_rank": // 员工所在部门某职级
			result = getBySelfDeptRank(loginUser, candidateValue);
			break;
		case "higher_dept_rank": // 员工上级部门某职级
			result = getByHigherDeptRank(loginUser, candidateValue);
			break;
		case "position": // 指定岗位
			result = getByPosition(candidateValue);
			break;
		case "self_dept_position": // 员工所在部门某岗位
			result = getBySelfDeptPosition(loginUser, candidateValue);
			break;
		case "higher_dept_position": // 员工上级部门某岗位
			result = getByHigherDeptPosition(loginUser, candidateValue);
			break;
		default:
			break;
		}
		return result == null ? new ArrayList<>() : result;
	}

	private List<CandidateStaff> toCandidateStaff(List<StaffInformation> staffList) {
		return staffList.stream().map(e -> {
			return new CandidateStaff(e.getId(), e.getFldName(),
					e.getDeptNames().stream().map(d -> d.getDeptName()).collect(Collectors.joining("/")));
		}).collect(Collectors.toList());
	}
	
	@SuppressWarnings("unchecked")
	private <T> List<T> toIdList(List<LabelDTO> list) {
		return list.stream().map(e -> (T) e.getId()).collect(Collectors.toList());
	}

	/**
	 * 指定人员
	 * 
	 * @param candidateValue
	 * @return
	 */
	private List<CandidateStaff> getFixed(String candidateValue) {
		List<LabelDTO> list = JSON.parseArray(candidateValue, LabelDTO.class);
		List<StaffInformation> staffList = staffFeignClient.getStaffBatch(toIdList(list));
		return toCandidateStaff(staffList);
	}

	/**
	 * 指定职级
	 * 
	 * @param candidateValue
	 * @return
	 */
	private List<CandidateStaff> getByRank(String candidateValue) {
		List<Integer> ids = JSON.parseArray(candidateValue, Integer.class);
		List<StaffInformation> list = staffFeignClient.listStaffByParams(new QueryStaffParams().setRankIds(ids));
		return toCandidateStaff(list);
	}

	/**
	 * 员工所在部门某职级
	 * 
	 * @param loginUser
	 * @param candidateValue
	 * @return
	 */
	private List<CandidateStaff> getBySelfDeptRank(UserInformation loginUser, String candidateValue) {
		StaffInformation staff = staffFeignClient.findById(loginUser.getStaffId());
		List<Integer> ids = JSON.parseArray(candidateValue, Integer.class);
		List<StaffInformation> list = staffFeignClient.listStaffByParams(
				new QueryStaffParams().singletonOrganizationId(staff.getOrganizationId()).setRankIds(ids));
		return toCandidateStaff(list);
	}

	/**
	 * 员工上级部门某职级
	 * 
	 * @param loginUser
	 * @param candidateValue
	 * @return
	 */
	private List<CandidateStaff> getByHigherDeptRank(UserInformation loginUser, String candidateValue) {
		List<Integer> ids = JSON.parseArray(candidateValue, Integer.class);
		List<StaffInformation> list = staffFeignClient.listTopDeptStaff(loginUser.getStaffId(),
				new QueryTopStaffParams().setRankIds(ids));
		return toCandidateStaff(list);
	}

	/**
	 * 指定岗位
	 * @param candidateValue
	 * @return
	 */
	private List<CandidateStaff> getByPosition(String candidateValue) {
		List<Integer> ids = JSON.parseArray(candidateValue, Integer.class);
		List<StaffInformation> list = staffFeignClient.listStaffByParams(new QueryStaffParams().setPositionIds(ids));
		return toCandidateStaff(list);
	}

	/**
	 * 员工所在部门指定岗位
	 * @param loginUser
	 * @param candidateValue
	 * @return
	 */
	private List<CandidateStaff> getBySelfDeptPosition(UserInformation loginUser, String candidateValue) {
		StaffInformation staff = staffFeignClient.findById(loginUser.getStaffId());
		List<Integer> ids = JSON.parseArray(candidateValue, Integer.class);
		List<StaffInformation> list = staffFeignClient.listStaffByParams(
				new QueryStaffParams().singletonOrganizationId(staff.getOrganizationId()).setPositionIds(ids));
		return toCandidateStaff(list);
	}

	/**
	 * 员工上级部门指定岗位
	 * @param loginUser
	 * @param candidateValue
	 * @return
	 */
	private List<CandidateStaff> getByHigherDeptPosition(UserInformation loginUser, String candidateValue) {
		List<Integer> ids = JSON.parseArray(candidateValue, Integer.class);
		List<StaffInformation> list = staffFeignClient.listTopDeptStaff(loginUser.getStaffId(),
				new QueryTopStaffParams().setPositionIds(ids));
		return toCandidateStaff(list);
	}

}
