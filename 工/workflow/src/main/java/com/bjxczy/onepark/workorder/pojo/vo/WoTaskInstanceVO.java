package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.WoTaskInstancePO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskVariablesPO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskdefPO;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @ClassName WoTaskInstanceVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/3 15:57
 * @Version 1.0
 */
@Data
public class WoTaskInstanceVO extends WoTaskInstancePO {

    private String mobile;

    private WoOrderdefVO woOrderdefVO;

    private WoTaskdefVO woTaskdefVO;

    private List<WoTaskVariablesPO> formExtra;

    private String formId;

    private String instStatusName;

    private String triggerName;


}
