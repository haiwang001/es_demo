package com.bjxczy.onepark.workflow.process.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefStaffPO;
import com.bjxczy.onepark.workflow.process.mapper.ProcdefStaffMapper;
import com.bjxczy.onepark.workflow.process.service.ProcdefStaffService;

@Service
public class ProcdefStaffServiceImpl extends ServiceImpl<ProcdefStaffMapper, ProcdefStaffPO>
		implements ProcdefStaffService {

	@Autowired
	private StaffFeignClient staffFeignClient;

	@Override
	public void saveByProcess(ProcdefPO po, List<Integer> staffIds) {
		this.lambdaUpdate().eq(ProcdefStaffPO::getProcdefId, po.getId()).remove();
		List<ProcdefStaffPO> list = staffIds.stream().map(e -> new ProcdefStaffPO(null, po.getId(), e))
				.collect(Collectors.toList());
		this.saveBatch(list);
	}

	@Override
	public List<StaffInformation> listStaffInfo(String processId) {
		List<ProcdefStaffPO> list = this.lambdaQuery().eq(ProcdefStaffPO::getProcdefId, processId).list();
		if (list.isEmpty()) {
			return new ArrayList<>();
		}
		List<Integer> ids = list.stream().map(e -> e.getStaffId()).collect(Collectors.toList());
		return staffFeignClient.getStaffBatch(ids);
	}

	@Override
	public void removeByProcdefId(String procdefId) {
		this.lambdaUpdate().eq(ProcdefStaffPO::getProcdefId, procdefId).remove();
	}

	@Override
	public List<ProcdefStaffPO> listByDefId(String procdefId) {
		return lambdaQuery().eq(ProcdefStaffPO::getProcdefId, procdefId).list();
	}

}
