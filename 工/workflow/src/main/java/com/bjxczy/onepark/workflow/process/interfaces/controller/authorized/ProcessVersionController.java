package com.bjxczy.onepark.workflow.process.interfaces.controller.authorized;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.log.LogParam;
import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.CreateVersionDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.UpdateVersionDTO;
import com.bjxczy.onepark.workflow.process.service.ProcessVersionService;

@ApiResourceController
@RequestMapping("/process/{processId}/version")
public class ProcessVersionController {

	@Autowired
	private ProcessVersionService processVersionService;

	@GetMapping(name = "查询版本列表")
	@SysLog(operationType = OperationType.SELECT, operation = "查询流程版本")
	public List<ProcdefVersionPO> list(@PathVariable("processId") String procdefId) {
		return processVersionService.listByProcdefId(procdefId);
	}

	@PostMapping(name = "创建版本")
	@SysLog(operationType = OperationType.INSERT, operation = "创建版本：${dto.version}")
	public void create(@PathVariable("processId") String procdefId, @RequestBody @LogParam CreateVersionDTO dto) {
		processVersionService.create(procdefId, dto);
	}

	@PutMapping(value = "/{versionId}", name = "编辑版本")
	@SysLog(operationType = OperationType.UPDATE, operation = "编辑版本：${dto.version}")
	public void edit(@PathVariable("processId") String procdefId, @PathVariable("versionId") String versionId,
			@RequestBody UpdateVersionDTO dto) {
		processVersionService.edit(procdefId, versionId, dto);
	}

	@DeleteMapping(value = "/{versionId}", name = "删除版本")
	@SysLog(operationType = OperationType.DELETE, operation = "删除版本：${ctx.name}")
	public void remove(@PathVariable("processId") String procdefId, @PathVariable("versionId") String versionId) {
		processVersionService.delete(procdefId, versionId);
	}
	
	@PutMapping(value = "/{versionId}/deploy", name = "部署版本")
	@SysLog(operationType = OperationType.UPDATE, operation = "部署版本：${ctx.name}")
	public void deploy(@PathVariable("processId") String procdefId, @PathVariable("versionId") String versionId) {
		processVersionService.deploy(procdefId, versionId);
	}

}
