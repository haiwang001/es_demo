package com.bjxczy.onepark.workflow.process.pojo.vo;

import java.util.Date;

import lombok.Data;

@Data
public class TaskVO {
	
	private String taskId;
	
	private String taskName;
	
	private Date taskTime;
	
	private Integer staffId;
	
	private String employeeNo;
	
	private String staffName;
	
	private String mobile;
	
	private String deptName;
	
	private Date submitTime;
	
	private String procdefName;

}
