package com.bjxczy.onepark.workorder.pojo.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.po.EoTaskPO;
import lombok.Data;

/**
 * @ClassName EoTaskDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 14:27
 * @Version 1.0
 */
@Data
public class EoTaskDTO extends EoTaskPO {

    private String sSTime;

    private String eSTime;

    private String sETime;

    private String eETime;

    private EoHotelDTO eoHotelDTO;

    private EoMettingDTO eoMettingDTO;

    private EoCateringDTO eoCateringDTO;

    private EoVehicleDTO eoVehicleDTO;

    private Integer pageNum = 1;

    private Integer pageSize = 10;

    public <T> Page<T> toPage() {
        return new Page<>(pageNum, pageSize);
    }

}
