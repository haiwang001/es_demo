package com.bjxczy.onepark.workflow.form.entity;

import java.util.Map;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.bjxczy.core.web.base.LogicDelete;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@TableName(value = "wf_form_item", autoResultMap = true)
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class FormItemPO extends LogicDelete {
	
	private static final long serialVersionUID = 6504469386783551265L;

	@TableId(type = IdType.ASSIGN_UUID)
	private String id;
	
	private String formId;
	
	private String formKey;
	
	private String category;
	
	private String label;
	
	private String type;
	
	private Integer required;
	
	private Integer fixed;
	
	private Integer sort;
	
	@TableField(typeHandler = FastjsonTypeHandler.class)
	private Map<String, Object> extra;

}
