package com.bjxczy.onepark.workflow.process.service.impl;

import java.util.Date;
import java.util.List;

import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workflow.process.entity.HiTaskPO;
import com.bjxczy.onepark.workflow.process.entity.RuProcessPO;
import com.bjxczy.onepark.workflow.process.mapper.HiTaskMapper;
import com.bjxczy.onepark.workflow.process.pojo.dto.RejectTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.TaskCompleteDTO;
import com.bjxczy.onepark.workflow.process.service.HiTaskService;

@Service
public class HiTaskServiceImpl extends ServiceImpl<HiTaskMapper, HiTaskPO> implements HiTaskService {

	@Autowired
	private StaffFeignClient staffFeignClient;

	@Override
	public void saveOnStart(RuProcessPO process) {
		HiTaskPO entity = new HiTaskPO();
		entity.setProcessId(process.getId());
		entity.setStaffId(process.getStaffId());
		entity.setEmployeeNo(process.getEmployeeNo());
		entity.setOperator(process.getStaffName());
		entity.setApprovalName("提交");
		entity.setApprovalStatus(1);
		entity.setCategory("start");
		entity.setCreateTime(new Date());
		this.save(entity);
	}

	@Override
	public void saveOnComplete(RuProcessPO process, TaskCompleteDTO dto, Task task, UserInformation user) {
		HiTaskPO entity = new HiTaskPO();
		entity.setProcessId(process.getId());
		entity.setTaskId(task.getId());
		entity.setStaffId(user.getStaffId());
		entity.setEmployeeNo(user.getEmployeeNo());
		entity.setOperator(user.getStaffName());
		entity.setApprovalName(task.getName());
		entity.setApprovalText(dto.getApprovalText());
		entity.setApprovalStatus(1);
		entity.setCategory(task.getCategory());
		entity.setCreateTime(new Date());
		this.save(entity);
	}

	@Override
	public void saveOnEnd(RuProcessPO process) {
		HiTaskPO entity = new HiTaskPO();
		entity.setProcessId(process.getId());
		entity.setStaffId(process.getStaffId());
		entity.setEmployeeNo(process.getEmployeeNo());
		entity.setOperator(process.getStaffName());
		entity.setApprovalName("结束");
		entity.setApprovalStatus(1);
		entity.setCategory("end");
		entity.setCreateTime(new Date());
		this.save(entity);
	}

	@Override
	public List<HiTaskPO> listByProcessId(Integer processId) {
		return lambdaQuery().eq(HiTaskPO::getProcessId, processId).list();
	}

	@Override
	public HiTaskPO getByTask(Task task) {
		StaffInformation staff = staffFeignClient.findById(Integer.parseInt(task.getAssignee()));
		HiTaskPO entity = new HiTaskPO();
		entity.setTaskId(task.getId());
		entity.setApprovalName(task.getName());
		entity.setCategory(task.getCategory());
		entity.setStaffId(staff.getId().intValue());
		entity.setOperator(staff.getFldName());
		entity.setEmployeeNo(staff.getEmployeeNo());
		entity.setApprovalStatus(-1);
		return entity;
	}

	@Override
	public void saveOnReject(RuProcessPO process, RejectTaskDTO dto, Task task, UserInformation user) {
		HiTaskPO entity = new HiTaskPO();
		entity.setProcessId(process.getId());
		entity.setTaskId(task.getId());
		entity.setStaffId(user.getStaffId());
		entity.setEmployeeNo(user.getEmployeeNo());
		entity.setOperator(user.getStaffName());
		entity.setApprovalName(task.getName());
		entity.setApprovalText(dto.getApprovalText());
		entity.setApprovalStatus(0); // 拒绝任务
		entity.setCategory(task.getCategory());
		entity.setCreateTime(new Date());
		this.save(entity);
	}

}
