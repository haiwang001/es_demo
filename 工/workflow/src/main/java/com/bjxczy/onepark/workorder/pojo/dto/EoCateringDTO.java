package com.bjxczy.onepark.workorder.pojo.dto;

import com.bjxczy.onepark.workorder.pojo.po.EoCateringPO;
import com.bjxczy.onepark.workorder.pojo.po.EoCateringSchedulePO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EoCateringDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 14:43
 * @Version 1.0
 */
@Data
public class EoCateringDTO extends EoCateringPO {
    private List<EoCateringSchedulePO> eoCateringSchedulePOS;
}
