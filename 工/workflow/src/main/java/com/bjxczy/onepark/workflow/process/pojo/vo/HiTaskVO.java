package com.bjxczy.onepark.workflow.process.pojo.vo;

import java.util.Date;

import lombok.Data;

@Data
public class HiTaskVO {
	
	private String taskId;
	
	private Integer staffId;
	
	private String employeeNo;
	
	private String staffName;
	
	private String mobile;
	
	private Integer deptId;
	
	private String deptName;
	
	private String approvalName;
	
	private String approvalStatus;
	
	private String approvalText;
	
	private Date taskTime;
	
	private Date submitTime;
	
	private String procdefName;

}
