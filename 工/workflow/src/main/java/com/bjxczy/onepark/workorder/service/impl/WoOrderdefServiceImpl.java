package com.bjxczy.onepark.workorder.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.workflow.form.mapper.FormdefMapper;
import com.bjxczy.onepark.workorder.mapper.WoOrderdefMapper;
import com.bjxczy.onepark.workorder.pojo.dto.WoOrderdefDto;
import com.bjxczy.onepark.workorder.pojo.po.WoOrderdefExcelPO;
import com.bjxczy.onepark.workorder.pojo.po.WoOrderdefPO;
import com.bjxczy.onepark.workorder.pojo.vo.WoOrderdefVO;
import com.bjxczy.onepark.workorder.service.WoOrderdefService;
import com.bjxczy.onepark.workorder.utils.ChineseCharacterUtils;
import com.bjxczy.onepark.workorder.utils.DateTools;
import com.bjxczy.onepark.workorder.utils.ExcelUtils;
import com.bjxczy.onepark.workorder.utils.ListConverterUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @ClassName WoOrderdefServiceImpl
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/30 11:47
 * @Version 1.0
 */
@Service
public class WoOrderdefServiceImpl extends ServiceImpl<WoOrderdefMapper, WoOrderdefPO> implements WoOrderdefService {

    @Autowired
    private WoOrderdefMapper woOrderdefMapper;

    @Autowired
    private FormdefMapper formdefMapper;

    @Autowired
    private ExcelUtils excelUtils;

    @Autowired
    private DictService dictService;

    @Override
    public void addWoOrder(WoOrderdefDto woOrderdefDto) {
        WoOrderdefPO woOrderdefPO = new WoOrderdefPO();
        BeanUtils.copyProperties(woOrderdefDto, woOrderdefPO);
        Optional.ofNullable(this.baseMapper.selectList(Wrappers.<WoOrderdefPO>lambdaQuery()
                .eq(WoOrderdefPO::getName, woOrderdefDto.getName()))).ifPresent(woOrderdefPOS -> {
            if (woOrderdefPOS.size() > 0) {
                throw new ResultException("工单名称已存在！");
            }
        });
        //工单配置编号
        Optional.ofNullable(woOrderdefDto.getName()).ifPresent(name -> {
            //如果名称全为汉字那就取大写首字母+时间戳 如果有数字或者英文 直接用原名+时间戳
            if (ChineseCharacterUtils.isChineseStr(name)) {
                woOrderdefPO.setSequenceNo(ChineseCharacterUtils.getUpperCase(name, false) + DateTools.format(new Date(), DateTools.FORMAT_yyyyMMddHHmmss));
            } else {
                woOrderdefPO.setSequenceNo(name + DateTools.format(new Date(), DateTools.FORMAT_yyyyMMddHHmmss));
            }
        });
        this.baseMapper.insert(woOrderdefPO);
    }

    @Override
    public void editWoOrder(WoOrderdefDto woOrderdefDto) {
        WoOrderdefPO woOrderdefPO = new WoOrderdefPO();
        BeanUtils.copyProperties(woOrderdefDto, woOrderdefPO);
        Optional.ofNullable(this.baseMapper.selectOne(Wrappers.<WoOrderdefPO>lambdaQuery()
                .eq(WoOrderdefPO::getName, woOrderdefDto.getName()))).ifPresent(woOrderdefInfo -> {
            if (!woOrderdefInfo.getId().equals(woOrderdefDto.getId())) {
                throw new ResultException("工单名称已存在！");
            }
        });
        //联动编号的修改
        Optional.ofNullable(woOrderdefDto.getName()).ifPresent(name -> {
            //如果名称全为汉字那就取大写首字母+时间戳 如果有数字或者英文 直接用原名+时间戳
            if (ChineseCharacterUtils.isChineseStr(name)) {
                woOrderdefPO.setSequenceNo(ChineseCharacterUtils.getUpperCase(name, false) + DateTools.format(new Date(), DateTools.FORMAT_yyyyMMddHHmmss));
            } else {
                woOrderdefPO.setSequenceNo(name + DateTools.format(new Date(), DateTools.FORMAT_yyyyMMddHHmmss));
            }
        });
        this.baseMapper.updateById(woOrderdefPO);
    /*    //如果联动策略为用户自选
        String linkStrategy = woOrderdefDto.getLinkStrategy();
        if ("1".equals(linkStrategy)){
            //删除其关联工单
            baseMapper.update(woOrderdefPO,Wrappers.<WoOrderdefPO>lambdaUpdate()
                    .set(WoOrderdefPO::getTriggerId,null)
                    .eq(WoOrderdefPO::getId,woOrderdefDto.getId()));
        }*/
    }

/*    //判断是否循环关联  拿掉 不用判断 客户自己就知道了
    public void judgeCycle(String triggerId,String id) {
        if (triggerId.equals(id)){
            throw new ResultException("存在循环关联，请重新选择关联工单！");
        }
        WoOrderdefPO woOrderdefPO = woOrderdefMapper.selectById(triggerId);
        Set<String> set = new HashSet<>();
        while (woOrderdefPO!=null){
            if (null==woOrderdefPO.getTriggerIds()){
                woOrderdefPO=null;
                continue;
            }
            set.addAll(woOrderdefPO.getTriggerIds());
            woOrderdefPO=woOrderdefMapper.selectById(woOrderdefPO.getTriggerId());
        }
        if (set.contains(id)){
            throw new ResultException("存在循环关联，请重新选择关联工单！");
        }
    }*/

    @Override
    public void deleteWoOrder(List<String> ids) {
        ids.forEach(id->{
            Optional.ofNullable(this.baseMapper.selectById(id)).ifPresent(woOrderdefPO -> {
                if (1==woOrderdefPO.getType()){
                    throw new ResultException("内置工单配置不可删除！");
                }
            });
        });
        this.baseMapper.deleteBatchIds(ids);
    }

    @Override
    public void exportWoOrder(HttpServletResponse response, WoOrderdefDto woOrderdefDto) {
        List<WoOrderdefExcelPO> exportList = this.baseMapper.getExportList(woOrderdefDto);
        for (WoOrderdefExcelPO woOrderdefExcelPO : exportList) {
            woOrderdefExcelPO.setOrderTypeName(dictService.getLabel("workOrderType", woOrderdefExcelPO.getOrderType()));
            woOrderdefExcelPO.setBusinessTypeName(dictService.getLabel("woBusinessName", woOrderdefExcelPO.getBusinessType()));
            woOrderdefExcelPO.setOrderLevelName(dictService.getLabel("workOrderLevel", woOrderdefExcelPO.getOrderLevel()));
            woOrderdefExcelPO.setTaskTypeName(dictService.getLabel("woTaskType", woOrderdefExcelPO.getTaskType()));
            woOrderdefExcelPO.setFormName(formdefMapper.selectById(woOrderdefExcelPO.getFormId()).getName());
            Optional.ofNullable(woOrderdefExcelPO.getTriggerId()).ifPresent(triggerId -> {
                Optional.ofNullable(this.baseMapper.selectById(triggerId)).ifPresent(woOrderdefPO -> {
                    woOrderdefExcelPO.setTriggerName(woOrderdefPO.getName());
                });
            });
            // 状态：禁用：0，启用：1
            Integer status = woOrderdefExcelPO.getEnable();
            if (status == 1) {
                woOrderdefExcelPO.setStrEnable("启用");
            } else {
                woOrderdefExcelPO.setStrEnable("禁用");
            }
        }
        excelUtils.export(response, "工单配置列表数据导出", exportList, WoOrderdefExcelPO.class);
    }

    @Override
    public Page<WoOrderdefVO> list(WoOrderdefDto woOrderdefDto) {
        Page<WoOrderdefVO> woOrderdefVOPage = this.baseMapper.listPage(woOrderdefDto.toPage(), woOrderdefDto);
        for (WoOrderdefVO record : woOrderdefVOPage.getRecords()) {
            record.setOrderTypeName(dictService.getLabel("workOrderType", record.getOrderType()));
            record.setBusinessTypeName(dictService.getLabel("woBusinessName", record.getBusinessType()));
            record.setOrderLevelName(dictService.getLabel("workOrderLevel", record.getOrderLevel()));
            record.setTaskTypeName(dictService.getLabel("woTaskType", record.getTaskType()));
            Optional.ofNullable(formdefMapper.selectById(record.getFormId())).ifPresent(formdefPO -> {
                record.setFormName(formdefPO.getName());
            });
            Optional.ofNullable(record.getLinkStrategy()).ifPresent(linkStrategy -> {
                record.setLinkStrategyName(dictService.getLabel("strategyType", linkStrategy));
            });
            Optional.ofNullable(record.getTriggerIds()).ifPresent(triggerIds -> {
                LinkedHashSet<String> set = new LinkedHashSet<>();
                for (String triggerId : triggerIds) {
                    Optional.ofNullable(this.baseMapper.selectById(triggerId)).ifPresent(trigger -> {
                        set.add(trigger.getName());
                    });
                }
                record.setTriggerNames(set);
            });
        }
        return woOrderdefVOPage;
    }

    @Override
    public List<WoOrderdefVO> pullList() {
        List<WoOrderdefPO> woOrderdefPOS = this.baseMapper.selectList(Wrappers.<WoOrderdefPO>lambdaQuery()
                .eq(WoOrderdefPO::getEnable,1));
        ListConverterUtils util = new ListConverterUtils<>(WoOrderdefVO.class);
        List<WoOrderdefVO> list = util.converterList(woOrderdefPOS);
        for (WoOrderdefVO woOrderdefVO : list) {
            woOrderdefVO.setOrderTypeName(dictService.getLabel("workOrderType", woOrderdefVO.getOrderType()));
            woOrderdefVO.setBusinessTypeName(dictService.getLabel("woBusinessName", woOrderdefVO.getBusinessType()));
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> mobPullList(String id) {
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        Optional.ofNullable(woOrderdefMapper.selectById(id)).ifPresent(woOrderdefPO -> {
            Optional.ofNullable(woOrderdefPO.getTriggerIds()).ifPresent(triggerIds -> {
                for (String triggerId : triggerIds) {
                    HashMap<String, Object> map = new HashMap<>();
                    Optional.ofNullable(woOrderdefMapper.selectById(triggerId)).ifPresent(woOrderdefInfo -> {
                        map.put("triggerId", triggerId);
                        map.put("name", woOrderdefInfo.getName());
                    });
                    list.add(map);
                }
            });
        });
        return list;
    }
}
