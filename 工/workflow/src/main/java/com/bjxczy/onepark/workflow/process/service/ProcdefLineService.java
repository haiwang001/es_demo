package com.bjxczy.onepark.workflow.process.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;

public interface ProcdefLineService extends IService<ProcdefLinePO> {

	void saveByVersion(ProcdefVersionPO po, List<ProcdefLinePO> lines);

	ProcdefLinePO getByConditionType(String versionId, String conditionType);

	List<ProcdefLinePO> listByVersionId(String versionId);

}
