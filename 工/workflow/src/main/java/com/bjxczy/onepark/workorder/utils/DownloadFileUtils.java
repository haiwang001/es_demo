package com.bjxczy.onepark.workorder.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

/**
 * @ClassName DownloadFileToResponse
 * @Description 以流的方式下载文件
 * @Author zhanghongguo
 * @Date 2022/5/13 15:41
 * @Version 1.0
 */
@Slf4j
public class DownloadFileUtils {
    public static void downloadByStream(String path,HttpServletResponse response){
        InputStream in =new DownloadFileUtils().getClass().getResourceAsStream(path);
        try(
                BufferedInputStream fis =new BufferedInputStream(in);
                BufferedOutputStream toClient =new BufferedOutputStream(response.getOutputStream())){
            response.reset();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition","attachment;filename="+ new String(URLEncoder.encode("夜间盘点导入模板", "UTF-8").getBytes("utf-8"), "ISO8859-1"));
            int len =0;
            while((len = fis.read())!=-1){
                toClient.write(len);
                toClient.flush();
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    public static String getFilePath(String resource){
        return new DownloadFileUtils().getClass().getResource(resource).getPath();
    }

    public static void main(String[] args) {
        System.out.println(getFilePath("/templates/visitTemplate.xlsx"));
    }
    public static void downloadFile(String filePath, HttpServletResponse response) {
        try {
            // path是指想要下载的文件的路径
            File file = new File(filePath);
            log.warn(file.getPath());
            // 获取文件名
            String filename = file.getName();
            // 获取文件后缀名
            String ext = filename.substring(filename.lastIndexOf(".") + 1).toLowerCase();

            // 将文件写入输入流
            FileInputStream fileInputStream = new FileInputStream(file);
            InputStream fis = new BufferedInputStream(fileInputStream);
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();

            // 清空response
            response.reset();
            // 设置response的Header
            response.setCharacterEncoding("UTF-8");
            //Content-Disposition的作用：告知浏览器以何种方式显示响应返回的文件，用浏览器打开还是以附件的形式下载到本地保存
            //attachment表示以附件方式下载   inline表示在线打开   "Content-Disposition: inline; filename=文件名.mp3"
            // filename表示文件的默认名称，因为网络传输只支持URL编码的相关支付，因此需要将文件名URL编码后进行传输,前端收到后需要反编码才能获取到真正的名称
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
            // 告知浏览器文件的大小
            response.addHeader("Content-Length", "" + file.length());
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            outputStream.write(buffer);
            outputStream.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public static void downloadPdf(String filePath, HttpServletResponse response) {
        try {
            // path是指想要下载的文件的路径
            File file = new File(filePath);
            log.warn(file.getPath());
            // 获取文件名
            String filename = file.getName();
            // 获取文件后缀名
            String ext = filename.substring(filename.lastIndexOf(".") + 1).toLowerCase();

            // 将文件写入输入流
            FileInputStream fileInputStream = new FileInputStream(file);
            InputStream fis = new BufferedInputStream(fileInputStream);
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();

            // 清空response
            response.reset();
            // 设置response的Header
            response.setCharacterEncoding("UTF-8");
            //Content-Disposition的作用：告知浏览器以何种方式显示响应返回的文件，用浏览器打开还是以附件的形式下载到本地保存
            //attachment表示以附件方式下载   inline表示在线打开   "Content-Disposition: inline; filename=文件名.mp3"
            // filename表示文件的默认名称，因为网络传输只支持URL编码的相关支付，因此需要将文件名URL编码后进行传输,前端收到后需要反编码才能获取到真正的名称
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
            // 告知浏览器文件的大小
            response.addHeader("Content-Length", "" + file.length());
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/pdf");
            outputStream.write(buffer);
            outputStream.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
