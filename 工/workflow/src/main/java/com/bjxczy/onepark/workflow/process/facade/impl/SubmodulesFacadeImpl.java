package com.bjxczy.onepark.workflow.process.facade.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bjxczy.core.web.workflow.ProcessPayload;
import com.bjxczy.core.web.workflow.WorkflowController;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.workflow.process.facade.ISubmodulesFacade;

@Service
public class SubmodulesFacadeImpl implements ISubmodulesFacade {

	@Autowired
	@Qualifier("rpcRestTemplate")
	private RestTemplate restTemplate;

	private String url(String serviceName, String path, String key) {
		return "http://" + serviceName + path + "/" + key;
	}

	@Override
	public void validated(String serviceName, String key, Map<String, Object> variables, Integer staffId) {
		R<?> resp = restTemplate.postForObject(url(serviceName, WorkflowController.VAILDATE_PATH, key),
				new ProcessPayload(staffId, variables), R.class);
		if (!resp.isSuccess()) {
			throw new ResultException(resp.getMessage());
		}
	}

	@Override
	public void approved(String serviceName, String key, Map<String, Object> variables, Integer staffId) {
		R<?> resp = restTemplate.postForObject(url(serviceName, WorkflowController.APPROVED_PATH, key),
				new ProcessPayload(staffId, variables), R.class);
		if (!resp.isSuccess()) {
			throw new ResultException(resp.getMessage());
		}
	}

}
