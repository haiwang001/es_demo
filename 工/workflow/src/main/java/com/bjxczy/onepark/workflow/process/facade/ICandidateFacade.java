package com.bjxczy.onepark.workflow.process.facade;

import java.util.List;

import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workflow.process.pojo.dto.CandidateStaff;

public interface ICandidateFacade {
	
	List<CandidateStaff> invoke(String key, UserInformation loginUser, String condidateValue); 

}
