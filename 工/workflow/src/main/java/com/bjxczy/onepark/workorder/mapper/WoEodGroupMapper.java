package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.dto.WoEodDTO;
import com.bjxczy.onepark.workorder.pojo.po.WoEodGroupPO;
import com.bjxczy.onepark.workorder.pojo.vo.EodVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WoEodGroupMapper extends BaseMapper<WoEodGroupPO> {
    Page<EodVO> getEodList(@Param("toPage") Page<Object> toPage, @Param("woEodDTO") WoEodDTO woEodDTO);
}
