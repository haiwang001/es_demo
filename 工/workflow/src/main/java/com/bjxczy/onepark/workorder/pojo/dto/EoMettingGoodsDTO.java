package com.bjxczy.onepark.workorder.pojo.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.po.EoMettingGoodsPO;
import lombok.Data;

/**
 * @ClassName EoMettingGoodsDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/24 16:50
 * @Version 1.0
 */
@Data
public class EoMettingGoodsDTO extends EoMettingGoodsPO {
    @ExcelIgnore
    private Integer pageNum = 1;

    @ExcelIgnore
    private Integer pageSize = 10;

    public <T> Page<T> toPage() {
        return new Page<>(pageNum, pageSize);
    }
}
