package com.bjxczy.onepark.workorder.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateTools {
    public static final String FORMAT_01 = "yyyy-MM-dd HH:mm:ss";

    public static final String FORMAT_05 = "yyyy-MM-dd HH:mm";

    public static final String FORMAT_02 = "yyyy-MM-dd";

    public static final String FORMAT_03 = "yyyyMMdd";

    public static final String FORMAT_04 = "yyyy-MM-dd'T'HH:mm:ss";

    public static final String FORMAT_yyyyMMddHHmmss = "yyyyMMddHHmmss";
    public static final String FORMAT_HHmmss = "HH:mm:ss";


    public static String getDateTOString(String format,Date date){
        SimpleDateFormat form = new SimpleDateFormat(format);
        return form.format(date);
    }

    public static String getYear() {
        Date d=new Date();
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        String format = df.format(new Date(d.getTime() - 2 * 24 * 60 * 60 * 1000));
        return format.substring(0, 4);
    }
    public static String getMonth() {
        Date d=new Date();
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        String format = df.format(new Date(d.getTime() - 2 * 24 * 60 * 60 * 1000));
        return format.substring(5, 7);
    }
    public static String getDay() {
        Date d=new Date();
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        String format = df.format(new Date(d.getTime() - 2 * 24 * 60 * 60 * 1000));
        return format.substring(8, 10);
    }


    /**
     * 获取两个时间相隔的分钟数
     *
     * @param now
     * @param inTime
     * @return
     */
    public static Long getMin(Date now, Date inTime) {
        long nowtime = now.getTime();
        long intime = inTime.getTime();
        long diff = (nowtime - intime) / 1000 / 60;
        return diff;
    }

    /**
     * 日期格式化
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String format(Date date, String pattern) {
        String returnValue = "";
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            returnValue = df.format(date);
        }
        return (returnValue);
    }

    /**
     * 获取当前时间前diff
     *
     * @return
     */
    public static String getBeforDiffTimeStr(Integer diff) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_01);
        Calendar beforeTime = Calendar.getInstance();
        beforeTime.add(Calendar.MINUTE, -diff);
        Date beforeD = beforeTime.getTime();
        String time = sdf.format(beforeD);
        return time;
    }

    /**
     * 字符串时间格式化
     *
     * @param strDate
     * @param patternOld
     * @param patternNew
     * @return
     */
    public static String format(String strDate, String patternOld, String patternNew) {
        String returnValue = "";
        SimpleDateFormat dfOld = new SimpleDateFormat(patternOld);
        Date date = null;
        try {
            date = dfOld.parse(strDate);
            SimpleDateFormat dfNew = new SimpleDateFormat(patternNew);
            returnValue = dfNew.format(date);
        } catch (ParseException e) {
            log.error(e.getMessage());
            return null;
        }
        return (returnValue);
    }

    /**
     * 将字符串类型的日期转化成UTC时间格式 精确到grade等级，如grade=1精确到毫秒，grade=1000精确到秒
     *
     * @param strDate
     * @param pattern
     * @param grade
     */
    public static Integer parseTime(String strDate, String pattern, long grade) {
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = df.parse(strDate);
            Long time = (date.getTime() + 28800 * 1000) / grade;
            return time.intValue();
        } catch (ParseException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    /**
     * 日期格式化
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String format(String date, String pattern) {
        String returnValue = "";
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            returnValue = df.format(Long.parseLong(date));
        }
        return (returnValue);
    }

    /**
     * 将字符串类型的日期转化成Date类型
     *
     * @param strDate
     * @param pattern
     * @return
     */
    public static Date parse(String strDate, String pattern) {
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = df.parse(strDate);
            return date;
        } catch (ParseException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    /**
     * 获取时间戳
     */
    public static String getTimeString() {
        SimpleDateFormat df = new SimpleDateFormat(FORMAT_01);
        Calendar calendar = Calendar.getInstance();
        return df.format(calendar.getTime());
    }

    /**
     * 获取日期年份
     *
     * @param date 日期
     * @return
     */
    public static String getYear(Date date) {
        return format(date, "yyyy-MM-dd HH:mm:ss").substring(0, 4);
    }

    /**
     * 按默认格式的字符串距离今天的天数
     *
     * @param date 日期字符串
     * @return
     */
    public static int countDays(String date) {
        long t = Calendar.getInstance().getTime().getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(parse(date, "yyyy-MM-dd HH:mm:ss"));
        long t1 = c.getTime().getTime();
        return (int) (t / 1000 - t1 / 1000) / 3600 / 24;
    }

    /**
     * 按用户格式字符串距离今天的天数
     *
     * @param date   日期字符串
     * @param format 日期格式
     * @return
     */
    public static int countDays(String date, String format) {
        long t = Calendar.getInstance().getTime().getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(parse(date, format));
        long t1 = c.getTime().getTime();
        return (int) (t / 1000 - t1 / 1000) / 3600 / 24;
    }

    /**
     * 获取某天凌晨时间 <一句话功能简述> <功能详细描述>
     *
     * @param date
     * @return [参数说明]
     *
     * @return Date [返回类型说明]
     * @exception throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static Date getMorning(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }

    /**
     * 获取当前时间的时间戳，精确到毫秒 <功能详细描述>
     *
     * @return [参数说明]
     *
     * @return String [返回类型说明]
     * @exception throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static String getCurrentTimeMillis() {
        return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
    }

    /**
     * 获取多少天之后的日期 <功能详细描述>
     *
     * @param startDate
     * @param days
     * @return [参数说明]
     *
     * @return String [返回类型说明]
     * @exception throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static String getAfterDate(String startDate, int days) {
        Date date;
        Calendar cal = Calendar.getInstance();
        try {
            date = (new SimpleDateFormat("yyyy-MM-dd")).parse(startDate);

            cal.setTime(date);
            cal.add(Calendar.DATE, days);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }

        return (new SimpleDateFormat("yyyy-MM-dd")).format(cal.getTime());
    }

    public static String getAfterDateByFormat(SimpleDateFormat format,String startDate, int days) {
        Date date;
        Calendar cal = Calendar.getInstance();
        try {
            date = (format.parse(startDate));

            cal.setTime(date);
            cal.add(Calendar.DATE, days);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }

        return format.format(cal.getTime());
    }

    /**
     * 日期比较 <功能详细描述>
     *
     * @param startDate
     * @param endDate
     * @return [参数说明]
     *
     * @return String [返回类型说明]
     * @exception throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static boolean compareDate(String startDate, String endDate, String format) {
        Date newDate = strDateToDate(startDate, format);
        Date newDate2 = strDateToDate(endDate, format);
        return compareDate(newDate, newDate2);
    }

    /**
     * 将字符转换为日期类型 <功能详细描述>
     *
     * @param strDate
     * @param sourceFormat
     * @return [参数说明]
     *
     * @return Date [返回类型说明]
     * @exception throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static Date strDateToDate(String strDate, String sourceFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(sourceFormat);
        try {
            return dateFormat.parse(strDate);
        } catch (ParseException e) {
            log.error(e.getMessage());
            throw new RuntimeException("日期转化异常");
        }

    }

    /**
     * 通过时间秒毫秒数判断两个时间的间隔
     *
     * @return
     * @throws ParseException
     */
    public static int differentDaysByMillisecond(Long a, Long b) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String d1 = format.format(a);

        String d2 = format.format(b);
        Date date1 = format.parse(d1);

        Date date2 = format.parse(d2);
        int days = (int) ((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
        return days;
    }

    /**
     * 判断时间大小 <功能详细描述>
     *
     * @param date1
     * @param date2
     * @return [参数说明]
     *
     * @return boolean [返回类型说明]
     * @exception throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static boolean compareDate(Date date1, Date date2) {
        if (date1.compareTo(date2) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取当前时间 <功能详细描述>
     *
     * @return [参数说明]
     *
     * @return String [返回类型说明]
     * @exception throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static String now() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date());
    }

    /**
     * 获取x个半小时后 <功能详细描述>
     *
     * @param date
     * @param halfHour
     * @return [halfHour]
     *
     * @return String [返回类型说明]
     * @exception throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static String afterHalfHour(String date, int halfHour) {
        String reslut = "";
        try {
            Calendar cale = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat(FORMAT_01);
            Date parse = format.parse(date);
            cale.setTime(parse);
            cale.add(Calendar.MINUTE, halfHour * 30);
            Date tasktime = cale.getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            reslut = dateFormat.format(tasktime);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return reslut;
    }

    /**
     * 获取几小时后的时间
     *
     * @param date
     * @param hour
     * @return
     */
    public static String afterHour(String date, int hour) {
        String reslut = "";
        try {
            Calendar cale = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat(FORMAT_01);
            Date parse = format.parse(date);
            cale.setTime(parse);
            cale.add(Calendar.HOUR_OF_DAY, hour);
            Date tasktime = cale.getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            reslut = dateFormat.format(tasktime);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return reslut;
    }

    /**
     * 根据秒获取当前时间
     */
    public static String getTime(Integer s) {

        String hour = Integer.toString(s / 3600);
        String second = Integer.toString((s % 3600) / 60);
        if (second.length() < 2) {
            second = "0" + second;
        }
        String date = hour + ":" + second;
        return date;
    }

    /**
     * lzx 获取当前日期是星期几<br>
     *
     * @return 当前日期是星期几
     */
    public static String getWeekOfDate() {
        Date date = new Date();
        String[] weekDays = { "0", "1", "2", "3", "4", "5", "6" };
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0) {
            w = 0;
        }
        return weekDays[w];
    }

    /**
     * lzx
     *
     * @param we 必须是 二进制的 字符串 0100001
     * @return
     */
    public static Boolean alaerAy(String we) {
        Boolean bool = false;
        for (int i = 0; i < we.length(); i++) {
            if (Integer.parseInt(DateTools.getWeekOfDate()) == i) {
                if (we.charAt(i) == '1') {
                    bool = true;
                }
                break;
            }
        }
        return bool;
    }

    /**
     * lzx 根据时间戳 判断当前是否产生告警信息
     *
     * @param atartTime 开始时间
     * @param andTime   结束时间
     * @param eTime     nat推送的时间戳
     * @return
     */
    public static Boolean teimAlar(Long atartTime, Long andTime, Long eTime) {
        Boolean bool = false;
        if (atartTime < andTime) {
            if (atartTime < eTime && eTime < andTime) {
                bool = true;
            } else if (atartTime.equals(eTime) || andTime.equals(eTime)) {
                bool = true;
            }

        } else if (atartTime > andTime) {
            if (andTime < eTime || eTime < atartTime) {
                bool = false;
            } else {
                bool = true;
            }
        } else if (atartTime.equals(andTime)) {
            if (atartTime.equals(eTime)) {
                bool = true;
            } else {
                bool = false;
            }
        }
        return bool;
    }

    /**
     * 查询最近一年的数据
     * @return
     */
    public static List<String> getLast12Months(){
        ArrayList<String> latest12Months = new ArrayList<>(12);
        LocalDate today = LocalDate.now();
        for(long i = 11L;i >=0L; i--){
            LocalDate localDate = today.minusMonths(i);
            String ss = localDate.toString().substring(0,7);
            latest12Months.add(ss);
        }
        return latest12Months;
    }

    /**
     * 传入两个时间范围，返回这两个时间范围内的所有日期，并保存在一个集合中
     *
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public static List<String> getDaysBetweenDate(String beginTime, String endTime){
        //创建一个放所有日期的集合
        List<String> dates = new ArrayList();

        //创建时间解析对象规定解析格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        //将传入的时间解析成Date类型,相当于格式化
        Date dBegin = null;
        try {
            dBegin = sdf.parse(beginTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date dEnd = null;
        try {
            dEnd = sdf.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //将格式化后的第一天添加进集合
        dates.add(sdf.format(dBegin));

        //使用本地的时区和区域获取日历
        Calendar calBegin = Calendar.getInstance();

        //传入起始时间将此日历设置为起始日历
        calBegin.setTime(dBegin);

        //判断结束日期前一天是否在起始日历的日期之后
        while (dEnd.after(calBegin.getTime())) {

            //根据日历的规则:月份中的每一天，为起始日历加一天
            calBegin.add(Calendar.DAY_OF_MONTH, 1);

            //得到的每一天就添加进集合
            dates.add(sdf.format(calBegin.getTime()));
            //如果当前的起始日历超过结束日期后,就结束循环
        }
        return dates;
    }

    /**
     * 获取两个日期之间的所有月份 (年月)
     *
     * @param startTime
     * @param endTime
     * @return：list
     */
    public static List<String> getMonthBetweenDate(String startTime, String endTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        // 声明保存日期集合
        List<String> list = new ArrayList<>();
        try {
            // 转化成日期类型
            Date startDate = sdf.parse(startTime);
            Date endDate = sdf.parse(endTime);

            //用Calendar 进行日期比较判断
            Calendar calendar = Calendar.getInstance();
            while (startDate.getTime() <= endDate.getTime()) {

                // 把日期添加到集合
                list.add(sdf.format(startDate));

                // 设置日期
                calendar.setTime(startDate);

                //把月数增加 1
                calendar.add(Calendar.MONTH, 1);

                // 获取增加后的日期
                startDate = calendar.getTime();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 获取两个日期之间的所有年
     *
     * @param startTime
     * @param endTime
     * @return：list
     */
    public static List<String> getYearBetweenDate(String startTime, String endTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        // 声明保存日期集合
        List<String> list = new ArrayList<>();
        try {
            // 转化成日期类型
            Date startDate = sdf.parse(startTime);
            Date endDate = sdf.parse(endTime);

            //用Calendar 进行日期比较判断
            Calendar calendar = Calendar.getInstance();
            while (startDate.getTime() <= endDate.getTime()) {

                // 把日期添加到集合
                list.add(sdf.format(startDate));

                // 设置日期
                calendar.setTime(startDate);

                //把年数增加 1
                calendar.add(Calendar.YEAR, 1);

                // 获取增加后的日期
                startDate = calendar.getTime();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 获取当前日期
     *
     * @param
     * @return
     */
    public static String getCurrentDate() {
        Date date = new Date();
        return format(date, FORMAT_03);
    }

    public static String getNextNDaysDateTime(int N) {
        Date date = getNdaysAfter(new Date(), N);
        return format(date, FORMAT_04);
    }

    public static String getNextNDays(int N, String format) {
        Date date = getNdaysAfter(new Date(), N);
        return format(date, format);
    }

    public static Date getNdaysAfter(Date date, int N) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, +N);// +1今天的时间加一天
        date = calendar.getTime();
        return date;
    }

    public static String getNmonthAfter(String date, String dateType, int month) {
        String nowDate = null;
        SimpleDateFormat format = new SimpleDateFormat(dateType);
        try {
            Date parse = format.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(parse);
            calendar.add(Calendar.MONTH, month);
            nowDate = format.format(calendar.getTime());
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return nowDate;
    }

    /**
     * 时间间隔转换 —转换为*天*时*分*秒
     *
     * @param startDate
     * @param endDate
     * @param format
     * @return
     */
    public static String getDateDiff(String startDate, String endDate, String format) {
        String result = " ";
        Date oldDate = strDateToDate(startDate, format);
        Date newDate = strDateToDate(endDate, format);
        long l = newDate.getTime() - oldDate.getTime();
        long day = l / (24 * 60 * 60 * 1000);
        if (0 != day) {
            result += day + "天";
        }
        long hour = (l / (60 * 60 * 1000) - day * 24);
        if (0 != hour) {
            result += hour + "小时";
        }
        long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
        if (0 != min) {
            result += min + "分";
        }
        long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        if (0 != s) {
            result += s + "秒";
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.between(DateUtil.parse("14:00:00"),new Date(), DateUnit.MINUTE));
    }
    /**
     * 时间间隔转换 —转换为*天*时*分*秒
     *
     * @param format
     * @return
     */
    public static String getDateDiffDate(Date oldDate, Date newDate, String format) {
        String result = " ";
        long l = newDate.getTime() - oldDate.getTime();
        long day = l / (24 * 60 * 60 * 1000);
        if (0 != day) {
            result += day + "天";
        }
        long hour = (l / (60 * 60 * 1000) - day * 24);
        if (0 != hour) {
            result += hour + "小时";
        }
        long min = ((l / (60 * 1000)) - day * 24 * 60 - hour * 60);
        if (0 != min) {
            result += min + "分";
        }
        long s = (l / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        if (0 != s) {
            result += s + "秒";
        }
        return result;
    }

    /**
     * 时间间隔转换 转化为x个半小时小时，不满半小时以半小时计算
     *
     * @param startDate
     * @param endDate
     * @param format
     * @return
     */
    public static Integer getDiffHalfHour(String startDate, String endDate, String format) {
        Date oldDate = strDateToDate(startDate, format);
        Date newDate = strDateToDate(endDate, format);
        long l = newDate.getTime() - oldDate.getTime();
        long Halfhour = l / (30 * 60 * 1000);

        Long diff = l % (30 * 60 * 1000) == 0 ? Halfhour : Halfhour + 1;
        return diff.intValue();
    }

    /**
     * 时长转换 秒—>*天*时*分*秒
     *
     * @param second
     * @return
     */
    public static String transformTime(String second) {
        String result = " ";
        Integer param = Integer.valueOf(second);
        Integer day = param / (24 * 60 * 60);
        if (0 != day) {
            result += day + "天";
        }
        Integer hour = (param / (60 * 60) - day * 24);
        if (0 != hour) {
            result += hour + "小时";
        }
        Integer min = ((param / (60)) - day * 24 * 60 - hour * 60);
        if (0 != min) {
            result += min + "分";
        }
        Integer s = (param - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        if (0 != s) {
            result += s + "秒";
        }
        return result;
    }

    /**
     * 根据年和月份获取对应月份的所有日期
     */
    public static List<String> getMonthFullDay(int year, int month) {
        SimpleDateFormat dateFormatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
        List<String> fullDayList = new ArrayList<>(32);
        // 获得当前日期对象
        Calendar cal = Calendar.getInstance();
        cal.clear();// 清除信息
        cal.set(Calendar.YEAR, year);
        // 1月从0开始
        cal.set(Calendar.MONTH, month - 1);
        // 当月1号
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int count = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int j = 1; j <= count; j++) {
            fullDayList.add(dateFormatYYYYMMDD.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return fullDayList;
    }


    /**
     * 根据年和月份获取对应月份的所有日期
     */
    public static List<String> getMonthFullDayyyyMMdd(int year, int month) {
        SimpleDateFormat dateFormatYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
        List<String> fullDayList = new ArrayList<>(32);
        // 获得当前日期对象
        Calendar cal = Calendar.getInstance();
        cal.clear();// 清除信息
        cal.set(Calendar.YEAR, year);
        // 1月从0开始
        cal.set(Calendar.MONTH, month - 1);
        // 当月1号
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int count = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int j = 1; j <= count; j++) {
            fullDayList.add(dateFormatYYYYMMDD.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return fullDayList;
    }

    /**
     * 根据年/月/日份获取对应月份到当天为止的所有日期
     */
    public static List<String> getMonthFullDay(int year, int month, int day) {
        SimpleDateFormat dateFormatYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
        List<String> fullDayList = new ArrayList<>(32);
        // 获得当前日期对象
        Calendar cal = Calendar.getInstance();
        cal.clear();// 清除信息
        cal.set(Calendar.YEAR, year);
        // 1月从0开始
        cal.set(Calendar.MONTH, month - 1);
        // 当月1号
        cal.set(Calendar.DAY_OF_MONTH, 1);
        for (int j = 1; j <= day; j++) {
            fullDayList.add(dateFormatYYYYMMDD.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return fullDayList;
    }

    /**
     * 获取传入时间的24小时的yyyy-MM-dd HH:mm:ss
     *
     * @param date
     * @return
     */
    public static ArrayList<String> getTwentyFourHourByDay(String date) {
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                list.add(date + " 0" + i + ":00:00");
            } else {
                list.add(date + " " + i + ":00:00");
            }
        }
        return list;
    }

    /**
     * 根据分钟数返回对应的天/小时/分钟
     * @param minutes
     * @return
     */
    public static String convertToDaysHoursMinutes(long minutes) {
        int day = (int) TimeUnit.MINUTES.toDays(minutes);
        long hours = TimeUnit.MINUTES.toHours(minutes) - (day * 24);
        long minute = TimeUnit.MINUTES.toMinutes(minutes) - (TimeUnit.MINUTES.toHours(minutes) * 60);

        String result = "";

        if (day != 0) {
            result += day +"天";
        }
        if (hours != 0) {
            result += hours+"小时";
        }
        if (minute != 0) {
            result += minute+"分钟";
        }
        return result;
    }
}
