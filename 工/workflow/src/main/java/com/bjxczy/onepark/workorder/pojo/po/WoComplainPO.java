package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName WoComplain
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/27 14:52
 * @Version 1.0
 */
@Data
@TableName("wo_complain")
public class WoComplainPO extends UserOperator {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "投诉编号")
    private String sequenceNo;

    @ApiModelProperty(value = "投诉部门Id")
    private Integer deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "发起员工ID")
    private Integer initiatorId;

    @ApiModelProperty(value = "发起员工姓名")
    private String initiatorName;

    @ApiModelProperty(value = "操作人Id")
    private Integer operatorId;

    @ApiModelProperty(value = "投诉标题")
    private String title;

    @ApiModelProperty(value = "投诉内容")
    private String content;

    @ApiModelProperty(value = "反馈内容")
    private String feedback;

    @ApiModelProperty(value = "投诉状态")
    private String status;



}
