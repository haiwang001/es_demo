package com.bjxczy.onepark.workflow.process.pojo.vo;

import java.util.List;

import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefNodePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProcdefVersionVO extends ProcdefVersionPO {
	
	private static final long serialVersionUID = -3130837657554928533L;

	private List<ProcdefNodePO> nodes;
	
	private List<ProcdefLinePO> lines;

}
