package com.bjxczy.onepark.workorder.feign;

import com.bjxczy.core.feign.client.authority.BaseSpaceFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("bmpspace")
public interface SpaceFeignClient extends BaseSpaceFeignClient {

}
