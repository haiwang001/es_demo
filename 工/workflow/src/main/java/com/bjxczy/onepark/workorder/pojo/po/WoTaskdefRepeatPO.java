package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 任务定义-重复执行关联表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("wo_taskdef_repeat")
@ApiModel(value="WoTaskdefRepeatPO", description="任务定义-重复执行关联表")
public class WoTaskdefRepeatPO {

    @ApiModelProperty(value = "自增Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "任务定义ID")
    private Integer taskDefId;

    @ApiModelProperty(value = "执行时间：HH:mm:ss")
    private String execTime;


}
