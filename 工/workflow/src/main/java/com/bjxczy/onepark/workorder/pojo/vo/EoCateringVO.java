package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoCateringPO;
import com.bjxczy.onepark.workorder.pojo.po.EoCateringSchedulePO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EoCateringVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 16:00
 * @Version 1.0
 */
@Data
public class EoCateringVO extends EoCateringPO {

    private String diningFormName;
    private List<EoCateringSchedulePO> eoCateringSchedulePOS;
}
