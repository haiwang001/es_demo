package com.bjxczy.onepark.workflow.process.service.impl;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;
import com.bjxczy.onepark.workflow.process.entity.RuProcessPO;
import com.bjxczy.onepark.workflow.process.mapper.RuProcessMapper;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMonitorDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMyTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryRuProcessDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.HiTaskVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.TaskVO;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;
import com.bjxczy.onepark.workflow.process.service.ProcessVersionService;
import com.bjxczy.onepark.workflow.process.service.RuProcessService;

@Service
public class RuProcessServiceImpl extends ServiceImpl<RuProcessMapper, RuProcessPO> implements RuProcessService {

	@Autowired
	private StaffFeignClient staffFeignClient;
	@Autowired
	private ProcessDefinitionService procdefService;
	@Autowired
	private ProcessVersionService procdefVersionSerivce;

	@Override
	public RuProcessPO saveByInst(String key, UserInformation user) {
		RuProcessPO entity = new RuProcessPO();
		StaffInformation staff = staffFeignClient.findById(user.getStaffId());
		ProcdefPO def = procdefService.getByDeployId(key);
		ProcdefVersionPO version = procdefVersionSerivce.getById(def.getActVersion());
		entity.setStaffId(staff.getId());
		entity.setEmployeeNo(staff.getEmployeeNo());
		entity.setStaffName(staff.getFldName());
		entity.setMobile(staff.getMobile());
		entity.setDeptId(staff.getOrganizationId());
		entity.setDeptName(staff.getDeptNames().stream().map(e -> e.getDeptName()).collect(Collectors.joining("/")));
		entity.setRankId(staff.getRankId());
		entity.setRankName(staff.getRankName());
		entity.setProcDefKey(def.getDeployId());
		entity.setProcDefName(def.getName());
		entity.setProcVersion(def.getActVersion());
		entity.setFormId(version.getFormId());
		entity.setApplyStatus(0);
		this.save(entity);
		return entity;
	}

	@Override
	public IPage<TaskVO> listMyTask(QueryMyTaskDTO dto, UserInformation user) {
		return baseMapper.selectTask(dto.toPage(), dto, user.getStaffId());
	}

	@Override
	public RuProcessPO getByInstId(String processInstanceId) {
		return lambdaQuery().eq(RuProcessPO::getProcInstId, processInstanceId).one();
	}

	@Override
	public IPage<RuProcessPO> listPage(QueryRuProcessDTO dto, UserInformation user) {
		return baseMapper.selectProcess(dto.toPage(), dto, user.getStaffId());
	}

	@Override
	public IPage<HiTaskVO> listMyHiTask(QueryMyTaskDTO dto, UserInformation user) {
		return baseMapper.selectHiTask(dto.toPage(), dto, user.getStaffId());
	}

	@Override
	public IPage<RuProcessPO> listMonitorPage(QueryMonitorDTO dto) {
		return baseMapper.selectMonitorPage(dto.toPage(), dto);
	}

}
