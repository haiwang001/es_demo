package com.bjxczy.onepark.workflow.process.core.listener;

import java.util.Map;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.excel.util.StringUtils;
import com.bjxczy.core.web.message.MessageService;
import com.bjxczy.onepark.common.model.workbench.TemplateMessagePayload;
import com.bjxczy.onepark.workflow.process.core.constant.ProcessConstant;
import com.bjxczy.onepark.workflow.process.core.constant.TaskListenerType;
import com.bjxczy.onepark.workflow.process.core.constant.TaskVariables;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;
import com.bjxczy.onepark.workflow.process.util.ProcessUtils;

@Component(TaskListenerType.SUBMIT_TASK_LISTENER)
public class SubmitTaskListener implements TaskListener {

	@Autowired
	private TaskService taskService;
	@Autowired
	private MessageService messageService;
	@Autowired
	private ProcessDefinitionService procdefService;
	@Autowired
	private RepositoryService repositoryService;

	@Override
	public void notify(DelegateTask task) {
		Map<String, Object> variables = task.getVariables();
		Integer retryCnt = (Integer) variables.get(TaskVariables.VAR_RETRY_CNT);
		String assignee = (String) variables.get(TaskVariables.VAR_INITIATOR_KEY);
		if (retryCnt > 0 && StringUtils.isNotBlank(assignee)) {
			// 发送调整消息
			ProcessDefinition def = repositoryService.createProcessDefinitionQuery()
					.processDefinitionId(task.getProcessDefinitionId()).singleResult();
			ProcdefPO po = procdefService.getByDeployId(def.getKey());
			variables.put("procdefName", def.getName());
			String eventCode = ProcessUtils.getEventCode(po, ProcessConstant.EVENT_RETRY_SUFFIX);
			TemplateMessagePayload payload = new TemplateMessagePayload(Integer.parseInt(assignee), task.getId(),
					eventCode, variables);
			messageService.pushTemplateMessage(payload);
		} else {
			// 首次提交，自动完成提交任务
			taskService.complete(task.getId(), variables);
		}
	}

	private static final long serialVersionUID = -165053839292106626L;

}
