package com.bjxczy.onepark.workflow.process.pojo.dto;

import java.util.List;

import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefNodePO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = false)
@EqualsAndHashCode(callSuper = false)
public class UpdateVersionDTO extends CreateVersionDTO {
	
	private String formId;
	
	private List<ProcdefNodePO> nodes;
	
	private List<ProcdefLinePO> lines;
	
	private String sourceCode;

}
