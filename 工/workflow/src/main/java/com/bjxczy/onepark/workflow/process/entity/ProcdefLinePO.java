package com.bjxczy.onepark.workflow.process.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName("wf_re_procdef_line")
public class ProcdefLinePO extends BaseBpmnElement {
	
	private String sourceRef;
	
	private String targetRef;
	
	private String conditionType;
	
	private String expression;
	
	@TableField(exist = false)
	private String extra;
	
}
