package com.bjxczy.onepark.workflow.form.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;

public interface FormItemService extends IService<FormItemPO> {

	void saveByForm(FormdefPO entity, List<FormItemPO> formItem);

	void removeByForm(FormdefPO po);

	List<FormItemPO> listByFormId(String id);

	List<FormItemPO> igoreDelListByFormId(String formId);

}
