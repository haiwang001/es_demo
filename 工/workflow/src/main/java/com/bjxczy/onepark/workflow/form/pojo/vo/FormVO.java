package com.bjxczy.onepark.workflow.form.pojo.vo;

import java.util.List;

import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class FormVO extends FormdefPO {
	
	private static final long serialVersionUID = 9065153395584846962L;
	
	private List<FormItemPO> formItem;

}
