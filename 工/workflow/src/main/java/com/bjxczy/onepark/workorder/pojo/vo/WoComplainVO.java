package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.WoComplainPO;
import lombok.Data;

/**
 * @ClassName WoComplainVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/27 16:24
 * @Version 1.0
 */
@Data
public class WoComplainVO extends WoComplainPO {
}
