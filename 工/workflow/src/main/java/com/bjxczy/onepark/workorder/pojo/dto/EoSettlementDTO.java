package com.bjxczy.onepark.workorder.pojo.dto;

import com.bjxczy.onepark.workorder.pojo.po.EoSettlementPO;
import lombok.Data;

/**
 * @ClassName EoSettlementDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/26 15:27
 * @Version 1.0
 */
@Data
public class EoSettlementDTO extends EoSettlementPO {
}
