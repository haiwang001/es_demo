package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 餐饮Eo单表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="EoCatering对象", description="餐饮Eo单表")
@TableName("eo_catering")
public class EoCateringPO {

    @ApiModelProperty(value = "主键Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "Eo单Id")
    private String eoId;

    @ApiModelProperty(value = "早餐时间")
    private String breakfastTime;

    @ApiModelProperty(value = "午餐时间")
    private String lunchTime;

    @ApiModelProperty(value = "晚餐时间")
    private String dinnerTime;

    @ApiModelProperty(value = "用餐地点")
    private String diningPlace;

    @ApiModelProperty(value = "用餐形式（字典表：diningForm）")
    private String diningForm;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "用餐类型（字典表：mealType）")
    private String mealType;


}
