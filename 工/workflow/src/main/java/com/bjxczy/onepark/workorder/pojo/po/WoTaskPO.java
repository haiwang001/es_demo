package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Set;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 任务执行表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-31
 */
@Data
@TableName("wo_task")
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="WoTaskPO", description="任务执行表")
public class WoTaskPO extends UserOperator {

    @ApiModelProperty(value = "自增ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "任务定义ID")
    private Integer taskDefId;

    @ApiModelProperty(value = "工单配置ID")
    private String woId;

    @ApiModelProperty(value = "工单编号")
    private String sequenceNo;

    @ApiModelProperty(value = "工单类型")
    private String orderType;

    @ApiModelProperty(value = "业务模块")
    private String businessType;

    @ApiModelProperty(value = "紧急程度")
    private String orderLevel;

    @ApiModelProperty(value = "接单类型")
    private String taskType;

    @ApiModelProperty(value = "部门")
    private Integer assignDeptId;

    @ApiModelProperty(value = "部门名称")
    private String assignDeptName;

    @ApiModelProperty(value = "过期时间（分钟）")
    private Integer expireTime;

    @ApiModelProperty(value = "发起员工ID")
    private Integer initiatorId;

    @ApiModelProperty(value = "发起员工姓名")
    private String initiatorName;

    @ApiModelProperty(value = "部门主管员工ID")
    private Integer ownerId;

    @ApiModelProperty(value = "部门主管姓名")
    private String ownerName;

    @ApiModelProperty(value = "任务状态")
    private String taskStatus;

    @ApiModelProperty(value = "驳回意见")
    private String rejectReason;

    @ApiModelProperty(value = "手机号")
    private String mobile;

}
