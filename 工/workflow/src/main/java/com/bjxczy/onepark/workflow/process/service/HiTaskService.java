package com.bjxczy.onepark.workflow.process.service;

import java.util.List;

import org.activiti.engine.task.Task;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workflow.process.entity.HiTaskPO;
import com.bjxczy.onepark.workflow.process.entity.RuProcessPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.RejectTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.TaskCompleteDTO;

public interface HiTaskService extends IService<HiTaskPO> {

	void saveOnStart(RuProcessPO process);

	void saveOnComplete(RuProcessPO process, TaskCompleteDTO dto, Task task, UserInformation user);

	void saveOnEnd(RuProcessPO process);

	List<HiTaskPO> listByProcessId(Integer id);

	HiTaskPO getByTask(Task task);

	void saveOnReject(RuProcessPO ruProcess, RejectTaskDTO dto, Task task, UserInformation user);

}
