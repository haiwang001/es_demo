package com.bjxczy.onepark.workflow.process.facade;

import java.util.Map;

public interface ISubmodulesFacade {

	void validated(String serviceName, String key, Map<String, Object> variables, Integer staffId);

	void approved(String serviceName, String key, Map<String, Object> variables, Integer staffId);

}
