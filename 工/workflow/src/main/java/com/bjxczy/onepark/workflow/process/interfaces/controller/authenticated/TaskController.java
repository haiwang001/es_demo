package com.bjxczy.onepark.workflow.process.interfaces.controller.authenticated;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.Payload;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMyTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.RejectTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.TaskCompleteDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.CandidateVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.HiTaskVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.TaskVO;
import com.bjxczy.onepark.workflow.process.service.TaskExecService;

@ApiResourceController
@RequestMapping("/task")
public class TaskController {

	@Autowired
	private TaskExecService taskExecService;

	@PostMapping(value = "/submit/{key}", name = "提交申请")
	public void submit(@PathVariable("key") String key, @RequestBody Map<String, Object> variables,
			@LoginUser(isFull = true) UserInformation user) {
		taskExecService.runProcess(key, variables, user);
	}

	@PostMapping(value = "/candidate/{key}", name = "查询候选人")
	public List<CandidateVO> getCandidate(@PathVariable("key") String key, 
			@RequestParam(value = "processId", required = false) Integer processId,
			@RequestBody Map<String, Object> variables,
			@LoginUser(isFull = true) UserInformation user) {
		return taskExecService.getCandidate(key, processId, variables, user);
	}

	@PostMapping(value = "/complete/{taskId}", name = "提交审批")
	public void complete(@PathVariable("taskId") String taskId, @RequestBody TaskCompleteDTO dto,
			@LoginUser(isFull = true) UserInformation user) {
		taskExecService.complete(taskId, dto, user);
	}

	@PostMapping(value = "/reject/{taskId}", name = "驳回审批")
	public void reject(@PathVariable("taskId") String taskId, @RequestBody RejectTaskDTO dto,
			@LoginUser(isFull = true) UserInformation user) {
		taskExecService.reject(taskId, dto, user);
	}

	@GetMapping(value = "/my", name = "我的审批")
	public IPage<TaskVO> listMyTask(QueryMyTaskDTO dto, @LoginUser(isFull = true) UserInformation user) {
		return taskExecService.listMyTask(dto, user);
	}

	@GetMapping(value = "/history", name = "历史审批")
	public IPage<HiTaskVO> listMyHiTask(QueryMyTaskDTO dto, @LoginUser(isFull = true) UserInformation user) {
		return taskExecService.listMyHiTask(dto, user);
	}

	@GetMapping(value = "/variables/{taskId}", name = "任务详情")
	public Payload variables(@PathVariable("taskId") String taskId) {
		return taskExecService.getVariables(taskId);
	}

}
