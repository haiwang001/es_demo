package com.bjxczy.onepark.workflow.form.pojo.dto;

import lombok.Data;

@Data
public class CreateFormDTO {
	
	private String name;
	
	private String remark;
	
}
