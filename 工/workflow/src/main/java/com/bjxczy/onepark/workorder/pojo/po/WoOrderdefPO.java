package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 工单配置 表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value="wo_orderdef",autoResultMap = true)
@ApiModel(value="WoOrderdefPO", description="工单配置表")
public class WoOrderdefPO extends UserOperator {


    @TableId(type = IdType.ASSIGN_UUID)
    @ApiModelProperty(value = "uuid")
    private String id;

    @ApiModelProperty(value = "工单编号")
    private String sequenceNo;

    @ApiModelProperty(value = "部门Id")
    private Integer deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "部门主管Id")
    private Integer ownerId;

    @ApiModelProperty(value = "部门主管姓名")
    private String ownerName;

    @ApiModelProperty(value = "工单名称")
    private String name;

    @ApiModelProperty(value = "工单描述")
    private String description;

    @ApiModelProperty(value = "工单类型")
    private String orderType;

    @ApiModelProperty(value = "业务模块")
    private String businessType;

    @ApiModelProperty(value = "紧急程度")
    private String orderLevel;

    @ApiModelProperty(value = "接单类型")
    private String taskType;

    @ApiModelProperty(value = "过期时间（分钟）")
    private Integer expireTime;

    @ApiModelProperty(value = "表单")
    private String formId;

    @TableField(typeHandler = FastjsonTypeHandler.class)
    @ApiModelProperty(value = "完成后触发工单ID")
    private LinkedHashSet<String> triggerIds;

    @ApiModelProperty(value = "是否启用：0-禁用；1-启用")
    private Integer enable;

    @ApiModelProperty(value = "联动策略")
    private String linkStrategy;

    @ApiModelProperty(value = "开关（开：1 关：0）")
    private Integer deviceSwitch;

    @ApiModelProperty(value = "类型 1：内置工单 0：非内置工单")
    private Integer type;

}
