package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 车辆EO单表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("eo_vehicle")
@ApiModel(value="EoVehicle对象", description="车辆EO单表")
public class EoVehiclePO {

    @ApiModelProperty(value = "主键Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "EO单Id")
    private String eoId;

    @ApiModelProperty(value = "车辆数量")
    private Integer carCount;

    @ApiModelProperty(value = "备注")
    private String remark;


}
