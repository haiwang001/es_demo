package com.bjxczy.onepark.workflow.process.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;

@Mapper
public interface ProcdefLineMapper extends BaseMapper<ProcdefLinePO> {

}
