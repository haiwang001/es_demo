package com.bjxczy.onepark.workorder.pojo.po;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 工单配置 表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="WoOrderdefExcelPO", description="工单配置导出表")
public class WoOrderdefExcelPO {

    @ExcelProperty("工单配置编号")
    @ApiModelProperty(value = "工单编号")
    private String sequenceNo;

    @ExcelProperty("工单名称")
    @ApiModelProperty(value = "工单名称")
    private String name;

    @ExcelIgnore
    private String orderType;

    @ExcelProperty("工单类型")
    @ApiModelProperty(value = "工单类型")
    private String orderTypeName;

    @ExcelIgnore
    private String taskType;

    @ExcelProperty("接单类型")
    @ApiModelProperty(value = "接单类型")
    private String taskTypeName;

    @ExcelIgnore
    private String businessType;

    @ExcelProperty("业务模块")
    @ApiModelProperty(value = "业务模块")
    private String businessTypeName;

    @ExcelProperty("部门名称")
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ExcelIgnore
    @ApiModelProperty(value = "紧急程度")
    private String orderLevel;

    @ExcelProperty("紧急程度")
    private String orderLevelName;

    @ExcelIgnore
    @ApiModelProperty(value = "表单")
    private String formId;

    @ExcelProperty("任务填报表")
    private String formName;

    @ExcelProperty("关联工单")
    //关联表单
    private String triggerName;

    @ExcelIgnore
    @ApiModelProperty(value = "完成后触发工单ID")
    private String triggerId;

    @ExcelIgnore
    @ApiModelProperty(value = "状态：禁用：0，启用：1")
    private Integer enable;

    @ExcelProperty("状态")
    private String strEnable;
}
