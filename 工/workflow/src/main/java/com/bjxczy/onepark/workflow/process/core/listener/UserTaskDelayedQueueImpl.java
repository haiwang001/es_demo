package com.bjxczy.onepark.workflow.process.core.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjxczy.core.redisson.constant.DelayedQueueNames;
import com.bjxczy.core.redisson.delayed.DelayedQueueImpl;
import com.bjxczy.core.redisson.delayed.IDelayedQueue;
import com.bjxczy.core.redisson.delayed.RDelayedPayload;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workflow.process.pojo.dto.RejectTaskDTO;
import com.bjxczy.onepark.workflow.process.service.TaskExecService;

import lombok.extern.slf4j.Slf4j;

@Component(DelayedQueueNames.WORKFLOW_USER_TASK_QUEUE)
@Slf4j
public class UserTaskDelayedQueueImpl extends DelayedQueueImpl implements IDelayedQueue {
	
	@Autowired
	private TaskExecService taskExecService;
	
	@Override
	public void execute(RDelayedPayload payload) {
		String taskId = payload.getId();
		log.info("[DelayedQueue] 审批任务超时，taskId={}", taskId);
		// TODO 超时自动驳回，后续分类型处理：无操作、自动驳回、自动通过
		UserInformation user = new UserInformation();
		user.setStaffName("系统操作");
		taskExecService.reject(taskId, new RejectTaskDTO(null, "审批超时自动驳回"), user);
	}

}
