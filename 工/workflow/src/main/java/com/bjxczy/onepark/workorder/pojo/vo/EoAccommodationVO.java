package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoAccommodationPO;
import lombok.Data;

/**
 * @ClassName EoAccommodationVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 15:41
 * @Version 1.0
 */
@Data
public class EoAccommodationVO extends EoAccommodationPO {

    private String roomTypeName;
}
