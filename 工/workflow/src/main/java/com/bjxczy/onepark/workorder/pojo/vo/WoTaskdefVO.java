package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.WoTaskDescribePO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskdefPO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskdefRepeatPO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName WoTaskdefVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/31 10:55
 * @Version 1.0
 */
@Data
public class WoTaskdefVO extends WoTaskdefPO {
    private String orderTypeName;

    private String businessTypeName;

    private String orderLevelName;

    private String taskTypeName;

    private String repeatTypeName;

    private String deviceName;

    private String categoryName;

    private List<WoTaskdefRepeatPO> woTaskdefRepeats;

    private String initiatorMobile;

    private List<WoTaskDescribePO> woTaskDescribePOS;

    private Integer deviceSwitch;

}
