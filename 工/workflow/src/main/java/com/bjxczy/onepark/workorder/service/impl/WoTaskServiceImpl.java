package com.bjxczy.onepark.workorder.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.core.redisson.constant.DelayedQueueNames;
import com.bjxczy.core.redisson.delayed.IDelayedQueue;
import com.bjxczy.core.redisson.delayed.RDelayedPayload;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.message.MessageService;
import com.bjxczy.onepark.common.model.device.GetDeviceInfoVo;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.workbench.DealMessageInformation;
import com.bjxczy.onepark.common.model.workbench.TemplateMessagePayload;
import com.bjxczy.onepark.common.model.workflow.DescribeItem;
import com.bjxczy.onepark.common.model.workflow.PushOrderPayload;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.form.mapper.FormItemMapper;
import com.bjxczy.onepark.workflow.form.mapper.FormdefMapper;
import com.bjxczy.onepark.workorder.feign.BmpDeviceFeignClient;
import com.bjxczy.onepark.workorder.feign.SpaceFeignClient;
import com.bjxczy.onepark.workorder.mapper.*;
import com.bjxczy.onepark.workorder.pojo.dto.*;
import com.bjxczy.onepark.workorder.pojo.excel.TaskFormExport;
import com.bjxczy.onepark.workorder.pojo.po.*;
import com.bjxczy.onepark.workorder.pojo.vo.*;
import com.bjxczy.onepark.workorder.service.WoTaskService;
import com.bjxczy.onepark.workorder.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @ClassName WoTaskServiceImpl
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/31 10:46
 * @Version 1.0
 */
@Service
@Slf4j
public class WoTaskServiceImpl implements WoTaskService {

    @Autowired
    private BmpDeviceFeignClient bmpDeviceFeignClient;

    @Autowired
    private FormdefMapper formdefMapper;

    @Autowired
    private MessageService messageService;

    @Autowired
    private FormItemMapper formItemMapper;

    @Autowired
    private StaffFeignClient staffFeignClient;

    @Autowired
    private WoTaskDescribeMapper woTaskDescribeMapper;

    @Autowired
    @Qualifier(DelayedQueueNames.WORKFLOW_ORDER_CREATE_TASK_QUEUE)
    private IDelayedQueue orderCreateTaskQueue;

    @Autowired
    @Qualifier(DelayedQueueNames.WORKFLOW_ORDER_TASK_QUEUE)
    private IDelayedQueue orderTaskQueue;

    @Autowired
    @Qualifier(DelayedQueueNames.WORKFLOW_ORDER_WARN_TASK_QUEUE)
    private IDelayedQueue orderWarnTaskQueue;

    @Autowired
    private DictService dictService;

    @Autowired
    private ExcelUtils excelUtils;

    @Autowired
    private WoOrderdefMapper woOrderdefMapper;

    @Autowired
    private WoTaskdefMapper woTaskdefMapper;

    @Autowired
    private WoTaskdefRepeatMapper woTaskdefRepeatMapper;

    @Autowired
    private WoTaskMapper woTaskMapper;

    @Autowired
    private SpaceFeignClient spaceFeignClient;

    @Autowired
    private WoTaskInstanceMapper woTaskInstanceMapper;

    @Autowired
    private WoTaskAssigneeMapper woTaskAssigneeMapper;

    @Autowired
    private WoTaskVariablesMapper woTaskVariablesMapper;

    @Autowired
    private WoOrderdefServiceImpl woOrderdefService;

    @Override
    public void pcDefCreate(WoTaskDTO woTaskDTO, UserInformation user) {
        WoTaskdefPO woTaskdefPO = new WoTaskdefPO();
        BeanUtils.copyProperties(woTaskDTO, woTaskdefPO);
        //工单任务编号 工单名首字母+时间
        Optional.ofNullable(woTaskDTO.getName()).ifPresent(name -> {
            //如果名称全为汉字那就取大写首字母+时间戳 如果有数字或者英文 直接用原名+时间戳
            if (ChineseCharacterUtils.isChineseStr(name)) {
                woTaskdefPO.setSequenceNo(ChineseCharacterUtils.getUpperCase(name, false) + DateTools.format(new Date(), DateTools.FORMAT_yyyyMMddHHmmss));
            } else {
                woTaskdefPO.setSequenceNo(name + DateTools.format(new Date(), DateTools.FORMAT_yyyyMMddHHmmss));
            }
        });
        //工单位置
        String position;
        if (null!=woTaskDTO.getSpaceId()){
            position = spaceFeignClient.getPosition(woTaskDTO.getSpaceId());
        }else if (null!=woTaskDTO.getTenantId()){
            position=spaceFeignClient.getPositionByBuildingId(woTaskDTO.getTenantId());
        }else {
            position=spaceFeignClient.getPositionByAreaId(woTaskDTO.getAreaId());
        }
        woTaskdefPO.setPosition(position);
        //发起人
        woTaskdefPO.setInitiatorId(user.getStaffId());
        woTaskdefPO.setInitiatorName(user.getStaffName());
        /*//本地
        woTaskdefPO.setInitiatorId(13239);
        woTaskdefPO.setInitiatorName("张洪国");*/
        woTaskdefMapper.insert(woTaskdefPO);
        //任务重复类型：
        if ("1".equals(woTaskDTO.getRepeatType())) {//仅一次类型工单
            //生成执行工单
            WoTaskPO woTaskPO = new WoTaskPO();
            BeanUtils.copyProperties(woTaskdefPO, woTaskPO);
            woTaskPO.setTaskDefId(woTaskdefPO.getId());
            woTaskPO.setOwnerId(woTaskDTO.getOwnerId());
            woTaskPO.setOwnerName(woTaskDTO.getOwnerName());
            woTaskPO.setTaskStatus("0");//已创建
            woTaskPO.setMobile(user.getMobile());
            woTaskMapper.insert(woTaskPO);
            // 发待办
            HashMap<String, Object> params = new HashMap<>();
            params.put("woTaskName", woTaskdefPO.getName());
            String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_ASSIGN_SUFFIX;
            messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskPO.getOwnerId(), String.valueOf(woTaskPO.getId()), eventCode, params));
            //延时队列 跟踪状态
            RDelayedPayload rDelayedPayload = new RDelayedPayload(woTaskPO.getId());
            orderTaskQueue.offer(rDelayedPayload, woTaskdefPO.getExpireTime(), TimeUnit.MINUTES);
        } else {//定时类工单（每天定时、起止时间）只生成时间配置，执行工单通过延时队列生成
            woTaskDTO.getExecTimes().forEach(execTime -> {
                WoTaskdefRepeatPO woTaskdefRepeatPO = new WoTaskdefRepeatPO();
                woTaskdefRepeatPO.setTaskDefId(woTaskdefPO.getId());
                woTaskdefRepeatPO.setExecTime(execTime);
                woTaskdefRepeatMapper.insert(woTaskdefRepeatPO);
            });
        }
    }

    @Override
    public Page<WoTaskdefVO> defListPage(WoTaskDTO woTaskDTO) {
        Page<WoTaskdefVO> woTaskdefVOPage = woTaskdefMapper.defListPage(woTaskDTO.toPage(), woTaskDTO);
        for (WoTaskdefVO record : woTaskdefVOPage.getRecords()) {
            record.setOrderTypeName(dictService.getLabel("workOrderType", record.getOrderType()));
            record.setBusinessTypeName(dictService.getLabel("woBusinessName", record.getBusinessType()));
            record.setOrderLevelName(dictService.getLabel("workOrderLevel", record.getOrderLevel()));
            record.setTaskTypeName(dictService.getLabel("woTaskType", record.getTaskType()));
            record.setRepeatType(dictService.getLabel("woTaskRepeatType", record.getRepeatType()));
            List<WoTaskdefRepeatPO> woTaskdefRepeatPOS = woTaskdefRepeatMapper.selectList(Wrappers.<WoTaskdefRepeatPO>lambdaQuery()
                    .eq(WoTaskdefRepeatPO::getTaskDefId, record.getId()));
            record.setWoTaskdefRepeats(woTaskdefRepeatPOS);
            Optional.ofNullable(record.getWoId()).ifPresent(woId->{
                Optional.ofNullable(woOrderdefMapper.selectById(woId)).ifPresent(woOrderdefPO -> {
                    record.setDeviceSwitch(woOrderdefPO.getDeviceSwitch());
                });
            });
        }
        return woTaskdefVOPage;
    }

    @Override
    public void defEditTask(WoTaskDTO woTaskDTO) {
        WoTaskdefPO woTaskdefPO = new WoTaskdefPO();
        BeanUtils.copyProperties(woTaskDTO, woTaskdefPO);
        //更改position
        String position;
        if (null!=woTaskDTO.getSpaceId()){
            position = spaceFeignClient.getPosition(woTaskDTO.getSpaceId());
        }else if (null!=woTaskDTO.getTenantId()){
            woTaskdefMapper.update(new WoTaskdefPO(),Wrappers.<WoTaskdefPO>lambdaUpdate()
                    .eq(WoTaskdefPO::getId,woTaskDTO.getId())
                    .set(WoTaskdefPO::getSpaceId,null));
            position=spaceFeignClient.getPositionByBuildingId(woTaskDTO.getTenantId());
        }else {
            woTaskdefMapper.update(new WoTaskdefPO(),Wrappers.<WoTaskdefPO>lambdaUpdate()
                    .eq(WoTaskdefPO::getId,woTaskDTO.getId())
                    .set(WoTaskdefPO::getTenantId,null)
                    .set(WoTaskdefPO::getSpaceId,null));
            position=spaceFeignClient.getPositionByAreaId(woTaskDTO.getAreaId());
        }
        woTaskdefPO.setPosition(position);
        woTaskdefMapper.updateById(woTaskdefPO);
        // 任务重复时间调整（删除、插入新的时间）
        if (woTaskDTO.getRepeatType().equals("2") || woTaskDTO.getRepeatType().equals("3")) {
            //删除所有 重新添加
            woTaskdefRepeatMapper.delete(Wrappers.<WoTaskdefRepeatPO>lambdaQuery()
                    .eq(WoTaskdefRepeatPO::getTaskDefId, woTaskDTO.getId()));
            //插入新的
            woTaskDTO.getExecTimes().forEach(execTime -> {
                WoTaskdefRepeatPO woTaskdefRepeatPO = new WoTaskdefRepeatPO();
                woTaskdefRepeatPO.setTaskDefId(woTaskdefPO.getId());
                woTaskdefRepeatPO.setExecTime(execTime);
                woTaskdefRepeatMapper.insert(woTaskdefRepeatPO);
            });
        }
    }

    @Override
    public void defTaskExport(WoTaskDTO woTaskDTO, HttpServletResponse response) {
        List<WoTaskdefExcelPO> woTaskdefExcelPOS = woTaskdefMapper.exportList(woTaskDTO);
        for (WoTaskdefExcelPO woTaskdefExcelPO : woTaskdefExcelPOS) {
            woTaskdefExcelPO.setOrderTypeName(dictService.getLabel("workOrderType", woTaskdefExcelPO.getOrderType()));
            woTaskdefExcelPO.setOrderLevelName(dictService.getLabel("workOrderLevel", woTaskdefExcelPO.getOrderLevel()));
            woTaskdefExcelPO.setBusinessTypeName(dictService.getLabel("woBusinessName", woTaskdefExcelPO.getBusinessType()));
            woTaskdefExcelPO.setRepeatTypeName(dictService.getLabel("woTaskRepeatType", woTaskdefExcelPO.getRepeatType()));
        }
        excelUtils.export(response, "工单任务列表导出", woTaskdefExcelPOS, WoTaskdefExcelPO.class);
    }

    @Override
    public Page<WoTaskVO> executeListPage(WoTaskDTO woTaskDTO) {
        Page<WoTaskVO> woTaskVOPage = woTaskMapper.excuteListPage(woTaskDTO.toPage(), woTaskDTO);
        for (WoTaskVO record : woTaskVOPage.getRecords()) {
            WoOrderdefPO woOrderdefPO = woOrderdefMapper.selectById(record.getWoId());
            record.setOrderTypeName(dictService.getLabel("workOrderType", record.getOrderType()));
            record.setBusinessTypeName(dictService.getLabel("woBusinessName", record.getBusinessType()));
            record.setOrderLevelName(dictService.getLabel("workOrderLevel", record.getOrderLevel()));
            record.setTaskStatusName(dictService.getLabel("woTaskStatus", record.getTaskStatus()));
            List<WoTaskInstancePO> woTaskInstancePOS = woTaskInstanceMapper.selectList(Wrappers.<WoTaskInstancePO>lambdaQuery()
                    .eq(WoTaskInstancePO::getTaskExecId, record.getId()));
            StringBuilder sb = new StringBuilder();
            for (WoTaskInstancePO woTaskInstancePO : woTaskInstancePOS) {
                sb.append(woTaskInstancePO.getAssigneeName()).append(",");
            }
            if (StrUtil.isNotEmpty(sb)) {
                record.setPersons(sb.deleteCharAt(sb.lastIndexOf(",")).toString());
            }
            WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(record.getTaskDefId());
            Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
                WoTaskdefVO woTaskdefVO = new WoTaskdefVO();
                BeanUtils.copyProperties(woTaskdefPO, woTaskdefVO);
                record.setWoTaskdefVO(woTaskdefVO);
                ListConverterUtils listConverterUtils = new ListConverterUtils(WoTaskInstanceVO.class);
                List<WoTaskInstanceVO> list = listConverterUtils.converterList(woTaskInstancePOS);
                for (WoTaskInstanceVO woTaskInstanceVO : list) {
                    StaffInfoFine staff = staffFeignClient.getStaffById(woTaskInstanceVO.getAssigneeId());
                    Optional.ofNullable(staff).ifPresent(staffInfo -> {
                        woTaskInstanceVO.setMobile(staff.getMobile());
                        List<WoTaskVariablesPO> woTaskVariablesPOS = woTaskVariablesMapper.selectList(Wrappers.<WoTaskVariablesPO>lambdaQuery()
                                .eq(WoTaskVariablesPO::getTaskInstanceId, woTaskInstanceVO.getId()));
                        woTaskInstanceVO.setFormExtra(woTaskVariablesPOS);
                        Optional.ofNullable(woOrderdefPO).ifPresent(woOrderdefPO1 -> {
                            woTaskInstanceVO.setFormId(woOrderdefPO1.getFormId());
                        });
                    });
                }
                record.setWoTaskInstanceVOList(list);
            });
        }
        return woTaskVOPage;
    }

    @Override
    public void executeDelByIds(List<Integer> ids) {
        woTaskMapper.deleteBatchIds(ids);
    }

    @Override
    public void executeEdit(WoTaskDTO woTaskDTO) {
        WoTaskPO woTaskPO = new WoTaskPO();
        BeanUtils.copyProperties(woTaskDTO, woTaskPO);
        woTaskMapper.updateById(woTaskPO);
        //取消执行工单
        String taskStatus = woTaskDTO.getTaskStatus();
        if ("5".equals(taskStatus)) {
            WoTaskPO woTaskInfo = woTaskMapper.selectById(woTaskDTO.getId());
            //发送消息
            Map<String, Object> params = new HashMap<>();
            WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskInfo.getTaskDefId());
            Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
                params.put("woTaskName", woTaskdef.getName());
                params.put("dateTime", DateUtil.now());
                String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_CANCEL_SUFFIX;
                Optional.ofNullable(woTaskInfo.getInitiatorId()).ifPresent(initiatorId->{
                    messageService.pushTemplateMessage(new TemplateMessagePayload(initiatorId, String.valueOf(woTaskDTO.getId()), eventCode, params));
                });
            });
        }
    }

    @Override
    public List<WoTaskVO> mobAssignList(WoTaskDTO woTaskDTO, UserInformation user) {
        //派单列表
        List<WoTaskPO> woTaskPOS = woTaskMapper.selectList(Wrappers.<WoTaskPO>lambdaQuery()
                .eq(WoTaskPO::getOwnerId, user.getStaffId())//筛选自己工单
                .eq(null != woTaskDTO.getTaskStatus(), WoTaskPO::getTaskStatus, woTaskDTO.getTaskStatus())
                .orderByDesc(WoTaskPO::getUpdateTime));//待指派、已指派
        ListConverterUtils utils = new ListConverterUtils<>(WoTaskVO.class);
        List<WoTaskVO> list = utils.converterList(woTaskPOS);
        for (WoTaskVO woTaskVO : list) {
            woTaskVO.setTaskStatusName(dictService.getLabel("woTaskStatus", woTaskVO.getTaskStatus()));
            woTaskVO.setOrderLevelName(dictService.getLabel("workOrderLevel", woTaskVO.getOrderLevel()));
            woTaskVO.setBusinessTypeName(dictService.getLabel("woBusinessName", woTaskVO.getBusinessType()));
            woTaskVO.setOrderTypeName(dictService.getLabel("workOrderType", woTaskVO.getOrderType()));
            woTaskVO.setTaskTypeName(dictService.getLabel("woTaskType", woTaskVO.getTaskType()));
            //返回实例相关数据
            List<WoTaskInstancePO> woTaskInstancePOS = woTaskInstanceMapper.selectList(Wrappers.<WoTaskInstancePO>lambdaQuery()
                    .eq(WoTaskInstancePO::getTaskExecId, woTaskVO.getId()));
            ListConverterUtils util = new ListConverterUtils<>(WoTaskInstanceVO.class);
            List<WoTaskInstanceVO> woTaskInstances = util.converterList(woTaskInstancePOS);
            Optional.ofNullable(woTaskInstances).ifPresent(woTaskInstanceList -> {
                woTaskVO.setWoTaskInstanceVOList(woTaskInstanceList);
            });
            WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskVO.getTaskDefId());
            Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
                WoTaskdefVO woTaskdefVO = new WoTaskdefVO();
                BeanUtils.copyProperties(woTaskdefPO, woTaskdefVO);
                Optional.ofNullable(woTaskdefVO.getInitiatorId()).ifPresent(initiatorId->{
                    StaffInfoFine staffById = staffFeignClient.getStaffById(initiatorId);
                    Optional.ofNullable(staffById).ifPresent(staffInfo -> {
                        woTaskdefVO.setInitiatorMobile(staffInfo.getMobile());
                    });
                });
                Optional.ofNullable(woOrderdefMapper.selectById(woTaskdef.getWoId())).ifPresent(woOrderdefPO -> {
                    woTaskdefVO.setDeviceSwitch(woOrderdefPO.getDeviceSwitch());
                });
                Optional.ofNullable(woTaskdefVO.getDeviceCode()).ifPresent(deviceCode->{
                    Optional.ofNullable(bmpDeviceFeignClient.deviceInfoReportInfo(deviceCode)).ifPresent(deviceInfo->{
                        woTaskdefVO.setDeviceName(deviceInfo.getDeviceName());
                    });
                });
                woTaskdefVO.setRepeatTypeName(dictService.getLabel("woTaskRepeatType", woTaskdefVO.getRepeatType()));
                woTaskVO.setWoTaskdefVO(woTaskdefVO);
            });

        }
        return list;
    }


    @Override
    public List<WoTaskInstanceVO> mobInstanceList(WoTaskInstanceDTO woTaskInstanceDTO, UserInformation user) {
        //返回接口
        List<WoTaskInstanceVO> resList = new ArrayList<>();
        LambdaQueryWrapper<WoTaskInstancePO> wrapper = new LambdaQueryWrapper<>();
        //自己的工单
        wrapper.eq(WoTaskInstancePO::getAssigneeId, user.getStaffId());
        //工单状态筛选
        wrapper.eq(null != woTaskInstanceDTO.getInstStatus(), WoTaskInstancePO::getInstStatus, woTaskInstanceDTO.getInstStatus());
        wrapper.orderByDesc(WoTaskInstancePO::getUpdateTime);
        List<WoTaskInstancePO> woTaskInstancePOS = woTaskInstanceMapper.selectList(wrapper);
        for (WoTaskInstancePO woTaskInstancePO : woTaskInstancePOS) {
            Integer taskExecId = woTaskInstancePO.getTaskExecId();
            WoTaskPO woTaskPO = woTaskMapper.selectById(taskExecId);
            WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskPO.getTaskDefId());
            WoTaskInstanceVO woTaskInstanceVO = new WoTaskInstanceVO();
            BeanUtils.copyProperties(woTaskInstancePO, woTaskInstanceVO);
            WoTaskdefVO woTaskdefVO = new WoTaskdefVO();
            BeanUtils.copyProperties(woTaskdefPO, woTaskdefVO);
            woTaskdefVO.setOrderTypeName(dictService.getLabel("workOrderType", woTaskdefVO.getOrderType()));
            woTaskdefVO.setBusinessTypeName(dictService.getLabel("woBusinessName", woTaskdefVO.getBusinessType()));
            woTaskdefVO.setOrderLevelName(dictService.getLabel("workOrderLevel", woTaskdefVO.getOrderLevel()));
            woTaskdefVO.setTaskTypeName(dictService.getLabel("woTaskType", woTaskdefVO.getTaskType()));
            woTaskdefVO.setRepeatTypeName(dictService.getLabel("woTaskRepeatType", woTaskdefVO.getRepeatType()));
            //发起人手机号
            Optional.ofNullable(woTaskPO.getInitiatorId()).ifPresent(initiatorId->{
                StaffInfoFine staff = staffFeignClient.getStaffById(initiatorId);
                Optional.ofNullable(staff).ifPresent(staffInfo -> {
                    woTaskdefVO.setInitiatorMobile(staffInfo.getMobile());
                });
            });
            //额外信息
            Optional.ofNullable(woTaskdefPO.getId()).ifPresent(execId->{
                Optional.ofNullable(woTaskDescribeMapper.selectList(Wrappers.<WoTaskDescribePO>lambdaQuery()
                        .eq(WoTaskDescribePO::getTaskExecId,execId))).ifPresent(woTaskDescribes->{
                            woTaskdefVO.setWoTaskDescribePOS(woTaskDescribes);
                });
            });
            //设备名
            Optional.ofNullable(woTaskdefVO.getDeviceCode()).ifPresent(deviceCode->{
                Optional.ofNullable(bmpDeviceFeignClient.deviceInfoReportInfo(deviceCode)).ifPresent(deviceInfo->{
                    woTaskdefVO.setDeviceName(deviceInfo.getDeviceName());
                });

            });
            //状态名
            woTaskInstanceVO.setInstStatusName(dictService.getLabel("woTaskStatus", woTaskInstanceVO.getInstStatus()));
            woTaskInstanceVO.setWoTaskdefVO(woTaskdefVO);
            WoOrderdefPO woOrderdefPO = woOrderdefMapper.selectById(woTaskdefPO.getWoId());
            Optional.ofNullable(woOrderdefPO).ifPresent(woOrderdefPO1 -> {
                WoOrderdefVO woOrderdefVO = new WoOrderdefVO();
                BeanUtils.copyProperties(woOrderdefPO1, woOrderdefVO);
                Optional.ofNullable(woOrderdefVO.getLinkStrategy()).ifPresent(linkStrategy -> {
                    woOrderdefVO.setLinkStrategyName(dictService.getLabel("strategyType", linkStrategy));
                });
                woTaskInstanceVO.setWoOrderdefVO(woOrderdefVO);
                woTaskInstanceVO.setFormId(woOrderdefPO1.getFormId());
            });
            List<WoTaskVariablesPO> woTaskVariablesPOS = woTaskVariablesMapper.selectList(Wrappers.<WoTaskVariablesPO>lambdaQuery()
                    .eq(WoTaskVariablesPO::getTaskInstanceId, woTaskInstanceVO.getId()));
            woTaskInstanceVO.setFormExtra(woTaskVariablesPOS);
            Optional.ofNullable(woTaskInstanceVO.getTriggerId()).ifPresent(triggerId -> {
                woTaskInstanceVO.setTriggerName(woOrderdefMapper.selectById(triggerId).getName());
            });
            resList.add(woTaskInstanceVO);
        }
        return resList;
    }

    @Override
    public List<WoTaskVO> mobInitiateList(WoTaskDTO woTaskDTO, UserInformation user) {
        List<WoTaskPO> woTaskPOS = woTaskMapper.selectList(Wrappers.<WoTaskPO>lambdaQuery()
                .in(WoTaskPO::getInitiatorId, user.getStaffId())
                .eq(null != woTaskDTO.getTaskStatus(), WoTaskPO::getTaskStatus, woTaskDTO.getTaskStatus())
                .orderByDesc(WoTaskPO::getUpdateTime));
        ListConverterUtils utils = new ListConverterUtils<>(WoTaskVO.class);
        List<WoTaskVO> list = utils.converterList(woTaskPOS);
        for (WoTaskVO woTaskVO : list) {
            woTaskVO.setTaskStatusName(dictService.getLabel("woTaskStatus", woTaskVO.getTaskStatus()));
            woTaskVO.setOrderLevelName(dictService.getLabel("workOrderLevel", woTaskVO.getOrderLevel()));
            woTaskVO.setBusinessTypeName(dictService.getLabel("woBusinessName", woTaskVO.getBusinessType()));
            woTaskVO.setOrderTypeName(dictService.getLabel("workOrderType", woTaskVO.getOrderType()));
            woTaskVO.setTaskTypeName(dictService.getLabel("woTaskType", woTaskVO.getTaskType()));
            WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskVO.getTaskDefId());
            Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
                WoTaskdefVO woTaskdefVO = new WoTaskdefVO();
                BeanUtils.copyProperties(woTaskdefPO, woTaskdefVO);
                woTaskdefVO.setRepeatTypeName(dictService.getLabel("woTaskRepeatType", woTaskdefVO.getRepeatType()));
                Optional.ofNullable(woTaskdefVO.getDeviceCode()).ifPresent(deviceCode -> {
                    GetDeviceInfoVo deviceInfo = bmpDeviceFeignClient.deviceInfoReportInfo(deviceCode);
                    woTaskdefVO.setDeviceName(deviceInfo.getDeviceName());
                    woTaskdefVO.setCategoryName(deviceInfo.getDeviceTypeName());
                });
                //设备信息开关
                Optional.ofNullable(woOrderdefMapper.selectById(woTaskdef.getWoId())).ifPresent(woOrderdefPO -> {
                    woTaskdefVO.setDeviceSwitch(woOrderdefPO.getDeviceSwitch());
                });
                woTaskVO.setWoTaskdefVO(woTaskdefVO);
            });
        }
        return list;
    }

    @Override
    public void mobCreateTask(WoTaskDTO woTaskDTO, UserInformation user) {
        WoTaskdefPO woTaskdefPO = new WoTaskdefPO();
        BeanUtils.copyProperties(woTaskDTO, woTaskdefPO);
        //工单任务编号 工单名首字母+时间
        Optional.ofNullable(woTaskDTO.getName()).ifPresent(name -> {
            //如果名称全为汉字那就取大写首字母+时间戳 如果有数字或者英文 直接用原名+时间戳
            if (ChineseCharacterUtils.isChineseStr(name)) {
                woTaskdefPO.setSequenceNo(ChineseCharacterUtils.getUpperCase(name, false) + DateTools.format(new Date(), DateTools.FORMAT_yyyyMMddHHmmss));
            } else {
                woTaskdefPO.setSequenceNo(name + DateTools.format(new Date(), DateTools.FORMAT_yyyyMMddHHmmss));
            }
        });
        //工单位置
        String position;
        if (null!=woTaskDTO.getSpaceId()){
            position = spaceFeignClient.getPosition(woTaskDTO.getSpaceId());
        }else if (null!=woTaskDTO.getTenantId()){
            position=spaceFeignClient.getPositionByBuildingId(woTaskDTO.getTenantId());
        }else {
            position=spaceFeignClient.getPositionByAreaId(woTaskDTO.getAreaId());
        }
        woTaskdefPO.setPosition(position);
        //发起人
        woTaskdefPO.setInitiatorId(user.getStaffId());
        woTaskdefPO.setInitiatorName(user.getStaffName());
        woTaskdefMapper.insert(woTaskdefPO);
        //任务重复类型：
        if ("1".equals(woTaskDTO.getRepeatType())) {//仅一次类型工单
            //生成执行工单
            WoTaskPO woTaskPO = new WoTaskPO();
            BeanUtils.copyProperties(woTaskdefPO, woTaskPO);
            woTaskPO.setTaskDefId(woTaskdefPO.getId());
            woTaskPO.setOwnerId(woTaskDTO.getOwnerId());
            woTaskPO.setOwnerName(woTaskDTO.getOwnerName());
            woTaskPO.setTaskStatus("0");//已创建
            woTaskPO.setMobile(user.getMobile());
            woTaskMapper.insert(woTaskPO);
            // 发待办
            HashMap<String, Object> params = new HashMap<>();
            params.put("woTaskName", woTaskdefPO.getName());
            String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_ASSIGN_SUFFIX;
            messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskPO.getOwnerId(), String.valueOf(woTaskPO.getId()), eventCode, params));
            //延时队列 跟踪状态
            RDelayedPayload rDelayedPayload = new RDelayedPayload(woTaskPO.getId());
            orderTaskQueue.offer(rDelayedPayload, woTaskdefPO.getExpireTime(), TimeUnit.MINUTES);
        } else {//定时类工单（每天定时、起止时间）只生成时间配置，执行工单通过延时队列生成
            woTaskDTO.getExecTimes().forEach(execTime -> {
                WoTaskdefRepeatPO woTaskdefRepeatPO = new WoTaskdefRepeatPO();
                woTaskdefRepeatPO.setTaskDefId(woTaskdefPO.getId());
                woTaskdefRepeatPO.setExecTime(execTime);
                woTaskdefRepeatMapper.insert(woTaskdefRepeatPO);
            });
        }
    }

    @Override
    public void mobReject(WoTaskDTO woTaskDTO) {
        WoTaskPO woTaskPO = new WoTaskPO();
        woTaskPO.setId(woTaskDTO.getId());
        woTaskPO.setTaskStatus(woTaskDTO.getTaskStatus());
        woTaskPO.setRejectReason(woTaskDTO.getRejectReason());
        woTaskMapper.updateById(woTaskPO);
        //发起工单驳回
        WoTaskPO woTaskInfo = woTaskMapper.selectById(woTaskDTO.getId());
        Map<String, Object> params = new HashMap<>();
        WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskInfo.getTaskDefId());
        Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
            params.put("woTaskName", woTaskdef.getName());
            params.put("dateTime", DateUtil.now());
            String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_INITIATE_REJECT_SUFFIX;
            Optional.ofNullable(woTaskInfo.getInitiatorId()).ifPresent(initiatorId->{
                messageService.pushTemplateMessage(new TemplateMessagePayload(initiatorId, String.valueOf(woTaskDTO.getId()), eventCode, params));
            });
        });

    }

    @Override
    public void mobCancel(Integer id) {
        //将工单执行任务状态改为已取消 5
        WoTaskPO woTaskPO = new WoTaskPO();
        woTaskPO.setId(id);
        woTaskPO.setTaskStatus("5");
        woTaskMapper.updateById(woTaskPO);
        WoTaskPO woTaskInfo = woTaskMapper.selectById(id);
        //发送消息
        Map<String, Object> params = new HashMap<>();
        WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskInfo.getTaskDefId());
        Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
            params.put("woTaskName", woTaskdef.getName());
            params.put("dateTime", DateUtil.now());
            String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_CANCEL_SUFFIX;
            Optional.ofNullable(woTaskInfo.getInitiatorId()).ifPresent(initiatorId->{
                messageService.pushTemplateMessage(new TemplateMessagePayload(initiatorId, String.valueOf(id), eventCode, params));
            });
        });
    }

    /**
     * 抢单 查询执行工单任务为抢单类型的工单
     *
     * @return
     */
    @Override
    public List<WoTaskVO> mobGrabList() {
        List<WoTaskPO> woTaskPOS = woTaskMapper.selectList(Wrappers.<WoTaskPO>lambdaQuery()
                .eq(WoTaskPO::getTaskType, "1")
                .eq(WoTaskPO::getTaskStatus,"0")
                .orderByDesc(WoTaskPO::getUpdateTime));
        ListConverterUtils utils = new ListConverterUtils<>(WoTaskVO.class);
        List<WoTaskVO> list = utils.converterList(woTaskPOS);
        for (WoTaskVO woTaskVO : list) {
            woTaskVO.setOrderTypeName(dictService.getLabel("workOrderType", woTaskVO.getOrderType()));
            woTaskVO.setBusinessTypeName(dictService.getLabel("woBusinessName", woTaskVO.getBusinessType()));
            woTaskVO.setOrderLevelName(dictService.getLabel("workOrderLevel", woTaskVO.getOrderLevel()));
            woTaskVO.setTaskTypeName(dictService.getLabel("woTaskType", woTaskVO.getTaskType()));
            WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskVO.getTaskDefId());
            Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
                WoTaskdefVO woTaskdefVO = new WoTaskdefVO();
                BeanUtils.copyProperties(woTaskdefPO, woTaskdefVO);
                woTaskdefVO.setRepeatTypeName(dictService.getLabel("woTaskRepeatType", woTaskdefVO.getRepeatType()));
                //设备信息开关
                Optional.ofNullable(woOrderdefMapper.selectById(woTaskdef.getWoId())).ifPresent(woOrderdefPO -> {
                    woTaskdefVO.setDeviceSwitch(woOrderdefPO.getDeviceSwitch());
                });
                woTaskVO.setWoTaskdefVO(woTaskdefVO);
            });
        }
        return list;
    }

    /**
     * 转单列表  查询委派记录表每个实例最新的委派记录且查询委派状态为转单中的工单实例 3
     *
     * @return
     */
    @Override
    public List<WoTaskInstanceVO> mobTransferList(UserInformation user) {
        List<WoTaskInstanceVO> list = new ArrayList<>();
        List<WoTaskAssigneePO> woTaskAssigneePOS = woTaskAssigneeMapper.selectAssignTransfer(user.getStaffId());
        for (WoTaskAssigneePO woTaskAssigneePO : woTaskAssigneePOS) {
            WoTaskInstancePO woTaskInstancePO = woTaskInstanceMapper.selectById(woTaskAssigneePO.getInstId());
            WoTaskInstanceVO woTaskInstanceVO = new WoTaskInstanceVO();
            BeanUtils.copyProperties(woTaskInstancePO, woTaskInstanceVO);
            //任务定义
            WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskMapper.selectById(woTaskInstancePO.getTaskExecId()).getTaskDefId());
            WoTaskdefVO woTaskdefVO = new WoTaskdefVO();
            BeanUtils.copyProperties(woTaskdefPO, woTaskdefVO);
            woTaskdefVO.setOrderTypeName(dictService.getLabel("workOrderType", woTaskdefVO.getOrderType()));
            woTaskdefVO.setBusinessTypeName(dictService.getLabel("woBusinessName", woTaskdefVO.getBusinessType()));
            woTaskdefVO.setOrderLevelName(dictService.getLabel("workOrderLevel", woTaskdefVO.getOrderLevel()));
            woTaskdefVO.setTaskTypeName(dictService.getLabel("woTaskType", woTaskdefVO.getTaskType()));
            woTaskdefVO.setRepeatTypeName(dictService.getLabel("woTaskRepeatType", woTaskdefVO.getRepeatType()));
            StaffInfoFine staffById = staffFeignClient.getStaffById(woTaskdefVO.getInitiatorId());
            Optional.ofNullable(staffById).ifPresent(staffInfo -> {
                woTaskdefVO.setInitiatorMobile(staffInfo.getMobile());
            });
            woTaskInstanceVO.setWoTaskdefVO(woTaskdefVO);
            list.add(woTaskInstanceVO);
        }
        return list;
    }

    @Override
    public void assignTask(WoTaskInstanceDTO woTaskInstanceDTO, UserInformation user) {
        for (WoEodStaffPO staff : woTaskInstanceDTO.getWoEodStaffs()) {
            WoTaskInstancePO woTaskInstancePO = new WoTaskInstancePO();
            BeanUtils.copyProperties(woTaskInstanceDTO, woTaskInstancePO);
            woTaskInstancePO.setAssigneeId(staff.getStaffId());
            woTaskInstancePO.setAssigneeName(staff.getStaffName());
            woTaskInstancePO.setInstStatus("1");//已指派
            //插入任务受理实例表
            woTaskInstanceMapper.insert(woTaskInstancePO);
            //任务表改为执行中
            WoTaskPO woTaskPO = new WoTaskPO();
            woTaskPO.setId(woTaskInstanceDTO.getTaskExecId());
            woTaskPO.setTaskStatus("1");
            woTaskMapper.updateById(woTaskPO);
            //插入委派记录表
            WoTaskAssigneePO woTaskAssigneePO = new WoTaskAssigneePO();
            woTaskAssigneePO.setInstId(woTaskInstancePO.getId());
            woTaskAssigneePO.setTaskId(woTaskInstanceDTO.getTaskExecId());
            woTaskAssigneePO.setInitiatorId(user.getStaffId());
            woTaskAssigneePO.setInitiatorName(user.getStaffName());
            woTaskAssigneePO.setAssigneeId(staff.getStaffId());
            woTaskAssigneePO.setAssigneeName(staff.getStaffName());
            woTaskAssigneePO.setAssigneeStatus("1");
            woTaskAssigneeMapper.insert(woTaskAssigneePO);
            //插入任务执行表
            WoTaskVariablesPO woTaskVariablesPO = new WoTaskVariablesPO();
            Optional.ofNullable(woTaskMapper.selectById(woTaskInstanceDTO.getTaskExecId())).ifPresent(woTaskPO1 -> {
                Optional.ofNullable(woOrderdefMapper.selectById(woTaskPO1.getWoId())).ifPresent(woOrderdefPO -> {
                    Optional.ofNullable(formItemMapper.selectList(Wrappers.<FormItemPO>lambdaQuery()
                            .eq(FormItemPO::getFormId,woOrderdefPO.getFormId())
                            .orderByAsc(FormItemPO::getSort))).ifPresent(formItemPOs -> {
                                formItemPOs.forEach(formItemPO -> {
                                    woTaskVariablesPO.setLabel(formItemPO.getLabel());
                                    woTaskVariablesPO.setFormKey(formItemPO.getFormKey());
                                    woTaskVariablesPO.setTaskInstanceId(woTaskInstancePO.getId());
                                    woTaskVariablesMapper.insert(woTaskVariablesPO);
                                });
                    });
                });
            });
            //处理指派待办
            messageService.dealMessage(new DealMessageInformation(WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_ASSIGN_SUFFIX, String.valueOf(woTaskInstanceDTO.getTaskExecId()), 1));
            //发送执行待办
            WoTaskPO woTaskInfo = woTaskMapper.selectById(woTaskInstanceDTO.getTaskExecId());
            Map<String, Object> params = new HashMap<>();
            WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskInfo.getTaskDefId());
            Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
                params.put("woTaskName", woTaskdef.getName());
                String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_EXECUTE_SUFFIX;
                messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskInstancePO.getAssigneeId(), String.valueOf(woTaskInstancePO.getId()), eventCode, params));
            });
        }
    }

    @Override
    public void mobTransfer(WoTaskAssigneeDTO woTaskAssigneeDTO, UserInformation user) {
        //插入委派记录表
        WoTaskAssigneePO woTaskAssigneePO = new WoTaskAssigneePO();
        BeanUtils.copyProperties(woTaskAssigneeDTO, woTaskAssigneePO);
        woTaskAssigneePO.setInitiatorId(user.getStaffId());
        woTaskAssigneePO.setInitiatorName(user.getStaffName());
        woTaskAssigneePO.setAssigneeStatus("3");//状态为转单中：3
        woTaskAssigneeMapper.insert(woTaskAssigneePO);
        WoTaskPO woTaskInfo = woTaskMapper.selectById(woTaskAssigneeDTO.getTaskId());
        Map<String, Object> params = new HashMap<>();
        WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskInfo.getTaskDefId());
        Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
            params.put("staffName", user.getStaffName());
            params.put("dateTime", DateUtil.now());
            String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_TRANSFER_SUFFIX;
            messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskAssigneeDTO.getAssigneeId(), String.valueOf(woTaskAssigneeDTO.getInstId()), eventCode, params));
        });
    }

    @Override
    public void mobComplete(WoTaskInstanceDTO woTaskInstanceDTO) {
        //如果是审批完后不合格再次提交完成的需要将之前填写的任务调报表的value清空
        woTaskVariablesMapper.update(new WoTaskVariablesPO(),Wrappers.<WoTaskVariablesPO>lambdaUpdate()
                .eq(WoTaskVariablesPO::getTaskInstanceId,woTaskInstanceDTO.getId())
                .set(WoTaskVariablesPO::getValue,null));
        //完成任务填报表
        for (Map.Entry<String, Object> map : woTaskInstanceDTO.getFormExtra().entrySet()) {
            WoTaskVariablesPO woTaskVariablesPO = new WoTaskVariablesPO();
            woTaskVariablesPO.setFormKey(map.getKey());
            woTaskVariablesPO.setValue(String.valueOf(map.getValue()));
            woTaskVariablesPO.setTaskInstanceId(woTaskInstanceDTO.getId());
            FormItemPO form = formItemMapper.selectOne(Wrappers.<FormItemPO>lambdaQuery().eq(FormItemPO::getFormKey, map.getKey()));
            if (null == form) {
                //_label的格式
                form = formItemMapper.selectOne(Wrappers.<FormItemPO>lambdaQuery().eq(FormItemPO::getFormKey, map.getKey().split("_label")[0]));
                woTaskVariablesPO.setLabel(form.getLabel());
                woTaskVariablesMapper.insert(woTaskVariablesPO);
            } else {
                woTaskVariablesPO.setLabel(form.getLabel());
            }
            //woTaskVariablesMapper.insert(woTaskVariablesPO);
            woTaskVariablesMapper.update(woTaskVariablesPO, Wrappers.<WoTaskVariablesPO>lambdaUpdate()
                    .eq(WoTaskVariablesPO::getTaskInstanceId, woTaskVariablesPO.getTaskInstanceId())
                    .eq(WoTaskVariablesPO::getFormKey, woTaskVariablesPO.getFormKey())
                    .set(WoTaskVariablesPO::getValue,woTaskVariablesPO.getValue()));

        }
        //完成实例表 修改实例状态为已完成2 添加描述以及图片
        WoTaskInstancePO woTaskInstancePO = new WoTaskInstancePO();
        BeanUtils.copyProperties(woTaskInstanceDTO, woTaskInstancePO);
        woTaskInstancePO.setInstStatus("2");
        //如果是审批完后不合格再次提交完成的需要将之前审批的置空
        woTaskInstancePO.setRejectReason(null);
        woTaskInstancePO.setRejectPicture(null);
  /*      //关联工单Id校验
        Optional.ofNullable(woTaskInstanceDTO.getTriggerId()).ifPresent(triggerId -> {
            //根据实例Id查询工单配置Id
            String instanceId = woTaskMapper.selectWoIdByInstanceId(woTaskInstanceDTO.getId());
            Optional.ofNullable(instanceId).ifPresent(id -> {
                woOrderdefService.judgeCycle(triggerId, id);
            });
        });*/
        woTaskInstanceMapper.update(woTaskInstancePO, Wrappers.<WoTaskInstancePO>lambdaUpdate()
                .eq(WoTaskInstancePO::getId, woTaskInstanceDTO.getId())
                .set(WoTaskInstancePO::getRejectReason, null)
                .set(WoTaskInstancePO::getRejectPicture, null));
        //派单待办 完成
        messageService.dealMessage(new DealMessageInformation(WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_EXECUTE_SUFFIX, String.valueOf(woTaskInstanceDTO.getId()), 1));
        WoTaskInstancePO woTaskInstanceInfo = woTaskInstanceMapper.selectById(woTaskInstanceDTO.getId());
        //查询本任务实例所对应的任务的其他实例状态是否为已完成如果为已完成那就把该执行任务的状态改为已完成
        List<WoTaskInstancePO> woTaskInstancePOS = woTaskInstanceMapper.selectList(Wrappers.<WoTaskInstancePO>lambdaQuery()
                .eq(WoTaskInstancePO::getTaskExecId, woTaskInstanceInfo.getTaskExecId()));
        boolean flag = true;
        for (WoTaskInstancePO taskInstancePO : woTaskInstancePOS) {
            if (!"2".equals(taskInstancePO.getInstStatus())) {
                flag = false;
            }
        }
        if (flag) {//全都为已完成
            Integer taskExecId = woTaskInstanceInfo.getTaskExecId();
            WoTaskPO woTaskPO = new WoTaskPO();
            woTaskPO.setId(taskExecId);
            woTaskPO.setTaskStatus("2");
            woTaskMapper.updateById(woTaskPO);
            //发待办 验收
            WoTaskPO woTaskInfo = woTaskMapper.selectById(taskExecId);
            Map<String, Object> params = new HashMap<>();
            WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskInfo.getTaskDefId());
            Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
                params.put("woTaskName", woTaskdef.getName());
                params.put("dateTime", DateUtil.now());
                String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_CHECK_SUFFIX;
                messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskInfo.getOwnerId(), String.valueOf(woTaskPO.getId()), eventCode, params));
            });
        }
    }

    /**
     * 根据工单配置Id生成 工单定义任务和 工单执行任务
     * @param triggerId
     */
    private void generateTask(String triggerId) {
        //查询工单配置
        Optional.ofNullable(woOrderdefMapper.selectById(triggerId)).ifPresent(woOrderdefPO -> {
            WoTaskdefPO woTaskdefPO = new WoTaskdefPO();
            BeanUtils.copyProperties(woOrderdefPO,woTaskdefPO);
            woTaskdefPO.setRepeatType("1");
            woTaskdefPO.setWoId(woOrderdefPO.getId());
            woTaskdefPO.setAssignDeptId(woOrderdefPO.getDeptId());
            woTaskdefPO.setAssignDeptName(woOrderdefPO.getDeptName());
            woTaskdefMapper.insert(woTaskdefPO);
            //生成执行工单
            WoTaskPO woTaskPO = new WoTaskPO();
            BeanUtils.copyProperties(woTaskdefPO, woTaskPO);
            woTaskPO.setTaskDefId(woTaskdefPO.getId());
            woTaskPO.setOwnerId(woOrderdefPO.getOwnerId());
            woTaskPO.setOwnerName(woOrderdefPO.getOwnerName());
            woTaskPO.setTaskStatus("0");//已创建
            woTaskMapper.insert(woTaskPO);
            // 发待办
            HashMap<String, Object> params = new HashMap<>();
            params.put("woTaskName", woTaskdefPO.getName());
            String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_ASSIGN_SUFFIX;
            messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskPO.getOwnerId(), String.valueOf(woTaskPO.getId()), eventCode, params));
            //延时队列 跟踪状态
            RDelayedPayload rDelayedPayload = new RDelayedPayload(woTaskPO.getId());
            orderTaskQueue.offer(rDelayedPayload, woTaskdefPO.getExpireTime(), TimeUnit.MINUTES);
        });
    }

    @Override
    public void executeCheck(WoTaskInstanceDTO woTaskInstanceDTO) {
        //已验收：状态改为已验收3 驳回：状态改为执行中1
        String instStatus = woTaskInstanceDTO.getInstStatus();
        //执行任务Id
        Integer taskExecId = woTaskInstanceDTO.getTaskExecId();
        List<WoTaskInstancePO> taskInstancePOList = woTaskInstanceMapper.selectList(Wrappers.<WoTaskInstancePO>lambdaQuery()
                .eq(WoTaskInstancePO::getTaskExecId, taskExecId));
        if ("3".equals(instStatus)) {
            //执行任务的状态改为已验收3
            WoTaskPO woTaskPO = new WoTaskPO();
            woTaskPO.setId(taskExecId);
            woTaskPO.setTaskStatus("3");
            woTaskMapper.updateById(woTaskPO);
            //已验收
            for (WoTaskInstancePO woTaskInstancePO : taskInstancePOList) {
                woTaskInstancePO.setInstStatus("3");
                woTaskInstanceMapper.updateById(woTaskInstancePO);
                //发待办 验收
                WoTaskPO woTaskInfo = woTaskMapper.selectById(woTaskPO.getId());
                WoTaskInstancePO woTaskInstancePOInfo = woTaskInstanceMapper.selectById(woTaskInstancePO);
                Map<String, Object> params = new HashMap<>();
                WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskInfo.getTaskDefId());
                Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
                    params.put("woTaskName", woTaskdef.getName());
                    params.put("dateTime", DateUtil.now());
                    String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_PASS_SUFFIX;
                    messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskInstancePOInfo.getAssigneeId(), String.valueOf(woTaskInstancePO.getId()), eventCode, params));
                });
            }
            //验收完成查看其关联工单 关联触发
            WoTaskPO woTaskInfo = woTaskMapper.selectById(taskExecId);
            WoOrderdefPO woOrderdefPO = woOrderdefMapper.selectById(woTaskInfo.getWoId());
            Optional.ofNullable(woOrderdefPO).ifPresent(woOrderdef -> {
                String linkStrategy = woOrderdef.getLinkStrategy();
                if ("1".equals(linkStrategy)) { //用户自选关联工单
                    //用户自选 关联工单是移动端的工单实例自己决定的（多个关联工单）
                    Optional.ofNullable(woTaskInstanceMapper.selectList(Wrappers.<WoTaskInstancePO>lambdaQuery()
                            .eq(WoTaskInstancePO::getTaskExecId,taskExecId))).ifPresent(woTaskInstancePOS -> {
                        for (WoTaskInstancePO woTaskInstancePO : woTaskInstancePOS) {
                            Optional.ofNullable(woTaskInstancePO.getTriggerId()).ifPresent(triggerId->{
                                generateTask(triggerId);
                            });
                        }
                    });
                } else {
                    //自动触发 关联工单是pc端的工单配置自己决定的（1一个关联工单）
                    Optional.ofNullable(woOrderdefPO.getTriggerIds().iterator().next()).ifPresent(triggerId -> {
                        //有关联工单，生成执行工单 并发送派单待办
                        generateTask(triggerId);
                    });
                }
            });
        } else {
            //执行任务的状态改为进行中1
            WoTaskPO woTaskPO = new WoTaskPO();
            woTaskPO.setId(taskExecId);
            woTaskPO.setTaskStatus("1");
            woTaskMapper.updateById(woTaskPO);
            //驳回 任务实例以及执行任务状态都改为执行中（任务实例保存其驳回原因和图片）
            for (WoTaskInstancePO woTaskInstancePO : taskInstancePOList) {
                woTaskInstancePO.setInstStatus("1");
                woTaskInstancePO.setRejectPicture(woTaskInstanceDTO.getRejectPicture());
                woTaskInstancePO.setRejectReason(woTaskInstanceDTO.getRejectReason());
                woTaskInstanceMapper.update(woTaskInstancePO, Wrappers.<WoTaskInstancePO>lambdaUpdate()
                        .eq(WoTaskInstancePO::getTaskExecId, taskExecId));
                //发待办 驳回
                WoTaskPO woTaskInfo = woTaskMapper.selectById(woTaskPO.getId());
                WoTaskInstancePO woTaskInstancePOInfo = woTaskInstanceMapper.selectById(woTaskInstancePO.getId());
                Map<String, Object> params = new HashMap<>();
                WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskInfo.getTaskDefId());
                Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
                    params.put("woTaskName", woTaskdef.getName());
                    params.put("dateTime", DateUtil.now());
                    String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_COMPLETE_REJECT_SUFFIX;
                    messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskInstancePOInfo.getAssigneeId(), String.valueOf(woTaskInstancePO.getId()), eventCode, params));
                });
            }
        }
        //验收待办完成
        messageService.dealMessage(new DealMessageInformation(WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_CHECK_SUFFIX,
                String.valueOf(woTaskInstanceDTO.getTaskExecId()), 1));
    }

    @Override
    public List<Map<String, Object>> pullList(PullDTO pullDTO) {
        return woTaskInstanceMapper.pullList(pullDTO);
    }

    @Override
    public void mdbGrab(WoTaskInstanceDTO woTaskInstanceDTO, UserInformation user) {
        //任务实例 状态改进行中 受理人是自己
        WoTaskInstancePO woTaskInstancePO = new WoTaskInstancePO();
        woTaskInstancePO.setAssigneeId(user.getStaffId());
        woTaskInstancePO.setAssigneeName(user.getStaffName());
        woTaskInstancePO.setInstStatus("1");
        woTaskInstancePO.setTaskExecId(woTaskInstanceDTO.getTaskExecId());
        woTaskInstanceMapper.insert(woTaskInstancePO);
        //抢完单后需要将此执行任务的状态改为执行中
        WoTaskPO woTaskPO = new WoTaskPO();
        woTaskPO.setId(woTaskInstanceDTO.getTaskExecId());
        woTaskPO.setTaskStatus("1");
        woTaskMapper.updateById(woTaskPO);
        //生成委派记录表 自己委派自己
        WoTaskAssigneePO woTaskAssigneePO = new WoTaskAssigneePO();
        woTaskAssigneePO.setTaskId(woTaskInstanceDTO.getTaskExecId());
        woTaskAssigneePO.setInstId(woTaskInstancePO.getId());
        woTaskAssigneePO.setInitiatorId(user.getStaffId());
        woTaskAssigneePO.setInitiatorName(user.getStaffName());
        woTaskAssigneePO.setAssigneeId(user.getStaffId());
        woTaskAssigneePO.setAssigneeName(user.getStaffName());
        woTaskAssigneeMapper.insert(woTaskAssigneePO);

    }

    @Override
    public void mobReceiveTask(WoTaskInstanceDTO woTaskInstanceDTO, UserInformation user) {
        //生成委派记录：状态为已接单
        WoTaskAssigneePO woTaskAssigneePO = new WoTaskAssigneePO();
        woTaskAssigneePO.setTaskId(woTaskInstanceDTO.getTaskExecId());
        woTaskAssigneePO.setInstId(woTaskInstanceDTO.getId());
        woTaskAssigneePO.setInitiatorId(woTaskInstanceDTO.getAssigneeId());
        woTaskAssigneePO.setInitiatorName(woTaskInstanceDTO.getAssigneeName());
        woTaskAssigneePO.setAssigneeId(user.getStaffId());
        woTaskAssigneePO.setAssigneeName(user.getStaffName());
        woTaskAssigneePO.setAssigneeStatus("2");
        woTaskAssigneeMapper.insert(woTaskAssigneePO);

        //修改任务实例、将受理人改为自己
        WoTaskInstancePO woTaskInstancePO = new WoTaskInstancePO();
        woTaskInstancePO.setId(woTaskInstanceDTO.getId());
        woTaskInstancePO.setAssigneeId(user.getStaffId());
        woTaskInstancePO.setAssigneeName(user.getStaffName());
        woTaskInstanceMapper.updateById(woTaskInstancePO);
        //转单待办的处理
        messageService.dealMessage(new DealMessageInformation(WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_TRANSFER_SUFFIX
                , woTaskInstanceDTO.getId(), 1));
    }

    @Override
    public void mobRejectTask(WoTaskInstanceDTO woTaskInstanceDTO, UserInformation user) {
        //委派记录加已拒单状态
        WoTaskAssigneePO woTaskAssigneePO = new WoTaskAssigneePO();
        woTaskAssigneePO.setTaskId(woTaskInstanceDTO.getTaskExecId());
        woTaskAssigneePO.setInstId(woTaskInstanceDTO.getId());
        woTaskAssigneePO.setInitiatorId(woTaskInstanceDTO.getAssigneeId());
        woTaskAssigneePO.setInitiatorName(woTaskInstanceDTO.getAssigneeName());
        woTaskAssigneePO.setAssigneeId(user.getStaffId());
        woTaskAssigneePO.setAssigneeName(user.getStaffName());
        woTaskAssigneePO.setAssigneeStatus("4");
        woTaskAssigneeMapper.insert(woTaskAssigneePO);
        //转单待办的处理
        messageService.dealMessage(new DealMessageInformation(WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_TRANSFER_SUFFIX
                , woTaskInstanceDTO.getId(), 1));
        //发送拒单消息
        WoTaskPO woTaskInfo = woTaskMapper.selectById(woTaskInstanceDTO.getTaskExecId());
        WoTaskInstancePO woTaskInstancePOInfo = woTaskInstanceMapper.selectById(woTaskInstanceDTO.getId());
        Map<String, Object> params = new HashMap<>();
        WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskInfo.getTaskDefId());
        Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef -> {
            params.put("woTaskName", woTaskdef.getName());
            params.put("dateTime", DateUtil.now());
            String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_TRANSFER_REJECT_SUFFIX;
            messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskInstancePOInfo.getAssigneeId(), String.valueOf(woTaskInstanceDTO.getId()), eventCode, params));
        });
    }

    @Override
    public Page<TaskFormVO> getTaskFormByPage(TaskFormDTO taskFormDTO) {
        Page<TaskFormVO> taskFormByPage = woTaskMapper.getTaskFormByPage(taskFormDTO.toPage(), taskFormDTO);
        for (TaskFormVO record : taskFormByPage.getRecords()) {
            record.setTaskTypeName(dictService.getLabel("woTaskType", record.getTaskType()));
            record.setBusinessTypeName(dictService.getLabel("woBusinessName", record.getBusinessType()));
            record.setOrderTypeName(dictService.getLabel("workOrderType", record.getOrderType()));
            List<Integer> taskExecIds = woTaskMapper.selectList(Wrappers.<WoTaskPO>lambdaQuery()
                    .eq(WoTaskPO::getWoId, record.getId())).stream().map(WoTaskPO::getId).collect(Collectors.toList());
            if (null!=taskExecIds&&taskExecIds.size()>0){
                List<DetailsVO> details = woTaskInstanceMapper.selectByTaskExecId(taskExecIds);
                record.setDetails(details);
                record.setDataSize(details.size());
                for (DetailsVO detailsVO : details) {
                    List<WoTaskVariablesPO> woTaskVariables = woTaskVariablesMapper.selectList(Wrappers.<WoTaskVariablesPO>lambdaQuery()
                            .eq(WoTaskVariablesPO::getTaskInstanceId, detailsVO.getTaskInstanceId()));
                    detailsVO.setWoTaskVariables(woTaskVariables);
                }
            }else{
                record.setDataSize(0);
            }
        }
        return taskFormByPage;
    }

    @Override
    public void taskFormExport(HttpServletResponse response, TaskFormDTO taskFormDTO) {
        List<TaskFormExport> formExportList = woTaskMapper.getFormExportList(taskFormDTO);
        for (TaskFormExport formVO : formExportList) {
            formVO.setOrderTypeName(dictService.getLabel("workOrderType", formVO.getOrderType()));
            formVO.setBusinessTypeName(dictService.getLabel("woBusinessName", formVO.getBusinessType()));
            List<Integer> taskExecIds = woTaskMapper.selectList(Wrappers.<WoTaskPO>lambdaQuery()
                    .eq(WoTaskPO::getWoId, formVO.getId())).stream().map(WoTaskPO::getId).collect(Collectors.toList());
            if (null!=taskExecIds&&taskExecIds.size()>0){
                List<DetailsVO> details = woTaskInstanceMapper.selectByTaskExecId(taskExecIds);
                formVO.setDataSize(details.size());
            }else {
                formVO.setDataSize(0);
            }
        }
        excelUtils.export(response, "任务填报表导出", formExportList, TaskFormExport.class);
    }

    @Override
    public void execTask() {
        //校验生效
        List<WoTaskdefPO> taskdefPOS = woTaskdefMapper.selectList(Wrappers.emptyWrapper());
        for (WoTaskdefPO taskdefPO : taskdefPOS) {
            String repeatType = taskdefPO.getRepeatType();
            if ("2".equals(repeatType)) {
                //每天定时
                List<WoTaskdefRepeatPO> woTaskdefRepeatPOS = woTaskdefRepeatMapper.selectList(Wrappers.<WoTaskdefRepeatPO>lambdaQuery()
                        .eq(WoTaskdefRepeatPO::getTaskDefId, taskdefPO.getId()));
                for (WoTaskdefRepeatPO woTaskdefRepeatPO : woTaskdefRepeatPOS) {
                    createTaskAndSendMsg(woTaskdefRepeatPO, taskdefPO);
                }

            } else if ("3".equals(repeatType)) {
                //起止时间
                //先校验是否生效
                Date startTime = taskdefPO.getStartTime();
                Date endTime = taskdefPO.getEndTime();
                boolean flag = DateUtil.isIn(new Date(), startTime, endTime);
                if (flag) {
                    //查询其定义的执行时间
                    List<WoTaskdefRepeatPO> woTaskdefRepeatPOS = woTaskdefRepeatMapper.selectList(Wrappers.<WoTaskdefRepeatPO>lambdaQuery()
                            .eq(WoTaskdefRepeatPO::getTaskDefId, taskdefPO.getId()));
                    for (WoTaskdefRepeatPO woTaskdefRepeatPO : woTaskdefRepeatPOS) {
                        createTaskAndSendMsg(woTaskdefRepeatPO, taskdefPO);
                    }
                }
            }
        }
    }

    @Override
    public void pushOrder(PushOrderPayload payload) {
        log.info("===========================================================================");
        log.info("触发工单：" + payload.getId());
        log.info("===========================================================================");

        WoOrderdefPO woOrderdefPO = woOrderdefMapper.selectById(payload.getId());
        if (null==woOrderdefPO){
            log.info("触发工单：" + payload.getId()+"不存在，请检查该配置工单是否存在！");
            return;
        }
        Integer areaId = payload.getAreaId();
        String tenantId = payload.getTenantId();
        String spaceId = payload.getSpaceId();
        String position = payload.getPosition();
        Integer initiatorId = payload.getInitiatorId();
        String initiatorName = payload.getInitiatorName();
        //插入定义任务表
        WoTaskdefPO woTaskdefPO = new WoTaskdefPO();
        BeanUtils.copyProperties(woOrderdefPO, woTaskdefPO);
        woTaskdefPO.setWoId(woOrderdefPO.getId());
        woTaskdefPO.setAssignDeptId(woOrderdefPO.getDeptId());
        woTaskdefPO.setAssignDeptName(woOrderdefPO.getDeptName());
        woTaskdefPO.setRepeatType("1");
        woTaskdefPO.setAreaId(areaId);
        woTaskdefPO.setTenantId(tenantId);
        woTaskdefPO.setSpaceId(spaceId);
        woTaskdefPO.setPosition(position);
        woTaskdefPO.setInitiatorId(initiatorId);
        woTaskdefPO.setInitiatorName(initiatorName);
        Optional.ofNullable(payload.getDescription()).ifPresent(description->{
            woTaskdefPO.setDescription(description);
        });
        Optional.ofNullable(payload.getDeviceId()).ifPresent(deviceId->{
            //如果有设备相关信息
            GetDeviceInfoVo deviceInfoVo = bmpDeviceFeignClient.deviceInfoReportInfo(deviceId);
            woTaskdefPO.setSpaceId(deviceInfoVo.getSpaceId());
            woTaskdefPO.setTenantId(deviceInfoVo.getBuildingId());
            woTaskdefPO.setAreaId(deviceInfoVo.getAreaId());
            woTaskdefPO.setPosition(deviceInfoVo.getOfficeAddress());
            if (null!=position){
                woTaskdefPO.setAreaId(areaId);
                woTaskdefPO.setTenantId(tenantId);
                woTaskdefPO.setSpaceId(spaceId);
                woTaskdefPO.setPosition(position);
            }
        });

        if (null != payload.getDescribes() && payload.getDescribes().size() > 0) {
            payload.getDescribes().forEach(describeItem -> {
                if ("eoId".equals(describeItem.getLabel())){
                    woTaskdefPO.setEoId(describeItem.getValue().getValue());
                }
                if ("mId".equals(describeItem.getLabel())){
                    woTaskdefPO.setMId(Integer.valueOf(describeItem.getValue().getValue()));
                }
            });
        }
        woTaskdefMapper.insert(woTaskdefPO);
        //插入执行任务
        WoTaskPO woTaskPO = new WoTaskPO();
        BeanUtils.copyProperties(woOrderdefPO, woTaskPO);
        woTaskPO.setAssignDeptId(woOrderdefPO.getDeptId());
        woTaskPO.setAssignDeptName(woOrderdefPO.getDeptName());
        woTaskPO.setWoId(woOrderdefPO.getId());
        woTaskPO.setTaskStatus("0");
        woTaskPO.setTaskDefId(woTaskdefPO.getId());
        woTaskPO.setInitiatorId(initiatorId);
        woTaskPO.setInitiatorName(initiatorName);
        woTaskMapper.insert(woTaskPO);
        // 发待办
        HashMap<String, Object> params = new HashMap<>();
        params.put("woTaskName", woTaskdefPO.getName());
        String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_ASSIGN_SUFFIX;
        messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskPO.getOwnerId(), String.valueOf(woTaskPO.getId()), eventCode, params));
        //延时队列 跟踪状态
        RDelayedPayload rDelayedPayload = new RDelayedPayload(woTaskPO.getId());
        orderTaskQueue.offer(rDelayedPayload, woTaskdefPO.getExpireTime(), TimeUnit.MINUTES);
        //插入任务触发描述表
        Optional.ofNullable(payload.getDescribes()).ifPresent(describes -> {
            for (DescribeItem describe : describes) {
                WoTaskDescribePO woTaskDescribePO = new WoTaskDescribePO();
                woTaskDescribePO.setTaskExecId(woTaskPO.getId());
                woTaskDescribePO.setType(describe.getValue().getClass().getSimpleName());
                woTaskDescribePO.setLabel(describe.getLabel());
                woTaskDescribePO.setDescribeValue(describe.getValue().getValue());
                woTaskDescribeMapper.insert(woTaskDescribePO);
            }
        });
    }

    @Override
    public Integer getTranAndGrapCount(UserInformation user) {
        int transferCount = mobTransferList(user).size();
        int grabConut = mobGrabList().size();
        return transferCount + grabConut;
    }

    private void createTaskAndSendMsg(WoTaskdefRepeatPO woTaskdefRepeatPO, WoTaskdefPO taskdefPO) {
        String execTime = woTaskdefRepeatPO.getExecTime();
        long diff = DateUtil.between(new Date(), DateUtil.parse(execTime), DateUnit.MINUTE);
        //生成执行任务
        WoTaskPO woTaskPO = new WoTaskPO();
        BeanUtils.copyProperties(taskdefPO, woTaskPO);
        woTaskPO.setTaskDefId(taskdefPO.getId());
        woTaskPO.setTaskStatus("0");
        StaffInfoFine staffInfo = staffFeignClient.getStaffById(taskdefPO.getInitiatorId());
        woTaskPO.setMobile(staffInfo.getMobile());
        WoOrderdefPO woOrderdefPO = woOrderdefMapper.selectById(taskdefPO.getWoId());
        woTaskPO.setOwnerId(woOrderdefPO.getOwnerId());
        woTaskPO.setOwnerName(woOrderdefPO.getOwnerName());
        woTaskMapper.insert(woTaskPO);
        //提前十分钟 发待办给主管
        long beforeDif = diff - 10;
        Map<String, Object> extra = new HashMap<>();
        extra.put("woTaskPO", woTaskPO);
        orderWarnTaskQueue.offer(new RDelayedPayload(woTaskPO.getId(), extra), beforeDif, TimeUnit.MINUTES);
        //跟踪状态
        orderTaskQueue.offer(new RDelayedPayload(woTaskPO.getId()), diff, TimeUnit.MINUTES);
    }
}
