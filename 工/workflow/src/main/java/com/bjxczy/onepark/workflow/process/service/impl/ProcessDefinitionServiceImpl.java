package com.bjxczy.onepark.workflow.process.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.core.web.log.LogContextHolder;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;
import com.bjxczy.onepark.workflow.form.facade.IProcessFacade;
import com.bjxczy.onepark.workflow.process.core.constant.ProcessConstant;
import com.bjxczy.onepark.workflow.process.entity.ProcdefNodePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;
import com.bjxczy.onepark.workflow.process.facade.IFormFacade;
import com.bjxczy.onepark.workflow.process.mapper.ProcdefMapper;
import com.bjxczy.onepark.workflow.process.pojo.dto.CreateProcessDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryProcessPageDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefActiveVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefDetailVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefVO;
import com.bjxczy.onepark.workflow.process.service.ProcdefNodeService;
import com.bjxczy.onepark.workflow.process.service.ProcdefStaffService;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;
import com.bjxczy.onepark.workflow.process.service.ProcessVersionService;

@Service
public class ProcessDefinitionServiceImpl extends ServiceImpl<ProcdefMapper, ProcdefPO>
		implements ProcessDefinitionService, IProcessFacade {

	@Autowired
	private ProcdefStaffService procdefStaffService;
	@Autowired
	private ProcessVersionService processVersionService;
	@Autowired
	private ProcdefNodeService procdefNodeService;
	@Autowired
	private IFormFacade formFacade;

	@Override
	public IPage<ProcdefVO> pageList(QueryProcessPageDTO dto) {
		return this.baseMapper.queryPage(dto.toPage(), dto);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void create(CreateProcessDTO dto) {
		ProcdefPO po = new ProcdefPO(dto.getName(), dto.getRemark(), dto.getApplyRange(), dto.getIcon());
		this.save(po);
		po.setDeployId("Proc_" + po.getId());
		this.updateById(po);
		// 保存允许申请员工
		if (null != dto.getStaffIds() && dto.getStaffIds().size() > 0) {
			procdefStaffService.saveByProcess(po, dto.getStaffIds());
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void edit(String id, CreateProcessDTO dto) {
		ProcdefPO po = getById(id);
		if (po != null) {
			po.setName(dto.getName());
			po.setRemark(dto.getRemark());
			po.setApplyRange(dto.getApplyRange());
			po.setIcon(dto.getIcon());
			this.updateById(po);
			// 保存允许申请员工
			if (null != dto.getStaffIds() && dto.getStaffIds().size() > 0) {
				procdefStaffService.saveByProcess(po, dto.getStaffIds());
			}
		}
	}

	@Override
	public ProcdefDetailVO getDetail(String id) {
		ProcdefVO vo = baseMapper.findVOById(id);
		ProcdefDetailVO detail = new ProcdefDetailVO();
		BeanUtils.copyProperties(vo, detail);
		detail.setAllowStaffList(procdefStaffService.listStaffInfo(vo.getId()));
		return detail;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void removeProcess(String id) {
		ProcdefPO po = this.getById(id);
		if (ProcessConstant.TYPE_BUSINESS_PROCESS.equals(po.getDeployType())) {
			throw new ResultException("业务流程无法被删除！");
		}
		if (po != null) {
			LogContextHolder.put("name", po.getName()); // 操作日志
			this.removeById(id);
			// 删除关联员工
			procdefStaffService.removeByProcdefId(id);
			// 删除流程版本
			processVersionService.removeByProcdefId(id);
		}
	}

	@Override
	public boolean isBandingProcess(String formId) {
		List<ProcdefVersionPO> list = processVersionService.getByFormId(formId);
		return list.size() > 0;
	}

	@Override
	public ProcdefPO getByDeployId(String deployId) {
		return lambdaQuery().eq(ProcdefPO::getDeployId, deployId).one();
	}

	@Override
	public ProcdefActiveVO getProcdefActive(String procdefId) {
		ProcdefPO def = this.getById(procdefId);
		ProcdefVersionPO version = processVersionService.getById(def.getActVersion());
		ProcdefActiveVO data = new ProcdefActiveVO(def);
		// 构建返回数据
		FormdefPO formdef = formFacade.getFormdefById(version.getFormId());
		data.setFormdef(formdef);
		List<FormItemPO> formItems = formFacade.listByFormId(version.getFormId());
		data.setFormItems(formItems);
		return data;
	}

	@Override
	public ProcdefNodePO getActiveProcdefNode(String deployId, String taskDefinitionKey) {
		ProcdefPO def = this.getByDeployId(deployId);
		return procdefNodeService.getByTagId(def.getActVersion(), taskDefinitionKey);
	}

	@Override
	public ProcdefVersionPO getActiveVersionByDeployId(String key) {
		ProcdefPO def = getByDeployId(key);
		return processVersionService.getById(def.getActVersion());
	}

	@Override
	public void updateStatus(String id, Integer enable) {
		ProcdefPO po = getById(id);
		if (po == null) {
			throw new ResultException("流程不存在！");
		}
		LogContextHolder.put("name", po.getName());
		if (enable == 1 && StringUtils.isBlank(po.getActVersion())) {
			throw new ResultException("流程未启用版本");
		}
		// TODO 流程校验
		po.setEnable(enable);
		updateById(po);
	}

	@Override
	public List<ProcdefPO> listByEnable(Integer enable) {
		return lambdaQuery().eq(enable != null, ProcdefPO::getEnable, enable).orderByAsc(ProcdefPO::getCreateTime)
				.list();
	}

}
