package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName WoTaskDescribePO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/12 9:59
 * @Version 1.0
 */
@Data
@TableName("wo_task_describe")
public class WoTaskDescribePO {
    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private Integer taskExecId;

    private String type;

    private String label;

    private String describeValue;

}
