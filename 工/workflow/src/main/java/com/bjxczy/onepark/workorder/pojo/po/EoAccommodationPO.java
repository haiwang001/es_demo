package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 酒店EO单-住宿表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="EoAccommodation对象", description="酒店EO单-住宿表")
@TableName("eo_accommodation")
public class EoAccommodationPO {


    @ApiModelProperty(value = "主键Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "酒店EO单Id")
    private Integer hotelId;

    @ApiModelProperty(value = "房型（字典：roomType）")
    private String roomType;

    @ApiModelProperty(value = "房型数量")
    private Integer typeCount;

}
