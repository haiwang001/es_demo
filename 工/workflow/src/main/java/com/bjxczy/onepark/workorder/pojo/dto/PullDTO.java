package com.bjxczy.onepark.workorder.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * @ClassName PullDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/7 10:09
 * @Version 1.0
 */
@Data
public class PullDTO {
    private List<Integer> deptIds;

    private List<String> orderIds;
}
