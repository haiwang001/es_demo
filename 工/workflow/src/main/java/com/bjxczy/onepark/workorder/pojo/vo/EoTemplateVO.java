package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoTemplatePO;
import lombok.Data;

/**
 * @ClassName EoTemplateVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/16 14:01
 * @Version 1.0
 */
@Data
public class EoTemplateVO extends EoTemplatePO {
}
