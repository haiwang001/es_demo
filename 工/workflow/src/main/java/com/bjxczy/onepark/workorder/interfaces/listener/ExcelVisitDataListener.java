package com.bjxczy.onepark.workorder.interfaces.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.workorder.mapper.EoVisitInfoMapper;
import com.bjxczy.onepark.workorder.pojo.dto.EoVisitInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @version 1.0.0
 * @ClassName ExcelDataListener
 * @Description 读取数据监听器
 * @createTime 2022年01月09日
 */
@Component
@Slf4j
public class ExcelVisitDataListener extends AnalysisEventListener<EoVisitInfoDTO> {

    List<EoVisitInfoDTO> list = new ArrayList<>();

    @Autowired
    private EoVisitInfoMapper eoVisitInfoMapper;

    public ExcelVisitDataListener(EoVisitInfoMapper eoVisitInfoMapper) {
        this.eoVisitInfoMapper = eoVisitInfoMapper;
    }

    @Override
    public void invoke(EoVisitInfoDTO eoVisitInfoData, AnalysisContext context) {
        log.info("解析到一条数据:{}", JSON.toJSONString(eoVisitInfoData));
        list.add(eoVisitInfoData);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        log.info("所有数据解析完成！");
    }

    public List<EoVisitInfoDTO> getList() {
        return list;
    }
    public void setList(List<EoVisitInfoDTO> list){
        this.list=list;
    }
}

