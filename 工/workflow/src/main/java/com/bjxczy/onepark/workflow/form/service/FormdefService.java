package com.bjxczy.onepark.workflow.form.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;
import com.bjxczy.onepark.workflow.form.pojo.dto.CreateFormDTO;
import com.bjxczy.onepark.workflow.form.pojo.dto.QueryFormPageDTO;
import com.bjxczy.onepark.workflow.form.pojo.dto.UpdateFormDTO;
import com.bjxczy.onepark.workflow.form.pojo.vo.FormVO;

public interface FormdefService extends IService<FormdefPO> {

	IPage<FormdefPO> pageList(QueryFormPageDTO dto);

	void create(CreateFormDTO dto);

	void update(String id, UpdateFormDTO dto);

	void removeForm(String id);

	FormVO findVOById(String id);

	void updateStatus(String id, Integer enable);

	List<FormdefPO> findByEnable(Integer enable);

}
