package com.bjxczy.onepark.workflow.process.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.CreateVersionDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.UpdateVersionDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefVersionVO;

public interface ProcessVersionService extends IService<ProcdefVersionPO> {

	List<ProcdefVersionPO> listByProcdefId(String procdefId);

	void create(String procdefId, CreateVersionDTO dto);

	void edit(String procdefId, String versionId, UpdateVersionDTO dto);

	void delete(String procdefId, String versionId);

	void removeByProcdefId(String id);

	List<ProcdefVersionPO> getByFormId(String formId);

	void deploy(String procdefId, String versionId);

	ProcdefVersionVO getDetail(String versionId);

}
