package com.bjxczy.onepark.workorder.interfaces.listener;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.workorder.event.RemoveStaffEvent;
import com.bjxczy.onepark.workorder.event.UpdateStaffEvent;
import com.bjxczy.onepark.workorder.mapper.WoEodGroupMapper;
import com.bjxczy.onepark.workorder.mapper.WoEodStaffMapper;
import com.bjxczy.onepark.workorder.mapper.WoEodStaffScheduleMapper;
import com.bjxczy.onepark.workorder.pojo.po.WoEodGroupPO;
import com.bjxczy.onepark.workorder.pojo.po.WoEodStaffPO;
import com.bjxczy.onepark.workorder.pojo.po.WoEodStaffSchedulePO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * @ClassName StaffEventListener
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/29 15:45
 * @Version 1.0
 */
@Component
@Slf4j
public class StaffEventListener {

    @Autowired
    private WoEodStaffMapper woEodStaffMapper;

    @Autowired
    private WoEodGroupMapper woEodGroupMapper;

    @Autowired
    private WoEodStaffScheduleMapper woEodStaffScheduleMapper;

    @EventListener
    public void removeStaffListener(RemoveStaffEvent event) {
        log.info("[workorder] RemoveStaffEvent: {}", event);
        //删除班组-员工
        woEodStaffMapper.delete(Wrappers.<WoEodStaffPO>lambdaQuery()
                .eq(WoEodStaffPO::getStaffId,event.getStaffId()));
        //停用该员工所在班组
        WoEodStaffPO woEodStaffPO = woEodStaffMapper.selectOne(Wrappers.<WoEodStaffPO>lambdaQuery()
                .eq(WoEodStaffPO::getStaffId, event.getStaffId()));
        woEodGroupMapper.update(new WoEodGroupPO(),Wrappers.<WoEodGroupPO>lambdaUpdate()
                .eq(WoEodGroupPO::getId,woEodStaffPO.getEodId())
                .set(WoEodGroupPO::getStatus,0));
        //删除员工排班
        woEodStaffScheduleMapper.delete(Wrappers.<WoEodStaffSchedulePO>lambdaQuery()
                .eq(WoEodStaffSchedulePO::getStaffId,event.getStaffId()));


    }
    @EventListener
    public void updateStaffListener(UpdateStaffEvent event) {
        log.info("[workorder] UpdateStaffEvent: {}", event);
        //更新班组-员工
        StaffInformation staffInformation = event.getStaffInformation();
        WoEodStaffPO woEodStaffPO = new WoEodStaffPO();
        woEodStaffMapper.update(woEodStaffPO, Wrappers.<WoEodStaffPO>lambdaUpdate()
                .eq(WoEodStaffPO::getStaffId,staffInformation.getId())
                .set(null!=staffInformation.getFldName(),WoEodStaffPO::getStaffName,staffInformation.getFldName()));
        //更新员工排班的员工信息
        WoEodStaffSchedulePO woEodStaffSchedulePO = new WoEodStaffSchedulePO();
        String deptName=null;
        StringBuilder sb = new StringBuilder();
        if (null!=staffInformation.getDeptNames()&&staffInformation.getDeptNames().size()>0){
            for (DeptNameInfo info : staffInformation.getDeptNames()) {
                sb.append(info.getDeptName()).append("/");
            }
         deptName=sb.deleteCharAt(sb.lastIndexOf("/")).toString();
        }
        woEodStaffScheduleMapper.update(woEodStaffSchedulePO,Wrappers.<WoEodStaffSchedulePO>lambdaUpdate()
                .eq(WoEodStaffSchedulePO::getStaffId,staffInformation.getId())
                .set(null!=staffInformation.getFldName(),WoEodStaffSchedulePO::getStaffName,staffInformation.getFldName())
                .set(null!=staffInformation.getMobile(),WoEodStaffSchedulePO::getMobile,staffInformation.getMobile())
                .set(null!=staffInformation.getPositionName(),WoEodStaffSchedulePO::getPositionName,staffInformation.getPositionName())
                .set(null!=deptName,WoEodStaffSchedulePO::getDeptName,deptName));
    }
}
