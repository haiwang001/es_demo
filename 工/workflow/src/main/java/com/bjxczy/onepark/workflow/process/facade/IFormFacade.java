package com.bjxczy.onepark.workflow.process.facade;

import java.util.List;

import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;

public interface IFormFacade {

	List<FormItemPO> listByFormId(String formId);

	FormdefPO getFormdefById(String formId);

}
