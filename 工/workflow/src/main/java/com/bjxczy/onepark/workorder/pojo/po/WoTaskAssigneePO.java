package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 任务委派记录表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-04-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("wo_task_assignee")
@ApiModel(value="WoTaskAssignee对象", description="任务委派记录表")
public class WoTaskAssigneePO extends UserOperator {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "自增ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "任务Id")
    private Integer taskId;

    @ApiModelProperty(value = "实例ID")
    private String instId;

    @ApiModelProperty(value = "发起员工ID")
    private Integer initiatorId;

    @ApiModelProperty(value = "发起员工姓名")
    private String initiatorName;

    @ApiModelProperty(value = "受理人ID")
    private Integer assigneeId;

    @ApiModelProperty(value = "受理人姓名")
    private String assigneeName;

    @ApiModelProperty(value = "委派状态（字典：woTaskAssigneeStatus）")
    private String assigneeStatus;


}
