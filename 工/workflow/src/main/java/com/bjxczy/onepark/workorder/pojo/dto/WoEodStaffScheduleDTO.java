package com.bjxczy.onepark.workorder.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WoEodStaffScheduleDTO {
    private String staffName;

    private String eodDate;

    private String mobile;

    @ApiModelProperty("值班状态：1值班中 0休息中")
    private Integer status;

}
