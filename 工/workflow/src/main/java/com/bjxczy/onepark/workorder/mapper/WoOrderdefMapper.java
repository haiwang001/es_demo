package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.dto.WoOrderdefDto;
import com.bjxczy.onepark.workorder.pojo.po.WoOrderdefExcelPO;
import com.bjxczy.onepark.workorder.pojo.po.WoOrderdefPO;
import com.bjxczy.onepark.workorder.pojo.vo.WoOrderdefVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 工单配置 表 Mapper 接口
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-30
 */
@Mapper
public interface WoOrderdefMapper extends BaseMapper<WoOrderdefPO> {

    Page<WoOrderdefVO> listPage(@Param("toPage") Page<Object> toPage, @Param("dto") WoOrderdefDto dto);

    List<WoOrderdefExcelPO> getExportList(WoOrderdefDto woOrderdefDto);

}
