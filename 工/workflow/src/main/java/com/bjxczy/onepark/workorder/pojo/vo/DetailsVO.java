package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.WoTaskVariablesPO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName DetailsVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/8 13:29
 * @Version 1.0
 */
@Data
public class DetailsVO {
    private String time;

    private String assigneeName;

    private String taskInstanceId;

    private List<WoTaskVariablesPO> woTaskVariables;
}
