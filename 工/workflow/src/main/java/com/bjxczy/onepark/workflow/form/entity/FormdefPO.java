package com.bjxczy.onepark.workflow.form.entity;

import java.util.Map;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.bjxczy.core.web.base.UserOperator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@TableName(value = "wf_formdef", autoResultMap = true)
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class FormdefPO extends UserOperator {

	private static final long serialVersionUID = 7691975853677431735L;

	@TableId(type = IdType.ASSIGN_UUID)
	private String id;

	private String name;

	private String remark;

	private Integer enable;

	private Integer formType;

	@TableField(typeHandler = FastjsonTypeHandler.class)
	private Map<String, Object> extra;

	@TableField(exist = false)
	private String formTypeName;

	public FormdefPO(String name, String remark) {
		super();
		this.name = name;
		this.remark = remark;
		this.enable = 1;
		this.formType = 2;
	}

}
