package com.bjxczy.onepark.workflow.process.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.log.LogContextHolder;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.common.DictInformation;
import com.bjxczy.onepark.workflow.process.core.bpmn.BpmnModelConvert;
import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;
import com.bjxczy.onepark.workflow.process.mapper.ProcdefVersionMapper;
import com.bjxczy.onepark.workflow.process.pojo.dto.CreateVersionDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.UpdateVersionDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefVersionVO;
import com.bjxczy.onepark.workflow.process.service.ProcdefLineService;
import com.bjxczy.onepark.workflow.process.service.ProcdefNodeService;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;
import com.bjxczy.onepark.workflow.process.service.ProcessVersionService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProcessVersionServiceImpl extends ServiceImpl<ProcdefVersionMapper, ProcdefVersionPO>
		implements ProcessVersionService {

	@Autowired
	private ProcessDefinitionService processDefService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private ProcdefNodeService procdefNodeService;
	@Autowired
	private ProcdefLineService procdefLineService;
	@Autowired
	private DictService dictService;

	@Override
	public List<ProcdefVersionPO> listByProcdefId(String procdefId) {
		List<ProcdefVersionPO> data = lambdaQuery()
				.select(ProcdefVersionPO::getId, ProcdefVersionPO::getProcdefId, ProcdefVersionPO::getVersionName,
						ProcdefVersionPO::getRemark, ProcdefVersionPO::getUpdateTime, ProcdefVersionPO::getCreateTime,
						ProcdefVersionPO::getOperator)
				.eq(ProcdefVersionPO::getProcdefId, procdefId).orderByDesc(ProcdefVersionPO::getUpdateTime).list();
		return data;
	}

	@Override
	public void create(String procdefId, CreateVersionDTO dto) {
		ProcdefVersionPO po = new ProcdefVersionPO(procdefId, dto.getVersion(), dto.getRemark(), dto.getFormId());
		this.save(po);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void edit(String procdefId, String versionId, UpdateVersionDTO dto) {
		ProcdefVersionPO po = this.getById(versionId);
		if (po != null) {
			po.edit(dto.getVersion(), dto.getRemark(), dto.getFormId(), dto.getSourceCode());
			this.updateById(po);
			// 保存节点配置
			procdefNodeService.saveByVersion(po, dto.getNodes());
			// 保存连线配置
			procdefLineService.saveByVersion(po, dto.getLines());
		}
	}

	@Override
	public void delete(String procdefId, String versionId) {
		ProcdefVersionPO po = this.getById(versionId);
		if (po != null) {
			// 日志记录
			LogContextHolder.put("name", po.getVersionName());
			lambdaUpdate().eq(ProcdefVersionPO::getProcdefId, procdefId).eq(ProcdefVersionPO::getId, versionId)
					.remove();
		}
	}

	@Override
	public void removeByProcdefId(String procdefId) {
		lambdaUpdate().eq(ProcdefVersionPO::getProcdefId, procdefId).remove();
	}

	@Override
	public List<ProcdefVersionPO> getByFormId(String formId) {
		return lambdaQuery().eq(ProcdefVersionPO::getFormId, formId).list();
	}

	@Override
	public void deploy(String procdefId, String versionId) {
		ProcdefPO procdef = processDefService.getById(procdefId);
		if (procdef == null) {
			throw new ResultException("流程不存在！");
		}
		ProcdefVersionPO version = this.getById(versionId);
		if (version == null) {
			throw new ResultException("版本不存在！");
		}
		List<ProcdefLinePO> lines = procdefLineService.listByVersionId(versionId);
		Map<String, DictInformation> dictMap = dictService.valueKeyMap("conditionType");
		lines.forEach(e -> {
			if (StringUtils.isNotBlank(e.getConditionType())) {
				DictInformation dict = dictMap.get(e.getConditionType());
				e.setExtra(dict.getExtra());
			}
		});
		Map<String, ProcdefLinePO> lineConfig = lines.stream().collect(Collectors.toMap(ProcdefLinePO::getTagId, e -> e));
		BpmnModel model = BpmnModelConvert.xml2Model(version.getSourceCode(), procdef, lineConfig);
		Deployment deployment = repositoryService.createDeployment().name(procdef.getName())
				.category(procdef.getDeployType()).key(procdef.getDeployId())
				.addBpmnModel(procdef.getDeployId() + ".bpmn", model).deploy();
//				.addString(procdef.getDeployId() + ".bpmn20.xml", version.getSourceCode()).deploy();
		// 更新默认激活版本
		procdef.setActVersion(version.getId());
		processDefService.updateById(procdef);
		log.info("[CreateProcess] 部署流程：deploymentId: {}", deployment.getId());
	}

	@Override
	public ProcdefVersionVO getDetail(String versionId) {
		ProcdefVersionPO po = this.getById(versionId);
		ProcdefVersionVO vo = new ProcdefVersionVO();
		vo.setNodes(procdefNodeService.listByVersionId(versionId));
		vo.setLines(procdefLineService.listByVersionId(versionId));
		BeanUtils.copyProperties(po, vo);
		return vo;
	}

}
