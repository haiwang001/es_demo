package com.bjxczy.onepark.workorder.feign;

import com.bjxczy.core.feign.client.device.GetDeviceInfoFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @ClassName BmpDeviceFeignClient
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/10 11:31
 * @Version 1.0
 */
@FeignClient("bmpdevice")
public interface BmpDeviceFeignClient extends GetDeviceInfoFeignClient {
}
