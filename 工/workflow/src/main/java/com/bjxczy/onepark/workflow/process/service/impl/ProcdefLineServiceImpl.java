package com.bjxczy.onepark.workflow.process.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;
import com.bjxczy.onepark.workflow.process.mapper.ProcdefLineMapper;
import com.bjxczy.onepark.workflow.process.service.ProcdefLineService;

@Service
public class ProcdefLineServiceImpl extends ServiceImpl<ProcdefLineMapper, ProcdefLinePO>
		implements ProcdefLineService {

	@Override
	public void saveByVersion(ProcdefVersionPO version, List<ProcdefLinePO> lines) {
		lambdaUpdate().eq(ProcdefLinePO::getVersionId, version.getId()).remove();
		lines.forEach(e -> e.setVersionId(version.getId()));
		this.saveBatch(lines);
	}

	@Override
	public ProcdefLinePO getByConditionType(String versionId, String conditionType) {
		return lambdaQuery().eq(ProcdefLinePO::getVersionId, versionId)
				.eq(ProcdefLinePO::getConditionType, conditionType).one();
	}

	@Override
	public List<ProcdefLinePO> listByVersionId(String versionId) {
		return lambdaQuery().eq(ProcdefLinePO::getVersionId, versionId).list();
	}

}
