package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 酒店EO单-会议表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="EoMetting对象", description="酒店EO单-会议表")
@TableName("eo_metting")
public class EoMettingPO {


    @ApiModelProperty(value = "主键Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "酒店EO单id")
    private Integer hotelId;

    @ApiModelProperty(value = "会议主题")
    private String topic;

    @ApiModelProperty(value = "会议开始时间")
    private String startTime;

    @ApiModelProperty(value = "会议结束时间")
    private String endTime;

    @ApiModelProperty(value = "会议室Id")
    private String metRoomId;


}
