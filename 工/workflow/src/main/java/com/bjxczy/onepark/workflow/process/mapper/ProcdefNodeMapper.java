package com.bjxczy.onepark.workflow.process.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workflow.process.entity.ProcdefNodePO;

@Mapper
public interface ProcdefNodeMapper extends BaseMapper<ProcdefNodePO> {

}
