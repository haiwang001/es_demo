package com.bjxczy.onepark.workflow.process.core.command;

import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.Process;
import org.activiti.engine.impl.history.HistoryManager;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntityManager;
import org.activiti.engine.impl.util.ProcessDefinitionUtil;

import lombok.Data;

@Data
public class RejectedCommand implements Command<Object> {
	
	private String taskId;
	
	private String targetTaskKey;
	
	public RejectedCommand(String taskId, String targetTaskKey) {
		super();
		this.taskId = taskId;
		this.targetTaskKey = targetTaskKey;
	}

	@Override
	public Object execute(CommandContext commandContext) {
		// 获取当前任务实例
		TaskEntityManager taskEntityManger = commandContext.getTaskEntityManager();
		TaskEntity currentTask = taskEntityManger.findById(taskId);
		// 获取节点执行实例
		ExecutionEntity execution = currentTask.getExecution();
		// 获取目标节点
		String processDefinitionId = execution.getProcessDefinitionId();
		Process process = ProcessDefinitionUtil.getProcess(processDefinitionId);
		FlowElement flowElement = process.getFlowElement(targetTaskKey);
		// 写入历史
		HistoryManager historyManager = commandContext.getHistoryManager();
		historyManager.recordActivityEnd(execution, "jump to" + flowElement.getName());
		historyManager.recordTaskEnd(taskId, "jump to " + flowElement.getName());
		// 删除当前执行任务
		taskEntityManger.delete(taskId);
		execution.setCurrentFlowElement(flowElement);
		commandContext.getAgenda().planContinueMultiInstanceOperation(execution);
		return null;
	}

}
