package com.bjxczy.onepark.workorder.interfaces.rpc.feign;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.core.feign.client.workflow.BaseWorkFlowFeignClient;
import com.bjxczy.onepark.common.model.workflow.WoOrderdefInfo;
import com.bjxczy.onepark.workorder.mapper.WoOrderdefMapper;
import com.bjxczy.onepark.workorder.pojo.po.WoOrderdefPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName WorkFlowFeignController
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/9 14:15
 * @Version 1.0
 */
@Controller
@ResponseBody
public class WorkFlowFeignController implements BaseWorkFlowFeignClient {

    @Autowired
    private WoOrderdefMapper woOrderdefMapper;

    @Override
    public WoOrderdefInfo findById(String id) {
        WoOrderdefPO woOrderdefPO = woOrderdefMapper.selectById(id);
        if (null == woOrderdefPO) {
            return null;
        }
        WoOrderdefInfo woOrderdefInfo = new WoOrderdefInfo();
        BeanUtils.copyProperties(woOrderdefPO, woOrderdefInfo);
        return woOrderdefInfo;
    }

    @Override
    public List<WoOrderdefInfo> getWorkOrderDefByBusinessType(String businessType) {
        List<WoOrderdefPO> woOrderdefPOS = woOrderdefMapper.selectList(Wrappers.<WoOrderdefPO>lambdaQuery()
                .eq(WoOrderdefPO::getBusinessType, businessType));
        ArrayList<WoOrderdefInfo> woOrderdefInfos = new ArrayList<>();
        woOrderdefPOS.forEach(woOrderdefPO -> {
            WoOrderdefInfo woOrderdefInfo = new WoOrderdefInfo();
            BeanUtils.copyProperties(woOrderdefPO,woOrderdefInfo);
            woOrderdefInfos.add(woOrderdefInfo);
        });
        return woOrderdefInfos;
    }
}
