package com.bjxczy.onepark.workflow.form.pojo.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.Data;

@Data
public class QueryFormPageDTO {
	
	private Integer pageNum = 1;
	
	private Integer pageSize = 10;
	
	private String name;
	
	public <T> Page<T> toPage() {
		return new Page<>(this.pageNum, this.pageSize);
	}

}
