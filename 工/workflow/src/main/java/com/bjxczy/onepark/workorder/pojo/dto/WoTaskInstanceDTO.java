package com.bjxczy.onepark.workorder.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.bjxczy.onepark.workorder.pojo.po.WoEodStaffPO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskInstancePO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskVariablesPO;
import com.google.gson.JsonObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 任务受理实例表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-04-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "WoTaskInstanceDTO", description = "任务受理实例表")
public class WoTaskInstanceDTO extends WoTaskInstancePO {

    private List<WoEodStaffPO> woEodStaffs;

    //表单
    private Map<String,Object> formExtra;

    private String triggerId;
}
