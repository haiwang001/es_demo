package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 部门员工班组表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("wo_eod_group")
@ApiModel(value="WoEodGroupPO", description="部门员工班组表")
public class WoEodGroupPO extends UserOperator {

    @ApiModelProperty(value = "班组Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "部门Id")
    private Integer deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "班组名称")
    private String name;

    @ApiModelProperty(value = "值班标题")
    private String eodName;

    @ApiModelProperty(value = "值班描述")
    private String eodDescribe;

    @ApiModelProperty(value = "区域Id")
    private Integer areaId;

    @ApiModelProperty(value = "局址Id")
    private String tenantId;

    @ApiModelProperty(value = "空间Id")
    private String spaceId;

    @ApiModelProperty(value = "值班区域")
    private String eodArea;

    @ApiModelProperty(value = "班组状态")
    private Integer status;
}
