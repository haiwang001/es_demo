package com.bjxczy.onepark.workorder.interfaces.listener;

import com.bjxczy.core.redisson.constant.DelayedQueueNames;
import com.bjxczy.core.redisson.delayed.DelayedQueueImpl;
import com.bjxczy.core.redisson.delayed.IDelayedQueue;
import com.bjxczy.core.redisson.delayed.RDelayedPayload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @ClassName WoTaskDelayedCreateTaskQueueImpl
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/11 11:39
 * @Version 1.0
 */
@Component(DelayedQueueNames.WORKFLOW_ORDER_CREATE_TASK_QUEUE)
@Slf4j
public class WoTaskDelayedCreateTaskQueueImpl extends DelayedQueueImpl implements IDelayedQueue {
    @Override
    public void execute(RDelayedPayload payload) {

    }
}
