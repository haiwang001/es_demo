package com.bjxczy.onepark.workflow.process.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RejectTaskDTO {
	
	private String approvalText;
	
	private String rejectType; 
	
	private String targetId;

	public RejectTaskDTO(String rejectType, String approvalText) {
		super();
		this.rejectType = rejectType;
		this.approvalText = approvalText;
	}

}
