package com.bjxczy.onepark.workflow.process.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;

@Data
public class BaseBpmnElement {
	
	@TableId(type = IdType.ASSIGN_UUID)
	private String id;
	
	private String tagId;
	
	private String versionId;
	
	private String name;
	
	private String documentation;
	
	private String tagName;

}
