package com.bjxczy.onepark.workorder.event;

import com.bjxczy.onepark.common.model.organization.StaffInformation;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @ClassName UpdateStaffEvent
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/29 15:42
 * @Version 1.0
 */
@Getter
public class UpdateStaffEvent extends ApplicationEvent {

    private StaffInformation staffInformation;

    public UpdateStaffEvent(Object source) {
        super(source);
        this.staffInformation=(StaffInformation) source;
    }
}
