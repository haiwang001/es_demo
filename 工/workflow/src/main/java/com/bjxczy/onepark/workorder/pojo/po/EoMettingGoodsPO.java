package com.bjxczy.onepark.workorder.pojo.po;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 会议-物品表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="EoMettingGoods对象", description="会议-物品表")
@TableName("eo_metting_goods")
public class EoMettingGoodsPO {

    @ExcelIgnore
    @ApiModelProperty(value = "主键Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ExcelIgnore
    @ApiModelProperty(value = "会议Id")
    private Integer mettingId;

    @ApiModelProperty(value = "物品名称")
    private String goName;

    @ApiModelProperty(value = "物品数量")
    private Integer goCount;
}
