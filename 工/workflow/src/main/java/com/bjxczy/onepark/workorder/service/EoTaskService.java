package com.bjxczy.onepark.workorder.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workorder.pojo.dto.*;
import com.bjxczy.onepark.workorder.pojo.po.EoMettingGoodsPO;
import com.bjxczy.onepark.workorder.pojo.vo.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface EoTaskService {
    void addEoTask(EoTaskDTO eoTaskDTO, UserInformation user);

    IPage<EoTaskVO> getEoTaskByPage(EoTaskDTO eoTaskDTO);

    void editEoTask(EoTaskDTO eoTaskDTO);

    void checkEoTask(EoTaskDTO eoTaskDTO);

    void delEoTask(String id);

    EoTaskVO getCateringInfo(String eoId);

    void addMettingGoods(EoMettingGoodsDTO eoMettingGoodsDTO);

    void addVehicleInfo(EoVehicleInfoDTO eoVehicleInfoDTO,UserInformation user);

    void addVisits(EoVisitInfoDTO eoVisitInfoDTO,UserInformation userInformation);

    void delMettingGoodsById(Integer id);

    void delVehicleById(Integer id);

    void delVisitById(Integer id);

    void downloadVisitTemplate(HttpServletResponse response);

    void downloadVehicleTemplate(HttpServletResponse response);

    void downloadMettingGoodsTemplate(HttpServletResponse response);

    void importVisitData(MultipartFile file,String eoId,UserInformation user);

    void importVehicleData(MultipartFile file,Integer vehicleId,UserInformation user);


    IPage<EoVisitInfoVO> getVisitInfoByPage(EoVisitInfoDTO eoVisitInfoDTO);

    IPage<EoVehicleInfoVO> getVehicleInfoByPage(EoVehicleInfoDTO eoVehicleInfoDTO);

    IPage<EoMettingGoodsPO> getMettingGoodsInfoByMetId(EoMettingGoodsDTO eoMettingGoodsDTO);

    void importMettingGoodsData(MultipartFile file, Integer mettingId);

    void execute(EoExecuteDTO eoExecuteDTO);

    void settlement(EoSettlementDTO eoSettlementDTO);

    EoSettlementVO getSettlementInfo(String eoId);
}
