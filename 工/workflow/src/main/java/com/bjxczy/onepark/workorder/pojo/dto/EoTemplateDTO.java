package com.bjxczy.onepark.workorder.pojo.dto;

import lombok.Data;

/**
 * @ClassName EoTemplateDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/16 14:01
 * @Version 1.0
 */
@Data
public class EoTemplateDTO extends BasePageDTO{

    private String id;

    private String name;

    private String catering;

    private String remark;
}
