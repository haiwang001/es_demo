package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoMettingGoodsPO;
import com.bjxczy.onepark.workorder.pojo.po.EoMettingPO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EoMettingVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 15:41
 * @Version 1.0
 */
@Data
public class EoMettingVO extends EoMettingPO {

    private List<EoMettingGoodsPO> eoMettingGoodsPOS;

}
