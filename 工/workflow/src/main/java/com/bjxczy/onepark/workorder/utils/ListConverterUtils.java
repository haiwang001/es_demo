package com.bjxczy.onepark.workorder.utils;

import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @ClassName ListVonverterUtils
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/29 18:00
 * @Version 1.0
 */
public class ListConverterUtils<T, F> {
    private final Class<F> fClazz;

    /**
     * 构造方法，传入目标List的Class对象
     *
     * @param fClazz
     */
    public ListConverterUtils(Class<F> fClazz) {
        this.fClazz = fClazz;
    }

    /**
     * 将List<T>转换为List<F>
     *
     * @param pList
     * @return
     */
    public List<F> converterList(List<T> pList) {
        if (pList != null && pList.size() != 0) {
            List<F> dList = new ArrayList<>();
            try {
                Iterator var3 = pList.iterator();
                while (var3.hasNext()) {
                    T t = (T) var3.next();
                    F f = this.fClazz.newInstance();
                    BeanUtils.copyProperties(t, f);
                    dList.add(f);
                }
            } catch (Exception var6) {
                var6.printStackTrace();
            }

            return dList;
        } else {
            return new ArrayList<>();
        }
    }
}

