package com.bjxczy.onepark.workorder.pojo.po;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.*;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName WoComplain
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/27 14:52
 * @Version 1.0
 */
@Data
public class WoComplainExcelPO {

    @ColumnWidth(35)
    @ExcelProperty("投诉编号")
    private String sequenceNo;

    @ExcelProperty("投诉部门")
    private String deptName;

    @ExcelProperty("投诉人")
    private String initiatorName;

    @ExcelProperty("投诉标题")
    private String title;

    @ExcelProperty("投诉内容")
    private String content;

    @ExcelProperty("反馈内容")
    private String feedback;

    @ExcelProperty("投诉状态")
    private String status;

    @ColumnWidth(18)
    @ExcelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ColumnWidth(18)
    @ExcelProperty("反馈时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ExcelProperty("反馈人")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String operator;


}
