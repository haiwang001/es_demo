package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workorder.pojo.po.EoCateringSchedulePO;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 餐饮时刻表 Mapper 接口
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Mapper
public interface EoCateringScheduleMapper extends BaseMapper<EoCateringSchedulePO> {

}
