package com.bjxczy.onepark.workorder.pojo.dto;

import com.bjxczy.onepark.workorder.pojo.po.EoVehicleInfoPO;
import com.bjxczy.onepark.workorder.pojo.po.EoVehiclePO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EoVehicleDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 14:45
 * @Version 1.0
 */
@Data
public class EoVehicleDTO extends EoVehiclePO {
    private List<EoVehicleInfoPO> eoVehicleInfoPOS;
}
