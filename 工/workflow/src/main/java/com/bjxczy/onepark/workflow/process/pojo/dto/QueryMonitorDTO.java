package com.bjxczy.onepark.workflow.process.pojo.dto;

import com.bjxczy.onepark.workflow.common.dto.BasePageDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryMonitorDTO extends BasePageDTO {
	
	private String key;
	
	private String unifyParams;
	
	private Integer status;

}
