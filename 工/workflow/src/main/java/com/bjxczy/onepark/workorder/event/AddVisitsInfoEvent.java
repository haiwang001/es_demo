package com.bjxczy.onepark.workorder.event;

import com.bjxczy.onepark.common.model.workflow.AddVehicleInfo;
import com.bjxczy.onepark.common.model.workflow.AddVisitsInfo;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @ClassName AddVehicleInfoEvent
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/26 17:17
 * @Version 1.0
 */
@Getter
public class AddVisitsInfoEvent extends ApplicationEvent {


    private final AddVisitsInfo addVisitsInfo;

    public AddVisitsInfoEvent(Object source) {
        super(source);
        this.addVisitsInfo = (AddVisitsInfo) source;
    }
}
