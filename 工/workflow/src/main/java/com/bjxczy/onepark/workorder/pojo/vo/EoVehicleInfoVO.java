package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoVehicleInfoPO;
import lombok.Data;

/**
 * @ClassName EoVehicleInfoVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/25 14:26
 * @Version 1.0
 */
@Data
public class EoVehicleInfoVO extends EoVehicleInfoPO {
}
