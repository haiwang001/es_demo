package com.bjxczy.onepark.workflow.form.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workflow.form.entity.FormItemPO;

@Mapper
public interface FormItemMapper extends BaseMapper<FormItemPO> {

	List<FormItemPO> igoreDelListByFormId(String formId);

	@Delete("delete from wf_form_item where form_id = #{formId}")
	void deleteByFormId(@Param("formId") String formId);

}
