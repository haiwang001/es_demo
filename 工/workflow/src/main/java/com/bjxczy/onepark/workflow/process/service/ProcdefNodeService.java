package com.bjxczy.onepark.workflow.process.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.workflow.process.entity.ProcdefNodePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;

public interface ProcdefNodeService extends IService<ProcdefNodePO> {

	void saveByVersion(ProcdefVersionPO po, List<ProcdefNodePO> nodes);

	List<ProcdefNodePO> listByVersionId(String actVersion);

	ProcdefNodePO getByTagId(String versionId, String taskDefinitionKey);

}
