package com.bjxczy.onepark.workorder.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.workorder.mapper.EoTemplateMapper;
import com.bjxczy.onepark.workorder.pojo.dto.EoTemplateDTO;
import com.bjxczy.onepark.workorder.pojo.po.EoTemplatePO;
import com.bjxczy.onepark.workorder.pojo.vo.EoTemplateVO;
import com.bjxczy.onepark.workorder.service.EoTemplateService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName EoTemplateServiceImpl
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/16 13:56
 * @Version 1.0
 */
@Service
public class EoTemplateServiceImpl extends ServiceImpl<EoTemplateMapper, EoTemplatePO> implements EoTemplateService {
    @Override
    public void addEoTemplate(EoTemplateDTO eoTemplateDTO) {
        EoTemplatePO eoTemplateInfo = this.baseMapper.selectOne(Wrappers.<EoTemplatePO>lambdaQuery()
                .eq(EoTemplatePO::getName, eoTemplateDTO.getName()));
        if (null!=eoTemplateInfo){
            throw new ResultException("该模板名称已存在！");
        }
        EoTemplatePO eoTemplatePO = new EoTemplatePO();
        BeanUtils.copyProperties(eoTemplateDTO,eoTemplatePO);
        this.baseMapper.insert(eoTemplatePO);
    }

    @Override
    public void editEoTemplate(EoTemplateDTO eoTemplateDTO) {
        EoTemplatePO eoTemplateInfo = this.baseMapper.selectOne(Wrappers.<EoTemplatePO>lambdaQuery()
                .eq(EoTemplatePO::getName, eoTemplateDTO.getName()));
        if (null!=eoTemplateInfo && !eoTemplateDTO.getId().equals(eoTemplateInfo.getId())){
            throw new ResultException("该模板名称已存在！");
        }
        EoTemplatePO eoTemplatePO = new EoTemplatePO();
        BeanUtils.copyProperties(eoTemplateDTO,eoTemplatePO);
        this.baseMapper.updateById(eoTemplatePO);
    }

    @Override
    public void delEoTemplate(String id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    public IPage<EoTemplateVO> getEoTemplates(EoTemplateDTO eoTemplateDTO) {
        Page<EoTemplatePO> data = this.page(eoTemplateDTO.toPage(), Wrappers.<EoTemplatePO>lambdaQuery()
                .like(StrUtil.isNotBlank(eoTemplateDTO.getName()), EoTemplatePO::getName, eoTemplateDTO.getName())
                .orderByDesc(EoTemplatePO::getCreateTime));
        return data.convert(eoTemplatePO -> {
            EoTemplateVO eoTemplateVO = new EoTemplateVO();
            BeanUtils.copyProperties(eoTemplatePO,eoTemplateVO);
            return eoTemplateVO;
        });
    }

    @Override
    public List<EoTemplateVO> getEoTemplatesPullList() {
        ArrayList<EoTemplateVO> resList = new ArrayList<>();
        this.baseMapper.selectList(Wrappers.emptyWrapper()).forEach(eoTemplatePO -> {
            EoTemplateVO eoTemplateVO = new EoTemplateVO();
            BeanUtils.copyProperties(eoTemplatePO,eoTemplateVO);
            resList.add(eoTemplateVO);
        });
        return resList;
    }
}
