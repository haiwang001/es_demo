package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoSettlementPO;
import lombok.Data;

/**
 * @ClassName EoSettlementVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/26 15:26
 * @Version 1.0
 */
@Data
public class EoSettlementVO extends EoSettlementPO {
}
