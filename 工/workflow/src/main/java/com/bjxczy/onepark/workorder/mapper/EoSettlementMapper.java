package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workorder.pojo.po.EoSettlementPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EoSettlementMapper extends BaseMapper<EoSettlementPO> {
}
