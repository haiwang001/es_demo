package com.bjxczy.onepark.workorder.constant;

import com.bjxczy.onepark.workorder.utils.DownloadFileUtils;

import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName EoOrderConstants
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/23 17:16
 * @Version 1.0
 */
public interface EoOrderConstants {
    /**
     * 酒店需求工单		c23718eaa097b92a807dc6773baf6a02
     * 会议需求工单		83916a800e73962d71cf57b0acb11e1e
     * 餐饮需求工单		df4aa78154c1755fe4daf003d5ce26c5
     * 访客需求工单		7d212bbf195a84680895336b5d2d2e2f
     * 财务需求工单		2db9cbcafab799713f2d081f5026a0ab
     */
    String HOTEL_ORDER="c23718eaa097b92a807dc6773baf6a02";

    String METTING_ORDER="83916a800e73962d71cf57b0acb11e1e";

    String CATERING_ORDER="df4aa78154c1755fe4daf003d5ce26c5";

    String VISITOR_ORDER="7d212bbf195a84680895336b5d2d2e2f";

    String FINANCE_ORDER = "2db9cbcafab799713f2d081f5026a0ab";

    String TEMPLATE_VISIT="/templates/visitTemplate.xlsx";

    String TEMPLATE_VEHICLE="/templates/vehicleTemplate.xlsx";

    String TEMPLATE_METTING_GOODS="/templates/mettingGoodsTemplate.xlsx";

    /**
     * 默认消息事件编码
     */
    String DEFAULT_EVENT_CODE = "EO";

    /**
     * 餐饮需求预定
     */
    String EVENT_CATERING_SUFFIX = "_Catering";
    /**
     * 酒店需求预定
     */
    String EVENT_HOTEL_SUFFIX = "_Hotel";
    /**
     * 会议需求预定
     */
    String EVENT_METTING_SUFFIX = "_Metting";
    /**
     * 来访需求预定
     */
    String EVENT_VISIT_SUFFIX = "_Visit";
    /**
     * EO单预定（财务）
     */
    String EVENT_FINANCE_SUFFIX = "_Finance";
    /**
     * EO单消息（驳回）
     */
    String EVENT_REJECT_SUFFIX = "_Reject";
    /**
     * EO单待办
     */
    String EVENT_EXECUTE_SUFFIX="_Execute";

}

