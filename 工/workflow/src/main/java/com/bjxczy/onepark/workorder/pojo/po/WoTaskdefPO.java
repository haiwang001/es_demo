package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Set;

/**
 * <p>
 * 任务定义表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "wo_taskdef",autoResultMap = true)
@ApiModel(value="WoTaskdef对象", description="任务定义表")
public class WoTaskdefPO extends UserOperator {

    @ApiModelProperty(value = "任务ID（自增）")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "工单名称")
    private String name;

    @ApiModelProperty(value = "工单配置ID")
    private String woId;

    @ApiModelProperty(value = "工单编号")
    private String sequenceNo;

    @ApiModelProperty(value = "工单类型")
    private String orderType;

    @ApiModelProperty(value = "业务模块")
    private String businessType;

    @ApiModelProperty(value = "紧急程度")
    private String orderLevel;

    @ApiModelProperty(value = "接单类型")
    private String taskType;

    @ApiModelProperty(value = "部门ID")
    private Integer assignDeptId;

    @ApiModelProperty(value = "部门名称")
    private String assignDeptName;

    @ApiModelProperty(value = "工单详述")
    private String description;

    @ApiModelProperty(value = "重复类型")
    private String repeatType;


    @ApiModelProperty(value = "过期时间（分钟）")
    private Integer expireTime;

    @ApiModelProperty(value = "发起员工ID")
    private Integer initiatorId;

    @ApiModelProperty(value = "发起员工姓名")
    private String initiatorName;

    @ApiModelProperty(value = "区域Id")
    private Integer areaId;

    @ApiModelProperty(value = "局址Id")
    private String tenantId;

    @ApiModelProperty(value = "空间Id")
    private String spaceId;

    @ApiModelProperty(value = "位置")
    private String position;

    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    @ApiModelProperty(value = "报修设备编码")
    private String deviceCode;

    @ApiModelProperty(value = "报修设备类型")
    private String category;

    @ApiModelProperty(value = "报修备注")
    private String remark;

    @ApiModelProperty(value = "报修图片")
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private Set<String> repairPicture;

    @ApiModelProperty("EO单id")
    private String eoId;

    @ApiModelProperty("会议Id")
    private Integer mId;
}

