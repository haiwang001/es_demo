package com.bjxczy.onepark.workflow.process.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.workflow.process.entity.ProcdefNodePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;
import com.bjxczy.onepark.workflow.process.mapper.ProcdefNodeMapper;
import com.bjxczy.onepark.workflow.process.service.ProcdefNodeService;

@Service
public class ProcdefNodeServiceImpl extends ServiceImpl<ProcdefNodeMapper, ProcdefNodePO>
		implements ProcdefNodeService {

	@Override
	public void saveByVersion(ProcdefVersionPO version, List<ProcdefNodePO> nodes) {
		lambdaUpdate().eq(ProcdefNodePO::getVersionId, version.getId()).remove();
		nodes.forEach(e -> e.setVersionId(version.getId()));
		this.saveBatch(nodes);
	}

	@Override
	public List<ProcdefNodePO> listByVersionId(String versionId) {
		return lambdaQuery().eq(ProcdefNodePO::getVersionId, versionId).list();
	}

	@Override
	public ProcdefNodePO getByTagId(String versionId, String tagId) {
		return lambdaQuery().eq(ProcdefNodePO::getVersionId, versionId).eq(ProcdefNodePO::getTagId, tagId).one();
	}

}
