package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.WoEodStaffSchedulePO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WoEodStaffScheduleVO extends WoEodStaffSchedulePO {

    private String eodName;

    @ApiModelProperty("值班中、休息中")
    private String dutyStatus;

    private String photo;
}
