package com.bjxczy.onepark.workflow.process.pojo.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateProcessDTO {
	
	private String name;
	
	private String remark;
	
	private String icon;
	
	private Integer applyRange;
	
	private List<Integer> staffIds;

}
