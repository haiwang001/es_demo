package com.bjxczy.onepark.workorder.pojo.po;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.*;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 部门员工班组表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="WoEodGroupExcelPO", description="部门员工班组Excel导出PO")
public class WoEodGroupExcelPO {

    @ExcelProperty("部门名称")
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ExcelProperty("班组名称")
    @ApiModelProperty(value = "班组名称")
    private String name;

    @ExcelProperty("值班标题")
    @ApiModelProperty(value = "值班标题")
    private String eodName;

    @ExcelProperty("值班详述")
    @ApiModelProperty(value = "值班描述")
    private String eodDescribe;

    @ColumnWidth(45)
    @ExcelProperty("值班区域")
    @ApiModelProperty(value = "值班区域")
    private String eodArea;

    @ExcelProperty("班组状态")
    @ApiModelProperty(value = "班组状态")
    private String status;

    @ColumnWidth(18)
    @ExcelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
