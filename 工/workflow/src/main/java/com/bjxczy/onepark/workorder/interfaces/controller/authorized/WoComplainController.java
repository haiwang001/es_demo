package com.bjxczy.onepark.workorder.interfaces.controller.authorized;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workorder.pojo.dto.WoComplainDTO;
import com.bjxczy.onepark.workorder.pojo.vo.WoComplainVO;
import com.bjxczy.onepark.workorder.service.WoComplainService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @ClassName WoComplainController
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/27 16:17
 * @Version 1.0
 */
@ApiResourceController
@Api(tags = "投诉模块接口")
@RequestMapping("/complain")
public class WoComplainController {
    @Autowired
    private WoComplainService woComplainService;

    @ApiOperation("移动端-发起投诉")
    @PostMapping(value = "/create",name = "移动端-发起投诉")
    public void createWoComplain(@RequestBody WoComplainDTO woComplainDTO,@LoginUser(isFull = true) UserInformation user){
        woComplainService.createWoComplain(woComplainDTO,user);
    }
    @ApiOperation("pc端-投诉列表")
    @GetMapping(value = "/list",name = "投诉列表")
    public Page<WoComplainVO> getComplainList(WoComplainDTO woComplainDTO){
        return woComplainService.getComplainList(woComplainDTO);
    }

    @ApiOperation("移动端-投诉列表")
    @GetMapping(value = "/mob/list",name = "投诉列表")
    public List<WoComplainVO> getComplainMobList(WoComplainDTO woComplainDTO){
        return woComplainService.getComplainMobList(woComplainDTO);
    }

    @ApiOperation("pc端-投诉反馈")
    @PutMapping(value = "/feedback",name = "投诉反馈")
    public void feedBackWoComplain(@RequestBody WoComplainDTO woComplainDTO){
        woComplainService.feedBackWoComplain(woComplainDTO);
    }
    @ApiOperation("pc端-删除投诉")
    @DeleteMapping(value = "/{id}",name = "pc端-删除投诉")
    public void delWoComplain(@PathVariable("id") Integer id){
        woComplainService.delWoComplain(id);
    }

    @ApiOperation("pc端-导出投诉列表数据")
    @PostMapping(value = "/export",name = "pc端-导出投诉列表数据")
    public void export(HttpServletResponse response){
        woComplainService.export(response);
    }

}

