package com.bjxczy.onepark.workflow.process.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.Payload;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMyTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.RejectTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.TaskCompleteDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.CandidateVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.HiTaskVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.TaskVO;

public interface TaskExecService {

	void runProcess(String key, Map<String, Object> variables, UserInformation user);
	
	List<CandidateVO> getCandidate(String key, Integer processId, Map<String, Object> variables, UserInformation user);

	void complete(String taskId, TaskCompleteDTO dto, UserInformation user);

	void reject(String taskId, RejectTaskDTO dto, UserInformation user);

	IPage<TaskVO> listMyTask(QueryMyTaskDTO dto, UserInformation user);

	Payload getVariables(String taskId);

	Payload getVariablesByProcess(Integer processId);

	IPage<HiTaskVO> listMyHiTask(QueryMyTaskDTO dto, UserInformation user);

	void cancel(Integer processId);

}
