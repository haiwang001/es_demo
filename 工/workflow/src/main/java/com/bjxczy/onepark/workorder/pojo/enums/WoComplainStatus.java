package com.bjxczy.onepark.workorder.pojo.enums;

import lombok.Getter;

public enum WoComplainStatus {

    FEEDBACK("1"),NOFEEDBACK("0");

    @Getter
    private String value;

    WoComplainStatus(String value){
        this.value=value;
    }
}
