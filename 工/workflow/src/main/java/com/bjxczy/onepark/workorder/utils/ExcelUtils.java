package com.bjxczy.onepark.workorder.utils;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.alibaba.excel.EasyExcel;

import cn.hutool.core.bean.BeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

/**
 * @ClassName ExcelUtils
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/29 17:14
 * @Version 1.0
 */
@Slf4j
@Component
public class ExcelUtils<T> {

    public void setResponse(HttpServletResponse response, String fileName) {
        try {
            String format = DateTools.format(new Date(), DateTools.FORMAT_yyyyMMddHHmmss);
            fileName = fileName + format + ".xlsx";
            fileName = URLEncoder.encode(fileName, "UTF-8");
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        } catch (Exception e) {
            log.error("文件导出失败：{}", e.getMessage());
        }
    }

    /**
     * list 复制到另外一个DTO，并导出
     *
     * @param response
     * @param fileName
     * @param datas
     * @param t
     */
    public void export(HttpServletResponse response, String fileName, List<Object> datas, Class<T> t) {
        try {
            List<T> list = new ArrayList<>();
            for (Object obj : datas) {
                T t1 = t.newInstance();
                BeanUtil.copyProperties(obj, t1);
                list.add(t1);
            }
            this.setResponse(response, fileName);
            EasyExcel.write(response.getOutputStream(), t).sheet("sheet1")
                    .registerWriteHandler(new CustomColumnWidthHandler())
                    .doWrite(list);
        } catch (Exception e) {
            log.info("{} 文件导出, 异常：{}", fileName, e.getMessage());
        }
    }

    /**
     * 将MultipartFile转换为File
     *
     * @param multiFile
     * @return
     */
    public static File MultipartFileToFile(MultipartFile multiFile) {
        // 获取文件名
        String fileName = multiFile.getOriginalFilename();
        // 获取文件后缀
        String prefix = fileName.substring(fileName.lastIndexOf("."));
        // 若须要防止生成的临时文件重复,能够在文件名后添加随机码

        try {
            File file = File.createTempFile(fileName, prefix);
            multiFile.transferTo(file);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

