package com.bjxczy.onepark.workorder.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workorder.pojo.dto.WoComplainDTO;
import com.bjxczy.onepark.workorder.pojo.vo.WoComplainVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface WoComplainService {
    void createWoComplain(WoComplainDTO woComplainDTO, UserInformation user);

    Page<WoComplainVO> getComplainList(WoComplainDTO woComplainDTO);

    void feedBackWoComplain(WoComplainDTO woComplainDTO);

    void delWoComplain(Integer id);

    void export(HttpServletResponse response);

    List<WoComplainVO> getComplainMobList(WoComplainDTO woComplainDTO);
}
