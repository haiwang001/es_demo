package com.bjxczy.onepark.workflow.common.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.bjxczy.core.web.configration.DefaultWebMvcConfig;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.dict.JdbcDictServiceImpl;

@Configuration
public class WebMvcConfig extends DefaultWebMvcConfig {
	
	@Bean
	public DictService dictService() {
		return new JdbcDictServiceImpl("wf_dict");
	}
	
	@Bean(name = "rpcRestTemplate")
	@LoadBalanced
	public RestTemplate rpcRestTemplate() {
		return new RestTemplate();
	}

}
