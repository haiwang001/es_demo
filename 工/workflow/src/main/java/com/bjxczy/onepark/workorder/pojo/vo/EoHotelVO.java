package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoHotelPO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EoHotelVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 15:40
 * @Version 1.0
 */
@Data
public class EoHotelVO extends EoHotelPO {
    //住宿相关
    private List<EoAccommodationVO> eoAccommodationVOS;
    //会议相关
    private List<EoMettingVO> eoMettingVOS;
    //园区
    private String parkName;


}
