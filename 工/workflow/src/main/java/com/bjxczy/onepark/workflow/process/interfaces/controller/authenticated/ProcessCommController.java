package com.bjxczy.onepark.workflow.process.interfaces.controller.authenticated;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefActiveVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefDetailVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefVersionVO;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;
import com.bjxczy.onepark.workflow.process.service.ProcessVersionService;

@ApiResourceController
@RequestMapping("/process")
public class ProcessCommController {

	@Autowired
	private ProcessDefinitionService processDefinitionService;
	@Autowired
	private ProcessVersionService processVersionService;

	@GetMapping(value = "/def", name = "查询流程列表")
	public List<ProcdefPO> listProcdef(@RequestParam(value = "enable", required = false) Integer enable) {
		return processDefinitionService.listByEnable(enable);
	}

	@GetMapping(value = "/def/{id}", name = "查询流程详情")
	public ProcdefDetailVO getDetail(@PathVariable("id") String id) {
		return processDefinitionService.getDetail(id);
	}

	@GetMapping(value = "/{processId}/version/{versionId}", name = "查询版本详情")
	public ProcdefVersionVO getVersion(@PathVariable("processId") String processId,
			@PathVariable("versionId") String versionId) {
		return processVersionService.getDetail(versionId);
	}

	@GetMapping(value = "/def/{procdefId}/active", name = "查询流程表单")
	public ProcdefActiveVO getProcdefActive(@PathVariable("procdefId") String procdefId) {
		return processDefinitionService.getProcdefActive(procdefId);
	}

}
