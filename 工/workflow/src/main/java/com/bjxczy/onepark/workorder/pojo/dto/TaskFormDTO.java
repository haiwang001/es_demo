package com.bjxczy.onepark.workorder.pojo.dto;

import lombok.Data;

/**
 * @ClassName TaskFormDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/8 11:36
 * @Version 1.0
 */
@Data
public class TaskFormDTO extends BasePageDTO{

    private String formName;

    private String sequenceNo;

    private String orderType;

    private String businessType;

    private String assignDeptName;

    private String orderName;
}
