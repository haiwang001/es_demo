package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.dto.WoComplainDTO;
import com.bjxczy.onepark.workorder.pojo.po.WoComplainPO;
import com.bjxczy.onepark.workorder.pojo.vo.WoComplainVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WoComplainMapper extends BaseMapper<WoComplainPO> {
    Page<WoComplainVO> getComplainList(@Param("toPage") Page<Object> toPage, @Param("woComplainDTO") WoComplainDTO woComplainDTO);

    List<WoComplainVO> getComplainMobList(@Param("woComplainDTO") WoComplainDTO woComplainDTO);
}
