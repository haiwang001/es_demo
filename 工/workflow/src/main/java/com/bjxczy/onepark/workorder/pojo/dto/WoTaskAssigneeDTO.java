package com.bjxczy.onepark.workorder.pojo.dto;

import com.bjxczy.onepark.workorder.pojo.po.WoTaskAssigneePO;
import lombok.Data;

/**
 * @ClassName WoTaskAssigneeDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/4 13:56
 * @Version 1.0
 */
@Data
public class WoTaskAssigneeDTO extends WoTaskAssigneePO {
}
