package com.bjxczy.onepark.workorder.pojo.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.po.EoVisitInfoPO;
import lombok.Data;

/**
 * @ClassName EoVisitInfoDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/24 17:13
 * @Version 1.0
 */
@Data
public class EoVisitInfoDTO extends EoVisitInfoPO {

    @ExcelIgnore
    private Integer pageNum = 1;
    @ExcelIgnore
    private Integer pageSize = 10;

    @ExcelProperty("到访园区")
    private String areaName;

    public <T> Page<T> toPage() {
        return new Page<>(pageNum, pageSize);
    }
}
