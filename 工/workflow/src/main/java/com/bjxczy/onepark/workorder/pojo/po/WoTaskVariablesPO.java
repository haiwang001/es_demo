package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 任务变量表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "wo_task_variables",autoResultMap = true)
@ApiModel(value="WoTaskVariablesPO", description="任务变量表")
public class WoTaskVariablesPO{


    @ApiModelProperty(value = "自增ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "任务ID")
    private String taskInstanceId;

    @ApiModelProperty(value = "字段描述")
    private String label;

//    @TableField(typeHandler = FastjsonTypeHandler.class)
    @ApiModelProperty(value = "表单值")
    private Object value;

    @ApiModelProperty(value = "表单值描述")
    private String valueDesc;

    @ApiModelProperty(value = "额外字段")
    private String extra;

    @ApiModelProperty(value = "字段key")
    private String formKey;

}
