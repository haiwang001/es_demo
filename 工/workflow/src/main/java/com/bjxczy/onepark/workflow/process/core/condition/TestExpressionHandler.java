package com.bjxczy.onepark.workflow.process.core.condition;

import org.springframework.stereotype.Component;

import com.alibaba.excel.util.StringUtils;

@Component
public class TestExpressionHandler extends AbstractExpressionHandler {
	
	public boolean isBlank(String str) {
		return StringUtils.isBlank(str);
	}

}
