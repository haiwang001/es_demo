package com.bjxczy.onepark.workflow.process.interfaces.controller.authenticated;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.privacy.MaskingOperation;
import com.bjxczy.core.web.privacy.ResponseMasking;
import com.bjxczy.core.web.privacy.iterator.IPageMaskingIterator;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.Payload;
import com.bjxczy.onepark.workflow.process.entity.RuProcessPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryRuProcessDTO;
import com.bjxczy.onepark.workflow.process.service.RuProcessService;
import com.bjxczy.onepark.workflow.process.service.TaskExecService;

@ApiResourceController
@RequestMapping("/process/run")
public class RuProcessController {
	
	@Autowired
	private RuProcessService ruProcessService;
	@Autowired
	private TaskExecService taskExecService;
	
	@GetMapping(value = "/page", name = "我的申请")
	@ResponseMasking(operation = MaskingOperation.QUERY, iterator = IPageMaskingIterator.class)
	public IPage<RuProcessPO> listPage(QueryRuProcessDTO dto, @LoginUser(isFull = true) UserInformation user) {
		return ruProcessService.listPage(dto, user);
	}
	
	@GetMapping(value = "/{id}", name = "申请详情")
	public Payload detail(@PathVariable("id") Integer processId) {
		return taskExecService.getVariablesByProcess(processId);
	}
	
	@PutMapping(value = "/{id}", name = "取消申请")
	public void cancel(@PathVariable("id") Integer processId) {
		taskExecService.cancel(processId);
	}

}
