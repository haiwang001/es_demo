package com.bjxczy.onepark.workflow.process.pojo.dto;

import com.bjxczy.onepark.workflow.common.dto.BasePageDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class QueryProcessPageDTO extends BasePageDTO {
	
	private String name;

}
