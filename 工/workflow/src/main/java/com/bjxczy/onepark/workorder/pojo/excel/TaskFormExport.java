package com.bjxczy.onepark.workorder.pojo.excel;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @ClassName TaskFormExport
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/8 14:37
 * @Version 1.0
 */
@Data
public class TaskFormExport {

    @ExcelIgnore
    private String businessType;

    @ExcelIgnore
    private String orderType;

    @ExcelIgnore
    private String id;

    @ExcelProperty("编号")
    private String sequenceNo;

    @ExcelProperty("任务填报表名称")
    private String formName;

    @ExcelProperty("业务模块")
    private String businessTypeName;

    @ExcelProperty("部门名称")
    private String assignDeptName;

    @ExcelProperty("工单名称")
    private String OrderName;

    @ExcelProperty("工单类型")
    private String orderTypeName;

    @ExcelProperty("所含数据")
    private Integer dataSize;
}
