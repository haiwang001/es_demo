package com.bjxczy.onepark.workflow.process.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;
import com.bjxczy.core.web.privacy.handler.EmployeeNoMaskingHandler;
import com.bjxczy.core.web.privacy.handler.MaskingField;
import com.bjxczy.core.web.privacy.handler.MobileMaskingHandler;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("wf_ru_process")
public class RuProcessPO extends UserOperator {
	
	private static final long serialVersionUID = 3538321694620277335L;

	@TableId(type = IdType.AUTO)
	private Integer id;
	
	private Integer staffId;
	
	@MaskingField(typeHandler = EmployeeNoMaskingHandler.class)
	private String employeeNo;
	
	private String staffName;
	
	@MaskingField(typeHandler = MobileMaskingHandler.class)
	private String mobile;
	
	private Integer deptId;
	
	private String deptName;
	
	private Integer rankId;
	
	private String rankName;
	
	private String procDefKey;
	
	private String procDefName;
	
	private String procVersion;
	
	private String procInstId;
	
	private String formId;
	
	private Integer applyStatus;
	
	private String closeMessage;
	
	@TableField(exist = false)
	private String applyStatusName;

}
