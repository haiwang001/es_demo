package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName EoTaskPO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 14:13
 * @Version 1.0
 */
@Data
@TableName("eo_task")
public class EoTaskPO extends UserOperator {
    @ApiModelProperty(value = "Eo")

    private String id;

    @ApiModelProperty(value = "EO单名称")
    private String name;

    @ApiModelProperty(value = "主账账号")
    private String account;

    @ApiModelProperty(value = "开始日期")
    private String startTime;

    @ApiModelProperty(value = "结束日期")
    private String endTime;

    @ApiModelProperty(value = "人数")
    private Integer personCount;

    @ApiModelProperty(value = "联系及签单人 姓名")
    private String signName;

    @ApiModelProperty(value = "联系及签单人 手机号")
    private String signMobile;

    @ApiModelProperty(value = "归属公司及部门")
    private String dept;

    @ApiModelProperty(value = "项目负责人 姓名")
    private String projName;

    @ApiModelProperty(value = "项目负责人 手机号")
    private String projMobile;

    @ApiModelProperty(value = "收费标准（模板Id）")
    private String templateId;

    @ApiModelProperty(value = "住宿")
    private String accommodation;

    @ApiModelProperty(value = "餐饮")
    private String catering;

    @ApiModelProperty(value = "场租")
    private String rent;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "EO单状态（字典：eoTaskStatus）")
    private String status;

    @ApiModelProperty(value = "驳回原因")
    private String rejectReason;

    @ApiModelProperty(value = "种类")
    private String eoType;

    @ApiModelProperty(value = "发起人")
    private Integer initiatorId;


}



