package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.dto.WoTaskDTO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskdefExcelPO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskdefPO;
import com.bjxczy.onepark.workorder.pojo.vo.WoTaskdefVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WoTaskdefMapper extends BaseMapper<WoTaskdefPO> {

    List<WoTaskdefExcelPO> exportList(WoTaskDTO woTaskDTO);

    Page<WoTaskdefVO> defListPage(@Param("toPage") Page<Object> toPage, @Param("dto") WoTaskDTO dto);

}
