package com.bjxczy.onepark.workflow.process.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workflow.process.entity.RuProcessPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMonitorDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMyTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryRuProcessDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.HiTaskVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.TaskVO;

public interface RuProcessService extends IService<RuProcessPO> {

	RuProcessPO saveByInst(String key, UserInformation user);

	IPage<TaskVO> listMyTask(QueryMyTaskDTO dto, UserInformation user);

	RuProcessPO getByInstId(String processInstanceId);

	IPage<RuProcessPO> listPage(QueryRuProcessDTO dto, UserInformation user);
	
	IPage<HiTaskVO> listMyHiTask(QueryMyTaskDTO dto, UserInformation user);

	IPage<RuProcessPO> listMonitorPage(QueryMonitorDTO dto);

}
