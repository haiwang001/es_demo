package com.bjxczy.onepark.workorder.interfaces.listener;

import com.bjxczy.core.rabbitmq.supports.WorkflowBaseListener;
import com.bjxczy.onepark.common.model.workflow.PushOrderPayload;
import com.bjxczy.onepark.workorder.service.WoTaskService;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName WorkFlowListener
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/12 9:40
 * @Version 1.0
 */
@Service
public class WorkFlowListener extends WorkflowBaseListener {

    @Autowired
    private WoTaskService woTaskService;

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmpworkflow_trigger_queue", durable = "true"),  // 持久化队列
                    key = WORKORDER_TRIGGER_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) // 绑定的交换机
    )})
    @Override
    public void pushOrder(PushOrderPayload payload, Channel channel, Message message) {
        this.confirm(()->{
            woTaskService.pushOrder(payload);
            return true;
        },channel,message);
    }
}
