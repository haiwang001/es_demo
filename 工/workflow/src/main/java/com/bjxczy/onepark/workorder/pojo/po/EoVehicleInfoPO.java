package com.bjxczy.onepark.workorder.pojo.po;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 车辆信息表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="EoVehicleInfo对象", description="车辆信息表")
@TableName("eo_vehicle_info")
public class EoVehicleInfoPO {


    @ExcelIgnore
    @ApiModelProperty(value = "主键Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ExcelIgnore
    @ApiModelProperty(value = "车辆EO单Id")
    private Integer vehicleId;

    @ExcelProperty("停车场")
    @ApiModelProperty(value = "停车场")
    private String park;

    @ExcelProperty("车牌号")
    @ApiModelProperty(value = "车牌号")
    private String carNumber;

    @ExcelProperty("姓名")
    @ApiModelProperty(value = "姓名")
    private String name;

    @ExcelProperty("车辆颜色")
    @ApiModelProperty(value = "车辆颜色")
    private String carColor;

    @ExcelProperty("手机号")
    @ApiModelProperty(value = "手机号")
    private String mobile;

}
