package com.bjxczy.onepark.workorder.interfaces.controller.authorized;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.workorder.pojo.dto.WoOrderdefDto;
import com.bjxczy.onepark.workorder.pojo.vo.WoOrderdefVO;
import com.bjxczy.onepark.workorder.service.WoOrderdefService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @ClassName WoOrderController
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/30 11:45
 * @Version 1.0
 */
@ApiResourceController
@Api(tags = "工单配置接口")
@RequestMapping("/order")
public class WoOrderController {

    @Autowired
    private WoOrderdefService woOrderdefService;

    @ApiOperation("pc端-新增工单配置")
    @PostMapping(value = "/add",name = "pc-端新增工单配置")
    public void addWoOrder(@RequestBody WoOrderdefDto woOrderdefDto){
        woOrderdefService.addWoOrder(woOrderdefDto);
    }

    @ApiOperation("pc端-查询工单配置（分页）")
    @GetMapping(value = "/list",name = "pc端-查询工单配置（分页）")
    public Page<WoOrderdefVO> list(WoOrderdefDto woOrderdefDto){
        return woOrderdefService.list(woOrderdefDto);
    }

    @ApiOperation("pc端-工单配置下拉框")
    @GetMapping(value = "/pullList",name = "pc端-工单配置下拉框")
    public List<WoOrderdefVO> pullList(){
        return woOrderdefService.pullList();
    }

    @ApiOperation("移动端-关联工单下拉框")
    @GetMapping(value = "mob/pullList/{id}",name = "pc端-工单配置下拉框")
    public List<Map<String,Object>> pullList(@PathVariable("id")String id){
        return woOrderdefService.mobPullList(id);
    }

    @ApiOperation("pc端-编辑工单配置")
    @PutMapping(value = "/edit",name = "pc端-编辑工单配置")
    public void editWoOrder(@RequestBody WoOrderdefDto woOrderdefDto){
        woOrderdefService.editWoOrder(woOrderdefDto);
    }

    @ApiOperation("pc端-删除工单配置")
    @DeleteMapping(value = "/delByIds",name = "pc端-删除工单配置")
    public void deleteWoOrder(@RequestBody List<String> ids){
        woOrderdefService.deleteWoOrder(ids);
    }

    @ApiOperation("pc端-导出工单配置数据")
    @PostMapping(value = "/export",name = "pc端-导出工单配置数据")
    public void exportWoOrder(HttpServletResponse response,@RequestBody(required = false) WoOrderdefDto woOrderdefDto){
        woOrderdefService.exportWoOrder(response,woOrderdefDto);
    }
}
