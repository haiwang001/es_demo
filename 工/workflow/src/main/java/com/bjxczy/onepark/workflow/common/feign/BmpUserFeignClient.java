package com.bjxczy.onepark.workflow.common.feign;

import org.springframework.cloud.openfeign.FeignClient;

import com.bjxczy.core.feign.client.user.BaseUserFeignClient;

@FeignClient("user-server")
public interface BmpUserFeignClient extends BaseUserFeignClient {

}