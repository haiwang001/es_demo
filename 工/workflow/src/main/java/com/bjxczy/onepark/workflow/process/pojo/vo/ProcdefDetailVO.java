package com.bjxczy.onepark.workflow.process.pojo.vo;

import java.util.List;

import com.bjxczy.onepark.common.model.organization.StaffInformation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ProcdefDetailVO extends ProcdefVO {
	
	private static final long serialVersionUID = -8289015855862934124L;
	
	private List<StaffInformation> allowStaffList;

}
