package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskAssigneePO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface WoTaskAssigneeMapper extends BaseMapper<WoTaskAssigneePO> {

    @Select("select * from wo_task_assignee wta inner join (select max(id) id\n" +
            "from wo_task_assignee\n" +
            "group by inst_id\n" +
            ")wtam on wta.id=wtam.id where assignee_status='3' and assignee_id=#{staffId} order by update_time desc")
    List<WoTaskAssigneePO> selectAssignTransfer(@Param("staffId") Integer staffId);
}

