package com.bjxczy.onepark.workflow.process.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workflow.process.entity.HiTaskPO;

@Mapper
public interface HiTaskMapper extends BaseMapper<HiTaskPO> {

}
