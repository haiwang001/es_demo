package com.bjxczy.onepark.workorder.utils;

import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * @ClassName WoEodUtils
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/28 16:25
 * @Version 1.0
 */
@Component
public class WoEodUtils {
    /**
     * 获取某个月的天数
     *
     * @param date
     * @return
     */
    public int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

}
