package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.WoTaskPO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskdefPO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName WoTaskVo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/31 11:03
 * @Version 1.0
 */
@Data
public class WoTaskVO extends WoTaskPO {

    private WoTaskdefVO woTaskdefVO;

    private List<WoTaskInstanceVO> woTaskInstanceVOList;

    private String taskStatusName;

    private String orderLevelName;

    private String businessTypeName;

    private String orderTypeName;

    private String taskTypeName;

    private String persons;

}
