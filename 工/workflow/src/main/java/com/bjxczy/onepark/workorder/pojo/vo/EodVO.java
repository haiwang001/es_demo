package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.WoEodGroupPO;
import com.bjxczy.onepark.workorder.pojo.po.WoEodStaffPO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EodVo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/27 18:29
 * @Version 1.0
 */
@Data
public class EodVO extends WoEodGroupPO {

    List<WoEodStaffPO> woEodStaffPOList;
}
