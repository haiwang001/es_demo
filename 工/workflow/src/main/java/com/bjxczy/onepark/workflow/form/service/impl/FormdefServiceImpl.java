package com.bjxczy.onepark.workflow.form.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.nacos.common.utils.Objects;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.log.LogContextHolder;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.common.DictInformation;
import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;
import com.bjxczy.onepark.workflow.form.facade.IProcessFacade;
import com.bjxczy.onepark.workflow.form.mapper.FormdefMapper;
import com.bjxczy.onepark.workflow.form.pojo.dto.CreateFormDTO;
import com.bjxczy.onepark.workflow.form.pojo.dto.QueryFormPageDTO;
import com.bjxczy.onepark.workflow.form.pojo.dto.UpdateFormDTO;
import com.bjxczy.onepark.workflow.form.pojo.vo.FormVO;
import com.bjxczy.onepark.workflow.form.service.FormItemService;
import com.bjxczy.onepark.workflow.form.service.FormdefService;
import com.bjxczy.onepark.workflow.process.facade.IFormFacade;

@Service
public class FormdefServiceImpl extends ServiceImpl<FormdefMapper, FormdefPO> implements FormdefService, IFormFacade {

	@Autowired
	private FormItemService formItemService;
	@Autowired
	private DictService dictService;
	@Autowired
	private IProcessFacade processFacade;

	@Override
	public IPage<FormdefPO> pageList(QueryFormPageDTO dto) {
		QueryWrapper<FormdefPO> wrapper = new QueryWrapper<>();
		wrapper.like(StringUtils.isNotBlank(dto.getName()), "name", dto.getName());
		wrapper.orderByDesc("update_time");
		Map<String, DictInformation> valueKeyMap = dictService.valueKeyMap("formType");
		Page<FormdefPO> data = this.page(dto.toPage(), wrapper);
		data.getRecords().forEach(e -> {
			if (e.getFormType() != null) {
				e.setFormTypeName(valueKeyMap.get(e.getFormType().toString()).getLabel());
			}
		});
		return data;
	}

	@Override
	public void create(CreateFormDTO dto) {
		this.save(new FormdefPO(dto.getName(), dto.getRemark()));
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void update(String id, UpdateFormDTO dto) {
		FormdefPO entity = this.getById(id);
		if (entity != null) {
			entity.setName(dto.getName());
			entity.setRemark(dto.getRemark());
			entity.setExtra(dto.getExtra());
			this.updateById(entity);
			this.formItemService.saveByForm(entity, dto.getFormItem());
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void removeForm(String id) {
		if (processFacade.isBandingProcess(id))  {
			throw new ResultException("表单已绑定流程，无法删除！");
		}
		FormdefPO po = this.getById(id);
		if (po != null) {
			LogContextHolder.put("name", po.getName());
			// 删除绑定表单
			this.formItemService.removeByForm(po);
			this.removeById(id);
		}
	}

	@Override
	public FormVO findVOById(String id) {
		FormdefPO po = this.getById(id);
		if (po != null) {
			FormVO vo = new FormVO();
			BeanUtils.copyProperties(po, vo);
			vo.setFormItem(this.formItemService.listByFormId(po.getId()));
			return vo;
		}
		return null;
	}

	@Override
	public void updateStatus(String id, Integer enable) {
		FormdefPO po = this.getById(id);
		if (po != null) {
			LogContextHolder.put("name", po.getName());
			po.setEnable(enable);
			this.updateById(po);
		}
	}

	@Override
	public List<FormdefPO> findByEnable(Integer enable) {
		return lambdaQuery().eq(Objects.nonNull(enable), FormdefPO::getEnable, enable).list();
	}

	@Override
	public List<FormItemPO> listByFormId(String formId) {
		return formItemService.igoreDelListByFormId(formId);
	}

	@Override
	public FormdefPO getFormdefById(String formId) {
		return baseMapper.igoreDelGetById(formId);
	}

}
