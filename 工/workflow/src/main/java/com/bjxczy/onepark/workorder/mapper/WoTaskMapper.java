package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.dto.TaskFormDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoTaskDTO;
import com.bjxczy.onepark.workorder.pojo.excel.TaskFormExport;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskPO;
import com.bjxczy.onepark.workorder.pojo.vo.TaskFormVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoTaskVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface WoTaskMapper extends BaseMapper<WoTaskPO> {
    Page<WoTaskVO> excuteListPage(@Param("toPage") Page<Object> toPage, @Param("dto") WoTaskDTO woTaskDTO);

    @Select("select distinct(assign_dept_name) from wo_task where assign_dept_id=#{assignDeptId};")
    String selDeptName(@Param("assignDeptId") Integer assignDeptId);

    List<Map<String,Object>> selFormExtra(@Param("formId") String formId);

    Page<TaskFormVO> getTaskFormByPage(@Param("toPage") Page<Object> toPage, @Param("dto") TaskFormDTO taskFormDTO);

    List<TaskFormExport> getFormExportList(@Param("dto") TaskFormDTO taskFormDTO);

    @Select("select wo_id as woId from wo_task where id =(select task_exec_id from wo_task_instance where id=#{instanceId});")
    String selectWoIdByInstanceId(@Param("instanceId") String instanceId);
}
