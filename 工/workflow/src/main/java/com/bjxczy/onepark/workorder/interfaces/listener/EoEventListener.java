package com.bjxczy.onepark.workorder.interfaces.listener;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.core.rabbitmq.supports.WorkflowBaseListener;
import com.bjxczy.onepark.workorder.event.AddEoMettingInfoEvent;
import com.bjxczy.onepark.workorder.event.AddVehicleInfoEvent;
import com.bjxczy.onepark.workorder.event.AddVisitsInfoEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @ClassName StaffEventListener
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/29 15:45
 * @Version 1.0
 */
@Component
@Slf4j
public class EoEventListener extends WorkflowBaseListener {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @EventListener
    public void AddEoMettingInfoListener(AddEoMettingInfoEvent event){
        log.info("[workorder] AddEoMettingInfoEvent: {}", event);
        log.info("==============================================================================");
        log.info("===========================EO单新增会议信息：联动会议管理===============================");
        log.info("===============================================================================");
        //同步
        rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME,EOORDER_ADD_METTING_INFO_EVENT,event.getAddEoMettingInfo());

    }
    @EventListener
    public void AddVehicleInfoListener(AddVehicleInfoEvent event){
        log.info("[workorder] AddVehicleInfoEvent: {}", event);
        log.info("==============================================================================");
        log.info("===========================EO单新增车辆信息：联动园区通行===============================");
        log.info("===============================================================================");
        //同步
        rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME,EOORDER_ADD_VEHICLE_INFO_EVENT,event.getAddVehicleInfo());

    }
    @EventListener
    public void AddVisitsInfoListener(AddVisitsInfoEvent event){
        log.info("[workorder] AddVisitsInfoEvent: {}", event);
        log.info("==============================================================================");
        log.info("===========================EO单新增访客信息：联动访客模块===============================");
        log.info("===============================================================================");
        //同步
        rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME,EOORDER_ADD_VISITS_INFO_EVENT,event.getAddVisitsInfo());

    }
}
