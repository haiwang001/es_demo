package com.bjxczy.onepark.workflow.form.interfaces.controller.authenticated;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;
import com.bjxczy.onepark.workflow.form.pojo.vo.FormVO;
import com.bjxczy.onepark.workflow.form.service.FormdefService;

@ApiResourceController
@RequestMapping("/form")
public class FormCommController {
	
	@Autowired
	private FormdefService formdefService;
	
	@GetMapping(value = "/{id}", name = "查询表单详情")
	public FormVO queryFormById(@PathVariable("id") String id) {
		return formdefService.findVOById(id);
	}
	
	@GetMapping(name = "查询表单")
	public List<FormdefPO> listFrom(@RequestParam(value = "enable", required = false) Integer enable) {
		return formdefService.findByEnable(enable);
	}

}
