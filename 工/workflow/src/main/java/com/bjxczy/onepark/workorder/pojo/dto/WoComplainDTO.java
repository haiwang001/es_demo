package com.bjxczy.onepark.workorder.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName WoComplainDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/27 16:30
 * @Version 1.0
 */
@Data
public class WoComplainDTO extends BasePageDTO{

    private Integer id;

    @ApiModelProperty(value = "投诉部门Id")
    private Integer deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "投诉标题")
    private String title;

    @ApiModelProperty(value = "投诉内容")
    private String content;

    @ApiModelProperty(value = "投诉状态")
    private String status;

    @ApiModelProperty(value = "反馈内容")
    private String feedback;

}
