package com.bjxczy.onepark.workorder.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.workorder.pojo.dto.EoTemplateDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoEodDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoEodStaffScheduleDTO;
import com.bjxczy.onepark.workorder.pojo.vo.EoTemplateVO;
import com.bjxczy.onepark.workorder.pojo.vo.EodVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoEodStaffScheduleVO;
import org.springframework.data.domain.Page;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface EoTemplateService {
    void addEoTemplate(EoTemplateDTO eoTemplateDTO);

    void editEoTemplate(EoTemplateDTO eoTemplateDTO);

    void delEoTemplate(String id);

    IPage<EoTemplateVO> getEoTemplates(EoTemplateDTO eoTemplateDTO);

    List<EoTemplateVO> getEoTemplatesPullList();
}
