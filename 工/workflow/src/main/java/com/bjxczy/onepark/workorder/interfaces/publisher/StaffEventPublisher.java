package com.bjxczy.onepark.workorder.interfaces.publisher;

import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.workorder.event.RemoveStaffEvent;
import com.bjxczy.onepark.workorder.event.UpdateStaffEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * @ClassName StaffEventPublisher
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/29 15:33
 * @Version 1.0
 */
@Component
public class StaffEventPublisher {

    @Autowired
    private ApplicationEventPublisher publisher;

    public void removeStaffEvent(RemoveStaffInfo info) {
        publisher.publishEvent(new RemoveStaffEvent(info.getId()));
    }

    public void updateStaffEvent(StaffInformation info) {
        publisher.publishEvent(new UpdateStaffEvent(info));
    }
}
