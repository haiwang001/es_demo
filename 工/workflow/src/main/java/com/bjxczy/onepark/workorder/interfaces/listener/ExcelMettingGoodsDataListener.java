package com.bjxczy.onepark.workorder.interfaces.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.workorder.mapper.EoMettingGoodsMapper;
import com.bjxczy.onepark.workorder.mapper.EoVisitInfoMapper;
import com.bjxczy.onepark.workorder.pojo.dto.EoMettingGoodsDTO;
import com.bjxczy.onepark.workorder.pojo.dto.EoVisitInfoDTO;
import com.bjxczy.onepark.workorder.pojo.po.EoMettingGoodsPO;
import com.bjxczy.onepark.workorder.pojo.po.EoVisitInfoPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @version 1.0.0
 * @ClassName ExcelDataListener
 * @Description 读取数据监听器
 * @createTime 2022年01月09日
 */
@Component
@Slf4j
public class ExcelMettingGoodsDataListener extends AnalysisEventListener<EoMettingGoodsDTO> {

    List<EoMettingGoodsDTO> list = new ArrayList<>();

    @Override
    public void invoke(EoMettingGoodsDTO eoMettingGoodsDTO, AnalysisContext context) {
        log.info("解析到一条数据:{}", JSON.toJSONString(eoMettingGoodsDTO));
        list.add(eoMettingGoodsDTO);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        log.info("所有数据解析完成！");
    }
    public List<EoMettingGoodsDTO> getList(){
        return list;
    }

    public void setList(List<EoMettingGoodsDTO> list){
        this.list=list;
    }
}

