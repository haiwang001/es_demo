
package com.bjxczy.onepark.workorder.service;

import com.bjxczy.onepark.workorder.pojo.dto.WoDataOverviewDTO;

import java.util.List;
import java.util.Map;

public interface WoDataOverviewService {
    Map<String,Object> taskCounts(WoDataOverviewDTO woDataOverviewDTO);

    Map<String, Object> chartList(WoDataOverviewDTO woDataOverviewDTO);

    List<Map<String, Object>> listByDepart(WoDataOverviewDTO woDataOverviewDTO);

    Map<String, Object> chartListByDepart(WoDataOverviewDTO woDataOverviewDTO);
}
