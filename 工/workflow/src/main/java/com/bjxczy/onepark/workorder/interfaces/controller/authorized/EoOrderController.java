package com.bjxczy.onepark.workorder.interfaces.controller.authorized;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workorder.pojo.dto.*;
import com.bjxczy.onepark.workorder.pojo.po.EoMettingGoodsPO;
import com.bjxczy.onepark.workorder.pojo.vo.*;
import com.bjxczy.onepark.workorder.service.EoTaskService;
import com.bjxczy.onepark.workorder.service.EoTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @ClassName EoTemplateController
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/16 13:51
 * @Version 1.0
 */
@Api(tags = "Eo单模块接口")
@ApiResourceController
@RequestMapping("/eo")
public class EoOrderController {


    @Autowired
    private EoTemplateService eoTemplateService;

    @Autowired
    private EoTaskService eoTaskService;

    @ApiOperation("移动端-查询餐饮需求预定、酒店需求预定、会议需求预定、来访需求预定、EO单预定")
    @GetMapping(value = "getCateringInfo", name = "移动端-查询餐饮需求预定、酒店需求预定、会议需求预定、来访需求预定、EO单预定")
    public EoTaskVO getCateringInfo(String eoId) {
        return eoTaskService.getCateringInfo(eoId);
    }

    @ApiOperation("新增EO单")
    @PostMapping(value = "/addEoTask", name = "新增EO单")
    public void addEoTask(@RequestBody EoTaskDTO eoTaskDTO, @LoginUser(isFull = true) UserInformation user) {
        eoTaskService.addEoTask(eoTaskDTO, user);
    }

    @ApiOperation("查询EO单-分页")
    @GetMapping(value = "/getEoTaskByPage", name = "查询EO单-分页")
    public IPage<EoTaskVO> getEoTaskByPage(EoTaskDTO eoTaskDTO) {
        return eoTaskService.getEoTaskByPage(eoTaskDTO);
    }

    @ApiOperation("编辑EO单")
    @PutMapping(value = "/editEoTask", name = "编辑EO单")
    public void editEoTask(@RequestBody EoTaskDTO eoTaskDTO) {
        eoTaskService.editEoTask(eoTaskDTO);
    }

    @ApiOperation("审批EO单")
    @PutMapping(value = "/checkEoTask", name = "审批EO单")
    public void checkEoTask(@RequestBody EoTaskDTO eoTaskDTO) {
        eoTaskService.checkEoTask(eoTaskDTO);
    }

    @ApiOperation("删除EO单")
    @DeleteMapping(value = "/delEoTask/{id}", name = "删除EO单")
    public void delEoTask(@PathVariable("id") String id) {
        eoTaskService.delEoTask(id);
    }

    @ApiOperation("新增消费模板")
    @PostMapping(value = "/addEoTemplate", name = "新增消费模板")
    public void addEoTemplate(@RequestBody EoTemplateDTO eoTemplateDTO) {
        eoTemplateService.addEoTemplate(eoTemplateDTO);
    }

    @ApiOperation("编辑消费模板")
    @PutMapping(value = "/editEoTemplate", name = "编辑消费模板")
    public void editEoTemplate(@RequestBody EoTemplateDTO eoTemplateDTO) {
        eoTemplateService.editEoTemplate(eoTemplateDTO);
    }

    @ApiOperation("删除消费模板")
    @DeleteMapping(value = "/delEoTemplate/{id}", name = "删除消费模板")
    public void delEoTemplate(@PathVariable("id") String id) {
        eoTemplateService.delEoTemplate(id);
    }

    @ApiOperation("查询消费模板-分页")
    @GetMapping(value = "/getEoTemplatesByPage", name = "查询消费模板-分页")
    public IPage<EoTemplateVO> getEoTemplatesByPage(EoTemplateDTO eoTemplateDTO) {
        return eoTemplateService.getEoTemplates(eoTemplateDTO);
    }

    @ApiOperation("收费标准下拉列表")
    @GetMapping(value = "/getEoTemplatesPullList", name = "收费标准下拉列表")
    public List<EoTemplateVO> getEoTemplatesPullList() {
        return eoTemplateService.getEoTemplatesPullList();
    }

    @ApiOperation("详情-新增访客人员信息")
    @PostMapping(value = "/addVisits",name = "详情-新增访客人员信息")
    public void addVisits(@RequestBody EoVisitInfoDTO eoVisitInfoDTO,@LoginUser(isFull = true) UserInformation user){
        eoTaskService.addVisits(eoVisitInfoDTO,user);
    }

    @ApiOperation("详情-新增车辆信息")
    @PostMapping(value = "/addVehicleInfo",name = "详情-新增车辆信息")
    public void addVehicleInfo(@RequestBody EoVehicleInfoDTO eoVehicleInfoDTO ,@LoginUser(isFull = true) UserInformation user){
        eoTaskService.addVehicleInfo(eoVehicleInfoDTO,user);
    }

    @ApiOperation("详情-新增会议物品信息")
    @PostMapping(value = "/addMettingGoods",name = "详情-新增会议物品信息")
    public void addMettingGoods(@RequestBody EoMettingGoodsDTO eoMettingGoodsDTO){
        eoTaskService.addMettingGoods(eoMettingGoodsDTO);
    }

    @ApiOperation("详情-删除访客人员信息")
    @DeleteMapping(value = "/delVisitById/{id}",name = "详情-删除访客人员信息")
    public void delVisitById(@PathVariable("id") Integer id){
        eoTaskService.delVisitById(id);
    }

    @ApiOperation("详情-删除车辆信息")
    @DeleteMapping(value = "/delVehicleById/{id}",name = "详情-删除车辆信息")
    public void delVehicleById(@PathVariable("id")Integer id){
        eoTaskService.delVehicleById(id);
    }

    @ApiOperation("详情-删除会议物品")
    @DeleteMapping(value = "/delMettingGoodsById/{id}",name = "详情-删除会议物品")
    public void delMettingGoodsById(@PathVariable("id") Integer id){
        eoTaskService.delMettingGoodsById(id);
    }
    @ApiOperation("下载人员信息模板")
    @GetMapping(value = "/downloadVisitTemplate",name = "下载人员信息模板")
    public void downloadVisitTemplate(HttpServletResponse response){
        eoTaskService.downloadVisitTemplate(response);
    }

    @ApiOperation("下载车辆信息模板")
    @GetMapping(value = "/downloadVehicleTemplate",name = "下载车辆信息模板")
    public void downloadVehicleTemplate(HttpServletResponse response){
        eoTaskService.downloadVehicleTemplate(response);
    }

    @ApiOperation("下载会议物品模板")
    @GetMapping(value = "/downloadMettingGoodsTemplate",name = "下载会议物品模板")
    public void downloadMettingGoodsTemplate(HttpServletResponse response){
        eoTaskService.downloadMettingGoodsTemplate(response);
    }

    @ApiOperation("导入访客数据")
    @PostMapping(value = "/importVisitData",name = "导入访客数据")
    public void importVisitData(@RequestParam("file") MultipartFile file,
                                @RequestParam("eoId") String eoId,
                                @LoginUser(isFull = true) UserInformation user){
        eoTaskService.importVisitData(file,eoId,user);
    }

    @ApiOperation("导入车辆数据")
    @PostMapping(value = "/importVehicleData",name = "导入车辆数据")
    public void importVehicleData(@RequestParam("file") MultipartFile file,
                                  @RequestParam("vehicleId") Integer vehicleId,
                                  @LoginUser(isFull = true) UserInformation user){
        eoTaskService.importVehicleData(file,vehicleId,user);
    }

    @ApiOperation("导入会议物品数据")
    @PostMapping(value = "/importMettingGoodsData",name = "导入会议物品数据")
    public void importMettingGoodsData(@RequestParam("file") MultipartFile file,@RequestParam("mettingId") Integer mettingId){
        eoTaskService.importMettingGoodsData(file,mettingId);
    }

    @ApiOperation("详情-查询人员信息分页")
    @GetMapping(value ="/getVisitInfoByPage",name = "详情-查询人员信息分页")
    public IPage<EoVisitInfoVO>getVisitInfoByPage(EoVisitInfoDTO eoVisitInfoDTO){
        return eoTaskService.getVisitInfoByPage(eoVisitInfoDTO);
    }

    @ApiOperation("详情-查询车辆信息分页")
    @GetMapping(value ="/getVehicleInfoByPage",name = "详情-查询车辆信息分页")
    public IPage<EoVehicleInfoVO>getVehicleInfoByPage(EoVehicleInfoDTO eoVehicleInfoDTO){
        return eoTaskService.getVehicleInfoByPage(eoVehicleInfoDTO);
    }
    @ApiOperation("详情-查询会议物品信息分页")
    @GetMapping(value ="/getMettingGoodsInfoByPage",name = "详情-查询会议物品信息分页")
    public IPage<EoMettingGoodsPO>getMettingGoodsInfoByMetId(EoMettingGoodsDTO eoMettingGoodsDTO){
        return eoTaskService.getMettingGoodsInfoByMetId(eoMettingGoodsDTO);
    }

    @ApiOperation("执行EO单-联动工单")
    @PostMapping(value = "/execute",name = "执行EO单-联动工单")
    public void execute(@RequestBody EoExecuteDTO eoExecuteDTO){
        eoTaskService.execute(eoExecuteDTO);
    }

    @ApiOperation("EO单-结算EO单")
    @PostMapping(value = "/settlement",name = "EO单-结算EO单")
    public void settlement(@RequestBody EoSettlementDTO eoSettlementDTO){
        eoTaskService.settlement(eoSettlementDTO);
    }

    @ApiOperation("Eo单-根据eoId查询结算信息")
    @GetMapping(value = "/getSettlementInfo",name = "Eo单-根据eoId查询结算信息")
    public EoSettlementVO getSettlementInfo(String eoId){
        return eoTaskService.getSettlementInfo(eoId);
    }

}
