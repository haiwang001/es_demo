package com.bjxczy.onepark.workflow.form.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;
import com.bjxczy.onepark.workflow.form.mapper.FormItemMapper;
import com.bjxczy.onepark.workflow.form.service.FormItemService;

@Service
public class FormItemServiceImpl extends ServiceImpl<FormItemMapper, FormItemPO> implements FormItemService {
	
	@Override
	public void saveByForm(FormdefPO entity, List<FormItemPO> formItem) {
		// 物理删除
		baseMapper.deleteByFormId(entity.getId());
		if (formItem != null && formItem.size() > 0) {
			for (int i = 0; i < formItem.size(); i++) {
				FormItemPO e = formItem.get(i);
				e.setSort(i);
				e.setFormId(entity.getId());
			}
			this.saveBatch(formItem);
		}
	}

	@Override
	public void removeByForm(FormdefPO po) {
		this.lambdaUpdate().eq(FormItemPO::getFormId, po.getId()).remove();
	}

	@Override
	public List<FormItemPO> listByFormId(String formId) {
		return this.lambdaQuery().eq(FormItemPO::getFormId, formId).orderByAsc(FormItemPO::getSort).list();
	}

	@Override
	public List<FormItemPO> igoreDelListByFormId(String formId) {
		return baseMapper.igoreDelListByFormId(formId);
	}

}
