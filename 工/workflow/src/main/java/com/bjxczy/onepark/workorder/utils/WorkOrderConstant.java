package com.bjxczy.onepark.workorder.utils;

public interface WorkOrderConstant {
    /**
     * 默认事件编码
     */
    String DEFAULT_EVENT_CODE = "WorkOrder";

    /**
     * 工单指派
     */
    String EVENT_ASSIGN_SUFFIX = "_Assign";

    /**
     * 工单取消
     */
    String EVENT_CANCEL_SUFFIX = "_Cancel";

    /**
     * 工单验收
     */
    String EVENT_CHECK_SUFFIX = "_Check";

    /**
     * 完成工单驳回
     */
    String EVENT_COMPLETE_REJECT_SUFFIX = "_Complete_Reject";

    /**
     * 工单执行
     */
    String EVENT_EXECUTE_SUFFIX = "_Execute";

    /**
     * 发起工单驳回
     */
    String EVENT_INITIATE_REJECT_SUFFIX = "_Initiate_Reject";

    /**
     * 验收通过
     */
    String EVENT_PASS_SUFFIX = "_Pass";

    /**
     * 转单求助
     */
    String EVENT_TRANSFER_SUFFIX = "_Transfer";

    /**
     * 转单拒单
     */
    String EVENT_TRANSFER_REJECT_SUFFIX="_Transfer_Reject";

}

