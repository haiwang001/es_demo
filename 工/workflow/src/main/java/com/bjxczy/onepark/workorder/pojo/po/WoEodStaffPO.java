package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 班组员工表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("wo_eod_staff")
@ApiModel(value="WoEodStaffPO", description="班组员工表")
public class WoEodStaffPO  {

    @ApiModelProperty(value = "自增Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "班组Id")
    private Integer eodId;

    @ApiModelProperty(value = "员工Id")
    private Integer staffId;

    @ApiModelProperty(value = "员工姓名")
    private String staffName;

    @ApiModelProperty(value = "值班排序")
    private Integer sort;

}
