package com.bjxczy.onepark.workorder.constant;

/**
 * @ClassName EoTaskStatusConstants
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 10:52
 * @Version 1.0
 */
public interface EoTaskStatusConstants {
    // 待审批
    String CHECK_STATUS="0";
    // 已通过
    String PASS_STATUS="1";
    // 已驳回
    String REJECT_STATUS="2";
    // 已执行
    String EXECUTE_STATUS="3";
    // 已完成
    String FINISH_STATUS="4";
}
