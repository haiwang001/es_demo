package com.bjxczy.onepark.workflow.process.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.workflow.process.entity.ProcdefNodePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefVersionPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.CreateProcessDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryProcessPageDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefActiveVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefDetailVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefVO;

public interface ProcessDefinitionService extends IService<ProcdefPO> {

	IPage<ProcdefVO> pageList(QueryProcessPageDTO dto);

	void create(CreateProcessDTO dto);

	void edit(String id, CreateProcessDTO dto);

	ProcdefDetailVO getDetail(String id);

	void removeProcess(String id);

	ProcdefPO getByDeployId(String id);

	ProcdefActiveVO getProcdefActive(String procdefId);

	ProcdefNodePO getActiveProcdefNode(String key, String taskDefinitionKey);

	ProcdefVersionPO getActiveVersionByDeployId(String key);

	void updateStatus(String id, Integer enable);

	List<ProcdefPO> listByEnable(Integer enable);

}
