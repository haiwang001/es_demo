package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 班组排班表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("wo_eod_staff_schedule")
@ApiModel(value="WoEodStaffSchedulePO", description="班组排班表")
public class WoEodStaffSchedulePO{

    @ApiModelProperty(value = "自增Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "值班时间")
    private String eodTime;


    @ApiModelProperty(value = "值班时间类型（1.白班，2.夜班，3.休息）")
    private Integer eodTimeType;

    @ApiModelProperty(value = "员工Id")
    private Integer staffId;

    @ApiModelProperty(value = "员工编号")
    private String employeeNo;

    @ApiModelProperty(value = "员工姓名")
    private String staffName;

    @ApiModelProperty(value = "电话")
    private String mobile;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "岗位名称")
    private String positionName;

    @ApiModelProperty(value = "值班日期")
    private String eodDate;

    @ApiModelProperty(value = "班组id")
    private Integer eodId;

    private Date startTime;

    private Date endTime;


}
