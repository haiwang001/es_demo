package com.bjxczy.onepark.workflow.process.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@TableName("wf_re_procdef")
public class ProcdefPO extends UserOperator {
	
	private static final long serialVersionUID = -2552716083327995289L;

	@TableId(type = IdType.ASSIGN_UUID)
	private String id;
	
	private String deployId;
	
	private String deployType;
	
	private String name;
	
	private String remark;
	
	private Integer applyRange;
	
	private String icon;
	
	private Integer enable;
	
	private String actVersion;
	
	private String woId;
	
	private String woName;
	
	private String serviceName;

	public ProcdefPO(String name, String remark, Integer applyRange, String icon) {
		super();
		this.deployType = "2";
		this.name = name;
		this.remark = remark;
		this.applyRange = applyRange;
		this.icon = icon;
		this.enable = 0;
	}
	
	public void setWorkOrder(String woId, String woName) {
		this.woId = woId;
		this.woName = woName;
	}
	
	public void removeWorkOrder() {
		this.setWorkOrder(null, null);
	}

}
