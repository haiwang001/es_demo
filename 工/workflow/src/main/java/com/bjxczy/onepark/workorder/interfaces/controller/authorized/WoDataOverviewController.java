package com.bjxczy.onepark.workorder.interfaces.controller.authorized;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.workorder.pojo.dto.PullDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoDataOverviewDTO;
import com.bjxczy.onepark.workorder.service.WoDataOverviewService;
import com.bjxczy.onepark.workorder.service.WoTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

/**
 * @ClassName WoDataOverview
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/4 15:31
 * @Version 1.0
 */
@ApiResourceController
@Api(tags = "数据概览接口")
@RequestMapping("/data/overview")
public class WoDataOverviewController {

    @Autowired
    private WoTaskService woTaskService;

    @Autowired
    private WoDataOverviewService woDataOverviewService;

    @ApiOperation("工单数统计")
    @GetMapping(value = "/count/list",name = "工单数统计")
    public Map<String, Object> taskCounts(WoDataOverviewDTO woDataOverviewDTO){
        return woDataOverviewService.taskCounts(woDataOverviewDTO);
    }

    @ApiOperation("工单数柱状图统计")
    @GetMapping(value = "/chart/list",name = "工单数柱状图统计")
    public Map<String, Object> chartList(WoDataOverviewDTO woDataOverviewDTO){
        return woDataOverviewService.chartList(woDataOverviewDTO);
    }

    @ApiOperation("部门完成工单数统计")
    @GetMapping(value = "/count/listByDepart",name = "部门完成工单数统计")
    public List<Map<String,Object>> listByDepart(WoDataOverviewDTO woDataOverviewDTO){
        return woDataOverviewService.listByDepart(woDataOverviewDTO);
    }

    @ApiOperation("部门完成工单数柱状图统计")
    @GetMapping(value = "/chart/listByDepart",name = "部门完成工单数柱状图统计")
    public Map<String, Object> chartListByDepart(WoDataOverviewDTO woDataOverviewDTO){
        return woDataOverviewService.chartListByDepart(woDataOverviewDTO);
    }

    @ApiOperation("pc端-部门工单人员下拉联动")
    @PostMapping(value = "/pullList",name = "pc端-部门工单人员下拉联动")
    public List<Map<String,Object>> pullList(@RequestBody PullDTO pullDTO){
        return woTaskService.pullList(pullDTO);
    }
}
