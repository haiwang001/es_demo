package com.bjxczy.onepark.workflow.process.core.condition;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;

@Component
public class OrganizationExpressionHandler extends AbstractExpressionHandler {

	/**
	 * 发起人 属于某部门
	 * 
	 * @param deployKey
	 * @param staffId
	 * @return
	 */
	public boolean belongSomeDept(String deployKey, Integer staffId) {
		return handler(deployKey, staffId, "belong_dept", (staff, line) -> {
			List<Integer> values = JSON.parseArray(line.getExpression(), Integer.class);
			for (DeptNameInfo item : staff.getDeptNames()) {
				if (values.contains(item.getId())) {
					return true;
				}
			}
			return false;
		});
	}

	/**
	 * 发起人 属于某职级
	 * 
	 * @param deployKey
	 * @param staffId
	 * @return
	 */
	public boolean bolongSomeRank(String deployKey, Integer staffId) {
		return handler(deployKey, staffId, "belong_rank", (staff, line) -> {
			List<Integer> values = JSON.parseArray(line.getExpression(), Integer.class);
			return values.contains(staff.getRankId());
		});
	}

	/**
	 * 发起人 属于某些人其中之一
	 * 
	 * @param deployKey
	 * @param staffId
	 * @return
	 */
	public boolean belongSomeOne(String deployKey, Integer staffId) {
		ProcdefLinePO linedef = getLinedef(deployKey, "belong_some_one");
		List<Integer> values = JSON.parseArray(linedef.getExpression(), Integer.class);
		return values.contains(staffId);
	}

}
