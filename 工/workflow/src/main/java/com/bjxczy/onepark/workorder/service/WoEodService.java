package com.bjxczy.onepark.workorder.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.dto.WoEodDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoEodStaffScheduleDTO;
import com.bjxczy.onepark.workorder.pojo.vo.EodVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoEodStaffScheduleVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface WoEodService {
    void createEod(WoEodDTO woEodDTO);

    Page<EodVO> getEodList(WoEodDTO woEodDTO);

    void updateEod(WoEodDTO woEodDTO);

    Map<String, Object> sort(List<String> staffNames);

    void generateSchedule(Integer eodId);

    Boolean isEodStaff(Integer staffId);

    void delWoEod(List<Integer> ids);

    void export(HttpServletResponse response);

    List<WoEodStaffScheduleVO> mobList(WoEodStaffScheduleDTO dto);
}
