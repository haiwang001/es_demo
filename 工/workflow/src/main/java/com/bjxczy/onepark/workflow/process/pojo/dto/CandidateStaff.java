package com.bjxczy.onepark.workflow.process.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CandidateStaff {
	
	private Integer id;
	
	private String staffName;
	
	private String deptName;

}
