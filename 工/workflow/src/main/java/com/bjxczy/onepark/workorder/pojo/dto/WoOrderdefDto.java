package com.bjxczy.onepark.workorder.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 * @ClassName WoOrderdefDto
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/30 15:03
 * @Version 1.0
 */
@Data
public class WoOrderdefDto extends BasePageDTO {

    private String id;

    @ApiModelProperty(value = "工单编号")
    private String sequenceNo;

    @ApiModelProperty(value = "部门Id")
    private Integer deptId;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "部门主管Id")
    private Integer ownerId;

    @ApiModelProperty(value = "部门主管姓名")
    private String ownerName;

    @ApiModelProperty(value = "工单名称")
    private String name;

    @ApiModelProperty(value = "工单描述")
    private String description;

    @ApiModelProperty(value = "工单类型")
    private String orderType;

    @ApiModelProperty(value = "业务模块")
    private String businessType;

    @ApiModelProperty(value = "紧急程度")
    private String orderLevel;

    @ApiModelProperty(value = "接单类型")
    private String taskType;

    @ApiModelProperty(value = "过期时间（分钟）")
    private Integer expireTime;

    @ApiModelProperty(value = "表单")
    private String formId;

    @ApiModelProperty(value = "完成后触发工单ID")
    private LinkedHashSet<String> triggerIds;

    @ApiModelProperty(value = "是否启用：0-禁用；1-启用")
    private Integer enable;

    private String linkStrategy;

    @ApiModelProperty(value = "开关（开：1 关：0）")
    private Integer deviceSwitch;


}
