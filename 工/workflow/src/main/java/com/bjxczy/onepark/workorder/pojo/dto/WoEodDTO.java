package com.bjxczy.onepark.workorder.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName WoEodDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/28 13:38
 * @Version 1.0
 */
@Data
public class WoEodDTO extends BasePageDTO{
    private Integer id;

    @ApiModelProperty(value = "部门Id")
    private Integer deptId;

    @ApiModelProperty(value = "班组名称")
    private String name;

    @ApiModelProperty(value = "值班标题")
    private String eodName;

    @ApiModelProperty(value = "值班描述")
    private String eodDescribe;

    @ApiModelProperty(value = "区域Id")
    private Integer areaId;

    @ApiModelProperty(value = "局址Id")
    private String tenantId;

    @ApiModelProperty(value = "空间Id")
    private String spaceId;

    @ApiModelProperty(value = "班组员工")
    private List<Integer> staffIds;

    @ApiModelProperty(value = "班组状态")
    private Integer status;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

}
