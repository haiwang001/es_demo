package com.bjxczy.onepark.workorder.interfaces.listener;

import com.bjxczy.core.rabbitmq.supports.OrganizationBaseListener;
import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.workorder.interfaces.publisher.StaffEventPublisher;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrganizationEventListener extends OrganizationBaseListener {

	@Autowired
	private StaffEventPublisher staffEventPublisher;

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmpworkflow_update_staff_queue", durable = "true"),  // 持久化队列
                    key = UPDATE_STAFF_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) // 绑定的交换机
            )
    })
    @Override
    public void updateStaffEventHandler(StaffInformation info, Channel channel, Message message) {
        log.info("[UPDATE_STAFF_EVENT] message={}", info);
        staffEventPublisher.updateStaffEvent(info);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmpworkflow_remove_staff_queue", durable = "true"),  // 持久化队列
                    key = REMOVE_STAFF_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) // 绑定的交换机
            )
    })
    @Override
    public void removeStaffEventHandler(RemoveStaffInfo info, Channel channel, Message message) {
        log.info("[UPDATE_STAFF_EVENT] message={}", info);
		staffEventPublisher.removeStaffEvent(info);
    }

}
