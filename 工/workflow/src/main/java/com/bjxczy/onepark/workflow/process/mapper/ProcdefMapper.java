package com.bjxczy.onepark.workflow.process.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryProcessPageDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefVO;

@Mapper
public interface ProcdefMapper extends BaseMapper<ProcdefPO> {

	IPage<ProcdefVO> queryPage(IPage<ProcdefPO> page, @Param("dto") QueryProcessPageDTO dto);
	
	ProcdefVO findVOById(@Param("id") String id);

}
