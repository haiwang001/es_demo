package com.bjxczy.onepark.workflow.process.interfaces.controller.authorized;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.privacy.MaskingOperation;
import com.bjxczy.core.web.privacy.ResponseMasking;
import com.bjxczy.core.web.privacy.iterator.IPageMaskingIterator;
import com.bjxczy.onepark.workflow.process.entity.RuProcessPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMonitorDTO;
import com.bjxczy.onepark.workflow.process.service.RuProcessService;

@ApiResourceController
@RequestMapping("/process/run")
public class ProcessMonitorController {
	
	@Autowired
	private RuProcessService ruProcessService;
	
	@GetMapping(value = "/monitor", name = "审批监控")
	@ResponseMasking(operation = MaskingOperation.QUERY, iterator = IPageMaskingIterator.class)
	public IPage<RuProcessPO> listMonitorPage(QueryMonitorDTO dto) {
		return ruProcessService.listMonitorPage(dto);
	}

}
