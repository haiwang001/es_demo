package com.bjxczy.onepark.workorder.pojo.po;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName EoVisitInfoPO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/24 17:05
 * @Version 1.0
 */
@Data
@TableName("eo_settlement")
public class EoSettlementPO {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "EO单Id")
    private String eoId;

    @ApiModelProperty(value = "住宿实付金额")
    private Integer feeAccommodation;

    @ApiModelProperty(value = "餐饮实付金额")
    private Integer feeCatering;

    @ApiModelProperty(value = "会议实付金额")
    private Integer feeMetting;

    @ApiModelProperty(value = "实付总计")
    private Integer feeTotal;
}
