package com.bjxczy.onepark.workorder.pojo.po;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 任务定义表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="WoTaskdef导出PO", description="任务定义导出表")
public class WoTaskdefExcelPO {

    @ExcelProperty("工单编号")
    @ApiModelProperty(value = "工单编号")
    private String sequenceNo;

    @ExcelProperty("工单名称")
    @ApiModelProperty(value = "工单名称")
    private String name;

    @ExcelIgnore
    @ApiModelProperty(value = "工单类型")
    private String orderType;

    @ExcelProperty("工单类型")
    private String orderTypeName;

    @ExcelProperty("业务模块")
    private String businessTypeName;

    @ExcelIgnore
    @ApiModelProperty(value = "业务模块")
    private String businessType;

    @ExcelProperty("部门名称")
    @ApiModelProperty(value = "部门名称")
    private String assignDeptName;

    @ExcelProperty("工单详述")
    @ApiModelProperty(value = "工单详述")
    private String description;

    @ExcelIgnore
    @ApiModelProperty(value = "紧急程度")
    private String orderLevel;

    @ExcelProperty("紧急程度")
    private String orderLevelName;

    @ExcelProperty("位置")
    private String position;

    @ExcelProperty("任务类型")
    private String repeatTypeName;

    @ExcelIgnore
    @ApiModelProperty(value = "重复类型")
    private String repeatType;

    @ColumnWidth(18)
    @ExcelProperty("创建时间")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

}
