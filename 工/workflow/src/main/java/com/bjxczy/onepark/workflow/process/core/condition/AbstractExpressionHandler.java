package com.bjxczy.onepark.workflow.process.core.condition;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.nacos.common.utils.BiFunction;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.service.ProcdefLineService;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;

public class AbstractExpressionHandler {

	@Autowired
	protected StaffFeignClient staffFeignClient;
	@Autowired
	protected ProcessDefinitionService procdefService;
	@Autowired
	protected ProcdefLineService procdefLineService;

	protected boolean handler(String deployKey, Integer staffId, String conditionType,
			BiFunction<StaffInformation, ProcdefLinePO, Boolean> func) {
		return func.apply(getStaff(staffId), getLinedef(deployKey, conditionType));
	}

	protected StaffInformation getStaff(Integer staffId) {
		return staffFeignClient.findById(staffId);
	}

	protected ProcdefLinePO getLinedef(String deployKey, String conditionType) {
		ProcdefPO def = procdefService.getByDeployId(deployKey);
		return procdefLineService.getByConditionType(def.getActVersion(), conditionType);
	}

}
