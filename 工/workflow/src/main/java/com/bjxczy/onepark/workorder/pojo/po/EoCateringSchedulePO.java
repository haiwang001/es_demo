package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 餐饮时刻表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="EoCateringSchedule对象", description="餐饮时刻表")
@TableName("eo_catering_schedule")
public class EoCateringSchedulePO {


    @ApiModelProperty(value = "主键Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "餐饮Eo单Id")
    private Integer cateringId;

    @ApiModelProperty(value = "日期")
    private String cateringDate;

    @ApiModelProperty(value = "早餐时间")
    private String breakfastTime;

    @ApiModelProperty(value = "午餐时间")
    private String lunchTime;

    @ApiModelProperty(value = "晚餐时间")
    private String dinnerTime;

    @ApiModelProperty(value = "用餐类型（字典表：mealType）")
    private String mealType;

}
