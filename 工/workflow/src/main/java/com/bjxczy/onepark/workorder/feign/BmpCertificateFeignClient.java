package com.bjxczy.onepark.workorder.feign;

import com.bjxczy.core.feign.client.car.CarFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("certificate")
public interface BmpCertificateFeignClient extends CarFeignClient {

}
