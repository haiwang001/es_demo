package com.bjxczy.onepark.workflow.process.interfaces.controller.authorized;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.log.LogParam;
import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import com.bjxczy.onepark.workflow.process.pojo.dto.CreateProcessDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryProcessPageDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.ProcdefVO;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;

@ApiResourceController
@RequestMapping("/process/def")
public class ProcessDefinitionController {
	
	@Autowired
	private ProcessDefinitionService processDefinitionService;
	
	@GetMapping(value = "/page", name = "流程定义查询（分页）")
	@SysLog(operationType = OperationType.SELECT, operation = "流程查询：页数${dto.pageNum}")
	public IPage<ProcdefVO> page(@LogParam QueryProcessPageDTO dto) {
		return processDefinitionService.pageList(dto);
	}
	
	@PostMapping(name = "创建流程")
	@SysLog(operationType = OperationType.INSERT, operation = "创建流程：${dto.name}")
	public void create(@RequestBody @LogParam CreateProcessDTO dto) {
		processDefinitionService.create(dto);
	}
	
	@PutMapping(value = "/{id}", name = "编辑流程信息")
	@SysLog(operationType = OperationType.UPDATE, operation = "编辑流程：${dto.name}")
	public void edit(@PathVariable("id") String id, @RequestBody @LogParam CreateProcessDTO dto) {
		processDefinitionService.edit(id, dto);
	}
	
	@DeleteMapping(value = "/{id}", name = "删除流程")
	@SysLog(operationType = OperationType.DELETE, operation = "删除流程：${ctx.name}")
	public void removeById(@PathVariable("id") String id) {
		processDefinitionService.removeProcess(id);
	}
	
	@PutMapping(value = "/{id}/status", name = "启用/禁用流程")
	@SysLog(operationType = OperationType.UPDATE, operation = "#if($enable==1)启用#else禁用#end流程：${ctx.name}")
	public void updateStatus(@PathVariable("id") String id, @RequestParam("enable") @LogParam Integer enable) {
		processDefinitionService.updateStatus(id, enable);
	}

}
