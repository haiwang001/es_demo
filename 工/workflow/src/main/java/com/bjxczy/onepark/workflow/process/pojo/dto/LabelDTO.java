package com.bjxczy.onepark.workflow.process.pojo.dto;

import lombok.Data;

@Data
public class LabelDTO {
	
	private Object id;
	
	private String label;
	
}
