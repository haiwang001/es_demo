package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;
import org.hibernate.validator.constraints.Length;
import lombok.Data;

/**
 * @ClassName EoTemplate
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/16 13:44
 * @Version 1.0
 */
@Data
@TableName("eo_template")
public class EoTemplatePO extends UserOperator {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    @Length(min = 0, max = 50, message = "名字字数在0-50之间！")
    private String name;

    private String catering;

    private String remark;
}
