package com.bjxczy.onepark.workorder.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workorder.feign.SpaceFeignClient;
import com.bjxczy.onepark.workorder.mapper.WoEodGroupMapper;
import com.bjxczy.onepark.workorder.mapper.WoEodStaffMapper;
import com.bjxczy.onepark.workorder.mapper.WoEodStaffScheduleMapper;
import com.bjxczy.onepark.workorder.pojo.dto.WoEodDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoEodStaffScheduleDTO;
import com.bjxczy.onepark.workorder.pojo.po.WoEodGroupExcelPO;
import com.bjxczy.onepark.workorder.pojo.po.WoEodGroupPO;
import com.bjxczy.onepark.workorder.pojo.po.WoEodStaffPO;
import com.bjxczy.onepark.workorder.pojo.po.WoEodStaffSchedulePO;
import com.bjxczy.onepark.workorder.pojo.vo.EodVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoEodStaffScheduleVO;
import com.bjxczy.onepark.workorder.service.WoEodService;
import com.bjxczy.onepark.workorder.utils.DateTools;
import com.bjxczy.onepark.workorder.utils.ExcelUtils;
import com.bjxczy.onepark.workorder.utils.ListConverterUtils;
import com.bjxczy.onepark.workorder.utils.WoEodUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @ClassName WoEodServiceImpl
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/28 13:36
 * @Version 1.0
 */
@Slf4j
@Service
public class WoEodServiceImpl implements WoEodService {

    @Autowired
    private WoEodGroupMapper woEodGroupMapper;

    @Autowired
    private WoEodStaffMapper woEodStaffMapper;

    @Autowired
    private SpaceFeignClient spaceFeignClient;

    @Autowired
    private StaffFeignClient staffFeignClient;

    @Autowired
    private WoEodUtils woEodUtils;

    @Autowired
    private WoEodStaffScheduleMapper woEodStaffScheduleMapper;

    @Autowired
    private ExcelUtils excelUtils;

    @Override
    public void createEod(WoEodDTO woEodDTO) {
        //校验员工是否已经分班组了
        checkEod(woEodDTO.getStaffIds(), null);
        WoEodGroupPO woEodGroupPO = new WoEodGroupPO();
        BeanUtils.copyProperties(woEodDTO, woEodGroupPO);
        //区域 远程调用获取区域名称：区域+楼宇+区间
        String position = spaceFeignClient.getPosition(woEodDTO.getSpaceId());
        woEodGroupPO.setEodArea(position);
        woEodGroupMapper.insert(woEodGroupPO);
        //插入班组员工表
        int sort = 1;
        for (Integer staffId : woEodDTO.getStaffIds()) {
            WoEodStaffPO woEodStaffPO = new WoEodStaffPO();
            log.info("=====================班组id为：{}===========================", woEodGroupPO.getId());
            woEodStaffPO.setEodId(woEodGroupPO.getId());
            woEodStaffPO.setStaffId(staffId);
            //远程调用获取员工名称
            StaffInformation staff = staffFeignClient.findById(staffId);
            woEodStaffPO.setStaffName(staff.getFldName());
            woEodStaffPO.setSort(sort);
            this.woEodStaffMapper.insert(woEodStaffPO);
            sort += 1;
        }
    }

    private void checkEod(List<Integer> staffIds, Integer eodId) {
        String staffNames = null;
        HashSet<String> set = new HashSet<>();
        for (Integer staffId : staffIds) {
            WoEodStaffPO woEodStaffPO = woEodStaffMapper.selectOne(Wrappers.<WoEodStaffPO>lambdaQuery()
                    .eq(WoEodStaffPO::getStaffId, staffId));
            if (null != eodId) {
                if (null != woEodStaffPO) {
                    if (!woEodStaffPO.getEodId().equals(eodId)){
                        set.add(woEodStaffPO.getStaffName());
                    }
                }
            } else {
                if (null != woEodStaffPO) {
                    set.add(woEodStaffPO.getStaffName());
                }
            }
        }
        staffNames = String.join(",", set);
        if (null != staffNames&&!"".equals(staffNames)) {
            throw new ResultException(staffNames+" 已有班组！");
        }
    }

    @Override
    public Page<EodVO> getEodList(WoEodDTO woEodDTO) {
        Page<EodVO> eodList = woEodGroupMapper.getEodList(woEodDTO.toPage(), woEodDTO);
        for (EodVO record : eodList.getRecords()) {
            List<WoEodStaffPO> woEodStaffPOS = woEodStaffMapper.selectList(Wrappers.<WoEodStaffPO>lambdaQuery()
                    .eq(WoEodStaffPO::getEodId, record.getId())
                    .orderByAsc(WoEodStaffPO::getSort));
            record.setWoEodStaffPOList(woEodStaffPOS);
        }
        return eodList;
    }

    // @Async
    @Override
    public void updateEod(WoEodDTO woEodDTO) {
        checkEod(woEodDTO.getStaffIds(),woEodDTO.getId());
        WoEodGroupPO woEodGroupPO = new WoEodGroupPO();
        BeanUtils.copyProperties(woEodDTO, woEodGroupPO);
        //改了局址
        Optional.ofNullable(woEodDTO.getSpaceId()).ifPresent(spaceId -> {
            //区域 远程调用获取区域名称：区域+楼宇+区间
            String position = spaceFeignClient.getPosition(spaceId);
            woEodGroupPO.setEodArea(position);
        });
        woEodGroupMapper.updateById(woEodGroupPO);
        //改班组的员工(删除该班组所有员工，然后重新添加员工)
        woEodStaffMapper.delete(Wrappers.<WoEodStaffPO>lambdaQuery()
                .eq(WoEodStaffPO::getEodId, woEodDTO.getId()));
        //插入班组员工表
        int sort = 1;
        for (Integer staffId : woEodDTO.getStaffIds()) {
            WoEodStaffPO woEodStaffPO = new WoEodStaffPO();
            woEodStaffPO.setEodId(woEodDTO.getId());
            woEodStaffPO.setStaffId(staffId);
            //远程调用获取员工名称
            StaffInformation staff = staffFeignClient.findById(staffId);
            woEodStaffPO.setStaffName(staff.getFldName());
            woEodStaffPO.setSort(sort);
            this.woEodStaffMapper.insert(woEodStaffPO);
            sort += 1;
        }
        //涉及排班
        // 1、改班组的员工 重新排班
        List<Integer> staffIds = woEodDTO.getStaffIds();
        if (null != staffIds && staffIds.size() > 0) {
            //删除改班组的原有排班
            woEodStaffScheduleMapper.delete(Wrappers.<WoEodStaffSchedulePO>lambdaQuery()
                    .eq(WoEodStaffSchedulePO::getEodId, woEodDTO.getId()));
            //重新生成该班组的排班
            generateSchedule(woEodDTO.getId());
        }
        // 2、改班组的状态 （0-1：排班该班组人员 1-0：删除该班组人员的排班）
        Integer status = woEodDTO.getStatus();
        Optional.ofNullable(status).ifPresent(sta -> {
            if (1 == sta) {
                //删除重新排班
                woEodStaffScheduleMapper.delete(Wrappers.<WoEodStaffSchedulePO>lambdaQuery()
                        .eq(WoEodStaffSchedulePO::getEodId, woEodDTO.getId()));
                //重新生成该班组的排班
                generateSchedule(woEodDTO.getId());
            } else {
                //删除重新排班
                woEodStaffScheduleMapper.delete(Wrappers.<WoEodStaffSchedulePO>lambdaQuery()
                        .eq(WoEodStaffSchedulePO::getEodId, woEodDTO.getId()));
            }
        });
    }

    @Override
    public Map<String, Object> sort(List<String> staffNames) {

        int dutyNum = staffNames.size();
        //日期
        int days = woEodUtils.getDaysOfMonth(new Date());
        //班次
        String[] classArr = {"白", "夜", "休", "休"};
        Map<String, Object> resMap = new HashMap<>();
        int classNum = classArr.length;
        for (int i = 0; i < days; i++) {
            String timeOfDay = getTimeOfDay(i + 1);
            List<String> staffList = new ArrayList<>();
            for (int j = classNum - 1; j >= 0; j--) {
                int a = i % dutyNum;
                if (j + a < dutyNum) {
                    System.out.println(timeOfDay + " 排班：" + staffNames.get(j + a));
                    staffList.add(staffNames.get(j + a));
                } else if (j + a >= dutyNum) {
                    System.out.println(timeOfDay + " 排班：" + staffNames.get((j + a) - dutyNum));
                    staffList.add(staffNames.get((j + a) - dutyNum));
                }
            }
            resMap.put(timeOfDay, staffList);
        }
        Map<String, Object> collect = resMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        return collect;
    }


    @Override
    public void generateSchedule(Integer eodId) {
        //生成每个启用班组的排班
        for (WoEodGroupPO woEodGroupPO : woEodGroupMapper.selectList(Wrappers.<WoEodGroupPO>lambdaQuery()
                .eq(WoEodGroupPO::getStatus, 1)
                .eq(null != eodId, WoEodGroupPO::getId, eodId))) {
            Integer eodItemId = woEodGroupPO.getId();
            List<WoEodStaffPO> woEodStaffPOS = woEodStaffMapper.selectList(Wrappers.<WoEodStaffPO>lambdaQuery()
                    .eq(WoEodStaffPO::getEodId, eodItemId));
            sortAllEodGroups(woEodStaffPOS, woEodGroupPO.getId());
        }
    }

    @Override
    public Boolean isEodStaff(Integer staffId) {
        List<WoEodStaffPO> woEodStaffPOS = woEodStaffMapper.selectList(Wrappers.<WoEodStaffPO>lambdaQuery()
                .eq(WoEodStaffPO::getStaffId, staffId));
        return woEodStaffPOS.size() > 0;
    }

    @Override
    public void delWoEod(List<Integer> ids) {
        if (null == ids || ids.size() == 0) {
            throw new ResultException("请选择要删除的班组！");
        }
        woEodGroupMapper.deleteBatchIds(ids);
        ids.forEach(eodId -> {
            //删除班组员工、删除员工排班
            woEodStaffMapper.delete(Wrappers.<WoEodStaffPO>lambdaQuery()
                    .eq(WoEodStaffPO::getEodId, eodId));
            woEodStaffScheduleMapper.delete(Wrappers.<WoEodStaffSchedulePO>lambdaQuery()
                    .eq(WoEodStaffSchedulePO::getEodId, eodId));
        });
    }

    @Override
    public void export(HttpServletResponse response) {
        List<WoEodGroupPO> woEodGroupPOS = woEodGroupMapper.selectList(Wrappers.emptyWrapper());
        List<WoEodGroupExcelPO> list = new ArrayList<>();
        woEodGroupPOS.forEach(woEodGroupPO -> {
            WoEodGroupExcelPO woEodGroupExcelPO = new WoEodGroupExcelPO();
            BeanUtils.copyProperties(woEodGroupPO, woEodGroupExcelPO);
            if (0 == woEodGroupPO.getStatus()) {
                woEodGroupExcelPO.setStatus("停用");
            } else {
                woEodGroupExcelPO.setStatus("正常");
            }
            list.add(woEodGroupExcelPO);
        });
        excelUtils.export(response, "班组列表数据导出", list, WoEodGroupExcelPO.class);
    }

    @Override
    public List<WoEodStaffScheduleVO> mobList(WoEodStaffScheduleDTO dto) {
        Date date = new Date();
        List<WoEodStaffSchedulePO> woEodStaffSchedulePOS = this.woEodStaffScheduleMapper.selectList(Wrappers.<WoEodStaffSchedulePO>lambdaQuery()
                .like(null != dto.getStaffName(), WoEodStaffSchedulePO::getStaffName, dto.getStaffName())
                .like(null != dto.getMobile(), WoEodStaffSchedulePO::getMobile, dto.getMobile())
                .like(null != dto.getEodDate(), WoEodStaffSchedulePO::getEodDate, dto.getEodDate()));
        ListConverterUtils utils = new ListConverterUtils<>(WoEodStaffScheduleVO.class);
        List<WoEodStaffScheduleVO> list = utils.converterList(woEodStaffSchedulePOS);
        list.forEach(vo -> {
            if (null != vo.getEodTime()) {
                //值班状态
                boolean startFlag = DateTools.compareDate(date, vo.getStartTime());
                boolean endFlag = DateTools.compareDate(vo.getEndTime(), date);
                if (startFlag && endFlag) {
                    vo.setDutyStatus("值班中");
                } else {
                    vo.setDutyStatus("休息中");
                }
            } else {
                vo.setDutyStatus("休息中");
            }
            StaffInfoFine staffInfo = staffFeignClient.getStaffById(vo.getStaffId());
            Optional.ofNullable(staffInfo.getPhoto()).ifPresent(photo -> {
                if (photo.size() > 0) {
                    vo.setPhoto(photo.get(0));
                }
            });
            //班组名称
            WoEodGroupPO woEodGroupPO = woEodGroupMapper.selectOne(Wrappers.<WoEodGroupPO>lambdaQuery()
                    .eq(WoEodGroupPO::getId, vo.getEodId()));
            vo.setEodName(woEodGroupPO.getEodName());
        });
        if (null != dto.getStatus() && dto.getStatus() == 1) {
            //值班中
            return list.stream().filter(woEodStaffScheduleVO -> {
                return woEodStaffScheduleVO.getDutyStatus().equals("值班中");
            }).collect(Collectors.toList());
        } else if (null != dto.getStatus() && dto.getStatus() == 0) {
            return list.stream().filter(woEodStaffScheduleVO -> {
                return woEodStaffScheduleVO.getDutyStatus().equals("休息中");
            }).collect(Collectors.toList());
        }
        return list;
    }

    public void sortAllEodGroups(List<WoEodStaffPO> woEodStaffPOS, Integer eodId) {
        int dutyNum = woEodStaffPOS.size();
        //日期
        int days = woEodUtils.getDaysOfMonth(new Date());
        //班次
        String[] classArr = {"白", "夜", "休", "休"};
        Map<String, Object> resMap = new HashMap<>();
        int classNum = classArr.length;
        for (int i = 0; i < days; i++) {
            String timeOfDay = getTimeOfDay(i + 1);
            List<String> staffList = new ArrayList<>();
            for (int j = classNum - 1; j >= 0; j--) {
                WoEodStaffSchedulePO woEodStaffSchedulePO = new WoEodStaffSchedulePO();
                woEodStaffSchedulePO.setEodId(eodId);
                int a = i % dutyNum;
                if (j + a < dutyNum) {
                    StaffInformation staffInformation = staffFeignClient.findById(woEodStaffPOS.get(j + a).getStaffId());
                    String positionName = staffInformation.getPositionName();
                    StringBuilder sb = new StringBuilder();
                    for (DeptNameInfo deptInfo : staffInformation.getDeptNames()) {
                        sb.append(deptInfo.getDeptName()).append("/");
                    }
                    String deptName = null;
                    if (StrUtil.isNotEmpty(sb)) {
                        deptName = sb.deleteCharAt(sb.lastIndexOf("/")).toString();
                    }
                    System.out.println(timeOfDay + " 排班：" + woEodStaffPOS.get(j + a).getStaffName());
                    woEodStaffSchedulePO.setStaffId(woEodStaffPOS.get(j + a).getStaffId());
                    woEodStaffSchedulePO.setStaffName(woEodStaffPOS.get(j + a).getStaffName());
                    woEodStaffSchedulePO.setEodDate(timeOfDay);
                    //白：8:30-20:30 夜：20:30-8:30 休息：null
                    if (j == 3) {//白
                        woEodStaffSchedulePO.setEodTime("08:30-20:30");
                        woEodStaffSchedulePO.setStartTime(DateTools.strDateToDate(timeOfDay + " 08:30", DateTools.FORMAT_05));
                        woEodStaffSchedulePO.setEndTime(DateTools.strDateToDate(timeOfDay + " 20:30", DateTools.FORMAT_05));
                        woEodStaffSchedulePO.setEodTimeType(1);
                    } else if (j == 2) {//夜
                        woEodStaffSchedulePO.setStartTime(DateTools.strDateToDate(DateTools.getAfterDate(timeOfDay, 1) + " 08:30", DateTools.FORMAT_05));
                        woEodStaffSchedulePO.setEndTime(DateTools.strDateToDate(DateTools.getAfterDate(timeOfDay, 1) + " 20:30", DateTools.FORMAT_05));
                        woEodStaffSchedulePO.setEodTime("20:30-08:30");
                        woEodStaffSchedulePO.setEodTimeType(2);
                    }else {
                        woEodStaffSchedulePO.setEodTimeType(3);
                    }
                    woEodStaffSchedulePO.setDeptName(deptName);
                    woEodStaffSchedulePO.setPositionName(positionName);
                    woEodStaffSchedulePO.setMobile(staffInformation.getMobile());
                    woEodStaffSchedulePO.setEmployeeNo(staffInformation.getEmployeeNo());
                } else if (j + a >= dutyNum) {
                    System.out.println(timeOfDay + " 排班：" + woEodStaffPOS.get((j + a) - dutyNum).getStaffName());
                    StaffInformation staffInformation = staffFeignClient.findById(woEodStaffPOS.get((j + a) - dutyNum).getStaffId());
                    String positionName = staffInformation.getPositionName();
                    StringBuilder sb = new StringBuilder();
                    for (DeptNameInfo deptInfo : staffInformation.getDeptNames()) {
                        sb.append(deptInfo.getDeptName()).append("/");
                    }
                    String deptName = null;
                    if (StrUtil.isNotEmpty(sb)) {
                        deptName = sb.deleteCharAt(sb.lastIndexOf("/")).toString();
                    }
                    System.out.println(timeOfDay + " 排班：" + woEodStaffPOS.get((j + a) - dutyNum).getStaffName());
                    woEodStaffSchedulePO.setStaffId(woEodStaffPOS.get((j + a) - dutyNum).getStaffId());
                    woEodStaffSchedulePO.setStaffName(woEodStaffPOS.get((j + a) - dutyNum).getStaffName());
                    woEodStaffSchedulePO.setEodDate(timeOfDay);
                    //白：8:30-20:30 夜：20:30-8:30 休息：null
                    if (j == 3) {//白
                        woEodStaffSchedulePO.setEodTime("08:30-20:30");
                        woEodStaffSchedulePO.setStartTime(DateTools.strDateToDate(timeOfDay + " 08:30", DateTools.FORMAT_05));
                        woEodStaffSchedulePO.setEndTime(DateTools.strDateToDate(timeOfDay + " 20:30", DateTools.FORMAT_05));
                        woEodStaffSchedulePO.setEodTimeType(1);
                    } else if (j == 2) {//夜
                        woEodStaffSchedulePO.setEodTime("20:30-08:30");
                        woEodStaffSchedulePO.setStartTime(DateTools.strDateToDate(DateTools.getAfterDate(timeOfDay, 1) + " 08:30", DateTools.FORMAT_05));
                        woEodStaffSchedulePO.setEndTime(DateTools.strDateToDate(DateTools.getAfterDate(timeOfDay, 1) + " 20:30", DateTools.FORMAT_05));
                        woEodStaffSchedulePO.setEodTimeType(2);
                    }else {
                        woEodStaffSchedulePO.setEodTimeType(3);
                    }
                    woEodStaffSchedulePO.setDeptName(deptName);
                    woEodStaffSchedulePO.setPositionName(positionName);
                    woEodStaffSchedulePO.setMobile(staffInformation.getMobile());
                    woEodStaffSchedulePO.setEmployeeNo(staffInformation.getEmployeeNo());
                }
                woEodStaffScheduleMapper.insert(woEodStaffSchedulePO);
            }
            resMap.put(timeOfDay, staffList);
        }
    }

    public String getTimeOfDay(int i) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, i);
        Date time = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(time);
    }
}
