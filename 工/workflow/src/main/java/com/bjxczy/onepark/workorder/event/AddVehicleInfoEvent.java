package com.bjxczy.onepark.workorder.event;

import com.bjxczy.onepark.common.model.workflow.AddVehicleInfo;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @ClassName AddVehicleInfoEvent
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/26 17:17
 * @Version 1.0
 */
@Getter
public class AddVehicleInfoEvent extends ApplicationEvent {


    private final AddVehicleInfo addVehicleInfo;

    public AddVehicleInfoEvent(Object source) {
        super(source);
        this.addVehicleInfo = (AddVehicleInfo) source;
    }
}
