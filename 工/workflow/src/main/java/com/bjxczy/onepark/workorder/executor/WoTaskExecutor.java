package com.bjxczy.onepark.workorder.executor;

import com.bjxczy.onepark.workorder.service.WoEodService;
import com.bjxczy.onepark.workorder.service.WoTaskService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName WoTaskExecutor
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/10 18:09
 * @Version 1.0
 */
@Component
@Slf4j
@RestController
public class WoTaskExecutor {

    @Autowired
    private WoTaskService woTaskService;

    @Autowired
    private WoEodService woEodService;
    /**
     * 工单模块：创建执行任务
     */
    @XxlJob("execTask")
    @GetMapping("/execute/task")
    public void execTask() {
        log.info("=====================================================================");
        log.info("========================工单模块：创建执行任务===========================");
        log.info("=====================================================================");
        //定时任务：创建执行任务
        woTaskService.execTask();
    }

    /**
     * 班组模块：每月一号自动生成本月的排班
     */
/*    @XxlJob("generateSchedule")
    @GetMapping("/generate/schedule")
    public void generateSchedule(){
        log.info("================================================================================");
        log.info("========================班组模块：每月一号自动生成本月的排班===========================");
        log.info("================================================================================");
        woEodService.generateSchedule(null);
    }*/
}
