package com.bjxczy.onepark.workflow.process.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workflow.process.entity.RuProcessPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMonitorDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMyTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryRuProcessDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.HiTaskVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.TaskVO;

@Mapper
public interface RuProcessMapper extends BaseMapper<RuProcessPO> {

	IPage<TaskVO> selectTask(IPage<RuProcessPO> page, @Param("dto") QueryMyTaskDTO dto,
			@Param("staffId") Integer staffId);

	IPage<RuProcessPO> selectProcess(IPage<RuProcessPO> page, @Param("dto") QueryRuProcessDTO dto,
			@Param("staffId") Integer staffId);

	IPage<HiTaskVO> selectHiTask(Page<Object> page, @Param("dto") QueryMyTaskDTO dto,
			@Param("staffId") Integer staffId);

	IPage<RuProcessPO> selectMonitorPage(Page<RuProcessPO> page, @Param("dto") QueryMonitorDTO dto);

}
