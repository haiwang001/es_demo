package com.bjxczy.onepark.workorder.interfaces.listener;

import com.bjxczy.core.redisson.constant.DelayedQueueNames;
import com.bjxczy.core.redisson.delayed.DelayedQueueImpl;
import com.bjxczy.core.redisson.delayed.IDelayedQueue;
import com.bjxczy.core.redisson.delayed.RDelayedPayload;
import com.bjxczy.core.web.message.MessageService;
import com.bjxczy.onepark.common.model.workbench.TemplateMessagePayload;
import com.bjxczy.onepark.workorder.mapper.WoTaskdefMapper;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskPO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskdefPO;
import com.bjxczy.onepark.workorder.utils.WorkOrderConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @ClassName WoTaskDelayedWarnQueueImpl
 * @Description 提醒发待办
 * @Author zhanghongguo
 * @Date 2023/4/11 11:39
 * @Version 1.0
 */
@Component(DelayedQueueNames.WORKFLOW_ORDER_WARN_TASK_QUEUE)
public class WoTaskDelayedWarnQueueImpl extends DelayedQueueImpl implements IDelayedQueue {

    @Autowired
    private WoTaskdefMapper woTaskdefMapper;

    @Autowired
    private MessageService messageService;

    @Override
    public void execute(RDelayedPayload payload) {
        Map<String, Object> extra = payload.getExtra();
        WoTaskPO woTaskPO = (WoTaskPO) extra.get("woTaskPO");
        //发待办 派单
        Map<String, Object> params = new HashMap<>();
        WoTaskdefPO woTaskdefPO = woTaskdefMapper.selectById(woTaskPO.getTaskDefId());
        Optional.ofNullable(woTaskdefPO).ifPresent(woTaskdef->{
            params.put("woTaskName",woTaskdef.getName());
            String eventCode = WorkOrderConstant.DEFAULT_EVENT_CODE + WorkOrderConstant.EVENT_ASSIGN_SUFFIX;
            messageService.pushTemplateMessage(new TemplateMessagePayload(woTaskPO.getOwnerId(),String.valueOf(woTaskPO.getId()),eventCode,params));
        });
    }
}
