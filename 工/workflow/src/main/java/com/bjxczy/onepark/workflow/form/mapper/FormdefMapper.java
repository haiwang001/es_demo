package com.bjxczy.onepark.workflow.form.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;

@Mapper
public interface FormdefMapper extends BaseMapper<FormdefPO> {

	FormdefPO igoreDelGetById(@Param("formId")String formId);

}
