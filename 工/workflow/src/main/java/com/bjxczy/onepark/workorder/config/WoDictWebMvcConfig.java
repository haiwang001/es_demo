package com.bjxczy.onepark.workorder.config;

import com.bjxczy.core.web.configration.DefaultWebMvcConfig;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.dict.JdbcDictServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

@Configuration
public class WoDictWebMvcConfig extends DefaultWebMvcConfig {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
    }
}
