package com.bjxczy.onepark.workorder.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * @ClassName TaskFormVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/8 11:34
 * @Version 1.0
 */
@Data
public class TaskFormVO {

    private String id;

    private String sequenceNo;

    private String formName;

    private String taskType;

    private String taskTypeName;

    private String businessType;

    private String businessTypeName;

    private String assignDeptId;

    private String assignDeptName;

    private String OrderName;

    private String orderType;

    private String orderTypeName;

    private String formId;

    private Integer dataSize;

    private List<DetailsVO> details;
}
