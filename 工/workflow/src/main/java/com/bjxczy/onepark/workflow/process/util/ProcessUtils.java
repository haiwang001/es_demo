package com.bjxczy.onepark.workflow.process.util;

import com.alibaba.nacos.common.utils.Objects;
import com.bjxczy.onepark.workflow.process.core.constant.ProcessConstant;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;

public class ProcessUtils {
	
	public static String getEventCode(ProcdefPO def, String suffix) {
		String eventCode = ProcessConstant.DEFAULT_EVENT_CODE;
		if (isBusinessProcess(def)) {
			// 业务流程拥有独立消息模板
			eventCode = def.getDeployId();
		}
		eventCode += suffix;
		return eventCode;
	}
	
	public static boolean isBusinessProcess(ProcdefPO def) {
		return Objects.equals(def.getDeployType(), ProcessConstant.TYPE_BUSINESS_PROCESS);
	}
	
}
