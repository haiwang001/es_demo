package com.bjxczy.onepark.workorder.interfaces.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.workorder.pojo.dto.EoVehicleInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @version 1.0.0
 * @ClassName ExcelDataListener
 * @Description 读取数据监听器
 * @createTime 2022年01月09日
 */
@Component
@Slf4j
public class ExcelVehicleDataListener extends AnalysisEventListener<EoVehicleInfoDTO> {

    List<EoVehicleInfoDTO> list = new ArrayList<>();

    @Override
    public void invoke(EoVehicleInfoDTO eoVehicleInfoDTO, AnalysisContext context) {
        log.info("解析到一条数据:{}", JSON.toJSONString(eoVehicleInfoDTO));
        list.add(eoVehicleInfoDTO);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        log.info("所有数据解析完成！");
    }

    public List<EoVehicleInfoDTO> getList() {
        return list;
    }

    public void setList(List<EoVehicleInfoDTO> list){
        this.list=list;
    }

}

