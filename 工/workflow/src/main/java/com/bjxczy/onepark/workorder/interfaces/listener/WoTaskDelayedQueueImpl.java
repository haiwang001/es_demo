package com.bjxczy.onepark.workorder.interfaces.listener;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.core.redisson.constant.DelayedQueueNames;
import com.bjxczy.core.redisson.delayed.DelayedQueueImpl;
import com.bjxczy.core.redisson.delayed.IDelayedQueue;
import com.bjxczy.core.redisson.delayed.RDelayedPayload;
import com.bjxczy.onepark.workorder.mapper.WoTaskInstanceMapper;
import com.bjxczy.onepark.workorder.mapper.WoTaskMapper;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskInstancePO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @ClassName WoTaskDelayedQueueImpl
 * @Description 追踪工单任务的状态
 * @Author zhanghongguo
 * @Date 2023/4/11 10:27
 * @Version 1.0
 */
@Component(DelayedQueueNames.WORKFLOW_ORDER_TASK_QUEUE)
@Slf4j
public class WoTaskDelayedQueueImpl extends DelayedQueueImpl implements IDelayedQueue {
    @Autowired
    private WoTaskMapper woTaskMapper;

    @Autowired
    private WoTaskInstanceMapper woTaskInstanceMapper;

    @Override
    public void execute(RDelayedPayload payload) {
        Integer taskId = payload.getId();
        WoTaskPO woTaskPO = woTaskMapper.selectById(taskId);
        Optional.ofNullable(woTaskPO).ifPresent(wotask -> {
            if ("0".equals(wotask.getTaskStatus())) {//若此时工单仍为已创建状态，状态改成 指派已过期
                WoTaskPO woTask = new WoTaskPO();
                woTask.setId(taskId);
                woTask.setTaskStatus("4");
                woTaskMapper.updateById(woTask);
            }else if ("1".equals(wotask.getTaskStatus())){//若 此时工单状态为执行中 工单任务改成 工单异常状态
                WoTaskPO woTask = new WoTaskPO();
                woTask.setId(taskId);
                woTask.setTaskStatus("7");
                woTaskMapper.updateById(woTask);
                //校验其关联的工单实例 如果是执行中那就改成 工单异常状态
                List<WoTaskInstancePO> woTaskInstancePOS = woTaskInstanceMapper.selectList(Wrappers.<WoTaskInstancePO>lambdaQuery().eq(WoTaskInstancePO::getTaskExecId, taskId));
                Optional.ofNullable(woTaskInstancePOS).ifPresent(woTaskInstanceList -> {
                    for (WoTaskInstancePO woTaskInstancePO : woTaskInstanceList) {
                        if ("1".equals(woTaskInstancePO.getInstStatus())) {
                            woTaskInstancePO.setInstStatus("7");
                            woTaskInstanceMapper.updateById(woTaskInstancePO);
                        }
                    }
                });
            }
        });
    }
}
