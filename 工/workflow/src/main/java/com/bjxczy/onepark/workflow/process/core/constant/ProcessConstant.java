package com.bjxczy.onepark.workflow.process.core.constant;

public interface ProcessConstant {
	
	/**
	 * 业务流程类型
	 */
	String TYPE_BUSINESS_PROCESS = "1";
	
	/**
	 * 自定义流程类型
	 */
	String TYPE_CUSTOM_RPOCES = "2";
	
	
	/**
	 * 用户申请任务ID
	 */
	String SUBMIT_TASK_ID = "retryTask";
	
	/**
	 * 结束节点ID
	 */
	String END_EVENT_ID = "endEvent";
	
	/**
	 * 默认事件编码
	 */
	String DEFAULT_EVENT_CODE = "ProcessEngine";
	
	/**
	 * 审批重试后缀
	 */
	String EVENT_RETRY_SUFFIX = "_Retry";
	
	/**
	 * 审批待办后缀
	 */
	String EVENT_START_SUFFIX = "_Start";
	
	/**
	 * 审批通知后缀
	 */
	String EVENT_END_SUFFIX = "_End";
	
	/**
	 * 流程通过状态
	 */
	Integer APPLY_STATUS_PASS = 1;
	
	/**
	 * 流程关闭状态
	 */
	Integer APPLY_STATUS_CLOSE = 2;
	
	/**
	 * 审批节点标识
	 */
	String CATEGORY_APPROVAL = "approval";
	
	/**
	 * 重试节点标识
	 */
	String CATEGORY_RETRY = "retry";
	
	String VARIABLE_LABEL_SUFFIX = "_label";
	
}
