package com.bjxczy.onepark.workflow.process.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.LogicDelete;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@TableName("wf_hi_task")
@EqualsAndHashCode(callSuper = false)
public class HiTaskPO extends LogicDelete {
	
	private static final long serialVersionUID = -1639498781699947861L;

	@TableId(type = IdType.AUTO)
	private Integer id;
	
	private Integer processId;
	
	private String taskId;
	
	private Integer staffId;
	
	private String employeeNo;
	
	private String category;
	
	private String approvalName;
	
	private Integer approvalStatus;
	
	private String approvalText;
	
	private Date createTime;

	private String operator;
	
}
