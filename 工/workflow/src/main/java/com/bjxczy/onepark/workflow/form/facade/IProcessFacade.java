package com.bjxczy.onepark.workflow.form.facade;

public interface IProcessFacade {

	boolean isBandingProcess(String id);

}
