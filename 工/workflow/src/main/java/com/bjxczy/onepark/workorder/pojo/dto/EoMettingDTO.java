package com.bjxczy.onepark.workorder.pojo.dto;

import com.bjxczy.onepark.workorder.pojo.po.EoMettingGoodsPO;
import com.bjxczy.onepark.workorder.pojo.po.EoMettingPO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EoMettingDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 14:42
 * @Version 1.0
 */
@Data
public class EoMettingDTO extends EoMettingPO {
    private List<EoMettingGoodsPO> eoMettingGoodsPOS;
}
