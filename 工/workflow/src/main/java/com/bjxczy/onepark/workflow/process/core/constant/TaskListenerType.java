package com.bjxczy.onepark.workflow.process.core.constant;

public interface TaskListenerType {
	
	/**
	 * 用户任务监听
	 */
	String USER_TASK_START_LISTENER = "userTaskStartListener";
	
	/**
	 * 流程结束监听
	 */
	String PROCESS_END_LISTENER = "processEndListener";
	
	/**
	 * 用户提交监听
	 */
	String SUBMIT_TASK_LISTENER = "submitTaskListener";

}
