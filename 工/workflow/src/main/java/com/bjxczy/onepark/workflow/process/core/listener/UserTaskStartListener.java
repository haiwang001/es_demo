package com.bjxczy.onepark.workflow.process.core.listener;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.alibaba.excel.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.bjxczy.core.redisson.constant.DelayedQueueNames;
import com.bjxczy.core.redisson.delayed.IDelayedQueue;
import com.bjxczy.core.redisson.delayed.RDelayedPayload;
import com.bjxczy.core.web.message.MessageService;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.workbench.TemplateMessagePayload;
import com.bjxczy.onepark.common.utils.SysUserUtils;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workflow.process.core.constant.ProcessConstant;
import com.bjxczy.onepark.workflow.process.core.constant.TaskListenerType;
import com.bjxczy.onepark.workflow.process.core.constant.TaskVariables;
import com.bjxczy.onepark.workflow.process.entity.ProcdefNodePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.pojo.dto.LabelDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.RejectTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.TaskCompleteDTO;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;
import com.bjxczy.onepark.workflow.process.service.TaskExecService;
import com.bjxczy.onepark.workflow.process.util.ProcessUtils;

@Component(TaskListenerType.USER_TASK_START_LISTENER)
public class UserTaskStartListener implements TaskListener {

	@Autowired
	private StaffFeignClient staffFeignClient;
	@Autowired
	private MessageService messageService;
	@Autowired
	private ProcessDefinitionService procdefService;
	@Autowired
	private RepositoryService repositoryService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private TaskExecService taskExecService;
	@Autowired
	@Qualifier(DelayedQueueNames.WORKFLOW_USER_TASK_QUEUE)
	private IDelayedQueue userTaskDelayedQueue;

	@Override
	public void notify(DelegateTask task) {
		Map<String, Object> variables = task.getVariables();
		ProcessDefinition def = repositoryService.createProcessDefinitionQuery()
				.processDefinitionId(task.getProcessDefinitionId()).singleResult();
		ProcdefNodePO node = procdefService.getActiveProcdefNode(def.getKey(), task.getTaskDefinitionKey());
		// 任务过期处理
		userTaskDelayedQueue.offer(new RDelayedPayload(task.getId()), node.getExpireTime(), TimeUnit.MINUTES);
		// 获取审批人信息
		String assingee = task.getAssignee();
		// 当审批人为空情况下，依据流程节点定义进行操作
		if (StringUtils.isBlank(assingee)) {
			// 查询任务节点定义
			UserInformation user = new UserInformation();
			user.setStaffName("系统操作");
			switch (node.getAssigneeIsNull()) {
			case "auto_complete": // 自动通过
				taskExecService.complete(task.getId(), new TaskCompleteDTO("系统自动通过"), user);
				break;
			case "auto_reject": // 自动拒绝
				taskExecService.reject(task.getId(), new RejectTaskDTO("close", "系统自动拒绝"), user);
				break;
			case "forward": // 转交指定人员
				LabelDTO label = JSON.parseObject(node.getAssigneeValue(), LabelDTO.class);
				assingee = label.getId().toString();
				taskService.setAssignee(task.getId(), assingee);
				break;
			default:
				break;
			}
		}
		if (StringUtils.isNotBlank(assingee)) {
			// 获取申请人信息
			String staffId = (String) variables.get(TaskVariables.VAR_INITIATOR_KEY);
			StaffInformation staff = staffFeignClient.findById(Integer.parseInt(staffId));
			variables.put("start_staff_name", staff.getFldName());
			variables.put("start_dept_name", SysUserUtils.getDept2Name(staff));
			// 获取流程定义
			ProcdefPO po = procdefService.getByDeployId(def.getKey());
			variables.put("procdefName", po.getName());
			// 发送模板消息
			String eventCode = ProcessUtils.getEventCode(po, ProcessConstant.EVENT_START_SUFFIX);
			TemplateMessagePayload payload = new TemplateMessagePayload(Integer.parseInt(assingee), task.getId(),
					eventCode, variables);
			messageService.pushTemplateMessage(payload);
		}
	}

	private static final long serialVersionUID = -4155404224482540341L;

}
