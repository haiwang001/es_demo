package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 酒店EO单表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="EoHotel对象", description="酒店EO单表")
@TableName("eo_hotel")
public class EoHotelPO{

    @ApiModelProperty(value = "主键Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "EO单Id")
    private String eoId;

    @ApiModelProperty(value = "选择园区（字典：park）")
    private String park;

    @ApiModelProperty(value = "备注")
    private String remark;


}
