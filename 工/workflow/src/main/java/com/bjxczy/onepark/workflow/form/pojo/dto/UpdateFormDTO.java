package com.bjxczy.onepark.workflow.form.pojo.dto;

import java.util.List;
import java.util.Map;

import com.bjxczy.onepark.workflow.form.entity.FormItemPO;

import lombok.Data;

@Data
public class UpdateFormDTO {
	
	private String name;
	
	private String remark;
	
	private Map<String, Object> extra;
	
	private List<FormItemPO> formItem;

}
