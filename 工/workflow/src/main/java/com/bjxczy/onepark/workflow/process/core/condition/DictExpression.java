package com.bjxczy.onepark.workflow.process.core.condition;

import java.util.Map;

import com.bjxczy.onepark.workflow.process.core.constant.TaskVariables;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DictExpression {

	private String bean;

	private String method;
	
	public DictExpression(Map<String, Object> extra) {
		this.bean = extra.get("bean").toString();
		this.method = extra.get("method").toString();
	}

	public String toConditionExpression() {
		return "${" + bean + "." + method + "(" + TaskVariables.VAR_PROC_DEF_ID + ","
				+ TaskVariables.VAR_INITIATOR_KEY + ")}";
	}

}
