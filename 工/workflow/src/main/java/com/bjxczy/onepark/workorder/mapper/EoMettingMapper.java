package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workorder.pojo.po.EoMettingPO;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 酒店EO单-会议表 Mapper 接口
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Mapper
public interface EoMettingMapper extends BaseMapper<EoMettingPO> {

}
