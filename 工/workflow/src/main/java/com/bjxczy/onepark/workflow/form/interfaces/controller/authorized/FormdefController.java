package com.bjxczy.onepark.workflow.form.interfaces.controller.authorized;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.log.LogParam;
import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;
import com.bjxczy.onepark.workflow.form.pojo.dto.CreateFormDTO;
import com.bjxczy.onepark.workflow.form.pojo.dto.QueryFormPageDTO;
import com.bjxczy.onepark.workflow.form.pojo.dto.UpdateFormDTO;
import com.bjxczy.onepark.workflow.form.service.FormdefService;

@ApiResourceController
@RequestMapping("/form")
public class FormdefController {

	@Autowired
	private FormdefService formdefService;

	@GetMapping(value = "/page", name = "查询表单（分页）")
	@SysLog(operationType = OperationType.SELECT, operation = "查询表单列表")
	public IPage<FormdefPO> page(QueryFormPageDTO dto) {
		return formdefService.pageList(dto);
	}

	@PostMapping(name = "创建表单")
	@SysLog(operationType = OperationType.INSERT, operation = "创建表单：${dto.name}")
	public void createForm(@RequestBody CreateFormDTO dto) {
		formdefService.create(dto);
	}

	@PutMapping(value = "/{id}", name = "更新表单")
	@SysLog(operationType = OperationType.UPDATE, operation = "更新表单：${dto.name}")
	public void updateForm(@PathVariable("id") String id, @RequestBody UpdateFormDTO dto) {
		formdefService.update(id, dto);
	}

	@DeleteMapping(value = "/{id}", name = "删除表单")
	@SysLog(operationType = OperationType.DELETE, operation = "删除表单：${ctx.name}")
	public void deleteForm(@PathVariable("id") String id) {
		formdefService.removeForm(id);
	}

	@PutMapping(value = "/{id}/status", name = "启用/禁用表单")
	@SysLog(operationType = OperationType.UPDATE, operation = "#if($enable==1)启用#else禁用#end表单：${ctx.name}")
	public void updateStatus(@PathVariable("id") String id, @RequestParam("enable") @LogParam Integer enable) {
		formdefService.updateStatus(id, enable);
	}

}
