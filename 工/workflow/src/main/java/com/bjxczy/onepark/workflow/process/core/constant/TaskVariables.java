package com.bjxczy.onepark.workflow.process.core.constant;

public interface TaskVariables {
	
	/**
	 * 审批任务：assingee后缀
	 */
	String VAR_TASK_ASSIGNEE_SUFFIX = "_assignee";
	
	/**
	 * 流程定义KEY变量名
	 */
	String VAR_PROC_DEF_ID = "proc_def_id";
	
	/**
	 * 流程启动人变量名
	 */
	String VAR_INITIATOR_KEY = "startStaffId";
	
	/**
	 * 驳回次数变量名称
	 */
	String VAR_RETRY_CNT = "retryCnt";
	
	/**
	 * 提前结束
	 */
	String VAR_EARLY_END = "wf_early_end";
	
	/**
	 * 提前结束消息
	 */
	String VAR_EARLY_END_MESSAGE = "wf_early_end_message";
	
	/**
	 * 提交时间
	 */
	String VAR_SUBMIT_TIME = "val_submit_time";
}
