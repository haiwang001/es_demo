package com.bjxczy.onepark.workflow.process.pojo.vo;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.form.entity.FormdefPO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class ProcdefActiveVO extends ProcdefPO {
	
	private static final long serialVersionUID = 9102186696830199377L;
	
	private FormdefPO formdef;
	
	private List<FormItemPO> formItems;

	public ProcdefActiveVO(ProcdefPO po) {
		BeanUtils.copyProperties(po, this);
	}

}
