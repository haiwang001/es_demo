package com.bjxczy.onepark.workorder.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * @ClassName WoDataOverviewDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/4/4 17:44
 * @Version 1.0
 */
@Data
public class WoDataOverviewDTO {

    private List<String> statusList;

    private String dateType;//y:1 m:2 d:3

    private String startTime;//2023-01-01

    private String endTime;

    private List<String> dateList;

    private String status;

    //部门Id
    private List<Integer> assignDeptIds;

    //受理人
    private List<Integer> assigneeIds;

    //工单Id
    private List<String> woIds;

}
