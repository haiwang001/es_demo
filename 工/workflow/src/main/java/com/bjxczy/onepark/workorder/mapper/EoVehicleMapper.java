package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workorder.pojo.po.EoVehiclePO;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 车辆EO单表 Mapper 接口
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-05-22
 */
@Mapper
public interface EoVehicleMapper extends BaseMapper<EoVehiclePO> {

}
