package com.bjxczy.onepark.workorder.feign;

import com.bjxczy.core.feign.client.pcs.FileFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @ClassName PcsFeignClient
 * @Description
 * @Author zhanghongguo
 * @Date 2023/6/7 15:13
 * @Version 1.0
 */
@FeignClient("pcs")
public interface PcsFeignClient extends FileFeignClient {

}
