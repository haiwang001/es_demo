package com.bjxczy.onepark.workflow.process.pojo.vo;

import java.util.List;

import com.bjxczy.onepark.workflow.process.pojo.dto.CandidateStaff;

import lombok.Data;

@Data
public class CandidateVO {

	private String name;

	private String formKey;

	private List<CandidateStaff> candidates;

}
