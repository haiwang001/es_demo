package com.bjxczy.onepark.workorder.pojo.dto;

import io.swagger.models.auth.In;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EoExecuteDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/25 20:03
 * @Version 1.0
 */
@Data
public class EoExecuteDTO {

    private String eoId;

    private String mettingIds;
}
