package com.bjxczy.onepark.workorder.pojo.vo;

import com.bjxczy.onepark.workorder.pojo.po.EoVehicleInfoPO;
import com.bjxczy.onepark.workorder.pojo.po.EoVehiclePO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EoVehicleVO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 16:02
 * @Version 1.0
 */
@Data
public class EoVehicleVO extends EoVehiclePO {
    private List<EoVehicleInfoPO> eoVehicleInfoPOS;
}
