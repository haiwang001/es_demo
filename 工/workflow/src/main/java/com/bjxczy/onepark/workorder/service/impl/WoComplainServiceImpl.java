package com.bjxczy.onepark.workorder.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workorder.mapper.WoComplainMapper;
import com.bjxczy.onepark.workorder.pojo.dto.WoComplainDTO;
import com.bjxczy.onepark.workorder.pojo.enums.WoComplainStatus;
import com.bjxczy.onepark.workorder.pojo.po.WoComplainExcelPO;
import com.bjxczy.onepark.workorder.pojo.po.WoComplainPO;
import com.bjxczy.onepark.workorder.pojo.vo.WoComplainVO;
import com.bjxczy.onepark.workorder.service.WoComplainService;
import com.bjxczy.onepark.workorder.utils.ExcelUtils;
import com.bjxczy.onepark.workorder.utils.ListConverterUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @ClassName WoComplainService
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/27 16:28
 * @Version 1.0
 */
@Service
public class WoComplainServiceImpl extends ServiceImpl<WoComplainMapper, WoComplainPO>implements WoComplainService {


    @Autowired
    private ExcelUtils excelUtils;

    @Override
    public void createWoComplain(WoComplainDTO woComplainDTO, UserInformation user) {
        WoComplainPO woComplainPO = new WoComplainPO();
        BeanUtils.copyProperties(woComplainDTO,woComplainPO);
        woComplainPO.setSequenceNo(UUID.randomUUID().toString());
        Optional.ofNullable(user).ifPresent(u->{
            woComplainPO.setInitiatorId(u.getStaffId());
            woComplainPO.setInitiatorName(u.getStaffName());
        });
        woComplainPO.setStatus(WoComplainStatus.NOFEEDBACK.getValue());
        this.baseMapper.insert(woComplainPO);
    }

    @Override
    public Page<WoComplainVO> getComplainList(WoComplainDTO woComplainDTO) {
        Page<WoComplainVO> complainList = baseMapper.getComplainList(woComplainDTO.toPage(), woComplainDTO);
        return complainList;
    }

    @Override
    public void feedBackWoComplain(WoComplainDTO woComplainDTO) {
        WoComplainPO woComplainPO = new WoComplainPO();
        BeanUtils.copyProperties(woComplainDTO,woComplainPO);
        woComplainPO.setStatus(WoComplainStatus.FEEDBACK.getValue());
        this.baseMapper.updateById(woComplainPO);
    }

    @Override
    public void delWoComplain(Integer id) {
        this.baseMapper.deleteById(id);
    }

    @Override
    public void export(HttpServletResponse response) {
        List<WoComplainPO> woComplainPOS = this.baseMapper.selectList(Wrappers.emptyWrapper());
        ListConverterUtils listConverterUtils = new ListConverterUtils(WoComplainExcelPO.class);
        woComplainPOS.forEach(woComplainPO -> {
            if ("0".equals(woComplainPO.getStatus())){
                woComplainPO.setStatus("待反馈");
            }else {
                woComplainPO.setStatus("已反馈");
            }
        });
        List list = listConverterUtils.converterList(woComplainPOS);
        try {
            excelUtils.export(response,"投诉列表导出",list,WoComplainExcelPO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<WoComplainVO> getComplainMobList(WoComplainDTO woComplainDTO) {
        List<WoComplainVO> complainList = baseMapper.getComplainMobList(woComplainDTO);
        return complainList;
    }
}
