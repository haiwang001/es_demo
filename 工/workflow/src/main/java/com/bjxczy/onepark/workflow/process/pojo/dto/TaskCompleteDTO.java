package com.bjxczy.onepark.workflow.process.pojo.dto;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskCompleteDTO {
	
	private String approvalText;
	
	private Map<String, Object> variables;

	public TaskCompleteDTO(String approvalText) {
		super();
		this.approvalText = approvalText;
	}

}
