package com.bjxczy.onepark.workorder.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.workorder.pojo.dto.WoOrderdefDto;
import com.bjxczy.onepark.workorder.pojo.vo.WoOrderdefVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface WoOrderdefService {
    void addWoOrder(WoOrderdefDto woOrderdefDto);

    void editWoOrder(WoOrderdefDto woOrderdefDto);

    void deleteWoOrder(List<String> id);

    void exportWoOrder(HttpServletResponse response, WoOrderdefDto woOrderdefDto);

    Page<WoOrderdefVO> list(WoOrderdefDto woOrderdefDto);

    List<WoOrderdefVO> pullList();

    List<Map<String, Object>> mobPullList(String id);
}
