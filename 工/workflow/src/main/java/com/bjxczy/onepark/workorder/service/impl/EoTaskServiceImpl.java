package com.bjxczy.onepark.workorder.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelPicUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.message.MessageService;
import com.bjxczy.core.web.workflow.WorkOrderService;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.authority.AreaInformation;
import com.bjxczy.onepark.common.model.car.ParkingLotInfo;
import com.bjxczy.onepark.common.model.common.DictInformation;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.workbench.TemplateMessagePayload;
import com.bjxczy.onepark.common.model.workflow.*;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workorder.constant.EoOrderConstants;
import com.bjxczy.onepark.workorder.constant.EoTaskStatusConstants;
import com.bjxczy.onepark.workorder.feign.BmpCertificateFeignClient;
import com.bjxczy.onepark.workorder.feign.PcsFeignClient;
import com.bjxczy.onepark.workorder.feign.SpaceFeignClient;
import com.bjxczy.onepark.workorder.interfaces.listener.ExcelMettingGoodsDataListener;
import com.bjxczy.onepark.workorder.interfaces.listener.ExcelVehicleDataListener;
import com.bjxczy.onepark.workorder.interfaces.listener.ExcelVisitDataListener;
import com.bjxczy.onepark.workorder.interfaces.publisher.EoEventPublisher;
import com.bjxczy.onepark.workorder.mapper.*;
import com.bjxczy.onepark.workorder.pojo.dto.*;
import com.bjxczy.onepark.workorder.pojo.po.*;
import com.bjxczy.onepark.workorder.pojo.vo.*;
import com.bjxczy.onepark.workorder.service.EoTaskService;
import com.bjxczy.onepark.workorder.utils.DownloadFileUtils;
import com.bjxczy.onepark.workorder.utils.ExcelUtils;
import org.apache.http.entity.ContentType;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName EoTaskServiceImpl
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 14:31
 * @Version 1.0
 */
@Service
public class EoTaskServiceImpl extends ServiceImpl<EoTaskMapper, EoTaskPO> implements EoTaskService {


    @Autowired
    private PcsFeignClient pcsFeignClient;
    private final Integer inAndOutParkRows = 999;

    @Autowired
    private SpaceFeignClient spaceFeignClient;

    @Autowired
    private BmpCertificateFeignClient bmpCertificateFeignClient;

    @Autowired
    private EoEventPublisher eoEventPublisher;

    @Autowired
    private EoSettlementMapper eoSettlementMapper;

    @Autowired
    private WorkOrderService workOrderService;

    @Autowired
    private ExcelMettingGoodsDataListener excelMettingGoodsDataListener;

    @Autowired
    private ExcelVisitDataListener excelVisitDataListener;

    @Autowired
    private ExcelVehicleDataListener excelVehicleDataListener;

    @Autowired
    private EoVisitInfoMapper eoVisitInfoMapper;

    @Autowired
    private WoOrderdefMapper woOrderdefMapper;

    @Autowired
    private MessageService messageService;

    @Autowired
    private EoVehicleInfoMapper eoVehicleInfoMapper;

    @Autowired
    private EoMettingGoodsMapper eoMettingGoodsMapper;

    @Autowired
    private DictService dictService;

    @Autowired
    private EoHotelMapper eoHotelMapper;

    @Autowired
    private EoAccommodationMapper eoAccommodationMapper;

    @Autowired
    private EoMettingMapper eoMettingMapper;

    @Autowired
    private EoCateringMapper eoCateringMapper;

    @Autowired
    private EoCateringScheduleMapper eoCateringScheduleMapper;

    @Autowired
    private EoTaskMapper eoTaskMapper;

    @Autowired
    private EoVehicleMapper eoVehicleMapper;

    @Autowired
    private StaffFeignClient staffFeignClient;

    @Override
    public void addEoTask(EoTaskDTO eoTaskDTO, UserInformation user) {
        EoTaskPO eoTaskPO = new EoTaskPO();
        BeanUtils.copyProperties(eoTaskDTO, eoTaskPO);
        // 规则 ：SCXS-时间戳+eo单数（默认展示三位不够补0）
        Integer count = this.baseMapper.selectCount(Wrappers.emptyWrapper());
        eoTaskPO.setId("SCXS-"+DateUtil.format(new Date(),"yyyyMMddHHmmss")+String.format("%03d",count));
        eoTaskPO.setStatus(EoTaskStatusConstants.CHECK_STATUS);
        eoTaskPO.setInitiatorId(user.getStaffId());
        this.baseMapper.insert(eoTaskPO);
        // 1、酒店
        Optional.ofNullable(eoTaskDTO.getEoHotelDTO()).ifPresent(eoHotelDTO -> {
            EoHotelPO eoHotelPO = new EoHotelPO();
            BeanUtils.copyProperties(eoHotelDTO, eoHotelPO);
            eoHotelPO.setEoId(eoTaskPO.getId());
            eoHotelMapper.insert(eoHotelPO);
            //住宿相关
            Optional.ofNullable(eoHotelDTO.getEoAccommodationPOS()).ifPresent(eoAccommodationPOS -> {
                eoAccommodationPOS.forEach(eoAccommodationPO -> {
                    eoAccommodationPO.setHotelId(eoHotelPO.getId());
                    eoAccommodationMapper.insert(eoAccommodationPO);
                });
            });
            //会议相关
            Optional.ofNullable(eoHotelDTO.getEoMettingPOS()).ifPresent(eoMettingDTOS -> {
                eoMettingDTOS.forEach(eoMettingDTO -> {
                    EoMettingPO eoMettingPO = new EoMettingPO();
                    BeanUtils.copyProperties(eoMettingDTO, eoMettingPO);
                    eoMettingPO.setHotelId(eoHotelPO.getId());
                    eoMettingMapper.insert(eoMettingPO);
                    //联动会议模块
                    AddEoMettingInfo addEoMettingInfo = new AddEoMettingInfo();
                    BeanUtils.copyProperties(eoMettingPO,addEoMettingInfo);
                    addEoMettingInfo.setStaffId(user.getStaffId());
                    addEoMettingInfo.setDept(eoTaskDTO.getDept());
                    addEoMettingInfo.setPersonCount(eoTaskDTO.getPersonCount());
                    eoEventPublisher.addMettingInfo(addEoMettingInfo);
                });
            });
        });
        // 2、餐饮
        Optional.ofNullable(eoTaskDTO.getEoCateringDTO()).ifPresent(eoCateringDTO -> {
            EoCateringPO eoCateringPO = new EoCateringPO();
            BeanUtils.copyProperties(eoCateringDTO, eoCateringPO);
            eoCateringPO.setEoId(eoTaskPO.getId());
            eoCateringMapper.insert(eoCateringPO);
            //餐饮时刻
            Optional.ofNullable(eoCateringDTO.getEoCateringSchedulePOS()).ifPresent(eoCateringSchedulePOS -> {
                eoCateringSchedulePOS.forEach(eoCateringSchedulePO -> {
                    eoCateringSchedulePO.setCateringId(eoCateringPO.getId());
                    eoCateringScheduleMapper.insert(eoCateringSchedulePO);
                });
            });
        });
        // 3、安全管理部 车辆信息
        Optional.ofNullable(eoTaskDTO.getEoVehicleDTO()).ifPresent(eoVehicleDTO -> {
            EoVehiclePO eoVehiclePO = new EoVehiclePO();
            BeanUtils.copyProperties(eoVehicleDTO, eoVehiclePO);
            eoVehiclePO.setEoId(eoTaskPO.getId());
            eoVehicleMapper.insert(eoVehiclePO);
        });
    }

    @Override
    public IPage<EoTaskVO> getEoTaskByPage(EoTaskDTO eoTaskDTO) {
        Page<EoTaskPO> data = this.page(eoTaskDTO.toPage(), Wrappers.<EoTaskPO>lambdaQuery()
                .like(StrUtil.isNotBlank(eoTaskDTO.getName()), EoTaskPO::getName, eoTaskDTO.getName())
                .between(StrUtil.isNotBlank(eoTaskDTO.getSSTime()) && StrUtil.isNotBlank(eoTaskDTO.getESTime()), EoTaskPO::getStartTime, eoTaskDTO.getSSTime(), eoTaskDTO.getESTime())
                .between(StrUtil.isNotBlank(eoTaskDTO.getSETime()) && StrUtil.isNotBlank(eoTaskDTO.getEETime()), EoTaskPO::getEndTime, eoTaskDTO.getSETime(), eoTaskDTO.getEETime())
                .orderByDesc(EoTaskPO::getCreateTime));
        return data.convert(eoTaskPO -> {
            EoTaskVO eoTaskVO = new EoTaskVO();
            BeanUtils.copyProperties(eoTaskPO, eoTaskVO);

            //1、酒店
            Optional.ofNullable(eoHotelMapper.selectOne(Wrappers.<EoHotelPO>lambdaQuery().eq(EoHotelPO::getEoId, eoTaskPO.getId()))).ifPresent(eoHotelPO -> {
                EoHotelVO eoHotelVO = new EoHotelVO();
                BeanUtils.copyProperties(eoHotelPO, eoHotelVO);
                // 园区字典翻译（park）
                eoHotelVO.setParkName(dictService.getLabel("park", eoHotelVO.getPark()));
                // 住宿
                List<EoAccommodationPO> eoAccommodationPOS = eoAccommodationMapper.selectList(Wrappers.<EoAccommodationPO>lambdaQuery().eq(EoAccommodationPO::getHotelId, eoHotelPO.getId()));
                List<EoAccommodationVO> eoAccommodationVOS = new ArrayList<>();
                for (EoAccommodationPO eoAccommodationPO : eoAccommodationPOS) {
                    EoAccommodationVO eoAccommodationVO = new EoAccommodationVO();
                    BeanUtils.copyProperties(eoAccommodationPO, eoAccommodationVO);
                    // 房型字典翻译（roomType）
                    eoAccommodationVO.setRoomTypeName(dictService.getLabel("roomType", eoAccommodationVO.getRoomType()));
                    eoAccommodationVOS.add(eoAccommodationVO);
                }
                eoHotelVO.setEoAccommodationVOS(eoAccommodationVOS);

                // 会议
                List<EoMettingVO> eoMettingVOS = new ArrayList<>();
                for (EoMettingPO eoMettingPO : eoMettingMapper.selectList(Wrappers.<EoMettingPO>lambdaQuery().eq(EoMettingPO::getHotelId, eoHotelPO.getId()))) {
                    EoMettingVO eoMettingVO = new EoMettingVO();
                    BeanUtils.copyProperties(eoMettingPO, eoMettingVO);
                    // List<EoMettingGoodsPO> eoMettingGoodsPOS = eoMettingGoodsMapper.selectList(Wrappers.<EoMettingGoodsPO>lambdaQuery().eq(EoMettingGoodsPO::getMettingId, eoMettingVO.getId()));
                    // eoMettingVO.setEoMettingGoodsPOS(eoMettingGoodsPOS);
                    eoMettingVOS.add(eoMettingVO);
                }
                eoHotelVO.setEoMettingVOS(eoMettingVOS);
                eoTaskVO.setEoHotelVO(eoHotelVO);
            });
            //2、餐饮
            Optional.ofNullable(eoCateringMapper.selectOne(Wrappers.<EoCateringPO>lambdaQuery().eq(EoCateringPO::getEoId, eoTaskPO.getId()))).ifPresent(eoCateringPO -> {
                EoCateringVO eoCateringVO = new EoCateringVO();
                BeanUtils.copyProperties(eoCateringPO, eoCateringVO);
                //就餐形式（字典翻译：diningForm）
                eoCateringVO.setDiningFormName(dictService.getLabel("diningForm", eoCateringVO.getDiningForm()));
                //餐饮时刻表
                eoCateringVO.setEoCateringSchedulePOS(eoCateringScheduleMapper.selectList(Wrappers.<EoCateringSchedulePO>lambdaQuery().eq(EoCateringSchedulePO::getCateringId, eoCateringVO.getId())));
                eoTaskVO.setEoCateringVO(eoCateringVO);
            });
            //3、安全管理部（车辆相关）
            Optional.ofNullable(eoVehicleMapper.selectOne(Wrappers.<EoVehiclePO>lambdaQuery().eq(EoVehiclePO::getEoId, eoTaskPO.getId()))).ifPresent(eoVehiclePO -> {
                EoVehicleVO eoVehicleVO = new EoVehicleVO();
                BeanUtils.copyProperties(eoVehiclePO, eoVehicleVO);
                // 车辆相关信息
                // eoVehicleVO.setEoVehicleInfoPOS(eoVehicleInfoMapper.selectList(Wrappers.<EoVehicleInfoPO>lambdaQuery().eq(EoVehicleInfoPO::getVehicleId, eoVehicleVO.getId())));
                eoTaskVO.setEoVehicleVO(eoVehicleVO);
            });

            return eoTaskVO;
        });
    }

    @Override
    public void editEoTask(EoTaskDTO eoTaskDTO) {
        EoTaskPO eoTaskPO = new EoTaskPO();
        BeanUtils.copyProperties(eoTaskDTO,eoTaskPO);
        // EO单状态改为待审批
        eoTaskPO.setStatus("0");
        eoTaskMapper.updateById(eoTaskPO);
        Optional.ofNullable(eoTaskDTO.getEoHotelDTO()).ifPresent(eoHotelDTO -> {
            EoHotelPO eoHotelPO = new EoHotelPO();
            BeanUtils.copyProperties(eoHotelDTO, eoHotelPO);
            eoHotelMapper.updateById(eoHotelPO);
            Optional.ofNullable(eoHotelDTO.getEoAccommodationPOS()).ifPresent(eoAccommodationPOS -> {
                eoAccommodationPOS.forEach(eoAccommodationPO -> {
                    eoAccommodationMapper.updateById(eoAccommodationPO);
                });
            });
        });
        Optional.ofNullable(eoTaskDTO.getEoMettingDTO()).ifPresent(eoMettingDTO -> {
            EoMettingPO eoMettingPO = new EoMettingPO();
            BeanUtils.copyProperties(eoMettingDTO, eoMettingPO);
            eoMettingMapper.updateById(eoMettingPO);
        });
        Optional.ofNullable(eoTaskDTO.getEoCateringDTO()).ifPresent(eoCateringDTO -> {
            EoCateringPO eoCateringPO = new EoCateringPO();
            BeanUtils.copyProperties(eoCateringDTO, eoCateringPO);
            eoCateringMapper.updateById(eoCateringPO);
            Optional.ofNullable(eoCateringDTO.getEoCateringSchedulePOS()).ifPresent(eoCateringSchedulePOS -> {
                eoCateringSchedulePOS.forEach(eoCateringSchedulePO -> {
                    eoCateringScheduleMapper.updateById(eoCateringSchedulePO);
                });
            });
        });
        Optional.ofNullable(eoTaskDTO.getEoVehicleDTO()).ifPresent(eoVehicleDTO -> {
            EoVehiclePO eoVehiclePO = new EoVehiclePO();
            BeanUtils.copyProperties(eoVehicleDTO, eoVehiclePO);
            eoVehicleMapper.updateById(eoVehiclePO);
        });
    }

    @Override
    public void checkEoTask(EoTaskDTO eoTaskDTO) {
        EoTaskPO eoTaskPO = new EoTaskPO();
        eoTaskPO.setId(eoTaskDTO.getId());
        eoTaskPO.setStatus(eoTaskDTO.getStatus());
        eoTaskPO.setRejectReason(eoTaskDTO.getRejectReason());
        if (eoTaskDTO.getStatus().equals(EoTaskStatusConstants.REJECT_STATUS)) {
            //审批驳回 发送驳回消息给发起人
            Optional.ofNullable(eoTaskMapper.selectById(eoTaskDTO.getId())).ifPresent(eoTaskPOInfo -> {
                Optional.ofNullable(eoTaskPOInfo.getInitiatorId()).ifPresent(initiatorId -> {
                    Optional.ofNullable(staffFeignClient.findById(initiatorId)).ifPresent(staffInfo -> {
                        String eventCode = EoOrderConstants.DEFAULT_EVENT_CODE + EoOrderConstants.EVENT_REJECT_SUFFIX;
                        pushMessage(eoTaskPOInfo, staffInfo, initiatorId, eoTaskDTO, eventCode);
                    });
                });
            });
        } else {
            // 审批通过
            Optional.ofNullable(eoTaskMapper.selectById(eoTaskDTO.getId())).ifPresent(eoTaskPOInfo -> {
                Optional.ofNullable(eoTaskPOInfo.getInitiatorId()).ifPresent(initiatorId -> {
                    Optional.ofNullable(staffFeignClient.findById(initiatorId)).ifPresent(staffInfo -> {
                        //1、发送EO单待办给发起人
                        String eventCo = EoOrderConstants.DEFAULT_EVENT_CODE + EoOrderConstants.EVENT_EXECUTE_SUFFIX;
                        HashMap<String, Object> params = new HashMap<>();
                        params.put("eoName", eoTaskPOInfo.getName());
                        params.put("createTime", DateUtil.formatDateTime(eoTaskPOInfo.getCreateTime()));
                        messageService.pushTemplateMessage(new TemplateMessagePayload(initiatorId, eoTaskDTO.getId(), eventCo, params));
                        // 2、发送消息给工单配置的主管
                        String eoType = eoTaskPOInfo.getEoType();
                        if (eoType.contains("1")) {
                            //酒店
                            Optional.ofNullable(woOrderdefMapper.selectById(EoOrderConstants.HOTEL_ORDER)).ifPresent(woOrderdefPO -> {
                                String eventCode = EoOrderConstants.DEFAULT_EVENT_CODE + EoOrderConstants.EVENT_HOTEL_SUFFIX;
                                pushMessage(eoTaskPOInfo, staffInfo, woOrderdefPO.getOwnerId(), eoTaskDTO, eventCode);
                            });
                        }
                        if (eoType.contains("2")) {
                            //餐饮
                            Optional.ofNullable(woOrderdefMapper.selectById(EoOrderConstants.CATERING_ORDER)).ifPresent(woOrderdefPO -> {
                                String eventCode = EoOrderConstants.DEFAULT_EVENT_CODE + EoOrderConstants.EVENT_CATERING_SUFFIX;
                                pushMessage(eoTaskPOInfo, staffInfo, woOrderdefPO.getOwnerId(), eoTaskDTO, eventCode);
                            });
                        }
                        if (eoType.contains("3")) {
                            //会议
                            Optional.ofNullable(woOrderdefMapper.selectById(EoOrderConstants.METTING_ORDER)).ifPresent(woOrderdefPO -> {
                                String eventCode = EoOrderConstants.DEFAULT_EVENT_CODE + EoOrderConstants.EVENT_METTING_SUFFIX;
                                pushMessage(eoTaskPOInfo, staffInfo, woOrderdefPO.getOwnerId(), eoTaskDTO, eventCode);
                            });
                        }
                        //财务
                        Optional.ofNullable(woOrderdefMapper.selectById(EoOrderConstants.FINANCE_ORDER)).ifPresent(woOrderdefPO -> {
                            String eventCode = EoOrderConstants.DEFAULT_EVENT_CODE + EoOrderConstants.EVENT_FINANCE_SUFFIX;
                            pushMessage(eoTaskPOInfo, staffInfo, woOrderdefPO.getOwnerId(), eoTaskDTO, eventCode);
                        });
                        //来访
                        Optional.ofNullable(woOrderdefMapper.selectById(EoOrderConstants.VISITOR_ORDER)).ifPresent(woOrderdefPO -> {
                            String eventCode = EoOrderConstants.DEFAULT_EVENT_CODE + EoOrderConstants.EVENT_VISIT_SUFFIX;
                            pushMessage(eoTaskPOInfo, staffInfo, woOrderdefPO.getOwnerId(), eoTaskDTO, eventCode);
                        });
                    });
                });
            });
        }
        eoTaskMapper.updateById(eoTaskPO);
    }

    private void pushMessage(EoTaskPO eoTaskPOInfo, StaffInformation staffInfo, Integer initiatorId, EoTaskDTO eoTaskDTO, String eventCode) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("deptName", staffInfo.getDeptNames().get(0).getDeptName());
        params.put("initiatorName", staffInfo.getFldName());
        params.put("eoName", eoTaskPOInfo.getName());
        params.put("startTime", eoTaskPOInfo.getStartTime());
        params.put("createTime", DateUtil.formatDateTime(eoTaskPOInfo.getCreateTime()));
        params.put("eoId",eoTaskPOInfo.getId());
        messageService.pushTemplateMessage(new TemplateMessagePayload(initiatorId, eoTaskDTO.getId(), eventCode, params));
    }
    @Override
    public void delEoTask(String id) {
        // eo单任务删除
        eoTaskMapper.deleteById(id);
        // eo_hotel删除
        eoHotelMapper.delete(Wrappers.<EoHotelPO>lambdaQuery()
                .eq(EoHotelPO::getEoId, id));
        Optional.ofNullable(eoHotelMapper.selectOne(Wrappers.<EoHotelPO>lambdaQuery()
                .eq(EoHotelPO::getEoId, id))).ifPresent(eoHotelPO -> {
            // eo_accommodation删除
            eoAccommodationMapper.delete(Wrappers.<EoAccommodationPO>lambdaQuery().eq(EoAccommodationPO::getHotelId, eoHotelPO.getId()));
            // eo_metting删除
            eoMettingMapper.delete(Wrappers.<EoMettingPO>lambdaQuery().eq(EoMettingPO::getHotelId, eoHotelPO.getId()));
            Optional.ofNullable(eoMettingMapper.selectList(Wrappers.<EoMettingPO>lambdaQuery().eq(EoMettingPO::getHotelId, eoHotelPO.getId()))).ifPresent(eoMettingPOS -> {
                eoMettingPOS.forEach(eoMettingPO -> {
                    // eo_metting_goods删除
                    eoMettingGoodsMapper.delete(Wrappers.<EoMettingGoodsPO>lambdaQuery().eq(EoMettingGoodsPO::getMettingId, eoMettingPO.getId()));
                });
            });
            // eo_catering删除
            eoCateringMapper.delete(Wrappers.<EoCateringPO>lambdaQuery().eq(EoCateringPO::getEoId, id));
            Optional.ofNullable(eoCateringMapper.selectOne(Wrappers.<EoCateringPO>lambdaQuery().eq(EoCateringPO::getEoId, id))).ifPresent(eoCateringPO -> {
                // eo_catering_schedule删除
                eoCateringScheduleMapper.delete(Wrappers.<EoCateringSchedulePO>lambdaQuery().eq(EoCateringSchedulePO::getCateringId, eoCateringPO.getId()));
            });
            // eo_vehicle删除
            eoVehicleMapper.delete(Wrappers.<EoVehiclePO>lambdaQuery().eq(EoVehiclePO::getEoId, id));
            Optional.ofNullable(eoVehicleMapper.selectOne(Wrappers.<EoVehiclePO>lambdaQuery().eq(EoVehiclePO::getEoId, id))).ifPresent(eoVehiclePO -> {
                // eo_vehicle_info删除
                eoVehicleInfoMapper.delete(Wrappers.<EoVehicleInfoPO>lambdaQuery().eq(EoVehicleInfoPO::getVehicleId, eoVehiclePO.getId()));
            });
            // eo_visit_info删除
            eoVisitInfoMapper.delete(Wrappers.<EoVisitInfoPO>lambdaQuery()
                    .eq(EoVisitInfoPO::getEoId,id));
        });
    }

    @Override
    public EoTaskVO getCateringInfo(String eoId) {
        EoTaskPO eoTaskPO = this.baseMapper.selectOne(Wrappers.<EoTaskPO>lambdaQuery().eq(EoTaskPO::getId, eoId));
        EoTaskVO eoTaskVO = new EoTaskVO();
        Optional.ofNullable(eoTaskPO).ifPresent(eoTaskPO1 -> {
            BeanUtils.copyProperties(eoTaskPO, eoTaskVO);
            //1、酒店
            Optional.ofNullable(eoHotelMapper.selectOne(Wrappers.<EoHotelPO>lambdaQuery().eq(EoHotelPO::getEoId, eoTaskPO.getId()))).ifPresent(eoHotelPO -> {
                EoHotelVO eoHotelVO = new EoHotelVO();
                BeanUtils.copyProperties(eoHotelPO, eoHotelVO);
                // 园区字典翻译（park）
                eoHotelVO.setParkName(dictService.getLabel("park", eoHotelVO.getPark()));
                // 住宿
                List<EoAccommodationPO> eoAccommodationPOS = eoAccommodationMapper.selectList(Wrappers.<EoAccommodationPO>lambdaQuery().eq(EoAccommodationPO::getHotelId, eoHotelPO.getId()));
                List<EoAccommodationVO> eoAccommodationVOS = new ArrayList<>();
                for (EoAccommodationPO eoAccommodationPO : eoAccommodationPOS) {
                    EoAccommodationVO eoAccommodationVO = new EoAccommodationVO();
                    BeanUtils.copyProperties(eoAccommodationPO, eoAccommodationVO);
                    // 房型字典翻译（roomType）
                    eoAccommodationVO.setRoomTypeName(dictService.getLabel("roomType", eoAccommodationVO.getRoomType()));
                    eoAccommodationVOS.add(eoAccommodationVO);
                }
                eoHotelVO.setEoAccommodationVOS(eoAccommodationVOS);

                // 会议
                List<EoMettingVO> eoMettingVOS = new ArrayList<>();
                for (EoMettingPO eoMettingPO : eoMettingMapper.selectList(Wrappers.<EoMettingPO>lambdaQuery().eq(EoMettingPO::getHotelId, eoHotelPO.getId()))) {
                    EoMettingVO eoMettingVO = new EoMettingVO();
                    BeanUtils.copyProperties(eoMettingPO, eoMettingVO);
                    // TODO: 2023/5/22  会议室名称回显
                    List<EoMettingGoodsPO> eoMettingGoodsPOS = eoMettingGoodsMapper.selectList(Wrappers.<EoMettingGoodsPO>lambdaQuery().eq(EoMettingGoodsPO::getMettingId, eoMettingVO.getId()));
                    eoMettingVO.setEoMettingGoodsPOS(eoMettingGoodsPOS);
                    eoMettingVOS.add(eoMettingVO);
                }
                eoHotelVO.setEoMettingVOS(eoMettingVOS);
                eoTaskVO.setEoHotelVO(eoHotelVO);
            });
            //2、餐饮
            Optional.ofNullable(eoCateringMapper.selectOne(Wrappers.<EoCateringPO>lambdaQuery().eq(EoCateringPO::getEoId, eoTaskPO.getId()))).ifPresent(eoCateringPO -> {
                EoCateringVO eoCateringVO = new EoCateringVO();
                BeanUtils.copyProperties(eoCateringPO, eoCateringVO);
                //就餐形式（字典翻译：diningForm）
                eoCateringVO.setDiningFormName(dictService.getLabel("diningForm", eoCateringVO.getDiningForm()));
                //餐饮时刻表
                eoCateringVO.setEoCateringSchedulePOS(eoCateringScheduleMapper.selectList(Wrappers.<EoCateringSchedulePO>lambdaQuery().eq(EoCateringSchedulePO::getCateringId, eoCateringVO.getId())));
                eoTaskVO.setEoCateringVO(eoCateringVO);
            });
            //3、安全管理部（车辆相关）
            Optional.ofNullable(eoVehicleMapper.selectOne(Wrappers.<EoVehiclePO>lambdaQuery().eq(EoVehiclePO::getEoId, eoTaskPO.getId()))).ifPresent(eoVehiclePO -> {
                EoVehicleVO eoVehicleVO = new EoVehicleVO();
                BeanUtils.copyProperties(eoVehiclePO, eoVehicleVO);
                // 车辆相关信息
                eoVehicleVO.setEoVehicleInfoPOS(eoVehicleInfoMapper.selectList(Wrappers.<EoVehicleInfoPO>lambdaQuery().eq(EoVehicleInfoPO::getVehicleId, eoVehicleVO.getId())));
                eoTaskVO.setEoVehicleVO(eoVehicleVO);
            });
        });
        return eoTaskVO;
    }

    @Override
    public void addMettingGoods(EoMettingGoodsDTO eoMettingGoodsDTO) {
        EoMettingGoodsPO eoMettingGoodsPO = new EoMettingGoodsPO();
        BeanUtils.copyProperties(eoMettingGoodsDTO,eoMettingGoodsPO);
        eoMettingGoodsMapper.insert(eoMettingGoodsPO);
    }

    @Override
    public void addVehicleInfo(EoVehicleInfoDTO eoVehicleInfoDTO, UserInformation user) {
        EoVehicleInfoPO eoVehicleInfoPO = new EoVehicleInfoPO();
        BeanUtils.copyProperties(eoVehicleInfoDTO, eoVehicleInfoPO);
        Optional.ofNullable(eoVehicleInfoDTO.getEoId()).ifPresent(eoId -> {
            Optional.ofNullable(eoVehicleMapper.selectOne(Wrappers.<EoVehiclePO>lambdaQuery()
                    .eq(EoVehiclePO::getEoId, eoId))).ifPresent(eoVehiclePO -> {
                eoVehicleInfoPO.setVehicleId(eoVehiclePO.getId());
                eoVehicleInfoMapper.insert(eoVehicleInfoPO);
                // 联动临时车
                AddVehicleInfo addVehicleInfo = new AddVehicleInfo();
                BeanUtils.copyProperties(eoVehicleInfoPO, addVehicleInfo);
                Optional.ofNullable(eoTaskMapper.selectById(eoId)).ifPresent(eoTaskPO -> {
                    // 开始日期
                    addVehicleInfo.setStartTime(eoTaskPO.getStartTime());
                    // 结束日期
                    addVehicleInfo.setEndTime(eoTaskPO.getEndTime());
                });
                //登录信息
                addVehicleInfo.setUserInformation(user);
                eoEventPublisher.addVehicleInfo(addVehicleInfo);
            });
        });
    }

    @Override
    public void addVisits(EoVisitInfoDTO eoVisitInfoDTO,UserInformation userInformation) {
        EoVisitInfoPO eoVisitInfoPO = new EoVisitInfoPO();
        BeanUtils.copyProperties(eoVisitInfoDTO, eoVisitInfoPO);
        eoVisitInfoPO.setInterviewee(userInformation.getStaffId());
        eoVisitInfoMapper.insert(eoVisitInfoPO);
        Optional.ofNullable(eoVisitInfoPO.getPhotoUrl()).ifPresent(photoUrl -> {
            //联动访客模块
            AddVisitsInfo addVisitsInfo = new AddVisitsInfo();
            BeanUtils.copyProperties(eoVisitInfoPO, addVisitsInfo);
            Optional.ofNullable(eoTaskMapper.selectById(eoVisitInfoPO.getEoId())).ifPresent(eoTaskPO -> {
                addVisitsInfo.setStartTime(eoTaskPO.getStartTime());
                addVisitsInfo.setEndTime(eoTaskPO.getEndTime());
            });
            addVisitsInfo.setUserInformation(userInformation);
            eoEventPublisher.addVisitsInfo(addVisitsInfo);
        });
    }

    @Override
    public void delMettingGoodsById(Integer id) {
        eoMettingGoodsMapper.deleteById(id);
    }

    @Override
    public void delVehicleById(Integer id) {
        eoVehicleInfoMapper.deleteById(id);
    }

    @Override
    public void delVisitById(Integer id) {
        eoVisitInfoMapper.deleteById(id);
    }

    @Override
    public void downloadVisitTemplate(HttpServletResponse response) {
        List<String> headerList = new ArrayList<String>();
        headerList.add("访客名称");
        headerList.add("证件类型");
        headerList.add("访客证件");
        headerList.add("访客手机");
        headerList.add("照片");
        headerList.add("到访事由");
        headerList.add("到访园区");
        headerList.add("备注");
        /** 第一步，创建一个Workbook，对应一个Excel文件  */
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFFont font = wb.createFont();
        XSSFCellStyle cellStyle = wb.createCellStyle();

        XSSFCellStyle defaultColStyle = wb.createCellStyle();


        XSSFDataFormat format = wb.createDataFormat();
        defaultColStyle.setDataFormat(format.getFormat("@"));


        font.setColor(HSSFColor.HSSFColorPredefined.RED.getColor().getIndex());
        cellStyle.setFont(font);
        /** 第二步，在Workbook中添加一个sheet,对应Excel文件中的sheet  */
        XSSFSheet sheet = wb.createSheet("Sheet1");

        int rowNum = 0;
        XSSFRow row1 = sheet.createRow(rowNum++);
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        //查询到访事由
        List<DictInformation> arriveType = dictService.listDict("arriveType");
        String[] arriveTypes = arriveType.stream().collect(Collectors.toMap(DictInformation::getLabel, DictInformation::getValue)).keySet().toArray(new String[]{});
        XSSFDataValidationConstraint arriveTypesConstraint = (XSSFDataValidationConstraint) dvHelper
                .createExplicitListConstraint(arriveTypes);

        //查询证件类型
        List<DictInformation> certificateTypes = dictService.listDict("certificateType");
        String[] types = certificateTypes.stream().collect(Collectors.toMap(DictInformation::getLabel, DictInformation::getValue)).keySet().toArray(new String[]{});
        XSSFDataValidationConstraint typesConstraint = (XSSFDataValidationConstraint) dvHelper
                .createExplicitListConstraint(types);

        CellRangeAddressList addressList = null;
        XSSFDataValidation validation = null;

        for (int i = 0; i < inAndOutParkRows; i++) {
            addressList = new CellRangeAddressList(1, i + 2, 5, 5);
            validation = (XSSFDataValidation) dvHelper.createValidation(
                    arriveTypesConstraint, addressList);
            sheet.addValidationData(validation);
        }

        for (int i = 0; i < inAndOutParkRows; i++) {
            addressList = new CellRangeAddressList(1, i + 2, 1, 1);
            validation = (XSSFDataValidation) dvHelper.createValidation(
                    typesConstraint, addressList);
            sheet.addValidationData(validation);
        }
        //查询到访园区
        String[] areas = spaceFeignClient.listArea().stream().collect(Collectors.toMap(AreaInformation::getName, AreaInformation::getId)).keySet().toArray(new String[]{});
        XSSFDataValidationConstraint areasConstraint = (XSSFDataValidationConstraint) dvHelper
                .createExplicitListConstraint(areas);
        for (int i = 0; i < inAndOutParkRows; i++) {
            addressList = new CellRangeAddressList(1, i + 2, 6, 6);
            validation = (XSSFDataValidation) dvHelper.createValidation(
                    areasConstraint, addressList);
            sheet.addValidationData(validation);
        }
        for (int j = 0; j < headerList.size(); j++) {
            sheet.setDefaultColumnStyle(j, defaultColStyle);
            if (j == 2 || j == 3) {
                sheet.setColumnWidth(j, 25 * 256);
            } else {
                sheet.setColumnWidth(j, 15 * 256);
            }
            XSSFCell tempCell = row1.createCell(j);
            tempCell.setCellValue(headerList.get(j));
        }
        try {
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=" + new String(URLEncoder.encode("访客模板.xlsx", "UTF-8").getBytes(StandardCharsets.UTF_8), "ISO8859-1"));

            OutputStream stream = response.getOutputStream();
            if (null != wb && null != stream) {
                wb.write(stream);// 将数据写出去
                wb.close();
                stream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // DownloadFileUtils.downloadByStream(EoOrderConstants.TEMPLATE_VISIT,response);
    }

    @Override
    public void downloadVehicleTemplate(HttpServletResponse response) {
        List<String> headerList = new ArrayList<String>();
        headerList.add("停车场");
        headerList.add("车牌号");
        headerList.add("姓名");
        headerList.add("车辆颜色");
        headerList.add("手机号");

        /** 第一步，创建一个Workbook，对应一个Excel文件  */
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFFont font = wb.createFont();
        XSSFCellStyle cellStyle = wb.createCellStyle();

        XSSFCellStyle defaultColStyle = wb.createCellStyle();


        XSSFDataFormat format = wb.createDataFormat();
        defaultColStyle.setDataFormat(format.getFormat("@"));


        font.setColor(HSSFColor.HSSFColorPredefined.RED.getColor().getIndex());
        cellStyle.setFont(font);
        /** 第二步，在Workbook中添加一个sheet,对应Excel文件中的sheet  */
        XSSFSheet sheet = wb.createSheet("Sheet1");

        int rowNum = 0;
        XSSFRow row1 = sheet.createRow(rowNum++);
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper(sheet);
        //查询车辆停车场
        String[] parkLots = bmpCertificateFeignClient.selectParkingLotList().stream().collect(Collectors.toMap(ParkingLotInfo::getName, ParkingLotInfo::getId)).keySet().toArray(new String[]{});
        XSSFDataValidationConstraint parkLotsConstraint = (XSSFDataValidationConstraint) dvHelper
                .createExplicitListConstraint(parkLots);

        CellRangeAddressList addressList = null;
        XSSFDataValidation validation = null;

        for (int i = 0; i < inAndOutParkRows; i++) {
            addressList = new CellRangeAddressList(1, i + 2, 0, 0);
            validation = (XSSFDataValidation) dvHelper.createValidation(
                    parkLotsConstraint, addressList);
            sheet.addValidationData(validation);

        }
        for (int j = 0; j < headerList.size(); j++) {
            sheet.setDefaultColumnStyle(j, defaultColStyle);
            if (j == 2 || j == 3) {
                sheet.setColumnWidth(j, 25 * 256);
            } else {
                sheet.setColumnWidth(j, 15 * 256);
            }
            XSSFCell tempCell = row1.createCell(j);
            tempCell.setCellValue(headerList.get(j));
        }
        try {
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=" + new String(URLEncoder.encode("车辆模板.xlsx", "UTF-8").getBytes(StandardCharsets.UTF_8), "ISO8859-1"));

            OutputStream stream = response.getOutputStream();
            if (null != wb && null != stream) {
                wb.write(stream);// 将数据写出去
                wb.close();
                stream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // DownloadFileUtils.downloadByStream(EoOrderConstants.TEMPLATE_VEHICLE,response);
    }

    @Override
    public void downloadMettingGoodsTemplate(HttpServletResponse response) {
        DownloadFileUtils.downloadByStream(EoOrderConstants.TEMPLATE_METTING_GOODS,response);
    }

    @Override
    public void importVisitData(MultipartFile file,String eoId,UserInformation user) {
        File toFile = ExcelUtils.MultipartFileToFile(file);
        excelVisitDataListener.setList(new ArrayList<EoVisitInfoDTO>());
        EasyExcel.read(toFile, EoVisitInfoDTO.class, excelVisitDataListener).sheet(0).doRead();
        List<EoVisitInfoDTO> list = excelVisitDataListener.getList();
        // 使用HuTools工具类解析表格的图片
        //注意这个方法只能读取浮动的图片，嵌入单元格的读取不到！
        //注意这个方法只能读取浮动的图片，嵌入单元格的读取不到！
        //注意这个方法只能读取浮动的图片，嵌入单元格的读取不到！
        ExcelReader reader = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(toFile);
            reader = ExcelUtil.getReader(fileInputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //key是图片位置，row_cell的格式；value是图片数据
        Map<String, PictureData> picMap = ExcelPicUtil.getPicMap(reader.getWorkbook(), 0);
        //这里只关心行数，把数据装到Bean里面去，也可用map在循环中取获取
        picMap.forEach((k, v) -> {
            String[] split = k.split(StrUtil.UNDERLINE);
            Integer index = Integer.valueOf(split[0]);
            byte[] data = v.getData();
            try {
                InputStream is = new ByteArrayInputStream(data);//转换成输入流
                MultipartFile fileResult = new MockMultipartFile("file", Math.random() + ".jpg",
                        ContentType.APPLICATION_OCTET_STREAM.toString(), is);
                String url = pcsFeignClient.fileUpload(fileResult);
                list.get(index-1).setPhotoUrl(url);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        list.forEach(eoVisitInfoDTO -> {
            EoVisitInfoPO eoVisitInfoPO = new EoVisitInfoPO();
            BeanUtils.copyProperties(eoVisitInfoDTO, eoVisitInfoPO);
            eoVisitInfoPO.setEoId(eoId);
            //证件类型
            Optional.ofNullable(eoVisitInfoDTO.getCertificateType()).ifPresent(certType->{
                dictService.listDict("certificateType").forEach(certificateType -> {
                    if (certificateType.getLabel().equals(certType)) {
                        eoVisitInfoPO.setCertificateType(certificateType.getValue());
                    }
                });
            });
            //到访事由
            Optional.ofNullable(eoVisitInfoDTO.getVisitReason()).ifPresent(reason->{
                dictService.listDict("arriveType").forEach(arriveType->{
                    if (arriveType.getLabel().equals(reason)){
                        eoVisitInfoPO.setVisitReason(arriveType.getValue());
                    }
                });
            });

            // 到访园区
            Optional.ofNullable(eoVisitInfoDTO.getAreaName()).ifPresent(areaName->{
                Map<String, Integer> map = spaceFeignClient.listArea().stream().collect(Collectors.toMap(AreaInformation::getName, AreaInformation::getId));
                if (map.containsKey(areaName)){
                    eoVisitInfoPO.setAreaId(map.get(eoVisitInfoDTO.getAreaName()));
                }
            });
            // 接访人
            eoVisitInfoPO.setInterviewee(user.getStaffId());
            eoVisitInfoMapper.insert(eoVisitInfoPO);
            // 联动访客
            Optional.ofNullable(eoVisitInfoPO.getPhotoUrl()).ifPresent(photoUrl -> {
                //联动访客模块
                AddVisitsInfo addVisitsInfo = new AddVisitsInfo();
                BeanUtils.copyProperties(eoVisitInfoPO, addVisitsInfo);
                Optional.ofNullable(eoTaskMapper.selectById(eoVisitInfoPO.getEoId())).ifPresent(eoTaskPO -> {
                    addVisitsInfo.setStartTime(eoTaskPO.getStartTime());
                    addVisitsInfo.setEndTime(eoTaskPO.getEndTime());
                });
                addVisitsInfo.setUserInformation(user);
                eoEventPublisher.addVisitsInfo(addVisitsInfo);
            });
        });
    }

    @Override
    public void importVehicleData(MultipartFile file,Integer vehicleId,UserInformation user) {
        File toFile = ExcelUtils.MultipartFileToFile(file);
        excelVehicleDataListener.setList(new ArrayList<EoVehicleInfoDTO>());
        EasyExcel.read(toFile, EoVehicleInfoDTO.class, excelVehicleDataListener).sheet(0).doRead();
        List<EoVehicleInfoDTO> list = excelVehicleDataListener.getList();
        list.forEach(eoVehicleInfoDTO -> {
            EoVehicleInfoPO eoVehicleInfoPO = new EoVehicleInfoPO();
            BeanUtils.copyProperties(eoVehicleInfoDTO, eoVehicleInfoPO);
            eoVehicleInfoPO.setVehicleId(vehicleId);
            bmpCertificateFeignClient.selectParkingLotList().forEach(parkingLotInfo -> {
                if (eoVehicleInfoPO.getPark().equals(parkingLotInfo.getName())) {
                    eoVehicleInfoPO.setPark(parkingLotInfo.getId());
                }
            });
            eoVehicleInfoMapper.insert(eoVehicleInfoPO);
            // 联动临时车
            AddVehicleInfo addVehicleInfo = new AddVehicleInfo();
            BeanUtils.copyProperties(eoVehicleInfoPO, addVehicleInfo);
            Optional.ofNullable(eoVehicleMapper.selectById(vehicleId)).ifPresent(eoVehiclePO -> {
                Optional.ofNullable(eoTaskMapper.selectById(eoVehiclePO.getEoId())).ifPresent(eoTaskPO -> {
                    // 开始日期
                    addVehicleInfo.setStartTime(eoTaskPO.getStartTime());
                    // 结束日期
                    addVehicleInfo.setEndTime(eoTaskPO.getEndTime());
                });
            });
            //登录信息
            addVehicleInfo.setUserInformation(user);
            eoEventPublisher.addVehicleInfo(addVehicleInfo);
        });
    }

    @Override
    public IPage<EoVisitInfoVO> getVisitInfoByPage(EoVisitInfoDTO eoVisitInfoDTO) {
        Page<EoVisitInfoPO> data = eoVisitInfoMapper.selectPage(eoVisitInfoDTO.toPage(), Wrappers.<EoVisitInfoPO>lambdaQuery()
                .eq(EoVisitInfoPO::getEoId, eoVisitInfoDTO.getEoId()));
        return data.convert(e -> {
            EoVisitInfoVO eoVisitInfoVO = new EoVisitInfoVO();
            BeanUtils.copyProperties(e, eoVisitInfoVO);
            if (null!=e.getInterviewee()) {
                StaffInfoFine staffInfo = staffFeignClient.getStaffById(e.getInterviewee());
                eoVisitInfoVO.setIntervieweeName(staffInfo.getFldName());
            }
            if(null!=e.getAreaId()){
                String areaName = spaceFeignClient.getPositionByAreaId(e.getAreaId());
                eoVisitInfoVO.setAreaName(areaName);
            }
            return eoVisitInfoVO;
        });
    }

    @Override
    public IPage<EoVehicleInfoVO> getVehicleInfoByPage(EoVehicleInfoDTO eoVehicleInfoDTO) {
        EoVehiclePO eoVehiclePO = eoVehicleMapper.selectOne(Wrappers.<EoVehiclePO>lambdaQuery()
                .eq(EoVehiclePO::getEoId, eoVehicleInfoDTO.getEoId()));
        if (null != eoVehiclePO) {
            Page<EoVehicleInfoPO> data = eoVehicleInfoMapper.selectPage(eoVehicleInfoDTO.toPage(), Wrappers.<EoVehicleInfoPO>lambdaQuery()
                    .eq(EoVehicleInfoPO::getVehicleId,eoVehiclePO.getId()));
            return data.convert(e -> {
                EoVehicleInfoVO eoVehicleInfoVO = new EoVehicleInfoVO();
                BeanUtils.copyProperties(e, eoVehicleInfoVO);
                return eoVehicleInfoVO;
            });
        } else {
            throw new ResultException("该Eo单Id:" + eoVehicleInfoDTO.getEoId() + "没有车辆EO单相关信息！");
        }
    }

    @Override
    public IPage<EoMettingGoodsPO> getMettingGoodsInfoByMetId(EoMettingGoodsDTO eoMettingGoodsDTO) {
        return eoMettingGoodsMapper.selectPage(eoMettingGoodsDTO.toPage(), Wrappers.<EoMettingGoodsPO>lambdaQuery()
                .eq(EoMettingGoodsPO::getMettingId, eoMettingGoodsDTO.getMettingId()));
    }

    @Override
    public void importMettingGoodsData(MultipartFile file, Integer mettingId) {
        File toFile = ExcelUtils.MultipartFileToFile(file);
        excelMettingGoodsDataListener.setList(new ArrayList<EoMettingGoodsDTO>());
        EasyExcel.read(toFile, EoMettingGoodsDTO.class, excelMettingGoodsDataListener).sheet(0).doRead();
        //从excel解析出来的物品相关信息
        excelMettingGoodsDataListener.getList().forEach(mettingGoodsDTO -> {
            EoMettingGoodsPO eoMettingGoodsPO = new EoMettingGoodsPO();
            BeanUtils.copyProperties(mettingGoodsDTO,eoMettingGoodsPO);
            eoMettingGoodsPO.setMettingId(mettingId);
            eoMettingGoodsMapper.insert(eoMettingGoodsPO);
        });
    }

    @Override
    public void execute(EoExecuteDTO eoExecuteDTO) {
        Optional.ofNullable(eoExecuteDTO.getEoId()).ifPresent(eoId -> {
            Optional.ofNullable(eoTaskMapper.selectById(eoId)).ifPresent(eoTaskPO -> {
                //改成已执行状态
                eoTaskPO.setStatus(EoTaskStatusConstants.EXECUTE_STATUS);
                eoTaskMapper.updateById(eoTaskPO);
                String eoType = eoTaskPO.getEoType();
                //1、酒店
                if (eoType.contains("1")) {
                    DescribeItem describeItem = new DescribeItem();
                    describeItem.setLabel("eoId");
                    describeItem.setValue(new TextDescribeValue(eoId));
                    List<DescribeItem> items = CollectionUtil.newArrayList(describeItem);
                    workOrderService.pushOrder(EoOrderConstants.HOTEL_ORDER, "酒店EO单联动工单", items);
                }
                //2、餐饮
                if (eoType.contains("2")) {
                    DescribeItem describeItem = new DescribeItem();
                    describeItem.setLabel("eoId");
                    describeItem.setValue(new TextDescribeValue(eoId));
                    List<DescribeItem> items = CollectionUtil.newArrayList(describeItem);
                    workOrderService.pushOrder(EoOrderConstants.CATERING_ORDER, "餐饮EO单联动工单", items);
                }
                //3、会议
                if (eoType.contains("3")) {
                    Optional.ofNullable(eoExecuteDTO.getMettingIds()).ifPresent(mIds->{
                        List<DescribeItem> items =new ArrayList<>();
                        for (String mId : mIds.split(",")) {
                            DescribeItem d_1 = new DescribeItem();
                            d_1.setLabel("eoId");
                            d_1.setValue(new TextDescribeValue(eoId));
                            items.add(d_1);
                            DescribeItem d_2 = new DescribeItem();
                            d_2.setLabel("mId");
                            d_2.setValue(new TextDescribeValue(mId));
                            items.add(d_2);
                            workOrderService.pushOrder(EoOrderConstants.METTING_ORDER, "会议EO单联动工单", items);
                        }
                    });
                }
                //4、访客  无论如何都会联动eo单
                DescribeItem describeItem = new DescribeItem();
                describeItem.setLabel("eoId");
                describeItem.setValue(new TextDescribeValue(eoId));
                List<DescribeItem> items = CollectionUtil.newArrayList(describeItem);
                workOrderService.pushOrder(EoOrderConstants.VISITOR_ORDER, "访客EO单联动工单", items);
            });
        });
    }

    @Override
    public void settlement(EoSettlementDTO eoSettlementDTO) {
        EoSettlementPO eoSettlementPO = new EoSettlementPO();
        BeanUtils.copyProperties(eoSettlementDTO,eoSettlementPO);
        eoSettlementMapper.insert(eoSettlementPO);
        //结算完后将eo单状态改为已完成
        EoTaskPO eoTaskPO = new EoTaskPO();
        eoTaskPO.setId(eoSettlementDTO.getEoId());
        eoTaskPO.setStatus(EoTaskStatusConstants.FINISH_STATUS);
        eoTaskMapper.updateById(eoTaskPO);
        //发送财务EO单
        //餐饮
        Optional.ofNullable(woOrderdefMapper.selectById(EoOrderConstants.FINANCE_ORDER)).ifPresent(woOrderdefPO -> {
            String eventCode = EoOrderConstants.DEFAULT_EVENT_CODE + EoOrderConstants.EVENT_FINANCE_SUFFIX;
            DescribeItem describeItem = new DescribeItem();
            describeItem.setLabel("eoId");
            describeItem.setValue(new TextDescribeValue(eoSettlementDTO.getEoId()));
            List<DescribeItem> items = CollectionUtil.newArrayList(describeItem);
            workOrderService.pushOrder(EoOrderConstants.FINANCE_ORDER, "财务EO单联动工单", items);
        });
    }

    @Override
    public EoSettlementVO getSettlementInfo(String eoId) {
        EoSettlementVO eoSettlementVO = new EoSettlementVO();
        Optional.ofNullable(eoSettlementMapper.selectOne(Wrappers.<EoSettlementPO>lambdaQuery().eq(EoSettlementPO::getEoId, eoId))).ifPresent(eoSettlementPO -> {
            BeanUtils.copyProperties(eoSettlementPO,eoSettlementVO);
        });
        return eoSettlementVO;
    }
}
