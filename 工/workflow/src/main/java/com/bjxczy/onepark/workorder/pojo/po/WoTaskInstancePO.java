package com.bjxczy.onepark.workorder.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.bjxczy.core.web.base.UserOperator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

/**
 * <p>
 * 任务受理实例表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-04-03
 */
@Data
@TableName(value = "wo_task_instance", autoResultMap = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "WoTaskInstance对象", description = "任务受理实例表")
public class WoTaskInstancePO extends UserOperator {

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty(value = "受理人ID")
    private Integer assigneeId;

    @ApiModelProperty(value = "受理人名称")
    private String assigneeName;

    @ApiModelProperty(value = "任务执行ID")
    private Integer taskExecId;

    @ApiModelProperty(value = "任务状态（字典：woTaskStatus，但无已创建状态）")
    private String instStatus;

    @ApiModelProperty(value = "描述")
    private String instDesc;

    @ApiModelProperty(value = "图片")
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private Set<String> instPictures;

    @ApiModelProperty(value = "驳回图片")
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private Set<String> rejectPicture;

    @ApiModelProperty(value = "驳回原因")
    private String rejectReason;

    @ApiModelProperty(value = "关联工单Id")
    private String triggerId;

}
