package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.workorder.pojo.dto.PullDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoDataOverviewDTO;
import com.bjxczy.onepark.workorder.pojo.po.WoTaskInstancePO;
import com.bjxczy.onepark.workorder.pojo.vo.DetailsVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoTaskVariablesVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface WoTaskInstanceMapper extends BaseMapper<WoTaskInstancePO> {
    List<Map<String, Object>> chartList(WoDataOverviewDTO woDataOverviewDTO);

    List<Map<String, Object>> listByDepart(WoDataOverviewDTO woDataOverviewDTO);

    Integer selectcounts(WoDataOverviewDTO woDataOverviewDTO);

    List<Map<String, Object>> chartListByDepart(WoDataOverviewDTO woDataOverviewDTO);

    List<Integer> listDeptId(WoDataOverviewDTO woDataOverviewDTO);

    List<Map<String, Object>> pullList(PullDTO pullDTO);

    List<DetailsVO> selectByTaskExecId(@Param("taskExecIds") List<Integer> taskExecIds);
}
