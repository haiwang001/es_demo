package com.bjxczy.onepark.workflow.process.core.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.el.ExpressionFactory;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.apache.commons.lang3.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * BPMN流程预执行，获取执行经过流程节点
 * 
 * @author zzz
 */
@Getter
public class ProcessPreEngine {

	@Setter
	private Map<String, Object> beans;
	@Setter
	private JexlEngine jexlEngine = new JexlEngine();

	private ExpressionFactory factory = ExpressionFactory.newInstance();

	private String startId;

	private Map<String, Node> flowNodeContainer;
	@Setter
	private List<Line> lines;

	public ProcessPreEngine setNodes(List<Node> flowNodeList) {
		Optional<Node> startEvent = flowNodeList.stream().filter(e -> Objects.equals("startEvent", e.getTagName()))
				.findFirst();
		startEvent.orElseThrow(() -> new RuntimeException("流程不存在开始节点！"));
		this.startId = startEvent.get().id;
		this.flowNodeContainer = flowNodeList.stream().collect(Collectors.toMap(Node::getId, e -> e));
		return this;
	}

	public List<Node> startProcess(Map<String, Object> variables) {
		List<Node> historyNode = new ArrayList<>();
		Node node = flowNodeContainer.get(this.startId);
		do {
			historyNode.add(node);
			node = execution(node, variables);
		} while (node != null);
		return historyNode;
	}

	private Node execution(Node sourceNode, Map<String, Object> variables) {
		String targetRef = null;
		Node targetNode = null;
		// 获取节点连线
		List<Line> nodeLine = getBySourceRef(sourceNode.getId());
		if (nodeLine.size() == 1) {
			// 无分支
			targetRef = nodeLine.get(0).getTargetRef();
		} else if (nodeLine.size() > 1) {
			// 多目标分支流程
			for (Line line : nodeLine) {
				if (hasTrueCondition(line.getExpression(), variables)) {
					targetRef = line.getTargetRef();
					break;
				}
				if (Objects.equals(line.getId(), sourceNode.getDefaultFlow())) {
					targetRef = line.getTargetRef();
				}
			}
		}
		if (null != targetRef && flowNodeContainer.containsKey(targetRef)) {
			targetNode = flowNodeContainer.get(targetRef);
		}
		return targetNode;
	}

	private List<Line> getBySourceRef(String sourceRef) {
		return lines.stream().filter(e -> Objects.equals(sourceRef, e.getSourceRef())).collect(Collectors.toList());
	}

	private boolean hasTrueCondition(String conditionExpression, Map<String, Object> variables) {
		if (StringUtils.isNoneBlank(conditionExpression)) {
			JexlContext context = getVariableContext(variables);
			// 去除activiti表达式${}
			conditionExpression = conditionExpression.replaceAll("\\$\\{", "").replaceAll("\\}", "");
			Expression expression = jexlEngine.createExpression(conditionExpression);
			Object value = expression.evaluate(context);
			if (value instanceof Boolean) {
				return (Boolean) value;
			} else {
				throw new RuntimeException("expression return is not boolean ! return " + value.toString());
			}
		}
		return false;
	}

	private JexlContext getVariableContext(Map<String, Object> variables) {
		JexlContext context = new MapContext();
		this.beans.forEach((key, value) -> context.set(key, value));
		variables.forEach((key, value) -> context.set(key, value));
		return context;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Node {

		private String id;

		private String tagName;

		private String defaultFlow;

	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Line {

		private String id;

		private String sourceRef;

		private String targetRef;

		private String expression;

	}

}
