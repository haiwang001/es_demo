package com.bjxczy.onepark.workorder.interfaces.publisher;

import com.bjxczy.onepark.common.model.workflow.AddEoMettingInfo;
import com.bjxczy.onepark.common.model.workflow.AddVehicleInfo;
import com.bjxczy.onepark.common.model.workflow.AddVisitsInfo;
import com.bjxczy.onepark.workorder.event.AddEoMettingInfoEvent;
import com.bjxczy.onepark.workorder.event.AddVehicleInfoEvent;
import com.bjxczy.onepark.workorder.event.AddVisitsInfoEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * @ClassName EoEventPublisher
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/26 17:00
 * @Version 1.0
 */
@Component
public class EoEventPublisher {

    @Autowired
    private ApplicationEventPublisher publisher;

    public void addVehicleInfo(AddVehicleInfo addVehicleInfo) {
        publisher.publishEvent(new AddVehicleInfoEvent(addVehicleInfo));
    }

    public void addVisitsInfo(AddVisitsInfo addVisitsInfo) {
        publisher.publishEvent(new AddVisitsInfoEvent(addVisitsInfo));
    }

    public void addMettingInfo(AddEoMettingInfo addEoMettingInfo) {
        publisher.publishEvent(new AddEoMettingInfoEvent(addEoMettingInfo));
    }
}
