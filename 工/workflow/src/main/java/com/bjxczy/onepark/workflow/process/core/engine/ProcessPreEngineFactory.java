package com.bjxczy.onepark.workflow.process.core.engine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.jexl2.JexlEngine;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.bjxczy.onepark.workflow.process.core.condition.AbstractExpressionHandler;


@Component
public class ProcessPreEngineFactory implements ApplicationContextAware {

	private JexlEngine jexlEngine = new JexlEngine();

	private Map<String, Object> beans;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		Map<String, AbstractExpressionHandler> beansOfType = applicationContext
				.getBeansOfType(AbstractExpressionHandler.class);
		this.beans = new HashMap<>();
		beansOfType.forEach((key, value) -> beans.put(key, value));
	}

	public ProcessPreEngine createProcessPreInstance(List<ProcessPreEngine.Node> nodes,
			List<ProcessPreEngine.Line> lines) {
		ProcessPreEngine instance = new ProcessPreEngine();
		instance.setNodes(nodes);
		instance.setLines(lines);
		instance.setBeans(beans);
		instance.setJexlEngine(jexlEngine);
		return instance;
	}

}
