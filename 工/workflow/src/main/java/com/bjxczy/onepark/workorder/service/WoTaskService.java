package com.bjxczy.onepark.workorder.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.workflow.PushOrderPayload;
import com.bjxczy.onepark.workorder.pojo.dto.*;
import com.bjxczy.onepark.workorder.pojo.vo.TaskFormVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoTaskInstanceVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoTaskVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoTaskdefVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface WoTaskService {
    void pcDefCreate(WoTaskDTO woTaskDTO, UserInformation user);

    Page<WoTaskdefVO> defListPage(WoTaskDTO woTaskDTO);

    void defEditTask(WoTaskDTO woTaskDTO);

    void defTaskExport(WoTaskDTO woTaskDTO, HttpServletResponse response);

    Page<WoTaskVO> executeListPage(WoTaskDTO woTaskDTO);

    void executeDelByIds(List<Integer> ids);

    void executeEdit(WoTaskDTO woTaskDTO);

    List<WoTaskVO> mobAssignList(WoTaskDTO woTaskDTO, UserInformation user);

    void assignTask(WoTaskInstanceDTO woTaskInstanceDTO, UserInformation user);

    void mobTransfer(WoTaskAssigneeDTO woTaskAssigneeDTO, UserInformation user);

    List<WoTaskInstanceVO> mobInstanceList(WoTaskInstanceDTO woTaskInstanceDTO, UserInformation user);

    List<WoTaskVO> mobInitiateList(WoTaskDTO woTaskDTO, UserInformation user);

    void mobCreateTask(WoTaskDTO woTaskDTO, UserInformation user);

    void mobReject(WoTaskDTO woTaskDTO);

    void mobCancel(Integer id);

    List<WoTaskVO> mobGrabList();

    List<WoTaskInstanceVO> mobTransferList(UserInformation user);

    void mobComplete(WoTaskInstanceDTO woTaskInstanceDTO);

    void executeCheck(WoTaskInstanceDTO woTaskInstanceDTO);

    List<Map<String, Object>> pullList(PullDTO pullDTO);

    void mdbGrab(WoTaskInstanceDTO woTaskInstanceDTO,UserInformation user);

    void mobReceiveTask(WoTaskInstanceDTO woTaskInstanceDTO,UserInformation user);

    void mobRejectTask(WoTaskInstanceDTO woTaskInstanceDTO, UserInformation user);

    Page<TaskFormVO> getTaskFormByPage(TaskFormDTO taskFormDTO);

    void taskFormExport(HttpServletResponse response, TaskFormDTO taskFormDTO);

    void execTask();

    void pushOrder(PushOrderPayload payload);

    Integer getTranAndGrapCount(UserInformation user);

}
