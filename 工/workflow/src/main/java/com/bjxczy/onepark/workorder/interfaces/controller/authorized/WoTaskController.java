package com.bjxczy.onepark.workorder.interfaces.controller.authorized;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.core.redisson.constant.DelayedQueueNames;
import com.bjxczy.core.redisson.delayed.IDelayedQueue;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.workflow.common.feign.StaffFeignClient;
import com.bjxczy.onepark.workorder.pojo.dto.TaskFormDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoTaskAssigneeDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoTaskDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoTaskInstanceDTO;
import com.bjxczy.onepark.workorder.pojo.vo.TaskFormVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoTaskInstanceVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoTaskVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoTaskdefVO;
import com.bjxczy.onepark.workorder.service.WoTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @ClassName WoTaskController
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/31 9:55
 * @Version 1.0
 */
@ApiResourceController
@Api(tags = "工单任务接口")
@RequestMapping("/task")
public class WoTaskController {

    @Autowired
    private StaffFeignClient staffFeignClient;

    @Autowired
    private WoTaskService woTaskService;

    @Autowired
    @Qualifier(DelayedQueueNames.WORKFLOW_ORDER_TASK_QUEUE)
    private IDelayedQueue orderTaskQueue;

    @ApiOperation("pc端-新增工单任务")
    @PostMapping(value = "/pc/create", name = "pc端-新增工单任务")
    public void pcCreate(@RequestBody WoTaskDTO woTaskDTO, @LoginUser(isFull = true) UserInformation user) {
        woTaskService.pcDefCreate(woTaskDTO, user);
    }

    @ApiOperation("pc端-查询工单任务列表（分页）")
    @GetMapping(value = "/def/listPage", name = "pc端-查询工单任务列表（分页）")
    public Page<WoTaskdefVO> defListPage(WoTaskDTO woTaskDTO) {
        return woTaskService.defListPage(woTaskDTO);
    }

    @ApiOperation("pc端-编辑工单任务")
    @PutMapping(value = "/def/edit", name = "pc端-编辑工单任务")
    public void defEditTask(@RequestBody WoTaskDTO woTaskDTO) {
        woTaskService.defEditTask(woTaskDTO);
    }

    @ApiOperation("pc端-导出工单任务列表数据")
    @PostMapping(value = "/def/export", name = "pc-导出工单列表数据")
    public void defTaskExport(@RequestBody(required = false) WoTaskDTO woTaskDTO, HttpServletResponse response) {
        woTaskService.defTaskExport(woTaskDTO, response);
    }

    @ApiOperation("pc端-执行工单列表（分页）")
    @GetMapping(value = "/execute/listPage", name = "pc端-执行工单列表（分页）")
    public Page<WoTaskVO> executeListPage(WoTaskDTO woTaskDTO) {
        return woTaskService.executeListPage(woTaskDTO);
    }

    @ApiOperation("pc端-执行工单验收审批")
    @PutMapping(value = "/execute/check", name = "pc端-执行工单验收审批")
    public void executeCheck(@RequestBody WoTaskInstanceDTO woTaskInstanceDTO) {
        woTaskService.executeCheck(woTaskInstanceDTO);
    }

    @ApiOperation("pc端-删除执行工单列表（支持批量）")
    @DeleteMapping(value = "/execute/delByIds", name = "pc端-删除执行工单列表（支持批量）")
    public void executeDelByIds(@RequestBody List<Integer> ids) {
        woTaskService.executeDelByIds(ids);
    }

    @ApiOperation("pc端-编辑执行工单")
    @PutMapping(value = "/execute/edit", name = "pc端-编辑执行工单")
    public void executeEdit(@RequestBody WoTaskDTO woTaskDTO) {
        woTaskService.executeEdit(woTaskDTO);
    }

    @ApiOperation("移动端-发起执行工单任务")
    @PostMapping(value = "/mob/create", name = "移动端-发起工单")
    public void mobCreateTask(@RequestBody WoTaskDTO woTaskDTO, @LoginUser(isFull = true) UserInformation user) {
        woTaskService.mobCreateTask(woTaskDTO, user);
    }

    @ApiOperation("移动端-查询派单列表（主管）")
    @GetMapping(value = "/mob/assign/list", name = "移动端-查询执行工单任务列表")
    public List<WoTaskVO> mobAssignList(WoTaskDTO woTaskDTO, @LoginUser(isFull = true) UserInformation user) {
        return woTaskService.mobAssignList(woTaskDTO, user);
    }

    @ApiOperation("移动端-查询任务实例列表（普通员工）")
    @GetMapping(value = "/mob/instance/list", name = "移动端-查询任务实例列表（普通员工）")
    public List<WoTaskInstanceVO> mobInstanceList(WoTaskInstanceDTO woTaskInstanceDTO, @LoginUser(isFull = true) UserInformation user) {
        return woTaskService.mobInstanceList(woTaskInstanceDTO, user);
    }

    @ApiOperation("移动端-我发起的工单列表查询")
    @GetMapping(value = "/mob/initiate/list", name = "移动端-我发起的工单任务列表查询")
    public List<WoTaskVO> mobInitiateList(WoTaskDTO woTaskDTO, @LoginUser(isFull = true) UserInformation user) {
        return woTaskService.mobInitiateList(woTaskDTO, user);
    }

    @ApiOperation("移动端-取消我发起的工单任务")
    @PutMapping(value = "/mob/cancel/{id}", name = "移动端-取消我发起的工单任务")
    public void mobCancel(@PathVariable("id") Integer id) {
        woTaskService.mobCancel(id);
    }

    @ApiOperation("移动端-执行工单任务指派(主管)")
    @PostMapping(value = "/mob/assignTask", name = "移动端-工单指派")
    public void assignTask(@RequestBody WoTaskInstanceDTO woTaskInstanceDTO, @LoginUser(isFull = true) UserInformation user) {
        woTaskService.assignTask(woTaskInstanceDTO, user);
    }

    @ApiOperation("移动端-查询指派人员")
    @GetMapping("/mob/getStaffsByOrgId/{organizationId}")
    public List<StaffInformation> getStaffsByOrgId(@PathVariable("organizationId") Integer organizationId){
        return staffFeignClient.getStaffs(organizationId);
    }

    @ApiOperation("移动端-执行工单任务驳回(主管)")
    @PutMapping(value = "/mob/reject", name = "移动端-执行工单任务驳回(主管)")
    public void mobReject(@RequestBody WoTaskDTO woTaskDTO) {
        woTaskService.mobReject(woTaskDTO);
    }

    @ApiOperation("移动端-转单")
    @PostMapping(value = "/mob/transfer", name = "移动端-转单")
    public void mobTransfer(@RequestBody WoTaskAssigneeDTO woTaskAssigneeDTO, @LoginUser(isFull = true) UserInformation user) {
        woTaskService.mobTransfer(woTaskAssigneeDTO, user);
    }

    @ApiOperation("移动端-查询转单+抢单数")
    @GetMapping(value = "/mob/tranAndGrapCount", name = "移动端-查询转单+抢单数")
    public Integer getTranAndGrapCount(@LoginUser(isFull = true) UserInformation user) {
        return woTaskService.getTranAndGrapCount(user);
    }

    @ApiOperation("移动端-查询转单列表")
    @GetMapping(value = "/mob/transfer/list", name = "移动端-查询转单列表")
    public List<WoTaskInstanceVO> mobTransferList(@LoginUser(isFull = true) UserInformation user) {
        return woTaskService.mobTransferList(user);
    }

    @ApiOperation("移动端-查询抢单列表")
    @GetMapping(value = "/mob/grab/list", name = "移动端-查询抢单列表")
    public List<WoTaskVO> mobGrabList() {
        return woTaskService.mobGrabList();
    }

    @ApiOperation("移动端-接单（他人转的工单）")
    @PostMapping(value = "/mob/receive", name = "移动端-接单（他人转的工单）")
    public void mobReceiveTask(@RequestBody WoTaskInstanceDTO woTaskInstanceDTO, @LoginUser(isFull = true) UserInformation user) {
        woTaskService.mobReceiveTask(woTaskInstanceDTO, user);
    }

    @ApiOperation("移动端-抢单")
    @PostMapping(value = "/mob/grab")
    public void mdbGrab(@RequestBody WoTaskInstanceDTO woTaskInstanceDTO, @LoginUser(isFull = true) UserInformation user) {
        woTaskService.mdbGrab(woTaskInstanceDTO, user);
    }

    @ApiOperation("移动端-拒单（转单的）")
    @PostMapping(value = "/mob/rejectTask", name = "移动端-拒单（转单的）")
    public void mobRejectTask(@RequestBody WoTaskInstanceDTO woTaskInstanceDTO, @LoginUser(isFull = true) UserInformation user) {
        woTaskService.mobRejectTask(woTaskInstanceDTO, user);
    }

    @ApiOperation("移动端-完成工单")
    @PostMapping(value = "/mob/complete", name = "移动端-完成工单")
    public void mobComplete(@RequestBody WoTaskInstanceDTO woTaskInstanceDTO) {
        woTaskService.mobComplete(woTaskInstanceDTO);
    }

    @ApiOperation("pc端-查询任务填报表(分页)")
    @GetMapping(value = "/page/taskForm", name = "pc端-查询任务填报表（分页）")
    public Page<TaskFormVO> getTaskForm(TaskFormDTO taskFormDTO) {
        return woTaskService.getTaskFormByPage(taskFormDTO);
    }

    @ApiOperation("pc端-导出任务填报表数据")
    @PostMapping(value = "/taskForm/export", name = "pc端-导出任务填报表数据")
    public void taskFormExport(HttpServletResponse response, @RequestBody TaskFormDTO taskFormDTO) {
        woTaskService.taskFormExport(response, taskFormDTO);
    }
}
