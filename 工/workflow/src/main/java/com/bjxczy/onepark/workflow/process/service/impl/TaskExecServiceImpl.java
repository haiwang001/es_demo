package com.bjxczy.onepark.workflow.process.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.ibatis.jdbc.RuntimeSqlException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.message.MessageService;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.workbench.DealMessageInformation;
import com.bjxczy.onepark.common.resp.Payload;
import com.bjxczy.onepark.workflow.form.entity.FormItemPO;
import com.bjxczy.onepark.workflow.process.core.bpmn.BpmnModelConvert;
import com.bjxczy.onepark.workflow.process.core.command.RejectedCommand;
import com.bjxczy.onepark.workflow.process.core.constant.ProcessConstant;
import com.bjxczy.onepark.workflow.process.core.constant.TaskVariables;
import com.bjxczy.onepark.workflow.process.core.engine.ProcessPreEngine;
import com.bjxczy.onepark.workflow.process.core.engine.ProcessPreEngineFactory;
import com.bjxczy.onepark.workflow.process.entity.HiTaskPO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefNodePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefStaffPO;
import com.bjxczy.onepark.workflow.process.entity.RuProcessPO;
import com.bjxczy.onepark.workflow.process.facade.ICandidateFacade;
import com.bjxczy.onepark.workflow.process.facade.IFormFacade;
import com.bjxczy.onepark.workflow.process.facade.ISubmodulesFacade;
import com.bjxczy.onepark.workflow.process.pojo.dto.QueryMyTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.RejectTaskDTO;
import com.bjxczy.onepark.workflow.process.pojo.dto.TaskCompleteDTO;
import com.bjxczy.onepark.workflow.process.pojo.vo.CandidateVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.HiTaskVO;
import com.bjxczy.onepark.workflow.process.pojo.vo.TaskVO;
import com.bjxczy.onepark.workflow.process.service.HiTaskService;
import com.bjxczy.onepark.workflow.process.service.ProcdefLineService;
import com.bjxczy.onepark.workflow.process.service.ProcdefNodeService;
import com.bjxczy.onepark.workflow.process.service.ProcdefStaffService;
import com.bjxczy.onepark.workflow.process.service.ProcessDefinitionService;
import com.bjxczy.onepark.workflow.process.service.RuProcessService;
import com.bjxczy.onepark.workflow.process.service.TaskExecService;
import com.bjxczy.onepark.workflow.process.util.ProcessUtils;

import cn.hutool.core.date.DateTime;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TaskExecServiceImpl implements TaskExecService {

	@Autowired
	private RuntimeService runtimeService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private HistoryService historyService;
	@Autowired
	private IdentityService identityService;
	@Autowired
	private ManagementService managementService;
	@Autowired
	private ProcessPreEngineFactory processPreEngineFactory;
	@Autowired
	private ProcessDefinitionService procdefService;
	@Autowired
	private ProcdefStaffService procdefStaffService;
	@Autowired
	private ProcdefNodeService procdefNodeService;
	@Autowired
	private ProcdefLineService procdefLineService;
	@Autowired
	private RuProcessService ruProcessService;
	@Autowired
	private HiTaskService hiTaskService;
	@Autowired
	private MessageService messageService;
	@Autowired
	private IFormFacade formFacade;
	@Autowired
	private ICandidateFacade candidateFacade;
	@Autowired
	private ISubmodulesFacade submodulesFacade;
	
	private void initVariables(String key, Map<String, Object> variables, UserInformation user, RuProcessPO process) {
		variables.put(TaskVariables.VAR_PROC_DEF_ID, key);
		variables.put(TaskVariables.VAR_RETRY_CNT, 0); // 重试次数
		if (process != null) {
			variables.put(TaskVariables.VAR_SUBMIT_TIME, DateTime.of(process.getCreateTime()).toString("yyyy年MM月dd日HH:mm"));
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void runProcess(String key, Map<String, Object> variables, UserInformation user) {
		ProcdefPO def = procdefService.getByDeployId(key);
		// 校验审批范围
		if (def.getApplyRange() == 1) {
			List<ProcdefStaffPO> allows = procdefStaffService.listByDefId(def.getId());
			if (allows.stream().noneMatch(e -> Objects.equals(user.getStaffId(), e.getStaffId()))) {
				throw new ResultException("您无权限提交此申请，请联系管理员");
			}
		}
		// 业务子模块校验
		if (ProcessUtils.isBusinessProcess(def)) {
			submodulesFacade.validated(def.getServiceName(), key, variables, user.getStaffId());
		}
		// 设置流程申请人
		identityService.setAuthenticatedUserId(String.valueOf(user.getStaffId()));
		// 保存执行记录
		RuProcessPO process = ruProcessService.saveByInst(key, user);
		// 初始化变量
		initVariables(key, variables, user, process);
		// 启动流程
		ProcessInstance instance = runtimeService.startProcessInstanceByKey(key, variables);
		process.setProcInstId(instance.getId());
		ruProcessService.updateById(process);
		// 保存审批节点
		hiTaskService.saveOnStart(process);
		log.info("[StartTask] - 启动流程，businessKey = {}", instance.getBusinessKey());
	}

	@Override
	public List<CandidateVO> getCandidate(String key, Integer processId, Map<String, Object> variables,
			UserInformation user) {
		// 初始化变量
		initVariables(key, variables, user, null);
		variables.put(TaskVariables.VAR_INITIATOR_KEY, user.getStaffId());
		// 获取流程定义
		String versionId = null;
		if (processId == null) {
			ProcdefPO def = procdefService.getByDeployId(key);
			versionId = def.getActVersion();
		} else {
			// 调整申请时需获取申请时版本
			RuProcessPO ruProcess = ruProcessService.getById(processId);
			versionId = ruProcess.getProcVersion();
		}
		List<ProcdefNodePO> node = procdefNodeService.listByVersionId(versionId);
		List<ProcdefLinePO> line = procdefLineService.listByVersionId(versionId);
		// 预执行流程
		List<ProcessPreEngine.Node> flowNodeList = node.stream().map(e -> {
			return new ProcessPreEngine.Node(e.getTagId(), e.getTagName(), e.getDefaultFlow());
		}).collect(Collectors.toList());
		List<ProcessPreEngine.Line> flowList = line.stream().map(e -> {
			ProcessPreEngine.Line item = new ProcessPreEngine.Line(e.getTagId(), e.getSourceRef(), e.getTargetRef(),
					null);
			if (StringUtils.isNotBlank(e.getConditionType())) {
				// 设置条件表达式
				item.setExpression(BpmnModelConvert.getLineExpression(e));
			}
			return item;
		}).collect(Collectors.toList());
		ProcessPreEngine instance = processPreEngineFactory.createProcessPreInstance(flowNodeList, flowList);
		List<ProcessPreEngine.Node> list = instance.startProcess(variables);
		// 获取审批候选人
		return list.stream().map(e -> {
			Optional<ProcdefNodePO> opt = node.stream().filter(t -> Objects.equals(e.getId(), t.getTagId()))
					.findFirst();
			return opt.get();
		}).filter(e -> {
			// 过滤需选择候选人节点
			return Objects.equals(e.getTagName(), "userTask") && StringUtils.isNotBlank(e.getCandidateType());
		}).map(e -> {
			CandidateVO vo = new CandidateVO();
			vo.setName(e.getName());
			vo.setFormKey(e.getTagId() + TaskVariables.VAR_TASK_ASSIGNEE_SUFFIX);
			vo.setCandidates(candidateFacade.invoke(e.getCandidateType(), user, e.getCandidateValue()));
			return vo;
		}).collect(Collectors.toList());
	}

	private void dealMessage(String deployKey, Task task) {
		// 更新待办状态
		ProcdefPO def = procdefService.getByDeployId(deployKey);
		String suffix = ProcessConstant.CATEGORY_RETRY.equals(task.getCategory()) ? ProcessConstant.EVENT_RETRY_SUFFIX
				: ProcessConstant.EVENT_START_SUFFIX;
		String eventCode = ProcessUtils.getEventCode(def, suffix);
		messageService.dealMessage(new DealMessageInformation(eventCode, task.getId(), 1));
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void complete(String taskId, TaskCompleteDTO dto, UserInformation user) {
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		if (task == null) {
			throw new ResultException("任务不存在！");
		}
		if (!Objects.equals(task.getAssignee(), String.valueOf(user.getStaffId()))) {
			throw new ResultException("无权审批该任务！");
		}
		// 保存执行记录
		RuProcessPO process = ruProcessService.getByInstId(task.getProcessInstanceId());
		hiTaskService.saveOnComplete(process, dto, task, user);
		// 完成任务
		taskService.complete(taskId, dto.getVariables());
		dealMessage(process.getProcDefKey(), task);
	}

	@Override
	public void reject(String taskId, RejectTaskDTO dto, UserInformation user) {
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		if (task == null) {
			throw new ResultException("任务不存在！");
		}
		RuProcessPO ruProcess = ruProcessService.getByInstId(task.getProcessInstanceId());
		if (StringUtils.isBlank(dto.getRejectType())) {
			ProcdefNodePO node = procdefService.getActiveProcdefNode(ruProcess.getProcDefKey(),
					task.getTaskDefinitionKey());
			dto.setRejectType(node.getRejectType());
		}
		String targetId = null;
		switch (dto.getRejectType()) {
		case "close":
			// 结束流程
			taskService.setVariable(taskId, TaskVariables.VAR_EARLY_END, true);
			targetId = ProcessConstant.END_EVENT_ID;
			break;
		case "to_initiator":
			// 驳回至申请人
			targetId = ProcessConstant.SUBMIT_TASK_ID;
			break;
		case "custom":
			// 自选节点
			targetId = dto.getTargetId();
		default:
			taskService.setVariable(taskId, TaskVariables.VAR_EARLY_END, true);
			targetId = ProcessConstant.END_EVENT_ID;
			break;
		}
		hiTaskService.saveOnReject(ruProcess, dto, task, user);
		// 更新重试次数
		Integer retryCnt = (Integer) taskService.getVariable(task.getId(), TaskVariables.VAR_RETRY_CNT);
		taskService.setVariable(taskId, TaskVariables.VAR_RETRY_CNT, ++retryCnt);
		// 跳转流程
		managementService.executeCommand(new RejectedCommand(taskId, targetId));
		// 更新待办状态
		dealMessage(ruProcess.getProcDefKey(), task);
	}

	@Override
	public IPage<TaskVO> listMyTask(QueryMyTaskDTO dto, UserInformation user) {
		return ruProcessService.listMyTask(dto, user);
	}

	@Override
	public Payload getVariables(String taskId) {
		HistoricTaskInstance t = historyService.createHistoricTaskInstanceQuery().taskId(taskId).singleResult();
		if (null == t.getEndTime()) {
			// 执行中任务
			return getRunTaskVariables(taskId);
		} else {
			// 历史任务
			return getHistoryTaskVariables(t.getProcessInstanceId());
		}
	}

	private Payload getHistoryTaskVariables(String processInstanceId) {
		Payload data = new Payload();
		try {
			List<HistoricVariableInstance> varList = historyService.createHistoricVariableInstanceQuery()
					.processInstanceId(processInstanceId).list();
			// 获取流程信息
			RuProcessPO ruProcess = ruProcessService.getByInstId(processInstanceId);
			List<FormItemPO> formItem = formFacade.listByFormId(ruProcess.getFormId());
			data.set("process", ruProcess);
			data.set("formdef", formFacade.getFormdefById(ruProcess.getFormId()));
			data.set("formItems", formItem);
			// 表单变量
			Map<String, Object> variables = varList.stream()
					.collect(Collectors.toMap(e -> e.getVariableName(), e -> e.getValue()));

			data.set("formValues", getFormValues(variables, formItem));
			// 流程信息
			data.set("tasks", hiTaskService.listByProcessId(ruProcess.getId()));
		} catch (Exception e) {
			throw new RuntimeSqlException("查询任务详情失败！", e);
		}
		return data;
	}

	private Payload getRunTaskVariables(String taskId) {
		Payload data = new Payload();
		try {
			Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
			RuProcessPO ruProcess = ruProcessService.getByInstId(task.getProcessInstanceId());
			// 获取表单信息
			List<FormItemPO> formItem = formFacade.listByFormId(ruProcess.getFormId());
			data.set("process", ruProcess);
			data.set("taskdef",
					procdefService.getActiveProcdefNode(ruProcess.getProcDefKey(), task.getTaskDefinitionKey()));
			data.set("formdef", formFacade.getFormdefById(ruProcess.getFormId()));
			data.set("formItems", formItem);
			// 表单变量
			data.set("formValues", getFormValues(taskService.getVariables(taskId), formItem));
			// 流程信息
			List<HiTaskPO> hiTaskList = hiTaskService.listByProcessId(ruProcess.getId());
			hiTaskList.add(hiTaskService.getByTask(task));
			data.set("tasks", hiTaskList);
		} catch (Exception e) {
			throw new RuntimeException("查询任务详情失败！", e);
		}
		return data;
	}

	private Map<String, Object> getFormValues(Map<String, Object> variables, List<FormItemPO> formItem) {
		Map<String, Object> map = new HashMap<>();
		for (FormItemPO e : formItem) {
			if (variables.containsKey(e.getFormKey())) {
				map.put(e.getFormKey(), variables.get(e.getFormKey()));
				// 业务字段描述
				String variableLabelKey = e.getFormKey() + ProcessConstant.VARIABLE_LABEL_SUFFIX;
				if (variables.containsKey(variableLabelKey)) {
					map.put(variableLabelKey, variables.get(variableLabelKey));
				}
			}
		}
		return map;
	}

	@Override
	public Payload getVariablesByProcess(Integer processId) {
		RuProcessPO ruProcess = ruProcessService.getById(processId);
		List<Task> list = taskService.createTaskQuery().processInstanceId(ruProcess.getProcInstId()).list();
		Payload data = null;
		if (list != null && list.size() > 0) {
			data = getRunTaskVariables(list.get(0).getId());
		} else {
			data = getHistoryTaskVariables(ruProcess.getProcInstId());
		}
		data.set("process", ruProcess);
		return data;
	}

	@Override
	public IPage<HiTaskVO> listMyHiTask(QueryMyTaskDTO dto, UserInformation user) {
		return ruProcessService.listMyHiTask(dto, user);
	}

	@Override
	public void cancel(Integer processId) {
		RuProcessPO ruProcess = ruProcessService.getById(processId);
		List<Task> list = taskService.createTaskQuery().processInstanceId(ruProcess.getProcInstId()).list();
		if (list.size() > 0) {
			for (Task task : list) {
				taskService.setVariable(task.getId(), TaskVariables.VAR_EARLY_END, true);
				taskService.setVariable(task.getId(), TaskVariables.VAR_EARLY_END_MESSAGE, "员工取消申请");
				managementService.executeCommand(new RejectedCommand(task.getId(), ProcessConstant.END_EVENT_ID));
				// 更新待办状态
				dealMessage(ruProcess.getProcDefKey(), task);
			}
		}
	}

}
