package com.bjxczy.onepark.workorder.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @ClassName RemoveStaffEvent
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/29 15:39
 * @Version 1.0
 */
@Getter
public class RemoveStaffEvent extends ApplicationEvent {
    private Integer staffId;
    public RemoveStaffEvent(Object source) {
        super(source);
        this.staffId=(Integer) source;
    }
}
