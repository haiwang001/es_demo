package com.bjxczy.onepark.workorder.interfaces.controller.authorized;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.workorder.pojo.dto.WoEodDTO;
import com.bjxczy.onepark.workorder.pojo.dto.WoEodStaffScheduleDTO;
import com.bjxczy.onepark.workorder.pojo.vo.EodVO;
import com.bjxczy.onepark.workorder.pojo.vo.WoEodStaffScheduleVO;
import com.bjxczy.onepark.workorder.service.WoEodService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @ClassName WoEodController
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/27 18:19
 * @Version 1.0
 */
@ApiResourceController
@Api(tags = "班组模块接口")
@RequestMapping("/eod")
public class WoEodController {

    @Autowired
    private WoEodService woEodService;

    @ApiOperation("pc端-新增班组")
    @PostMapping(value = "/create", name = "pc端-新增班组")
    public void createEod(@RequestBody WoEodDTO woEodDTO) {
        woEodService.createEod(woEodDTO);
    }

    @ApiOperation("pc端-班组列表")
    @GetMapping(value = "/list", name = "pc端-班组列表")
    public Page<EodVO> getEodList(WoEodDTO woEodDTO) {
        return woEodService.getEodList(woEodDTO);
    }

    @ApiOperation("pc端-班组管理")
    @PutMapping(value = "/update", name = "pc-班组管理")
    public void updateEod(@RequestBody WoEodDTO woEodDTO) {
        woEodService.updateEod(woEodDTO);
    }

    @ApiOperation("pc端-值班管理-值班排序")
    @PostMapping(value = "/schedule/sort", name = "pc端-值班管理-值班排序")
    public Map<String,Object> sort(@RequestBody List<String> staffNames) {
        return woEodService.sort(staffNames);
    }
    @ApiOperation("pc端-查询部门人员是否为班组人员")
    @GetMapping(value = "/isEodStaff",name = "pc端-查询部门人员是否为班组人员")
    public Boolean isEodStaff(@RequestParam("staffId") Integer staffId){
        return woEodService.isEodStaff(staffId);
    }

    @ApiOperation("pc端-删除班组")
    @DeleteMapping(value = "/delByIds",name = "pc端-删除班组")
    public void delWoEod(@RequestBody List<Integer> ids){
        woEodService.delWoEod(ids);
    }

    @ApiOperation("定时任务：每月一号自动生成本月的排班")
    @GetMapping("/generate/schedule")
    public void generateSchedule(){
        woEodService.generateSchedule(null);
    }

    @ApiOperation("pc端-导出班组列表数据")
    @PostMapping(value = "/export",name = "pc端-导出班组列表数据")
    public void export(HttpServletResponse response){
        woEodService.export(response);
    }

    @ApiOperation("移动端-查询值班管理列表")
    @GetMapping(value = "/mob/list",name = "移动端-查询值班管理列表")
    public List<WoEodStaffScheduleVO> mobList(WoEodStaffScheduleDTO dto){
        return woEodService.mobList(dto);
    }
}
