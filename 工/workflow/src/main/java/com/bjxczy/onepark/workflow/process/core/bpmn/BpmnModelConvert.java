package com.bjxczy.onepark.workflow.process.core.bpmn;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.activiti.bpmn.model.ActivitiListener;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.EndEvent;
import org.activiti.bpmn.model.ExclusiveGateway;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.ImplementationType;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.StartEvent;
import org.activiti.bpmn.model.UserTask;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.alibaba.excel.util.StringUtils;
import com.alibaba.fastjson2.JSON;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.workflow.process.core.condition.DictExpression;
import com.bjxczy.onepark.workflow.process.core.condition.FormItemExpression;
import com.bjxczy.onepark.workflow.process.core.constant.ProcessConstant;
import com.bjxczy.onepark.workflow.process.core.constant.TaskListenerType;
import com.bjxczy.onepark.workflow.process.core.constant.TaskVariables;
import com.bjxczy.onepark.workflow.process.entity.ProcdefLinePO;
import com.bjxczy.onepark.workflow.process.entity.ProcdefPO;

import lombok.SneakyThrows;

public class BpmnModelConvert {

	@SuppressWarnings("unchecked")
	@SneakyThrows
	public static BpmnModel xml2Model(String xml, ProcdefPO def, Map<String, ProcdefLinePO> lineConfig) {
		Document doc = DocumentHelper.parseText(xml);
		BpmnModel model = new BpmnModel();
		Process process = new Process();
		// 解析XML
		Element definitions = doc.getRootElement();
		Element elProcess = definitions.element("process");
		// 添加流程节点
		List<Element> elements = elProcess.elements();
		List<FlowElement> flowElements = elements.stream().map(e -> xml2Element(e)).collect(Collectors.toList());
		// 重新构造流程
		refactorFlow(flowElements, lineConfig);
		flowElements.forEach(e -> process.addFlowElement(e));
		// 基本信息设置
		process.setId(def.getDeployId());
		process.setName(def.getName());
		process.setExecutable(true);
		model.addProcess(process);
		return model;
	}

	/**
	 * 重构流程
	 * 
	 * @param flowElements
	 * @param lineConfig
	 */
	private static void refactorFlow(List<FlowElement> flowElements, Map<String, ProcdefLinePO> lineConfig) {
		// 添加用户提交任务
		addSubmitTask(flowElements);
		// 结束节点重定向
		refactorEndElement(flowElements);
		// 网关设置默认分支
		setGatewayDefaultFlow(flowElements, lineConfig);
		// 设置分支条件
		setLineCondition(flowElements, lineConfig);
	}

	/**
	 * 在startEvent后增加userTask，用于驳回至申请人
	 * 
	 * @param flowElements
	 */
	private static void addSubmitTask(List<FlowElement> flowElements) {
		Optional<FlowElement> opt = flowElements.stream().filter(e -> e instanceof StartEvent).findFirst();
		List<FlowElement> list = flowElements.stream().filter(e -> e instanceof StartEvent)
				.collect(Collectors.toList());
		assertTrue(list.size() == 1, "流程需有且只有一个开始节点！");
		// 员工申请任务
		UserTask submitTask = getSubmitTask();
		opt.ifPresent(startEvent -> {
			String startId = startEvent.getId();
			// 将开始节点与员工申请任务连线
			SequenceFlow toRetryTask = new SequenceFlow(startId, submitTask.getId());
			// 获取开始节点连线
			List<SequenceFlow> lines = filterSequenceFlow(flowElements, startId, f -> f.getSourceRef());
			// 重新连线
			for (SequenceFlow line : lines) {
				line.setSourceRef(submitTask.getId());
			}
			flowElements.add(submitTask);
			flowElements.add(toRetryTask);
		});
	}

	/**
	 * 重新设置结束节点ID，用于驳回时直接结束流程
	 * 
	 * @param flowElements
	 */
	private static void refactorEndElement(List<FlowElement> flowElements) {
		List<FlowElement> list = flowElements.stream().filter(e -> e instanceof EndEvent).collect(Collectors.toList());
		assertTrue(list.size() == 1, "流程中需有且只有一个结束节点！");
		FlowElement endEvent = list.get(0);
		List<SequenceFlow> lines = filterSequenceFlow(flowElements, endEvent.getId(), f -> f.getTargetRef());
		endEvent.setId(ProcessConstant.END_EVENT_ID);
		lines.forEach(e -> e.setTargetRef(ProcessConstant.END_EVENT_ID));
	}

	/**
	 * 设置网关默认分支
	 * 
	 * @param flowElements
	 * @param lineConfig
	 */
	private static void setGatewayDefaultFlow(List<FlowElement> flowElements, Map<String, ProcdefLinePO> lineConfig) {
		List<ExclusiveGateway> gatewayList = flowElements.stream().filter(e -> e instanceof ExclusiveGateway)
				.map(e -> (ExclusiveGateway) e).collect(Collectors.toList());
		for (ExclusiveGateway item : gatewayList) {
			List<SequenceFlow> lines = filterSequenceFlow(flowElements, item.getId(), f -> f.getSourceRef());
			for (SequenceFlow line : lines) {
				ProcdefLinePO config = lineConfig.get(line.getId());
				// retryTask连线无法找到对应配置，产生空指针异常，提前判断
				if (config != null && Objects.equals("else", config.getConditionType())) {
					item.setDefaultFlow(line.getId());
					break;
				}
				assertTrue(StringUtils.isBlank(item.getDefaultFlow()), item.getName() + "默认分支未设置！");
			}
		}
	}

	private static List<SequenceFlow> filterSequenceFlow(List<FlowElement> flowElements, String id,
			Function<SequenceFlow, String> func) {
		return flowElements.stream().filter(e -> {
			if (e instanceof SequenceFlow) {
				return Objects.equals(id, func.apply((SequenceFlow) e));
			}
			return false;
		}).map(e -> (SequenceFlow) e).collect(Collectors.toList());
	}

	/**
	 * 设置连线判断条件
	 * 
	 * @param flowElements
	 * @param lineConfig
	 */
	private static void setLineCondition(List<FlowElement> flowElements, Map<String, ProcdefLinePO> lineConfig) {
		List<SequenceFlow> lines = flowElements.stream().filter(e -> e instanceof SequenceFlow)
				.map(e -> (SequenceFlow) e).collect(Collectors.toList());
		for (SequenceFlow line : lines) {
			ProcdefLinePO config = lineConfig.get(line.getId());
			// retryTask连线无法找到对应配置，产生空指针异常，提前判断
			if (config != null && StringUtils.isNotBlank(config.getConditionType())) {
				line.setConditionExpression(getLineExpression(config));
			}
		}
	}

	/**
	 * 生成条件表达式
	 * @param line
	 * @return
	 */
	public static String getLineExpression(ProcdefLinePO line) {
		String conditionExpression = null;
		switch (line.getConditionType()) {
		case "formItem":
			// 表单字段条件
			FormItemExpression expression = JSON.parseObject(line.getExpression(), FormItemExpression.class);
			conditionExpression = expression.toConditionExpression();
			break;
		case "else":
			conditionExpression = line.getExpression();
			break;
		default:
			DictExpression dictExpression = JSON.parseObject(line.getExpression(), DictExpression.class);
			conditionExpression = dictExpression.toConditionExpression();
			break;
		}
		return conditionExpression;
	}

	/**
	 * xml转换为activiti对象
	 * 
	 * @param el
	 * @param nodeConfig
	 * @return
	 */
	private static FlowElement xml2Element(Element el) {
		String tagName = el.getName(), id = el.attributeValue("id");
		FlowElement flowElement = null;
		switch (tagName) {
		case "startEvent":
			StartEvent start = new StartEvent();
			// 设置启动人变量
			start.setInitiator(TaskVariables.VAR_INITIATOR_KEY);
			flowElement = start;
			break;
		case "endEvent":
			EndEvent end = new EndEvent();
			// 设置流程结束Listener
			end.setExecutionListeners(Arrays.asList(getListener("end", TaskListenerType.PROCESS_END_LISTENER)));
			flowElement = end;
			break;
		case "sequenceFlow":
			SequenceFlow flow = new SequenceFlow();
			flow.setSourceRef(el.attributeValue("sourceRef"));
			flow.setTargetRef(el.attributeValue("targetRef"));
			flowElement = flow;
			break;
		case "userTask":
			UserTask usertask = new UserTask();
			usertask.setCategory(ProcessConstant.CATEGORY_APPROVAL);
			usertask.setTaskListeners(Arrays.asList(getListener("create", TaskListenerType.USER_TASK_START_LISTENER)));
			usertask.setAssignee("${" + id + TaskVariables.VAR_TASK_ASSIGNEE_SUFFIX + "}");
			flowElement = usertask;
			break;
		case "exclusiveGateway":
			ExclusiveGateway exclusiveGateway = new ExclusiveGateway();
			flowElement = exclusiveGateway;
		default:
			break;
		}
		// 通用字段设置
		flowElement.setId(id);
		flowElement.setName(el.attributeValue("name"));
		Element documentation = el.element("documentation");
		if (documentation != null) {
			flowElement.setDocumentation(documentation.getText());
		}
		return flowElement;
	}

	private static ActivitiListener getListener(String event, String implementation) {
		ActivitiListener listener = new ActivitiListener();
		listener.setEvent(event);
		listener.setImplementationType(ImplementationType.IMPLEMENTATION_TYPE_DELEGATEEXPRESSION);
		listener.setImplementation("${" + implementation + "}");
		return listener;
	}
	
	private static UserTask getSubmitTask() {
		UserTask submitTask = new UserTask();
		submitTask.setId(ProcessConstant.SUBMIT_TASK_ID);
		submitTask.setName("调整申请");
		submitTask.setCategory(ProcessConstant.CATEGORY_RETRY);
		submitTask.setAssignee("${" + TaskVariables.VAR_INITIATOR_KEY + "}"); // 用户申请任务的处理人为申请人
		submitTask.setTaskListeners(Arrays.asList(getListener("create", TaskListenerType.SUBMIT_TASK_LISTENER)));
		return submitTask;
	}

	private static void assertTrue(boolean success, String message) {
		if (!success) {
			throw new ResultException(message);
		}
	}

}
