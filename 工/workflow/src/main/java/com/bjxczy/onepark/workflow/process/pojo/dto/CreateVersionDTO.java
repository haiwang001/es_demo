package com.bjxczy.onepark.workflow.process.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateVersionDTO {
	
	private String version;
	
	private String remark;
	
	private String formId;

}
