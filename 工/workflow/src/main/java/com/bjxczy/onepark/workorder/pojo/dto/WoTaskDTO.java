package com.bjxczy.onepark.workorder.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.bjxczy.onepark.workorder.pojo.po.WoEodStaffPO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @ClassName WoTaskDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/31 10:51
 * @Version 1.0
 */
@Data
public class WoTaskDTO extends BasePageDTO{

    private Integer id;

    @ApiModelProperty(value = "工单名称")
    private String name;

    @ApiModelProperty(value = "工单配置ID")
    private String woId;

    @ApiModelProperty(value = "工单编号")
    private String sequenceNo;

    @ApiModelProperty(value = "工单类型")
    private String orderType;

    @ApiModelProperty(value = "业务模块")
    private String businessType;

    @ApiModelProperty(value = "紧急程度")
    private String orderLevel;

    @ApiModelProperty(value = "接单类型")
    private String taskType;

    @ApiModelProperty(value = "部门ID")
    private Integer assignDeptId;

    @ApiModelProperty(value = "部门名称")
    private String assignDeptName;

    @ApiModelProperty(value = "工单详述")
    private String description;

    @ApiModelProperty(value = "重复类型")
    private String repeatType;

    @ApiModelProperty(value = "过期时间（分钟）")
    private Integer expireTime;

    @ApiModelProperty(value = "发起员工ID")
    private Integer initiatorId;

    @ApiModelProperty(value = "发起员工姓名")
    private String initiatorName;

    @ApiModelProperty(value = "区域Id")
    private Integer areaId;

    @ApiModelProperty(value = "局址Id")
    private String tenantId;

    @ApiModelProperty(value = "空间Id")
    private String spaceId;

    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    @ApiModelProperty(value = "执行时间")
    private List<String> execTimes;

    @ApiModelProperty(value = "部门主管ID")
    private Integer ownerId;

    @ApiModelProperty(value = "部门主管姓名")
    private String ownerName;

    @ApiModelProperty(value = "受理人姓名")
    private String assigneeName;

    @ApiModelProperty(value = "派单(包含转单)、抢单")
    private String woTaskType;

    @ApiModelProperty(value = "执行工单状态 0：已创建、1：执行中")
    private String taskStatus;

    @ApiModelProperty(value = "图片")
    private Set<String> repairPicture;

    @ApiModelProperty(value = "驳回原因")
    private String rejectReason;

    @ApiModelProperty(value = "已认领、转单中")
    private String woTaskAssigneeStatus;

    @ApiModelProperty(value = "报修设备编码")
    private String deviceCode;

    @ApiModelProperty(value = "报修设备类型")
    private String category;

    @ApiModelProperty(value = "报修备注")
    private String remark;

}
