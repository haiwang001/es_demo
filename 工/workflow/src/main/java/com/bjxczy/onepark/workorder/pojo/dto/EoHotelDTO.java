package com.bjxczy.onepark.workorder.pojo.dto;

import com.bjxczy.onepark.workorder.pojo.po.EoAccommodationPO;
import com.bjxczy.onepark.workorder.pojo.po.EoHotelPO;
import lombok.Data;

import java.util.List;

/**
 * @ClassName EoHotelDTO
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/22 14:40
 * @Version 1.0
 */
@Data
public class EoHotelDTO extends EoHotelPO {
    // 住宿相关
    private List<EoAccommodationPO> eoAccommodationPOS;
    // 会议相关
    private List<EoMettingDTO> eoMettingPOS;
}
