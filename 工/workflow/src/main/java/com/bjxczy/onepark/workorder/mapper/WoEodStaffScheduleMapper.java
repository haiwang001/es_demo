package com.bjxczy.onepark.workorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.common.model.scheduling.OnDutyStaffInfo;
import com.bjxczy.onepark.workorder.pojo.po.WoEodStaffSchedulePO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface WoEodStaffScheduleMapper extends BaseMapper<WoEodStaffSchedulePO> {
    @Select("select * from wo_eod_staff_schedule where eod_id=#{eodId} and #{time} >=start_time and #{time}<= end_time;")
    OnDutyStaffInfo getOnDutyStaffInfo(@Param("eodId") Integer eodId, @Param("time") String time);

}
