package com.bjxczy.core.feign.client.car;

import org.springframework.web.bind.annotation.GetMapping;

import com.bjxczy.onepark.common.model.car.CarCertificateInfo;
import org.springframework.web.bind.annotation.PathVariable;


public interface CarCertificateFeignClient {

	@GetMapping("/rpc/carCertificate/isCarCertificate/{plateNumber}/{parkId}")
	Boolean isCarCertificate(@PathVariable("plateNumber")String plateNumber, @PathVariable("parkId")String parkId);
	
	
	@GetMapping("/rpc/carCertificate/getByPlateNumber/{plateNumber}")
	CarCertificateInfo getByPlateNumber(@PathVariable("plateNumber")String plateNumber);

	@GetMapping("/rpc/carCertificate/getByPlateNumberNew/{plateNumber}")
	CarCertificateInfo getByPlateNumberNew(@PathVariable("plateNumber")String plateNumber);


}
