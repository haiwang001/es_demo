package com.bjxczy.core.feign.client.scheduling;

import com.bjxczy.onepark.common.model.scheduling.OnDutyStaffInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface BaseSchedulingFeignClient {

	@GetMapping("/rpc/workGroupSchedule/getOnDutyStaffInfo")
	OnDutyStaffInfo getOnDutyStaffInfo(@RequestParam("groupId") Integer groupId,@RequestParam("seqNum") Integer seqNum, @RequestParam("time")String time);

}
