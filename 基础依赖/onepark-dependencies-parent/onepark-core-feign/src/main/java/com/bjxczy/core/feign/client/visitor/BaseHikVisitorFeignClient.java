package com.bjxczy.core.feign.client.visitor;


import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/*
 *@Author wlw
 *@Date 2023/6/25 17:08
 */
public interface BaseHikVisitorFeignClient {
    @GetMapping("/rpc/hikvisitor/createVisitor/{receptionistId}/{visitStartTime}/{visitEndTime}/{visitorName}/{mobile}/{gender}/{visitorPhoto}")
    CreateVisitorAndDoorVo createVisitor(@PathVariable("receptionistId") String receptionistId,
                                         @PathVariable("visitStartTime") String visitStartTime,
                                         @PathVariable("visitEndTime") String visitEndTime,
                                         @PathVariable("visitorName") String visitorName,
                                         @PathVariable("mobile") String mobile,
                                         @PathVariable("gender") String gender,
                                         @PathVariable("visitorPhoto") String visitorPhoto);

    @GetMapping("/rpc/hikvisitor/getVisitorRecords/{visitorName}/{visitStartTimeBegin}/{visitStartTimeEnd}/{orderId}")
    Boolean getVisitorRecords(@PathVariable("visitorName") String visitorName,
                              @PathVariable("visitStartTimeBegin") String visitStartTimeBegin,
                              @PathVariable("visitStartTimeEnd") String visitStartTimeEnd,
                              @PathVariable("orderId") String orderId);
}
