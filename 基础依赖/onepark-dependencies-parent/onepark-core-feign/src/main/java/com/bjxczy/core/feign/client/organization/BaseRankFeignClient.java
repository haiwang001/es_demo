package com.bjxczy.core.feign.client.organization;

import com.bjxczy.onepark.common.model.organization.OrganizationTree;
import com.bjxczy.onepark.common.model.organization.RankInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface BaseRankFeignClient {

    /**
     * 职位列表
     * @return
     */
    @GetMapping("/rpc/rank/getRankLists")
    List<RankInfo> getRankLists();

    /**
     * 查询部门架构
     * @return
     */
    @GetMapping("/rpc/rank/findOrgTree")
    List<OrganizationTree> findOrgTree();

    /**
     * @Description: 根据部门ID查询部门名称回显
     * @Param: deptId
     * @return: java.lang.String
     */
    @GetMapping("/rpc/rank/findOrgDeptNameById")
    String findOrgDeptNameById(@RequestParam("deptId") Integer deptId);

    /**
     * 根据部门名称查询部门Id
     * @param deptName
     * @return
     */
    @GetMapping("/rpc/rank/findOrgDeptIdByDeptName")
    Integer findOrgDeptIdByName(@RequestParam("deptName")String deptName);
}
