package com.bjxczy.core.feign.client.pcs;

import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

public interface FileFeignClient {
	
	/**
	 * 上传文件
	 * @param file 
	 * @return
	 */
	
	@PostMapping(value = "/rpc/pcs/uploadFile",produces = MediaType.APPLICATION_PROBLEM_JSON_VALUE,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	String fileUpload(@RequestPart("file") MultipartFile file);
}
