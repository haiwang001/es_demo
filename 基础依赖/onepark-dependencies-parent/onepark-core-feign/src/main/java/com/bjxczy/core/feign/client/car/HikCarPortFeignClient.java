package com.bjxczy.core.feign.client.car;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.model.car.CarCertificate;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Auther: Administrator
 * @Date: 2023/3/31 0031 17:55
 * @Description: HikCarPortFeignClient
 * @Version 1.0.0
 */
public interface HikCarPortFeignClient {
    @PostMapping("/rpc/car/queryCarport")
    JSONObject queryCarport(@RequestBody CarCertificate carCertificate );
}
