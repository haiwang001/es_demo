package com.bjxczy.core.feign.client.voucher;

import com.bjxczy.onepark.common.model.voucher.StaffVoucherInfo;
import com.bjxczy.onepark.common.resp.R;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

public interface BaseVoucherFeignClient {

    @PostMapping(value = "/rpc/bmpvoucher/upload", produces = MediaType.APPLICATION_PROBLEM_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    R<?> upload(@RequestPart("file") MultipartFile file) throws Exception;

    @GetMapping("/rpc/bmpvoucher/getByStaffId/{staffId}")
    StaffVoucherInfo getByStaffId(@PathVariable("staffId") Integer staffId);
}
