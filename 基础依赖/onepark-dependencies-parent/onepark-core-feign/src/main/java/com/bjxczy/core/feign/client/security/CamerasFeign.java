package com.bjxczy.core.feign.client.security;

import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.security.CameraInformation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface CamerasFeign {

    @GetMapping("/rpc/security/cameras")
    List<CameraInformation> allCameras();
    @GetMapping("/rpc/security/updatePlanStatus/{planId}/{status}")
    void update(@PathVariable("planId") Integer planId,@PathVariable("status") Integer status);

}
