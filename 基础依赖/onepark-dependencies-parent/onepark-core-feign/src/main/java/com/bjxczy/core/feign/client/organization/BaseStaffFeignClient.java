package com.bjxczy.core.feign.client.organization;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.bjxczy.onepark.common.model.organization.QueryStaffParams;
import com.bjxczy.onepark.common.model.organization.QueryTopStaffParams;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;

public interface BaseStaffFeignClient {

    /**
     * 根据ID查询员工
     *
     * @param id
     * @return
     */
    @GetMapping("/rpc/staff/byId/{id}")
    StaffInformation findById(@PathVariable("id") Integer id);

    /**
     * @param id 员工id
     * @return 返回更加细致的员工信息
     */
    @GetMapping("/rpc/staff/getStaffById/{id}")
    StaffInfoFine getStaffById(@PathVariable("id") Integer id);

    /**
     * 根据手机号查询员工
     *
     * @param mobile
     * @return
     */
    @GetMapping("/rpc/staff/byMobile/{mobile}")
    StaffInformation findByMobile(@PathVariable("mobile") String mobile);

    /**
     * 根据uuid查询员工
     *
     * @param uuid
     * @return
     */
    @GetMapping("/rpc/staff/byUuid/{uuid}")
    StaffInformation findByUuid(@PathVariable("uuid") String uuid);


    /**
     * 根据名称获取员工信息
     *
     * @param name
     * @return
     */
    @GetMapping("/rpc/staff/getStaffByPersonName/{name}")
    List<StaffInformation> getStaffByPersonName(@PathVariable("name") String name);

    /**
     * 查询所有部门下子部门ID
     * @param id
     * @return
     */
    @GetMapping("/rpc/organization/listChildrenId")
    List<Integer> listOrgChildrenId(@RequestParam("organizationId") Integer organizationId);

    /**
     * 根据组织架构获取员工信息
     *
     * @param organizationId
     * @return
     */
    @GetMapping("/rpc/staff/getStaffs/{organizationId}")
    List<StaffInformation> getStaffs(@PathVariable("organizationId") Integer organizationId);
    
    /**
     * 批量获取员工列表
     * @param ids
     * @return
     */
    @PostMapping("/rpc/staff/getStaffBatch")
    List<StaffInformation> getStaffBatch(@RequestBody List<Integer> ids);
    
    /**
     * N 通过部门、职级、岗位过滤员工数据
     * @param params
     * @return
     */
    @PostMapping("/rpc/staff/listByParams")
    List<StaffInformation> listStaffByParams(@RequestBody QueryStaffParams params);
    
    /**
     * N 查询员工上级部门某职级、岗位员工
     * @param staffId
     * @param params
     * @return
     */
    @PostMapping("/rpc/staff/listTopDeptByParams")
    List<StaffInformation> listTopDeptStaff(@RequestParam("staffId") Integer staffId, @RequestBody QueryTopStaffParams params);
    
}
