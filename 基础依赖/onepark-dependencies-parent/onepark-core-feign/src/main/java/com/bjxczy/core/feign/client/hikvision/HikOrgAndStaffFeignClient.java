package com.bjxczy.core.feign.client.hikvision;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface HikOrgAndStaffFeignClient {
	
	/**
	 * 根据部门id查询海康是是否存在该部门
	 * @param orgId
	 * @return
	 */
	@GetMapping("/rpc/hik/org/isOrgHik")
	Boolean isOrgHik(@RequestParam("orgId") Integer orgId);

	/**
	 * 根据员工id查询海康是是否存在该员工
	 * @param staffId
	 * @return
	 */
	@GetMapping("/rpc/hik/org/isStaffHik")
	Boolean isStaffHik(@RequestParam("staffId") Integer staffId);
}
