package com.bjxczy.core.feign.client.device;

import com.bjxczy.onepark.common.model.device.DeviceInformation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

public interface SysDeviceFeignClient {
    @PostMapping("/rpc/device/sysDeviceInfo")
    void sysDeviceInfo(List<DeviceInformation> deviceInformationList);

    @PostMapping("/rpc/device/sysDeviceStatus")
    void sysDeviceStatus(List<DeviceInformation> deviceInformationList);
}
