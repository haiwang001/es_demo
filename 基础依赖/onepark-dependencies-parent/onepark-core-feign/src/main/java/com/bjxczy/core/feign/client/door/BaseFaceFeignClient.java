package com.bjxczy.core.feign.client.door;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bjxczy.onepark.common.model.door.StaffFaceInformation;

public interface BaseFaceFeignClient {

	/**
	 * 获取人脸信息
	 * @param id 员工ID
	 * @return
	 */
    @GetMapping("/rpc/face/getOne/{id}")
    StaffFaceInformation getByStaffId(@PathVariable("id") Integer id);
    
    /**
     * 批量获取人脸信息
     * @param idList 员工ID数组
     * @return
     */
    @PostMapping("/rpc/face/getBatch")
    List<StaffFaceInformation> listByStaffId(@RequestBody List<Integer> idList);
    
	
}
