package com.bjxczy.core.feign.client.authority;

import java.util.List;

import com.bjxczy.onepark.common.model.authority.AreaInformation;
import com.bjxczy.onepark.common.model.authority.FloorInformation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.bjxczy.onepark.common.model.authority.SpaceInformation;
import com.bjxczy.onepark.common.model.authority.SpaceTree;

/**
 * @ClassName BaseSpaceFeignClient
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/6 10:06
 * @Version 1.0
 */
public interface BaseSpaceFeignClient {

    @GetMapping("/rpc/space/getPosition")
    String getPosition(@RequestParam("spaceId") String spaceId);

    @GetMapping("/rpc/space/getPositionByAreaId{areaId}")
    String getPositionByAreaId(@PathVariable("areaId") Integer areaId);

    @GetMapping("/rpc/space/getPositionByBuildingId{buildingId}")
    String getPositionByBuildingId(@PathVariable("buildingId")String buildingId);

    @GetMapping("/rpc/space/getSpaceName")
    String getSpaceName(@RequestParam("spaceId") String spaceId);

    @GetMapping("/rpc/space/getSpaceIds")
    List<String> getSpaceIds(@RequestParam("spaceId") String spaceId);

    /**
     *  查询全部局址
     * @return
     */
    @GetMapping("/rpc/space/list")
    List<SpaceInformation> listSpace();

    /**
     * 查询所有区域
     * @return
     */
    @GetMapping("/rpc/area/list")
    List<AreaInformation> listArea();

    @GetMapping("/rpc/space/floorListByAreaId")
    List<String> floorList(@RequestParam("areaId") Integer areaId);

    @GetMapping("/rpc/space/getBuildingNameById/{buildingId}")
    String getBuildingName(@PathVariable("buildingId") String buildingId);

    @GetMapping("/rpc/space/getAreaInfoById")
    AreaInformation getAreaInfoById(@RequestParam(value = "areaId",required = false)Integer areaId,
                                    @RequestParam(value = "areaNo",required = false)String areaNo);
    @GetMapping("/rpc/space/getBuildingInfoById}")
    SpaceInformation getBuildingInfoById(@RequestParam(value = "buildingId",required = false)String buildingId,
                                         @RequestParam(value = "buildingNo",required = false)String buildingNo);
    @GetMapping("/rpc/space/getFloorInfoById}")
    FloorInformation getFloorInfoById(@RequestParam(value = "spaceId",required = false)String spaceId,
                                      @RequestParam(value = "spaceNo",required = false)String spaceNo);
    
    @GetMapping("/rpc/space/tree")
    List<SpaceTree> listSpaceTree();
}
