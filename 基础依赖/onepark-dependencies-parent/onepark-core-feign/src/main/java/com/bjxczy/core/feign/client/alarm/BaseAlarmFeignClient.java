package com.bjxczy.core.feign.client.alarm;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bjxczy.onepark.common.model.alarm.AlarmEventInfo;
import com.bjxczy.onepark.common.model.alarm.AlarmRecordInfo;
import com.bjxczy.onepark.common.model.alarm.CreateAlarmRecordCommand;
import com.bjxczy.onepark.common.model.alarm.QueryAlarmRecord;
import com.bjxczy.onepark.common.model.alarm.UpdateAlarmStatusCommand;

public interface BaseAlarmFeignClient {

    @PostMapping("/rpc/alarm/addEventRecord")
    void addEventRecord(@RequestBody CreateAlarmRecordCommand createAlarmRecordCommand);

    @PostMapping("/rpc/alarm/updateAlarmStatus")
    void updateAlarmStatus(@RequestBody UpdateAlarmStatusCommand command);

    @PostMapping("/rpc/alarm/queryEventRecord")
    List<AlarmRecordInfo> queryEventRecord(@RequestBody QueryAlarmRecord dto);
    
    @GetMapping("/rpc/event/{eventCode}")
    AlarmEventInfo getAlarmEvent(@PathVariable("eventCode") String eventCode);

}
