package com.bjxczy.core.feign.client.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.bjxczy.onepark.common.model.user.UserInformation;

public interface BaseUserFeignClient {

	@GetMapping("/rpc/user/byId/{id}")
	UserInformation findById(@PathVariable("id") Integer id);
	
	@GetMapping("/rpc/user/byUsername/{username}")
	UserInformation findByUsename(@PathVariable("username") String username);
	
}
