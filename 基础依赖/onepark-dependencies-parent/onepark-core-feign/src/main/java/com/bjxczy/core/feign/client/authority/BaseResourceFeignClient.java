package com.bjxczy.core.feign.client.authority;

import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.bjxczy.onepark.common.model.authority.MenuInformation;
import com.bjxczy.onepark.common.model.authority.MenuNavigatorDefinitionApisDetail;
import com.bjxczy.onepark.common.model.common.ApiResourceInformation;

public interface BaseResourceFeignClient {

	@GetMapping("/rpc/menu/listMenuApiTree")
	List<MenuNavigatorDefinitionApisDetail> listMenuApiTree();

	@GetMapping("/rpc/apis/{name}")
	List<ApiResourceInformation> listUnauthorizedApis(@PathVariable("name") String name);

	@PostMapping("/rpc/list/apis")
	List<ApiResourceInformation> listByIds(@RequestBody Set<String> ids);

	@GetMapping("/rpc/apis/all")
	List<ApiResourceInformation> listApisAll();

	@GetMapping("/rpc/menu/findByApi")
	MenuInformation findByApi(@RequestParam("serviceName") String serviceName, @RequestParam("method") String method,
			@RequestParam("path") String path);

}
