package com.bjxczy.core.feign.client.door;

import com.bjxczy.onepark.common.model.door.HikDoorDeviceInformation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface BaseHikSyncDoorDeviceFeignClient {

    @GetMapping("/rpc/device/getlist")
    List<HikDoorDeviceInformation> getDoorInformation();

    @GetMapping("/rpc/device/getDoorOnline/{ip}")
    Integer getDoorOnline(@PathVariable("ip") String ip);
}
