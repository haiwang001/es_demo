package com.bjxczy.core.feign.client.workflow;

import com.bjxczy.onepark.common.model.workflow.WoOrderdefInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface BaseWorkFlowFeignClient {

	@GetMapping("/rpc/workFlow/getOrderDefById/{id}")
	WoOrderdefInfo findById(@PathVariable("id") String id);

	@GetMapping("/rpc/workFlow/getWorkOrderDefByBusinessType/{businessType}")
	List<WoOrderdefInfo> getWorkOrderDefByBusinessType(@PathVariable("businessType") String businessType);

}
