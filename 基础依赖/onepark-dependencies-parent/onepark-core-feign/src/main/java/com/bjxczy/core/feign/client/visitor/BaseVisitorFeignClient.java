package com.bjxczy.core.feign.client.visitor;

import com.bjxczy.onepark.common.model.visitor.VisitorDtoInformation;
import com.bjxczy.onepark.common.model.visitor.VisitorInformation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.naming.Name;
import java.util.List;


public interface BaseVisitorFeignClient {
    @GetMapping("/rpc/bmpvoucher/selectVisitorByMobile/{mobile}")
    VisitorInformation selectVisitorByMobile(@PathVariable("mobile") String Mobile);

    @GetMapping("/rpc/bmpvoucher/selectVisitorByVisitToday/{day}")
    List<VisitorInformation> selectVisitorByVisitToday(@PathVariable("day") String day, @RequestParam(name = "id", required = false) String id);

    @GetMapping("/rpc/bmpvoucher/allVisitor")
    List<VisitorDtoInformation> selectAll();
}
