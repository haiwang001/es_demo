package com.bjxczy.core.feign.client.car;

import com.bjxczy.onepark.common.model.car.ParkBlackWhiteListInfo;
import com.bjxczy.onepark.common.model.car.ParkingInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface ParkingFeignClient {
    @GetMapping("/rpc/parking/selectOneParking")
    ParkingInfo selectOneParking(ParkingInfo parkingInfo);

    @GetMapping("/rpc/parking/parkBlackWhite")
    List<ParkBlackWhiteListInfo> parkBlackWhite(ParkBlackWhiteListInfo info);

    @GetMapping("/rpc/parking/queryCarStatus")
    Boolean queryCarStatus(@PathVariable("plateNumber") String plateNumber, @PathVariable("parkId") String parkId);

    @GetMapping("/rpc/parking/queryCarStatusIsNotDel")
    Boolean queryCarStatusIsNotDel(@PathVariable("plateNumber")String plateNumber);

}
