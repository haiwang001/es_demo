package com.bjxczy.core.feign.client.hikvision;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bjxczy.onepark.common.model.hikvision.GetPlaybackURLsDTO;
import com.bjxczy.onepark.common.model.hikvision.GetPreviewURLsDTO;
import com.bjxczy.onepark.common.model.security.CameraInformation;
import com.bjxczy.onepark.common.model.security.CameraOnlineInfo;
import com.bjxczy.onepark.common.model.security.QueryCameraOnLineCommand;
import com.bjxczy.onepark.common.resp.Payload;
import com.bjxczy.onepark.common.resp.R;

public interface BaseHikvisionFeignClient {
	
	/**
	 * 查询摄像头列表
	 * @return
	 */
	@GetMapping("/resource/cameras")
	List<CameraInformation> cameras();
	
	/**
	 * 查询摄像头在线状态
	 * @param command
	 * @return
	 */
	@PostMapping("/resource/cameras/online/get")
	List<CameraOnlineInfo> cameraOnLine(@RequestBody QueryCameraOnLineCommand command);
	
	/**
	 * 获取海康实时视频流地址
	 * @param dto 查询条件，详见源码
	 * @return
	 */
	@PostMapping("/resource/cameras/previewURLs")
	R<Payload> previewURLs(@RequestBody GetPreviewURLsDTO dto);
	
	/**
	 * 获取海康录像回放列表
	 * @param dto 查询条件，详见源码
	 * @return
	 */
	@PostMapping("/resource/cameras/playbackURLs")
	R<Payload> playbackURLs(@RequestBody GetPlaybackURLsDTO dto);

}
