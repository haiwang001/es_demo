package com.bjxczy.core.feign.client.device;

import com.bjxczy.onepark.common.model.device.AssetInfo;
import com.bjxczy.onepark.common.model.device.GetDeviceInfoVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


public interface GetDeviceInfoFeignClient {

    @PostMapping("/rpc/device/report/info")
    GetDeviceInfoVo deviceInfoReportInfo(
            @RequestParam("deviceId") String deviceId);


    @GetMapping("/rpc/device/getDeviceBySpaceId")
    List<GetDeviceInfoVo> getDeviceBySpaceId(
            @RequestParam(value = "spaceId",required = false) String spaceId,
            @RequestParam("deviceType") String deviceType
    );

    @GetMapping("/rpc/device/getDeviceCount")
    Integer getDeviceCountByRegion(
            @RequestParam(value = "categorys",required = false)List<String> category,
            @RequestParam(value = "areaId",required = false) Integer areaId,
            @RequestParam(value = "buildingId",required = false) String buildingId,
            @RequestParam(value = "spaceId",required = false) String spaceId
    );

    @GetMapping("/rpc/device/getAssetsCount")
    Integer getAssetsCountByRegion(
            @RequestParam(value = "areaId",required = false) Integer areaId,
            @RequestParam(value = "buildingId",required = false) String buildingId,
            @RequestParam(value = "spaceId",required = false) String spaceId
    );

    @GetMapping("/rpc/device/getAssetInfo")
    AssetInfo getAssetInfoByUpkeepRecordTaskId(@RequestParam(value = "taskExecId") Integer taskExecId);
}
