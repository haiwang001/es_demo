package com.bjxczy.core.feign.client.device;

import com.bjxczy.onepark.common.model.device.DeviceInformation;
import com.bjxczy.onepark.common.model.device.GetDeviceInfoVo;
import com.bjxczy.onepark.common.model.device.ListByGidDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

public interface ReportDeviceFeignClient {
    @GetMapping("/rpc/device/report/info")
    List<DeviceInformation> deviceInfoReportInfo();

    @GetMapping("/rpc/device/report/status")
    List<DeviceInformation> deviceInfoReportStatus();

    @PostMapping("/rpc/device/report/list")
    List<DeviceInformation> getDeviceList(ListByGidDto dto);


}
