package com.bjxczy.core.feign.client.door;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.bjxczy.onepark.common.model.door.DoorDeviceInformation;

import java.util.List;

public interface BaseDoorDeviceFeignClient {
	
	@GetMapping("/rpc/device/getOne/{code}")
	DoorDeviceInformation getDoorInformation(@PathVariable("code") String deviceCode);

	/**
	 * @param inOut 方向 进/出 1进 2出
	 * @return 设备编码列表
	 */
	@GetMapping("/rpc/device/getDeviceCodeListByDirection/{inOut}")
	List<String> getDeviceCodeInformation(@PathVariable("inOut") String inOut);
}
