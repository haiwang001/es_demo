package com.bjxczy.core.feign.client.car;

import com.bjxczy.onepark.common.model.car.CarInfo;
import com.bjxczy.onepark.common.model.car.ParkingLotInfo;
import com.bjxczy.onepark.common.model.car.WhiteListInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface CarFeignClient {

	@GetMapping("/rpc/car/carInfoByPlateNumber/{plateNumber}")
	CarInfo selectCarInfoByPlateNumber(@PathVariable("plateNumber") String plateNumber);

	@GetMapping("/rpc/parking/selectParkingLotByThirdId/{id}")
	ParkingLotInfo selectParkingLotByThirdId(@PathVariable("id") String id);

	@GetMapping("/rpc/parking/selectParkingLotById/{id}")
	ParkingLotInfo selectParkingLotById(@PathVariable("id") String id);


	@GetMapping("/rpc/parking/selectParkingLotList")
	List<ParkingLotInfo> selectParkingLotList();

	@GetMapping("/rpc/whiteList/getWhiteByPlateNumber")
	List<WhiteListInfo> getWhiteByPlateNumber(@RequestParam("plateNumber") String plateNumber);

	@GetMapping("/rpc/whiteList/getTodayWhite")
	List<WhiteListInfo> getTodayWhite();


}
