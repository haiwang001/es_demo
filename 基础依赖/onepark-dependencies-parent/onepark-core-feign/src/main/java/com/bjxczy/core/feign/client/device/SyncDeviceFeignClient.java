package com.bjxczy.core.feign.client.device;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.bjxczy.onepark.common.model.device.DeviceInformation;
import com.bjxczy.onepark.common.model.device.DeviceStatus;

/**
 * 设备同步feign接口
 * @author zzz
 */
public interface SyncDeviceFeignClient {
	
	/**
	 * 上报子系统全部设备列表
	 * @param infoList
	 */
	@PostMapping("/rpc/sync/third/list")
	void reportThirdDeviceList(@RequestParam("category") String category, @RequestBody List<DeviceInformation> infoList);
	
	/**
	 * 上报子系统设备状态
	 * @param category 设备类型
	 * @param Map<T, R> status, T: 设备ID、R：设备状态
	 */
	@PostMapping("/rpc/sync/third/status")
	void reportThirdDeviceStatus(@RequestParam("category")String category, @RequestBody Map<String, DeviceStatus> status);

}
