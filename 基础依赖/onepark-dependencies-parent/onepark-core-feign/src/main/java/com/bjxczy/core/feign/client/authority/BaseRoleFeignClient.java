package com.bjxczy.core.feign.client.authority;

import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bjxczy.onepark.common.model.common.ApiResourceInformation;

public interface BaseRoleFeignClient {
	
	@PostMapping("/rpc/user/apis")
	List<ApiResourceInformation> getApisByLoginUser(@RequestBody Set<String> authorities);

}
