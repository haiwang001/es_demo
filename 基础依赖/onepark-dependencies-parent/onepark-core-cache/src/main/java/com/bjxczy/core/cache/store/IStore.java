package com.bjxczy.core.cache.store;

/**
 * 适用于配置类型存储
 * @author xczy
 *
 * @param <T>
 */
public interface IStore<T> {

	void store(T value);
	
	T get();
	
	boolean isExpire();
	
}
