package com.bjxczy.core.cache.store.nacos;

import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.nacos.api.exception.NacosException;
import com.bjxczy.core.cache.store.IStore;

import lombok.Setter;
import lombok.SneakyThrows;
import lombok.ToString;

@ToString
public class NacosStore<T> implements IStore<T> {

	private NacosConfigManager manager;

	private String dataId;

	private String group;

	private T value;
	
	private TypeReference<T> type;
	
	@Setter
	private Consumer<T> receive;
	
	public NacosStore(NacosConfigManager manager, String dataId, String group, TypeReference<T> type) {
		this.manager = manager;
		this.dataId = dataId;
		this.group = group;
		this.type = type;
		addListenter();
	}

	@SneakyThrows
	private void addListenter() {
		String defaultConfig = this.manager.getConfigService().getConfigAndSignListener(dataId, group, 5000, new NacosStoreListener(config -> {
			this.value = JSON.parseObject(config, this.type);
			if (this.receive != null) {
				this.receive.accept(this.value);
			}
		}));
		if (StringUtils.isNotBlank(defaultConfig)) {
			this.value = JSON.parseObject(defaultConfig, this.type);
		}
	}

	@Override
	public void store(T value) {
		try {
			this.value = value;
			manager.getConfigService().publishConfig(dataId, group,
					JSON.toJSONString(value, SerializerFeature.PrettyFormat));
		} catch (NacosException e) {
			throw new RuntimeException(e);
		}
	}

	@SneakyThrows
	@Override
	public T get() {
		if (this.value == null) {
			String config = manager.getConfigService().getConfig(dataId, group, 10 * 1000);
			if (StringUtils.isNotBlank(config)) {
				this.value = JSON.parseObject(config, this.type);
			}
		}
		return this.value;
	}

	@Override
	public boolean isExpire() {
		return this.value == null;
	}

}
