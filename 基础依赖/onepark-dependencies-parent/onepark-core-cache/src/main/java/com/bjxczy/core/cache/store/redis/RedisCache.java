package com.bjxczy.core.cache.store.redis;

import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.RedisTemplate;

import com.bjxczy.core.cache.store.ICache;

public class RedisCache<T> implements ICache<T> {
	
	private RedisTemplate<String, Object> redisTemplate;
	
	private Long timeout;
	
	public RedisCache(RedisTemplate<String, Object> redisTemplate) {
		super();
		this.redisTemplate = redisTemplate;
	}

	public RedisCache(RedisTemplate<String, Object> redisTemplate, Long timeout) {
		super();
		this.redisTemplate = redisTemplate;
		this.timeout = timeout;
	}

	@Override
	public void store(String key, T payload) {
		store(key, payload, timeout);
	}

	@Override
	public void store(String key, T payload, Long timeout) {
		if (timeout == null) {
			redisTemplate.opsForValue().set(key, payload);
		} else {
			redisTemplate.opsForValue().set(key, payload, timeout, TimeUnit.MILLISECONDS);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get(String key) {
		return (T) redisTemplate.opsForValue().get(key);
	}

	@Override
	public boolean hasKey(String key) {
		return redisTemplate.hasKey(key);
	}

}
