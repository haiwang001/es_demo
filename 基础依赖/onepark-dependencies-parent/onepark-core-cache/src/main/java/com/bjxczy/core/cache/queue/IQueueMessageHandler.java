package com.bjxczy.core.cache.queue;

public interface IQueueMessageHandler<T> {
	
	boolean onMessage(T data);

}
