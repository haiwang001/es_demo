package com.bjxczy.core.cache.queue.redis;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.connection.stream.StreamRecords;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.Assert;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.bjxczy.core.cache.queue.IQueue;
import com.bjxczy.core.cache.queue.IQueueMessageHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RedisQueue<T> implements IQueue<T>, InitializingBean {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	private IQueueMessageHandler<T> handler;

	private String name;

	private TypeReference<T> type;
	
	private static final String QUEUE_SUFFIX = ":queue";

	private static final String ERROR_SUFFIX = ":error";

	public RedisQueue(String name, TypeReference<T> type) {
		this.name = name;
		this.type = type;
	}

	@Override
	public void add(T value) {
		ObjectRecord<String, String> record = StreamRecords.newRecord().in(getStreamKey()).ofObject(JSON.toJSONString(value))
				.withId(RecordId.autoGenerate());
		this.redisTemplate.opsForStream().add(record);
	}

	@Override
	public void messageHandler(IQueueMessageHandler<T> handler) {
		this.handler = handler;
	}

	@Override
	public void onMessage(ObjectRecord<String, String> message) {
		boolean success = false;
		String value = message.getValue();
		try {
			// 去除首尾冒号，替换转义字符
			value = value.substring(1, value.length() - 1).replace("\\", "");
			// 由于ObjectRecord反序列化存在BUG，所以手动处理
			success = this.handler.onMessage(JSON.parseObject(value, type));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!success) {
				log.error("[RedisQueue] handler error! add to error cache, data={}", value);
				this.redisTemplate.opsForList().leftPush(errorKey(), value);
			}
		}
	}

	private String errorKey() {
		return this.name + ERROR_SUFFIX;
	}

	@Scheduled(cron = "0 0/5 * * * ?")
	public void retryError() {
		Long size = this.redisTemplate.opsForList().size(errorKey());
		for (int i = 0; i < size; i++) {
 			boolean success = false;
			String value = (String) this.redisTemplate.opsForList().rightPop(errorKey());
			if (value == null) {
				// 当List中不存在数据时跳出循环
				break;
			}
			try {
				success = this.handler.onMessage(JSON.parseObject(value, type));
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (!success) {
					// 捕获异常或者处理失败时，重新加入至队列头部，等待下次重试
					this.redisTemplate.opsForList().leftPush(errorKey(), value);
				}
			}
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(handler, "handler is required!");
	}

	@Override
	public String getStreamKey() {
		return this.name + QUEUE_SUFFIX;
	}

}
