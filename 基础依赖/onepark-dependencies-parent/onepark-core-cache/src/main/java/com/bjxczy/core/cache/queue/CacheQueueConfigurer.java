package com.bjxczy.core.cache.queue;

import java.time.Duration;

import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.stream.StreamMessageListenerContainer;
import org.springframework.data.redis.stream.StreamMessageListenerContainer.StreamMessageListenerContainerOptions;

public abstract class CacheQueueConfigurer {

	@Bean(initMethod = "start", destroyMethod = "stop")
	public StreamMessageListenerContainer<String, ObjectRecord<String, String>> listenerContainer(
			RedisConnectionFactory connectionFactory) {
		StreamMessageListenerContainerOptions<String, ObjectRecord<String, String>> options = StreamMessageListenerContainer.StreamMessageListenerContainerOptions
				.builder().batchSize(10) // 一次最多获取多少条消息
				.pollTimeout(Duration.ofSeconds(1)).keySerializer(RedisSerializer.string())
				.hashValueSerializer(RedisSerializer.string()).targetType(String.class).build();
		StreamMessageListenerContainer<String, ObjectRecord<String, String>> listenerContainer = StreamMessageListenerContainer
				.create(connectionFactory, options);
		this.register(listenerContainer);
		return listenerContainer;
	}

	public abstract void register(
			StreamMessageListenerContainer<String, ObjectRecord<String, String>> listenerContainer);

}
