package com.bjxczy.core.cache.store.nacos;

import java.util.concurrent.Executor;
import java.util.function.Consumer;

import com.alibaba.nacos.api.config.listener.Listener;

public class NacosStoreListener implements Listener {
	
	private Consumer<String> func;
	
	public NacosStoreListener(Consumer<String> func) {
		this.func = func;
	}

	@Override
	public Executor getExecutor() {
		return null;
	}

	@Override
	public void receiveConfigInfo(String configInfo) {
		this.func.accept(configInfo);
	}

}
