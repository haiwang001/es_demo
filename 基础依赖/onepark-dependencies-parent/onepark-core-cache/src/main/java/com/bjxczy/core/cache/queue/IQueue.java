package com.bjxczy.core.cache.queue;

import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.stream.StreamListener;

public interface IQueue<T> extends StreamListener<String, ObjectRecord<String, String>> {
	
	void add(T value);
	
	void messageHandler(IQueueMessageHandler<T> handler);
	
	String getStreamKey();
	
}
