package com.bjxczy.core.cache.store;

public interface ICache<T> {
	
	void store(String key, T payload);
	
	void store(String key, T payload, Long timeout);
	
	T get(String key);
	
	boolean hasKey(String key);

}
