package com.bjxczy.core.cache.store.redis;

import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.RedisTemplate;

import com.bjxczy.core.cache.store.IStore;

import lombok.ToString;

@ToString
public class RedisStore<T> implements IStore<T> {
	
	private RedisTemplate<String, Object> redisTemplate;
	
	private String key;
	
	private Long timeout; // 过时时间毫秒
	
	public RedisStore(RedisTemplate<String, Object> redisTemplate, String key) {
		super();
		this.redisTemplate = redisTemplate;
		this.key = key;
	}

	public RedisStore(RedisTemplate<String, Object> redisTemplate, String key, Long timeout) {
		this.redisTemplate = redisTemplate;
		this.key = key;
		this.timeout = timeout;
	}

	@Override
	public void store(T value) {
		if (this.timeout == null) {
			redisTemplate.opsForValue().set(this.key, value);
		} else {
			redisTemplate.opsForValue().set(this.key, value, this.timeout, TimeUnit.MILLISECONDS);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get() {
		return (T) redisTemplate.opsForValue().get(this.key);
	}

	@Override
	public boolean isExpire() {
		return redisTemplate.hasKey(this.key);
	}

}
