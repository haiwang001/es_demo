# GN-OnePark接口文档

## 1. 平台对接指南

### 1.1 通用鉴权方式

API采用JWT鉴权方式，需在HTTP HEADER中加入以下请求头：

| 请求标头      | value                |
| ------------- | -------------------- |
| Authorization | Bearer {获取的Token} |

### 1.2 返回码定义

| 返回码 | 描述 | 备注 |
| ------ | ---- | ---- |
| 0      | 请求成功 |      |
| 1 | 请求失败 |      |
| 500 | 请求异常 | |


## 2. API接口文档

### 2.1 安防监控

#### 2.1.1 查询监控点取流URL

**基本信息**

| Path     | /v2/cameras/previewURLs |
| -------- | ------------------ |
| Method   | POST                |

**请求参数**

Header

无

Body

| 参数名称 | 类型    | 是否必须 | 备注         |
| -------- | ------- |------|------------|
| cameraIndexCode  | String | 是    | 摄像头编号      |
| protocol  | String | 是    | 类型（固定传 ws） |
| streamType  | Integer | 是    | 固定 0       |
| transmode  | Integer | 是    | 固定 1       |

**请求示例**

```json
{
  "cameraIndexCode": "8f26368a1dcf45d983dc32dfa1f6b4c4",
  "streamType": 0,
  "protocol": "ws",
  "transmode": 1
}
```

**返回数据**

| 名称    | 类型      | 是否必须 | 备注          |
| ------- |---------| -------- |-------------|
| code    | Integer | 是       | 返回码         |
| message | String  | 是       | 返回描述        |
| data    | Object  | 是       | websocket连接 |
| exception    | String  | 是       | 异常信息        |
| success    | boolean | 是       | 是否成功        |

**返回示例**

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": "ws://10.156.97.245:559/openUrl/wJXGss8",
  "success": true
}
```

#### 2.1.2 获取监控点预览取流URLv2

**基本信息**

| Path   | /v2/cameras/previewURLs |
| ------ | ----------------------- |
| Method | POST                    |

**请求参数**

Header

无

Body

| 参数名称 | 类型    | 是否必须 | 备注         |
| -------- | ------- |------|------------|
| cameraIndexCode  | String | 是    |  监控点唯一标识 |
| streamType  | Integer | 是    |  码流类型，0:主码流；1:子码流；2:第三码流，参数不填，默认为主码流     |
| protocol  | String | 是    | 取流协议（应用层协议），“hik”:HIK私有协议，使用视频SDK进行播放时，传入此类型；“rtsp”:RTSP协议；“rtmp”:RTMP协议（RTMP协议只支持海康SDK协议、EHOM协议、ONVIF协议接入的设备；只支持H264视频编码和AAC音频编码）；“hls”:HLS协议（HLS协议只支持海康SDK协议、EHOME协议、ONVIF协议接入的设备；只支持H264视频编码和AAC音频码）；“ws”:Websocket协议（一般用于H5视频播放器取流播放）。参数不填，默认为HIK协议 |
| transmode  | Integer | 否    | 传输协议（传输层协议），0:UDP 1:TCP 默认是TCP 注：GB28181 2011及以前版本只支持UDP传输  |

**请求示例**

```json
{
    "cameraIndexCode": "748d84750e3a4a5bbad3cd4af9ed5101",
    "streamType": 0,
    "protocol": "ws",
    "transmode": 1
}
```

**返回数据**

| 名称    | 类型      | 是否必须 | 备注          |
| ------- |---------| -------- |-------------|
| code    | Integer | 是       | 返回码         |
| message | String  | 是       | 返回描述        |
| data    | Object  | 是       | 返回数据 |
| + url    | Strng  | 是       | 取流URL |
| exception    | String  | 是       | 异常信息        |
| success    | boolean | 是       | 是否成功        |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "url": "ws://192.165.1.166:559/openUrl/ZAVJBLy"
    },
    "success": true
}
```

#### 2.1.3 获取监控点回放取流URLv2

**基本信息**

| Path     | /v2/cameras/playbackURLs |
| -------- | ------------------ |
| Method   | POST                |

**请求参数**

Header

无

Body

| 参数名称 | 类型    | 是否必须 | 备注         |
| -------- | ------- |------|------------|
| cameraIndexCode | string | True | 监控点唯一标识，分页获取监控点资源接口获取返回参数cameraIndexCode |
| recordLocation | number | False | 存储类型,0：中心存储；1：设备存储默认为中心存储 |
| protocol | string | False | 取流协议（应用层协议)，“hik”:HIK私有协议，使用视频SDK进行播放时，传入此类型；“rtsp”:RTSP协议；“rtmp”:RTMP协议（RTMP协议只支持海康SDK协议、EHOME协议、ONVIF协议接入的设备；只支持H264视频编码和AAC音频编码；RTMP回放要求录像片段连续，需要在URL后自行拼接beginTime=20190902T100303&endTime=20190902T100400，其中20190902T100303至20190902T100400为查询出有连续录像的时间段。对于不连续的录像，需要分段查询分段播放）；“hls”:HLS协议（HLS协议只支持海康SDK协议、EHOME协议、ONVIF协议接入的设备；只支持H264视频编码和AAC音频编码；hls协议只支持云存储，不支持设备存储，云存储版本要求v2.2.4及以上的2.x版本，或v3.0.5及以上的3.x版本；ISC版本要求v1.2.0版本及以上，需在运管中心-视频联网共享中切换成启动平台外置VOD）,“ws”:Websocket协议（一般用于H5视频播放器取流播放）。参数不填，默认为HIK协议 |
| transmode | integer | False | 传输协议（传输层协议）0:UDP；1:TCP，默认为TCP，在protocol设置为rtsp或者rtmp时有效，注：EHOME设备回放只支持TCP传输，GB28181 2011及以前版本只支持UDP传输 |
| beginTime | string | True | 开始查询时间（IOS8601格式：yyyy-MM-dd’T’HH:mm:ss.SSSXXX）例如北京时间：2017-06-14T00:00:00.000+08:00，参考附录B ISO8601时间格式说明 |
| endTime | string | True | 结束查询时间，开始时间和结束时间相差不超过三天；（IOS8601格式：yyyy-MM-dd’T’HH:mm:ss.SSSXXX）例如北京时间：2017-06-15T00:00:00.000+08:00，参考附录B ISO8601时间格式说明 |
| uuid | string | False | 分页查询id，上一次查询返回的uuid，用于继续查询剩余片段，默认为空字符串。当存储类型为设备存储时，该字段生效，中心存储会一次性返回全部片段。 |
| expand | string | False | 扩展内容，格式：key=value，调用方根据其播放控件支持的解码格式选择相应的封装类型；多个扩展时，以“&”隔开；支持的内容详见附录F expand扩展内容说明 |
| streamform | string | False | 输出码流转封装格式，“ps”:PS封装格式、“rtp”:RTP封装协议。当protocol=rtsp时生效，且不传值时默认为RTP封装协议 |
| lockType | integer | False | 查询录像的锁定类型，0-查询全部录像；1-查询未锁定录像；2-查询已锁定录像，不传默认值为0。通过录像锁定与解锁接口来进行录像锁定与解锁。 |

**请求示例**

```json
{
  "cameraIndexCode": "8f26368a1dcf45d983dc32dfa1f6b4c4",
  "streamType": 0,
  "protocol": "ws",
  "transmode": 1
}
```

**返回数据**

| 名称    | 类型      | 是否必须 | 备注          |
| ------- |---------| -------- |-------------|
| code    | Integer | 是       | 返回码         |
| message | String  | 是       | 返回描述        |
| data    | Object  | 是       | websocket连接 |
| exception    | String  | 是       | 异常信息        |
| success    | boolean | 是       | 是否成功        |

**返回示例**

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": "ws://10.156.97.245:559/openUrl/wJXGss8",
  "success": true
}
```

## 3. 集中运营管控中心

### 3.1 安防态势

#### 3.1.1 AI事件任务概览

**基本信息**

| Path     | /api/bmpsecurity/ioc/ai/event/overview |
| -------- | ------------------------------------------- |
| Method   | GET                                        |
| 接口描述 | 今日触发AI事件数量查询                      |

**请求参数**

无

**返回数据**

| 名称    | 类型      | 是否必须 | 备注          |
| ------- |---------| -------- |-------------|
| code    | Integer | 是       | 返回码         |
| message | String  | 是       | 返回描述        |
| data    | Object  | 是       | 返回数据 |
| + totalCnt | Integer | 是 | 本日触发总数 |
| + processedCnt | Integer | 是 | 已处理 |
| + pendingCnt | Integer | 是 | 待处理 |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "data": {
        "totalCnt": 1,
        "processedCnt": 1,
        "pendingCnt": 0
    }
}
```


#### 3.1.2 AI事件列表

**基本信息**

| Path     | /api/bmpsecurity/ioc/ai/event/list |
| -------- | ------------------------------------------- |
| Method   | GET                                        |
| 接口描述 | 今日触发AI事件列表                    |

**请求参数**

无

**返回数据**

| 名称    | 类型      | 是否必须 | 备注          |
| ------- |---------| -------- |-------------|
| code    | Integer | 是       | 返回码         |
| message | String  | 是       | 返回描述        |
| data    | Object[]  | 是       | 返回数据 |
| + location | Integer | 是 | 位置 |
| + eventType | Integer | 是 | 事件名称 |
| + happenTime | Integer | 是 | 触发时间 |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "data": [
        {
            "location": "testst",
            "eventType": "视频遮挡",
            "happenTime": "10:38:28"
        }
    ]
}
```

#### 3.1.3 人脸布控概览

**基本信息**

| Path     | /api/bmpsecurity/ioc/face/ctrl/overview |
| -------- | ------------------------------------------- |
| Method   | GET                                        |

**请求参数**

无

**返回数据**

| 名称    | 类型      | 是否必须 | 备注          |
| ------- |---------| -------- |-------------|
| code    | Integer | 是       | 返回码         |
| message | String  | 是       | 返回描述        |
| data    | Object  | 是       | 返回数据 |
| + faceCtrlCnt | Integer | 是 | 重点人员识别数 |
| + strangersCnt | Integer | 是 | 陌生人识别数 |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "data": {
        "faceCtrlCnt": 0,
        "strangersCnt": 0
    }
}
```

#### 3.1.4 重点人员列表

**基本信息**

| Path     | /api/bmpsecurity/ioc/face/ctrl/faces |
| -------- | ------------------------------------------- |
| Method   | GET                                        |

**请求参数**

无

**返回数据**

| 名称    | 类型      | 是否必须 | 备注          |
| ------- |---------| -------- |-------------|
| code    | Integer | 是       | 返回码         |
| message | String  | 是       | 返回描述        |
| data    | Object[]  | 是       | 返回数据 |
| + id | Integer | 是 | id |
| + personName | Integer | 是 | 姓名 |
| + location | Integer | 是 | 位置 |
| + faceImage | Integer | 是 | 抓拍图 |
| + happenTime | Integer | 是 | 发生时间 |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": [
        {
            "id": 46,
            "personName": "郑悦来",
            "location": "昌平中心西区/302配电室",
            "faceImage": "http://10.33.47.14:6040/pic?=d2ei666i31f6935-f775aa--cf835d1a6d0ccidp= 89 d1",
            "happenTime": "17:57:02"
        }
    ],
    "success": true
}
```

#### 3.1.5 陌生人列表

**基本信息**

| Path     | /api/bmpsecurity/ioc/face/ctrl/strangers |
| -------- | ------------------------------------------- |
| Method   | GET                                        |

**请求参数**

无

**返回数据**

| 名称    | 类型      | 是否必须 | 备注          |
| ------- |---------| -------- |-------------|
| code    | Integer | 是       | 返回码         |
| message | String  | 是       | 返回描述        |
| data    | Object[]  | 是       | 返回数据 |
| + id | Integer | 是 | id |
| + age | Integer | 是 | 年龄段 |
| + location | Integer | 是 | 位置 |
| + faceImage | Integer | 是 | 抓拍图 |
| + happenTime | Integer | 是 | 发生时间 |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": [
        {
            "id": 46,
            "age": "青年",
            "location": "昌平中心西区/302配电室",
            "faceImage": "http://10.33.47.14:6040/pic?=d2ei666i31f6935-f775aa--cf835d1a6d0ccidp= 89 d1",
            "happenTime": "17:57:02"
        }
    ],
    "success": true
}
```

#### 3.1.6 视频巡更概览

**基本信息**

| Path     | /api/bmpsecurity/ioc/videoStaticOverview |
| -------- | ---------------------------------------- |
| Method   | GET                                      |
| 接口描述 | 今日视频巡更概览                         |

**请求参数**

无

**返回数据**

| 名称              | 类型    | 是否必须 | 备注       |
| ----------------- | ------- | -------- | ---------- |
| code              | Integer | 是       | 返回码     |
| message           | String  | 是       | 返回描述   |
| data              | Object  | 是       | 返回数据   |
| + allTaskCnt      | Integer | 是       | 所有任务数 |
| + executedCnt     | Integer | 是       | 已执行     |
| + waitExecutedCnt | Integer | 是       | 待执行     |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "allTaskCnt": 12,
        "executedCnt": 12,
        "waitExecutedCnt": 0
    },
    "success": true
}
```

#### 3.1.7 获取视频巡更列表

**基本信息**

| Path     | /api/bmpsecurity/ioc/videoOverviewList |
| -------- | -------------------------------------- |
| Method   | GET                                    |
| 接口描述 | 今日视频巡更列表                       |

**请求参数**

| 参数名称 | 类型    | 是否必须 | 备注                               |
| -------- | ------- | -------- | ---------------------------------- |
| type     | Integer | 是       | 类型：null查全部、1已执行、2待执行 |

**返回数据**

| 名称        | 类型    | 是否必须 | 备注     |
| ----------- | ------- | -------- | -------- |
| code        | Integer | 是       | 返回码   |
| message     | String  | 是       | 返回描述 |
| data[]      | Object  | 是       | 返回数据 |
| + beginTime | String  | 是       | 开始时间 |
| + staffName | String  | 是       | 巡更人员 |
| + planName  | String  | 是       | 计划名称 |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": [
        {
            "beginTime": "2023-06-27 16:30:00",
            "staffName": "邹凯",
            "planName": "计划大大大"
     
        }
    ],
    "success": true
}
```

#### 3.1.8 获取视频巡更异常列表

**基本信息**

| Path     | /api/bmpsecurity/ioc/videoErrorList |
| -------- | ----------------------------------- |
| Method   | GET                                 |
| 接口描述 | 今日视频巡更异常列表                |

**请求参数**

无

**返回数据**

| 名称          | 类型    | 是否必须 | 备注       |
| ------------- | ------- | -------- | ---------- |
| code          | Integer | 是       | 返回码     |
| message       | String  | 是       | 返回描述   |
| data[]        | Object  | 是       | 返回数据   |
| + createTime  | String  | 是       | 发生事件   |
| + planName    | String  | 是       | 计划名称   |
| + staffName   | String  | 是       | 人员名称   |
| + cameraName  | String  | 是       | 摄像头名称 |
| + description | String  | 是       | 描述       |
| + staffId     | Integer | 是       | 人员id     |
| + spaceId     | String  | 是       | 空间id     |
| + position    | String  | 是       | 设备位置   |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": [
        {
            "createTime": "2023-06-05 13:53:40",
            "planName": "计划开始",
            "staffName": "付宇玲",
            "cameraName": "BJ CCTV 01",
            "description": "11",
            "staffId": 13159,
            "spaceId": "b17bb00405314f02a8277cecbc04fa40",
            "position": "测试区域1/01/测试空间"
        }
    ],
    "success": true
}
```

#### 3.1.9 视频巡更右上通知

对应大屏ioc的key为：videoPatrol

| 名称        | 类型   | 是否必须 | 备注         |
| ----------- | ------ | -------- | ------------ |
| title       | String | 是       | 标题         |
| staffName   | String | 是       | 巡更人姓名   |
| staffMobile | String | 是       | 巡更人手机号 |
| planName    | String | 是       | 计划名称     |
| createTime  | String | 是       | 执行时间     |
| routeName   | String | 是       | 路线名称     |

#### 3.1.10 视频巡更告警弹框

对应大屏ioc的key为：patrolAbnormal

| 名称        | 类型   | 是否必须 | 备注         |
| ----------- | ------ | -------- | ------------ |
| title       | String | 是       | 标题         |
| staffName   | String | 是       | 巡更人姓名   |
| deptName    | String | 是       | 部门名称     |
| staffMobile | String | 是       | 巡更人手机号 |
| cameraName  | String | 是       | 设备名称     |
| errorTime   | String | 是       | 异常时间     |
| description | String | 是       | 异常描述     |

#### 3.1.11 获取视频巡更电子巡更异常数据概览

**基本信息**

| Path     | /api/bmpsecurity/ioc/videoErrorOverview |
| -------- | --------------------------------------- |
| Method   | GET                                     |
| 接口描述 | 今日视频巡更电子巡更异常数据概览        |

**请求参数**

无

**返回数据**

| 名称                 | 类型    | 是否必须 | 备注             |
| -------------------- | ------- | -------- | ---------------- |
| code                 | Integer | 是       | 返回码           |
| message              | String  | 是       | 返回描述         |
| data                 | Object  | 是       | 返回数据         |
| + videoErrorCnt      | Integer | 是       | 视频巡更异常数量 |
| + electronicErrorCnt | Integer | 是       | 电子巡更异常数量 |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": [
        {
            "videoErrorCnt": 1,
            "electronicErrorCnt": 4
        }
    ],
    "success": true
}
```

#### 3.1.12 安防态势-电子巡更数据

**基本信息**

| Path     | /api/bmpsecurity/ioc/getKeepWatchTask |
| -------- | ----------------------------------- |
| Method   | GET                                 |
| 接口描述 | 安防态势-电子巡更数据                |

**请求参数**

无

**返回数据**

| 名称            | 类型      | 是否必须 | 备注    |
|---------------|---------| -------- |-------|
| code          | Integer | 是       | 返回码   |
| message       | String  | 是       | 返回描述  |
| data[]        | Object  | 是       | 返回数据  |
| + totalNum    | Integer | 是       | 总数    |
| + nonExecute  | Integer | 是       | 待巡更   |
| + executed    | Integer | 是       | 已巡更   |
| + nonList    | List    | 是       | 待巡更列表 |
| ++ planName   | Integer | 是       | 巡更计划  |
| ++ staffName  | Integer | 是       | 巡更人员  |
| ++ roundStartTime | Integer | 是       | 开始时间  |

**返回示例**

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": {
    "nonList": [
      {
        "pageNo": null,
        "pageSize": null,
        "id": 129,
        "taskCode": "xgrw20230630112949",
        "planId": 47,
        "planName": "6-28ioc测试计划",
        "planStartTime": "2023-06-30",
        "planEndTime": "2023-07-01",
        "planRouteType": 0,
        "planRouteTypeName": null,
        "roundStartTime": "11:35:00",
        "roundEndTime": "11:37:00",
        "routeId": 3,
        "routeName": "路线名称A",
        "routeRemark": null,
        "staffName": "邹凯",
        "staffPhone": "13051729748",
        "roundId": 226,
        "roundSort": 1,
        "taskStatus": 3,
        "taskStatusName": "已完成（含漏检）",
        "taskResult": 0,
        "taskResultName": null,
        "planTime": null,
        "roundTime": null,
        "clockDTOS": null,
        "exceptions": null
      },
      {
        "pageNo": null,
        "pageSize": null,
        "id": 128,
        "taskCode": "xgrw20230630112926",
        "planId": 47,
        "planName": "6-28ioc测试计划",
        "planStartTime": "2023-06-30",
        "planEndTime": "2023-07-01",
        "planRouteType": 0,
        "planRouteTypeName": null,
        "roundStartTime": "11:30:00",
        "roundEndTime": "11:33:00",
        "routeId": 3,
        "routeName": "路线名称A",
        "routeRemark": null,
        "staffName": "邹凯",
        "staffPhone": "13051729748",
        "roundId": 225,
        "roundSort": 1,
        "taskStatus": 3,
        "taskStatusName": "已完成（含漏检）",
        "taskResult": 0,
        "taskResultName": null,
        "planTime": null,
        "roundTime": null,
        "clockDTOS": null,
        "exceptions": null
      }
    ],
    "totalNum": 2,
    "nonExecute": 0,
    "executed": 2
  },
  "success": true
}
```

#### 3.1.13 安防态势-电子巡更数据详情

**基本信息**

| Path     | /api/bmpsecurity/ioc/getKeepWatchTaskInfo |
| -------- | ----------------------------------- |
| Method   | GET                                 |
| 接口描述 | 安防态势-电子巡更数据详情                |

**请求参数**

| 名称          | 类型    | 是否必须 | 备注   |
| ------------- | ------- | -------- |------|
| id          | Integer | 是       | 主键id |

**返回数据**

| 字段             | 类型    | 名称       | 备注 |
|----------------| ------- |----------| ---- |
| id             | Integer  | 主键id     |  |
| taskCode       | String | 任务编号     |  |
| planId         | Integer    | 计划id     | 无   |
| planName       | String  | 计划名称     | 无   |
| routeName      | String  | 路线名称     | 无   |
| staffName      | String  | 人员名称     | 无   |
| planTime       | String  | 计划开始结束时间 | 无   |
| roundTime      | String  | 轮次开始结束时间 | 无   |
| roundSort      | Integer  | 轮次       | 无   |
| taskStatus     | Integer  | 任务状态     | 无   |
| taskResult     | Integer  | 任务结果     | 无   |
| taskStatusName | String  | 任务状态名称   | 无   |
| taskResultName | String  | 任务结果名称   | 无   |
| clockDTOS      | List  | 打卡记录     | 无   |
| +id            | Integer  | 打卡主键id   | 无   |
| +taskId        | Integer  | 任务id     | 无   |
| +pointId       | Integer  | 点位id     | 无   |
| +pointName     | String  | 点位名称     | 无   |
| +sort          | Integer  | 点位顺序     | 无   |
| +clockStatus   | Integer  | 打卡状态     | 无   |
| +clockTime     | String  | 打卡时间     | 无   |
| exceptions     | List  | 异常列表     | 无   |
| +id            | Integer  | 异常id     | 无   |
| +taskId        | Integer  | 任务id     | 无   |
| +pointId       | Integer  | 点位id     | 无   |
| +pointName     | String  | 点位名称     | 无   |
| +createTime    | Date  | 发生事件     | 无   |
| +imgs          | List  | 异常图片     | 无   |
| ++exceptionId  | Integer  | 异常id     | 无   |
| ++sort         | Integer  | 排序       | 预留字段   |
| ++imgUrl       | String  | 图片路径     | 无   |

**返回示例**

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": {
    "pageNo": null,
    "pageSize": null,
    "id": 128,
    "taskCode": "xgrw20230630112926",
    "planId": 47,
    "planName": "6-28ioc测试计划",
    "planStartTime": "2023-06-30",
    "planEndTime": "2023-07-01",
    "planRouteType": 0,
    "planRouteTypeName": "顺序巡更",
    "roundStartTime": "11:30:00",
    "roundEndTime": "11:33:00",
    "routeId": 3,
    "routeName": "路线名称A",
    "routeRemark": null,
    "staffName": "邹凯",
    "staffPhone": "13051729748",
    "roundId": 225,
    "roundSort": 1,
    "taskStatus": 3,
    "taskStatusName": "已完成（含漏检）",
    "taskResult": 0,
    "taskResultName": "正常",
    "planTime": "2023-06-30-2023-07-01",
    "roundTime": "11:30:00-11:33:00",
    "clockDTOS": [
      {
        "id": 237,
        "taskId": 128,
        "pointId": 8,
        "pointName": "巡更点7",
        "sort": 1,
        "clockStatus": 2,
        "clockTime": null,
        "exceptionStatus": 0
      }
    ],
    "exceptions": []
  },
  "success": true
}
```

#### 3.1.14 安防态势-巡更异常数据

**基本信息**

| Path     | /api/bmpsecurity/ioc/getTaskExceptionList |
| -------- | ----------------------------------- |
| Method   | GET                                 |
| 接口描述 | 安防态势-巡更异常数据                |

**请求参数**

无

**返回数据**

| 名称                 | 类型      | 是否必须 | 备注     |
|--------------------|---------| -------- |--------|
| code               | Integer | 是       | 返回码    |
| message            | String  | 是       | 返回描述   |
| data[]             | Object  | 是       | 返回数据   |
| + planName         | String | 是       | 巡更计划   |
| + staffName        | String | 是       | 巡更人员   |
| + createTimeString | String | 是       | 异常上报时间 |

**返回示例**

```json
{
 "code": 0,
 "message": "请求成功！",
 "exception": null,
 "data": [
  {
   "id": 1,
   "taskId": 108,
   "pointId": 8,
   "pointName": null,
   "createTime": "2023-06-27 20:13:35",
   "createTimeString": "20:13:35",
   "remark": "这是备注点1",
   "imgs": null,
   "staffName": "马慧龙",
   "phone": null,
   "deptName": null,
   "planName": "6-27"
  }
 ],
 "success": true
}
```

#### 3.1.15 安防态势-电子巡更异常详情

**基本信息**

| Path     | /api/bmpsecurity/ioc/getTaskExceptionInfo |
| -------- | ----------------------------------- |
| Method   | GET                                 |
| 接口描述 | 安防态势-电子巡更异常详情                |

**请求参数**

| 名称  | 类型      | 是否必须 | 备注   |
|-----|---------| -------- |------|
| id  | Integer | 是       | 主键id |

**返回数据**

| 名称           | 类型      | 是否必须 | 备注   |
|--------------|---------|------|------|
| code         | Integer | 是    | 返回码  |
| message      | String  | 是    | 返回描述 |
| data[]       | Object  | 是    | 返回数据 |
| + phone      | String  | 是    | 手机号  |
| + staffName  | String  | 是    | 巡更人员 |
| + deptName   | String  | 是    | 部门   |
| + createTime | String  | 是    | 异常时间 |
| + pointName  | String  | 是    | 点位名称 |
| + remark     | String  | 是    | 描述   |
| + imgs       | List    | 是    | 图片路径 |
| ++ exceptionId      | Integer | 是    | 异常id |
| ++ sort      | Integer | 是    | 顺序   |
| ++ imgUrl      | String | 是    | 图片路径 |

**返回示例**

```json
{
 "code": 0,
 "message": "请求成功！",
 "exception": null,
 "data": {
  "id": 1,
  "taskId": 108,
  "pointId": 8,
  "pointName": "巡更点7",
  "createTime": "2023-06-27 20:13:35",
  "remark": "这是备注点1",
  "imgs": [
   {
    "exceptionId": 1,
    "sort": null,
    "imgUrl": "/123/ccc/ddd.img"
   }
  ],
  "staffName": "马慧龙",
  "phone": "18810260094",
  "deptName": "测试部门一",
  "planName": null
 },
 "success": true
}
```

>>>>>>> d3b2b4307c09f7e49d8f1e7cd3b1f3fa1838e3f0
### 3.2消息模块

#### 3.2.1 消息推送（WebSocket）

| URL  | /api/iocservice/ws/message/{staffId} |
| ---- | ------------------------------------ |
| 类型 | WebSocket                            |

请求参数：

| 参数    | 类型    | 位置 | 说明       |
| ------- | ------- | ---- | ---------- |
| staffId | Integer | path | 登录用户Id |

推送数据：

| 参数       | 类型    | 说明                                                         |
| ---------- | ------- | ------------------------------------------------------------ |
| title      | String  | 标题                                                         |
| iocItemKey | String  | Ioc模块key                                                   |
| type       | String  | 消息类型：success(成功)/warning(警告)/info(正常)/error(错误) |
| autoClose  | boolean | 是否自动关闭                                                 |
| extra      | Object  | 额外内容                                                     |

#### 3.2.2 消息模块的使用

> IOC消息模块，可实时推送数据给平台所有用户

##### 1）引入依赖

```xml
<dependency>
    <groupId>com.bjxczy.onepark</groupId>
    <artifactId>onepark-core-web</artifactId>
</dependency>
```

##### 2）发送消息

消息体：

| 名称        | 类型    | 说明                                      |
| ----------- | ------- | ----------------------------------------- |
| title       | String  | 标题                                      |
| messageType | String  | 消息类型（info、success、warning、error） |
| autoClose   | Boolean | 是否自动关闭                              |
| extra       | Map     | 额外内容（业务相关内容）                  |
| iocItemKey  | String  | Ioc模块key                                |

发送平台消息：

```java
@SpringBootTest
public SomeServiceTest {
    
    @Autowired
    private IOCMessageService messageService;
    
    @Test
    public void pushMessage() {
        PushIOCMessageInformation information = new PushIOCMessageInformation();
        // 填充相关信息
        information.setIocItemKey("aiEvent");
        information.setTitle("AI事件消息");
        information.setMessageType("info");
        information.setAutoClose(true);
        HashMap<String, Object> extra = new HashMap<>();
        extra.put()
        ... //额外内容自由定义（业务相关内容）
        information.setExtra(extra);
        iocMessageService.pushMessage(information);
	}
    
}
```

IOC模块key定义：

| 模块名称 | 模块Key          |
| -------- | ---------------- |
| AI事件   | aiEvent          |
| 访客     | visitor          |
| 视频巡更 | videoPatrol      |
| 电子巡更 | electronicPatrol |
| 巡更异常 | patrolAbnormal   |
| 门禁     | entranceGuard    |
| 车辆     | vehicle          |

#### 

### 3.3 通行态势

#### 3.3.1 [IOC]  - 在园人数 

**基本信息**

| URL    | api/bmpinandout/screen/parkArea |
| ------ | --------------------------------- |
| Method | get                               |

 **请求参数**

| 参数           | 类型     | 说明     |
| ------------- | ------  | -------- |
| days          |  Integer     | 当前 - 过去的 天数  |


**返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| staff          | Integer |     员工数      |
| visitor        | Integer |     访客数      |
| proprietor     | Integer |     业主数      |
| In[]            | array |   入场      |
| + x           | String |    yyyy-mm-dd HH 时间轴      |
| + y           |  Integer |   数量      |
| Out []          | array |   出场  |
| + x           | String |    yyyy-mm-dd HH 时间轴      |
| + y           |  Integer |   数量      |

**返回数据** 

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"In": [
			{
				"x": "2023-06-14",
				"y": 0
			},
			{
				"x": "2023-06-13",
				"y": 6
			},
			{
				"x": "2023-06-12",
				"y": 0
			},
			{
				"x": "2023-06-11",
				"y": 0
			}
		],
		"Out": [
			{
				"x": "2023-06-14",
				"y": 0
			},
			{
				"x": "2023-06-13",
				"y": 0
			},
			{
				"x": "2023-06-12",
				"y": 0
			},
			{
				"x": "2023-06-11",
				"y": 0
			}
		],
		"staff": 2,
		"visitor": 1,
		"proprietor": 3
	},
	"success": true
}
 ```

#### 3.3.2 [IOC]  - 今日访客 

**基本信息**

| URL    | api/bmpinandout/screen/todayVisitor |
| ------ | --------------------------------- |
| Method | get                               |

 **请求参数**

| 参数           | 类型     | 说明     |
| ------------- | ------  | -------- |
| day           | String     | 今日时间 yyyy-mm-dd  |


**返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| visitor          |  String |   访客姓名 手机号      |
| person           |  String |   接访人姓名 手机号  |
| recordTime       |  String |   入场开门时间      |

**返回数据** 

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [
		{
			"visitor": "qq 18230201158",
			"person": "wlw 15664791649",
			"recordTime": "2023-06-13 16:59:16"
		}
	],
	"success": true
}
 ```

#### 3.3.3 [IOC]  - 当日通行变化

**基本信息**

| URL    | api/bmpinandout/screen/todayChange |
| ------ | --------------------------------- |
| Method | get                               |

 **请求参数**

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           |  string     | 时间 yyyy-mm-dd  |
| id            |  Integer      | 区域id           |


**返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| In            | array |   入场      |
| + x           | string |    yyyy-mm-dd HH 时间轴      |
| + y           |  Integer |   数量      |
| Out           | array |   出场  |
| + x           | string |    yyyy-mm-dd HH 时间轴      |
| + y           |  Integer |   数量      |

**返回数据** 

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"In": [
			{
				"x": "2023-06-15 14",
				"y": 0
			},
			{
				"x": "2023-06-15 13",
				"y": 0
			},
			{
				"x": "2023-06-15 12",
				"y": 0
			},
			{
				"x": "2023-06-15 11",
				"y": 1
			}
		],
		"Out": [
			{
				"x": "2023-06-15 14",
				"y": 0
			},
			{
				"x": "2023-06-15 13",
				"y": 0
			},
			{
				"x": "2023-06-15 12",
				"y": 0
			},
			{
				"x": "2023-06-15 11",
				"y": 0
			}
		]
	},
	"success": true
}
 ```
#### 3.3.4 [IOC]  - 当日出入详情

**基本信息**

| URL    | api/bmpinandout/screen/inOutDetails |
| ------ | --------------------------------- |
| Method | get                               |

 **请求参数**

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           | string     | 时间 yyyy-mm-dd  |
| page Integer       |  Integer      | 当前页码           |
| pageSize      |  Integer      | 页容量           |


**返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| recordsCount          | Integer |   总记录数      |
| pages                | Integer |   总记页数      |
| records[]               | array |   数据列表      |
| +   index                | Integer |   顺序标号      |
| +   carNumber            | string |    通行人名      |
| +   type                 | string |    通行地点      |
| +   time                 | string |    通行时间      |
| +   parkName             | string |    入园/出园      |
| currentRecordsCount    | Integer |   当前记录数      |
| navigatePageNums[]       | array |   页码      |

**返回数据** 

```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"recordsCount": 6,
		"pages": 2,
		"records": [
			{
				"index": 1,
				"carNumber": "",
				"type": "昌平中心东区-104楼",
				"time": "2023-06-13 16:59:17",
				"parkName": "入园"
			},
			{
				"index": 2,
				"carNumber": "qq",
				"type": "昌平中心东区-104楼",
				"time": "2023-06-13 16:59:16",
				"parkName": "入园"
			},
			{
				"index": 3,
				"carNumber": "wlw",
				"type": "昌平中心东区-104楼",
				"time": "2023-06-13 16:23:57",
				"parkName": "入园"
			},
			{
				"index": 4,
				"carNumber": "",
				"type": "昌平中心东区-101楼",
				"time": "2023-06-13 16:23:56",
				"parkName": "入园"
			},
			{
				"index": 5,
				"carNumber": "",
				"type": "昌平中心东区-101楼",
				"time": "2023-06-13 16:23:52",
				"parkName": "入园"
			}
		],
		"currentRecordsCount": 5,
		"pageSize": 5,
		"pageNum": 1,
		"navigatePageNums": [
			1,
			2
		]
	},
	"success": true
}
```
 #### 3.3.5 [IOC]  -  今日访客滞留1

 **基本信息**

| URL    | api/bmpinandout/screen/retention |
| ------ | --------------------------------- |
| Method | get                               |

  **请求参数**

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           | string     | 时间 yyyy-mm-dd  |


 **返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| count                |  Integer |    滞留访客总人数      |
| duration             |  Integer |    滞留访客平均时长 H单位      |
| data[]               | array |    滞留访客      |
| +visitor             | string |    访客人名  访客手机号      |
| +person              | string |    接待人名  接访人手机号      |
| +recordTime          | string |   开门时间      |

**返回数据** 

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"count": 1,
		"duration": 47,
		"data": [
			{
				"visitor": "qq 18230201158",
				"person": "wlw 15664791649",
				"recordTime": "2023-06-13 16:59:16"
			}
		]
	},
	"success": true
}
 ```

 #### 3.3.6   [IOC]  - 总人数

 **基本信息**

| URL    | api/bmpinandout/screen/headCount |
| ------ | --------------------------------- |
| Method | get                               |

  **请求参数**

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           | string     | 时间 yyyy-mm-dd  |

 **返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| visitor                |  Integer |    访客数      |
| proprietor             |  Integer |    业主数      |
| staff                  |  Integer |    员工数      |

 **返回数据** 

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"visitor": 0,
		"proprietor": 1,
		"staff": 2
	},
	"success": true
}
 ```


 #### 3.3.7  [IOC]  - 出入统计

 **基本信息**

| URL    | api/bmpinandout/screen/entryAndExitStatistics |
| ------ | --------------------------------- |
| Method | get                               |

  **请求参数**

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           | string     | 时间 yyyy-mm-dd  |


 **返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| in[]                | array |    进园人数      |
| out[]               | array |    出园人数      |
| ++visitor                |  Integer |    访客数      |
| ++proprietor             |  Integer |    业主数      |
| ++staff                  | Integer |    员工数      |

 **返回数据** 

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"in": {
			"visitor": 0,
			"proprietor": 0,
			"staff": 6
		},
		"out": {
			"visitor": 0,
			"proprietor": 0,
			"staff": 0
		}
	},
	"success": true
}
 ```
 #### 3.3.8  [IOC]  - 今日访客v2

 **基本信息**

| URL    | api/bmpinandout/screen/todayVisitor/v2 |
| ------ | --------------------------------- |
| Method | get                               |

  **请求参数**

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           | string     | 时间 yyyy-mm-dd  |


 **返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| visit                  |  Integer |    到访数      |
| noShow                 |  Integer |    未到访      |

 **返回数据** 

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"visit": 0,
		"noShow": 0
	},
	"success": true
}
 ```

 #### 3.3.9  [IOC]  - 门禁通行

 **基本信息**

| URL    | api/bmpinandout/screen/goThroughEntranceGuard |
| ------ | --------------------------------- |
| Method | get                               |

  **请求参数**

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           | string     | 时间 yyyy-MM-dd  |


 **返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| name                 | string |    人名      |
| type                 | string |    人员类型      |
| direction            | string |    方向      |
| time                 | string |    开门时间      |
| face                 | string |    照片      |
| mobile               | string |    手机号      |
| position             | string |    设备位置      |
| deviceName           | string |    设备名      |
| openingMode          | string |    开门方式      |

 **返回数据** 

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test02",
			"position": "昌平中心东区-101楼-2层",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test01",
			"position": "昌平中心东区-104楼-104箱变",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test02",
			"position": "昌平中心东区-101楼-2层",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test01",
			"position": "昌平中心东区-104楼-104箱变",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test02",
			"position": "昌平中心东区-101楼-2层",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test01",
			"position": "昌平中心东区-104楼-104箱变",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test01",
			"position": "昌平中心东区-104楼-104箱变",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test01",
			"position": "昌平中心东区-104楼-104箱变",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test02",
			"position": "昌平中心东区-101楼-2层",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:25:35",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test02",
			"position": "昌平中心东区-101楼-2层",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:58:07",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test02",
			"position": "昌平中心东区-101楼-2层",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 09:58:07",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test01",
			"position": "昌平中心东区-104楼-104箱变",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 10:02:32",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test02",
			"position": "昌平中心东区-101楼-2层",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 10:02:33",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test01",
			"position": "昌平中心东区-104楼-104箱变",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 10:08:04",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test02",
			"position": "昌平中心东区-101楼-2层",
			"openingMode": "人脸识别"
		},
		{
			"name": "法外狂徒",
			"type": "访客",
			"direction": "进",
			"time": "2023-06-20 10:08:04",
			"face": null,
			"mobile": "18230201157",
			"deviceName": "test01",
			"position": "昌平中心东区-104楼-104箱变",
			"openingMode": "人脸识别"
		}
	],
	"success": true
}
 ```
#### 3.3.10  [IOC]  - 访客滞留v2

 **基本信息**

| URL    | api/bmpinandout/screen/retention/v2 |
| ------ | --------------------------------- |
| Method | get                               |

  **请求参数**

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           | string     | 时间 yyyy-mm-dd  |


 **返回参数** 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| count                |  Integer |    滞留总数      |
| list[]               | array |    滞留人集合      |
|  + name              | string |    滞留人名      |
|  + face               | string |    滞留人照片      |
|  + mobile             | string |    滞留人手机号      |
|  + date              | string |    滞留人访问时间范围  %月%日 - %月%日     |

 **返回数据** 

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"list": [
			{
				"name": "qq",
				"face": null,
				"mobile": "18230201158",
				"date": "6月13日 - 6月14日"
			}
		],
		"count": 1
	},
	"success": true
}
 ```
#### 3.3.11 通行态势-总人数-车辆数

**基本信息**

| Path     | /api/record/iocOverview/getInParkCarNumGroupByCarType |
| -------- |-----------------------------------|
| Method   | GET                               |
| 接口描述 | 当日在场车辆数根据车辆类型分类                   |

**请求参数**

| 名称    | 类型      | 是否必须 | 备注   |
| ------- |---------|------|------|
| areaId    | Integer | 是    | 区域id |

**返回数据**

| 名称    | 类型      | 是否必须 | 备注   |
| ------- |---------| -------- |------|
| code    | Integer | 是       | 返回码  |
| message | String  | 是       | 返回描述 |
| data    | Object  | 是       | 返回数据 |
| + total | Integer | 是 | 总数   |
| + ygNum | Integer | 是 | 员工数  |
| + fkNum | Integer | 是 | 访客数  |
| + yzNum | Integer | 是 | 业主数  |

**返回示例**

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": {
    "ygNum": 3,
    "total": 5,
    "yzNum": 0,
    "fkNum": 2
  },
  "success": true
}
```

#### 3.3.12 通行态势-车辆通行列表查询

**基本信息**

| Path     | /api/record/iocOverview/getInAndOutRecord |
| -------- |-----------------------------------|
| Method   | GET                               |
| 接口描述 | 通行态势-车辆通行列表查询                   |

**请求参数**

| 名称    | 类型      | 是否必须 | 备注   |
| ------- |---------|------|------|
| areaId    | Integer | 是    | 区域id |

**返回数据**

| 名称    | 类型      | 是否必须 | 备注   |
| ------- |---------| -------- |------|
| code    | Integer | 是       | 返回码  |
| message | String  | 是       | 返回描述 |
| data    | Object  | 是       | 返回数据 |
| + id    | Integer | 是 | 主键id |
| + plateNumber  | String  | 是 | 车牌号  |
| + inAndOutName | String  | 是 | 方向   |
| + saveTimeString | String  | 是 | 时间   |

**返回示例**

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": [
    {
      "id": 21080,
      "plateNumber": "京AA1213",
      "saveTime": "2023-06-27 08:30:01",
      "saveTimeString": "08:30:01",
      "inOrOut": 0,
      "parkingId": "1",
      "deviceId": "d32807d7bade4871a937eed7e4989cc6",
      "captureImg": null,
      "parkEventType": "771760131",
      "tenantId": null,
      "delFlag": 0,
      "extra": "{\"data\":{\"alarmCar\":0,\"carAttributeName\":\"临时车\",\"cardNo\":\"\",\"eventCmd\":3,\"eventIndex\":\"A649E203-16C2-4E97-9405-E90D5D7E7354\",\"gateIndex\":\"d32807d7bade4871a937eed7e4989cc6\",\"gateName\":\"西北门口\",\"inResult\":{\"rlsResult\":{\"releaseAuth\":16,\"releaseReason\":370,\"releaseResult\":0,\"releaseResultEx\":0,\"releaseWay\":10}},\"inoutType\":0,\"mainLogo\":2,\"parkIndex\":\"51ecf08c0f89413fa58a0c025cc14e07\",\"parkName\":\"停车场\",\"picUrl\":{\"platePicUrl\":\"/pic?2dd03649c-2do4c1l*3ae31e--d2d765f836f44i7b3*=ids1*=idp5*=tdpe*m5i15=3872*62z9i9s=5b156pi-f7o=9c2=2i45d\",\"vehiclePicUrl\":\"/pic?=d59id742i2c5=o6f-9p5=1b-1d2d765f836f44i7b3*=ids1*=idp6*=tdpe*m5i15=3872*62z9i3s3ee3*-1c5oda-cl4640d3292\"},\"svrIndex\":\"17b5ef3b-faa8-48ff-9c8e-75080d712981\",\"plateBelieve\":97,\"plateColor\":0,\"plateNo\":\"京AA1213\",\"plateType\":0,\"roadwayIndex\":\"e8d332c9e70448a6881a6a469a797b66\",\"roadwayName\":\"常入口\",\"roadwayType\":1,\"subLogo\":0,\"subModel\":0,\"time\":\"2023-06-27T08:30:00.955+08:00\",\"vehicleClass\":1,\"vehicleColor\":255,\"vehicleType\":1},\"eventId\":\"A649E203-16C2-4E97-9405-E90D5D7E7354\",\"eventType\":771760131,\"happenTime\":\"2023-06-27T08:30:00.955+08:00\",\"srcIndex\":\"e8d332c9e70448a6881a6a469a797b66\",\"srcParentIdex\":\"d32807d7bade4871a937eed7e4989cc6\",\"srcType\":\"roadway\",\"status\":0,\"timeout\":30}",
      "deviceName": null,
      "inAndOutName": "进"
    },
    {
      "id": 21081,
      "plateNumber": "京P12345",
      "saveTime": "2023-06-27 08:30:01",
      "saveTimeString": "08:30:01",
      "inOrOut": 0,
      "parkingId": "1",
      "deviceId": "d32807d7bade4871a937eed7e4989cc6",
      "captureImg": null,
      "parkEventType": "771760131",
      "tenantId": null,
      "delFlag": 0,
      "extra": "{\"data\":{\"alarmCar\":0,\"carAttributeName\":\"临时车\",\"cardNo\":\"\",\"eventCmd\":3,\"eventIndex\":\"A649E203-16C2-4E97-9405-E90D5D7E7354\",\"gateIndex\":\"d32807d7bade4871a937eed7e4989cc6\",\"gateName\":\"西北门口\",\"inResult\":{\"rlsResult\":{\"releaseAuth\":16,\"releaseReason\":370,\"releaseResult\":0,\"releaseResultEx\":0,\"releaseWay\":10}},\"inoutType\":0,\"mainLogo\":2,\"parkIndex\":\"51ecf08c0f89413fa58a0c025cc14e07\",\"parkName\":\"停车场\",\"picUrl\":{\"platePicUrl\":\"/pic?2dd03649c-2do4c1l*3ae31e--d2d765f836f44i7b3*=ids1*=idp5*=tdpe*m5i15=3872*62z9i9s=5b156pi-f7o=9c2=2i45d\",\"vehiclePicUrl\":\"/pic?=d59id742i2c5=o6f-9p5=1b-1d2d765f836f44i7b3*=ids1*=idp6*=tdpe*m5i15=3872*62z9i3s3ee3*-1c5oda-cl4640d3292\"},\"svrIndex\":\"17b5ef3b-faa8-48ff-9c8e-75080d712981\",\"plateBelieve\":97,\"plateColor\":0,\"plateNo\":\"京P12345\",\"plateType\":0,\"roadwayIndex\":\"e8d332c9e70448a6881a6a469a797b66\",\"roadwayName\":\"常入口\",\"roadwayType\":1,\"subLogo\":0,\"subModel\":0,\"time\":\"2023-06-27T08:30:00.955+08:00\",\"vehicleClass\":1,\"vehicleColor\":255,\"vehicleType\":1},\"eventId\":\"A649E203-16C2-4E97-9405-E90D5D7E7354\",\"eventType\":771760131,\"happenTime\":\"2023-06-27T08:30:00.955+08:00\",\"srcIndex\":\"e8d332c9e70448a6881a6a469a797b66\",\"srcParentIdex\":\"d32807d7bade4871a937eed7e4989cc6\",\"srcType\":\"roadway\",\"status\":0,\"timeout\":30}",
      "deviceName": null,
      "inAndOutName": "进"
    }
  ],
  "success": true
}
```

#### 3.3.13 通行态势-车辆通行-详情

**基本信息**

| Path     | /api/record/iocOverview/getInAndOutRecordInfo |
| -------- |-----------------------------------|
| Method   | GET                               |
| 接口描述 | 通行态势-车辆通行-详情                   |

**请求参数**

| 名称    | 类型      | 是否必须 | 备注   |
| ------- |---------| -------- |------|
| id    | Integer | 是       | 主键id |

**返回数据**

| 名称    | 类型      | 是否必须 | 备注   |
| ------- |---------| -------- |------|
| code    | Integer | 是       | 返回码  |
| message | String  | 是       | 返回描述 |
| data    | Object  | 是       | 返回数据 |
| + staffName    | Integer | 是 | 人员名称 |
| + staffType  | String  | 是 | 人员类型 |
| + mobile  | String  | 是 | 联系方式 |
| + plateNumber | String  | 是 | 车牌号  |
| + inAndOutName | String  | 是 | 方向   |
| + deviceName | String  | 是 | 设备名称   |
| + position | String  | 是 | 位置   |
| + happenTime | String  | 是 | 进出时间   |

**返回示例**

```json
{
    "code": 0,
    "message": "请求成功！",
    "exception": null,
    "data": {
        "staffName": "秦圆",
        "staffType": "员工",
        "mobile": "15931892363",
        "plateNumber": "京A12344",
        "inAndOutName": "进",
        "deviceName": "西北门口",
        "position": "西北门口",
        "happenTime": "2023-06-27 08:30:01"
    },
    "success": true
}
```

#### 3.3.14 通行态势-今日访客-预约车数

**基本信息**

| Path     | /api/record/iocOverview/getWhiteNum |
| -------- |-----------------------------------|
| Method   | GET                               |
| 接口描述 | 通行态势-今日访客-预约车数                   |

**请求参数**

| 名称    | 类型      | 是否必须 | 备注   |
| ------- |---------|------|------|
| areaId    | Integer | 是    | 区域id |

**返回数据**

| 名称    | 类型      | 是否必须 | 备注   |
| ------- |---------| -------- |------|
| code    | Integer | 是       | 返回码  |
| message | String  | 是       | 返回描述 |
| data    | Object  | 是       | 返回数据 |
| + total    | Integer | 是 | 总数   |
| + arrival  | String  | 是 | 已到   |
| + nonArrival  | String  | 是 | 未到   |

**返回示例**

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": {
    "total": 3,
    "arrival": 2,
    "nonArrival": 1
  },
  "success": true
}
```

#### 3.3.15 通行态势-访客滞留-车辆滞留

**基本信息**

| Path     | /api/record/iocOverview/getRetentionCar |
| -------- |-----------------------------------|
| Method   | GET                               |
| 接口描述 | 通行态势-访客滞留-车辆滞留                   |

**请求参数**

| 名称    | 类型     | 是否必须 | 备注             |
| ------- |--------| -------- |----------------|
| time    | String | 是       | 时间（yyyy-MM-dd） |

**返回数据**

| 名称             | 类型      | 是否必须 | 备注 |
|----------------|---------| -------- |--|
| code           | Integer | 是       | 返回码 |
| message        | String  | 是       | 返回描述 |
| data           | Object  | 是       | 返回数据 |
| + num          | Integer | 是 | 滞留数 |
| + list          | List | 是 | 列表 |
| + staffName    | Integer | 是 | 人员名称 |
| + staffType    | String  | 是 | 人员类型 |
| + mobile       | String  | 是 | 联系方式 |
| + plateNumber  | String  | 是 | 车牌号 |
| + inAndOutName | String  | 是 | 方向 |
| + deviceName   | String  | 是 | 设备名称 |
| + position     | String  | 是 | 位置 |
| + happenTimeString   | String  | 是 | 进出时间 |

**返回示例**

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": {
    "num": 1,
    "list": [
      {
        "staffName": "郝宪燕",
        "staffType": null,
        "mobile": null,
        "plateNumber": "京A12346",
        "inAndOutName": null,
        "deviceName": null,
        "position": null,
        "happenTime": "2023-06-26 08:30:01",
        "happenTimeString": "08:30:01"
      }
    ]
  },
  "success": true
}
```

### 3.4 环境态势

#### 3.4.1 温湿度监控指标

**基本信息**

| Path     | /api/bmpenvironment/ioc/getRecentlyHumitureAndMethane |
| -------- |--------------------------|
| Method   | GET                      |
| 接口描述 | 环境态势-温湿度监控指标&甲烷指标最新值查询             |

**请求参数**

| 名称    | 类型     | 是否必须 | 备注 |
| ------- |--------| -------- |--|
| roomId    | String | 是       | 配电室id |

**返回数据**

| 名称    | 类型      | 是否必须 | 备注   |
| ------- |---------| -------- |------|
| code    | Integer | 是       | 返回码  |
| message | String  | 是       | 返回描述 |
| data    | Object  | 是       | 返回数据 |
| + temp | String | 是 | 温度   |
| + hum | String | 是 | 湿度   |
| + methane | String | 是 | 甲烷   |

**返回示例**

```json
{
  "code": 0,
  "message": "请求成功！",
  "exception": null,
  "data": {
    "hum": "",
    "temp": "",
    "methane": ""
  },
  "success": true
}
```

#### 3.4.2 配电室查询

| URL    | api/bmpspace/common/buildingfloor/switchroom/list |
| ------ | ------------------------------------------------- |
| Method | GET                                               |

请求参数：

无

请求示例：

```
GET /api/bmpspace/common/buildingfloor/switchroom/list
```

返回参数：

| 参数 | 类型   | 说明       |
| ---- | ------ | ---------- |
| id   | String | 配电室ID   |
| name | String | 配电室名称 |