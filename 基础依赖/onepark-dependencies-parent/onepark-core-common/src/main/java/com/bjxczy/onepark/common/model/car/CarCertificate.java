package com.bjxczy.onepark.common.model.car;

import lombok.Data;

import java.io.Serializable;

@Data
public class CarCertificate implements Serializable {


    /**
    * 描述：业务id
    * 是否必填：true
    **/
    private Integer id;
    /**
    * 描述：停车库唯一标识
    * 是否必填：true
    **/
    private String parkSyscode;

    /**
    * 描述：车牌号码
    * 是否必填：true
    **/
    private String plateNo;

    /**
    * 描述：包期费用，单位:元，2位小数
    * 是否必填：false
    **/
    private String fee;

    /**
    * 描述：包期开始时间
     * 时间格式：yyyy-MM-dd，如：2018-07-26
    * 是否必填：true
    **/
    private String startTime;

    /**
    * 描述：包期结束时间
     * 时间格式：yyyy-MM-dd，如：2018-07-27
    * 是否必填：true
    **/
    private String endTime;


}
