package com.bjxczy.onepark.common.model.car;


import lombok.Data;

@Data
public class ParkingInfo {

    private String parkId;
    private String  parkName;

    private String  parkCode;
    private Integer totalSpace;

    private String  telephone;

    private String  address;

    private String  longitude;

    private String  latitude;

    private String  chargeStandard;

    private String   monthChargeStandard;

    private String status;

    private String  attach;

    private String thirdCode;

    private String itemId;
}
