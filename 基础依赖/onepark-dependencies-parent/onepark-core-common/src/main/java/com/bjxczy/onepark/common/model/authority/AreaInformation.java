package com.bjxczy.onepark.common.model.authority;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName AreaInformation
 * @Description
 * @Author zhanghongguo
 * @Date 2023/6/7 17:26
 * @Version 1.0
 */
@Data
public class AreaInformation implements Serializable {
    private static final long serialVersionUID = -5433073396656373013L;

    private Integer id;

    private String name;

    private Integer sort;

    private String areaNo;
}
