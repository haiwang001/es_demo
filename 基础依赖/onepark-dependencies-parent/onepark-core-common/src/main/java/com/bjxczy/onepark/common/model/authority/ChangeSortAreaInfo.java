package com.bjxczy.onepark.common.model.authority;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName ChangeSortAreaInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/1 10:11
 * @Version 1.0
 */
@Data
public class ChangeSortAreaInfo implements Serializable {
    private static final long serialVersionUID = 694968855331002297L;
}
