package com.bjxczy.onepark.common.model.device;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @ClassName DeviceInformation
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/4 10:15
 * @Version 1.0
 */
@Data
public class DeviceInformation implements Serializable {

    private static final long serialVersionUID = -1516002370397539753L;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备编码
     */
    private String deviceCode;

    /**
     * 在线状态
     */
    private Integer onlineStatus;

    /**
     * 设备位置
     */
    private String devicePosition;

    /**
     * 设备所使用的协议，分为http、nb或mqtt
     */
    private String protocol;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 设备备注
     */
    private String deviceRemark;

    /**
     * 扩展数据,保存设备在Onenet的DeviceId或第三方信息
     */
    private Map extra;

    private Integer groupId;

    private String groupName;
    private  String spaceId;
}
