package com.bjxczy.onepark.common.resp;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.model.device.OnenetDeviceData;

public class OnenetBodyObj {
    private OnenetDeviceData msg;
    private String nonce;
    private String msgSignature;

    public OnenetDeviceData getMsg() {
        return msg;
    }

    public void setMsg(OnenetDeviceData msg) {
        this.msg = msg;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getMsgSignature() {
        return msgSignature;
    }

    public void setMsgSignature(String msgSignature) {
        this.msgSignature = msgSignature;
    }

    public String toString(){
        return "{ \"msg\":"+this.msg+",\"nonce\":"+this.nonce+",\"signature\":"+this.msgSignature+"}";
    }

}
