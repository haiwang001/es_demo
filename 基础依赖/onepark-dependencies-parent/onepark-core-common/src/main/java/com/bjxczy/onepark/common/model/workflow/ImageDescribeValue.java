package com.bjxczy.onepark.common.model.workflow;

import java.util.List;

import com.alibaba.fastjson2.JSON;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ImageDescribeValue implements DescribeValue {
	
	private List<String> urls;

	public ImageDescribeValue(List<String> urls) {
		super();
		this.urls = urls;
	}

	@Override
	public String getValue() {
		return JSON.toJSONString(this.urls);
	}

}
