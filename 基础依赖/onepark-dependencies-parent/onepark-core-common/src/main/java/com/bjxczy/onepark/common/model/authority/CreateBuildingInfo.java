package com.bjxczy.onepark.common.model.authority;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @ClassName CreateBuildingInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/1 13:21
 * @Version 1.0
 */
@Data
public class CreateBuildingInfo implements Serializable {
    private static final long serialVersionUID = -4033315133690994372L;
    private String id;

    private Integer areaId;

    private String buildingName;

    private Integer sort;

    private String buildingNo;

    private List floors;

    private List<String> delFloorIdList;

    private Map<String, Object> buildingInfo;
}
