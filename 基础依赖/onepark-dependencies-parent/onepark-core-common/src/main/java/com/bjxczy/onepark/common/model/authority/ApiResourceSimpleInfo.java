package com.bjxczy.onepark.common.model.authority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiResourceSimpleInfo {

	private String key;

	private String name;

	private String menuKey;

}
