package com.bjxczy.onepark.common.utils;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class OptionalUtils {
	
	public static <T> void ifPresentOrElse(Optional<T> optional, Consumer<T> fun, Supplier<RuntimeException> ex) {
		optional.orElseThrow(ex);
		optional.ifPresent(fun);
	}

}
