package com.bjxczy.onepark.common.model.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CameraOnlineInfo {
	
	private String cameraIndexCode;
	
	private Integer online;

}
