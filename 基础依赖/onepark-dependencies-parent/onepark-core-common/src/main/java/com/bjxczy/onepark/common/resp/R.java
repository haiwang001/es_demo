package com.bjxczy.onepark.common.resp;

import java.util.function.Consumer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class R<T> {

	private Integer code;

	private String message;

	private String exception;

	private T data;

	/**
	 * 统一返回值
	 * 
	 * @param resultEnum 状态码定义
	 * @param message    消息
	 * @param data       数据
	 */
	public R(ResultEnum resultEnum, String message, T data) {
		super();
		this.code = resultEnum.getCode();
		if (message != null) {
			this.message = message;
		} else {
			this.message = resultEnum.getMessage();
		}
		this.data = data;
	}

	public boolean isSuccess() {
		return this.code != null && this.code == ResultEnum.SUCCESS.getCode();
	}

	public static R<?> success() {
		return new R<Object>(ResultEnum.SUCCESS, null, null);
	}

	public static R<?> success(String message) {
		return new R<Object>(ResultEnum.SUCCESS, message, null);
	}

	public static <U> R<U> success(U data) {
		return new R<U>(ResultEnum.SUCCESS, null, data);
	}

	public static <U> R<U> success(String message, U data) {
		return new R<U>(ResultEnum.SUCCESS, message, data);
	}

	public static <U> R<U> success(U data, String message) {
		return new R<U>(ResultEnum.SUCCESS, message, data);
	}

	public static R<Payload> success(Consumer<Payload> fun) {
		Payload payload = new Payload();
		fun.accept(payload);
		return new R<Payload>(ResultEnum.SUCCESS, null, payload);
	}

	public static R<?> fail() {
		return new R<Object>(ResultEnum.FAIL, null, null);
	}

	public static R<?> fail(String message) {
		return new R<Object>(ResultEnum.FAIL, message, null);
	}

	public static <U> R<U> fail(String message, U data) {
		return new R<U>(ResultEnum.FAIL, message, data);
	}

	public static R<?> error() {
		return new R<Object>(ResultEnum.ERROR, null, null);
	}

	public static R<?> error(String message) {
		return new R<Object>(ResultEnum.ERROR, message, null);
	}

}
