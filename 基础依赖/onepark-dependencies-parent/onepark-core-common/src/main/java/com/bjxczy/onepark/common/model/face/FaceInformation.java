package com.bjxczy.onepark.common.model.face;

import lombok.Data;

import java.io.Serializable;

@Data
public class FaceInformation implements Serializable {

	private static final long serialVersionUID = 7279295495508630687L;

	private Integer id;

    private String personName;

    private Integer sex;

    private String cardType;

    private String cardId;

    private String facepath;

    private String facepathShow;

    private String groupId;

    private String groupName;

    private String description; //分组描述

    private String cardNum;
}
