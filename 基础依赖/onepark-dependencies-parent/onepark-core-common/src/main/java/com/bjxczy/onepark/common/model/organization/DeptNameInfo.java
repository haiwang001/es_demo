package com.bjxczy.onepark.common.model.organization;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeptNameInfo implements Serializable {
	
	private static final long serialVersionUID = 1154604818538119767L;

	private Integer id;
	
	private String deptName;

}
