package com.bjxczy.onepark.common.constant;



public class  WaterDeviceConstant {

   public   enum HexDataConstant{
       ACTION_CODE(56,58),
       HISTORY_CODE(72,74),
       HISTORY_LENGTH(82,84),
       HISTORY_DATA(84,86),
       IMEI_CODE(138, 140),
       IMEI_DATA(142, 158);
       private Integer start;
       private Integer end;
       HexDataConstant(Integer start, Integer end) {
           this.start = start;
           this.end = end;
       }
       public Integer getStart() {
           return start;
       }

       public Integer getEnd() {
           return end;
       }

   }
}
