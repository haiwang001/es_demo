package com.bjxczy.onepark.common.model.security;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryCameraOnLineCommand {
	
	private List<String> indexCodes;

}
