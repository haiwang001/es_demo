package com.bjxczy.onepark.common.model.common;

import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExportWrapper {
	
	private String name;
	
	private List<Map<String, Object>> data;
	
	private Map<String, String> headMap;

}
