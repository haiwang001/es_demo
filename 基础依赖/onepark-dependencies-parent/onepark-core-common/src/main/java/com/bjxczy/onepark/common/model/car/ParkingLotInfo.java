package com.bjxczy.onepark.common.model.car;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class ParkingLotInfo implements Serializable {

    private String id;

    private String name;

    private String tenantId;

    private String thirdId;


}
