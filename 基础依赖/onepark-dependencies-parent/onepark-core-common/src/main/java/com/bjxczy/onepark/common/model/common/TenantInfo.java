package com.bjxczy.onepark.common.model.common;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class TenantInfo {

	private static final String SPLIT_STR = ",";

	private String value;

	private List<String> tenantList;

	private String inCondition;

	public TenantInfo(String value) {
		this.value = value;
		this.tenantList = Arrays.asList(value.split(SPLIT_STR));
		this.inCondition = this.tenantList.stream().map(e -> "'" + e + "'").collect(Collectors.joining(","));
		this.inCondition = "(" + inCondition + ")";
	}
	
	public void assertContains(String value) {
		if (!this.tenantList.contains(value)) {
			throw new RuntimeException("tenantId无权访问");
		}
	}
	
}
