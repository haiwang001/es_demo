package com.bjxczy.onepark.common.context;

import com.bjxczy.onepark.common.model.common.TenantInfo;

public class TenantContextHolder {

	private static final ThreadLocal<TenantInfo> CONTEXT = new ThreadLocal<>();

	private TenantContextHolder() {}

	public static void set(TenantInfo info) {
		CONTEXT.set(info);
	}

	public static void assertNotNull() {
		if (!isExists()) {
			throw new RuntimeException("Tenant不能为空！");
		}
	}

	/**
	 * 判断tenantInfo是否存在
	 * @return
	 */
	public static boolean isExists() {
		return CONTEXT.get() != null;
	}

	/**
	 * 获取TenantInfo，如不存在则抛出异常
	 * @return
	 */
	public static TenantInfo get() {
		assertNotNull();
		return CONTEXT.get();
	}

	/**
	 * 获取TenantInfo，required=false时，允许为空
	 * @param required
	 * @return
	 */
	public static TenantInfo get(boolean required) {
		return required ? get() : CONTEXT.get();
	}

	public static void clear() {
		CONTEXT.remove();
	}

}
