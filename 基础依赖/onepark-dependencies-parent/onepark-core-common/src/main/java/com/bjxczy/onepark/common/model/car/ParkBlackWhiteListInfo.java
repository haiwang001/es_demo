package com.bjxczy.onepark.common.model.car;

import lombok.Data;

import java.io.Serializable;

/**
 * @author liuwei
 * @title: ParkBlackWhiteList
 * @projectName thirdparty_parking_jieshun
 * @description: 停车场黑白名单
 * @date 2019/12/2416:05
 */
@Data
public class ParkBlackWhiteListInfo implements Serializable {

    private Integer id;
    private String  parkName;

    private String  parkCode;
    private String parkCodeList;
    private String  carNumber;


    private Integer type;

    private String beginDate;

    private String endDate;

    private Integer  status;
    private Integer isDelete;

    private String operator;
    private String recordTime;

    private String userName;
    
    private String phone;
    private String interName;
    private String remark;
    private Integer interId;
    
    private String carColor;

}
