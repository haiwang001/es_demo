package com.bjxczy.onepark.common.model.workbench;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

/**
 * @ClassName PushMessageInformation
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/9 14:33
 * @Version 1.0
 */
@Data
public class PushMessageInformation implements Serializable {

    private static final long serialVersionUID = 5211707862255425618L;

    private Integer staffId;

    private String businessId;

    private String eventCode;

    private String eventType;

    private String title;

    private String content;

    private String messageType;

    private Boolean autoClose;

    private Map<String,Object> extra;

}
