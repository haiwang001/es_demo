package com.bjxczy.onepark.common.model.workflow;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName EoMettingInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/6/6 16:36
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddEoMettingInfo implements Serializable {
    private static final long serialVersionUID = -7825731850472949185L;

    // 会议主题
    private String topic;

    // 会议开始时间
    private String startTime;

    // 会议结束时间
    private String endTime;

    // 会议室Id
    private String metRoomId;

    // 操作人
    private Integer staffId;

    //人数
    private Integer personCount;

    // 归属公司及部门
    private String dept;
}
