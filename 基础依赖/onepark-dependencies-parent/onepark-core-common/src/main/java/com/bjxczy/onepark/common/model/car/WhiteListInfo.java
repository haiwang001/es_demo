package com.bjxczy.onepark.common.model.car;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: Administrator
 * @Date: 2023/5/18 0018 17:43
 * @Description: WhiteListInfo
 * @Version 1.0.0
 */
@Data
public class WhiteListInfo implements Serializable {

    /**
    * 描述：主键id
    * 是否必填：
    **/
    private Integer id;

    /**
    * 描述：姓名
    * 是否必填：
    **/
    private String userName;

    /**
    * 描述：手机号
    * 是否必填：
    **/
    private String phone;

    /**
    * 描述：停车场名称
    * 是否必填：
    **/
    private String parkName;

    /**
    * 描述：停车场ID
    * 是否必填：
    **/
    private String parkingId;

    /**
    * 描述：车牌号
    * 是否必填：
    **/
    private String plateNumber;

    /**
    * 描述：任务状态:0:进行中；1:已失效；2:未开始
    * 是否必填：
    **/
    private Integer status;

    /**
    * 描述：开始日期
    * 是否必填：
    **/
    private Date beginDate;

    /**
    * 描述：结束日期
    * 是否必填：
    **/
    private Date endDate;

    /**
    * 描述：车辆颜色
    * 是否必填：
    **/
    private String carColor;

    /**
    * 描述：园区id
    * 是否必填：
    **/
    private String tenantId;

    /**
    * 描述：接访人ID（员工）
    * 是否必填：
    **/
    private Integer interId;

    /**
    * 描述：接访人姓名
    * 是否必填：
    **/
    private String interName;

    /**
    * 描述：描述
    * 是否必填：
    **/
    private String remark;
}
