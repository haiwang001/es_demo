package com.bjxczy.onepark.common.model.door;

import java.io.Serializable;

import lombok.Data;

@Data
public class StaffFaceInformation implements Serializable {
	
	private static final long serialVersionUID = -2938014493361279461L;
	
	/**
	 * 人脸ID
	 */
	private Integer id;
	
	/**
	 * 员工ID
	 */
	private Integer staffId;
	
	/**
	 * 员工姓名
	 */
	private String staffName;
	
	/**
	 * 员工编号
	 */
	private String employeeNo;
	
	/**
	 * 手机号
	 */
	private String mobile;
	
	/**
	 * 身份证号
	 */
	private String identityCard;
	
	/**
	 * 部门ID
	 */
	private Integer organizationId;
	
	/**
	 * 部门名称（多级）
	 */
	private String deptNames;
	
	/**
	 * 人脸照片URL
	 */
	private String faceUrl;
	
}
