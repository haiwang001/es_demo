package com.bjxczy.onepark.common.model.alarm;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

/**
 * @ClassName CreateAlarmRecordCommand
 * @Description
 * @Author zhanghongguo
 * @Date 2022/12/29 10:48
 * @Version 1.0
 */
@Data
public class CreateAlarmRecordCommand {

	private String uuid;

    /**
     * 模块名称
     */
    private String moduleName;

    /**
     * 事件编码
     */
    private String eventCode;

    /**
     * 事件名称
     */
    private String eventName;

    /**
     * 设备Id
     */
    private String deviceId;


    /**
     * 状态(0:未处理,1:处理中,2:已处理)
     */
    private Integer status;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 告警等级
     */
    private String eventLevel;

}
