package com.bjxczy.onepark.common.model.device;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @ClassName DeviceInfoAll
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/13 14:34
 * @Version 1.0
 */
@Data
public class DeviceInfoAll implements Serializable {
    private static final long serialVersionUID = 9002965875631568874L;
    private Integer id;

    /**
     * 分组id
     */
    private Integer groupId;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 设备类型编码
     */
    private String category;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备编码
     */
    private String deviceCode;

    /**
     * 在线状态
     */
    private Integer onlineStatus;


    /**
     * 设备位置
     */
    private String devicePosition;

    /**
     * 设备所使用的协议，分为http、nb或mqtt
     */
    private String protocol;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 设备备注
     */
    private String deviceRemark;

    /**
     * 扩展数据,保存设备在Onenet的DeviceId或第三方信息
     */
    private String extra;
    /**
     * 区域Id
     */
    private Integer areaId;

    /**
     * 区域名称

     */
    private String areaName;

    /**
     * 楼宇Id
     */
    private String buildingId;

    /**
     * 楼宇名称
     */
    private String buildingName;

    /**
     * 空间Id
     */
    private String spaceId;

    /**
     * 空间名称

     */
    private String spaceName;

    /**
     * 二维码
     */
    private String qrCode;
}
