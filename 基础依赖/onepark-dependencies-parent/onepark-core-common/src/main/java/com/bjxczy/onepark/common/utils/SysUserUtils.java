package com.bjxczy.onepark.common.utils;

import java.util.List;
import java.util.Set;

import com.bjxczy.onepark.common.constant.RoleConstant;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;

public class SysUserUtils {
	
	public static boolean isAdmin(UserInformation user) {
		if (user == null || user.getRoles() == null) {
			return false;
		}
		Set<String> roles = user.getRoles();
		return roles.contains(RoleConstant.ROLE_SYS_ADMIN);
	}
	
	public static boolean isAdmin(Set<String> authorities) {
		if (authorities == null || authorities.isEmpty()) {
			return false;
		}
		return authorities.contains(RoleConstant.ROLE_SYS_ADMIN);
	}
	
	public static String getDept2Name(StaffInformation staff) {
		String deptName = "";
		if (staff != null) {
			List<DeptNameInfo> dept4A = staff.getDeptNames();
			if (dept4A.size() >= 2) {
				deptName = dept4A.get(1).getDeptName();
			} else if (dept4A.size() == 1) {
				deptName = dept4A.get(0).getDeptName();
			}
		}
		return deptName;
	}

}
