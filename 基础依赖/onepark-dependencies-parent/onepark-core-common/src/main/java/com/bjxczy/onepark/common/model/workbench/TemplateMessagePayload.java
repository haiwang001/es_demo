package com.bjxczy.onepark.common.model.workbench;

import java.io.Serializable;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TemplateMessagePayload implements Serializable {
	
	private static final long serialVersionUID = 7640977444265069310L;

	private Integer staffId;
	
	private String businessId;
	
	private String eventCode;
	
	private Map<String, Object> params;

}
