package com.bjxczy.onepark.common.model.authority;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuNavigatorDefinitionApisDetail implements Serializable {

	private static final long serialVersionUID = -930300930538157230L;

	private String code;
	
	private String name;
	
	private String serviceName;
	
	private String key;
	
	private String fldKey;
	
	private String icon;
	
	private String type;
	
	private Map<String, Object> extra;
	
	private List<MenuNavigatorDefinitionApisDetail> items;
	
	private List<ApiResourceSimpleInfo> apis;
	
	public MenuNavigatorDefinitionApisDetail putExtra(String key, Object value) {
		if (this.extra == null) {
			this.extra = new HashMap<>();
		}
		this.extra.put(key, value);
		return this;
	}
	
	public <T> T getExtra(String key) {
		return this.extra == null ? null : (T) this.extra.get(key);
	}
	
}
