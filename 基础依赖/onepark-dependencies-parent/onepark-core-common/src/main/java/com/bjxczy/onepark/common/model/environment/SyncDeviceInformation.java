package com.bjxczy.onepark.common.model.environment;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName SyncDeviceInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/2 14:40
 * @Version 1.0
 */
@Data
public class SyncDeviceInformation implements Serializable {

    private static final long serialVersionUID = -839604413953910862L;

    private String deviceCode;

    private Integer onlineStatus;

    private String indexFactor;

    private String deviceName;

    private String deviceType;

}
