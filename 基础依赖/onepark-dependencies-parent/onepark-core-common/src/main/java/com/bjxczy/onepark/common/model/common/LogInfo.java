package com.bjxczy.onepark.common.model.common;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class LogInfo implements Serializable {
	
	private static final long serialVersionUID = -58743865606704621L;

	/**
	 * 保存时间
	 */
	private Date saveTime;
	
	/**
	 * 服务名称
	 */
	private String applicationName;
	
	/**
	 * 用户ID
	 */
	private Integer userId;

	/**
	 * 用户名
	 */
	private String userName;
	
	/**
	 * 员工ID
	 */
	private Integer staffId;
	
	/**
	 * 租户ID
	 */
	private String tenantId;
	
	/**
	 * 操作类型
	 */
	private String operationType;
	
	/**
	 * 操作类型描述
	 */
	private String operationName;
	
	/**
	 * 操作描述
	 */
	private String operation;
	
	/**
	 * 请求IP
	 */
	private String requestIp;
	
	/**
	 * 请求方式
	 */
	private String requestMethod;
	
	/**
	 * 请求路径
	 */
	private String requestPath;
	
	/**
	 * token
	 */
	private String token;
	
	/**
	 * 请求参数
	 */
	private String requestParams;
	
	/**
	 * 来源平台
	 */
	private String platform;
	
}
