package com.bjxczy.onepark.common.model.security;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CameraInformation implements Serializable {
	
	private static final long serialVersionUID = -770276547455692179L;
	
	/**
	 * 摄像头名称
	 */
	private String cameraName;
	
	/**
	 * 摄像头设备编码
	 */
	private String cameraIndexCode;
	
	/**
	 * 摄像头状态
	 */
	private Integer status;
	
	/**
	 * 摄像头类型
	 */
	private Integer cameraType;
	
	/**
	 * 摄像头类型描述
	 */
	private String cameraTypeName;
	
	private Date createTime;
	
	private Date updateTime;
}
