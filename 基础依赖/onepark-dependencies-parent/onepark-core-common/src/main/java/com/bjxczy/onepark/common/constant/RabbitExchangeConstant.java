package com.bjxczy.onepark.common.constant;

public interface RabbitExchangeConstant {

	 String UPDATE_STAFF_EVENT = "UPDATE_STAFF_EVENT";
	 String UPDATE_DEVICE_GROUP_EVENT = "UPDATE_DEVICE_GROUP_EVENT";

}
