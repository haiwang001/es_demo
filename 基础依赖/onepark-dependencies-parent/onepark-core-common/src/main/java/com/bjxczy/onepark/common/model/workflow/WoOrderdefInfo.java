package com.bjxczy.onepark.common.model.workflow;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.LinkedHashSet;

/**
 * <p>
 * 工单配置 表
 * </p>
 *
 * @author Zhanghongguo
 * @since 2023-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WoOrderdefInfo {


    private String id;

    private String sequenceNo;

    private Integer deptId;

    private String deptName;

    private Integer ownerId;

    private String ownerName;

    private String name;

    private String description;

    private String orderType;

    private String businessType;

    private String orderLevel;

    private String taskType;

    private Integer expireTime;

    private String formId;

    private LinkedHashSet<String> triggerIds;

    private Integer enable;

    private String linkStrategy;


}
