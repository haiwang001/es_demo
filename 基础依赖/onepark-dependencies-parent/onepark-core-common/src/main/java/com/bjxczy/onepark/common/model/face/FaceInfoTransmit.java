package com.bjxczy.onepark.common.model.face;


import lombok.Data;

import java.io.Serializable;

/*
 *@Date 2023/3/2 15:28
 */
@Data
public class FaceInfoTransmit implements Serializable {

    private static final long serialVersionUID = -8332957572479084358L;
    private String faceImg;
    private String employeeNo;
    private String fldName;
    private String mobile;
    private String deptName;
    private String identityCard; // 身份证号码
    private Integer staffType;
    private Integer rankId; // 职级id
    /**
     * 部门id
     */
    private Integer deptId;
    /*
     * 员工id
     */
    private Integer staffId;
}
