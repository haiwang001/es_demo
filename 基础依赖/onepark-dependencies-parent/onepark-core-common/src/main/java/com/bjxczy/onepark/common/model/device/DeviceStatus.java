package com.bjxczy.onepark.common.model.device;

import lombok.Getter;

public enum DeviceStatus {

	OFFLINE(0),ONLINE(1),  ALARM(2), UNKONW(-1);
	
	@Getter
	private Integer value;
	
	DeviceStatus(Integer value) {
		this.value = value;
	}
	
}
