package com.bjxczy.onepark.common.model.car;

import lombok.Data;

import java.io.Serializable;

/**
 * @Auther: qinyuan
 * @Date: 2023/3/23  13:36
 * @Description: 海康添加车辆实体
 * @Version 1.0.0
 */
@Data
public class HikCarInfo implements Serializable {

    /**
    * 描述：海康车辆id
    * 是否必填：（修改必填）
    **/
    private String vehicleId;

    /**
    * 描述：业务id
    * 是否必填：true
    **/
    private Integer clientId;

    /**
    * 描述：车牌号码
    * 是否必填：true
    **/
    private String plateNo;

    /**
    * 描述：人员ID
    * 是否必填：false
    **/
    private String personId;

    /**
    * 描述：车牌类型
    * 是否必填：false
    **/
    private String plateType;

    /**
    * 描述：车牌颜色
    * 是否必填：false
    **/
    private String plateColor;

    /**
    * 描述：车辆类型
    * 是否必填：false
    **/
    private String vehicleType;

    /**
    * 描述：车辆颜色
    * 是否必填：false
    **/
    private String vehicleColor;

    /**
    * 描述：车辆描述
    * 是否必填：false
    **/
    private String description;


}
