package com.bjxczy.onepark.common.model.organization;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RankInfo implements Serializable {

	private static final long serialVersionUID = 8120468201616463906L;

	private Integer id;
	
	private String fldName;
	
	private Integer fldLevel;

}
