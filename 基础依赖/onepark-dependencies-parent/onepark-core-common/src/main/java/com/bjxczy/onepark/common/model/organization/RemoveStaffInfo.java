package com.bjxczy.onepark.common.model.organization;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RemoveStaffInfo implements Serializable {

    private static final long serialVersionUID = 7004085957071039815L;
    private Integer id;

    private String uuid;

    private String mobile;

}
