package com.bjxczy.onepark.common.exception;

public class ResultException extends RuntimeException {

	private static final long serialVersionUID = 8459730644354612328L;
	
	public ResultException(String message) {
		super(message);
	}
	
	public ResultException(String message, Throwable cause) {
		super(message, cause);
	}

}
