package com.bjxczy.onepark.common.model.gateway;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RemoveTokenDTO {
	
	private List<String> clients;
	
	private List<String> usernames;

}
