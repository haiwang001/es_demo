package com.bjxczy.onepark.common.model.face;


import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class RecognitionFaceInfo implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4168042317053474825L;
	private Integer id;
    private String indexCode;
    private String name;
    private String faceGroupIndexCodes;
    private String cameraIndexCodes;
    private String recognitionResourceIndexCodes;
    private String recognitionResourceType;
    private String recognitionPlanType;
    private String description;
    private String status;
    private String available;
    private String startTime;
    private String usingTime;
    private String threshold;
    private List<Map<String,Object>> timeBlockList;
}
