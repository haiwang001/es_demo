package com.bjxczy.onepark.common.model.alarm;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @ClassName AlarmOverviewDto
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/20 13:48
 * @Version 1.0
 */
@Data
public class AlarmOverviewDto {

    private Integer areaId;

    private String buildingId;

    private String spaceId;

    private Integer status;

    private Map<String,String> unit;
}
