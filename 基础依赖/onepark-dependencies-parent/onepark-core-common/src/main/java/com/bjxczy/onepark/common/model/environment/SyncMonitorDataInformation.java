package com.bjxczy.onepark.common.model.environment;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName SyncMonitorDataInformation
 * @Description
 * @Author zhanghongguo
 * @Date 2023/3/2 18:09
 * @Version 1.0
 */
@Data
public class SyncMonitorDataInformation implements Serializable {
    //设备名称
    private String fMetername;

    //设备编码
    private String fMetercode;

    //指标编码
    private String fParamcode;

    //指标名称
    private String fParamname;

    //指标值
    private String fValue;

    //单位
    private String fUnitCode;

    //监测时间
    private String monitorTime;

    //设备类型
    private String category;

}
