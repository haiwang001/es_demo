package com.bjxczy.onepark.common.model.car;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: qinyuan
 * @Date: 2023/3/24 16:06
 * @Description: 海康处理车辆进出记录实体
 * @Version 1.0.0
 */
@Data
public class HikInAndOutRecord implements Serializable {
    /**
    * 描述：车牌号
    * 是否必填：true
    **/
    private String plateNumber;
    /**
    * 描述：保存时间
    * 是否必填：true
    **/
    private Date saveTime;
    /**
    * 描述：进出标记：0入场、1-出场
    * 是否必填：true
    **/
    private Integer inOrOut;

    /**
    * 描述：停车场ID(目前用停车场code代替)
    * 是否必填：true
    **/
    private String parkId;
    /**
    * 描述：入场/出场设备ID
    * 是否必填：false
    **/
    private String deviceId;
    /**
     * 描述：入场/出场设备ID
     * 是否必填：false
     **/
    private String deviceName;

    /**
    * 描述：识别照片
    * 是否必填：false
    **/
    private String image;

    /**
    * 描述：车场事件类型
    * 是否必填：false
    **/
    private String parkEventType;

    /**
     * 描述：报文原数据
     * 是否必填：true
     **/
    private String extra;


}
