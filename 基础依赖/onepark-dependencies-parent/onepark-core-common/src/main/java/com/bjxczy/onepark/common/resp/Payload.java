package com.bjxczy.onepark.common.resp;

import java.util.HashMap;

public class Payload extends HashMap<String, Object> {

	private static final long serialVersionUID = 6957114457675701777L;

	public Payload set(String key, Object value) {
		this.put(key, value);
		return this;
	}

}
