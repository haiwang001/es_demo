package com.bjxczy.onepark.common.model.visitor;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
 *@Author wlw
 *@Date 2023/7/14 9:32
 */
@Data
@NoArgsConstructor
public class VisitorDtoInformation implements Serializable {
    private static final long serialVersionUID = 3291181317016089825L;
    private String name;
    private String type;
    private String id; // 访客临时唯一id
}
