package com.bjxczy.onepark.common.context;

import java.util.function.Function;

import com.bjxczy.onepark.common.model.user.UserInformation;

public class LoginUserContextHolder {
	
	private static final ThreadLocal<UserInformation> CONTEXT = new ThreadLocal<>();
	
	private LoginUserContextHolder() {}
	
	public static void set(UserInformation user) {
		CONTEXT.set(user);
	}
	
	public static UserInformation get() {
		return CONTEXT.get();
	}
	
	public static <T> T getValue(Function<UserInformation, T> func) {
		if (CONTEXT.get() == null) {
			return null;
		}
		return func.apply(CONTEXT.get());
	}
	
	public static void clear() {
		CONTEXT.remove();
	}

}
