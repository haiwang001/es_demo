package com.bjxczy.onepark.common.model.authority;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName CreateAreaInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/1 10:07
 * @Version 1.0
 */
@Data
public class CreateAreaInfo implements Serializable {
    private static final long serialVersionUID = 6925200950565738317L;

    private Integer id;

    private String name;

    private Integer sort;
}
