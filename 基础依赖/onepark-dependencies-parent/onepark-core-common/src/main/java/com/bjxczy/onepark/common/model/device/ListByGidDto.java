package com.bjxczy.onepark.common.model.device;

import lombok.Data;

import java.util.List;



@Data
public class ListByGidDto {
    private List<String > types;
    private Integer gid;
}
