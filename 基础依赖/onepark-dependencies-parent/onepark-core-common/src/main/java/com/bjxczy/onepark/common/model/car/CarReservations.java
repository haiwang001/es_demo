package com.bjxczy.onepark.common.model.car;

import lombok.Data;

import java.io.Serializable;

/**
 * @Auther: qinyuan
 * @Date: 2023/3/23 0023 09:14
 * @Description: 车辆预约-海康实体
 * @Version 1.0.0
 */
@Data
public class CarReservations implements Serializable {

    /**
     * 描述：业务id
     * 是否必填：true
     **/
    private Integer id;

    /**
    * 描述：停车库唯一标识
    * 是否必填：true
    **/
    private String parkSyscode;
    /**
    * 描述：车牌号码
    * 是否必填：true
    **/
    private String plateNo;
    /**
    * 描述：联系人姓名
    * 是否必填：false
    **/
    private String owner;

    /**
     * 描述：允许进出次数，
     * 0：单次
     * 1：多次
     * 是否必填：false
     **/
    private String allowTimes;

    /**
     * 描述：是否收费
     * 0：收费
     * 1：免费
     * 是否必填：false
     **/
    private String isCharge;

    /**
    * 描述：预约方式，5:第三方预约（默认）
    * 6:访客
    * 是否必填：false
    **/
    private Integer resvWay;
    /**
    * 描述：预约开始时间
     * 按时间预约时必填，按次预约不填
     * ISO8601格式：
     * yyyy-MM-ddTHH:mm:ss+当前时区，例如北京时间：
     * 2018-07-26T15:00:00+08:00
    * 是否必填： true
    **/
    private String startTime;
    /**
    * 描述：预约结束时间
     * 按时间预约时必填，按次预约不填
     * ISO8601格式：
     * yyyy-MM-ddTHH:mm:ss+当前时区，例如北京时间：
     * 2018-07-26T15:00:00+08:00
    * 是否必填：true
    **/
    private String endTime;
    /**
    * 描述：手机号，纯数字
    * 是否必填：true
    **/
    private String phoneNo;


}
