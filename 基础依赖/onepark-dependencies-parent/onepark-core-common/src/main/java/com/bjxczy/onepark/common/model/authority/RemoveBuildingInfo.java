package com.bjxczy.onepark.common.model.authority;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName RemoveBuildingInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/1 13:21
 * @Version 1.0
 */
@Data
public class RemoveBuildingInfo implements Serializable {
    private static final long serialVersionUID = -9198273124880171873L;
}
