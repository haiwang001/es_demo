package com.bjxczy.onepark.common.model.ioc;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @ClassName PushMessageInformation
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/9 14:33
 * @Version 1.0
 */
@Data
public class PushIOCMessageInformation implements Serializable {

    private static final long serialVersionUID = -733256692636122961L;

    private String title;

    private String messageType;

    private Boolean autoClose;

    private String iocItemKey;

    private Map<String,Object> extra;

}
