package com.bjxczy.onepark.common.model.device;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName SyncSubDeviceInfo
 * @Description 同步子系统
 * @Author zhanghongguo
 * @Date 2023/3/7 16:23
 * @Version 1.0
 */
@Data
public class SyncSubDeviceInfo implements Serializable {

    private static final long serialVersionUID = -96808153067863053L;

    private String deviceCode;

    private String deviceName;

    private String deviceType;

    private String devicePosition;

    private String category;

    /**
     * 设备类型类别
     */
    private Integer classType;

    /**
     * 设备位置 xxx区/xxx楼/xxx层
     */
    private String areaPosition;

    /**
     * 区域Id
     */
    private Integer areaId;

    private String spaceId;

    /**
     * 楼宇Id
     */
    private String buildingId;

}
