package com.bjxczy.onepark.common.model.common;

import java.io.Serializable;
import java.util.Set;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccessToken implements Serializable {
	
	private static final long serialVersionUID = 5001228221010249644L;
	
	private String accessToken;
	
	private String refreshToken;
	
	private String tokenType;
	
	private int expiresIn;
	
	private Set<String> scope;
	
}
