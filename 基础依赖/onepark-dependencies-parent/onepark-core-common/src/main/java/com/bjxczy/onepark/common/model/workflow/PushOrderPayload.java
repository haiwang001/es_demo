package com.bjxczy.onepark.common.model.workflow;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PushOrderPayload implements Serializable {

	private static final long serialVersionUID = 1713915148188620741L;

	/**
	 * 业务ID
	 */
	private String businessId;

	/**
	 * 工单配置ID
	 */
	private String id;

	/**
	 * 设备ID
	 */
	private String deviceId;

	/**
	 * 工单详述
	 */
	private String description;

	/**
	 * 发起人Id
	 */
	private Integer initiatorId;

	/**
	 * 发起人姓名
	 */
	private String initiatorName;

	/**
	 * 区域Id
	 */
	private Integer areaId;

	/**
	 * 局址Id
	 */
	private String tenantId;

	/**
	/**
	 * 空间Id
	 */
	private String spaceId;

	/**
	 * 位置信息 东区/101楼/8层
	 */
	private String position;

	/**
	 * 紧急程度
	 */
	private String orderLevel;

	/**
	 * 联动字段
	 */
	private List<DescribeItem> describes;

	public PushOrderPayload(String id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	public PushOrderPayload(String id, String description, List<DescribeItem> describes) {
		super();
		this.id = id;
		this.description = description;
		this.describes = describes;
	}

}
