package com.bjxczy.onepark.common.model.device;

import lombok.Data;

import java.io.Serializable;

@Data
public class OnenetDeviceData implements Serializable {
    private String onenetDeviceId;
    private String deviceCode;
    private Integer type;
    private Object value;
}
