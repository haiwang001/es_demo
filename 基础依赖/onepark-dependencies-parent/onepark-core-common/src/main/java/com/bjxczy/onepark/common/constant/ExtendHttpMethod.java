package com.bjxczy.onepark.common.constant;

public enum ExtendHttpMethod {
	GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE, /** 所有method方法*/ALL;
}
