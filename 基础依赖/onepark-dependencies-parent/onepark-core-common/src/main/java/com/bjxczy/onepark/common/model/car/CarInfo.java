package com.bjxczy.onepark.common.model.car;

import lombok.Data;

/**
 * @Auther: qinyuan
 * @Date: 2023/3/31 0031 20:22
 * @Description: CarInfo
 * @Version 1.0.0
 */
@Data
public class CarInfo {
    private Integer id;

    private String plateNumber;

    private Integer carType;

    private String carTypeName;

    private String carSize;

    private String carColor;

    private String plateColor;

    private String brand;

}
