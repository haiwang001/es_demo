package com.bjxczy.onepark.common.model.workbench;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName DealMessageInformation
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/9 16:29
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DealMessageInformation implements Serializable {
	
    private static final long serialVersionUID = 5597699478377788519L;
    
    private String eventCode;

    private String businessId;

    private Integer status;
    
    private String statusName;

	public DealMessageInformation(String eventCode, String businessId, Integer status) {
		super();
		this.eventCode = eventCode;
		this.businessId = businessId;
		this.status = status;
	}
    
}
