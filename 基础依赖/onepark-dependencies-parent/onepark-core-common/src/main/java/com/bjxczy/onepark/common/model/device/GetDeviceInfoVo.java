package com.bjxczy.onepark.common.model.device;

import lombok.Data;

/**
 * @ClassName DeviceInfoVo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/18 18:02
 * @Version 1.0
 */
@Data
public class GetDeviceInfoVo {

    private Integer areaId;

    private String spaceId;

    private String buildingId;

    private String officeAddress;

    private String deviceTypeName;

    private String deviceName;

    private String buildingName;

    private String floor;

    private String groupId;

    private String groupName;
    
    private String deviceCode;
    
    private Integer status;

}
