package com.bjxczy.onepark.common.model.workflow;

import com.bjxczy.onepark.common.model.user.UserInformation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName AddVisitsInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/31 10:36
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddVisitsInfo implements Serializable {
    private static final long serialVersionUID = 4132761317121651735L;

    // EO单Id
    private String eoId;

    // 访客姓名
    private String name;

    // 证件类型
    private String certificateType;

    // 访客证件
    private String certificateNo;

    // 访客手机
    private String mobile;

    // 访客照片地址
    private String photoUrl;

    private UserInformation userInformation;

    private String startTime;

    private String endTime;

    // 到访事由
    private String visitReason;

    // 到访园区
    private Integer areaId;

    // 备注
    private String remark;

    // 接访人
    private Integer interviewee;
}
