package com.bjxczy.onepark.common.model.car;

import lombok.Data;

@Data
public class CarTypeInfo {
	
	private Integer id;
	
	private String typeName;
	
	private Integer typeValue;
	
	private Integer flag;
	
	private String tenantId;

}
