package com.bjxczy.onepark.common.model.environment;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName SyncDeviceAlarmData
 * @Description
 * @Author zhanghongguo
 * @Date 2023/6/1 16:51
 * @Version 1.0
 */
@Data
public class SyncDeviceAlarmData implements Serializable {

    private static final long serialVersionUID = -1594473018317078235L;

    //告警id
    private String alarmEventLogId;

    //设备编码
    private String devicecode;

    private String alarmTime;

    private String indexName;

    private String indexCode;

    private String indexValue;

    private String fMessInfoExplain;
}

