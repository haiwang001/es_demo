package com.bjxczy.onepark.common.model.door;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
 *@Author wlw
 *@Date 2023/7/3 17:06
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HikDoorDeviceInformation implements Serializable {
    private static final long serialVersionUID = -339987606849495919L;
    private String name;// 设备名称
    private String indexCode;// 设备编号
    private Integer online;// 是否在线状态
    private String ip;// 设备ip
}
