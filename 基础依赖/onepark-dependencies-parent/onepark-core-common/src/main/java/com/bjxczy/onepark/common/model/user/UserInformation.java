package com.bjxczy.onepark.common.model.user;

import java.io.Serializable;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInformation implements Serializable {

	private static final long serialVersionUID = 3297162985409330140L;
	/**
	 * 用户ID
	 */
	private Integer id;

	/**
	 * 用户名
	 */
	private String username;

	/**
	 * 员工姓名
	 */
	private String staffName;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 手机号
	 */
	private String mobile;

	/**
	 * 员工编号
	 */
	private String employeeNo;

	/**
	 * 是否启用
	 */
	private Boolean enable;

	/**
	 * 角色
	 */
	private Set<String> roles;

	/**
	 * 员工ID
	 */
	private Integer staffId;

	public UserInformation(Integer id, String username, String staffName) {
		super();
		this.id = id;
		this.username = username;
		this.staffName = staffName;
	}

}
