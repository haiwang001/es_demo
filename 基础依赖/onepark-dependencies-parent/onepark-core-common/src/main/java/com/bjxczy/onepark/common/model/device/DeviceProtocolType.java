package com.bjxczy.onepark.common.model.device;

public enum DeviceProtocolType {

	HTTP("http"), NB("nb"), MQTT("mqtt"), EDP("edp"), UNKNOWN("unknown");

	private String value;

	private DeviceProtocolType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

}
