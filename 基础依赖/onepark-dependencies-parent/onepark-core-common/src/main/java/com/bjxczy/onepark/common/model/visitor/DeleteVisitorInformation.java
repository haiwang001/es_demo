package com.bjxczy.onepark.common.model.visitor;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
 *@Author wlw
 *@Date 2023/6/24 10:50
 */
@Data
@NoArgsConstructor
public class DeleteVisitorInformation  implements Serializable {
    private static final long serialVersionUID = 4442614746479929312L;
    private String id;//访客主键
    private String orderId;

    public DeleteVisitorInformation(String id, String orderId) {
        this.id = id;
        this.orderId = orderId;
    }
}
