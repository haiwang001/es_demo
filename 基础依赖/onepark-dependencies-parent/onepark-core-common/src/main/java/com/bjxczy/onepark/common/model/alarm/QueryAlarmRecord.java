package com.bjxczy.onepark.common.model.alarm;

import lombok.Data;

import java.util.Date;

@Data
public class QueryAlarmRecord {
    private Integer id;

    private String officeAddress;

    private String moduleName;

    private String deviceTypeName;

    private String deviceId;

    private String deviceName;

    private String eventName;

    private String eventLevel;

    private Integer isTimeout;

    private Date createTime;

    private Date updateTime;

    private Integer status;

    private String operator;

    private String remarks;

    private String groupId;

    private String groupName;

    private String startTime;

    private String endTime;

}
