package com.bjxczy.onepark.common.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.bjxczy.onepark.common.model.common.ApiResourceInformation;
import com.bjxczy.onepark.common.model.common.ExportWrapper;
import com.bjxczy.onepark.common.model.common.PageWrapper;

public interface TemplateSqlService {

	/**
	 * SQL查询
	 * @param key 查询KEY，格式：{namespace}/{id}
	 * @param params 查询参数
	 * @return
	 */
	List<Map<String, Object>> list(String key, Map<String, Object> params);

	/**
	 * SQL分页查询
	 * @param key 查询KEY，格式：{namespace}/{id}
	 * @param pageNum 页数（elasticsearch查询不支持）
	 * @param pageSize 每页条数
	 * @param params 查询参数
	 * @return
	 */
	PageWrapper<Map<String, Object>> page(String key, Integer pageNum, Integer pageSize, Map<String, Object> params);
	
	/**
	 * SQL导出查询
	 * @param key 查询KEY，格式：{namespace}/{id}
	 * @param fetchSize 导出条数，为null则全部导出
	 * @param params 查询参数
	 * @return
	 */
	ExportWrapper export(String key, Integer fetchSize, Map<String, Object> params);
	
	/**
	 * SQL查询（单数据）
	 * @param key 查询KEY，格式：{namespace}/{id}
	 * @param params 查询参数
	 * @return
	 */
	Optional<Map<String, Object>> queryForMap(String key, Map<String, Object> params);
	
	List<ApiResourceInformation> listApis();
	
	Boolean isResource(String key, String path);

}
