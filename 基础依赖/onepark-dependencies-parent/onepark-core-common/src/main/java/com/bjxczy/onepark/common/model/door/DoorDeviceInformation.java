package com.bjxczy.onepark.common.model.door;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DoorDeviceInformation {
	
	/**
	 * 设备编码
	 */
	private String deviceCode;
	
	/**
	 * 设备类型
	 */
	private String deviceType;
	
	/**
	 * 设备名称
	 */
	private String deviceName;
	
	/**
	 * 设备方向 (1-进，2-出)
	 */
	private Integer direction; 
	
	/**
	 * 区域ID
	 */
	private String areaId;
	
	/**
	 * 租户ID
	 */
	private String tenantId;
	
	/**
	 * 空间ID
	 */
	private String spaceId;
	
	/**
	 * 位置信息
	 */
	private String location;

}
