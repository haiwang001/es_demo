package com.bjxczy.onepark.common.model.scheduling;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @ClassName OnDutyStaffInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/23 10:50
 * @Version 1.0
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class OnDutyStaffInfo {

    private Integer id;

    /**
    * 描述：上班日期
    * 是否必填：
    **/
    private Date workDate;

    /**
    * 描述：上班类型：1-白班、2-夜班
    * 是否必填：
    **/
    private Integer workType;

    /**
    * 描述：班组ID
    * 是否必填：
    **/
    private Integer groupId;

    /**
    * 描述：班组名称
    * 是否必填：
    **/
    private String groupName;

    /**
    * 描述：小组：1、2、3、4
    * 是否必填：
    **/
    private Integer subGroupId;

    /**
    * 描述：序列号
    * 是否必填：
    **/
    private Integer seqNo;

    /**
    * 描述：员工ID
    * 是否必填：
    **/
    private Integer staffId;

    /**
    * 描述：员工编号
    * 是否必填：
    **/
    private String employeeNo;

    /**
    * 描述：员工名称
    * 是否必填：
    **/
    private String staffName;

    /**
    * 描述：手机号
    * 是否必填：
    **/
    private String mobile;

    /**
    * 描述：部门ID
    * 是否必填：
    **/
    private Integer deptId;

    /**
    * 描述：创建时间
    * 是否必填：
    **/
    private Date createTime;

}
