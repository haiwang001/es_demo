package com.bjxczy.onepark.common.model.alarm;

import lombok.Data;

import java.util.Date;

@Data
public class AlarmRecordInfo {
    private Integer id;

    private String officeAddress;

    private String moduleName;

    private String deviceTypeName;

    private String deviceId;

    private String deviceName;

    private String buildingName;

    private String floor;

    private String eventName;

    private String eventCode;

    private String eventLevel;

    private String pushName;

    private String noticePerson;

    private Integer isTimeout;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    private String operator;

    private String remarks;

    private String groupId;

    private String groupName;

    private String uuid;

    private Integer operatorId;
}
