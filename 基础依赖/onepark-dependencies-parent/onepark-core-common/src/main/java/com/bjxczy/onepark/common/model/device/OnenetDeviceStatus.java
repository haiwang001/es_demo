package com.bjxczy.onepark.common.model.device;

import lombok.Data;

import java.io.Serializable;

@Data
public class OnenetDeviceStatus implements Serializable {
    private String imei;
    private Integer status;
}
