package com.bjxczy.onepark.common.model.workflow;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TextDescribeValue implements DescribeValue {
	
	private String text;
	
	public TextDescribeValue(String text) {
		super();
		this.text = text;
	}

	@Override
	public String getValue() {
		return this.text;
	}

}
