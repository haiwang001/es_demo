package com.bjxczy.onepark.common.model.organization;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName OrganizationInformation
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/31 17:30
 * @Version 1.0
 */
@Data
public class OrganizationInformation implements Serializable {
    private static final long serialVersionUID = 1234913975429983945L;

    private Integer id;

    private String fldName;

    private String description;

    private Integer parentId;



}
