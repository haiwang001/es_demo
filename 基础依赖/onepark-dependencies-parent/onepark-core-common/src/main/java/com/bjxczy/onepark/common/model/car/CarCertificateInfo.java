package com.bjxczy.onepark.common.model.car;

import java.util.Date;

import cn.hutool.db.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Data
//@NoArgsConstructor
//@EqualsAndHashCode(callSuper = false)
public class CarCertificateInfo {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private Integer carId;
	
	private Integer staffId;
	
	private String misCode;
	
	private String staffName;
	
	private String mobile;
	
	private String deptNameOne;
	
	private String deptNameTwo;
	
	private String deptNameThree;
	
	private Integer carType;
	
	private Integer applyType;
	
	private Integer packageId;
	
	private String assignParkingLot;
	
	private Date createTime;
	
	
	private Integer delFlag;
	
	private String applyName;
	
	private String carTypeName;
	
	
	private Integer packageRest;
	
	private Date endTime;
	
	
	private String deptName;
	
	private String plateNumber;
	
	private Integer rankId;

	private String rankName;

	private Integer resignFlag;

	private String tenantId;
	
	private Integer oldFlag;
	
	private String certCnt;
	
	private String carColor;
	
	private String carSize;
	
	private Integer inBlackList;
	private String rankCorrect;
	
	private Integer staffDelFlag;
	
	private String  remark;
	private String residentName;
	private String employeeNo;
	private String correctResident;
	private Integer deptId;

	
}
