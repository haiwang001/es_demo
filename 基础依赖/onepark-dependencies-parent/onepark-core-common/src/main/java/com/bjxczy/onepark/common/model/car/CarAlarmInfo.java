package com.bjxczy.onepark.common.model.car;

import lombok.Data;

import java.io.Serializable;

/**
 * @Auther: Administrator
 * @Date: 2023/3/29 0029 10:20
 * @Description: CarAlarmInfo
 * @Version 1.0.0
 */
@Data
public class CarAlarmInfo implements Serializable {


    /**
     * 描述：业务id
     * 是否必填：true
     **/
    private Integer id;

    /**
     * 描述：车牌号码
     * 是否必填：false
     **/
    private String plateNo;

    /**
     * 描述：卡号
     * 是否必填：false
     **/
    private String cardNo;

    /**
     * 描述：驾驶员名称
     * 是否必填：false
     **/
    private String driver;

    /**
     * 描述：驾驶员电话
     * 是否必填：false
     **/
    private String driverPhone;

    /**
     * 描述：备注信息
     * 是否必填：false
     **/
    private String remark;

    /**
     * 描述：布控结束时间,ISO8601格式：
     * yyyy-MM-ddTHH:mm:ss+当前时区，例如北京时间：
     * 2018-07-26T15:00:00+08:00
     * 是否必填：false
     **/
    private String endTime;
}
