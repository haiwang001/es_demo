package com.bjxczy.onepark.common.model.common;

import lombok.Data;

@Data
public class DictInformation {
	
	/**
	 * ID
	 */
	private Integer id;
	
	/**
	 * 分组编码
	 */
	private String groupCode;
	
	/**
	 * 字典名称
	 */
	private String label;
	
	/**
	 * 字典值
	 */
	private String value;
	
	/**
	 * 额外内容
	 */
	private String extra;
	
	/**
	 * 删除标记
	 */
	private Integer delFlag;

}
