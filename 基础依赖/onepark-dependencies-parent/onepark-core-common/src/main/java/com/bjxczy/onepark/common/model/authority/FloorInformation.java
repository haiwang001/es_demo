package com.bjxczy.onepark.common.model.authority;

import lombok.Data;

/**
 * @ClassName FloorInformation
 * @Description
 * @Author zhanghongguo
 * @Date 2023/7/5 14:25
 * @Version 1.0
 */
@Data
public class FloorInformation {
    private String id;

    private String parentId;

    private String name;

    private String description;

    private String floorNo;

    private String tenantId;

    private Integer sort;

    private String spaceNo;
}
