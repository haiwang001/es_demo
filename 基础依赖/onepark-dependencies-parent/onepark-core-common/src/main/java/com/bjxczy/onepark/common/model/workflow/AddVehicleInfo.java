package com.bjxczy.onepark.common.model.workflow;

import com.bjxczy.onepark.common.model.user.UserInformation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @ClassName AddVehicleInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/5/26 17:11
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddVehicleInfo implements Serializable {

    private static final long serialVersionUID = 9036472884925949111L;
    // 停车场
    private String park;

    // 车牌号
    private String carNumber;

    // 姓名
    private String name;

    // 车辆颜色
    private String carColor;

    // 手机号
    private String mobile;

    // 开始日期
    private String startTime;

    // 结束日期
    private String endTime;

    private UserInformation userInformation;
}
