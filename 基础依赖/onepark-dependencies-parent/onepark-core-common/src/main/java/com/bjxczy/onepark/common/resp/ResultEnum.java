package com.bjxczy.onepark.common.resp;

import lombok.Getter;

@Getter
public enum ResultEnum {

	SUCCESS(0, "请求成功！"), FAIL(1, "请求失败！"), ERROR(500, "请求异常！");
	
	private Integer code;
	
	private String message;
	
	private ResultEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	
}
