package com.bjxczy.onepark.common.model.device;

import lombok.Data;

/**
 * @ClassName AssetInfoVo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/7/18 11:18
 * @Version 1.0
 */
@Data
public class AssetInfo {
    private String assetName;

    private String areaPosition;

    private String assetCode;
}
