package com.bjxczy.onepark.common.model.organization;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class StaffInformation implements Serializable {

	private static final long serialVersionUID = 8120468201616463906L;

	private Integer id;

	private String fldName;

	private String mobile;

	private String uuid;

	private Integer gender;

	private String email;

	private String employeeNo;

	private String identityCard;

	private Integer organizationId;

	private String positionIds;

	private String positionName;

	private Integer rankId;

	private String rankName;

	private Integer remark;

	private Integer hasAccount;

	private List<String> photo;

	private List<DeptNameInfo> deptNames;

	private Integer dimission;

	private Integer comeFrom;

}
