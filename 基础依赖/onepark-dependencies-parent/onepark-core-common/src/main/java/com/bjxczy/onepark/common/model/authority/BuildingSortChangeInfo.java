package com.bjxczy.onepark.common.model.authority;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName BuildingSortChangeInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/1 13:22
 * @Version 1.0
 */
@Data
public class BuildingSortChangeInfo implements Serializable {
    private static final long serialVersionUID = -2296942953283879683L;
}
