package com.bjxczy.onepark.common.model.ioc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TemplateIOCMessagePayload implements Serializable {

	private static final long serialVersionUID = -8437668673504316173L;

	private String eventCode;

	private Map<String, Object> params;

}
