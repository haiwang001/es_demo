package com.bjxczy.onepark.common.model.authority;

import java.io.Serializable;

import lombok.Data;

@Data
public class SpaceInformation implements Serializable {
	
	private static final long serialVersionUID = -5423235339293344340L;
	
	private String id;
	
	private Integer areaId;
	
	private String name;
	
	private String buildingNo;
	
	private Integer sort;

}
