package com.bjxczy.onepark.common.model.visitor;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.Serializable;

/*
 *@Author wlw
 *@Date 2023/6/19 17:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VisitorInformation implements Serializable {
    private static final long serialVersionUID = 7665264796057274092L;
    /**
     * 访客姓名          visitor_name
     */
    @NonNull
    private String visitorName;
    /**
     * 访客手机号       visitor_mobile
     */
    private String visitorMobile;
    /**
     * 访客单位          visitor_work
     */
    private String visitorWork;
    /**
     * 接访人id      receptionist_id
     */
    private Integer receptionistId;
    /**
     * 接访人
     */
    private String staffName;
    /**
     * 接访人部门 receptionist_dept_name  xx/xx/xx
     */
    private String receptionistDeptName;
    /**
     * 访客状态
     */
    private Integer arriveStatus;
    /**
     * 到访时间            visit_time
     */
    private String visitTime;
    /**
     * 结束时间            leave_time
     */
    private String leaveTime;
}
