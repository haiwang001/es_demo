package com.bjxczy.onepark.common.model.organization;

import lombok.Data;

import java.util.List;

@Data
public class OrganizationTree extends OrganizationInformation{
    private String fidCode;
    private List<OrganizationTree> children;
    private List<OrganizationInformation> path;
    private List<StaffInfoFine> staffs;
}
