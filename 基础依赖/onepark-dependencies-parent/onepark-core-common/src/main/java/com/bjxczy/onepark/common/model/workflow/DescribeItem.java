package com.bjxczy.onepark.common.model.workflow;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DescribeItem implements Serializable {
	
	private static final long serialVersionUID = -7001145090485105691L;

	private String label;
	
	private DescribeValue value;

}
