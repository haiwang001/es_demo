package com.bjxczy.onepark.common.model.alarm;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateAlarmStatusCommand implements Serializable {

	private static final long serialVersionUID = 4063083443894377069L;
	/**
	 * 告警UUID
	 */
	private String uuid;

	/**
	 * 事件编码
	 */
	private String eventCode;

	/**
	 * 处理用户ID
	 */
	private Integer userId;

	/**
	 * 处理用户姓名
	 */
	private String staffName;

	/**
	 * 处理状态
	 */
	private Integer status;

	/**
	 * 备注
	 */
	private String remarks;

}
