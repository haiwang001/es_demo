package com.bjxczy.onepark.common.model.authority;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SpaceTree implements Serializable {
	
	private static final long serialVersionUID = 7442848983960063399L;

	/**
	 * ID
	 */
	private Object id;
	
	/**
	 * 名称
	 */
	private String label;
	
	/**
	 * 类型，space：空间；device：设备
	 */
	private SpaceTypes type;
	
	/**
	 * 设备数量
	 */
	private Integer count;
	
	private List<SpaceTree> children;
	
	private Object extra;
	

}
