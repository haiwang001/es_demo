package com.bjxczy.onepark.common.model.alarm;

import lombok.Data;

@Data
public class AlarmEventInfo {
	
	private Integer id;
	
	private String eventCode;
	
	private String moduleName;
	
	private String eventName;
	
	private String eventLevel;
	
}
