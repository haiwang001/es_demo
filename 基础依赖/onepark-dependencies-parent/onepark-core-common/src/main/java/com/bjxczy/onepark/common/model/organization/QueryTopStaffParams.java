package com.bjxczy.onepark.common.model.organization;

import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class QueryTopStaffParams {

	private List<Integer> rankIds;

	private List<Integer> positionIds;

	public QueryTopStaffParams singletonRankId(Integer rankId) {
		this.rankIds = Arrays.asList(rankId);
		return this;
	}

	public QueryTopStaffParams singletonPositionId(Integer positionId) {
		this.positionIds = Arrays.asList(positionId);
		return this;
	}

	public QueryTopStaffParams setRankIds(List<Integer> rankIds) {
		this.rankIds = rankIds;
		return this;
	}

	public QueryTopStaffParams setPositionIds(List<Integer> positionIds) {
		this.positionIds = positionIds;
		return this;
	}
}
