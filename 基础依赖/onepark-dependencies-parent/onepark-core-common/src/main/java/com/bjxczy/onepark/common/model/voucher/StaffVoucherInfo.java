package com.bjxczy.onepark.common.model.voucher;

import lombok.Data;

/**
 * @Auther: Administrator
 * @Date: 2023/7/5 0005 13:36
 * @Description: StaffVoucherInfo
 * @Version 1.0.0
 */
@Data
public class StaffVoucherInfo {

    /**
     * 记录id
     */
    private Integer id;

    /**
     * 员工ID
     */
    private Integer staffId;

    /**
     * 记录员工编号 employee_no
     */
    private String employeeNo;

    /**
     * 员工姓名 fld_name
     */
    private String staffName;

    /**
     * 员工手机号
     */
    private String mobile;

    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 员工部门名称 a/b/c dept_name
     */
    private String deptName;

    /**
     * 职级ID
     */
    private Integer rankId;

    /**
     * 职级名称
     */
    private String rankName;

    /**
     * 人员类型
     */
    private String comeFrom;


    /**
     * 证件号 identity_card
     */
    private String identityCard;

    /**
     * 人脸照片 不导出
     */
    private String faceImg;

    /**
     * 门禁卡号  access_code
     */
    private String accessCode;

    /**
     * 凭证数量
     */
    private Integer voucherCnt;
}
