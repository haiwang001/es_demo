package com.bjxczy.onepark.common.model.voucher;


import lombok.Data;

import java.io.Serializable;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/7/13 15:36
 */
@Data
public class CardInfoTransmit  implements Serializable {
    private static final long serialVersionUID = -7985788983625831511L;
    /**
     * 卡号,8~20个字符，支持数字和大写字母。 建议是由读卡器直接读出来的。 如果是三方系统同步过来，需要提前调研卡号的规则所购买的设备上是否兼容
     */
    private String cardNo;
    /**
     * 人员ID，根据 获取人员列表v2 接口获取返回参数personId
     * 对应智慧园区的 staffId
     */
    private String personId;
    /**
     * 卡片类型，默认是1：IC卡
     * 1：IC卡
     * 2：CPU卡
     * 3：远距离卡
     * 4：MIFARE卡
     */
    private String cardType;

    public CardInfoTransmit(String cardNo, String personId, String cardType) {
        this.cardNo = cardNo;
        this.personId = personId;
        this.cardType = cardType;
    }
}
