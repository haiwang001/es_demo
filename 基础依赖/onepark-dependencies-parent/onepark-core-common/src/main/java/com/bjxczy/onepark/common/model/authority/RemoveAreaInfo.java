package com.bjxczy.onepark.common.model.authority;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName RemoveAreaInfo
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/1 10:08
 * @Version 1.0
 */
@Data
public class RemoveAreaInfo implements Serializable {
    private static final long serialVersionUID = 7740198847125360263L;

    private Integer id;
}
