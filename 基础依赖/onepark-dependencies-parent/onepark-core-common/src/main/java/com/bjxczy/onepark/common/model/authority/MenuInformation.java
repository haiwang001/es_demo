package com.bjxczy.onepark.common.model.authority;

import lombok.Data;

@Data
public class MenuInformation {
	
	private Integer id;
	
	private String uuid;
	
	private String code;
	
	private String name;
	
	private String fullName;
	
}
