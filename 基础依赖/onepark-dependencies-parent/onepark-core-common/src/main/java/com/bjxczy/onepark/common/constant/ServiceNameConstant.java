package com.bjxczy.onepark.common.constant;

public interface ServiceNameConstant {
	
	String ORGANIZATION = "organization";
	
	String USER = "user-server";
	
	String AUTHORITY_RESOURCE = "bmpresource";
	
	String AUTHORITY_ROLE = "bmprole";

}
