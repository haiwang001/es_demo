package com.bjxczy.onepark.common.model.workflow;

import java.io.Serializable;

public interface DescribeValue extends Serializable {
	
	String getValue();

}
