package com.bjxczy.onepark.common.model.visitor;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
 *@Author wlw
 *@Date 2023/6/25 17:15
 */
@Data
@NoArgsConstructor
public class CreateVisitorAndDoorVo implements Serializable {
    private static final long serialVersionUID = 5408449823071002452L;
    private String id;//访客主键
    private int code;
    private String msg;
    private String type;// 开放平台类型StaffInformation 海康/汉王/dds 等等

    public CreateVisitorAndDoorVo(int code, String msg, String type) {
        this.code = code;
        this.msg = msg;
        this.type = type;
    }

    public CreateVisitorAndDoorVo(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
