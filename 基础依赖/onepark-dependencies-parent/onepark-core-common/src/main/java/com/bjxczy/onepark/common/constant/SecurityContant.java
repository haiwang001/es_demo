package com.bjxczy.onepark.common.constant;

public interface SecurityContant {

	String SYS_USER_ID = "sys-user-id";
	
	String SYS_STAFF_NAME = "sys-staff-name";
	
	String SYS_USERNAME = "sys-username";
	
	String SYS_USER_ROLES = "sys-user-roles";
	
	String SYS_TENANT_NAME = "x-tenant-header";
	
}
