package com.bjxczy.onepark.common.model.visitor;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.Serializable;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/24 10:35
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateVisitorInformation implements Serializable {
    private static final long serialVersionUID = -5021132464039581185L;
    private String id;//访客主键
    private String visitorName;
    private Integer receptionistId;
    private String visitStartTime;
    private String visitEndTime;
    private String mobile;
    private String gender;
    private String visitorPhoto;
    private List<String> group; //访客权限组
}
