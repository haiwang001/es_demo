package com.bjxczy.onepark.common.model.common;

import java.io.Serializable;
import java.util.Set;

import com.bjxczy.onepark.common.constant.ExtendHttpMethod;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ApiResourceInformation implements Serializable {
	
	private static final long serialVersionUID = -7144644933072493088L;

	private String name;
	
	private String serviceName;
	
	private String className;
	
	private Set<ExtendHttpMethod> httpMethods;
	
    private Set<String> patterns;
    
}
