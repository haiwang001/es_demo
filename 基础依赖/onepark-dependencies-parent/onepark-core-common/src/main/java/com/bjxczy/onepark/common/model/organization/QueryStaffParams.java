package com.bjxczy.onepark.common.model.organization;

import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class QueryStaffParams {

	private List<Integer> organizationIds;

	private List<Integer> rankIds;

	private List<Integer> positionIds;

	public QueryStaffParams singletonOrganizationId(Integer organizationId) {
		this.organizationIds = Arrays.asList(organizationId);
		return this;
	}

	public QueryStaffParams singletonRankId(Integer rankId) {
		this.rankIds = Arrays.asList(rankId);
		return this;
	}

	public QueryStaffParams singletonPositionId(Integer positionId) {
		this.positionIds = Arrays.asList(positionId);
		return this;
	}

	public QueryStaffParams setOrganizatinIds(List<Integer> organizatinIds) {
		this.organizationIds = organizatinIds;
		return this;
	}

	public QueryStaffParams setRankIds(List<Integer> rankIds) {
		this.rankIds = rankIds;
		return this;
	}

	public QueryStaffParams setPositionIds(List<Integer> positionIds) {
		this.positionIds = positionIds;
		return this;
	}

}
