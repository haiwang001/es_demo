package com.bjxczy.onepark.common.model.organization;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/*
 *@ClassName StaffVO
 *@Author 温良伟
 *@Date 2023/1/20 9:48
 *@Version 1.0
 */
@Data
public class StaffInfoFine implements Serializable {

    private Date createTime;
    private Date updateTime;
    private String operator;

    private static final long serialVersionUID = 3695563047918075672L;

    private String positionName;

    private String rankName;

    private List<DeptNameInfo> deptNames;

    private Integer id;

    private String fldName;

    private String mobile;

    private String uuid;

    private String gender;

    private String email;

    private String employeeNo;

    private String identityCard;

    private List<String> photo;

    private Integer hasAccount;

    private Integer organizationId;

    private String positionIds;

    private Integer rankId;

    private String remark;

    private Integer comeFrom;

    /*
     * 员工id
     */
    private String staffId;
}
