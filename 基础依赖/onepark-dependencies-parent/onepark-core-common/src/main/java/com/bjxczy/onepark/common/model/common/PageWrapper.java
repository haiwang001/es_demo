package com.bjxczy.onepark.common.model.common;

import java.util.List;

import lombok.Data;

@Data
public class PageWrapper<T> {
	
	private Integer pageNum;
	
	private Integer pageSize;
	
	private Long total;
	
	private Integer pages;
	
	private List<T> records;

}
