package com.bjxczy.onepark.car.common.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class TrimUtils {

	public static void reflect(Object t) throws Exception {
//		Class cls = t.getClass();
//		Field[] fields = cls.getDeclaredFields();
//		for (int i = 0; i < fields.length; i++) {
//			Field f = fields[i];
//			f.setAccessible(true);
//			System.out.println("属性名:" + f.getName() + " 属性值:" + f.get(t));
//		}
//		return t;
		if(t!=null){
            //获取所有的字段包括public,private,protected,private
            Field[] fields = t.getClass().getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Field f = fields[i];
                if (f.getType().getName().equals("java.lang.String")) {
                    String key = f.getName();//获取字段名
                    Object value = getFieldValue(t, key);
                    
                    if (value == null)
                        continue;
                    
                    setFieldValue(t, key, value.toString().trim());
                }
            }
        }
	}
	
	private static Object getFieldValue(Object bean, String fieldName)
            throws Exception {
        StringBuffer result = new StringBuffer();
        String methodName = result.append("get")
                .append(fieldName.substring(0, 1).toUpperCase())
                .append(fieldName.substring(1)).toString();

        Object rObject = null;
        Method method = null;

        @SuppressWarnings("rawtypes")
        Class[] classArr = new Class[0];
        method = bean.getClass().getMethod(methodName, classArr);
        rObject = method.invoke(bean, new Object[0]);

        return rObject;
    }
	
	private static void setFieldValue(Object bean, String fieldName, Object value)
            throws Exception {
        StringBuffer result = new StringBuffer();
        String methodName = result.append("set")
                .append(fieldName.substring(0, 1).toUpperCase())
                .append(fieldName.substring(1)).toString();

        /**
         * 利用发射调用bean.set方法将value设置到字段
         */
        Class[] classArr = new Class[1];
        classArr[0]="java.lang.String".getClass();
        Method method=bean.getClass().getMethod(methodName,classArr);
        method.invoke(bean,value);
    }

}
