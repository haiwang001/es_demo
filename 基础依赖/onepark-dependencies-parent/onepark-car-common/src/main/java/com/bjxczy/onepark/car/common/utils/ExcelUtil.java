package com.bjxczy.onepark.car.common.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;

public class ExcelUtil {
	/**
	 * 
	 * @param response
	 * @param fileName			文件名称
	 * @param headNameMap		表头
	 * @param list				DTO数据
	 */
	public static <T> void exportXlsxByBean(HttpServletResponse response, String fileName, 
			Map<String, String> headNameMap, List<T> list){
		
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		List<Field> fields = new ArrayList<>();
		try {
			for(T t: list){
				Class<?> e = t.getClass();
				while (e != null){
					fields.addAll(Arrays.asList(e.getDeclaredFields()));
					e = e.getSuperclass();
				}
				Map<String, Object> map = new HashMap<String, Object>();
				if(fields != null){
					for(Field field : fields){
						field.setAccessible(true);
						map.put(field.getName(), field.get(t));
					}
				}
				dataList.add(map);
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		exportXlsx(response, fileName, headNameMap, dataList);
		
	}


	/**
	 * 设置web响应输出的文件名称
	 *
	 * @param response web响应
	 * @param fileName 导出文件名称
	 */
	private static void setResponseHeader(HttpServletResponse response, String fileName) {
		response.reset();
		response.setContentType("application/vnd.ms-excel;charset=utf-8");
		try {
			response.setHeader("content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName + ".xls", "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		response.setCharacterEncoding("UTF-8");
	}

	public static void exportExcal(HttpServletResponse response, String fileName, String sheetName, List<?> data, Class<?> tClass, Integer column) throws IOException {
		setResponseHeader(response, fileName);
		ExportParams exportParams = new ExportParams();
		// 设置sheet得名称
		exportParams.setSheetName(sheetName);
//		exportParams.setDataHandler(new ExeclDataHandler());
		Map<String, Object> exportMap = new HashMap<>();
		// title的参数为ExportParams类型，目前仅仅在ExportParams中设置了sheetName
		exportMap.put("title", exportParams);
		// 模版导出对应得实体类型
		exportMap.put("entity", tClass);
		// sheet中要填充得数据
		exportMap.put("data", data);
		List<Map<String, Object>> sheetsList = new ArrayList<>();
		sheetsList.add(exportMap);
		//Workbook就是在内存中的Excel对应的对象
		Workbook wb = ExcelExportUtil.exportExcel(sheetsList,
				ExcelType.HSSF);

		//获取到你这个Excel的长和宽
		Sheet sheet = wb.getSheetAt(0);
		Row row = sheet.getRow(0);
		int rowNum = sheet.getLastRowNum();
		int colNum = row.getPhysicalNumberOfCells();

		//创建字体对象，注意这不是awt包下的，是poi给我们封装了一个
		Font font = wb.createFont();
		short index = HSSFColor.HSSFColorPredefined.RED.getIndex();
		font.setColor(index);
		font.setFontHeightInPoints((short) 10);
		font.setFontName("Arial");
		Integer line = 1;
		//效率非常低的二次循环遍历
		for (int i = 1; i <= rowNum; i++) {
			row = sheet.getRow(i);
			int j = 0;
			while (j < colNum) {
				//这里我们就获得了Cell对象，对他进行操作就可以了
				Cell cell = row.getCell((short) j);
				String value = row.getCell((short) j).toString();
				value = value.trim();
				if (j == line) {
					if (i >=1 && i < column+1) {
						CellStyle cellStyle = wb.createCellStyle();
						cellStyle.setAlignment(HorizontalAlignment.CENTER);
						cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
						cellStyle.setFont(font);
						cell.setCellStyle(cellStyle);
					}
				}
				j++;
			}
		}
		ServletOutputStream outputStream = response.getOutputStream();
		wb.write(outputStream);
		outputStream.flush();
		outputStream.close();
	}
	/**
	 * 
	 * @param response
	 * @param fileName
	 * @param headNameMap
	 * @param dataList
	 */
	public static void exportXlsx(HttpServletResponse response, String fileName, 
			Map<String, String> headNameMap, List<Map<String, Object>> dataList){
		
		Workbook workbook = exportXlsx(fileName, headNameMap, dataList);
		
		response.setContentType("application/binary;charset=ISO8859_1");
		
		OutputStream outputStream = null;
		
		try {
			outputStream = response.getOutputStream();
			String fn = new String(fileName.getBytes(), "ISO8859_1");
			response.setHeader("Content-disposition", "attachment; filename=" + fn  + ".xlsx");
			workbook.write(outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(outputStream != null){
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	/**
	 * 导出数据
	 * @param headNameMap
	 * @param dataList
	 */
	public static Workbook exportXlsx(String sheetName, Map<String, String> headNameMap, List<Map<String, Object>> dataList){
		
//		Workbook workbook = new XSSFWorkbook();
		Workbook workbook = new SXSSFWorkbook();
		
		Sheet sheet = workbook.createSheet(sheetName);
		
		Set<String> keys = headNameMap.keySet();
		
		int i = 0, j = 0;
		Row row = sheet.createRow(i++);
		//表头
		for(String key : keys){
			Cell cell = row.createCell(j++);
			cell.setCellValue(headNameMap.get(key));
		}
		
		//内容
		if(dataList != null && !dataList.isEmpty()){
			for(Map<String, Object> map : dataList){
				row = sheet.createRow(i++);
				j = 0;
				for(String key : keys){
					Cell cell = row.createCell(j++);
					setCellValue(cell, map.get(key));
				}
			}
		}
		
		return workbook;
	}
	
	private static void setCellValue(Cell cell, Object obj){
		if(obj == null){
		}else if(obj instanceof String){
			cell.setCellValue((String) obj);
		}else if(obj instanceof Date){
			Date date = (Date) obj;
			if(date != null){
				cell.setCellValue(DateUtils.dfDateTime.format(date));
			}
		}else if(obj instanceof Calendar){
			Calendar calendar = (Calendar) obj;
			if(calendar != null){
				cell.setCellValue(DateUtils.dfDateTime.format(calendar.getTime()));
			}
		}else if(obj instanceof Timestamp){
			Timestamp timestamp = (Timestamp) obj;
			if(timestamp != null){
				cell.setCellValue(DateUtils.dfDateTime.format(new Date(timestamp.getTime())));	
			}
		}else if(obj instanceof Double){
			cell.setCellValue((Double) obj);
		}else{
			cell.setCellValue(obj.toString());
		}
	}
	
	/**
	 * 读取excel
	 * @param path
	 * @param startIdx
	 * @return
	 */
	public static List<List<String>> readXlsx(String path, int startIdx){
		
		try {
			InputStream is = new FileInputStream(path);
			
			return readXlsx(is, startIdx);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@SuppressWarnings("resource")
	public static List<List<String>> readXlsx(InputStream is, int startIdx){
		
		List<List<String>> list = new ArrayList<List<String>>();
		
		try {
			
			XSSFWorkbook xssfWorkbook = new XSSFWorkbook(is);
			
			XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
			if(xssfSheet == null){
				return list;
			}
			
			int endIdx = xssfSheet.getLastRowNum() + 1;
			if(endIdx > startIdx){
				for(; startIdx < endIdx; startIdx++){
					XSSFRow xssfRow = xssfSheet.getRow(startIdx);
					if(xssfRow != null){
						List<String> rowList = new ArrayList<String>();
						int colNum = xssfRow.getLastCellNum();
						boolean isAdd = false;
						for(int i=0; i<colNum; i++){
							XSSFCell cell = xssfRow.getCell(i);
							String str = getValue(cell);
							rowList.add(str);
							if(StringUtils.isNotBlank(str)){
								isAdd = true;
							}
						}
						if(isAdd){
							list.add(rowList);	
						}
					}
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return list;
	}
	
	 private static String getValue(XSSFCell xssFCell) {
		 String str = null;
		 if(xssFCell == null){
			 return str;
		 }
		 if (xssFCell.getCellType() == CellType.BOOLEAN) {
			 str = String.valueOf(xssFCell.getBooleanCellValue());
		 } else if (xssFCell.getCellType() == CellType.NUMERIC) {
			 str = String.valueOf(new DecimalFormat("#").format(xssFCell.getNumericCellValue()));
		 } else {
			 str = String.valueOf(xssFCell.getStringCellValue());
		 }
		 return StringUtils.trim(str);
	}

	 public static boolean validatePlateNumber(String plateNumber) {
		 Pattern p = Pattern.compile("^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1}$");
		 Pattern p2 = Pattern.compile("^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领]{1}[A-Z]{1}(([0-9]{5}[DABCEFGHJK]$)|([DABCEFGHJK][A-HJ-NP-Z0-9][0-9]{4}$))");	
		 return p.matcher(plateNumber).matches() || p2.matcher(plateNumber).matches() ;
			
	 }
}
