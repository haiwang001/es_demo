package com.bjxczy.onepark.car.common.utils;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;

/**
 * @Author lizhipeng
 * @Date 2021.05.24 14:26
 * @Description：将pageHelper的pageInfo转化为园区统一page对象
 */
public class PageTransformUtil {

    public static <T>Page<T> transform(PageInfo<T> pageInfo){
        Page<T> page = new Page<>();
        page.setSize(pageInfo.getPageSize());
        page.setTotal(pageInfo.getTotal());
        page.setCurrent(pageInfo.getPageNum());
        page.setPages(pageInfo.getPages());
        page.setRecords(pageInfo.getList());
        return page;
    }

    /**
     * 将参数中可能的翻页参数为null情况置为默认值
     * @author lizhipeng
     */
    public static Map<String, Object> getParams(Map<String, Object> params){
        int pageNum = 1;
        String pageStr = (String)params.get("pageNum");
        if (StringUtils.isNotBlank(pageStr)) {
            pageNum = Integer.parseInt(pageStr);
        }
        params.put("pageNum", pageNum);
        int pageSize = 10;
        String pageSizeStr = (String)params.get("pageSize");
        if (StringUtils.isNotBlank(pageSizeStr)) {
            pageSize = Integer.parseInt(pageSizeStr);
        }
        params.put("pageSize", pageSize);
        return params;
    }
}
