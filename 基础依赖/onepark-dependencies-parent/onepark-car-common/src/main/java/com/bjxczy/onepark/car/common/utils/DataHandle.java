package com.bjxczy.onepark.car.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author admin
 * @Date 2021.07.01 11:36
 * @Description：${description}
 * @Version: $version$
 */
public class DataHandle {

    public static void setLineChart(Map<String, Object> result, String today, List<Map<String, Object>> todayInLineChart, List<Map<String, Object>> todayOutLineChart){
        List<Map<String, Object>> todayIn = new ArrayList<>();
        String todayTime = today+" 00:00:00";
        for(int i=0; i<24; i++){
            String d = DateTools.afterHour(todayTime,i);
            d = d.substring(11,16);
            Map<String, Object> empty = new HashMap<>();
            empty.put("inTime",d);
            empty.put("inNumber",0);
            todayIn.add(empty);
            for(Map<String, Object> re : todayInLineChart){
                if(re.get("inTime").equals(d)){
                    empty.put("inNumber",re.get("inNumber"));
                }
            }
        }
        List<Map<String, Object>> todayOut = new ArrayList<>();
        for(int i=0; i<24; i++){
            String d = DateTools.afterHour(todayTime,i);
            d = d.substring(11,16);
            Map<String, Object> empty = new HashMap<>();
            empty.put("outTime",d);
            empty.put("outNumber",0);
            todayOut.add(empty);
            for(Map<String, Object> re : todayOutLineChart){
                if(re.get("outTime").equals(d)){
                    empty.put("outNumber",re.get("outNumber"));
                }
            }
        }
        result.put("todayInLineChart",todayIn);
        result.put("todayOutLineChart",todayOut);
    }

    public static void setSevenDaysLineChart(Map<String, Object> result, String startDay, List<Map<String, Object>> sevenDaysOutLineChart, List<Map<String, Object>> sevenDaysInLineChart){
        List<Map<String, Object>> inEmpty = new ArrayList<>();
        for(int i=0; i<7; i++){
            String d = DateTools.getAfterDate(startDay,i);
            d = d.substring(5,10);
            Map<String, Object> empty = new HashMap<>();
            empty.put("inTime",d);
            empty.put("inNumber",0);
            inEmpty.add(empty);
            for(Map<String, Object> re : sevenDaysInLineChart){
                if(re.get("inTime").equals(d)){
                    empty.put("inNumber",re.get("inNumber"));
                }
            }
        }
        result.put("sevenDaysInLineChart",inEmpty);
        List<Map<String, Object>> outEmpty = new ArrayList<>();
        for(int i=0; i<7; i++){
            String d = DateTools.getAfterDate(startDay,i);
            d = d.substring(5,10);
            Map<String, Object> empty = new HashMap<>();
            empty.put("outTime",d);
            empty.put("outNumber",0);
            outEmpty.add(empty);
            for(Map<String, Object> re : sevenDaysOutLineChart){
                if(re.get("outTime").equals(d)){
                    empty.put("outNumber",re.get("outNumber"));
                }
            }
        }
        result.put("sevenDaysOutLineChart",outEmpty);
    }
}
