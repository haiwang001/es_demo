XC-OnePark架构设计文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录                                                |
| --------- | ------ | ---------- | ----------------------------------------------------------- |
| v0.0.1    | 郑悦来 | 2022.12.12 | 文档初版                                                    |
| v0.0.2    | 郑悦来 | 2022.12.12 | 修订伪代码中BUG                                             |
| v0.0.3    | 郑悦来 | 2022.12.13 | 新增四.4、四.5章节                                          |
| v0.0.4    | 郑悦来 | 2022.12.19 | 新增第五章：1，docker-compose部署                           |
| v0.0.5    | 郑悦来 | 2023.01.03 | 编辑第四章，3，部分                                         |
| v0.0.6    | 郑悦来 | 2023.01.12 | 编辑第三章1，单模块服务搭建                                 |
| v0.0.7    | 郑悦来 | 2023.01.18 | 编辑第四章4、6，完善使用细节，新增第四章7，统一设备管理接入 |
| v0.0.8    | 郑悦来 | 2023.02.07 | 接口权限的实现，完成编写                                    |
| v0.0.9    | 郑悦来 | 2023.02.10 | 统一数据权限的实现完成编写                                  |
| v0.0.10   | 郑悦来 | 2023.03.15 | elasticsearch，报表模块集成，统一日志接入编写               |

## 一、简介

OnePark智慧园区项目，采用Spring Cloud Alibaba为基础，以微服务方式支撑园区各式定制需求，如安防模块、智慧停车、门禁模块等。使用领域驱动设计拆分微服务模块，并采用DDD、事件驱动架构开发。

## 二、组件

### 1，技术选型

* 分布式系统套件版本：Spring Boot 2.x + Spring Cloud + Spring Cloud Alibaba 
* 服务治理注册与发现：Spring Cloud Alibaba Nacos 
* 统一配置中心：Spring Cloud Alibaba Nacos 
* 服务降级、熔断和限流：alibaba/Sentinel 
* 网关路由代理调用：Spring Cloud Gateway 
* 声明式服务调用：Spring Cloud OpenFeign 
* 服务负载均衡：Spring Cloud Netflix Ribbon 
* 服务安全认证：Spring Security 
* 数据访问层：Mybatis-plus 、Spring Data JPA
* 工具类：Apache Commons、hutools
* 其它：lombok、fastjson、mapstruct



### 2，定制通用组件

> XC-OnePark基础依赖：
>
> git：http://192.165.1.124:10000/chnEnergy/xconepark/onepark-dependencies-parent

#### R（标准返回）

ResultEnum.java

```Java
@Getter
public enum ResultEnum {

	SUCCESS(0, "请求成功！"), FAIL(1, "请求失败！"), ERROR(500, "请求异常！");
	
	private Integer code;
	
	private String message;
	
	private ResultEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	
}
```

如何使用

```java
// 构造方法

/**
 * 统一返回值
 * @param resultEnum 状态码定义
 * @param message 消息
 * @param data 数据
 */
public R(ResultEnum resultEnum, String message, T data)

// 返回成功消息
R.success();
// 返回JSON
{
    "code": 0,
    "message": "请求成功!",
    "data": null
}

// 返回消息体
Map<String, Object> data = new HashMap<>();
data.put("test", "test");
R.success("成功", data);
// 返回JSON
{
    "code": 0,
    "message": "请求成功!",
    "data": {
        "test": "test"
    }
}

// 返回失败消息
R.fail();
// 返回JSON
{
    "code": 1,
    "message": "请求失败!",
    "data": null
}

// 返回异常消息
R.error();
// 返回JSON
{
    "code": 500,
    "message": "请求异常!",
    "data": null
}

// 包含多个重载方法，具体可查看源码
```



#### Payload（数据包装）

```java
// 构造返回数据
R.success(payload -> {
    payload.set("name", "123");
    payload.set("age", 18);
});
R.success(payload -> payload.set("name","123").set("age"， 18));
// JSON
{
    "code": 0,
    "message": "请求成功!",
    "data": {
        "name": "123",
        "age": 18
    }
}
```



#### ResultException（异常信息返回）

```Java
@Service
public class TestService {
    ...
    @Override
    public void create() {
        boolean exists = // 业务判断
        ...
        if (exists) {
            throw new ResultException("XX已存在");
        }
        ...
    }
    ...
    @Override
    public void update() {
        boolean exists = // 业务判断
		// 使用hutools断言进行判断            
        Assert.isTrue(exists, () -> new ResultException("XX已存在"));
        ...
    }
    ...
}

// 经由统一异常处理，将返回JSON数据
{
    "code": 1,
    "message": "XX已存在",
    "data": null
}
```



#### 统一返回/异常处理

```java
@RestController
@RequestMapping("/user")
public class UserController {
    
    @PostMapping
    public void createUser() {
        // 创建用户
        userService.createUser()
    }
    // 当返回值为void时，经由统一返回处理，将转换为
	R.success();
    
    @GetMapping("/{id}")
    public Map<String, Object> getUser(@PathVariable("id") Integer id) {
        Map<String, Object> data = new HashMap<>();
        data.put("name", "123");
        data.put("age", 18);
        return data;
    }
    // 当返回值为Object时，经由统一返回处理，将转换为
    R.success(data);
    
    @GetMapping("/{id}")
    public R<?> getUser(@PathVariable("id") Integer id) {
        Map<String, Object> data = new HashMap<>();
        data.put("name", "123");
        data.put("age", 18);
        return R.success(data);
    }
    // 返回值为R实例时，将直接返回不进行处理
    
    // 当产生异常时，将转换为
    R.error();
    
}

```



#### MybatisPlus实现：逻辑删除、用户操作时间

> 为应对园区数据要求采用逻辑删除，用户操作数据需保存操作时间、操作人信息，故抽取为公用处理，采用Mybatis Plus的插件实现
>
> 官方文档：https://baomidou.com/pages/6b03c5/

基类

```Java
// LogicDelete 逻辑删除基类
@Getter
@Setter
public class LogicDelete implements Serializable {
	
    // 删除标记
	@TableLogic(value = "0", delval = "1")
	private Integer delFlag;

}
// UserOperator 用户操作基类
@Getter
@Setter
public class UserOperator extends LogicDelete implements Serializable {

    // 创建时间
	private Date createTime;
	// 更新时间
	private Date updateTime;
	// 操作人（暂未实现）
	private String operator;
	
}
```

实现逻辑删除

```Java
// 表结构
create table tbl_user (
    id varchar(32) primary key,
    name varchar(50),
    del_flag tinyint(1)
)

// 实体
@Data
@TableName("tbl_user")
public class UserPO extends LogicDelete {
    
    @TableId(type = IdType.UUID)
    private String id;
    
    private String name;
    
}
// Mybatis Plus ServiceImpl
ServiceImpl.save(userPO); // 将填充del_flag = 0
ServiceImpl.remove(userPO); // 将更新del_flag = 1
ServiceImpl.list()、ServiceImpl.lamdbaQuery().list(); // 等MybatisPlus提供查询操作将忽略del_flag = 1数据

```



实现用户操作

```java
// 表结构
create table tbl_user (
    id varchar(32) primary key,
    name varchar(50),
    create_time datetime,
    update_time datetime,
    operator varchar(50),
    del_flag tinyint(1)
)

// 实体
@Data
@TableName("tbl_user")
public class UserPO extends UserOperator {
    
    @TableId(type = IdType.UUID)
    private String id;
    
    private String name;
    
}
// Mybatis Plus ServiceImpl
ServiceImpl.save(userPO); 
// 将调用UserOperator.setCreateTime(new Date())、UserOperator.setUpdateTime(new Date());
ServiceImpl.updateById(userPO); 
// 将调用UserOperator.setUpdateTime(new Date())
```



#### 数据脱敏的实现

##### 1）基础依赖

```xml
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-web</artifactId>
</dependency>
```

##### 2）启用脱敏功能

application.yml增加如下配置

```yaml
onepark:
  privacy:
    enable: true
    default-config-id: workflow  # 默认configId，当@ResponseMasking.configId为空时，将使用此ID
```

##### 3）增加脱敏配置中心

nacos中privacyStore.json增加所需配置

```json
{
	...
	
	"baseEngine.sysLog": {  // 配置中心ID
		"title": "智脑引擎/日志管理",  // 标题
		"oper": [	// 支持操作
			"query",  
			"export"
		],
		"query": {
			"roles": [], // 例外角色，默认空即可，前端页面进行配置
			"options": [{  // 支持模式，最多支持以下三个选项，value不支持修改
					"label": "正常显示",
					"value": 1
				},
				{
					"label": "安全显示",
					"value": 2
				},
				{
					"label": "始终隐藏",
					"value": 3
				}
			],
			"title": "查询显示模式",  // 操作标题
			"value": 3   // 默认模块
		},
		"export": {
			"roles": [],
			"options": [{
					"label": "正常显示",
					"value": 1
				},
				{
					"label": "始终隐藏",
					"value": 3
				}
			],
			"title": "导出数据模式",
			"value": 3
		}
	}
	
	...
}
```

保存配置后，登录平台可通过页面：**设置/隐私设置**，进行配置

![image_20230425](doc/images/image_20230425.PNG)

##### 4）数据脱敏的实现

###### 基于@ResponseMasking的实现

> 适用与对象及Map的脱敏，仅可用于Controller层

通用脱敏处理器

> 对特定字段进行脱敏处理，自定义脱敏参考下文中脱敏处理的实现

* EmployeeNoMaskingHandler：员工编号脱敏
* IdCardMaskingHandler：身份证号脱敏
* MobileMaskingHandler：手机号脱敏
* PlateNumberMaskingHandler：车牌号脱敏

通用脱敏迭代器：

> 用于将Response的内容转换为列表，再进行迭代。自定义迭代器参考下文脱敏迭代器的实现

* ListMaskingIterator
* IPageMaskingIterator：MybatisPlus IPage
* PageWrapper：报表模块分页对象
* ExportWrapperMaskingIterator：报表模块导出迭代器
* SingletonMaskingIterator：单个对象迭代器

```java
@ApiResourceController
@RequestMapping("/log")
public class LogPageController {

	@Autowired
	private LogPageService logPageService;

  	// 支持Map的脱敏，通过对应MaskingField.name作为Key获取数据，进行脱敏后重新写入
	@GetMapping(value = "/page", name = "日志查询")
	@ResponseMasking(configId = "baseEngine.sysLog",  // 配置中心ID
                     operation = MaskingOperation.QUERY, // 配置操作，目前仅支持QUERY-查询、EXPORT-导出
                     fields = {
							@MaskingField(name = "mobile", typeHandler = MobileMaskingHandler.class), // 脱敏字段
							@MaskingField(name = "employeeNo", typeHandler = EmployeeNoMaskingHandler.class) // 脱敏字段
					 }, 
                     iterator = PageWrapperMaskingIterator.class)  // 迭代器
	public PageWrapper<Map<String, Object>> pageLog(QueryLogDTO dto) {
		return logPageService.queryPageLog(dto);
	}
    
    // 支持对象实例的脱敏，通过反射获取MaskingField.name对应字段值，进行脱敏后重新写入
    @GetMapping(value = "/page", name = "日志查询")
	@ResponseMasking(configId = "baseEngine.sysLog",  // 配置中心ID
                     operation = MaskingOperation.QUERY, // 配置操作，目前仅支持QUERY-查询、EXPORT-导出
                     fields = {
							@MaskingField(name = "mobile", typeHandler = MobileMaskingHandler.class), // 脱敏字段
							@MaskingField(name = "employeeNo", typeHandler = EmployeeNoMaskingHandler.class) // 脱敏字段
					 }, 
                     iterator = PageWrapperMaskingIterator.class)  // 迭代器
	public List<SysLogVO> pageLog(QueryLogDTO dto) {
		return logPageService.queryPageLog(dto);
	}
}
```

###### 基于@ResponseMasking与VO的实现

定义VO

```java
public class UserVO {

	private Integer staffId;
	
	private String staffName;
	
    @MaskingField(typeHandler = MobileMaskingHandler.class)
	private String mobile;
	
    @MaskingField(typeHandler = EmployeeNoMaskingHandler.class)
	private String employeeNo;
	
	private String email;
	
	private List<DeptNameInfo> deptNames;
	
	private Integer rankId;
	
	private String rankName;

}
```

返回将自动脱敏

```java
@GetMapping(value = "/users", name = "账号列表")
@ResponseMasking(configId = "baseEngine.user", operation = MaskingOperation.QUERY, iterator = IPageMaskingIterator.class)
public Page<UserVO> listUser(QueryUserDTO dto) {
	return userQueryService.listUser(dto);
}
```

###### 基于PrivacyConfigService的实现

> 手动执行脱敏操作，为@ResponseMasking的底层实现，一般适用于导出场景的数据脱敏

```java
@SpringBootTest
public class PrivacyConfigServiceTest {

	@Autowired
	private PrivacyConfigService privacyConfigService;
	
	@Test
    public void test1() {
        List<UserVO> list = // 查询用户列表
        // 基于VO中定义@MaskingField进行脱敏
        privacyConfigService.doMasking("baseEngine.user", MaskingOperation.EXPORT, new ListMaskingIterator().set(list));
        System.out.println(list)
    }
    
    @Test
    public void test2() {
		List<UserVO> list = // 查询用户列表
        // 基于手动设置的方式进行脱敏
        privacyConfigService.doMasking("baseEngine.user", MaskingOperation.EXPORT,
				MapUtil.builder(new HashMap<String, MaskingHandler>())
						.put("mobile", new MobileMaskingHandler())
						.put("employeeNo", new EmployeeNoMaskingHandler()).build(),
				new ListMaskingIterator().set(list));
        System.out.println(list)
    }

}
```

###### 单个对象的脱敏的实现

```java
@GetMapping(value = "/user/{id}", name = "获取用户")
@ResponseMasking(configId = "baseEngine.user", operation = MaskingOperation.QUERY, iterator = SingletonMaskingIterator.class)
public UserVO getUserById(@PathVariable("id") Integer userId) {
	return userQueryService.getVoById(userId);
}
```

##### 5）自定义脱敏处理类

###### 脱敏迭代器的实现

> 实现一个对数组进行脱敏迭代的处理

```java
public class ArrayMaskingIterator implements MaskingIterator {
	
	private Iterator<Object> it;

	@Override
	public MaskingIterator set(Object obj) {
		if (obj.getClass().isArray()) {
			Object[] array =  (Object[]) obj;
			this.it = Arrays.asList(array).iterator();
		} else {
			throw new IllegalArgumentException(
					"参数类型错误，期望数组，实际[" + obj.getClass() + "]");
		}
		return this;
	}

	@Override
	public Object next() {
		return it.next();
	}

	@Override
	public boolean hasNext() {
		return it.hasNext();
	}

}
```

###### 脱敏处理器的实现

> 实现一个姓名脱敏处理器

```java
public class ChineseNameMaskingHandler extends MaskingHandler {

	@Override
	public String getMaskingValue(String value) {
		return StrUtil.desensitized(value, DesensitizedUtil.DesensitizedType.CHINESE_NAME));
	}
	
}
```

#### 统一数据权限的实现

> 描述：用户根据角色的不同，所拥有数据权限将不一致，OnePark项目中通过局址划分数据权限

##### 1）基础依赖

```java
// 定义拦截器
@Configuration
public class WebMvcConfig extends DefaultWebMvcConfig {
	
}
// 如需自定义拦截器，请重写父类方法，但需要同时执行父类方法
@Configuration
public class WebMvcConfig extends DefaultWebMvcConfig {
    @Override
	public void addInterceptors(InterceptorRegistry registry) {
		// TODO 可添加自定义拦截
		super.addInterceptors(registry); // 此行需要保留
	}
}
```

##### 2）服务配置

> 如果服务数据需划分数据权限，则应进行此配置

1. 修改bmprole服务application.yml配置

```yaml
...
onepark:
  ...
  subdivision:
  - bmpsecurity
  - office
  - bmpcar
  - bmpdoor
  - {需增加根菜单code} 
...
```

2. 重新部署bmprole服务

##### 3）获取TenantInfo

> 前端传递header经由拦截器，将自动转换为TenantInfo，可通过ThreadLocal获取
>
> 样例：x-tenant-header:1598563657158098946,1598563778058911746
>

```java
// 获取TenantInfo，如不存在将抛出Runtime异常消息给前端
TenantInfo info = TenantContextHolder.get();
{
    "code": 500,
    "message": "系统异常",
    "exception": "Tenant不能为空！",
    "data": null
}
// 允许获取TenantInfo为空
TenantInfo info = TenantContextHolder.get(false);
// 判断TenantInfo是否存在
boolean exists = TenantContextHolder.isExists()
```
##### 4）TenantInfo的使用

```java
// 查询
TenantInfo info = TenantContextHolder.get();
String value = info.getValue(); // 返回header原始值
List<String> list = info.getTenantList(); // 返回转换List：["1598563657158098946","1598563778058911746"]
String inCondition = info.getInCondition(); // 返回：('1598563657158098946','1598563657158098946')，可拼接至SQL

// MybaticPlus查询业务
QueryWrapper<User> wrapper = new QueryWrapper<>();
if (dto.getTenantId() == null) {
    // 如前端未传递TenantId，则查询数据范围应为授权范围
    wrapper.in("tenant_id", info.getTenantList());
} else {
    // 如前端已传递TenantId，则按照实际条件过滤
    info.assertContains(dto.getTenantId()); // 校验租户ID是否满足数据范围
    wrapper.eq("tenant_id", dto.getTenantId());
}

// 更新
// entity: 表实体类
TenantContextHolder.get().assertContains(entity.getTenantId());
save(entity); // 保存数据前请校验是否满足数据范围
```




#### 接口参数校验的实现

// TODO



#### 接口权限的实现

##### 1）包结构

![images-20230207172354](doc/images/Snipaste_2023-02-07_17-23-54.png)

```java
*.controller
--authenticated  // 登录即可访问接口
--authorized  // 需授权访问接口
```

##### 2）定义Controller

```java
@ApiResourceController  // 接入接口权限API需定义此注解，功能等价于@RestController
@RequestMapping("/api")
public class ApiCommonController {

	@Autowired
	private ApiQueryService apiQueryService;

	@GetMapping(value = "/list/{scope}", name = "范围查询API列表") // 接口需定义name，用于页面显示
	public List<ApiResourceVO> listByScope(@PathVariable("scope") String scope,
			@RequestParam(value = "unbind", required = false) Integer unbind) {
		return apiQueryService.listByScope(scope, unbind);
	}
	
	@GetMapping(value = "/services", name = "查询API服务")
	public List<String> listApiServices() {
		return apiQueryService.listApiServices();
	}

}
```

##### 3）配置接口访问范围

重启应用，登录OnePark平台页面查看API：设置/API管理

![image-1](doc/images/Snipaste_2023-02-07_17-33-36.png)

修改API访问范围

![image-2](doc/images/Snipaste_2023-02-07_17-35-13.png)

同步API配置至网关模块

![image-4](doc/images/Snipaste_2023-02-07_17-38-31.png)

设置/菜单配置，绑定API至菜单，选择需绑定菜单，点击添加API

![image-4](doc/images/Snipaste_2023-02-07_17-41-54.png)

#### 统一字典模块

##### 1）创建字典表

```sql
create table user_dict # 表名称自由定义
(
	id int primary key,
	group_code varchar(100),
    label varchar(100),
    value varchar(100),
    extra varchar(255),
    del_flag tinyint
)
```

##### 2）引入依赖

```xml
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-common</artifactId>
</dependency>
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-web</artifactId>
</dependency>
```

##### 3）声明字典Bean组件

```java
// 配置类
@Configuration
public class SomeConfig {
	...
	@Bean
	public DictService dictService() {
		return new JdbcDictServiceImpl("user_dict"); // 事先创建的字典表名称，建议写入配置文件@Value方式引入
	}
	...
}

// DictServie提供方法
public interface DictService {
	
	/**
	 * 查询字典数据
	 * @param groupCode 分组编码
	 * @return
	 */
	List<DictInformation> listDict(String groupCode);
	
	/**
	 * 查询描述
	 * @param groupCode 分组编码
	 * @param value 字典值
	 * @return
	 */
	String getLabel(String groupCode, Object value);
	
	/**
	 * 获取value作为Key的Map
	 * @param groupCode
	 * @return
	 */
	Map<String, DictInformation> valueKeyMap(String groupCode);
	
}
```

##### 4）前端获取字典数据

> 上述配置完成后，将自动配置接口

​	4.1 查询字典列表

| 地址 | /dict/list/{groupCode} |
| - | - |
| 请求方式 | GET |

参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| groupCode | String | Path | 分组编码 |

返回数据：

```json
{
    "code": 0,
    "message": "请求成功！",
    "data": [
        {
            "id": 1,
            "groupCode": "userType",
            "label": "Web用户",
            "value": "1",
            "extra": null,
            "delFlag": 0
        },
        {
            "id": 2,
            "groupCode": "userType",
            "label": "手机用户",
            "value": "2",
            "extra": null,
            "delFlag": 0
        }
    ]
}
```
​	4.2 查询字典Map

| 地址 | /dict/map/{groupCode} |
| - | - |
| 请求方式 | GET |

参数：

| 参数名 | 类型 | 位置  | 说明  |
| - | - | - | - |
| groupCode | String | Path | 分组编码 |

返回数据：
```json
{
    "code": 0,
    "message": "请求成功！",
    "data": {
        "1": {
            "id": 1,
            "groupCode": "userType",
            "label": "Web用户",
            "value": "1",
            "extra": null,
            "delFlag": 0
        },
        "2": {
            "id": 2,
            "groupCode": "userType",
            "label": "手机用户",
            "value": "2",
            "extra": null,
            "delFlag": 0
        }
    }
}
```

##### 5）编程式获取字典数据

```java
@SpringBootTest
public class DictServiceTest {
	
	@Autowired
	private DictService dictService;
	
	@Test
	public void listDictTest() {
		List<DictInformation> list = dictService.listDict("userType");
		System.out.println(list);
	}
	
	@Test
	public void valueKeyMapTest() {
		Map<String, DictInformation> map = dictService.valueKeyMap("userType");
		System.out.println(map);
	}
	
	@Test
	public void getLabelTest() {
		String label = dictService.getLabel("userType", 1);
		System.out.println(label);
	}

}
```

#### 日志集成

> 业务场景：对于业务的实体，需在日志描述中有所体现，举例如下：
>
> 操作：编辑用户信息。日志：编辑用户${userName}信息

##### 1）启用日志配置

```yaml
onepark:
  log:
    enable: true  # 是否启用日志上报
    show-log: false  # 是否打印日志
```

##### 2）切点注解

```java
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface SysLog {

	OperationType operationType() default OperationType.UNKNOWN; // 操作类型
	
	String operation();  // 操作描述
	
	String platform() default PlatformConstants.WEB;  // 客户端：默认管理端
	
}

```

##### 3）OperactionType定义

```java
public enum OperationType {
    /**
     * 操作类型
     */
    UNKNOWN("unknown", "未知"),
    DELETE("remove", "删除"),
    SELECT("get", "查询"),
    UPDATE("put", "更新"),
    INSERT("post", "新增"),
    EXPORT("export", "导出"),
    APPROVAL("approval", "审批"),
    OPEN("open", "访问");

	@Getter
	@Setter
    private String value;
    
	@Getter
	@Setter
    private String label;

    OperationType(String value, String label) {
        this.value = value;
        this.label = label;
    }

}
```

##### 4）Controller操作

```Java
@RestController
@RequestMapping("/car/certificate")
public class CarCertificateController {

    @GetMapping(value = "overview", name = "车证概览")
    @SysLog(operationType = OperationType.SELECT, operation = "查询车证管理")
    public Result<List<CarOverviewVO>> queryOverview() {
        return Result.succeed(carCertificateService.queryOverview());
    }
    
}
```

##### 5）变量

> 日志操作描述operation支持Velocity语法，可使用以下两种方式传入变量

@LogParam

```java
// 参数
@SysLog(operationType = OperationType.INSERT, operation = "创建：${name}")
public void create(@LogParam String name) {
	...
}

// 参数命名
@SysLog(operationType = OperationType.INSERT, operation = "创建：${account}")
public void create(@LogParam("account") String name) {
	...
}

// 多参数
@SysLog(operationType = OperationType.INSERT, operation = "创建：${account}，创建人：${user.username}")
public void create(@LogParam("account") String name, @LoginUser @LogParam UserInformation user) {
	...
}

// 条件控制
@SysLog(operationType = OperationType.UPDATE, operation = "#if($command.enabled==1)启用#else禁用#end账号：${ctx.username}")
public void accountEnabled(@LogParam EnableUserCommand command) {
    userApplicationService.enableUser(command);
}
```

LogContextHolder

> 此方式可在Controller调用流程中使用，例如：Service

```java
// 需手动处理内容
@SysLog(operationType = OperationType.INSERT, operation = "创建账号：${ctx.staffName}") // 
public void create(@RequestBody CreateUserCommand command) {
    ...
	LogContextHolder.put("staffName", "test-staff"); // 此方式传入参数，固定使用$ctx调用
    ... 
}
```



#### 报表模块集成

> 简介：Mysql、elasticsearch查询功能的统一封装，支持查询服务连接的mysql数据库、elasticsearch的查询，对于定义的模板SQL，支持查询全部、分页查询、导出Excel等操作，同时支持接入OnePark架构下的接口权限、数据权限

##### 1）基础依赖

```xml
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-web</artifactId>
</dependency>
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-data</artifactId>
</dependency>
```

##### 2）配置

```yaml
onepark:
  data:
	template-sql:
      location: templateSql/*  // SQL文件存储位置
      remote-load: false  // 是否加载nacos中配置SQL
      show-sql: true  // 是否打印执行SQL

# 规范配置
# applicatin-dev.yml
onepark:
  data:
    template-sql:
      location: templateSql/*
      remote-load: false
      show-sql: true
      
# applicatin-prod.yml
onepark:
  data:
    template-sql:
      location: templateSql/*
      remote-load: true
      show-sql: false
```

##### 3）入门

* 在template-sql.location配置文件夹下，新建文件：bmpcar.sql，样例如下：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<template namespace="bmpcar">

	<sql id="query">
		select * from car_info
	</sql>

</template>
```

* 启动项目
* PS：如remote-load=true，则sql会上报至nacos，DataId={spring.application.name}_xxx.xml，可通过nacos上修改，实时更新查询SQL

* 通过接口测试：

```json
POST /template/list

body:
{
	"id" : "bmpcar/query", // 查询ID，格式：{namespace}/{id}
    "params": {} // SQL参数
}
```

##### 4）Template SQL的使用

> Template SQL时使用Velocity模板引擎进行编写的SQL
> Velocity语法介绍：https://www.jianshu.com/p/d458d7b8d759

###### 常用语法

```java
// 使用预编译参数
select * from some_table where name = :name

// 参数不为false的情况，拼接进入SQL
select * from some_table where 1=1
$th.nvl($name, " and name=:name") // 预编译参数方式
#if($th.isNotNull($age)) and age = $age #end // velocity方式

// 遍历条件
#set($b=false)
select 
#foreach($item in $columns) 
    #if($b) ,${item} #else ${item} #end
    #set($b=true)
#end
from some_table
```

###### $th的使用（TableHelper）

> Velocity可调用java实例方法，$th为内置工具类，具体操作可查看源码

```java
// 源码位置：com.bjxczy.core.data.template.SqlParser.TableHelper
$th.nvl // 判断参数为空则返回空，不为空则返回第二个参数值
$th.isBlank // 参数是否为空
$th.isNotBlank // 参数不为空
$th.isNull // 判断参数是否为null
$th.isNotNull // 判断参数是否不为null
$th._parameter // 获取parameter定义的值
```

###### 数据权限接入

> 请先查看“统一数据权限的实现”章节，获取TenantInfo会存在于VelocityContext中，可通过$tenant使用

```sql
select * from some_table where tenant_id in $tenant.getInCondition()
```

##### 5）编程式查询

TemplateSqlService

```java
public interface TemplateSqlService {

	/**
	 * SQL查询
	 * @param key 查询KEY，格式：{namespace}/{id}
	 * @param params 查询参数
	 * @return
	 */
	List<Map<String, Object>> list(String key, Map<String, Object> params);

	/**
	 * SQL分页查询
	 * @param key 查询KEY，格式：{namespace}/{id}
	 * @param pageNum 页数（elasticsearch查询不支持）
	 * @param pageSize 每页条数
	 * @param params 查询参数
	 * @return
	 */
	PageWrapper<Map<String, Object>> page(String key, Integer pageNum, Integer pageSize, Map<String, Object> params);
	
	/**
	 * SQL导出查询
	 * @param key 查询KEY，格式：{namespace}/{id}
	 * @param fetchSize 导出条数，为null则全部导出
	 * @param params 查询参数
	 * @return
	 */
	ExportWrapper export(String key, Integer fetchSize, Map<String, Object> params);
	
	List<ApiResourceInformation> listApis();
	
	Boolean isResource(String key, String path);

}
```

使用

```java
@SpringBootTest
public Class SomeService {
    
    @Autowired
	private TemplateSqlService templateSqlService; // 引入onepark-core-data，将自动注册
    
    @Test
    public void queryTest() {
        // 基本查询
        Map<String, Object> params = new HashMap<>();
        params.put("plate_number", "京A12345")
        List<Map<String, Object>> list_1 = templateSqlService.list("bmpcar/query", params);
        // 分页查询
        Integer pageNum = 1, pageSize = 10;
        PageWrapper<Map<String, Object>> list_1 = templateSqlService.page("bmpcar/query", pageNum, pageSize, params);
    }
    
} 
```

##### 6）统一查询接口

> 当同时引入onepark-core-web、onepark-core-data时，将自动注册TemplateSqlController，源码如下：

```java
@ApiResourceController
@RequestMapping("/template")
public class TemplateSqlController extends AbstractTemplateController {
	
	@Autowired
	private TemplateSqlService service;
	
	@PostMapping(value = "/list", name = "[通用]Template SQL查询")
	public List<Map<String, Object>> list(@RequestBody QueryDTO dto) {
		checkPermission(dto.getId(), null, this);
		return service.list(dto.getId(), dto.getParams());
	}
	
	@PostMapping(value = "/page", name = "[通用]Template SQL分页")
	public PageWrapper<Map<String, Object>> page(@RequestBody QueryPageDTO dto) {
		checkPermission(dto.getId(), null, this);
		return service.page(dto.getId(), dto.getPageNum(), dto.getPageSize(), dto.getParams());
	}
	
	@PostMapping(value = "/export", name = "[通用]Template SQL导出Excel")
	public void export(@RequestBody QueryPageDTO dto, HttpServletResponse response) {
		checkPermission(dto.getId(), null, this);
		ExportWrapper wrapper = service.export(dto.getId(), dto.getPageSize(), dto.getParams());
		String sheetName = wrapper.getName() + "_" + System.currentTimeMillis();
		ExcelUtil.exportXlsx(response, sheetName, wrapper.getHeadMap(), wrapper.getData());
	}

	@Override
	public TemplateSqlService getService() {
		return service;
	}

}
```

##### 7）统一导出接口

> 使用统一导出功能需定义export-map，定义后可调用/template/export

```xml
<?xml version="1.0" encoding="UTF-8"?>
<template namespace="bmpcar">

	<sql id="query" export-map="carinfo">
		select * from car_info
	</sql>
    
    <parameterMap id="carinfo">
        <column name="id">ID</column>
        <column name="name">姓名</column>
    </parameterMap>

</template>
```

##### 8）接口权限接入

> 由于园区接口需接入网关接口权限，所以报表系统也可提供个性化URL进行查询，并接入接口权限进行权限配置

name定义：上报API名称

apis定义：支持一个或多个访问方式，多个通过“|”分割，取值如下：

* list=支持查询全部数据，访问路径：/template/res/{namespace}/{id}/list
* page=支持分页查询，访问路径：/template/res/{namespace}/{id}/page
* export=支持导出，需配置export-map，访问路径：/template/res/{namespace}/{id}/export
* iinner=内部，仅支持内部TemplateSqlService调用

export-map：导出字段定义

```xml
<?xml version="1.0" encoding="UTF-8"?>
<template namespace="bmpcar">

    <!-- 支持全部数据查询 -->
	<sql id="query" name="车辆查询" apis="list">
		...
	</sql>
    
    <!-- 支持多种方式访问 -->
    <sql id="query" name="车辆查询" apis="list|page">
		...
	</sql>
    
    <!-- 内部SQL，外部无法调用 -->
    <sql id="query" name="车辆查询" apis="inner">
		...
	</sql>
    
    <!-- 支持导出访问 -->
    <sql id="query" name="车辆查询" apis="export" export-map="carinfo">
		...
	</sql>
    
    <parameterMap id="carinfo">
		...
    </parameterMap>

</template>
```

##### 9）elasticsearch查询

> 支持elasticsearch需增加相关配置，可参考以下章节：

[9，Elasticsearch的开发](#9，Elasticsearch的开发)

Template SQL配置，使用同mysql查询，但不支持预编译参数

Elasticsearch SQL语法参考：https://www.elastic.co/guide/en/elasticsearch/reference/current/sql-getting-started.html

```xml
<?xml version="1.0" encoding="UTF-8"?>
<template namespace="syslog">

	<elasticsearch id="query">
	<![CDATA[
		select * from pcs_sys_log_record where 1=1 
		#if($th.isNotBlank($unifyParams))
		  and ( employeeNo like '%${unifyParams}%'
		  or staffName like '%${unifyParams}%'
		  or mobile like '%${unifyParams}%' )
		#end
		#if($th.isNotBlank($operation)) and operation like '%${operation}%' #end
		#if($th.isNotNull($operationType)) and operationType = '${operationType}' #end
		#if($th.isNotNull($deptIds)) and deptId in ${deptIds} #end
		#if($th.isNotBlank($startTime) and $th.isNotBlank($endTime))
		  and saveTime > $th.toTimestamp($startTime, "yyyy-MM-dd HH:mm")
		  and saveTime < $th.toTimestamp($endTime, "yyyy-MM-dd HH:mm")
		#end
		order by saveTime desc
	]]>
	</elasticsearch>

</template>
```



## 三、微服务子模块搭建

### 1，单模块服务搭建

#### 1）创建Maven项目

pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

    <!-- 1.继承统一父项目 -->
	<parent>
		<groupId>com.bjxczy.onepark</groupId>
		<artifactId>onepark-dependencies-parent</artifactId>
		<version>0.0.1-SNAPSHOT</version>
	</parent>

	<artifactId>onepark-organization</artifactId>
	<description>Demo project for Spring Boot</description>

	<properties>
		<java.version>1.8</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<m2e.apt.activation>jdt_apt</m2e.apt.activation>
	</properties>

	<dependencies>
		...
	</dependencies>

    <!-- 2.配置打包插件 -->
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.1</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<encoding>UTF-8</encoding>
					<annotationProcessorPaths>
						<path>
							<groupId>org.mapstruct</groupId>
							<artifactId>mapstruct-processor</artifactId>
							<version>${mapstruct.version}</version>
						</path>
						<path>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok</artifactId>
							<version>${org.projectlombok.version}</version>
						</path>
						<path>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok-mapstruct-binding</artifactId>
							<version>${lombok-mapstruct-binding.version}</version>
						</path>
					</annotationProcessorPaths>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<version>2.3.7.RELEASE</version>
				<configuration>
					<mainClass>com.bjxczy.onepark.Application</mainClass>
				</configuration>
				<executions>
					<execution>
						<id>repackage</id>
						<goals>
							<goal>repackage</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

</project>

```

#### 2）创建包、启动类

```Java
// 约定根包结构
com.bjxczy.onepark
    
// 创建启动类
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
```

#### 3）创建配置文件

bootstrap.yml

```yaml
server:
  port: 8080
spring:
  profiles:
    active: dev
  application:
    name: # 应用名称
  servlet:
    multipart:
      enabled: true
      max-request-size: 100MB
      max-file-size: 50MB
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    time-zone: GMT+8
  cloud:
    nacos:
      # server-addr: 192.165.1.63:8848
      server-addr: 192.165.1.190:8848
      discovery:
        register-enabled: true #是否注册到nacos
      config:
        file-extension: yml

```

application.yml

```yaml
spring:
  datasource:
    url: jdbc:mysql://${env.datasource.host}/organization?allowPublicKeyRetrieval=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=Asia/Shanghai
    username: root
    password: onepark!123qwe
    driver-class-name: com.mysql.cj.jdbc.Driver
  cache:
    type: redis
    redis:
      key-prefix: "${spring.application.name}:"
  redis:
    host: 192.165.1.190
    port: 6379
    password: andnext_!QAZxsw2
    timeout: 5000
  rabbitmq:
    host: 192.165.1.190
    port: 31673
    username: admin 
    password: mdsoss 
        
mybatis-plus:
  mapper-locations: classpath*:mapper/*Mapper.xml
  # 查看sql日志
  configuration:
    call-setters-on-nulls: true
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
    
```

application-dev.yml

```yml
env:
  datasource:
    host: 192.165.1.190:3306
```

application-prod.yml

```yml
env:
  datasource:
    host: 192.165.1.190:3306
```

4）添加启动类

```java
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}

```



### 2，多模块服务搭建

> 出于业务考虑，各个微服务需更细化进行细化拆分，所以为根据业务划分，统一创建为微服务集合，根据业务需求也可封装业务模块公共服务

#### 1）创建Maven父项目

pom.xml

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.bjxczy.onepark</groupId>
		<artifactId>onepark-dependencies-parent</artifactId>
		<version>0.0.1-SNAPSHOT</version>
	</parent>

	<artifactId>onepark-template-parent</artifactId>
	<packaging>pom</packaging>

	<modules>
        <!-- 业务模块公有服务 -->
		<module>onepark-template-common</module>
        <!-- 业务模块启动类（暂未实现） -->
		<module>onepark-template-starter</module>
         <!-- 业务实现 -->
		<module>onepark-template-custom</module>
	</modules>

	<dependencies>
        <!-- 公用组件 -->
		<dependency>
			<groupId>com.bjxczy.onepark</groupId>
			<artifactId>onepark-core-common</artifactId>
		</dependency>
		<dependency>
			<groupId>com.bjxczy.onepark</groupId>
			<artifactId>onepark-core-web</artifactId>
		</dependency>
		<!-- 服务依赖定义 -->
        ...
	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>com.bjxczy.onepark</groupId>
				<artifactId>onepark-template-common</artifactId>
				<version>0.0.1-SNAPSHOT</version>
			</dependency>
		</dependencies>
	</dependencyManagement>

</project>
```

#### 2）创建子模块

> 参考单模块服务创建

## 四、业务开发

1，OnePark默认包规范

### 1，实现DDD领域事件驱动设计

#### 1）程序架构

![image-20221204165435456](doc/images/image-20221204165433505.png)



#### 2）包结构

![image-20221204165013028](doc/images/image-20221204165013028.png)

> 应用程序默认分为以下4大结构，子包下自成体系，自由定义

层间调用

![img](doc/images/4e22d630818f9bbd2779e210c47bf655.png)

应用层（application）

> 此层负责转发命令、跨领域交互、跨微服务交互操作，逻辑轻量的一层

```yaml
command: 应用命令
listener: 领域事件监听
service: 应用层服务接口定义
service.impl: 应用层服务实现
```

领域层（domain）

```yaml
model:
  {领域模型}:
    aggregate: 聚合
    entity: 实体
    event: 领域事件
  	repository: 仓储层接口
  	service: 领域服务
  	valueobject: 值对象
```

接口层（interfaces）

> 此层按业务需求分配，尽量保证简洁架构，避免包下存在大量类，以下供参考：

```yaml
controller: 页面接口
rpc.feign: 提供对外feign接口
rpc.rabbitmq: rabbitmq订阅
dto: 数据传输对象
service: 接口层服务接口定义
vo: View Object页面对象
```

基础设施层（infrastructure）

> 基础设施包括：框架、数据库、中间件提供功能，因以上内容可能根据业务情况进行调整，所以定义基础设施层进行解耦

```yaml
assembler: 数据转换，实现领域模块转换为PO
mapper: Mybatis Mapper
po: 持久化对象，与数据表字段一对一
repository.impl: 领域层仓储实现
service: 基础服务接口
service.impl: 基础服务实现
publish.rabbitmq: rabbitmq发布程序
```

#### 3）定义聚合

值对象定义

```java
// 定义用户名对象
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class Username extends ValueObject<Username> {
    
    @NotBlank(message = "账号不能为空！")
	@Length(min = 1, max = 20, message = "账号长度限制1-20字")
	private String value;
	
	public BuildingName(String value) {
		super();
		this.value = value;
	}

	@Override
	protected boolean equalsTo(Username other) {
		return getValue().equals(other.getValue());
	}
    
}

// 定义密码对象
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class Password extends ValueObject<Password> {
    
    @NotBlank(message = "密码不能为空！")
	@Length(min = 1, max = 20, message = "密码长度限制1-20字")
	private String value;
	
	public BuildingName(String value) {
		// 具体业务逻辑，对象值的业务需求，在值对象内部实现
        if (!passwdCheck()) {
            throw new ResultException("密码不符合要求！");
        }
        // TODO
		this.value = value;
	}

	@Override
	protected boolean equalsTo(Username other) {
		return getValue().equals(other.getValue());
	}
    
}
```

聚合根定义

```java
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class User extends AbstractAggregateRoot<User> {
    
    @Id
    private String id;
    @Valid
    private Username username;
    @Valid
    private Password password;
    
    public User(Username username, Password password) {
        this.username = username;
        this.password = password;
        this.validate(); // 参数校验，此操作将触发值对象内容注解校验
    }
    
}
```



#### 4）定义实体

```java
// 定义实体
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserImage extends Entity {
    
    private String id;
    
    private String imgUrl;
    
    public UserImage(String imgUrl) {
        // ID 生成方法待优化
        this.id = UUID.randomUUID().toString();
		this.imgUrl = imgUrl;
    }
    
}

// 增加字段
public class User extends AbstractAggregateRoot<User> {
    ...
    
    private String activeImgId;
    
    private List<UserImage> userImages;
    
    public User(Username username, Password password) {
        ...
        this.userImages = new ArrayList<>();
        ...
    }
    
    ...
        
    public void addUserImage(UserImage img) {
        // 用户上传头像操作，并设置为默认头像
        this.userImages.add(img);
        this.activeImgId = img.getId();
    }
    
    ...
}
```



#### 5）实现Repository

```java
// 定义仓储层接口，领域层只定义接口，不进行实现
public interface UserRepository extends AbstractRepository<User> {
    
}

// 定义PO
@Data
@TableName("sys_user")
public UserPO extends UserOperator {
    
    @TableId(type = IdType.UUID)
    private String id;
    
    private String username;
    
    private String password;
    
}
// 定义Mapper
@Mappper
public interface UserMapper extends BaseMapper<UserPO> {
    
}

// 基础设施层实现仓储接口，目前仅封装Mybatis Plus实现Mysql仓储层MyBatisPlusRepositoryImpl，具体实现见源码
// TODO redis仓储实现、mongodb仓储实现、elasticsearch实现
public class UserRepositoryImpl extends MyBatisPlusRepositoryImpl<UserMapper, UserPO, User> implements UserRepository {
    
    @Override
    public UserPO toPO(User model) {
        // 默认实现为BeanUtils实现，字段类型相同可使用默认实现
        // 需自行实现领域模型转换PO
    }
    
    @Override
    public User toModel(UserPO po) {
        // 默认实现为BeanUtils实现，字段类型相同可使用默认实现
        // 需自行实现PO转换领域模型
    }
    
}
// 以上实现可支撑UserRepository中基本信息存储，UserImages实体保存请查看：8）复杂领域模型

```



#### 6）应用层服务

```java
// 定义命令
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserCommand {
    
    private String username;
    
    private String password;
    
}

// 定义应用接口
public interface UserApplicationService {
    
    void createUser(CreateUserCommand command);
    
}

// 定义接口实现
@Service
public class UserApplicationServiceImpl implements UserApplicationService {
    
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public coid createUser(CreateUserCommand command) {
        User user = new User(new Username(command.getName()), new Password(command.getPassword));
        userRepository.saveModel(user);
    }
    
}
```



#### 7）测试领域模型

>外部接口与领域模型交互的唯一方式，就是通过command，所以良好的单元测试可反应领域模型的完善程度

```java
@SpringbootTest
public class UserApplicationServiceTest {
    
    @Autowird
    private UserApplicationService userApplicationService;
    
    @Test
    public void createUserTest() {
        userApplicationService.createUser(new CreateUserCommand("user", "123456"));
    }
    
    @Test
    public void createUserTest() {
        userApplicationService.createUser(new CreateUserCommand("", ""));
    }
    
    @Test
    public void createUserTest() {
        userApplicationService.createUser(new CreateUserCommand(null, ""));
    }
    
}
```

#### 8）复杂领域模型

```Java
// 定义UserImagePO
@Data
@TableName("sys_user_image")
public class UserImagePO extends LogicDelete {
    
    @TableId(type = IdType.INPUT)
    private String id;
    
    private String userId;
    
    private String imgUrl;
    
}
// 保存数据库使用Mybatis Plus实现，实现忽略
public interface UserImageService extends IService<UserImagePO> {
    ...
}

// 基于上文中创建UserImage值对象，修改UserRepositoryImpl
public class UserRepositoryImpl ... {
    
    @Autowird
    private UserImageService userImageService;
    
    @Override
    public Boolean saveModel(User model) {
        super.saveModel(model);
		List<UserImagePO> poList = toUserImagePOList(model.getUserImage());
        poList.forEach(e -> e.setUserId(model.getId()));
        userImageService.saveOrUpdateBatch(poList);
    }
    
    
    @Override
    public User toModel(UserPO po) {
        // 默认实现为BeanUtils实现，字段类型相同可使用默认实现
        // 需自行实现PO转换领域模型
        User model = toModel(po);
        List<UserImage> poList = userImageService.lambda().eq(UserImagePO::getUserId, po.getId()).list();
        model.setUserImages(toUserImage(poList));
        return model;
    }
    ...
}
```



#### 9）发布领域事件

```java
// 定义event类
@Getter
@ToString
public class CreateUserEvent extends ApplicationEvent {
    
    private User user;
    
    public CreateUserEvent(Object source) {
        super(source);
        this.user = (User) source;
    }
    
}

// 发布领域事件，修改User类
...
public class User ... {
    ...
    public User(Username username, Password password) {
        this.username = username;
        this.password = password;
        this.validate(); // 参数校验，此操作将触发值对象内容注解校验
        andEvent(new CreateUserEvent(this)); // 发布创建用户事件，该事件会在saveModel方法执行完成后发布
    }
    ...
}
```



#### 10）领域事件处理

```java
// 定义领域事件处理
@Components
@@Slf4j
public class UserEventListener {
    
    @EventListener
    public void createUserEventHandler(CreateUserEvent event) {
        log.info("[UserEvent] CreateUserEvent: {}", event);
        // TODO 事件处理
    }
    
}
```



### 2，CQRS查询

> CQRS查询旨在实现跨服务复杂查询需求，业务如果无相关需求，建议使用API组合模式

#### 1）事件处理

1. 需求：User查询需增加手机号、邮箱信息，两个字段数据来源于员工服务，并发布领域事件：用户信息变更

```java
// 领域模型User增加员工ID
// 由于手机号、邮箱字段不属于账号领域，账号领域无权修改，仅需查看，故领域模型无需增加该字段
public class User ... {
    ...
    private Integer staffId; // 员工ID
    ...
}
public class UserPO ... {
    ...
    private Integer staffId; // 员工ID
    ...
}

// 定义UserStaffPO，用于关联账号与员工信息
@Data
public class UserStaffPO extends LogicDelete {
    
    private Integer id;
    
    private Integer staffId; // 员工ID
    
    private String userId; // 账号ID
    
    private String mobile;
    
    private String email;
    
}

// 省略在基础设施层实现UserStaffMapper、UserStaffService过程，基于MybatisPlus实现
// 定义feign接口，用于调用员工服务
@FeignClient("organization")
public interface OrganizationFeignClient {
    ...
    @GetMapping("/staff/{id}")
    StaffInfo findStaffById(@PathVariable("id") Integer id)
    ...
}

// 保存账号员工信息逻辑
public class UserRepositoryImpl ... {
    ...
    @Autowired
    private OrganizationFeignClient organizationFeignClient;
    @Autowired
    private UserStaffService userStaffService;
    ...
    @Override
    public Boolean saveModel(User model) {
        // 保存用户头像
		List<UserImagePO> poList = toUserImagePOList(model.getUserImage());
        userImageService.saveOrUpdateBatch(poList);
        // 保存模型
        super.saveModel(model);
        // 查询员工信息，此步骤建议放在UserStaffService下，此处只做演示
        StaffInfo staff = organizationFeignClient.findById(model.getStaffId());
        // 实现略
        userStaffService.saveUserStaff(model.getId(), staff);
    }
    ...
}

// 实现rabbitmq listener
@Components
public class StaffRabbitListener {
    
    @Autowired
    private UserStaffService userStaffService;
    
    @RabbitHandler
    @RabbitListener(queuesToDeclare = @Queue("update_staff_event"))
    public void process(String message) {
        System.out.println("收到通知：" + message);
        StaffInfo staffInfo = toStaffInfo(message);
        // 调用UserStaffService根据staffId更新表UserStaff
        userStaffService.updateUserInfo(staffInfo);
    }
    
}


```

#### 2）实现查询

```sql
# 实现查询
# 定义VO，字段：username, mobile, email, createTime, updateTime, operator
# 查询SQL
select 
u.username, s.mobile, s.email, u.create_time, u.update_time, u.operator
from sys_user u
left join sys_user_staff s on u.id = s.userId
```



### 3，MVC应用分层开发

> 为应对物联网标准代码复用需求，并实现解耦、事件驱动需求，定义以下规范

#### 1）包结构

```java
common
--config 
--feign.client // feign引用外部服务
--constant // 常量定义
--util // 工具类
...
{模块名称} // 根据业务拆分
--interfaces
----controller // 提供页面接口
----task  // 定时任务
----feign  // 提供feign接口
----listener  // spring事件订阅、rabbitmq事件订阅
--pojo.dto
--pojo.vo
--entity // 实体
--service // 服务层接口
--service.impl  // 服务层实现
--mapper // mybatis mapper
--event  // 事件载体
--manager // 对第三方平台封装的层，预处理返回结果及转化异常信息。对 Service 层通用能力的下沉，如缓存方案、中间件通用处理。
```



#### 2）事件发布

> service中显式的发布事件，一般保存数据之后执行

定义事件载体

```java
public class AddUserEvent extends ApplicationEvent {
    
    private User user;
    
    public AddUserEvent(Object source, User user) {
        this.user = user;
    }
    
}
```

发布事件

```java
package com.bjxczy.onepark.demo.menu.service.impl;

public class TestServiceImpl implements TestService {
    
    @Autowired
    private ApplicationEventPublisher publisher;
    
    @Override
	public void saveUser(User user) {
        // 处理一些业务逻辑，处理成功后，发布新增员工事件
        publisher.publishEvent(new AddUserEvent(this, user));
    }
    
}
```



#### 3）事件订阅

```java
package com.bjxczy.onepark.demo.menu.interfaces.listener;
    
@Component
public class UserEventListener {

	@Autowired
	private DeviceAuthLinkService deviceAuthLinkService;

	@EventListener
	public void removeStaffCertHandler(AddUserEvent event) {
		// 调用service层处理
	}

}

```



### 4，Feign开发使用规范

#### 1）定义feign接口

> feign接口统一定义在基础依赖onepark-dependencies-parent项目中
>
> git：http://192.165.1.124:10000/chnEnergy/xconepark/onepark-dependencies-parent

```java
// feign统一放在包com.bjxczy.core.web.feign.*
package com.bjxczy.core.web.feign.user;
...
public interface UserFeignClient {

	@GetMapping("/rpc/user/byId/{id}")
	UserInformation findById(@PathVariable("id") Integer id);
	
	@GetMapping("/rpc/user/byUsername/{username}")
	UserInformation findByUsename(@PathVariable("username") String username);
	
}
// 定义所需POJO，统一放在包com.bjxczy.onepark.common.model.*
package com.bjxczy.onepark.common.model.user;
...
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInformation {
	
	private Integer id;
	
	private String username;
	
	private String staffName;
	
	private String password;
	
	private String mobile;
	
	private String employeeNo;
	
	private Boolean enable;
	
	private Set<String> roles;
	
}
```

#### 2）实现feign接口

引入基础依赖

```xml
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-feign</artifactId>
</dependency>
```

实先定义接口

```java
// interfaces层定义feign实现
@Controller // 暂不能使用@RestController
@ResponseBody
public class UserFeignController implements UserFeignClient {
	
	@Autowired
	private UserQueryService userQueryService;

    // 必须添加@PathVariable("id")，重写后导致注解失效
	@Override
	public UserInformation findById(@PathVariable("id") Integer id) {
		return userQueryService.findInfoById(id);
	}

	@Override
	public UserInformation findByUsename(@PathVariable("username") String username) {
		return userQueryService.findInfoByUsername(username);
	}

}
// 本地调试也可通过UserFeignClient中定义地址进行调用
```

#### 3）使用feign接口

引入基础依赖

```xml
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-feign</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
```

启动入口增加注解@EnableFeignClients

```java
...
@EnableFeignClients // 需增加此注解
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
```

定义feignclient接口

```java
// infrastructure层定义feignclient
@FeignClient("user-server")
public interface UserServiceFeignClient extends UserFeignClient {
	
    // ..如有需要可拓展
    
}
```

### 5，获取登录用户信息

> OnePark平台token为JWT形式，网关服务将统一对JWT进行校验及解析用户信息，所以获取登录用户信息需通过网关访问应用接口

#### 1）定义基础依赖

```java
// 定义拦截器
@Configuration
public class WebMvcConfig extends DefaultWebMvcConfig {
	
}
// 如需自定义拦截器，请重写父类方法，但需要同时执行父类方法
@Configuration
public class WebMvcConfig extends DefaultWebMvcConfig {
    @Override
	public void addInterceptors(InterceptorRegistry registry) {
		// TODO 可添加自定义拦截
		super.addInterceptors(registry); // 此行需要保留
	}
}
// 定义user-server服务feignClient，UserFeignClient提供方法请查看源码
// @LoginUser(isFull = true)，依赖此组件
@FeignClient("user-server")
public interface BmpUserFeignClient extends UserFeignClient {

}
```

#### 2）LoginUserContextHolder方式

> 通过spring mvc方式请求，整个生命周期流程内均可使用此方式获取登录用户信息
>
> 获取UserInformation中，仅填充id、username、staffName字段
>
> MybatisPlus自动填充操作人operator使用此方式实现

```java
// 获取UserInformation
UserInformation user = LoginUserContextHolder.get();
// 获取某个字段，此方式获取value前，会判断userInformation == null，如果为null则返回null，详见源码
String username = LoginUserContextHolder.getValue(u -> u.getUsername());
```

#### 3）@LoginUser方式

> 仅可使用在@Controller中

```java
@TestController
public class TestController {
    ...
    // 获取UserInformation中，仅填充id、username、staffName字段
    @GetMapping("/user/current")
    public UserInformation getCurrentUser(@LoginUser UserInformation user) {
        return user;
    }
    ...
    // 获取UserInfomation中，将全字段填充
    @GetMapping("/user/current")
    public UserInformation getCurrentUser(@LoginUser(isFull = true) UserInformation user) {
        return user;
    }
    
}
```
#### 4）@LoginUser本地调试
> 登录用户ID，可通过URL传递，仅spring.profiles.active=dev时生效
```java
GET xxxx/xxxx?userId=10

// 将填充userId对应账号信息
@LoginUser(isFUll = true) UserInfomation user
```

### 6，Rabbitmq开发使用规范

> 基础依赖

```xml
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-rabbitmq</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```

#### 1）事件发布/订阅

> git：http://192.165.1.124:10000/chnEnergy/xconepark/onepark-dependencies-parent
>
> 基础依赖：onepark-core-rabbitmq

约定每个服务定义一个交换机，服务内每个事件定义routing key

* 声明rabbitmq事件接口

```java
import org.springframework.amqp.core.Message;

...
/**
 * 此处声明服务内部事件，默认实现应为空方法
 */
public abstract class OrganizationBaseListener extends BaseRabbitListener {
	
	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "orgaization_default_exchange"; // 定义交换机名称
	/**
	 * 更新用户事件
	 */
	public static final String UPDATE_STAFF_EVENT = "update_staff_event"; // 定义route Key
	
	public void updateStaffEventHandler(StaffInformation staffInfomation, Channel channel, Message message) {}

}
```

* 发布事件

```java
@Component
public class StaffEventPublishImpl implements StaffEventPublish {

	@Autowired
	private StaffReposiotryImpl staffReposiotryImpl;
	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Override
	public void updateStaffEventPublish(UpdateStaffEvent event) {
		StaffInformation info = staffReposiotryImpl.findInfoById(event.getStaff().getId());
		rabbitTemplate.convertAndSend(
            	OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME, // 交换机名称
				OrganizationBaseListener.UPDATE_STAFF_EVENT, // routing Key
            	info);
	}

}
```

* 订阅事件


实现事件监听

```java
@Component
@Slf4j
public class OrganizationEventListener extends OrganizationBaseListener {
	
	@Autowired
	private UserStaffService userStaffService;
	
	@RabbitListener(bindings = {
            @QueueBinding(
                	// 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "bmpuser_update_staff_queue", durable = "true"),
                	// routing key
                    key = UPDATE_STAFF_EVENT, 
                	// 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) 
            )
    })
    @Override  // 重写所需订阅事件，并绑定队列
    public void updateStaffEventHandler(StaffInformation info, Channel channel, Message message) {
		log.info("[UPDATE_STAFF_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
       	this.confirm(() -> userStaffService.updateByEvent(info), channel, message);
	}

}
```

#### 2）异步调用

* 客户端

声明Configuration，继承AbstractRabbitAsyncConfigure

```java
@Configuration
public class MyRabbitAsyncConfig extends AbstractRabbitAsyncConfigure {
	
	@Bean
	public MessageConverter messageConverter() {
		return new SimpleMessageConverter();
	}

}
```

定义异步消息处理器

```java
@RabbitAsyncComponent(
		value = "myHandler", // 处理器KEY，同时作为Bean名称
		exchange = MyRabbitAsyncHandler.DEFAULT_EXCAHNGE_NAME, // 目的交换机（公共组件中定义）
		routingKey = MyRabbitAsyncHandler.ADD_EVENT  // 目的路由（公共组件中定义）
)
public class MyRabbitAsyncHandler extends RabbitAsyncHandler {

	public static final String DEFAULT_EXCAHNGE_NAME = "bmptemplate_default_exchange";
	public static final String ADD_EVENT = "bmptemplate_add_event";

	@Override
	public void receive(Object id, Message data) {
		TestReply obj = (TestReply) this.messageConverter.fromMessage(data);
		// 处理回复消息
	}
	
}
```

发送异步消息

```Java
@SpringBootTest
public class AsyncTest {
    
    @Autowired
	private MyRabbitAsyncHandler asyncHandler;
    
    @Test
	public void sendAsync() {
        String id = UUID.fastUUID().toString(); // 实际开发可使用具体的业务表ID，消息回复可返回该ID
		asyncHandler.sendMessage(id, "hello World!");
	}
    
}
```

* 服务端

声明依赖Bean

```java
@Configuration
public class SomeConfig {
    
    @Bean
	public MessageConverter messageConverter() {
		return new SimpleMessageConverter(); // 注意不要重复声明，如同时需要异步及处理时
	}
	
	@Bean
	public AsyncReplyHandler replyAsyncHandler() {
		return new AsyncReplyHandler(messageConverter());
	}
    
}
```

订阅并回复消息

```java
@Service
public class SomeService {
    
    @Autowired
	private AsyncReplyHandler replyHandler;
    
    @RabbitListener(bindings = {
        @QueueBinding(
                value = @Queue(name = "bmptest_add_event_queue", durable = "true"),  // 持久化队列
                key = ADD_EVENT,
                exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) // 绑定的交换机
        )
    })
    public void vssEventHandler(String msg, Channel channel, Message message) {
        System.out.println(msg);
        replyHandler.reply(message, new TestReply(msg));
    }
    
}
```

#### 3）死信队列

// TODO

### 7，统一设备管理接入

> OnePark子模块设备需统一接入设备管理，由设备管理关联设备局址、位置信息，设备关联位置信息后可通过事件通知子系统

#### 1）引入依赖

添加基础依赖，添加此依赖将同时引入onepark-core-common、onepark-core-feign、onepark-core-rabitmq、spring-cloud-starter-openfeign、spring-boot-starter-amqp

```xml
<dependency>
    <groupId>com.bjxczy.onepark</groupId>
    <artifactId>onepark-core-device</artifactId>
</dependency>
```

#### 2）实现设备上报接口

* 子模块存在多个设备类型，可定义多个DeviceCategoryHandler

```java
@DeviceCategoryDefinition("Camera") // value = 设备编码
public class CameraSyncHandlerImpl extends DeviceCategoryHandler {

	@Override
	protected List<DeviceInformation> thirdAll() {
		return // 全量设备列表
	}

	@Override
	protected Map<String, DeviceStatus> thirdStatus() {
		return // Map<设备ID, 设备状态>，全量设备状态数据
	}

    /**
	 * 获取子模块指定设备状态
	 * @param idSet 三方ID
	 * @return
	 */
	@Override
	protected Map<String, DeviceStatus> thirdStatus(Set<String> idSet) {
		return // Map<设备ID, 设备状态>，指定ID设备状态数据
	}

}
```

#### 3）统一空间树查询接口

**基本信息**

| Path   | /api/bmpdevice/common/space/tree |
| ------ | -------------------------------- |
| Method | POST                             |

**请求参数**

Header

无

Body

| 参数名称    | 类型         | 是否必须 | 备注         |
| ----------- | ------------ | -------- | ------------ |
| full        | boolean      | 否       | 是否返回设备 |
| deviceTypes | List<String> | 否       | 设备类型     |

**返回数据**

| 名称      | 类型    | 是否必须 | 备注     |
| --------- | ------- | -------- | -------- |
| code      | Integer | 是       | 返回码   |
| message   | String  | 是       | 返回描述 |
| data      | Object  | 是       | 返回数据 |
| + id      | Object  | 是       | id |
| + label      | String  | 是       | 名称 |
| + type      | String  | 是       | 类型：AREA-区域；BUILDING-楼宇；SPACE-空间；DEVICE-设备|
| + children      | Object[]  | 是       | 子数据 |
| + count      | Integer  | 否       | 设备数量 |
| + extra      | Integer  | 否       | 额外字段 |
| exception | String  | 是       | 异常信息 |
| success   | boolean | 是       | 是否成功 |

**返回示例**

```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [{
			"id": -1,
			"label": "全部",
			"type": "AREA",
			"count": 5,
			"children": [{
				"id": 78,
				"label": "昌平中心东区",
				"type": "AREA",
				"count": 5,
				"children": [{
						"id": "1629009850408140802",
						"label": "104楼",
						"type": "BUILDING",
						"count": 5,
						"children": [{
							"id": "f7cba6584801461e951c1ce8746f9035",
							"label": "4层",
							"type": "SPACE",
							"count": 5,
							"children": [{
								"id": 54038,
								"label": "333",
								"type": "DEVICE",
								"count": null,
								"children": null,
								"extra": {
									"delFlag": 0,
									"createTime": "2023-06-20 14:30:10",
									"updateTime": "2023-07-11 16:16:37",
									"operator": "李军",
									"id": 54038,
									"groupId": "24",
									"deviceType": "faceMachine",
									"category": "faceMachine",
									"deviceName": "333",
									"deviceCode": "333",
									"onlineStatus": 2,
									"devicePosition": null,
									"protocol": null,
									"longitude": null,
									"latitude": null,
									"deviceRemark": null,
									"extra": null,
									"areaId": 78,
									"areaName": null,
									"buildingId": "1629009850408140802",
									"buildingName": null,
									"spaceId": "f7cba6584801461e951c1ce8746f9035",
									"spaceName": null,
									"qrCode": "http://192.165.1.62:9881/group1/M00/00/BF/wKUBPmSRR3KANf1dAAAHo1nKYrg388.png",
									"brand": null,
									"manufacturer": null,
									"model": null
								}
							}],
							"extra": null
						}, ],
						"extra": null
					}

				],
				"extra": null
			}],
			"extra": null
		},
		{
			"id": -2,
			"label": "未绑定",
			"type": "AREA",
			"count": 0,
			"children": null,
			"extra": null
		}
	],
	"success": true
}
```

#### 

### 8，告警中心接入

> OnePark园区平台告警消息，需统一上报至告警中心

#### 1）引入依赖

添加基础依赖，添加此依赖将同时引入onepark-core-common、onepark-core-feign、onepark-core-rabitmq、spring-cloud-starter-openfeign、spring-boot-starter-amqp

```xml
<dependency>
    <groupId>com.bjxczy.onepark</groupId>
    <artifactId>onepark-core-alarm</artifactId>
</dependency>
```

#### 2）实现告警操作类

```java
@Component
public class DeviceAlarmHandler extends AlarmOperatorHandler {

    /**
     * 当告警中心页面进行处理告警完成时，会调用此方法，需同步处理状态至本地告警表
     */
	@Override
	public boolean alarmHandled(UpdateAlarmStatusCommand command) {
		boolean success = // 更新本地告警表操作
		return success;
	}

}
```

#### 3）告警上报/处理

```java
@Service
public class SomeService {
    
    @Autowird
    private DeviceAlarmHandler deviceAlarmHandler;
    
    // 告警上报
	public void pushAlarm() {
        CreateAlarmRecordCommand command = // 构造告警请求命令
        deviceAlarmHandler.pushAlarm(command);
    }
    
    // 告警处理
    public void updateAlaramStatus() {
        UpdateAlarmStatusCommand command = // 构造告警更新命令
        deviceAlarmHandler.updateAlarmStatus(command);
    }
    
}
```

### 9，Elasticsearch的开发

> 使用elasticsearch-java 8.5.1官方库进行封装，为降低使用难度，使之使用更接近于ORM框架

#### 1）引入依赖

```xml
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-data</artifactId>
</dependency>
```

#### 2）配置

application.yml

```yaml
onepark:
  data:
    elasticsearch:
      enable: true  # 是否启用
      host: 192.165.1.135 
      port: 9200
```

#### 3）ElasticsearchRepository

定义实体

```java
@Data
@AllArgsConstructor
@NoArgsConstructor
@ModelName("pcs_sys_log_record") // elasticsearch index
public class SysLogPO {

	@ModelId
	private String id; // elasticsearch ID

	private String employeeNo;

	private String staffName;

	private String mobile;
	
}
```

定义Repository

```java
@Configuration
public class ElasticsearchConfig {
	
	@Bean(name = "sysLogRepository")
	public IDataRepository<SysLogPO, String> sysLogRepository() {
		return new ElasticsearchRepository<>(SysLogPO.class);
	}

}
```

```java
// IDataRespoitory接口介绍
public interface IDataRepository<T, ID> {
	
	/**
	 * 新增、更新
	 * @param model
	 * @return
	 */
	boolean save(T model);
	
	/**
	 * 根据ID删除数据
	 * @param id
	 * @return
	 */
	boolean remove(ID id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	T findById(ID id);
	
	/**
	 * 查询全部数据
	 * @return
	 */
	List<T> list();
	
}
```

使用Repository

```java
@SpringBootTest
public class SomeServiceTest {
    
    @Autowired
	@Qualifier("sysLogRepository")
	public IDataRepository<SysLogPO, String> repository;
    
    @Test
    public void respositoryTest() {
        // 当ID为null，将使用elasticsearch自动生成ID，通过反射写入实体
        SysLogPO model = new SysLogPO(null, "E0001", "test", "13700001234");
        repository.save(model);
		String id = model.getId();
        System.out.println(id);
        // 查询
        List<SysLogPO> list = repository.list();
        System.out.println(list);
        // 删除
        repository.remove(id);
    }
    
}
```

#### 4）ElasticsearchSqlTemplate

> 类似于Spring JdbcTemplate，支持通过SQL（不完全支持）查询elasticsearch
>
> SQL语法参考：https://www.elastic.co/guide/en/elasticsearch/reference/current/sql-getting-started.html

```java
@SpringBootTest
public class SomeServiceTest {
    
    @Autowired
    private ElasticsearchSqlTemplate sqlTemplate;
    
    @Test
    public void indexTest() {
        // 新增数据
        String id = "1", index = "pcs_sys_log_record";
        SysLogPO model = new SysLogPO(id, "E0001", "test", "13700001234");
        // 自动生成ID
        IndexResponse response = sqlTemplate.index(index, model);
        // 手动设置ID
        sqlTemplate.index(index, id, model);
        // 删除
        sqlTemplate.delete(index, id);
    }
    
    @Test
    public void sqlQueryTest() {
        // 基本查询，不支持预编译参数
        String sql = "select * from pcs_sys_log_record";
        List<Map<String, Object> list_1 = sqlTemplate.queryForList(sql);
        // 翻页查询，不支持随机跳页
        Integer fetchSize = 10;
		List<Map<String, Object> list_2 = sqlTemplate.queryForList(sql, fetchSize);
        // 查询单条数据，如果查询出多条数据会抛出异常
        String sql = "select * from pcs_sys_log_record where id = 'e8be8f60-34d2-46e9-9e58-74fc57f1aed6'";
        Map<String, Object> map = sqlTemplate.queryForMap(sql);
        // 查询数据转实体
        List<SysLogPO> list_3 = sqlTemplate.queryListToEntity(sql);
    }
    
}
```

#### 5）Elasticsearch Java API Client官方库的使用

> 由于elasticsearch对SQL支持并不完整，如遇特殊需求无法实现，可使用官方API实现
>
> 官方文档：https://www.elastic.co/guide/en/elasticsearch/client/java-api-client/current/introduction.html

```java
@SpringBootTest
public class SomeServiceTest {
	
    @Autowired
	private ElasticsearchClient esClient; // 已注册为Bean，直接引用使用即可
    
    // 使用可参考ElasticsearchSqlTemplate源码
    
}
```

### 10，redisson延迟队列的使用
> 业务场景：需延时更新状态业务
> 例如：某权限1天后过期，可将业务对象添加至延时队列，并设置过期时间为1天后，消息过期后将回调业务模块

#### 1）引入依赖

```xml
<dependency>
	<groupId>com.bjxczy.onepark</groupId>
	<artifactId>onepark-core-redisson</artifactId>
</dependency>
```

#### 2）延迟队列实现

定义实现类

```java
@Component(DelayedQueueNames.WORKFLOW_USER_TASK_QUEUE) // 队列名称，必填
@Slf4j
public class UserTaskDelayedQueueImpl extends DelayedQueueImpl implements IDelayedQueue {
	
	@Override
	public void execute(RDelayedPayload payload) {
		// do something ... 延时队列回调
	}

}
```
添加内容至队列
```java
// 引入依赖
@Qualifier(DelayedQueueNames.WORKFLOW_USER_TASK_QUEUE)
private IDelayedQueue userTaskDelayedQueue;

// 写入队列，30分钟后过期
userTaskDelayedQueue.offer(new RDelayedPayload("1"), 30, TimeUnit.MINUTES);
// payload写入额外内容
RDelayedPayload payload = new RDelayedPayload();
payload.setId("1");
Map<String, Object> extra = new HashMap<>();
extra.put("oper", "timeout");
payload.setExtra(extra);
```

### 11，消息模块的使用

> 统一消息模块，可实时推送数据给平台用户，支持待办、消息两种推送方式

#### 1）引入依赖

```xml
<dependency>
    <groupId>com.bjxczy.onepark</groupId>
    <artifactId>onepark-core-web</artifactId>
</dependency>
```

#### 2）发送消息/待办

消息体：

| 名称        | 类型    | 说明                                      |
| ----------- | ------- | ----------------------------------------- |
| staffId     | Integer | 员工ID                                    |
| businessId  | String  | 业务ID                                    |
| eventCode   | String  | 事件编码                                  |
| eventType   | String  | 事件类型: 1-待办、2-消息                  |
| title       | String  | 标题                                      |
| content     | String  | 内容                                      |
| messageType | String  | 消息类型（info、success、warning、error） |
| autoClose   | Boolean | 是否自动关闭                              |
| extra       | Map     | 额外内容（业务相关内容）                  |

发送平台消息：

```java
@SpringBootTest
public SomeServiceTest {
    
    @Autowired
    private MessageService messageService;
    
    @Test
    public void pushMessage() {
        PushMessageInformation info = new PushMessageInformation();
        // 填充相关信息
        messageService.pushMessage(info);
	}
    
}
```

#### 3）发送模板消息

> 通过提前定义消息模板，传递变量方式发送平台消息

定义模板：workbench.workbench_dict where group_code = 'eventCode'，样例如下：

| group_code | label    | value               | extra                                                        |
| ---------- | -------- | ------------------- | ------------------------------------------------------------ |
| eventCode  | 提交申请 | ProcessEngine_Start | {"template":"已提交申请，请处理","eventType":"1","messageType":"info","autoClose":true,"extra":{}} |
| eventCode  | 审批结束 | ProcessEngine_End   | {"template":"审批已结束，请查看","eventType":"2","messageType":"info","autoClose":true,"extra":{}} |
| eventCode  | 调整申请 | ProcessEngine_Retry | {"template":"您的审批被驳回，请调整申请信息，重新提交","eventType":"1","messageType":"info","autoClose":true,"extra":{}} |

extra描述

```json
{
	"template": "审批已结束，请查看",  // velocity模板
	"eventType": "2",  // 事件类型: 1-待办、2-消息
	"messageType": "info", // 消息类型（info、success、warning、error）
	"autoClose": true,  // 是否自动关闭
	"extra": {
        "allowClick": "none", // 是否允许点击：none-不允许；always-始终允许；inProgress；仅允许状态为进行中时点击
        "routerName": "approval", // 前端路由Name
        ...  // 额外内容自由定义（业务相关内容）
    }
}
```

消息体：

| 名称       | 类型                | 说明     |
| ---------- | ------------------- | -------- |
| staffId    | Integer             | 员工ID   |
| businessId | String              | 业务ID   |
| eventCode  | String              | 事件编码 |
| params     | Map<String, Object> | Velocity模板参数 |

内置变量：

> velocitry模板参数中默认写入变量

| 变量名 | 类型 | 描述 |
| - | - | - |
| val_dept_name | String | 接收人二级部门名称 |
| val_staff_name | String | 接收人姓名 |

发送模板消息：

```java
@SpringBootTest
public SomeServiceTest {
    
    @Autowired
    private MessageService messageService;
    
    @Test
    public void pushMessage() {
        TemplateMessagePayload info = new TemplateMessagePayload();
        // 填充相关信息
        messageService.pushTemplateMessage(info);
	}
    
}
```

#### 4）平台消息处理

> 描述：当关联业务状态变更时，需更新平台待办状态

消息体：
| 名称       | 类型    | 说明                               |
| ---------- | ------- | ---------------------------------- |
| eventCode  | String  | 事件编码                           |
| businessId | String  | 业务ID                             |
| status     | Integer | 状态：0-未完成、1-已完成           |
| statusName | String  | 状态描述：非必填，默认根据状态取值 |

发送模板消息：
```java
@SpringBootTest
public SomeServiceTest {
    
    @Autowired
    private MessageService messageService;
    
    @Test
    public void pushMessage() {
        DealMessageInformation info = new DealMessageInformation("2de3idzld31234ksl", "door_devive_alarm", 1);
        messageService.dealMessage(info);
	}
    
}
```

### 12，审批业务开发

#### 1）引入依赖

```xml
<dependency>
    <groupId>com.bjxczy.onepark</groupId>
    <artifactId>onepark-core-web</artifactId>
</dependency>
```

#### 2）定义业务表单

* 登录园区平台
* 访问：智慧管理/智脑引擎/表单管理
* 创建表单，并配置业务所需表单，保存
* 数据库：workflow.wf_formdef，查找新创建表单数据，更新form_type=1
* 数据库：workflow.wf_form_item，查找创建表单关联字段，更新fixed=1

> form_type=1的表单为业务相关表单，无法被删除，并且会作为园区初始化数据
>
> fixed=1的字段为业务相关字段，无法被删除，并且会作为园区初始化数据

#### 3）定义业务流程

* 登录园区平台
* 访问：智慧管理/智脑引擎/审批流程管理
* 新建流程，新建版本，并配置业务流程，保存
* 数据库：workflow.wf_procdef，查找新创建流程，更新deploy_id={业务编码}；deploy_type=1；service_name={子模块服务名}

> deploy_type=1的流程为业务流程，无法被删除，并且会作为园区初始化数据

#### 4）配置待办、消息模板

> 参考第四章.11.2 发送模板消息，定义消息模板

需定义三种模板用于：提交申请待办、调整申请待办、审批结束消息，样例如下：
| group_code | label    | value               | extra                                                        |
| ---------- | -------- | ------------------- | ------------------------------------------------------------ |
| eventCode  | 提交申请 | ProcessEngine_Start | {"template":"已提交申请，请处理","eventType":"1","messageType":"info","autoClose":true,"extra":{}} |
| eventCode  | 审批结束 | ProcessEngine_End   | {"template":"审批已结束，请查看","eventType":"2","messageType":"info","autoClose":true,"extra":{}} |
| eventCode  | 调整申请 | ProcessEngine_Retry | {"template":"您的审批被驳回，请调整申请信息，重新提交","eventType":"1","messageType":"info","autoClose":true,"extra":{}} |

事件编码命名规则：

* 提交申请待办：{流程DeployId}_Start
* 调整申请待办：{流程DeployId}_Retry
* 审批结束消息：{流程DeployId}_End

默认内置变量：

> 用户提交申请的全部变量将作为velocity参数调用发送模板消息接口，key为wf_form_item.form_key

| 变量名           | 类型   | 描述                                                     |
| ---------------- | ------ | -------------------------------------------------------- |
| val_dept_name    | String | 接收人二级部门名称                                       |
| val_staff_name   | String | 接收人姓名                                               |
| start_staff_name | String | 申请人姓名（仅ProcessEngine_Start可用）                  |
| start_dept_name  | String | 申请二级部门名称（仅ProcessEngine_Start可用）            |
| val_submit_time  | String | 申请时间（仅ProcessEngine_Start、ProcessEngine_End可用） |

#### 5）实现IWorkflowService

IWorkflowService定义

```java
public interface IWorkflowService {

	/**
	 * 审批校验，需校验用户提交申请数据是否合理，及业务限制
	 * @param key 业务流程DeployId
	 * @param payload 员工ID、用户提交变量数据
	 * @return
	 */
	R<?> validated(String key, ProcessPayload payload);

	/**
	 * 审批通过处理
	 * @param key 业务流程DeployId
	 * @param payload 员工ID、用户提交变量数据
	 * @return
	 */
	R<?> approved(String key, ProcessPayload payload);

}
```

IWorkflowService实现

```java
@Service
public class TestServiceImpl implements IWorkflowService {

	@Override
	public R<?> validated(String key, ProcessPayload payload) {
		System.out.println(key);
		return R.error("校验未通过！");
	}

	@Override
	public R<?> approved(String key, ProcessPayload payload) {
		// TODO 业务处理
		return R.success();
	}

}
```

### 13，工单联动业务开发

#### 1）引入依赖

```xml
<dependency>
    <groupId>com.bjxczy.onepark</groupId>
    <artifactId>onepark-core-web</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```

#### 2）WorkOrderService的使用

```java
@SpringBootTest
public class WorkOrderTest {
	
	@Autowired
	private WorkOrderService workOrderService;
	
	 // 工单配置ID，根据业务在工单管理模块定义
    private String id = "78f41292ccfb21efc9199c0611d8aaad";
    
    @Test
    public void pushOrder() {
        // 简单工单
        workOrderService.pushOrder(id, "测试工单");
    }
    
    @Test
    public void pushOrder2() {
        // 复杂描述工单
        DescribeItem d_0 = new DescribeItem("申请人", new TextDescribeValue("李梦"));
        List<String> urls = CollectionUtil.newArrayList("https://dev.xconepark.com/images/xxx.jpg");
        DescribeItem d_1 = new DescribeItem("照片", new ImageDescribeValue(urls));
        List<DescribeItem> items = CollectionUtil.newArrayList(d_0, d_1);
        workOrderService.pushOrder(id, "申请已通过，请根据信息创建", items);
    }
    
    @Test
    public void pushOrder3() {
    	// 设备报修工单
    	PushOrderPayload payload = new PushOrderPayload(id, "XXX设备报修");
    	payload.setDeviceId("efdicialdfqoinvadsf");
    	workOrderService.pushOrder(payload);
    }

}
```

### 14，IOC大屏消息模块的使用

> IOC消息模块，可实时推送数据给平台所有用户

#### 1）引入依赖

```xml
<dependency>
    <groupId>com.bjxczy.onepark</groupId>
    <artifactId>onepark-core-web</artifactId>
</dependency>
```

#### 2）发送消息

消息体：

| 名称        | 类型    | 说明                                      |
| ----------- | ------- | ----------------------------------------- |
| title       | String  | 标题                                      |
| messageType | String  | 消息类型（info、success、warning、error） |
| autoClose   | Boolean | 是否自动关闭                              |
| extra       | Map     | 额外内容（业务相关内容）                  |
| iocItemKey  | String  | Ioc模块key                                |

发送平台消息：

```java
@SpringBootTest
public SomeServiceTest {
    
    @Autowired
    private IOCMessageService messageService;
    
    @Test
    public void pushMessage() {
        PushIOCMessageInformation information = new PushIOCMessageInformation();
        // 填充相关信息
        information.setIocItemKey("aiEvent");
        information.setTitle("AI事件消息");
        information.setMessageType("info");
        information.setAutoClose(true);
        HashMap<String, Object> extra = new HashMap<>();
        extra.put()
        ... //额外内容自由定义（业务相关内容）
        information.setExtra(extra);
        iocMessageService.pushMessage(information);
	}
    
}
```

IOC模块key定义：

| 模块名称 | 模块Key          |
| -------- | ---------------- |
| AI事件   | aiEvent          |
| 访客     | visitor          |
| 视频巡更 | videoPatrol      |
| 电子巡更 | electronicPatrol |
| 巡更异常 | patrolAbnormal   |
| 门禁     | entranceGuard    |
| 车辆     | vehicle          |

#### 


## 五、部署

### 1，docker-compose方式

多环境配置

```yaml
# 数据库配置
# application.yml
spring:
  datasource:
    url: jdbc:mysql://${env.datasource.host}/per_schema?allowPublicKeyRetrieval=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=Asia/Shanghai
    username: root
    password: onepark!123qwe
    driver-class-name: com.mysql.cj.jdbc.Driver
    
# 开发环境配置：创建application-dev.yml
env:
  datasource:
    host: 192.165.1.64:32770
# 生产环境配置：创建application-prod.yml
env:
  datasource:
    host: 192.165.1.64:32770 # 后续通过环境变量方式、nacos等方式替换
```

64服务器访问方式：

* IP：192.165.1.64
* SSH端口：22
* 账号：xczy
* 密码：mdsoss

镜像打包

```shell
# 使用mavne打包为jar包
mvn package -Dmaven.test.skip=true
# 上传jar至/home/xczy/xconepark/springboot，可使用rz上传
rz -be
# 修改名称为app.jar
mv onepark-user-0.0.1.jar app.jar
# 打包镜像，约定前缀xconepark，版本号：0.0.1(查询历史版本+1)
docker build -t xconepark/user:0.0.1 .
# 进入上级目录
cd /home/xczy/xconepark
# 使用vim修改对应版本号
vim docker-compose.yml
...
bmpuser:
    image: xconepark/bmpuser:0.0.6 # 修改为新版本
    network_mode: "host"
    environment:
      server.port: 30007
    env_file: onepark-variables.env
    deploy:
      replicas: ${replicas}
      restart_policy:
        condition: on-failure
      resources:
        limits:
          memory: 2G
        reservations:
          memory: 512M
... 
# -wq，保存
# 执行更新，并等待更新完成
docker-compose up -d bmpuser
```

### 2，k8s部署

> 基于GitLab CI/CD方式部署，部署文档查看以下仓库
>
> git：http://192.165.1.124:10000/chnEnergy/xconepark/devops-templates

