package com.bjxczy.core.redisson.delayed;

import java.util.concurrent.TimeUnit;

import org.redisson.api.RBlockingDeque;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import io.micrometer.core.instrument.util.StringUtils;

@Service
public abstract class DelayedQueueImpl implements IDelayedQueue, InitializingBean {

	@Autowired
	private RedissonClient redissonClient;

	protected RDelayedQueue<RDelayedPayload> delayQueue;

	protected RBlockingDeque<RDelayedPayload> blockingQueue;

	@Override
	public RBlockingDeque<RDelayedPayload> getBlockingQueue() {
		return blockingQueue;
	}

	@Override
	public void offer(RDelayedPayload value, long delay, TimeUnit timeUnit) {
		this.delayQueue.offer(value, delay, timeUnit);
	}

	@Override
	public void removeById(RDelayedPayload value) {
		this.delayQueue.remove(value);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Component component = this.getClass().getAnnotation(Component.class);
		String name = component.value();
		if (StringUtils.isBlank(name)) {
			throw new RuntimeException("[DelayedQueue] 延迟队列名称未定义！");
		}
		this.blockingQueue = redissonClient.getBlockingDeque(name);
		this.delayQueue = redissonClient.getDelayedQueue(this.blockingQueue);
	}

}
