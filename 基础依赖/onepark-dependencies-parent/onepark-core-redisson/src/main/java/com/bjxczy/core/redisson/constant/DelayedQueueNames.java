package com.bjxczy.core.redisson.constant;

public interface DelayedQueueNames {

	/**
	 * 资产管理模块：资产保养
	 */
	String DEVICE_UPKEEP_TASK_QUEUE="bmpdevice_upkeep_task_queue";

	/**
	 * 审批流模块：审批超时队列
	 */
	String WORKFLOW_USER_TASK_QUEUE = "bmpworkflow_user_task_queue";
	/**
	 * 巡更任务状态延时修改队列
	 */
	String SECURITY_PLAN_TASK_QUEUE = "bmpsecurity_plan_task_queue";

	/**
	 * 巡更任务状态延时修改队列
	 */
	String SECURITY_TASK_MESSAGE_QUEUE = "bmpsecurity_task_message_queue";

	/**
	 * 工单模块：工单超时队列
	 */
	String WORKFLOW_ORDER_TASK_QUEUE="workflow_order_task_queue";

	/**
	 * 工单模块：提前十分钟提醒派单
	 */
	String WORKFLOW_ORDER_WARN_TASK_QUEUE="workflow_order_warn_task_queue";

	/**
	 * 工单模块：发起执行工单
	 */
	String WORKFLOW_ORDER_CREATE_TASK_QUEUE="workflow_order_create_task_queue";


	/**
	 * 电子巡更-到点修改任务状态
	 */
	String SECURITY_KEEP_WATCH_PLAN_TASK_QUEUE = "bmpsecurity_keep_watch_plan_task_queue";

	/**
	 * 电子巡更-提前一小时发通知
	 */
	String SECURITY_KEEP_WATCH_TASK_MESSAGE_QUEUE = "bmpsecurity_keep_watch_task_message_queue";

	/**
	 * 智慧停车-临时车状态修改
	 */
	String CAR_WHITE_LIST_CHANGE_STATUS_QUEUE = "car_white_list_change_status_queue";

	/**
	 * 智慧停车-黑名单车状态修改
	 */
	String CAR_BLACK_LIST_CHANGE_STATUS_QUEUE = "car_black_list_change_status_queue";


	/**
	 * 人脸布控-布控计划下发状态修改
	 */
	String SECURITY_LAYOUT_CONTROL_PLAN_QUEUE = "security_layout_control_plan_queue";
}
