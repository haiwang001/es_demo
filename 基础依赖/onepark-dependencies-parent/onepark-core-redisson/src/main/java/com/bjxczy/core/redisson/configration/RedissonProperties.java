package com.bjxczy.core.redisson.configration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "spring.redis")
public class RedissonProperties {

	private String host;
	
	private String port;
	
	private String password;
	
	private Integer timeout = 3000;
	
	private Integer rsDataBase = 2;
	
	public String toConnectAddress() {
		return "redis://" + host + ":" + port;
	}
	
}
