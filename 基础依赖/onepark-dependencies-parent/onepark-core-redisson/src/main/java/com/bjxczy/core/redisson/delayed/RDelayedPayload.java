package com.bjxczy.core.redisson.delayed;

import java.io.Serializable;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RDelayedPayload implements Serializable {
	
	private static final long serialVersionUID = -2022803793590208025L;

	private Object id;
	
	private Map<String, Object> extra;
	
	public RDelayedPayload(Object id) {
		super();
		this.id = id;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getId() {
		return (T) id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public Map<String, Object> getExtra() {
		return extra;
	}

	public void setExtra(Map<String, Object> extra) {
		this.extra = extra;
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return this.id.equals(obj);
	}
	
}
