package com.bjxczy.core.redisson.delayed;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DelayedQueueInitializer implements ApplicationContextAware {
	
	private ExecutorService executor;
	
	private Map<String, IDelayedQueue> beans;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		 this.beans = applicationContext.getBeansOfType(IDelayedQueue.class);
		 this.executor = Executors.newCachedThreadPool(); // XXX 后续可能需优化为固定范围线程池
		 initListener();
	}
	
	public void shutdown() {
		this.executor.shutdownNow();
	}
	
	private void initListener() {
		this.executor.execute(() -> {
			while(true) {
				try {
					for (Entry<String, IDelayedQueue> entry : beans.entrySet()) {
						RDelayedPayload payload = entry.getValue().getBlockingQueue().poll();
						if (payload != null) {
							this.executor.execute(() -> {
								entry.getValue().execute(payload);
							});
						}
					}
					TimeUnit.MICROSECONDS.sleep(500);
				} catch (InterruptedException e) {
					log.info("[DelayedQueue] Redisson延迟队列监听异常中断，{}", e.getMessage());
					break;
				}
			}
		});
		log.info("[DelayedQueue] Redisson延迟队列监听启动成功！");
	}
	
}
