package com.bjxczy.core.redisson.delayed;

import java.util.concurrent.TimeUnit;

import org.redisson.api.RBlockingDeque;

public interface IDelayedQueue {
	
	/**
	 * 消息到期回调
	 * @param payload 消息内容
	 */
	void execute(RDelayedPayload payload);
	
	/**
	 * 新增延迟消息
	 * @param value 消息内容
	 * @param delay 延迟时间
	 * @param timeUnit 延迟单位
	 */
	void offer(RDelayedPayload value, long delay, TimeUnit timeUnit);
	
	/**
	 * 根据ID删除消息
	 * @param value
	 */
	void removeById(RDelayedPayload value);
	
	/**
	 * 获取回调队列实例
	 * @return
	 */
	RBlockingDeque<RDelayedPayload> getBlockingQueue();

}
