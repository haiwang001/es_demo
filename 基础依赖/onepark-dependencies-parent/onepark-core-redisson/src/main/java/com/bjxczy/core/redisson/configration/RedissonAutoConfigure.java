package com.bjxczy.core.redisson.configration;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bjxczy.core.redisson.delayed.DelayedQueueInitializer;

@Configuration
public class RedissonAutoConfigure {

	@Bean
	public RedissonProperties redisProperties() {
		return new RedissonProperties();
	}

	@Bean(destroyMethod = "shutdown")
	public RedissonClient redissonClient(RedissonProperties properties) {
		Config config = new Config();
		config.useSingleServer() // 单Redis节点模式
				.setAddress(properties.toConnectAddress())
				.setPassword(properties.getPassword())
				.setDatabase(properties.getRsDataBase())
				.setTimeout(properties.getTimeout());
		return Redisson.create(config);
	}
	
	@Bean(destroyMethod = "shutdown")
	public DelayedQueueInitializer delayedQueueInitializer() {
		return new DelayedQueueInitializer();
	}

}
