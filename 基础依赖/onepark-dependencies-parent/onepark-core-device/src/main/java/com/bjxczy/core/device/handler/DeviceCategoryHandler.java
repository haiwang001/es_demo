package com.bjxczy.core.device.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.bjxczy.core.device.feign.SyncDeviceFeignClientInstance;
import com.bjxczy.onepark.common.model.device.DeviceInformation;
import com.bjxczy.onepark.common.model.device.DeviceStatus;

public abstract class DeviceCategoryHandler {

	@Autowired
	private SyncDeviceFeignClientInstance syncFeignClient;

	/**
	 * 获取子模块全部设备列表
	 * @return
	 */
	protected List<DeviceInformation> thirdAll() {
		return null;
	}

	/**
	 * 获取子模块全部设备状态
	 * @return
	 */
	protected abstract Map<String, DeviceStatus> thirdStatus();

	/**
	 * 获取子模块指定设备状态
	 * @param idSet 三方ID
	 * @return
	 */
	protected abstract Map<String, DeviceStatus> thirdStatus(Set<String> idSet);

	public void reportThirdDeviceList(String category) {
		try {
			List<DeviceInformation> list = thirdAll();
			if (list != null) {
				syncFeignClient.reportThirdDeviceList(category, list);
			}
		} catch (Exception e) {
			e.printStackTrace();
//			throw new RuntimeException("上报设备列表失败！");
		}
	}

	public void reportThirdDeviceStatus(String category, Set<String> idSet) {
		try {
			Map<String, DeviceStatus> status = new HashMap<>();
			if (idSet != null && idSet.size() > 0) {
				status = thirdStatus(idSet);
			} else {
				status = thirdStatus();
			}
			syncFeignClient.reportThirdDeviceStatus(category, status);
		} catch (Exception e) {
			e.printStackTrace();
//			throw new RuntimeException("上报设备状态失败！");
		}
	}

}
