package com.bjxczy.core.device.handler;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.bjxczy.core.rabbitmq.supports.DeviceBaseListener;
import com.rabbitmq.client.Channel;

public class DeviceSyncRabbitListener extends DeviceBaseListener implements ApplicationContextAware {

	private Map<String, DeviceCategoryHandler> handlers;

	/**
	 * 多设备类型支持
	 *
	 * @param func
	 */
	private void callHandler(BiConsumer<DeviceCategoryHandler, DeviceCategoryDefinition> func) {
		for (Entry<String, DeviceCategoryHandler> entry : handlers.entrySet()) {
			DeviceCategoryHandler handler = entry.getValue();
			DeviceCategoryDefinition category = handler.getClass().getAnnotation(DeviceCategoryDefinition.class);
			if (category != null) {
				func.accept(handler, category);
			}
		}
	}

	@RabbitListener(bindings = { @QueueBinding(value =
			@Queue,
			key = SYNC_DEVICE_EVENT,
			exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) // 绑定的交换机
	)})
	@Override
	public void syncDeviceEvent(String body, Channel channel, Message message) {
		callHandler((handler, category) -> handler.reportThirdDeviceList(category.value()));
	}

	@RabbitListener(bindings = { @QueueBinding(
			value = @Queue,
			key = SYNC_DEVICE_STATUS_EVENT,
			exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) // 绑定的交换机
	)})
	@Override
	public void syncDeviceStatusEvent(Set<String> idSet, Channel channel, Message message) {
		callHandler((handler, category) -> handler.reportThirdDeviceStatus(category.value(), idSet));
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.handlers = applicationContext.getBeansOfType(DeviceCategoryHandler.class);
	}

}
