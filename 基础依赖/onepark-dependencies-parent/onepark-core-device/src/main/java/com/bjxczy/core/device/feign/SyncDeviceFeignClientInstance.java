package com.bjxczy.core.device.feign;

import org.springframework.cloud.openfeign.FeignClient;

import com.bjxczy.core.feign.client.device.SyncDeviceFeignClient;

@FeignClient(value = "bmpdevice", contextId = "innerDeviceFeignClient")
public interface SyncDeviceFeignClientInstance extends SyncDeviceFeignClient {
	
}
