package com.bjxczy.core.device.configuration;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bjxczy.core.device.handler.DeviceSyncRabbitListener;

@Configuration
@EnableFeignClients("com.bjxczy.core.device.feign")
public class DeviceAutoConfigure {
	
	@Bean
	public DeviceSyncRabbitListener deviceSyncRabbitListener() {
		return new DeviceSyncRabbitListener();
	}

}
