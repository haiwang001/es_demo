package com.bjxczy.core.data.configure;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bjxczy.core.data.core.elasticsearch.ElasticsearchSqlTemplate;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;

@Configuration
@ConditionalOnProperty(prefix = "onepark.data.elasticsearch", name = "enable", havingValue = "true", matchIfMissing = false)
public class ElasticsearchConfigure {

	@Bean
	public RestClient restClient(OneParkDataProperties properties) {
		return RestClient
				.builder(new HttpHost(properties.getElasticsearch().getHost(), properties.getElasticsearch().getPort()))
				.build();
	}

	@Bean
	public ElasticsearchClient elasticsearchClient(RestClient restClient) {
		ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
		return new ElasticsearchClient(transport);
	}

	@Bean
	public ElasticsearchSqlTemplate elasticsearchSqlTemplate() {
		return new ElasticsearchSqlTemplate();
	}

}
