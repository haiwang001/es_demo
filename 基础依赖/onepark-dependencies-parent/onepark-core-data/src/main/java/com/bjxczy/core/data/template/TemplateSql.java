package com.bjxczy.core.data.template;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.bjxczy.onepark.common.constant.ExtendHttpMethod;
import com.bjxczy.onepark.common.model.common.ApiResourceInformation;

import lombok.Data;

@Data
public class TemplateSql {

	private String namespaceKey;

	private String sql;

	private String nodeName;

	private String name;

	private Map<String, String> parameterMap;

	private Map<String, String> exportMap;

	private List<String> apis;

	private static final List<String> API_URL_SUFFIX = Arrays.asList("list", "page", "export", "inner");

	public void setApisByAttr(String attr) {
		String[] array = attr.split("\\|");
		// 校验关键字是否正确
		for (String item : array) {
			if (!API_URL_SUFFIX.contains(item)) {
				throw new UnsupportedOperationException("[TemplateSql] [" + this.namespaceKey + "] 定义不支持API类型：" + item);
			}
		}
		// 当作为API资源时，name必须定义
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("[TemplateSql] [" + this.namespaceKey + "] 未定义API资源name！");
		}
		this.apis = Arrays.asList(array);
	}

	public boolean isResource() {
		return this.apis != null && this.apis.size() > 0;
	}

	public List<ApiResourceInformation> toApiResources(String key, String applicationName) {
		if (isResource()) {
			return this.apis.stream()
					.filter(e -> !Objects.equals("inner", e)) // 内部API
					.map(e -> {
				ApiResourceInformation api = new ApiResourceInformation();
				api.setName(this.name);
				api.setServiceName(applicationName);
				api.setHttpMethods(Collections.singleton(ExtendHttpMethod.POST));
				api.setPatterns(Collections.singleton("/template/res/" + key + "/" + e));
				return api;
			}).collect(Collectors.toList());
		} else {
			throw new RuntimeException("SQL未开启apis配置！");
		}
	}

}
