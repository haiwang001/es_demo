package com.bjxczy.core.data.configure;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.bjxczy.core.data.configure.OneParkDataProperties.TemplateSqlProperties;
import com.bjxczy.core.data.io.Resources;
import com.bjxczy.core.data.template.NacosSqlConfigListener;
import com.bjxczy.core.data.template.SqlParser;
import com.bjxczy.core.data.template.TemplateSqlContainer;
import com.bjxczy.core.data.template.TemplateSqlServiceImpl;
import com.bjxczy.onepark.common.service.TemplateSqlService;

@Configuration
public class DataAutoConfigure {
	
	@Value("${spring.application.name}")
	private String applicationName;
	
	@Bean
	public OneParkDataProperties oneparkDataProperties() {
		return new OneParkDataProperties();
	}
	
	@Bean
	public TemplateSqlContainer templateSqlMap(NacosConfigManager nacosConfigManager, OneParkDataProperties properties)
			throws NacosException, IOException, DocumentException {
		TemplateSqlContainer container = new TemplateSqlContainer();
		NacosSqlConfigListener listener = new NacosSqlConfigListener(container);
		TemplateSqlProperties tsc = properties.getTemplateSql();
		if (StringUtils.isNotBlank(tsc.getLocation())) {
			Resource[] resources = Resources.getResources(tsc.getLocation());
			ConfigService configService = nacosConfigManager.getConfigService();
			InputStream in = null;
			for (Resource r : resources) {
				String dataId = applicationName + "_" + r.getFilename(), xml = null;
				if (tsc.getRemoteLoad()) {
					xml = configService.getConfig(dataId, applicationName, 10000);
					// 订阅配置更新
					configService.addListener(dataId, applicationName, listener);
				}
				if (xml == null) {
					in = r.getInputStream();
					xml = IOUtils.toString(in, "UTF-8");
					IOUtils.closeQuietly(in);
					if (tsc.getRemoteLoad()) {
						configService.publishConfig(dataId, applicationName, xml);
					}
				}
				container.putXmlTemplateSql(xml);
			}
		}
		return container;
	}
	
	@Bean
	public TemplateSqlService templateSqlService() {
		return new TemplateSqlServiceImpl();
	}
	
	@Bean
	public SqlParser sqlParser() {
		return new SqlParser();
	}
	
}
