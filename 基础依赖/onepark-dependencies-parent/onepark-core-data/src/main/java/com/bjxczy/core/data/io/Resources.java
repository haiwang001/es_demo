package com.bjxczy.core.data.io;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

public class Resources {

	private static final ClassLoaderWrapper classLoaderWrapper = new ClassLoaderWrapper();
	
	private static final ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();

	public static InputStream getResourceAsStream(String resource) throws IOException {
		return getResourceAsStream(null, resource);
	}

	public static InputStream getResourceAsStream(ClassLoader loader, String resource) throws IOException {
		InputStream in = classLoaderWrapper.getResourceAsStream(resource, loader);
		if (in == null) {
			throw new IOException("Could not find resource " + resource);
		}
		return in;
	}
	
	public static Resource[] getResources(String location) {
		try {
			return resourceResolver.getResources(location);
		} catch (IOException e) {
			return new Resource[0];
		}
	}
	
}
