package com.bjxczy.core.data.util;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.util.JdbcConstants;

public class SqlUtils {

	public static final String PAGE_START_KEY = "VAL_PAGE_START";

	public static final String PAGE_END_KEY = "VAL_PAGE_END";

	public static String getPageSql(String sql) {
		String pageSql = "select * from ( " + sql + " ) t limit :" + PAGE_START_KEY + ",:" + PAGE_END_KEY;
		return pageSql;
	}

	public static String getCountSql(String sql) {
		String countSql = "select count(*) \"COUNT\" from ( " + sql + " ) t ";
		return countSql;
	}

	public static String formatSql(String sql) {
		return SQLUtils.format(sql, JdbcConstants.MYSQL);
	}

	public static boolean sqlParamCheck(String str) {
		str = str.toLowerCase();// 统一转为小写
		String badStr = "select|update|and|or|delete|insert|truncate|char|into|substr|ascii|declare|exec|count|master|into|drop|execute|table";
		String[] badStrs = badStr.split("\\|");
		for (int i = 0; i < badStrs.length; i++) {
			// 循环检测，判断在请求参数当中是否包含SQL关键字
			if (str.contains(badStrs[i])) {
				return false;
			}
		}
		return true;
	}

	public static <T> String toInCondition(List<T> list, Function<T, String> func) {
		return "(" + list.stream().map(func).collect(Collectors.joining(",")) + ")";
	}

}
