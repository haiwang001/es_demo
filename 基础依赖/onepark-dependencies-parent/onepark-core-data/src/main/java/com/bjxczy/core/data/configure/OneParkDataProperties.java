package com.bjxczy.core.data.configure;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Configuration
@ConfigurationProperties(prefix = "onepark.data")
public class OneParkDataProperties {
	
	private ElasticsearchProperties elasticsearch = new ElasticsearchProperties();
	
	private TemplateSqlProperties templateSql = new TemplateSqlProperties();
	
	@Data
	public class ElasticsearchProperties {
		
		private Boolean enable;
		
		private String host;
		
		private Integer port;
		
	}
	
	@Data
	public class TemplateSqlProperties {
		
		private String location;
		
		private Boolean remoteLoad;
		
		private Boolean showSql;
		
	}

}
