package com.bjxczy.core.data.core.elasticsearch;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.bjxczy.core.data.annotation.ModelId;
import com.bjxczy.core.data.annotation.ModelName;
import com.bjxczy.core.data.core.IDataRepository;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.GetResponse;
import co.elastic.clients.elasticsearch.core.IndexRequest.Builder;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import lombok.SneakyThrows;

public class ElasticsearchRepository<T> implements IDataRepository<T, String> {

	@Autowired
	private ElasticsearchClient esClient;

	private Class<T> clazz;

	private Field idField;

	private String index;

	public ElasticsearchRepository(Class<T> clazz) {
		this.clazz = clazz;
		this.index = getName();
		this.idField = getIdField();
	}

	@SneakyThrows
	@Override
	public boolean save(T model) {
		IndexResponse response = esClient.index(i -> {
			Builder<Object> builder = i.index(this.index);
			Object id = getId(model);
			if (id != null) {
				builder.id(id.toString());
			}
			return builder.document(model);
		});
		setId(model, response.id());
		return true;
	}

	@SneakyThrows
	@Override
	public boolean remove(String id) {
		esClient.delete(d -> d.index(this.index).id(id));
		return true;
	}

	@SneakyThrows
	@Override
	public T findById(String id) {
		GetResponse<T> response = esClient.get(g -> g.index(this.index).id(id), clazz);
		return response.source();
	}

	@SneakyThrows
	@Override
	public List<T> list() {
		SearchResponse<T> response = esClient.search(s -> s.index(this.index), clazz);
		List<Hit<T>> hits = response.hits().hits();
		return hits.stream().map(e -> e.source()).collect(Collectors.toList());
	}

	private String getName() {
		ModelName modelName = this.clazz.getAnnotation(ModelName.class);
		if (modelName == null) {
			throw new RuntimeException("@ModelName 未定义！");
		}
		return modelName.value();
	}

	private Field getIdField() {
		Field[] fields = this.clazz.getDeclaredFields();
		for (Field f : fields) {
			ModelId modelId = f.getAnnotation(ModelId.class);
			if (modelId != null) {
				return f;
			}
		}
		throw new RuntimeException("@ModelId 未定义！");
	}

	@SneakyThrows
	private Object getId(T model) {
		this.idField.setAccessible(true);
		return this.idField.get(model);
	}

	@SneakyThrows
	private void setId(T model, Object value) {
		this.idField.setAccessible(true);
		this.idField.set(model, value);
	}

}
