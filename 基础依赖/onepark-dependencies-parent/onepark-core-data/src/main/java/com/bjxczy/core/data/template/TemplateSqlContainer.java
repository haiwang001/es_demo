package com.bjxczy.core.data.template;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TemplateSqlContainer extends TreeMap<String, TemplateSql> {

	private static final long serialVersionUID = 1738603574002769058L;

	private static final String NAME_SPACE_PRE = "NAME_SPACE_";

	public String getSql(String key) {
		return super.get(key).getSql();
	}

	public Map<String, String> getParameterMap(String key) {
		return super.get(key).getParameterMap();
	}

	public void putXmlTemplateSql(String xml) throws DocumentException {
		putXmlTemplateSql(xml, false);
	}

	public void putXmlTemplateSql(String xml, boolean replate) throws DocumentException {
		Document doc = DocumentHelper.parseText(xml);
		Element root = doc.getRootElement();

		String namespace = root.attributeValue("namespace");
		if (this.containsKey(NAME_SPACE_PRE + namespace) && !replate) {
			throw new RuntimeException("[TemplateSql] namespace不能重复！ - " + namespace);
		}
		this.put(NAME_SPACE_PRE + namespace, null);
		putItem(replate, getParameterMap(namespace, root.elements("parameterMap")), namespace, root.elements("sql"),
				root.elements("elasticsearch"));
	}

	private Map<String, Map<String, String>> getParameterMap(String namespace, List<Element> list) {
		Map<String, Map<String, String>> tempMap = new HashMap<>();
		for (Element e : list) {
			String key = e.attributeValue("id");
			Map<String, String> item = new HashMap<>();
			List<Element> colums = e.elements("column");
			for (Element c : colums) {
				item.put(c.attributeValue("name"), c.getText());
			}
			key = namespace + "/" + key;
			if (tempMap.containsKey(key)) {
				throw new RuntimeException("[TemplateSql] parameterMap id=["+ key +"] 已存在！ ");
			}
			tempMap.put(key, item);
		}
		return tempMap;
	}

	private void putItem(boolean replate, Map<String, Map<String, String>> parameterMap, String namespace,
			List<Element>... lists) {
		BiConsumer<String, Consumer<Map<String, String>>> func = (key, cons) -> {
			if (StringUtils.isNotBlank(key)) {
				key = namespace + "/" + key;
				if (parameterMap.containsKey(key)) {
					cons.accept(parameterMap.get(key));
				} else {
					throw new RuntimeException("[TemplateSql] [" + key + "] parameterMap不存在！");
				}
			}
		};
		for (List<Element> elements : lists) {
			for (Element e : elements) {
				String key = namespace + "/" + e.attributeValue("id"),
						apis = e.attributeValue("apis"),
						paramKey = e.attributeValue("parameter-map"),
						exportKey = e.attributeValue("export-map"),
						content = e.getText();
				TemplateSql sql = new TemplateSql();
				sql.setNamespaceKey(key);
				sql.setSql(content);
				sql.setName(e.attributeValue("name"));
				sql.setNodeName(e.getName());
				if (StringUtils.isNotBlank(apis)) {
					sql.setApisByAttr(apis);
				}
				func.accept(paramKey, map -> sql.setParameterMap(map));
				func.accept(exportKey, map -> sql.setExportMap(map));
				this.put(key, sql);
				log.info("Template Sql {} -> [{}]:{}", replate ? "reload" : "loaded", e.getName(), key);
			}
		}
	}

}
