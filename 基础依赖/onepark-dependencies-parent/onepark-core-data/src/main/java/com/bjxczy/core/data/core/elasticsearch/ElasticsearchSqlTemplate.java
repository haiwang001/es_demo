package com.bjxczy.core.data.core.elasticsearch;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import cn.hutool.core.util.ReflectUtil;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.ElasticsearchException;
import co.elastic.clients.elasticsearch.core.DeleteResponse;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.sql.Column;
import co.elastic.clients.elasticsearch.sql.QueryResponse;
import co.elastic.clients.json.JsonData;
import jakarta.json.JsonValue;
import lombok.SneakyThrows;

public class ElasticsearchSqlTemplate {

	@Autowired
	private ElasticsearchClient esClient;

	public IndexResponse index(String index, Object document) {
		return index(index, null, document);
	}

	public IndexResponse index(String index, String id, Object document) {
		try {
			return esClient.index(i -> {
				i.index(index);
				if (id != null) {
					i.id(id);
				}
				return i.document(document);
			});
		} catch (ElasticsearchException | IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public DeleteResponse delete(String index, String id) {
		try {
			return esClient.delete(d -> d.index(index).id(id));
		} catch (ElasticsearchException | IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private QueryResponse queryBySql(String sql, Integer fetchSize) throws ElasticsearchException, IOException {
		return esClient.sql().query(q -> {
			q.query(sql);
			if (fetchSize != null && fetchSize > 0) {
				q.fetchSize(fetchSize);
			}
			return q;
		});
	}

	public List<Map<String, Object>> queryForList(String sql) {
		return queryForList(sql, null);
	}

	@SneakyThrows
	public List<Map<String, Object>> queryForList(String sql, Integer fetchSize) {
		QueryResponse response = queryBySql(sql, fetchSize);
		List<List<JsonData>> rows = response.rows();
		return rows.stream().map(e -> toMap(response.columns(), e)).collect(Collectors.toList());
	}
	
	public <T> List<T> queryListToEntity(String sql, Class<T> clazz) {
		return queryListToEntity(sql, clazz, null);
	}

	@SneakyThrows
	public <T> List<T> queryListToEntity(String sql, Class<T> clazz, Integer fetchSize) {
		QueryResponse response = queryBySql(sql, fetchSize);
		List<List<JsonData>> rows = response.rows();
		return rows.stream().map(e -> toEntity(response.columns(), e, clazz)).collect(Collectors.toList());
	}

	public Map<String, Object> queryForMap(String sql) {
		List<Map<String, Object>> list = queryForList(sql);
		if (list.size() > 1) {
			throw new RuntimeException("Incorrect result size: expected 1, actual " + list.size());
		}
		return list.size() == 0 ? null : list.get(0);
	}

	public static Map<String, Object> toMap(List<Column> columns, List<JsonData> row) {
		Map<String, Object> item = new HashMap<>();
		int size = columns.size();
		for (int i = 0; i < size; i++) {
			String key = columns.get(i).name();
			Object value = toBaseValue(row.get(i));
			item.put(key, value);
		}
		return item;
	}
	
	@SneakyThrows
	private <T> T toEntity(List<Column> columns, List<JsonData> row, Class<T> clazz) {
		T entity = clazz.newInstance();
		int size = columns.size();
		for (int i = 0; i < size; i++) {
			String key = columns.get(i).name();
			Field field = ReflectUtil.getField(clazz, key); // 此方式字段不存在，不会抛出异常
			if (field != null) {
				JsonData jsonData = row.get(i);
				Class<?> fieldClass = field.getType();
				Object value = jsonData.to(fieldClass);
				field.setAccessible(true);
				field.set(entity, value);
			}
		}
		return entity;
	}

	private static Object toBaseValue(JsonData jsonData) {
		Object value = null;
		JsonValue jsonValue = jsonData.toJson();
		switch (jsonValue.getValueType()) {
		case NULL:
			break;
		case STRING:
			value = jsonData.to(String.class);
			break;
		case NUMBER:
			value = jsonData.to(Number.class);
			break;
		case TRUE:
		case FALSE:
			value = jsonData.to(Boolean.class);
			break;

		default:
			value = jsonValue.toString();
			break;
		}
		return value;
	}

}
