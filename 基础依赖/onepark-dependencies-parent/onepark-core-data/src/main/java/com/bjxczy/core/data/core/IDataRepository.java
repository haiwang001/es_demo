package com.bjxczy.core.data.core;

import java.util.List;

public interface IDataRepository<T, ID> {
	
	/**
	 * 新增、更新
	 * @param model
	 * @return
	 */
	boolean save(T model);
	
	/**
	 * 根据ID删除数据
	 * @param id
	 * @return
	 */
	boolean remove(ID id);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	T findById(ID id);
	
	/**
	 * 查询全部数据
	 * @return
	 */
	List<T> list();
	
}
