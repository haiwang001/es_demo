package com.bjxczy.core.data.template;

import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SqlParser {
	
	@Autowired
	private NamedParameterJdbcTemplate ntl;
	
	public String merge(String template, Map<String, Object> templateParams, Map<String, String> parameterMap) {
		Velocity.init();
		VelocityContext context = new VelocityContext();
		if (templateParams != null) {
			for (Entry<String, Object> entry : templateParams.entrySet()) {
				context.put(entry.getKey(), entry.getValue());
			}
		}
		TableHelper th = new TableHelper();
		th.setSqlParamMap(templateParams);
		th.setParameterMap(parameterMap);
		context.put("th", th);
//		context.put("multiuser", userConfig);
		StringWriter w = new StringWriter();
		Velocity.evaluate(context, w, "log", template);
		return w.toString();
	}
	
	@Data
	public class TableHelper {
		
		private Map<String, Object> sqlParamMap;
		
		private Map<String, String> parameterMap;
		
		public boolean exists(String tableName) {
			String sql = null;
			Map<String, Object> m = new HashMap<String, Object>();
			if (tableName.contains(".")) {
				String[] tn = tableName.split("\\.");
				tableName = tn[1];
				m.put("owner", tn[0]);
				sql = "select 1 from all_tables "
						+ " where lower(table_name)=lower(:tableName) "
						+ " and lower(owner)=lower(:owner)";
			} else {
				sql = "select 1 from user_tables where lower(table_name)=lower(:tableName)";
			}
			m.put("tableName", tableName);
			List<Map<String, Object>> l = ntl.queryForList(sql, m);
			if (l != null) {
				return l.size() > 0;
			}
			return false;
		}
		
		public String nvl(String sqlParam, String text) {
			if (sqlParamMap != null) {
				Object param = (sqlParamMap.get(sqlParam));
				if (param == null || StringUtils.isEmpty(param.toString())) {
					return "";
				} else {
					return text;
				}
			}
			return text;
		}

		public List<String> dtb(int days, String pattern, String timeFormat) throws ParseException {
			List<String> result = new ArrayList<String>();

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(timeFormat);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			Set<String> set = new LinkedHashSet<>();

			for (int i = 0; i < days; i++) {
				set.add(simpleDateFormat.format(calendar.getTimeInMillis() - 1L*i*24*60*60*1000));
			}

			Iterator<String> iterator = set.iterator();
			while (iterator.hasNext()){
				result.add(pattern.replace("$T", iterator.next()));
			}

			return result;
		}
		
		public boolean isBlank(String str) {
			return StringUtils.isBlank(str);
		}
		
		public boolean isNotBlank(String str) {
			return !isBlank(str);
		}
		
		public boolean isNotNull(Object obj) {
			return obj != null;
		}
		
		public boolean sizeIsZero(List list) {
			return list.size() >0;
		}
		
		public boolean sizeEqualsZero(List list) {
			return list.size() == 0;
		}
		
		public String _parameter(String key) {
			if (null != parameterMap && parameterMap.containsKey(key)) {
				return parameterMap.get(key);
			} else {
				log.info("[TemplateSql] 参数[{}]不存在！", key);
			}
			return null;
		}
		
		/**
		 * 日期字符串转时间戳
		 * @param source 日期
		 * @param pattern 格式化：yyyy-MM-dd HH:mm:ss
		 * @return
		 */
		@SneakyThrows
		public Long toTimestamp(String source, String pattern) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			return sdf.parse(source).getTime();
		}
		
	}

}
