package com.bjxczy.core.data.template;

import java.util.concurrent.Executor;

import org.dom4j.DocumentException;

import com.alibaba.nacos.api.config.listener.Listener;

public class NacosSqlConfigListener implements Listener {
	
	private TemplateSqlContainer container;

	public NacosSqlConfigListener(TemplateSqlContainer container) {
		this.container = container;
	}

	@Override
	public Executor getExecutor() {
		return null;
	}

	@Override
	public void receiveConfigInfo(String configInfo) {
		try {
			container.putXmlTemplateSql(configInfo, true);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
