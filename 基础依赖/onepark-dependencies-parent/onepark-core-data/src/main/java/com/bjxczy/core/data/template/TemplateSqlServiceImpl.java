package com.bjxczy.core.data.template;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.bjxczy.core.data.configure.OneParkDataProperties;
import com.bjxczy.core.data.core.elasticsearch.ElasticsearchSqlTemplate;
import com.bjxczy.core.data.util.SqlUtils;
import com.bjxczy.onepark.common.context.TenantContextHolder;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.common.ApiResourceInformation;
import com.bjxczy.onepark.common.model.common.ExportWrapper;
import com.bjxczy.onepark.common.model.common.PageWrapper;
import com.bjxczy.onepark.common.model.common.TenantInfo;
import com.bjxczy.onepark.common.service.TemplateSqlService;

import cn.hutool.core.lang.Assert;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TemplateSqlServiceImpl implements TemplateSqlService {

	@Autowired
	private TemplateSqlContainer templateSqlContainer;
	@Autowired
	private OneParkDataProperties properties;
	@Autowired
	private SqlParser sqlParser;
	@Autowired
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	@Autowired(required = false)
	private ElasticsearchSqlTemplate esSqlTemplate;
	@Value("${spring.application.name}")
	public String applicationName;

	private String getSql(String sql, Map<String, Object> params, Map<String, String> parameterMap) {
		paramsCheck(params); // SQL注入校验
		TenantInfo tenant = TenantContextHolder.get(false);
		if (tenant != null) {
			params.put("tenant", tenant);
		}
		sql = sqlParser.merge(sql, params, parameterMap);
		if (properties.getTemplateSql().getShowSql()) {
			log.info("generate sql ->\n[{}]", SqlUtils.formatSql(sql));
		}
		return sql;
	}
	
	private void paramsCheck(Map<String, Object> params) {
		if (params == null) {
			return;
		}
		for (Entry<String, Object> entry : params.entrySet()) {
			Object value = entry.getValue();
			if (value instanceof String) {
				if (!SqlUtils.sqlParamCheck((String) value)) {
					throw new ResultException("非法字符！");
				}
			} else if (value instanceof Map) {
				paramsCheck((Map<String, Object>) value);
			}
		}
	}

	private TemplateSql getTemplateSql(String key) {
		TemplateSql templateSql = templateSqlContainer.get(key);
		Assert.notNull(templateSql);
		return templateSql;
	}
	
	private ElasticsearchSqlTemplate getEsSqlTemplate() {
		if (esSqlTemplate == null) {
			throw new ResultException("未启用Elasticsearch支持！");
		}
		return esSqlTemplate;
	}

	@Override
	public List<Map<String, Object>> list(String key, Map<String, Object> params) {
		TemplateSql templateSql = getTemplateSql(key);
		String sql = getSql(templateSql.getSql(), params, templateSqlContainer.getParameterMap(key));
		switch (templateSql.getNodeName()) {
		case "sql":
			return namedJdbcTemplate.queryForList(sql, params);
		case "elasticsearch":
			return getEsSqlTemplate().queryForList(sql);
		default:
			throw new RuntimeException("不支持查询类型！");
		}
	}

	@Override
	public PageWrapper<Map<String, Object>> page(String key, Integer pageNum, Integer pageSize,
			Map<String, Object> params) {
		TemplateSql templateSql = getTemplateSql(key);
		String sql = getSql(templateSql.getSql(), params, templateSqlContainer.getParameterMap(key)),
				countSql = SqlUtils.getCountSql(sql);
		Long total = 0L;
		List<Map<String, Object>> records = new ArrayList<>();
		Function<Map<String, Object>, Long> getCount = map -> Long.valueOf(map.get("COUNT").toString());
		switch (templateSql.getNodeName()) {
		case "sql":
			String pageSql = SqlUtils.getPageSql(sql);
			params.put(SqlUtils.PAGE_START_KEY, (pageNum - 1) * pageSize);
			params.put(SqlUtils.PAGE_END_KEY, pageSize);
			total = getCount.apply(namedJdbcTemplate.queryForMap(countSql, params));
			records = namedJdbcTemplate.queryForList(pageSql, params);
			break;
		case "elasticsearch":
			pageNum = 1; // elasticsearch不支持随机跳页
			total = getCount.apply(getEsSqlTemplate().queryForMap(countSql));
			records = getEsSqlTemplate().queryForList(sql, pageSize);
			break;
		default:
			throw new RuntimeException("不支持查询类型！");
		}
		PageWrapper<Map<String, Object>> data = new PageWrapper<>();
		data.setTotal(total);
		data.setPageNum(pageNum);
		data.setPageSize(pageSize);
		data.setPages((int) Math.ceil((double) data.getTotal() / pageSize));
		data.setRecords(records);
		return data;
	}

	@Override
	public ExportWrapper export(String key, Integer fetchSize, Map<String, Object> params) {
		TemplateSql templateSql = getTemplateSql(key);
		String sql = getSql(templateSql.getSql(), params, templateSqlContainer.getParameterMap(key));
		List<Map<String, Object>> records = new ArrayList<>();
		switch (templateSql.getNodeName()) {
		case "sql":
			String pageSql = SqlUtils.getPageSql(sql);
			params.put(SqlUtils.PAGE_START_KEY, 0);
			params.put(SqlUtils.PAGE_END_KEY, fetchSize);
			records = namedJdbcTemplate.queryForList(pageSql, params);
			break;
		case "elasticsearch":
			records = getEsSqlTemplate().queryForList(sql, fetchSize);
			break;
		default:
			throw new RuntimeException("不支持查询类型！");
		}
		return new ExportWrapper(templateSql.getName(), records, templateSql.getExportMap());
	}
	
	@Override
	public Optional<Map<String, Object>> queryForMap(String key, Map<String, Object> params) {
		List<Map<String, Object>> list = list(key, params);
		if (list == null || list.isEmpty()) {
			return Optional.ofNullable(null);
		} else if (list.size() > 1) {
			throw new RuntimeException("Incorrect result size: expected 1, actual " + list.size());
		}
		return Optional.of(list.get(0));
	}

	@Override
	public List<ApiResourceInformation> listApis() {
		List<ApiResourceInformation> apis = new ArrayList<>();
		for (Entry<String, TemplateSql> entry : templateSqlContainer.entrySet()) {
			TemplateSql value = entry.getValue();
			if (value != null && value.isResource()) {
				apis.addAll(value.toApiResources(entry.getKey(), applicationName));
			}
		}
		return apis;
	}

	@Override
	public Boolean isResource(String key, String path) {
		TemplateSql templateSql = templateSqlContainer.get(key);
		if (templateSql == null) {
			return null;
		}
		if (path != null && templateSql.isResource()) {
			return templateSql.getApis().contains(path);
		}
		return templateSql.isResource();
	}

}
