package com.bjxczy.core.alarm.configuration;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients("com.bjxczy.core.alarm.feign")
public class AlarmAutoConfigure {

}
