package com.bjxczy.core.alarm.feign;

import org.springframework.cloud.openfeign.FeignClient;

import com.bjxczy.core.feign.client.alarm.BaseAlarmFeignClient;

@FeignClient(value = "bmpalarm", contextId = "innerAlarmFeignClient")
public interface AlarmFeignClient extends BaseAlarmFeignClient {

}
