package com.bjxczy.core.alarm.handler;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.bjxczy.core.alarm.feign.AlarmFeignClient;
import com.bjxczy.core.rabbitmq.supports.AlarmBaseListener;
import com.bjxczy.onepark.common.model.alarm.CreateAlarmRecordCommand;
import com.bjxczy.onepark.common.model.alarm.UpdateAlarmStatusCommand;
import com.rabbitmq.client.Channel;

public abstract class AlarmOperatorHandler extends AlarmBaseListener {
	
	@Autowired
	protected AlarmFeignClient alarmFeignClient;

	/**
	 * 发送告警至告警中心
	 */
	public void pushAlarm(CreateAlarmRecordCommand command) {
		this.alarmFeignClient.addEventRecord(command);
	}
	
	/**
	 * 更新告警处理状态
	 * @param command
	 */
	public void updateAlarmStatus(UpdateAlarmStatusCommand command) {
		alarmFeignClient.updateAlarmStatus(command);
	}
	
	/**
	 * 告警中心处理状态回调
	 * @return
	 */
	public abstract boolean alarmHandled(UpdateAlarmStatusCommand command);

	@RabbitListener(bindings = { @QueueBinding(
			value = @Queue, // 临时队列
			key = ALARM_HANDLED_EVENT, 
			exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT) // 绑定的交换机
	)})
	@Override
	public void syncDeviceEvent(UpdateAlarmStatusCommand command, Channel channel, Message message) {
		this.confirm(() -> this.alarmHandled(command), channel, message);
	}
	
}
