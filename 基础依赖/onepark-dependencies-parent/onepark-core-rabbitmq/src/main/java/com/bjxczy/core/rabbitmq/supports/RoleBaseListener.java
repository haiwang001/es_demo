package com.bjxczy.core.rabbitmq.supports;

import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.core.rabbitmq.event.authority.RoleUpdatePayload;
import com.rabbitmq.client.Channel;

public class RoleBaseListener extends BaseRabbitListener {
	
	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "bmprole_default_exchange";

	public static final String ROLE_UPDATE_EVENT = "role_update_event";

	/**
	 * 同步设备列表
	 */
	public void roleUpdateHandler(RoleUpdatePayload role, Channel channel, Message message) {}

}
