package com.bjxczy.core.rabbitmq.event.door;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RemoveStaffAuthDTO implements Serializable {
	
	private static final long serialVersionUID = -8004615387880549532L;
	
	private Integer staffId;

}
