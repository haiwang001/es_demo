package com.bjxczy.core.rabbitmq.deadletter;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.Getter;
import lombok.Setter;

public abstract class DeadLetterHandler<T> {
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	@Setter
	@Getter
	private String exchange;
	
	public void add(T body) {
		rabbitTemplate.convertAndSend(exchange, getRoutingKey(), body);
	}

	public abstract String getRoutingKey();
	
	public abstract boolean onExpire(T body);
	
}
