package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

/**
 * @ClassName UserBaseListener
 * @Description
 * @Author zhanghongguo
 * @Date 2023/1/30 17:46
 * @Version 1.0
 */
public abstract class UserBaseListener extends BaseRabbitListener {
    /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "user_server_default_exchange";

    /**
     * 新增账号
     */
    public static final String CREATE_USER_EVENT = "create_user_event";
    public void createUserEventHandler(UserInformation information, Channel channel, Message message) {}
    /**
     * 删除账号
     */
    public static final String REMOVE_USER_EVENT = "remove_user_event";
    public void removeUserEventHandler(UserInformation information, Channel channel, Message message) {}
}
