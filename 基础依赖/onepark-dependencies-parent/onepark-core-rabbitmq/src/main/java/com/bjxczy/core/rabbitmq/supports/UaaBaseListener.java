package com.bjxczy.core.rabbitmq.supports;

import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.common.AccessToken;
import com.rabbitmq.client.Channel;

public abstract class UaaBaseListener extends BaseRabbitListener {
	
	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "uaa_default_exchange";

	/**
	 * 用户授权事件
	 */
	public static final String AUTHENTICATION_SUCCESS_EVENT = "authentication_success_event";
	public void authenticationSuccessEvent(AccessToken accessToken, Channel channel, Message message) {}

}
