package com.bjxczy.core.rabbitmq.event.door;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InOrOutRecordPayload  implements Serializable {

	private static final long serialVersionUID = 921533365693451364L;
	/**
	 * 业务类型
	 */
	private BusinessType businessType;
	
	/**
	 * 业务ID，关联业务类型：员工ID、访客ID
	 */
	private String businessId;
	
	/**
	 * 开门时间
	 */
	private Date time;
	
	/**
	 * 设备编码
	 */
	private String deviceCode;
	
	/**
	 * 抓拍图片（BASE64）
	 */
	private String eventPictrue;
	
	/**
	 * 体温
	 */
	private String animalHeat;
	
	/**
	 * 是否佩戴口罩：1 - 佩戴，2 - 未佩戴
	 */
	private String wearMask;

	/**
	 * 描述：事件类型（区分是什么事件，在业务模块做相应处理）
	 * 是否必填：
	 **/
	private String eventType;

	/**
	* 描述：时间类型为卡的时候，存放卡号
	* 是否必填：
	**/
	private String cardNum;

}
