package com.bjxczy.core.rabbitmq.supports;

import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.rabbitmq.client.Channel;

/**
 * @ClassName BuildingBaseListener
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/1 13:25
 * @Version 1.0
 */
public abstract class BuildingBaseListener extends BaseRabbitListener {
    /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "space_default_exchange";

    /**
     * 新增楼宇事件  routing key
     */
    public static final String CREATE_BUILDING_EVENT = "create_building_event";

    /**
     * 删除楼宇事件  routing key
     */
    public static final String REMOVE_BUILDING_EVENT = "remove_building_event";

    /**
     * 楼宇移动顺序事件  routing key
     */
    public static final String CHANGE_SORT_BUILDING_EVENT = "change_sort_building_event";
    
    /**
     * 空间变更事件
     */
    public static final String CHANGE_SPACE_EVENT = "change_space_event";
    public void changeSpaceEvent(String payload, Channel channel, Message message) {}

}
