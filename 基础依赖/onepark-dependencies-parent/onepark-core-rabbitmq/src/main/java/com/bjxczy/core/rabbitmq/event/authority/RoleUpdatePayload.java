package com.bjxczy.core.rabbitmq.event.authority;

import java.io.Serializable;
import java.util.Set;

import com.bjxczy.core.rabbitmq.base.BasePayload;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class RoleUpdatePayload extends BasePayload implements Serializable {
	
	private static final long serialVersionUID = 3750729348112871563L;
	
	private String id;
	
	private String name;
	
	private String description;
	
	private Set<String> permissions;
	
	private Set<String> dataPermissions;

}
