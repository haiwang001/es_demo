package com.bjxczy.core.rabbitmq.event.security;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

@Data
public class EventVssPayload implements Serializable {
	
	private static final long serialVersionUID = 5142021535606345353L;
	
	/**
	 * 事件ID
	 */
	private String eventId;
	
	/**
	 * 设备ID
	 */
	private String srcIndex;
	
	/**
	 * 事件类型码
	 */
	private String eventType;
	
	/**
	 * 事件状态
	 */
	private String status;
	
	/**
	 * 事件等级
	 */
	private String eventLvl;
	
	/**
	 * 事件发生时间
	 */
	@JSONField(deserialize = false)
	private Date happenTime;

}
