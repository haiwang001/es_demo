
package com.bjxczy.core.rabbitmq.rpc;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractRabbitAsyncConfigure implements ApplicationContextAware {

	@Value("${spring.application.name}")
	private String applicationName;
	@Getter
	private Map<String, RabbitAsyncHandler> handlers = new HashMap<>();

	@Bean
	public Queue receiveQueue() {
		return new Queue(applicationName + RabbitAsyncTypes.RECEIVE_QUEUE_SUFFIX);
	}

	@Bean
	public TopicExchange receiveExchange() {
		return new TopicExchange(applicationName + RabbitAsyncTypes.RECEIVE_EXCHANGE_SUFFIX);
	}

	@Bean
	public Binding msgBuilding() {
		return BindingBuilder.bind(receiveQueue()).to(receiveExchange())
				.with(applicationName + RabbitAsyncTypes.RECEIVE_ROUTER_KEY_SUFFIX);
	}

	@Bean
	public SimpleMessageListenerContainer messageListenerContainer(ConnectionFactory factory) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(factory);
		container.setQueues(receiveQueue());
		container.setMessageListener((MessageListener) message -> {
			MessageProperties properties = message.getMessageProperties();
			String key = properties.getHeader(RabbitAsyncTypes.HEADER_REPLY_KEY);
			if (null != key && this.handlers.containsKey(key)) {
				Object id = properties.getHeader(RabbitAsyncTypes.HEADER_REPLY_ID);
				this.handlers.get(key).receive(id, message);
			} else {
				log.error("[RabbitAsync] unkwon key={}", key);
			}
		});
		return container;
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.handlers = context.getBeansOfType(RabbitAsyncHandler.class);
	}

}
