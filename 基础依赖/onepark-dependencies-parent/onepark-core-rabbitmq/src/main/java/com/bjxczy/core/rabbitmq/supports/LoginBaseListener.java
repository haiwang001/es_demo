package com.bjxczy.core.rabbitmq.supports;

import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.core.rabbitmq.event.gateway.UserLoginPayload;
import com.bjxczy.core.rabbitmq.event.gateway.UserLogoutPayload;
import com.rabbitmq.client.Channel;

public class LoginBaseListener extends BaseRabbitListener {
	
	/**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "gateway_default_exchange";

    /**
     * 用户登录事件
     */
    public static final String USER_LOGIN_EVENT = "user_login_event";
    public void userLoginEvent(UserLoginPayload payload, Channel channel, Message message) {}
    
    /**
     * 用户登出事件
     */
    public static final String USER_LOGOUT_EVENT = "user_logout_event";
    public void userLogoutEvent(UserLogoutPayload payload, Channel channel, Message message) {}

}
