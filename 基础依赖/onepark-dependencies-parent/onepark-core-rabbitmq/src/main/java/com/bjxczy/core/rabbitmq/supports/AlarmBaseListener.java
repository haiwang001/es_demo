package com.bjxczy.core.rabbitmq.supports;

import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.alarm.UpdateAlarmStatusCommand;
import com.rabbitmq.client.Channel;

public abstract class AlarmBaseListener extends BaseRabbitListener {

	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "bmpalarm_default_exchange";

	public static final String ALARM_HANDLED_EVENT = "alarm_handled_event";

	/**
	 * 同步设备列表
	 */
	public void syncDeviceEvent(UpdateAlarmStatusCommand command, Channel channel, Message message) {}

}
