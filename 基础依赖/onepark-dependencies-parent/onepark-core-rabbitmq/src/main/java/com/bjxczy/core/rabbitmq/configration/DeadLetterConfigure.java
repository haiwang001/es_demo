package com.bjxczy.core.rabbitmq.configration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;


public abstract class DeadLetterConfigure {
	
	@Value("${spring.application.name}")
	private String applicationName;

	public static final String SEND_QUEUE_SUFFIX = "dl_send_queue";
	
	public static final String RECEIVE_QUEUE_SUFFIX = "dl_receive_queue";
	
	public static final String SEND_EXCHANGE_SUFFIX = "_dl_send_exchange";
	
	public static final String RECEIVE_EXCHANGE_SUFFIX = "_dl_receive_exchange";
	
	private String applicationNameOf(String suffix) {
		return applicationName + suffix;
	}
	
	@Bean(name = "sendExchange")
	public FanoutExchange defaultExchange() {
		return new FanoutExchange("");
	}
	
	@Bean(name = "sendQueue")
	public Queue sendQueue() {
		Queue queue = new Queue("");
		queue.addArgument("x-dead-letter-exchange", "");
		return queue;
	}
	
	@Bean(name = "sendBinding")
	public Binding sendBinding(@Qualifier("sendExchange") FanoutExchange exchange, @Qualifier("sendQueue") Queue queue) {
		return BindingBuilder.bind(queue).to(exchange);
	}
	
	@Bean(name = "receiveExchange")
	public FanoutExchange receiveExchange() {
		return new FanoutExchange("");
	}
	
	@Bean(name = "receiveQueue")
	public Queue receiveQueue() {
		return new Queue("");
	}
	
	@Bean(name = "receiveBinding")
	public Binding receiveBinding() {
		return BindingBuilder.bind(receiveQueue()).to(receiveExchange());
	} 
	
	@RabbitListener(queues = "")
	public void onMessage(String body, Message message) {
		System.out.println(body);
	}

}
