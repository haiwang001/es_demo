package com.bjxczy.core.rabbitmq.event.door;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddStaffAuthDTO implements Serializable {
	
	private static final long serialVersionUID = -6338718622744701396L;
	
	/**
	 * 员工ID
	 */
	private Integer staffId;
	
	/**
	 * 员工姓名
	 */
	private String staffName;
	
	/**
	 * 员工编号 
	 */
	private String employeeNo;
	
	/**
	 * 员工身份证号
	 */
	private String idCard;
	
	/**
	 * 门禁卡号
	 */
	private String accessCode;
	
	/**
	 * 人脸路径
	 */
	private String faceUrl;
	
	/**
	 * 三方平台ID
	 */
	private String uuid;
	
}
