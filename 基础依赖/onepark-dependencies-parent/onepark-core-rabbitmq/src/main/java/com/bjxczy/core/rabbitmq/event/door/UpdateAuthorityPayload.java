package com.bjxczy.core.rabbitmq.event.door;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateAuthorityPayload implements Serializable {
	
	private static final long serialVersionUID = -8254619041120167498L;

	/**
	 * 授权设备ID List
	 */
	private List<AuthorityDeviceDTO> doorList;
	
	/**
	 * 授权员工ID List
	 */
	private List<AddStaffAuthDTO> staffList;

}
