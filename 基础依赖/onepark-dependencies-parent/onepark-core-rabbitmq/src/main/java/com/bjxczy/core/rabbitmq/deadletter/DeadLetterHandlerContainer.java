package com.bjxczy.core.rabbitmq.deadletter;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.rabbit.annotation.RabbitListener;

public class DeadLetterHandlerContainer {
	
	public String exchange;
	
	private String deadLetterQueue;
	
	private Map<String, DeadLetterHandler<?>> handlerMap;

	public DeadLetterHandlerContainer(String exchange, String deadLetterQueue) {
		this.exchange = exchange;
		this.deadLetterQueue = deadLetterQueue;
		this.handlerMap = new HashMap<>();
	}

	public void addHandler(DeadLetterHandler<?> handler) {
		handler.setExchange(exchange);
		this.handlerMap.put(handler.getRoutingKey(), handler);
	}
	
	@RabbitListener()
	public void onMessage() {
		
	}

}
