package com.bjxczy.core.rabbitmq.rpc;

public interface RabbitAsyncTypes {
	
	/**
	 * header：回复交换机名称
	 */
	String HEADER_REPLY_EXCHANGE = "onepark_reply_exchange";
	
	/**
	 * header：回复路由名称
	 */
	String HEADER_REPLY_ROUTER = "onepark_reply_router";
	
	/**
	 * header：回复Key名称
	 */
	String HEADER_REPLY_KEY = "onepark_reply_key";
	
	/**
	 * header：回复ID名称
	 */
	String HEADER_REPLY_ID = "onepark_reply_id";
	
	String RECEIVE_EXCHANGE_SUFFIX = "_rpc_exchange";
	
	String RECEIVE_QUEUE_SUFFIX = "_rpc_receive_queue";
	
	String RECEIVE_ROUTER_KEY_SUFFIX = "_rpc_router_key";

}
