package com.bjxczy.core.rabbitmq.rpc;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;

public class AsyncReplyHandler {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	private MessageConverter messageConverter;

	public AsyncReplyHandler(MessageConverter messageConverter) {
		super();
		this.messageConverter = messageConverter;
	}

	/**
	 * RPC消息回复
	 * @param message
	 * @param data
	 */
	public void reply(Message message, Object data) {
		MessageProperties properties = message.getMessageProperties();
		String exchange = properties.getHeader(RabbitAsyncTypes.HEADER_REPLY_EXCHANGE),
				routingKey = properties.getHeader(RabbitAsyncTypes.HEADER_REPLY_ROUTER);
		if (exchange == null || routingKey == null) {
			throw new NullPointerException("exchange=" + exchange + ",routingKey=" + routingKey);
		}
		message = messageConverter.toMessage(data, properties);
		rabbitTemplate.convertAndSend(exchange, routingKey, message);
	}

}
