package com.bjxczy.core.rabbitmq.event.door;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorityResultPayload implements Serializable {
	
	private static final long serialVersionUID = -8788933963645675559L;

	/**
	 * 授权设备编码
	 */
	private String deviceCode;
	
	/**
	 * 授权员工ID
	 */
	private Integer staffId;
	
	/**
	 * 授权类型：0 - 新增授权，1 - 取消授权
	 */
	private Integer type;
	
	/**
	 * 授权是否成功
	 */
	private Boolean success;
	
	/**
	 * 描述
	 */
	private String message;

}
