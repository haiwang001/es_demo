package com.bjxczy.core.rabbitmq.event.gateway;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLogoutPayload implements Serializable {

	private static final long serialVersionUID = -378357197571045512L;

	private String username;

	private String clientId;

}
