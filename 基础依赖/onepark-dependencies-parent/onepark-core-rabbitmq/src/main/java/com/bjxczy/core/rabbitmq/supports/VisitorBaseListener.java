package com.bjxczy.core.rabbitmq.supports;


import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorInformation;
import com.bjxczy.onepark.common.model.visitor.DeleteVisitorInformation;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

/*
 *@Author wlwCreateVisitorVo
 *@Date 2023/6/24 10:32
 */
public class VisitorBaseListener extends BaseRabbitListener {

    public static final String DEFAULT_EXCAHNGE_NAME = "visitor_server_default_exchange";
    public static final String ADD_EVENT = "bmpVisitor_add_event";

    public static final String CREATE_VISITOR_EVENT = "create_visitor_event";

    public void createVisitor(CreateVisitorInformation create, Channel channel, Message message) {
    }


    public static final String DEL_VISITOR_EVENT_MSG = "del_visitor_event_msg";

    public void createVisitorMsg(CreateVisitorAndDoorVo msg, Channel channel, Message message) {
    }

    public static final String DEL_VISITOR_EVENT = "del_visitor_event";

    public void deleteVisitor(DeleteVisitorInformation delete, Channel channel, Message message) {
    }



}
