package com.bjxczy.core.rabbitmq.event.gateway;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginPayload implements Serializable {
	
	private static final long serialVersionUID = 5658226647087845370L;

	private String username;
	
	private String clientId;
	
}
