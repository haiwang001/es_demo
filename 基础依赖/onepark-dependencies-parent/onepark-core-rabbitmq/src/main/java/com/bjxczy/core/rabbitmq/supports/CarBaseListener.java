package com.bjxczy.core.rabbitmq.supports;

import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.car.CarCertificate;
import com.bjxczy.onepark.common.model.car.CarReservations;
import com.bjxczy.onepark.common.model.car.HikCarInfo;
import com.bjxczy.onepark.common.model.car.HikInAndOutRecord;
import com.bjxczy.onepark.common.model.car.ParkBlackWhiteListInfo;
import com.rabbitmq.client.Channel;

public abstract class CarBaseListener extends BaseRabbitListener {
    /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "car_default_exchange";

    /**
     * 新增车辆黑名单事件  routing key
     */
    public static final String CREATE_BLACKLIST_EVENT = "create_blackList_event";
    public void createBlackListEventHandler(ParkBlackWhiteListInfo info, Channel channel, Message message) {}
    /**
     * 新增车辆黑名单事件  routing key
     */
    public static final String REMOVE_BLACKLIST_EVENT = "remove_blackList_event";
    public void removeBlackListEventHandler(ParkBlackWhiteListInfo info, Channel channel, Message message) {}

    /**
     * 新增车辆白名单事件  routing key
     */
    public static final String CREATE_WHITELIST_EVENT = "create_whiteList_event";
    public void createWhiteListEventHandler(ParkBlackWhiteListInfo info, Channel channel, Message message) {}
    /**
     * 删除车辆白名单事件  routing key
     */
    public static final String REMOVE_WHITELIST_EVENT = "remove_whiteList_event";
    public void removeWhiteListEventHandler(ParkBlackWhiteListInfo info, Channel channel, Message message) {}
    /**
     * 修改车辆白名单事件  routing key
     */
    public static final String UPDATE_WHITELIST_EVENT = "update_whiteList_event";
    public void updateWhiteListEventHandler(ParkBlackWhiteListInfo info, Channel channel, Message message) {}

    /**
    * 描述：车辆出入记录事件 routing key
    **/
    public static final String IN_AND_OUT_RECORD_EVENT = "in_and_out_record_event";
    public void inAndOutRecordEventHandler(HikInAndOutRecord info,Channel channel, Message message){}

    /**
     * 描述：添加临时车辆事件 routing key
     **/
    public static final String ADD_CAR_RESERVATIONS_EVENT = "add_car_reservations_event";
    public void addCarReservationsHandler(CarReservations carReservations,Channel channel, Message message){}

    /**
     * 描述：删除临时车辆事件 routing key
     **/
    public static final String DEL_CAR_RESERVATIONS_EVENT = "del_car_reservations_event";
    public void delCarReservationsHandler(CarReservations carReservations,Channel channel, Message message){}

    /**
     * 描述：添加车辆事件 routing key
     **/
    public static final String ADD_HIK_CAR_EVENT = "add_hik_car_event";
    public void addHikCarHandler(HikCarInfo hikCarInfo, Channel channel, Message message){}

    /**
     * 描述：删除车辆事件 routing key
     **/
    public static final String DEL_HIK_CAR_EVENT = "del_hik_car_event";
    public void delHikCarHandler(HikCarInfo hikCarInfo,Channel channel, Message message){}

    /**
     * 描述：添加车证事件 routing key
     **/
    public static final String ADD_CAR_CERTIFICATE_EVENT = "add_car_certificate_event";
    public void addCarCertificateHandler(CarCertificate carCertificate, Channel channel, Message message){}

    /**
     * 描述：删除车证事件 routing key
     **/
    public static final String DEL_CAR_CERTIFICATE_EVENT = "del_car_certificate_event";
    public void delCarCertificateHandler(CarCertificate carCertificate,Channel channel, Message message){}

}
