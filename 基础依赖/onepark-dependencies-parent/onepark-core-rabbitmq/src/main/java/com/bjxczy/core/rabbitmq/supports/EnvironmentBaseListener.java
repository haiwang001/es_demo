package com.bjxczy.core.rabbitmq.supports;

import com.alibaba.fastjson.JSONArray;
import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.alarm.UpdateAlarmStatusCommand;
import com.bjxczy.onepark.common.model.device.DeviceStatus;
import com.bjxczy.onepark.common.model.environment.SyncDeviceAlarmData;
import com.bjxczy.onepark.common.model.environment.SyncDeviceInformation;
import com.bjxczy.onepark.common.model.environment.SyncMonitorDataInformation;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public abstract class EnvironmentBaseListener extends BaseRabbitListener {

	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "environment_default_exchange";

	/**
	 * 获取硬件监测设备在线状态
	 */
	 public static final String GET_MONITOR_STATUS_EVENT = "get_monitor_status_event";
	 public void getMonitorStatusEvent(Channel channel, Message message) {}

	/**
	 * 获取硬件监测设备告警情况
	 */
	public static final String GET_MONITOR_ALARM_EVENT = "get_monitor_alarm_event";
	public void getMonitorAlarmEvent(Channel channel, Message message) {}


	/**
	 * 从硬件获取实时监测数据
	 */
	public static final String GET_MONITOR_DATA_EVENT = "get_monitor_data_event";
	public void getMonitorDataEvent(Set<String> deviceCodes, Channel channel, Message message) {}


	/**
	 * 同步监测数据给环境模块
	 */
	public static final String SYNC_MONITOR_DATA_EVENT = "sync_monitor_data_event";
	public void syncMonitorDataEvent(Map<String, Object> map , Channel channel, Message message) {}


	/**
	 * 同步告警数据给环境模块
	 */
	public static final String SYNC_ALARM_DATA_EVENT = "sync_alarm_data_event";
	public void syncAlarmDataEvent(ArrayList<SyncDeviceAlarmData> resList , Channel channel, Message message) {}


	/**
	 * 同步硬件监测设备在线状态给子系统
	 */
	public static final String SYNC_MONITOR_STATUS_EVENT = "sync_monitor_status_event";
	public void syncMonitorStatusEvent(Map<String, Object> map , Channel channel, Message message) {}


	/*
	*/
/**
	 * 同步设备状态
	 *//*

	public static final String SYNC_DEVICE_STATUS_EVENT = "sync_device_status_event";
	public void syncDeviceStatusEvent(SyncDeviceInformation syncDeviceInformation, Channel channel, Message message) {}
*/


}
