package com.bjxczy.core.rabbitmq.event.security;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class EventFacePayload extends EventVssPayload implements Serializable {
	
	private static final long serialVersionUID = -6055857632079635885L;
	
	private String ageGroup;
	
	private String glass;
	
	private String bkgUrl;
	
	private String faceUrl;
	
	private String faceSource;
	
	private String faceGroupCode;
	
	private String faceGroupName;
	
	private String faceInfoCode;
	
	private String faceInfoSex;
	
	private String faceInfoName;
	
	private String similarity;
	
	private String facePicUrl;
	
}
