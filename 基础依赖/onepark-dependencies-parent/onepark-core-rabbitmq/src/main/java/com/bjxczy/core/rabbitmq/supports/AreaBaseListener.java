package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;

/**
 * @ClassName AreaBaseListener
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/1 9:41
 * @Version 1.0
 */
public abstract class AreaBaseListener extends BaseRabbitListener {
    /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "space_default_exchange";

    /**
     * 新增区域事件  routing key
     */
    public static final String CREATE_AREA_EVENT = "create_area_event";

    /**
     * 删除区域事件  routing key
     */
    public static final String REMOVE_AREA_EVENT = "remove_area_event";

    /**
     * 区域移动顺序事件  routing key
     */
    public static final String CHANGE_SORT_AREA_EVENT = "change_sort_area_event";


}
