package com.bjxczy.core.rabbitmq.base;

import java.util.function.Supplier;

import org.springframework.amqp.core.Message;

import com.rabbitmq.client.Channel;

import lombok.SneakyThrows;

public abstract class BaseRabbitListener {

	/**
	 * 消息确认机制
	 * @param func
	 * @param channel
	 * @param message
	 */
	@SneakyThrows
	protected void confirm(Supplier<Boolean> func, Channel channel, Message message) {
		long deliveryTag = message.getMessageProperties().getDeliveryTag();
		Boolean success = false;
		try {
			success = func.get();
		} catch (Exception e) {
			e.printStackTrace();
			// 消息处理异常，将消息重新加入队列
			channel.basicNack(deliveryTag, true, true);
		}
		if (success) {
			// 消息处理成功，确认该消息
			channel.basicAck(deliveryTag, true);
		} else {
			// 当返回false拒绝消息
			this.reject(channel, message);
		}
	}

	/**
	 * 拒绝消息
	 * @param channel
	 * @param message
	 */
	@SneakyThrows
	protected void reject(Channel channel, Message message) {
		long deliveryTag = message.getMessageProperties().getDeliveryTag();
		channel.basicReject(deliveryTag, false);
	}
	
}
