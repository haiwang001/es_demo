package com.bjxczy.core.rabbitmq.supports;


import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

/*
 *@Author wlw
 *@Date 2023/5/9 16:36 在园人数监听
 */
public abstract class ParkPeopleNumberListener extends BaseRabbitListener {

    /**
     * 默认交换机名称
     */
    public static final String EXCHANGE_NAME = "voucher_default_exchange";
    /**
     * 增加在园人数
     */
    public static final String ADD_PEOPLE = "ADD_PEOPLE";
    public void addPeopleEvent(String msg, Channel channel, Message message) {}
    /**
     * 减少在园人数
     */
    public static final String REDUCTION_PEOPLE = "REDUCTION_PEOPLE";
    public void reductionPeopleEvent(String msg, Channel channel, Message message) {}
}
