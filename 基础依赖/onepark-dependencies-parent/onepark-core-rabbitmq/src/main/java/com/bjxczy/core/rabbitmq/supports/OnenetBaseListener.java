package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.common.AccessToken;
import com.bjxczy.onepark.common.model.device.OnenetDeviceData;
import com.bjxczy.onepark.common.model.device.OnenetDeviceStatus;
import com.bjxczy.onepark.common.resp.OnenetBodyObj;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

import java.util.List;

public class OnenetBaseListener extends BaseRabbitListener {
    /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "onenet_default_exchange";

    /**
     * onenet推送设备数据事件
     *
     */
    public static final String PUSH_DATA_EVENT = "push_data_event";
    public void pushHumitureDataEvent(OnenetDeviceData body, Channel channel, Message message) {}

    public void pushLevelDataEvent(OnenetDeviceData body, Channel channel, Message message) {}
    public static final String PUSH_DATA_STATUS_EVENT = "push_data_status_event";
    public void pushOnenetDeviceStatusEvent(List<OnenetDeviceStatus> list, Channel channel, Message message) {}
}
