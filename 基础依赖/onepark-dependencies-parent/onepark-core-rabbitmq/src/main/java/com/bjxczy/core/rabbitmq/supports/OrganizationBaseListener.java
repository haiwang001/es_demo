package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.onepark.common.model.organization.OrganizationInformation;
import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.rabbitmq.client.Channel;

public abstract class OrganizationBaseListener extends BaseRabbitListener {

	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "orgaization_default_exchange";

	/**
	 * 新增用户事件  routing key
	 */
	public static final String CREATE_STAFF_EVENT = "create_staff_event";
	public void createStaffEventHandler(StaffInformation staffInfomation, Channel channel, Message message) {}


	/**
	 * 更新用户事件  routing key
	 */
	public static final String UPDATE_STAFF_EVENT = "update_staff_event";
	public void updateStaffEventHandler(StaffInformation staffInfomation, Channel channel, Message message) {}

	/**
	 * 删除用户事件  routing key
	 */
	public static final String REMOVE_STAFF_EVENT = "remove_staff_event";
	public void removeStaffEventHandler(RemoveStaffInfo RemoveStaffInfo, Channel channel, Message message) {}

	/**
	 * 新增部门事件  routing key
	 */
	public static final String CREATE_ORGANIZATION_EVENT = "create_organization_event";
	public void createOrganizationEventHandler(OrganizationInformation information, Channel channel, Message message) {}


	/**
	 * 更新部门事件  routing key
	 */
	public static final String UPDATE_ORGANIZATION_EVENT = "update_organization_event";
	public void updateOrganizationEventHandler(OrganizationInformation information, Channel channel, Message message) {}

	/**
	 * 删除部门事件  routing key
	 */
	public static final String REMOVE_ORGANIZATION_EVENT = "remove_organization_event";
	public void removeOrganizationEventHandler(OrganizationInformation information, Channel channel, Message message) {}


}
