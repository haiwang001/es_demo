package com.bjxczy.core.rabbitmq.event.door;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorityDeviceDTO implements Serializable {

    private static final long serialVersionUID = -2398384511693108189L;
    /**
     * 设备编码
     */
    private String deviceCode;
    /**
     * 设备类型 device_name
     */
    private String deviceName;
    /**
     * 设备类型
     */
    private String deviceType;
}
