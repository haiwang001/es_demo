package com.bjxczy.core.rabbitmq.base;

import java.io.Serializable;

import lombok.Data;

@Data
public class BasePayload implements Serializable {
	
	private static final long serialVersionUID = 3132848812187771244L;
	
	private String authorization;

}
