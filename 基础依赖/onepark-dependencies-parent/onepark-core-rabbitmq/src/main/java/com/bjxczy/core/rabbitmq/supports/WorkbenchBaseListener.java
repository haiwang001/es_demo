package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.workbench.DealMessageInformation;
import com.bjxczy.onepark.common.model.workbench.PushMessageInformation;
import com.bjxczy.onepark.common.model.workbench.TemplateMessagePayload;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

/**
 * @ClassName WorkbenchBaseListener
 * @Description
 * @Author zhanghongguo
 * @Date 2023/2/9 14:17
 * @Version 1.0
 */
public abstract class WorkbenchBaseListener extends BaseRabbitListener {

    /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "workbench_default_exchange";

    /**
     * 平台消息推送 routing key
     */
    public static final String PUSH_MESSAGE_EVENT = "push_message_event";
    public void pushMessage(PushMessageInformation information, Channel channel, Message message){};

    /**
     * 平台消息处理 routing key
     */
    public static final String DEAL_MESSAGE_EVENT = "deal_message_event";
    public void dealMessage(DealMessageInformation information, Channel channel, Message message){};

    /**
     * 模板消息处理 routing key
     */
    public static final String TEMPLTE_MESSAGE_EVENT = "template_message_event";
    public void templateMessageHandler(TemplateMessagePayload payload, Channel channel, Message message){};

}
