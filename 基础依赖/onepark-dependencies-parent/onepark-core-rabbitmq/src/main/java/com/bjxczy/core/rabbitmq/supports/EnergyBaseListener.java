package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;

public abstract  class EnergyBaseListener extends BaseRabbitListener {
    /**
	 * 默认交换机名称
	 */
    public static final String DEFAULT_EXCAHNGE_NAME = "bmpenergy_default_exchange";


    /**
     * key：由智慧网关订阅获取数据事件
     */
    public static final String GET_ElECTRIC_DATA_TASK_EVENT = "get_monitor_data_event";


}
