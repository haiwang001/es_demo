package com.bjxczy.core.rabbitmq.rpc;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import lombok.Getter;

public abstract class RabbitAsyncHandler implements InitializingBean {

	@Getter
	private String key;
	@Getter
	private String exchange;
	@Getter
	private String routerKey;

	@Autowired
	protected RabbitTemplate rabbitTemplate;
	@Autowired
	protected MessageConverter messageConverter;

	@Value("${spring.application.name}")
	private String applicationName;

	@Override
	public void afterPropertiesSet() throws Exception {
		RabbitAsyncComponent component = this.getClass().getAnnotation(RabbitAsyncComponent.class);
		if (component != null) {
			this.key = component.value();
			this.exchange = component.exchange();
			this.routerKey = component.routingKey();
		} else {
			throw new NullPointerException("@RabbitAsyncComponent未找到！");
		}
	}

	private MessageProperties getProperties() {
		MessageProperties properties = new MessageProperties();
		properties.setHeader("handlerKey", key);
		return properties;
	}

	/**
	 * rabbitmq发送消息
	 * @param id  业务ID
	 * @param obj  消息内容
	 */
	public void sendMessage(Object id, Object obj) {
		MessageProperties properties = getProperties();
		properties.setHeader(RabbitAsyncTypes.HEADER_REPLY_EXCHANGE,
				applicationName + RabbitAsyncTypes.RECEIVE_EXCHANGE_SUFFIX);
		properties.setHeader(RabbitAsyncTypes.HEADER_REPLY_ROUTER,
				applicationName + RabbitAsyncTypes.RECEIVE_ROUTER_KEY_SUFFIX);
		properties.setHeader(RabbitAsyncTypes.HEADER_REPLY_KEY, this.key);
		properties.setHeader(RabbitAsyncTypes.HEADER_REPLY_ID, id);
		Message message = messageConverter.toMessage(obj, properties);
		rabbitTemplate.convertAndSend(this.exchange, this.routerKey, message);
	}

	/**
	 * rabbitmq异步消息回复
	 * @param id  业务ID
	 * @param data  消息内容
	 */
	public abstract void receive(Object id, Message data);

}
