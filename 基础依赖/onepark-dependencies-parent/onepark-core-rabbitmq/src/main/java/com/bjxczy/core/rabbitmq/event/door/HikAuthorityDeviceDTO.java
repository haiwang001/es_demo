package com.bjxczy.core.rabbitmq.event.door;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
 *@Author wlw
 *@Date 2023/7/4 12:03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HikAuthorityDeviceDTO implements Serializable {
    private static final long serialVersionUID = -1665159025579912088L;
    /**
     * 设备编码
     */
    private String deviceCode;
    /**
     * 设备名称
     */
    private String deviceName;
}
