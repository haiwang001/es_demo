package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.core.rabbitmq.event.door.HikAuthorityPayload;
import com.bjxczy.core.rabbitmq.event.door.InOrOutRecordPayload;
import com.bjxczy.core.rabbitmq.event.door.RemoveAuthorityPayload;
import com.bjxczy.core.rabbitmq.event.door.UpdateAuthorityPayload;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

public abstract class DoorMutualBaseListener extends BaseRabbitListener {

    /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "bmpdoor_default_exchange";

    /**
     * 更新门禁授权
     */
    public static final String UPDATE_DOOR_CTRL_AUTHORIZATION = "update_door_ctrl_authorization";

    public void updateDoorCtrlEvent(UpdateAuthorityPayload payload, Channel channel, Message message) { };

    /**
     * 删除门禁授权
     */
    public static final String REMOVE_DOOR_CTRL_AUTHORIZATION = "remove_door_ctrl_authorization";

    public void removeDoorCtrlEvent(RemoveAuthorityPayload payload, Channel channel, Message message) { };

    /**
     * 海康门禁授权
     */
    public static final String HIK_DOOR_AUTHORIZATION = "hik_door_authorization";

    public void hikDoorCtrlEvent(HikAuthorityPayload payload, Channel channel, Message message) { };


    /**
     * 删除海康门禁授权
     */
    public static final String REMOVE_HIK_DOOR_CTRL_AUTHORIZATION = "remove_hik_door_authorization";

    public void ClearHikDoorAuthorizationEvent(HikAuthorityPayload payload, Channel channel, Message message) { };

    /**
     * 上报汉王通行记录
     */
    public static final String PUSH_IN_OR_OUT_RECORD = "push_in_or_out_record";

    public void pushInOrOutRecord(InOrOutRecordPayload payload, Channel channel, Message message) { };


}
