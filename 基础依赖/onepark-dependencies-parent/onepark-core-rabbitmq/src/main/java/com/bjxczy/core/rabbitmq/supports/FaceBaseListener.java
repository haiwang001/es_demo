package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.bjxczy.onepark.common.model.face.RecognitionFaceInfo;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

public abstract class FaceBaseListener extends BaseRabbitListener {


    /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "face_default_exchange";

    /***********************************安防监控**************************************/
    /**
     * 新增人脸库
     */
    public static final String CREATE_FACEGROUP_EVENT = "create_facegroup_event";

    public void createFacegroupEvent(String body, Channel channel, Message message) {
    }

    /**
     * 更新人脸库
     */
    public static final String UPDATE_FACEGROUP_EVENT = "update_facegroup_event";

    public void updateFacegroupEvent(String body, Channel channel, Message message) {
    }

    /**
     * 删除人脸库
     */
    public static final String REMOVE_FACEGROUP_EVENT = "remove_facegroup_event";

    public void removeFacegroupEvent(String body, Channel channel, Message message) {
    }

    /**
     * 新增人脸
     */
    public static final String CREATE_FACE_EVENT = "create_face_event";

    public void createFaceEvent(String body, Channel channel, Message message) {
    }

    /**
     * 更新人脸
     */
    public static final String UPDATE_FACE_EVENT = "update_face_event";

    public void updateFaceEvent(String body, Channel channel, Message message) {
    }

    /**
     * 删除人脸
     */
    public static final String REMOVE_FACE_EVENT = "remove_face_event";

    public void removeFaceEvent(String body, Channel channel, Message message) {
    }

    /***********************************END**************************************/

    /**
     * 凭证同步到海康
     */
    public static final String ADD_HIK_FACE_EVENT = "voucher_to_hik_add_face_sync_event";

    public void addHikFaceEvent(FaceInfoTransmit info, Channel channel, Message message) {
    }
    /**
     * 海康人脸修改
     */
    public static final String PUT_HIK_FACE_EVENT = "voucher_to_hik_put_face_sync_event";

    public void putHikFaceEvent(FaceInfoTransmit info, Channel channel, Message message) {
    }
    /**
     * 海康人脸删除
     */
    public static final String DEL_HIK_FACE_EVENT = "voucher_to_hik_del_face_sync_event";

    public void delHikFaceEvent(FaceInfoTransmit info, Channel channel, Message message) {
    }
    /**
     * 新增人脸  人脸 同步  凭证
     * img         图像路径
     * employeeNo  员工编号
     * fldName     员工名称
     * mobile      员工手机号
     * deptName    局址拼接
     * identityCard身份证号
     * staffType   员工类型
     * deptId      最终局址id
     * rankId      职级id
     */
    public static final String ADD_FACE_EVENT = "add_face_event";

    public void addFaceEvent(FaceInfoTransmit info, Channel channel, Message message) {
    }


    /**
     * 更新凭证人脸
     */
    public static final String PUT_FACE_EVENT = "put_face_event";

    public void putFaceEvent(FaceInfoTransmit info, Channel channel, Message message) {
    }


    /**
     * 删除凭证的人脸
     */
    public static final String DEL_FACE_EVENT = "del_face_event";

    public void delFaceEvent(FaceInfoTransmit info, Channel channel, Message message) {
    }


    /**
     * 添加重点人员识别计划
     */
    public static final String ADD_RECOGNITIONBLACKFACE_EVENT = "add_recognitionBlackFace_event";

    public void AddRecognitionBlackFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
    }

    /**
     * 修改重点人员识别计划
     */
    public static final String UPD_RECOGNITIONBLACKFACE_EVENT = "upd_recognitionBlackFace_event";

    public void UpdRecognitionBlackFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
    }

    /**
     * 删除重点人员识别计划
     */
    public static final String DEL_RECOGNITIONBLACKFACE_EVENT = "del_recognitionBlackFace_event";

    public void DelRecognitionBlackFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
    }

    /**
     * 添加陌生人员识别计划
     */
    public static final String ADD_RECOGNITIONSTRANGERFACE_EVENT = "add_recognitionStrangerFace_event";

    public void AddRecognitionStrangerFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
    }

    /**
     * 修改陌生人员识别计划
     */
    public static final String UPD_RECOGNITIONSTRANGERFACE_EVENT = "upd_recognitionStrangerFace_event";

    public void UpdRecognitionStrangerFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
    }

    /**
     * 删除陌生人员识别计划
     */
    public static final String DEL_RECOGNITIONSTRANGERFACE_EVENT = "del_recognitionStrangerFace_event";

    public void DelRecognitionStrangerFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
    }
}
