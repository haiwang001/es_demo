package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.onepark.common.model.workflow.AddEoMettingInfo;
import com.bjxczy.onepark.common.model.workflow.AddVehicleInfo;
import com.bjxczy.onepark.common.model.workflow.AddVisitsInfo;
import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.workflow.PushOrderPayload;
import com.rabbitmq.client.Channel;

public abstract class WorkflowBaseListener extends BaseRabbitListener {

	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "bmpworkflow_default_exchange";


	/**
	 * 验收完成工单 关联业务ID回调
	 */
	public static final String WORKORDER_BUSINESS_EVENT = "workorder_business_event";
	public void callbackBusiness(String data, Channel channel, Message message) {}

	/**
	 * 触发工单
	 */
	public static final String WORKORDER_TRIGGER_EVENT = "workorder_trigger_event";
	public void pushOrder(PushOrderPayload payload, Channel channel, Message message) {}

	/**
	 * 新增车辆EO单联动
	 */
	public static final String EOORDER_ADD_VEHICLE_INFO_EVENT="eoorder_add_vehicle_info_event";
	public void addVehicleInfo(AddVehicleInfo addVehicleInfo, Channel channel,Message message){}

	/**
	 * 新增访客EO单联动
	 */
	public static final String EOORDER_ADD_VISITS_INFO_EVENT="eoorder_add_visits_info_event";
	public void addVisitsInfo(AddVisitsInfo addVisitsInfo, Channel channel, Message message){}

	/**
	 * 新增会议EO单联动
	 */
	public static final String EOORDER_ADD_METTING_INFO_EVENT="eoorder_add_metting_info_event";
	public void addMettingInfo(AddEoMettingInfo addEoMettingInfo, Channel channel, Message message){}

}
