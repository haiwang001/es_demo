package com.bjxczy.core.rabbitmq.supports;

import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.rabbitmq.client.Channel;

public abstract class ResourceBaseListener extends BaseRabbitListener {
	
	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "bmpresource_default_exchange";
	
	/**
	 * API权限变更事件
	 */
	public static final String API_SCOPE_CHANGE = "api_scope_change";
	public void apiScopeChange(Object obj, Channel channel, Message message) {} 

}
