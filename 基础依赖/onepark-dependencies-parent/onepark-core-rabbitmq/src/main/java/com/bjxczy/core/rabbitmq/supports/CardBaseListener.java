package com.bjxczy.core.rabbitmq.supports;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.voucher.CardInfoTransmit;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;

public abstract class CardBaseListener extends BaseRabbitListener {
    /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "card_default_exchange";

    /**
     * 海康开卡
     */
    public static final String ADD_HIK_CARD_EVENT = "voucher_to_hik_add_card_sync_event";

    public void addHikCardEvent(CardInfoTransmit info, Channel channel, Message message) {
    }

    /**
     * 卡片删除
     */
    public static final String DEL_HIK_CARD_EVENT = "voucher_to_hik_del_card_sync_event";

    public void delHikCardEvent(CardInfoTransmit info, Channel channel, Message message) {
    }

}
