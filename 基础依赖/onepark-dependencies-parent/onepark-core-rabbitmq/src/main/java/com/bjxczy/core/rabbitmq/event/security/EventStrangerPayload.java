package com.bjxczy.core.rabbitmq.event.security;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class EventStrangerPayload extends EventVssPayload implements Serializable {
	
	private static final long serialVersionUID = 785210287042147524L;
	
	private String ageGroup;
	
	private String gender;
	
	private String glass;
	
	private String bkgUrl;
	
	private String faceUrl;
	
	private String faceTime;
	
}
