package com.bjxczy.core.rabbitmq.supports;

import java.util.Set;

import com.bjxczy.onepark.common.model.device.DeviceInfoAll;
import com.bjxczy.onepark.common.model.device.OnenetDeviceData;
import com.bjxczy.onepark.common.model.device.SyncSubDeviceInfo;
import com.bjxczy.onepark.common.resp.OnenetBodyObj;
import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.rabbitmq.client.Channel;

public abstract class DeviceBaseListener extends BaseRabbitListener {

	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "bmpdevice_default_exchange";

	/**
	 * 增加设备
	 */
	public static final String ADD_DEVICE_EVENT="add_device_event";
	public void addDeviceEvent(SyncSubDeviceInfo syncSubDeviceInfo, Channel channel, Message message){};

	/**
	 * 编辑设备
	 */
	public static final String EDIT_DEVICE_EVENT="edit_device_event";
	public void editDeviceEvent(SyncSubDeviceInfo syncSubDeviceInfo, Channel channel, Message message){};

	/**
	 * 删除设备
	 */
	public static final String REMOVE_DEVICE_EVENT="remove_device_event";
	public void removeDeviceEvent(String deviceCode, Channel channel, Message message){};


	/**
	 * 同步设备列表
	 */
	public static final String SYNC_DEVICE_EVENT = "sync_device_event";
	public void syncDeviceEvent(String body, Channel channel, Message message) {}

	/**
	 * 同步设备状态
	 */
	public static final String SYNC_DEVICE_STATUS_EVENT = "sync_device_status_event";

	/**
	 * 删除设备分组
	 */
	public static final String DEL_DEVICE_GROUP="del_device_group";

	/**
	 * 更新设备分组
	 */
	public static final String EDIT_DEVICE_GROUP="edit_device_group";

	/**
	 * 绑定设备空间
	 */

	public static final String EDIT_DEVICE_SPACE="edit_device_space";
	public void editDeviceSpace(DeviceInfoAll deviceInfoAll, Channel channel, Message message) { }

	/**
	 * 解绑设备空间
	 */

	public static final String UNBIND_DEVICE_SPACE="unbind_device_space";
	public void unbindDeviceSpace(String deviceCode, Channel channel, Message message) { }

	public void syncDeviceStatusEvent(Set<String> ids, Channel channel, Message message) {}


	/**
	 * onenet水表设备key
	 */
	public static final String ENERGY_WATER_DATA_EVENT = "push_data_water_device_event";






}
