package com.bjxczy.core.rabbitmq.supports;

import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.onepark.common.model.common.LogInfo;
import com.rabbitmq.client.Channel;

public class PcsBaseListener extends BaseRabbitListener {
	
	/**
	 * 默认交换机名称
	 */
	public static final String DEFAULT_EXCAHNGE_NAME = "pcs_default_exchange";

	public static final String SAVE_LOG_EVENT = "save_log_event";

	/**
	 * 保存日志
	 */
	public void saveLogEvent(LogInfo command, Channel channel, Message message) {}

}
