package com.bjxczy.core.rabbitmq.supports;

import java.util.List;

import org.springframework.amqp.core.Message;

import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.core.rabbitmq.event.security.EventFacePayload;
import com.bjxczy.core.rabbitmq.event.security.EventStrangerPayload;
import com.bjxczy.core.rabbitmq.event.security.EventVssPayload;
import com.rabbitmq.client.Channel;

public abstract class SecurityBaseListener extends BaseRabbitListener {
	
	 /**
     * 默认交换机名称
     */
    public static final String DEFAULT_EXCAHNGE_NAME = "security_default_exchange";

    /**
     * AI视频事件
     */
    public static final String VSS_EVENT = "vss_event";
	public void vssEventHandler(List<EventVssPayload> list, Channel channel, Message message) {} 
    
    /**
     * HIK - 人脸布控事件：重点人员识别
     */
    public static final String FACE_EVENT = "face_event";
    public void faceEventHandler(List<EventFacePayload> list, Channel channel, Message message) {}
    
    /**
     * HIK - 人脸布控事件：陌生人识别
     */
    public static final String STANGER_EVENT = "stanger_event";
    public void stangerEventHandler(List<EventStrangerPayload> list, Channel channel, Message message) {}

}
