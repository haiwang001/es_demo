package com.bjxczy.core.rabbitmq.event.door;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/7/4 11:57
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HikAuthorityPayload implements Serializable {
    private static final long serialVersionUID = -4559504739276470921L;
    private List<String> staffList;
    private List<HikAuthorityDeviceDTO> doorList;
}
