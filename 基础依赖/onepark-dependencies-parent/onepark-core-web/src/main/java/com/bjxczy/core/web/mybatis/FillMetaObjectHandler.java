package com.bjxczy.core.web.mybatis;

import java.util.Date;

import org.apache.ibatis.reflection.MetaObject;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.bjxczy.onepark.common.context.LoginUserContextHolder;

public class FillMetaObjectHandler implements MetaObjectHandler {

	@Override
	public void insertFill(MetaObject metaObject) {
		Date now = new Date();
		metaObject.setValue("createTime", now);
		metaObject.setValue("updateTime", now);
		setOpetator(metaObject);
	}

	@Override
	public void updateFill(MetaObject metaObject) {
		Date now = new Date();
		metaObject.setValue("updateTime", now);
		setOpetator(metaObject);
	}
	
	private void setOpetator(MetaObject metaObject) {
		String operator = LoginUserContextHolder.getValue(u -> u.getStaffName());
		// 读取登录用户信息，并设置操作人
		if (operator != null) {
			metaObject.setValue("operator", operator);
		}
	}

}
