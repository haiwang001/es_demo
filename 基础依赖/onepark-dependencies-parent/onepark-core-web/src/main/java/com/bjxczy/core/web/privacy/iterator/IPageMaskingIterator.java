package com.bjxczy.core.web.privacy.iterator;

import java.util.Iterator;

import com.baomidou.mybatisplus.core.metadata.IPage;

public class IPageMaskingIterator implements MaskingIterator {

	private Iterator<?> interator;

	@Override
	public MaskingIterator set(Object obj) {
		if (obj instanceof IPage) {
			IPage<?> data = (IPage<?>) obj;
			this.interator = data.getRecords().iterator();
		} else {
			throw new IllegalArgumentException(
					"参数类型错误，期望[com.baomidou.mybatisplus.core.metadata.IPage]，实际[" + obj.getClass() + "]");
		}
		return this;
	}

	@Override
	public Object next() {
		return this.interator.next();
	}

	@Override
	public boolean hasNext() {
		return this.interator.hasNext();
	}

}
