package com.bjxczy.core.web.dict;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.model.common.DictInformation;

@ApiResourceController
public class DictController {

	@Autowired
	private DictService dictService;

	@GetMapping(value = "/dict/list/{groupCode}", name = "[通用]字典查询")
	public List<DictInformation> listDict(@PathVariable("groupCode") String groupCode) {
		return dictService.listDict(groupCode);
	}

	@GetMapping(value = "/dict/map/{groupCode}", name = "[通用]字典Map查询")
	public Map<String, DictInformation> valueKeyMap(@PathVariable("groupCode") String groupCode) {
		return dictService.valueKeyMap(groupCode);
	}

}
