package com.bjxczy.core.web.interceptor;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

import com.bjxczy.onepark.common.constant.SecurityContant;
import com.bjxczy.onepark.common.context.LoginUserContextHolder;
import com.bjxczy.onepark.common.model.user.UserInformation;

import cn.hutool.core.net.URLDecoder;

public class UserInformationInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String userId = request.getHeader(SecurityContant.SYS_USER_ID);
		String username = request.getHeader(SecurityContant.SYS_USERNAME);
		String staffName = request.getHeader(SecurityContant.SYS_STAFF_NAME);
		String roles = request.getHeader(SecurityContant.SYS_USER_ROLES);
		if (userId != null || username != null || staffName != null || roles != null) {
			UserInformation user = new UserInformation(Integer.parseInt(userId), username,
					URLDecoder.decode(staffName, Charset.forName("UTF-8")));
			// 设置角色信息
			user.setRoles(Arrays.asList(roles.split(";")).stream().collect(Collectors.toSet()));
			LoginUserContextHolder.set(user);
		}
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}

}
