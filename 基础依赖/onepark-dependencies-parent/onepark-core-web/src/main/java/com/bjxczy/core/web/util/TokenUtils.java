package com.bjxczy.core.web.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


public class TokenUtils {

	public static String getAccessToken() {
		String value = getHeaderValue();
		if (StringUtils.isNotBlank(value) && StringUtils.startsWith(value, "Bearer")) {
			return value.substring(value.indexOf(" ") + 1);
		}
		return null;
	}

	public static String getHeaderValue() {
		return getRequest().getHeader(HttpHeaders.AUTHORIZATION);
	}

	public static HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}
	
}
