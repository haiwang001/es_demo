package com.bjxczy.core.web.base;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import com.bjxczy.onepark.common.exception.ResultException;

public class ValidtorHelper {
	
	public static final String DEFAULT_ERRORS_SPLIT = "，";
	
	private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator(); 
	
	public static void validate(Object obj) {
		validate(obj, DEFAULT_ERRORS_SPLIT);
	}
	
	public static void validate(Object obj, String errorsSplit) {
		Set<ConstraintViolation<Object>> errors = VALIDATOR.validate(obj);
		if (null != errors && errors.size() > 0) {
			String message = errors.stream().map(e -> e.getMessage()).collect(Collectors.joining(errorsSplit));
			throw new ResultException(message);
		}
	}

}
