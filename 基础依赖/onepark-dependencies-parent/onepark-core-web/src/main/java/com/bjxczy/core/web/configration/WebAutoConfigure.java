package com.bjxczy.core.web.configration;

import com.bjxczy.core.web.ioc.IOCMessageService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bjxczy.core.web.dict.DictController;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.handler.ResponseDataEncryptHandler;
import com.bjxczy.core.web.handler.WebExceptionHandler;
import com.bjxczy.core.web.log.SysLogAspect;
import com.bjxczy.core.web.message.MessageService;
import com.bjxczy.core.web.privacy.PrivacyConfigService;
import com.bjxczy.core.web.properties.OneParkLogProperties;
import com.bjxczy.core.web.properties.OneParkPrivacyProperties;
import com.bjxczy.core.web.template.TemplateResController;
import com.bjxczy.core.web.template.TemplateSqlController;
import com.bjxczy.core.web.workflow.IWorkflowService;
import com.bjxczy.core.web.workflow.WorkOrderService;
import com.bjxczy.core.web.workflow.WorkflowController;
import com.bjxczy.onepark.common.service.TemplateSqlService;

@Configuration
public class WebAutoConfigure {

    @Bean
    public ResponseDataEncryptHandler responseDataEncryptHandler() {
        return new ResponseDataEncryptHandler();
    }

    @Bean
    public WebExceptionHandler webExceptionHandler() {
        return new WebExceptionHandler();
    }

    @Bean
    @ConditionalOnProperty(prefix = "onepark.privacy", name = "enable", havingValue = "true", matchIfMissing = false)
    public PrivacyConfigService privacyConfigService() {
        return new PrivacyConfigService();
    }

    @Bean
    @ConditionalOnProperty(prefix = "onepark.privacy", name = "enable", havingValue = "true", matchIfMissing = false)
    public OneParkPrivacyProperties privacyProperties() {
        return new OneParkPrivacyProperties();
    }

    @Bean
    @ConditionalOnBean({DictService.class})
    public DictController dictController() {
        return new DictController();
    }

    @Bean
    @ConditionalOnBean({TemplateSqlService.class})
    public TemplateSqlController templateSqlController() {
        return new TemplateSqlController();
    }

    @Bean
    @ConditionalOnBean({TemplateSqlService.class})
    public TemplateResController templateResController() {
        return new TemplateResController();
    }

    @Bean
    @ConditionalOnProperty(prefix = "onepark.log", name = "enable", havingValue = "true", matchIfMissing = false)
    public OneParkLogProperties oneparkLogProperites() {
        return new OneParkLogProperties();
    }

    @Bean
    @ConditionalOnProperty(prefix = "onepark.log", name = "enable", havingValue = "true", matchIfMissing = false)
    public SysLogAspect sysLogAspect() {
        return new SysLogAspect();
    }

    @Configuration
    @ConditionalOnClass({org.springframework.amqp.rabbit.core.RabbitTemplate.class})
    class RabbitServiceInitializerConfig {

        @Bean
        @ConditionalOnClass({org.springframework.amqp.rabbit.core.RabbitTemplate.class})
        public MessageService messageService() {
            return new MessageService();
        }

        @Bean
        @ConditionalOnClass({org.springframework.amqp.rabbit.core.RabbitTemplate.class})
        public IOCMessageService iocMessageService() {
            return new IOCMessageService();
        }

        @Bean
        @ConditionalOnClass({org.springframework.amqp.rabbit.core.RabbitTemplate.class})
        public WorkOrderService workOrderService() {
            return new WorkOrderService();
        }

    }

    @Bean
    @ConditionalOnBean({IWorkflowService.class})
    public WorkflowController workflowController() {
        return new WorkflowController();
    }

}
