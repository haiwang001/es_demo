package com.bjxczy.core.web.privacy;

import lombok.Getter;

public enum MaskingOperation {
	
	QUERY("query"),
	EXPORT("export");
	
	@Getter
	private String value;

	private MaskingOperation(String value) {
		this.value = value;
	}

}
