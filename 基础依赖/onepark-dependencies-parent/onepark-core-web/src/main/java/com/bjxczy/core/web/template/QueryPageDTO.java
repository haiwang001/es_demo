package com.bjxczy.core.web.template;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class QueryPageDTO extends QueryDTO {
	
	private Integer pageNum = 1;
	
	private Integer pageSize = 10;

}
