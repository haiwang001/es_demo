package com.bjxczy.core.web.base;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.annotation.Id;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.core.web.mybatis.DefaultAssemlerUtils;

import cn.hutool.core.util.ReflectUtil;
import javassist.NotFoundException;

public abstract class MyBatisPlusRepositoryImpl<M extends BaseMapper<T>, T, U extends AbstractAggregateRoot<U>> extends ServiceImpl<M, T>
		implements AbstractRepository<U>, InitializingBean {
	
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	protected Class<U> modelClass;

	protected Class<T> entityClass;
	
	protected Field modelId;

	public T toPO(U source) {
		return DefaultAssemlerUtils.copyProperties(source, entityClass);
	}

	public U toModel(T source) {
		return DefaultAssemlerUtils.copyProperties(source, modelClass);
	}
	
	@Override
	public AbstractRepository<U> getImpl() {
		return this;
	}

	@Override
	public Boolean defaultSaveModel(U model) {
		// 校验参数是否合法
		model.validate();
		T po = toPO(model);
		boolean bool = this.saveOrUpdate(po);
		Object idVal = getId(po); // 获取ID值
		ReflectUtil.setFieldValue(model, modelId, idVal);
		if (bool) {
			// 发布领域事件
			publishEvent(model);
		}
		return bool;
	}

	@Override
	public Optional<U> findById(Serializable id) {
		T po = this.getById(id);
		if (po == null) {
			return Optional.ofNullable(null);
		}
		return Optional.of(toModel(this.getById(id)));
	}
	
	@Override
	public Boolean defaultRemove(U model) {
		T po = toPO(model);
		boolean bool = super.removeById(getId(po));
		if (bool) {
			// 注册聚合删除事件
			model.andRemoveEvent();
			// 发布领域事件
			publishEvent(model);
		}
		return bool;
	}
	
	private void publishEvent(U model) {
		Collection<Object> domainEvents = model.domainEvents();
		for (Object event : domainEvents) {
			applicationEventPublisher.publishEvent(event);
		}
		model.clearDomainEvents();
	}
	
	public List<U> listToModel(List<T> list) {
		return list.stream().map(e -> toModel(e)).collect(Collectors.toList());
	}
	
	private Serializable getId(T po) {
		TableInfo tableInfo = TableInfoHelper.getTableInfo(entityClass);
		Object idVal = ReflectionKit.getFieldValue(po, tableInfo.getKeyProperty());
		return (Serializable) idVal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		entityClass = (Class<T>) ReflectionKit.getSuperClassGenericType(getClass(), 1);
		modelClass = (Class<U>) ReflectionKit.getSuperClassGenericType(getClass(), 2);
		Field[] fields = ReflectUtil.getFields(modelClass);
		for (Field field : fields) {
			Id anno = field.getAnnotation(Id.class);
			if (anno != null) {
				modelId = field;
				return;
			}
		}
		throw new NotFoundException("model need @id Annotation.");
	}

}
