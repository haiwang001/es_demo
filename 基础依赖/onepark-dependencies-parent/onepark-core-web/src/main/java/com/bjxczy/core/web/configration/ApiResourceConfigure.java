package com.bjxczy.core.web.configration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.bjxczy.core.web.api.ApiResourcesServletInitializer;
import com.bjxczy.core.web.api.IApiResourceStore;
import com.bjxczy.core.web.api.NacosResourceStore;
import com.bjxczy.core.web.properties.OneParkApiProperties;
import com.bjxczy.onepark.common.service.TemplateSqlService;

@Configuration
public class ApiResourceConfigure {
	
	@Autowired
	private NacosConfigManager nacosConfigManager;
	@Autowired(required = false)
	private TemplateSqlService templateSqlService;
	
	@Bean
	public IApiResourceStore apiStore() {
		return new NacosResourceStore(nacosConfigManager);
	}
	
	@Configuration
	@ConditionalOnClass(RequestMappingHandlerMapping.class)
	@AutoConfigureAfter({ RequestMappingHandlerMapping.class })
	class ApiResourcesServletInitializerConfig {
		@Autowired(required = false)
		private org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping requestMappingHandlerMapping;
		
		@Bean
		public OneParkApiProperties oneparkApiProperties() {
			return new OneParkApiProperties();
		}

		@Bean
		public ApiResourcesServletInitializer apiResourcesServletInitializer() {
			return new ApiResourcesServletInitializer(requestMappingHandlerMapping, templateSqlService, apiStore(), oneparkApiProperties());
		}
	}

}
