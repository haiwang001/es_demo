package com.bjxczy.core.web.privacy.iterator;

/**
 * 单个元素迭代器
 * @author zzz
 *
 */
public class SingletonMaskingIterator implements MaskingIterator {

	private Object bean;

	@Override
	public MaskingIterator set(Object obj) {
		this.bean = obj;
		return this;
	}

	@Override
	public Object next() {
		Object temp = bean;
		this.bean = null;
		return temp;
	}

	@Override
	public boolean hasNext() {
		return this.bean != null;
	}

}
