package com.bjxczy.core.web.ioc;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.supports.IOCBaseListener;
import com.bjxczy.onepark.common.model.ioc.PushIOCMessageInformation;
import com.bjxczy.onepark.common.model.ioc.TemplateIOCMessagePayload;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName IocMessageService
 * @Description
 * @Author zhanghongguo
 * @Date 2023/6/26 14:04
 * @Version 1.0
 */
public class IOCMessageService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 推送平台消息
     *
     * @param information
     */
    public void pushMessage(PushIOCMessageInformation information) {
        String infoStr = JSONObject.toJSONString(information);
        this.rabbitTemplate.convertAndSend(IOCBaseListener.DEFAULT_EXCHANGE_NAME,
                IOCBaseListener.PUSH_MESSAGE_EVENT, infoStr);
    }
}
