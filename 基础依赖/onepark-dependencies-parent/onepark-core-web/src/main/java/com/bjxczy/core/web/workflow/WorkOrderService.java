package com.bjxczy.core.web.workflow;

import java.util.List;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.bjxczy.core.rabbitmq.supports.WorkflowBaseListener;
import com.bjxczy.onepark.common.model.workflow.DescribeItem;
import com.bjxczy.onepark.common.model.workflow.PushOrderPayload;

public class WorkOrderService extends WorkflowBaseListener {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	/**
	 * 联动工单接口
	 * @param id 工单配置ID
	 * @param description 工单详述
	 */
	public void pushOrder(String id, String description) {
		this.pushOrder(new PushOrderPayload(id, description));
	}

	/**
	 * 联动工单接口
	 * @param id 工单配置ID
	 * @param description 工单详述
	 * @param items 联动字段
	 */
	public void pushOrder(String id, String description, List<DescribeItem> items) {
		this.pushOrder(new PushOrderPayload(id, description, items));
	}
	
	public void pushOrder(PushOrderPayload payload) {
		rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME, WORKORDER_TRIGGER_EVENT, payload);
	}

}
