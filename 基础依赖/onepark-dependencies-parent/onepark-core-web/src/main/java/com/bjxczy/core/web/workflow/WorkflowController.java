package com.bjxczy.core.web.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bjxczy.onepark.common.resp.R;

@RestController
public class WorkflowController {
	
	public static final String VAILDATE_PATH = "/rpc/process/validated";
	
	public static final String APPROVED_PATH = "/rpc/process/approved";
	
	@Autowired
	private IWorkflowService workflowService;
	
	@PostMapping(value = VAILDATE_PATH + "/{key}")
	public R<?> validated(@PathVariable("key") String key, @RequestBody ProcessPayload payload) {
		return workflowService.validated(key, payload);
	}
	
	@PostMapping(value = APPROVED_PATH + "/{key}")
	public R<?> approved(@PathVariable("key") String key, @RequestBody ProcessPayload payload) {
		return workflowService.approved(key, payload);
	}

}
