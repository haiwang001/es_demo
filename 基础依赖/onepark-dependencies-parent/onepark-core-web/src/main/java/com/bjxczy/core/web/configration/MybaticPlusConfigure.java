package com.bjxczy.core.web.configration;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.bjxczy.core.web.mybatis.FillMetaObjectHandler;
import com.bjxczy.core.web.mybatis.UserOperatorInterceptor;

@Configuration
@ConditionalOnBean(DataSource.class)
public class MybaticPlusConfigure {
	
	@Bean
	public MybatisPlusInterceptor mybatisPlusInterceptor() {
		MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
		// 自定义拦截器
		interceptor.addInnerInterceptor(new UserOperatorInterceptor());
		// 自带分页拦截器
		interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
		return interceptor;
	}
	
	@Bean
	public MetaObjectHandler metaObjectHandler() {
		return new FillMetaObjectHandler();
	}

}
