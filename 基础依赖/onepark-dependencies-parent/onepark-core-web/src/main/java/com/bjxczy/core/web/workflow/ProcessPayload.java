package com.bjxczy.core.web.workflow;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcessPayload {
	
	private Integer staffId;
	
	private Map<String, Object> variables;

}
