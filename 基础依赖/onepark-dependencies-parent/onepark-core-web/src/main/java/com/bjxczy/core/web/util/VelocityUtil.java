package com.bjxczy.core.web.util;

import java.io.StringWriter;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class VelocityUtil {
	
	public static String evaluate(String template, VelocityContext context) {
		Velocity.init();
		StringWriter out = new StringWriter();
		Velocity.evaluate(context, out, "mytemplate", template);
		return out.toString();
	}

}
