package com.bjxczy.core.web.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.privacy.PrivacyConfigService;
import com.bjxczy.core.web.privacy.ResponseMasking;
import com.bjxczy.onepark.common.resp.R;

@ControllerAdvice(annotations = { RestController.class, ApiResourceController.class })
public class ResponseDataEncryptHandler implements ResponseBodyAdvice<Object> {

	@Autowired(required = false)
	private PrivacyConfigService privacyConfigService;

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		// 统一脱敏处理
		if (privacyConfigService != null) {
			ResponseMasking dataMasking = returnType.getMethodAnnotation(ResponseMasking.class);
			privacyConfigService.doMasking(dataMasking, body);
		}
		if (body instanceof R) {
			return body;
		}
		// 统一封装数据
		return R.success(body);
	}

}
