package com.bjxczy.core.web.interceptor;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.bjxczy.core.feign.client.user.BaseUserFeignClient;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.context.LoginUserContextHolder;
import com.bjxczy.onepark.common.model.user.UserInformation;

public class LoginUserArgumentResolver implements HandlerMethodArgumentResolver {
	
	@Autowired
	private BaseUserFeignClient userFeignClient;
	@Value("${spring.profiles.active}")
	private String env;
	
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(LoginUser.class) && parameter.getParameterType().equals(UserInformation.class);
	}

	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		LoginUser loginUser = parameter.getParameterAnnotation(LoginUser.class);
        boolean isFull = loginUser.isFull();
        UserInformation user = LoginUserContextHolder.get();
        if (isFull && user != null) {
        	user = userFeignClient.findById(user.getId());
        }
        // dev 环境可通过URL传递userId
        if (Objects.equals(env, "dev")) {
        	String userId = webRequest.getParameter("userId");
        	if (StringUtils.isNotBlank(userId)) {
        		user = userFeignClient.findById(Integer.parseInt(userId));
        	}
        }
        LoginUserContextHolder.set(user);
    	return user;
	}

}
