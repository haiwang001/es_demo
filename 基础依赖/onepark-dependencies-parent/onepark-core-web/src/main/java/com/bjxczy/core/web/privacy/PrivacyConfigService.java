package com.bjxczy.core.web.privacy;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executor;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.nacos.api.config.listener.Listener;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.bjxczy.core.web.privacy.handler.MaskingField;
import com.bjxczy.core.web.privacy.handler.MaskingHandler;
import com.bjxczy.core.web.privacy.iterator.MaskingIterator;
import com.bjxczy.core.web.properties.OneParkPrivacyProperties;
import com.bjxczy.onepark.common.constant.NacosConstant;
import com.bjxczy.onepark.common.context.LoginUserContextHolder;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;

import lombok.SneakyThrows;

public class PrivacyConfigService implements InitializingBean {

	public static final String PRIVACY_STORE_DATA_ID = "privacyStore.json";

	@Autowired
	private NacosConfigManager manager;
	@Autowired
	private OneParkPrivacyProperties properties;

	private Map<String, Map<String, MaskingConfig>> configMap;

	/**
	 * 判断是否需要脱敏
	 * 
	 * @param oper 操作类型
	 * @param user
	 * @return
	 */
	public boolean isNeedMasking(MaskingOperation oper, UserInformation user) {
		return isNeedMasking(properties.getDefaultConfigId(), oper, user);
	}

	/**
	 * 判断是否需要脱敏
	 * 
	 * @param key
	 * @param oper 操作类型
	 * @param user
	 * @return
	 */
	public boolean isNeedMasking(String key, MaskingOperation oper, UserInformation user) {
		if (StringUtils.isNotBlank(key) && configMap.containsKey(key)) {
			Map<String, MaskingConfig> e = configMap.get(key);
			if (e.containsKey(oper.getValue())) {
				return e.get(oper.getValue()).needMasking(user);
			}
		}
		return false;
	}

	/**
	 * 获取前端脱敏配置
	 * 
	 * @param user
	 * @return
	 */
	public Map<String, Map<String, Boolean>> getFrontendMaskingConfig(UserInformation user) {
		Map<String, Map<String, Boolean>> data = new HashMap<>();
		for (Entry<String, Map<String, MaskingConfig>> entry : this.configMap.entrySet()) {
			Map<String, Boolean> e = new HashMap<>();
			for (Entry<String, MaskingConfig> item : entry.getValue().entrySet()) {
				e.put(item.getKey(), item.getValue().frontendMasking(user));
			}
			data.put(entry.getKey(), e);
		}
		return data;
	}

	/**
	 * 执行脱敏
	 * 
	 * @param obj
	 */
	@SneakyThrows
	public void doMasking(ResponseMasking dataMasking, Object obj) {
		if (dataMasking != null
				&& isNeedMasking(dataMasking.configId(), dataMasking.operation(), LoginUserContextHolder.get())) {
			if (obj instanceof R) {
				obj = ((R<?>) obj).getData();
			}
			MaskingIterator iterator = dataMasking.iterator().newInstance().set(obj);
			Map<String, MaskingHandler> handlerMap = null;
			if (dataMasking.fields().length > 0) {
				handlerMap = new HashMap<>();
				for (MaskingField item : dataMasking.fields()) {
					handlerMap.put(item.name(), item.typeHandler().newInstance());
				}
			}
			MaskingUtil.iteratorMasking(iterator, handlerMap);
		}
	}
	
	public void doMasking(MaskingOperation oper, MaskingIterator iterator) {
		doMasking(properties.getDefaultConfigId(), oper, null, iterator);
	}
	
	public void doMasking(String configId, MaskingOperation oper, MaskingIterator iterator) {
		doMasking(configId, oper, null, iterator);
	}

	public void doMasking(MaskingOperation oper, Map<String, MaskingHandler> handlerMap, MaskingIterator iterator) {
		doMasking(properties.getDefaultConfigId(), oper, handlerMap, iterator);
	}

	/**
	 * 编程方式执行脱敏
	 * 
	 * @param configId 隐私配置ID
	 * @param oper 操作类型
	 * @param handlerMap 脱敏处理器
	 * @param iterator 数据迭代器
	 */
	public void doMasking(String configId, MaskingOperation oper, Map<String, MaskingHandler> handlerMap,
			MaskingIterator iterator) {
		if (isNeedMasking(configId, oper, LoginUserContextHolder.get())) {
			if (handlerMap == null) {
				MaskingUtil.iteratorMasking(iterator);
			} else {
				MaskingUtil.iteratorMasking(iterator, handlerMap);
			}
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		String text = manager.getConfigService().getConfigAndSignListener(PRIVACY_STORE_DATA_ID,
				NacosConstant.RESOURCE_GROUP_ID, 10000, new Listener() {

					@Override
					public void receiveConfigInfo(String configInfo) {
						setConfigMap(configInfo);
					}

					@Override
					public Executor getExecutor() {
						return null;
					}
				});
		setConfigMap(text);
	}

	private void setConfigMap(String text) {
		JSONObject json = JSON.parseObject(text);
		Map<String, Map<String, MaskingConfig>> config = new HashMap<>();
		for (String key : json.keySet()) {
			JSONObject item = json.getJSONObject(key);
			JSONArray oper = item.getJSONArray("oper");
			Map<String, MaskingConfig> e = new HashMap<>();
			for (int i = 0; i < oper.size(); i++) {
				String operKey = oper.getString(i);
				e.put(operKey, item.getJSONObject(operKey).toJavaObject(MaskingConfig.class));
			}
			config.put(key, e);
		}
		this.configMap = config;
	}

}
