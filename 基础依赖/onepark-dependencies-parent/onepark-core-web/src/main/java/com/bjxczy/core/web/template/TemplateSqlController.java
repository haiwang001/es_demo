package com.bjxczy.core.web.template;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.util.ExcelUtil;
import com.bjxczy.onepark.common.model.common.ExportWrapper;
import com.bjxczy.onepark.common.model.common.PageWrapper;
import com.bjxczy.onepark.common.service.TemplateSqlService;

@ApiResourceController
@RequestMapping("/template")
public class TemplateSqlController extends AbstractTemplateController {
	
	@Autowired
	private TemplateSqlService service;
	
	@PostMapping(value = "/list", name = "[通用]Template SQL查询")
	public List<Map<String, Object>> list(@RequestBody QueryDTO dto) {
		checkPermission(dto.getId(), null, this);
		return service.list(dto.getId(), dto.getParams());
	}
	
	@PostMapping(value = "/page", name = "[通用]Template SQL分页")
	public PageWrapper<Map<String, Object>> page(@RequestBody QueryPageDTO dto) {
		checkPermission(dto.getId(), null, this);
		return service.page(dto.getId(), dto.getPageNum(), dto.getPageSize(), dto.getParams());
	}
	
	@PostMapping(value = "/export", name = "[通用]Template SQL导出Excel")
	public void export(@RequestBody QueryPageDTO dto, HttpServletResponse response) {
		checkPermission(dto.getId(), null, this);
		ExportWrapper wrapper = service.export(dto.getId(), dto.getPageSize(), dto.getParams());
		String sheetName = wrapper.getName() + "_" + System.currentTimeMillis();
		ExcelUtil.exportXlsx(response, sheetName, wrapper.getHeadMap(), wrapper.getData());
	}

	@Override
	public TemplateSqlService getService() {
		return service;
	}

}
