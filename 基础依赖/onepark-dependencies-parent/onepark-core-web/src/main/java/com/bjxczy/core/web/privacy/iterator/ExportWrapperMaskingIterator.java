package com.bjxczy.core.web.privacy.iterator;

import java.util.Iterator;

import com.bjxczy.onepark.common.model.common.ExportWrapper;

public class ExportWrapperMaskingIterator implements MaskingIterator {

	private Iterator<?> iterator;

	@Override
	public MaskingIterator set(Object obj) {
		if (obj instanceof ExportWrapper) {
			ExportWrapper wrapper = (ExportWrapper) obj;
			this.iterator = wrapper.getData().iterator();
		} else {
			throw new IllegalArgumentException(
					"参数类型错误，期望[com.bjxczy.onepark.common.model.common.ExportWrapper]，实际[" + obj.getClass() + "]");
		}
		return this;
	}

	@Override
	public Object next() {
		return iterator.next();
	}

	@Override
	public boolean hasNext() {
		return iterator.hasNext();
	}

}
