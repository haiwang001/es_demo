package com.bjxczy.core.web.log;

import lombok.Getter;
import lombok.Setter;

public enum OperationType {
    /**
     * 操作类型
     */
    UNKNOWN("unknown", "未知"),
    DELETE("remove", "删除"),
    SELECT("get", "查询"),
    UPDATE("put", "更新"),
    INSERT("post", "新增"),
    EXPORT("export", "导出"),
    APPROVAL("approval", "审批"),
    OPEN("open", "访问");

	@Getter
	@Setter
    private String value;
    
	@Getter
	@Setter
    private String label;

    OperationType(String value, String label) {
        this.value = value;
        this.label = label;
    }

}
