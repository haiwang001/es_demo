package com.bjxczy.core.web.privacy.iterator;

import java.util.Iterator;

import com.bjxczy.onepark.common.model.common.PageWrapper;

public class PageWrapperMaskingIterator implements MaskingIterator {

	private Iterator<?> iterator;

	@Override
	public MaskingIterator set(Object obj) {
		if (obj instanceof PageWrapper) {
			PageWrapper<?> wrapper = (PageWrapper<?>) obj;
			this.iterator = wrapper.getRecords().iterator();
		} else {
			throw new IllegalArgumentException(
					"参数类型错误，期望[com.bjxczy.onepark.common.model.common.PageWrapper]，实际[" + obj.getClass() + "]");
		}
		return this;
	}

	@Override
	public Object next() {
		return iterator.next();
	}

	@Override
	public boolean hasNext() {
		return iterator.hasNext();
	}

}
