package com.bjxczy.core.web.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.persistence.MappedSuperclass;

import org.springframework.data.domain.AfterDomainEventPublication;
import org.springframework.data.domain.DomainEvents;

import cn.hutool.core.lang.Assert;

@MappedSuperclass
public abstract class AbstractAggregateRoot<A extends AbstractAggregateRoot<A>> extends Entity {
	
	private transient final List<Object> domainEvents = new ArrayList<>();
	
	protected <T> T registerEvent(T event) {
        Assert.notNull(event, "Domain event must not be null!");
        this.domainEvents.add(event);
        return event;
    }
	
	@AfterDomainEventPublication
	protected void clearDomainEvents() {
		this.domainEvents.clear();
	}
	
	@DomainEvents
    protected Collection<Object> domainEvents() {
        return Collections.unmodifiableList(domainEvents);
    }
    
    @SuppressWarnings("unchecked")
	protected final A andEventsFrom(A aggregate) {
        Assert.notNull(aggregate, "Aggregate must not be null!");
        this.domainEvents.addAll(aggregate.domainEvents());
        return (A) this;
    }
    
    @SuppressWarnings("unchecked")
    protected final A andEvent(Object event) {
        registerEvent(event);
        return (A) this;
    }
    
    protected Object newRemoveEvent() {
		return null;
	}
    
    public void andRemoveEvent() {
		Object event = this.newRemoveEvent();
		if (event != null) {
			this.andEvent(event);
		}
	}

}
