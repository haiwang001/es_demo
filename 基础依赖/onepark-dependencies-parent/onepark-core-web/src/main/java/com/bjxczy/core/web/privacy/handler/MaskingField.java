package com.bjxczy.core.web.privacy.handler;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface MaskingField {
	
	/**
	 * 字段名称，实体中非必须
	 * ResponseMasking中必须
	 * @return
	 */
	String name() default "";

	/**
	 * 脱敏处理器
	 * @return
	 */
	Class<? extends MaskingHandler> typeHandler() default UnknowMaskingHandler.class;

}
