package com.bjxczy.core.web.base;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserOperator extends LogicDelete implements Serializable {

	private static final long serialVersionUID = 1564423940720503342L;

	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;
	
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private String operator;
	
}
