package com.bjxczy.core.web.dict;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.bjxczy.core.web.util.JdbcTemplateWrapper;
import com.bjxczy.onepark.common.model.common.DictInformation;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JdbcDictServiceImpl implements DictService {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private String tableName;

	public JdbcDictServiceImpl(String tableName) {
		this.tableName = tableName;
	}

	@Override
	public List<DictInformation> listDict(String groupCode) {
		String sql = "select * from " + tableName + " where group_code = ? and del_flag = 0";
		return JdbcTemplateWrapper.queryListForEntity(jdbcTemplate, sql, DictInformation.class, groupCode);
	}

	@Override
	public String getLabel(String groupCode, Object value) {
		String sql = "select * from " + tableName + " where group_code = ? and value = ? and del_flag = 0";
		List<DictInformation> list = JdbcTemplateWrapper.queryListForEntity(jdbcTemplate, sql, DictInformation.class,
				groupCode, value);
		if (list == null || list.isEmpty()) {
			log.warn("字段值不应为空！");
			return null;
		}
		if (list.size() > 1) {
			throw new RuntimeException("字典value不能重复！");
		}
		return list.get(0).getLabel();
	}

	@Override
	public Map<String, DictInformation> valueKeyMap(String groupCode) {
		List<DictInformation> list = listDict(groupCode);
		Map<String, DictInformation> map = new HashMap<>();
		list.forEach(e -> map.put(e.getValue(), e));
		return map;
	}

}
