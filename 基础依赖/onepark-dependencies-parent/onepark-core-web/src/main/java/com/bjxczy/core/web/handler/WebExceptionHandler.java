package com.bjxczy.core.web.handler;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.resp.R;

@ControllerAdvice
public class WebExceptionHandler {

	@ExceptionHandler(value = Exception.class)
	@ResponseBody
	public R<?> handle(Exception e, HttpServletResponse resp) {
		// 自定义异常得处理
		if (e instanceof ResultException) {
			ResultException myException = (ResultException) e;
			return R.fail(myException.getMessage());
		}
		e.printStackTrace();
		// 异常处理
		R<?> r = R.error("系统异常");
		r.setException(e.getMessage());
		resp.setStatus(500);
		return r;
	}

	@ExceptionHandler({ MethodArgumentNotValidException.class })
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public R<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
		BindingResult bindingResult = ex.getBindingResult();
		StringBuilder sb = new StringBuilder("请求失败：");
		for (FieldError fieldError : bindingResult.getFieldErrors()) {
			sb.append(fieldError.getDefaultMessage()).append(", ");
		}
		String msg = sb.toString();
		return R.fail(msg);
	}

}
