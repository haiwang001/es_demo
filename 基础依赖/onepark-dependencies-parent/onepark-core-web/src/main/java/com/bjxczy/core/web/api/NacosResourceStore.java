package com.bjxczy.core.web.api;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.nacos.api.exception.NacosException;
import com.bjxczy.onepark.common.constant.NacosConstant;

public class NacosResourceStore implements IApiResourceStore {

	private static final int DEFAULT_NACOS_CONFIG_TIMEOUT = 5000;

	private NacosConfigManager nacosConfigManager;

	public NacosResourceStore(NacosConfigManager nacosConfigManager) {
		this.nacosConfigManager = nacosConfigManager;
	}

	@Override
	public void store(String key, Object obj) {
		try {
			String prettyJsonString = JSON.toJSONString(JSON.parseObject(JSON.toJSONString(obj)),
					SerializerFeature.PrettyFormat);
			nacosConfigManager.getConfigService().publishConfig(key, NacosConstant.RESOURCE_GROUP_ID, prettyJsonString);
		} catch (NacosException e) {
			throw new RuntimeException("[ApiStore] save [" + key + "] error", e);
		}
	}

	@Override
	public String get(String key) {
		try {
			return nacosConfigManager.getConfigService().getConfig(key, NacosConstant.RESOURCE_GROUP_ID, DEFAULT_NACOS_CONFIG_TIMEOUT);
		} catch (NacosException e) {
			throw new RuntimeException("[ApiStore] get [" + key + "] error", e);
		}
	}

	@Override
	public <T> T get(String key, TypeReference<T> typeReference) {
		String prettyJsonString = get(key);
		if (StringUtils.isNotBlank(prettyJsonString)) {
			return JSON.parseObject(prettyJsonString, typeReference);
		} else {
			return null;
		}
	}

}
