package com.bjxczy.core.web.message;

import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.utils.OptionalUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.bjxczy.core.rabbitmq.supports.WorkbenchBaseListener;
import com.bjxczy.onepark.common.model.workbench.DealMessageInformation;
import com.bjxczy.onepark.common.model.workbench.PushMessageInformation;
import com.bjxczy.onepark.common.model.workbench.TemplateMessagePayload;

import java.util.Optional;

public class MessageService {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	/**
	 * 推送平台消息
	 *
	 * @param information
	 */
	public void pushMessage(PushMessageInformation information) {
		this.rabbitTemplate.convertAndSend(WorkbenchBaseListener.DEFAULT_EXCAHNGE_NAME,
				WorkbenchBaseListener.PUSH_MESSAGE_EVENT, information);
	}

	/**
	 * 更新消息状态
	 *
	 * @param information
	 */
	public void dealMessage(DealMessageInformation information) {
		this.rabbitTemplate.convertAndSend(WorkbenchBaseListener.DEFAULT_EXCAHNGE_NAME,
				WorkbenchBaseListener.DEAL_MESSAGE_EVENT, information);
	}

	/**
	 * 推送模板消息
	 *
	 * @param payload
	 */
	public void pushTemplateMessage(TemplateMessagePayload payload) {
		if (null!=payload.getStaffId()){
			this.rabbitTemplate.convertAndSend(WorkbenchBaseListener.DEFAULT_EXCAHNGE_NAME,
					WorkbenchBaseListener.TEMPLTE_MESSAGE_EVENT, payload);
		}else {
			throw new ResultException("发送模板/消息：staffId 不能为空！！！");
		}
	}
}
