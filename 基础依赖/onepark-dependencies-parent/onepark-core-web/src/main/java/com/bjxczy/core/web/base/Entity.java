package com.bjxczy.core.web.base;

public abstract class Entity {
	
	public static final String DEFAULT_ERRORS_SPLIT = "，";
	
	public String getErrorsSplit() {
		return DEFAULT_ERRORS_SPLIT;
	}

	/**
	 * 手动触发spring-validate 
	 */
	public void validate() {
		ValidtorHelper.validate(this, DEFAULT_ERRORS_SPLIT);
	}

}
