package com.bjxczy.core.web.template;

import java.util.Map;

import lombok.Data;

@Data
public class QueryDTO {
	
	private String id;
	
	private Map<String, Object> params;

}
