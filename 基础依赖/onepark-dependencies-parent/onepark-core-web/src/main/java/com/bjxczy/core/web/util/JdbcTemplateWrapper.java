package com.bjxczy.core.web.util;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import cn.hutool.core.util.ReflectUtil;
import lombok.SneakyThrows;

public class JdbcTemplateWrapper {

	public static final char UNDERLINE = '_';

	public static <T> int updateByEntity(NamedParameterJdbcTemplate namedTemplate, String sql, T entity) {
		Map<String, Object> paramMap = describe(entity);
		return namedTemplate.update(sql, paramMap);
	}

	public static <T> List<T> queryListForEntity(JdbcTemplate jdbcTemplate, String sql, Class<T> clazz) {
		return queryListForEntity(jdbcTemplate, sql, clazz, null);
	}

	public static <T> List<T> queryListForEntity(JdbcTemplate jdbcTemplate, String sql, Class<T> clazz,
			Object... params) {
		return jdbcTemplate.query(sql, new RowMapper<T>() {
			@SneakyThrows
			@Override
			public T mapRow(ResultSet resultSet, int i) throws SQLException {
				T entity = clazz.newInstance();
				ResultSetMetaData md = resultSet.getMetaData();
				for (int j = 1; j <= md.getColumnCount(); j++) {
					String columnName = md.getColumnLabel(j), name = underlineToCamel(columnName);
					Object value = resultSet.getObject(columnName);
					if (value != null) {
						Field field = clazz.getDeclaredField(name);
						field.setAccessible(true);
						field.set(entity, value);
					}
				}
				return entity;
			}
		}, params);
	}

	private static Map<String, Object> describe(Object entity) {
		Map<String, Object> map = new HashMap<>();
		if (entity != null) {
			Class<?> clazz = entity.getClass();
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				try {
					f.setAccessible(true);
					map.put(f.getName(), f.get(entity));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		return map;
	}

	public static String underlineToCamel(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		param = param.toLowerCase();
		int len = param.length();
		StringBuilder sb = new StringBuilder(len);
		Boolean flag = false; // "_" 后转大写标志,默认字符前面没有"_"
		for (int i = 0; i < len; i++) {
			char c = param.charAt(i);
			if (c == UNDERLINE) {
				flag = true;
				continue; // 标志设置为true,跳过
			} else {
				if (flag == true) {
					// 表示当前字符前面是"_" ,当前字符转大写
					sb.append(Character.toUpperCase(param.charAt(i)));
					flag = false; // 重置标识
				} else {
					sb.append(Character.toLowerCase(param.charAt(i)));
				}
			}
		}
		return sb.toString();
	}

	public static <T> List<T> listMap2Bean(List<Map<String, Object>> list, Class<T> clazz) {
		return list.stream().map(e -> {
			try {
				T inst = clazz.newInstance();
				Map<String, Field> map = ReflectUtil.getFieldMap(clazz);
				for (Entry<String, Object> entry : e.entrySet()) {
					String name = underlineToCamel(entry.getKey());
					if (map.containsKey(name)) {
						Field field = map.get(name);
						field.setAccessible(true);
						field.set(inst, entry.getValue());
					}
				}
				return inst;
			} catch (InstantiationException | IllegalAccessException | SecurityException
					| IllegalArgumentException e1) {
				throw new RuntimeException(e1);
			}
		}).collect(Collectors.toList());
	}

}
