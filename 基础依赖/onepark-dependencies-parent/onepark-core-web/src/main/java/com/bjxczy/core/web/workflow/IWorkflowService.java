package com.bjxczy.core.web.workflow;

import com.bjxczy.onepark.common.resp.R;

public interface IWorkflowService {

	/**
	 * 审批校验，需校验用户提交申请数据是否合理，及业务限制
	 * @param key 业务流程DeployId
	 * @param payload 员工ID、用户提交变量数据
	 * @return
	 */
	R<?> validated(String key, ProcessPayload payload);

	/**
	 * 审批通过处理
	 * @param key 业务流程DeployId
	 * @param payload 员工ID、用户提交变量数据
	 * @return
	 */
	R<?> approved(String key, ProcessPayload payload);

}
