package com.bjxczy.core.web.base;

import java.io.Serializable;
import java.util.Optional;

public interface AbstractRepository<T> {
	
	default Boolean saveModel(T model) {
		return getImpl().defaultSaveModel(model);
	}
	
	Optional<T> findById(Serializable id);
	
	default Boolean remove(T model) {
		return getImpl().defaultRemove(model);
	}
	
	AbstractRepository<T> getImpl();
	
	Boolean defaultSaveModel(T model);
	
	Boolean defaultRemove(T Model);

}
