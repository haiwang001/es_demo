package com.bjxczy.core.web.privacy;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;

import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.bjxczy.core.web.privacy.handler.MaskingField;
import com.bjxczy.core.web.privacy.handler.MaskingHandler;
import com.bjxczy.core.web.privacy.iterator.MaskingIterator;

import com.sun.org.apache.bcel.internal.generic.NEW;
import lombok.SneakyThrows;

public class MaskingUtil {

	/**
	 * 迭代器脱敏
	 * @param iterator
	 * @param handlerMap
	 */
	public static void iteratorMasking(MaskingIterator iterator) {
		iteratorMasking(iterator);
	}

	/**
	 * 迭代器脱敏
	 * @param iterator
	 * @param handlerMap
	 */
	@SuppressWarnings("unchecked")
	public static void iteratorMasking(MaskingIterator iterator, Map<String, MaskingHandler> handlerMap) {
		while(iterator.hasNext()) {
			Object data = iterator.next();
			if (handlerMap != null && data instanceof Map) {
				MaskingUtil.mapMasking((Map<String, Object>) data, handlerMap);
			} else if (handlerMap != null) {
				MaskingUtil.beanMasking(data, handlerMap);
			} else {
				MaskingUtil.beanMasking(data);
			}
		}
	}

	/**
	 * 实体类脱敏，基于实体中定义@MaskingField
	 * @param obj
	 */
	@SneakyThrows
	public static void beanMasking(Object obj) {
		Class<? extends Object> clazz = obj.getClass();
		Field[] fields = clazz.getDeclaredFields();
		for (Field f : fields) {
			MaskingField anno = f.getAnnotation(MaskingField.class);
			if (anno != null) {
				MaskingHandler handler = anno.typeHandler().newInstance();
				f.setAccessible(true);
				String value = f.get(obj) == null ? null : f.get(obj).toString();
				if (StringUtils.isNotBlank(value)) {
					value = handler.getMaskingValue(value);
					f.set(obj, value);
				}
			}
		}
	}

	/**
	 * 实体类脱敏
	 * @param obj
	 */
	@SneakyThrows
	public static void beanMasking(Object obj, Map<String, MaskingHandler> handler) {
		Class<? extends Object> clazz = obj.getClass();
		for (Entry<String, MaskingHandler> entry : handler.entrySet()) {
			Field f = ReflectUtil.getField(clazz,entry.getKey());
			if (f != null) {
				f.setAccessible(true);
				String value = f.get(obj) == null ? null : f.get(obj).toString();
				if (StringUtils.isNotBlank(value)) {
					value = entry.getValue().getMaskingValue(value);
					f.set(obj, value);
				}
			}
		}
	}

	/**
	 * Map类型脱敏
	 * @param data
	 * @param user
	 * @param handler
	 */
	public static void mapMasking(Map<String, Object> data, Map<String, MaskingHandler> handler) {
		for (Entry<String, MaskingHandler> entry : handler.entrySet()) {
			if (data.containsKey(entry.getKey())) {
				String value = data.get(entry.getKey()) == null ? null : data.get(entry.getKey()).toString();
				if (StringUtils.isNotBlank(value)) {
					value = entry.getValue().getMaskingValue(value);
					data.put(entry.getKey(), value);
				}
			}
		}
	}

}
