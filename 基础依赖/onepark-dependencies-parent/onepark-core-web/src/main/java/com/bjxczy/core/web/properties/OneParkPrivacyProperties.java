package com.bjxczy.core.web.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@ConfigurationProperties(prefix = "onepark.privacy")
public class OneParkPrivacyProperties {
	
	private boolean enable;
	
	private String defaultConfigId;

}
