package com.bjxczy.core.web.dict;

import java.util.List;
import java.util.Map;

import com.bjxczy.onepark.common.model.common.DictInformation;

public interface DictService {
	
	/**
	 * 查询字典数据
	 * @param groupCode 分组编码
	 * @return
	 */
	List<DictInformation> listDict(String groupCode);
	
	/**
	 * 查询描述
	 * @param groupCode 分组编码
	 * @param value 字典值
	 * @return
	 */
	String getLabel(String groupCode, Object value);
	
	/**
	 * 获取value作为Key的Map
	 * @param groupCode
	 * @return
	 */
	Map<String, DictInformation> valueKeyMap(String groupCode);
	
}
