package com.bjxczy.core.web.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class OperationUtils {
	
	public String dateFormat(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	
	public Object ifNull(Object obj, String text) {
		if (obj == null) {
			return text;
		}
		return obj;
	}
	
	public Object ifNotNull(Object obj, String text) {
		if (obj != null) {
			return text;
		}
		return "";
	}
	
	public String ifBlank(String str, String text) {
		if (StringUtils.isBlank(str)) {
			return text;
		}
		return str;
	}

}
