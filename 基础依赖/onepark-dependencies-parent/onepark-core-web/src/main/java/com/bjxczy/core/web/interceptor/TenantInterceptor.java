package com.bjxczy.core.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import com.bjxczy.onepark.common.constant.SecurityContant;
import com.bjxczy.onepark.common.context.TenantContextHolder;
import com.bjxczy.onepark.common.model.common.TenantInfo;

public class TenantInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String tenantValue = request.getHeader(SecurityContant.SYS_TENANT_NAME);
		if (StringUtils.isNotBlank(tenantValue)) {
			TenantContextHolder.set(new TenantInfo(tenantValue));
		}
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}

}
