package com.bjxczy.core.web.template;

import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.service.TemplateSqlService;

public abstract class AbstractTemplateController {

	public abstract TemplateSqlService getService();

	protected void checkPermission(String key, String path, Object source) {
		Boolean isResource = getService().isResource(key, path);
		if (isResource == null) {
			throw new ResultException("[TemplateSql] key=" + key + ", 不存在！");
		}
		if (source instanceof TemplateSqlController && isResource) {
			throw new ResultException("[TemplateSql] key=" + key + ", 需授权访问！");
		} else if (source instanceof TemplateResController && !isResource) {
			throw new ResultException("[TemplateSql] key=" + key + ", 未开启[" + path + "]访问！");
		}
	}

}
