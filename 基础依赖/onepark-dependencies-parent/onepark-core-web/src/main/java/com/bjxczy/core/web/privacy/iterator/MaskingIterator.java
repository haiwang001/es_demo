package com.bjxczy.core.web.privacy.iterator;

public interface MaskingIterator {
	
	MaskingIterator set(Object obj);
	
	Object next();
	
	boolean hasNext();

}
