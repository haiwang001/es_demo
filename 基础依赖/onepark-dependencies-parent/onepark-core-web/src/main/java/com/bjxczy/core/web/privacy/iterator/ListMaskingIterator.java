package com.bjxczy.core.web.privacy.iterator;

import java.util.Iterator;
import java.util.List;

public class ListMaskingIterator implements MaskingIterator {

	private Iterator<Object> iterator;

	@SuppressWarnings("unchecked")
	@Override
	public MaskingIterator set(Object obj) {
		if (obj instanceof List) {
			List<Object> list = (List<Object>) obj;
			this.iterator = list.iterator();
		} else {
			throw new IllegalArgumentException("参数类型错误，期望[java.util.List]，实际[" + obj.getClass() + "]");
		}
		return this;
	}

	@Override
	public Object next() {
		return this.iterator.next();
	}

	@Override
	public boolean hasNext() {
		return this.iterator.hasNext();
	}

}
