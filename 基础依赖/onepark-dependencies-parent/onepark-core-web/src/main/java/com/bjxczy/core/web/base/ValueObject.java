package com.bjxczy.core.web.base;

import java.io.Serializable;

public abstract class ValueObject<T> implements Serializable {

	private static final long serialVersionUID = -8099412520488398606L;

	 /**
	     * 通常来说，在比较相等性
	     * 时，我们将省略掉对非null的检查。
     *
     * @param object 要比较的对象
     * @return 相等返回true
     */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		}
		if (object.getClass() != this.getClass()) {
			return false;
		}
		T valueObject = (T) object;
		return equalsTo(valueObject);
	}
	
	protected void vaildate() {
		ValidtorHelper.validate(this);
	}
	
	/**
	 * 判断值对象是否相等，由派生类实现。
	 *
	 * @param other 比较对象
	 * @return 相等返回true，否则返回false
	 */
	protected abstract boolean equalsTo(T other);

}
