package com.bjxczy.core.web.mybatis;

import org.springframework.beans.BeanUtils;

public class DefaultAssemlerUtils {
	
	public static <R> R copyProperties(Object source, Class<R> clazz) {
		R target = null;
		if (source != null) {
			try {
				target = clazz.newInstance();
				BeanUtils.copyProperties(source, target);
			} catch (InstantiationException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
		return target;
	}

}
