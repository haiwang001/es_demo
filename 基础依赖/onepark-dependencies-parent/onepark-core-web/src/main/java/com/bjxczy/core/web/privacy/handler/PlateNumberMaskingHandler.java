package com.bjxczy.core.web.privacy.handler;

import cn.hutool.core.util.StrUtil;

/**
 * 手机号脱敏
 * @author zzz
 *
 */
public class PlateNumberMaskingHandler extends MaskingHandler {

	@Override
	public String getMaskingValue(String value) {
		return StrUtil.hide(value, 2, value.length() - 2);
	}
	
}
