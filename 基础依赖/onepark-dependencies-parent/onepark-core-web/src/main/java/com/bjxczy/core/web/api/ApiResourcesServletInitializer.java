package com.bjxczy.core.web.api;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.alibaba.fastjson.TypeReference;
import com.bjxczy.core.web.properties.OneParkApiProperties;
import com.bjxczy.onepark.common.constant.ExtendHttpMethod;
import com.bjxczy.onepark.common.constant.NacosConstant;
import com.bjxczy.onepark.common.model.common.ApiResourceInformation;
import com.bjxczy.onepark.common.service.TemplateSqlService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApiResourcesServletInitializer implements InitializingBean {

	private RequestMappingHandlerMapping requestMappingHandlerMapping;
	
	private TemplateSqlService templateSqlService;

	private IApiResourceStore store;
	
	private OneParkApiProperties properties;

	@Value("${spring.application.name}")
	private String applicationName;

	public ApiResourcesServletInitializer(RequestMappingHandlerMapping requestMappingHandlerMapping,
			TemplateSqlService templateSqlService, IApiResourceStore store, OneParkApiProperties properties) {
		super();
		this.templateSqlService = templateSqlService;
		this.requestMappingHandlerMapping = requestMappingHandlerMapping;
		this.store = store;
		this.properties = properties;
	}

	public Map<RequestMappingInfo, HandlerMethod> getHandlerMethodsMap() {
		if (requestMappingHandlerMapping == null) {
			log.warn("requestMappingHandlerMapping is null.");
			return null;
		}
		return requestMappingHandlerMapping.getHandlerMethods();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (properties.getAutoSync()) {
			init();
		}
	}

	protected void init() {
		Map<RequestMappingInfo, HandlerMethod> map = getHandlerMethodsMap();
		List<ApiResourceInformation> apiResources = new ArrayList<>();
		map.forEach((requestMappingInfo, handler) -> {
			ApiResourceController ofBean = handler.getBeanType().getAnnotation(ApiResourceController.class);
			RequestMapping controllerMapping = handler.getBeanType().getAnnotation(RequestMapping.class);
			if (ofBean != null) {
				ApiResourceInformation info = new ApiResourceInformation();
				info.setServiceName(applicationName);
				info.setHttpMethods(getMappingMethods(requestMappingInfo));
				info.setPatterns(getMappingPatterns(requestMappingInfo));
				info.setName(
						getApiName(requestMappingInfo, controllerMapping, info.getHttpMethods(), info.getPatterns()));
				info.setClassName(handler.getBeanType().getName());
				apiResources.add(info);
				log.info("[ApiGateway] Resource Path={}", info);
			}
		});
		if (templateSqlService != null) {
			apiResources.addAll(templateSqlService.listApis());
		}
		storeApiResourceInfomation(apiResources);
	}

	private String getApiName(RequestMappingInfo requestMappingInfo, RequestMapping controllerMapping,
			Set<ExtendHttpMethod> methods, Set<String> patterns) {
		String name = requestMappingInfo.getName();
		if (StringUtils.isBlank(name)) {
			name = methods.toString() + ":/" + this.applicationName + StringUtils.strip(patterns.toString(), "[]");
		} else if (null != controllerMapping && StringUtils.isNotBlank(controllerMapping.name())) {
			name = name.substring(name.indexOf("#") + 1);
			name = MessageFormat.format(name, controllerMapping.name());
		}
		return name;
	}
	
	private Set<ExtendHttpMethod> getMappingMethods(RequestMappingInfo requestMappingInfo) {
		return requestMappingInfo.getMethodsCondition().getMethods().stream()
				.map(item -> ExtendHttpMethod.valueOf(item.toString())).collect(Collectors.toSet());
	}

	private Set<String> getMappingPatterns(RequestMappingInfo requestMappingInfo) {
		return requestMappingInfo.getPatternsCondition().getPatterns();
	}

	private void storeApiResourceInfomation(List<ApiResourceInformation> apiResources) {
		Map<String, List<ApiResourceInformation>> existMap = store.get(NacosConstant.API_RESOURCE_DATA_ID,
				new TypeReference<Map<String, List<ApiResourceInformation>>>() {
				});
		if (existMap == null) {
			existMap = new HashMap<>();
		}
		existMap.put(this.applicationName, apiResources);
		store.store(NacosConstant.API_RESOURCE_DATA_ID, existMap);
	}

}
