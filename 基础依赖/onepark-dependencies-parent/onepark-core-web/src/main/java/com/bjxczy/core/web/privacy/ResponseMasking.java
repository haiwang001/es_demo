package com.bjxczy.core.web.privacy;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.bjxczy.core.web.privacy.handler.MaskingField;
import com.bjxczy.core.web.privacy.iterator.MaskingIterator;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface ResponseMasking {

	/**
	 * 脱敏配置KEY
	 * 
	 * @return
	 */
	String configId() default "";

	/**
	 * 脱敏操作
	 * 
	 * @return
	 */
	MaskingOperation operation();

	/**
	 * 迭代器
	 * 
	 * @return
	 */
	Class<? extends MaskingIterator> iterator();
	
	/**
	 * 脱敏字段，非必须
	 * @return
	 */
	MaskingField[] fields() default {};

}
