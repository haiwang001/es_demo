package com.bjxczy.core.web.api;

import com.alibaba.fastjson.TypeReference;

public interface IApiResourceStore {

	void store(String key, Object obj);

	String get(String key);

	<T> T get(String key, TypeReference<T> typeReference);

}
