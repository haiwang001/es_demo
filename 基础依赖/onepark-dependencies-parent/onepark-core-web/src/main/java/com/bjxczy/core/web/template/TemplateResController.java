package com.bjxczy.core.web.template;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bjxczy.core.web.util.ExcelUtil;
import com.bjxczy.onepark.common.model.common.ExportWrapper;
import com.bjxczy.onepark.common.model.common.PageWrapper;
import com.bjxczy.onepark.common.service.TemplateSqlService;

@RestController
@RequestMapping("/template")
public class TemplateResController extends AbstractTemplateController {

	@Autowired
	private TemplateSqlService service;

	@PostMapping("/res/{namespace}/{id}/list")
	public List<Map<String, Object>> list(@PathVariable("namespace") String namespace, @PathVariable("id") String id,
			@RequestBody QueryDTO dto) {
		dto.setId(namespace + "/" + id);
		checkPermission(dto.getId(), "list", this);
		return service.list(dto.getId(), dto.getParams());
	}

	@PostMapping("/res/{namespace}/{id}/page")
	public PageWrapper<Map<String, Object>> page(@PathVariable("namespace") String namespace,
			@PathVariable("id") String id, @RequestBody QueryPageDTO dto) {
		dto.setId(namespace + "/" + id);
		checkPermission(dto.getId(), "page", this);
		return service.page(dto.getId(), dto.getPageNum(), dto.getPageSize(), dto.getParams());
	}
	
	@PostMapping("/res/{namespace}/{id}/export")
	public void export(@PathVariable("namespace") String namespace,
			@PathVariable("id") String id, @RequestBody QueryPageDTO dto, HttpServletResponse response) {
		dto.setId(namespace + "/" + id);
		checkPermission(dto.getId(), "export", this);
		ExportWrapper wrapper = service.export(dto.getId(), dto.getPageSize(), dto.getParams());
		String sheetName = wrapper.getName() + "_" + System.currentTimeMillis();
		ExcelUtil.exportXlsx(response, sheetName, wrapper.getHeadMap(), wrapper.getData());
	}

	@Override
	public TemplateSqlService getService() {
		return this.service;
	}

}
