package com.bjxczy.core.web.privacy.handler;

import cn.hutool.core.util.StrUtil;

/**
 * 身份证号脱敏
 * @author zzz
 *
 */
public class IdCardMaskingHandler extends MaskingHandler {

	@Override
	public String getMaskingValue(String value) {
		return StrUtil.hide(value, 3, value.length() - 4);
	}

}
