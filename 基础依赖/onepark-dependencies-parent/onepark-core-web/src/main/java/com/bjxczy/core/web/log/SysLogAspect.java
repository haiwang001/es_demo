package com.bjxczy.core.web.log;

import java.lang.annotation.Annotation;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.bjxczy.core.feign.client.user.BaseUserFeignClient;
import com.bjxczy.core.rabbitmq.supports.PcsBaseListener;
import com.bjxczy.core.web.properties.OneParkLogProperties;
import com.bjxczy.core.web.util.AddrUtil;
import com.bjxczy.core.web.util.OperationUtils;
import com.bjxczy.core.web.util.VelocityUtil;
import com.bjxczy.onepark.common.context.LoginUserContextHolder;
import com.bjxczy.onepark.common.context.TenantContextHolder;
import com.bjxczy.onepark.common.model.common.LogInfo;
import com.bjxczy.onepark.common.model.common.TenantInfo;
import com.bjxczy.onepark.common.model.user.UserInformation;

import cn.hutool.core.lang.Assert;
import lombok.extern.slf4j.Slf4j;

@Aspect
@Slf4j
public class SysLogAspect extends PcsBaseListener {
	
	@Value("${spring.application.name}")
	private String applicationName;
	@Autowired
	private RabbitTemplate rabbitTemplate;
	@Autowired
	private BaseUserFeignClient userFeignClient;
	@Autowired
	private OneParkLogProperties properties;
	
	public static final OperationUtils OPER_UTILS = new OperationUtils();
	private static final DefaultParameterNameDiscoverer NAME_DISCOVERER = new DefaultParameterNameDiscoverer();
	
	@Pointcut("@annotation(com.bjxczy.core.web.log.SysLog)")
	public void pointcut() {}
	
	@After("pointcut() && @annotation(sysLog)")
	public void before(JoinPoint joinPoint, SysLog sysLog) throws Throwable {
		LogInfo logInfo = new LogInfo();
		logInfo.setSaveTime(new Date());
		logInfo.setApplicationName(applicationName);
		// 操作信息
		logInfo.setOperationType(sysLog.operationType().getValue());
		logInfo.setOperationName(sysLog.operationType().getLabel());
		logInfo.setPlatform(sysLog.platform());
		setOperation(joinPoint, logInfo, sysLog.operation());
		// 用户信息
		UserInformation userInfo = LoginUserContextHolder.get();
		Assert.notNull(userInfo, "[SysLog] onepark.log.enable=true, 获取登录用户不能为null!");
		userInfo = userFeignClient.findById(userInfo.getId());
		logInfo.setUserId(userInfo.getId());
		logInfo.setUserName(userInfo.getUsername());
		logInfo.setStaffId(userInfo.getStaffId());
		// 请求信息
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		logInfo.setRequestIp(AddrUtil.getIpAddr(request));
		logInfo.setRequestMethod(request.getMethod());
		logInfo.setRequestPath(request.getServletPath());
		logInfo.setToken(request.getHeader(HttpHeaders.AUTHORIZATION));
		Object[] args = joinPoint.getArgs();
		logInfo.setRequestParams(JSON.toJSONString(args, SerializerFeature.WriteMapNullValue));
		// 数据权限
		TenantInfo tenant = TenantContextHolder.get(false);
		if (tenant != null) {
			logInfo.setTenantId(tenant.getValue());
		}
		rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME, SAVE_LOG_EVENT, logInfo);
		if (properties.getShowLog()) {
			log.info("[SysLog] log={}", logInfo);
		}
	}

	private void setOperation(JoinPoint joinPoint, LogInfo logInfo, String operation) {
		// 解析LogParam标记参数
		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
		String[] paramNames = NAME_DISCOVERER.getParameterNames(methodSignature.getMethod());
		Annotation[][] parameterAnnontations = methodSignature.getMethod().getParameterAnnotations();
		Object[] args = joinPoint.getArgs();
		Map<String, Object> map = new HashMap<>();
		for (int i = 0; i < paramNames.length; i++) {
			if (parameterAnnontations[i].length > 0) {
				Annotation[] annos = parameterAnnontations[i];
				for (Annotation item : annos) {
					if (item instanceof LogParam) {
						LogParam logParam = (LogParam) item;
						String name = StringUtils.isNotBlank(logParam.value()) ? logParam.value() : paramNames[i];
						map.put(name, args[i]);
					}
				}
			}
		}
		VelocityContext context = new VelocityContext(map);
		context.put("ut", OPER_UTILS);
		// 解析LogContextHolder中参数
		context.put("ctx", LogContextHolder.getContextMap());
		logInfo.setOperation(VelocityUtil.evaluate(operation, context));
	}

}
