package com.bjxczy.core.web.base;

import java.io.Serializable;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * Restful API CRUD实现，基于MybatisPlus
 * @author zzz
 * @param <T> 实体
 * @param <ID> ID类型
 */
public abstract class AbstractRestController<T, ID extends Serializable> {
	
	/**
	 * 默认ID分隔符
	 */
	private static final String ID_SPLIT = ",";
	
	public abstract IService<T> getService();
	
	public String getIdSplit() {
		return ID_SPLIT;
	}
	
	@DeleteMapping(value = "/{id}", name = "删除{0}")
	public void removeById(@PathVariable("id") ID id) {
		getService().removeById(id);
	}
	
	@DeleteMapping(value = "/batch", name = "批量删除{0}")
	public void removeBatch(@RequestBody List<ID> ids) {
		getService().removeByIds(ids);
	}
	
	@GetMapping(value = "/{id}", name = "{0}详情")
	public T getById(@PathVariable("id") ID id) {
		return getService().getById(id);
	}
	
	@GetMapping(value = "/list", name = "查询{0}")
	public List<T> list() {
		return getService().list();
	}

}
