package com.bjxczy.core.web.privacy.handler;

/**
 * 手机号脱敏
 * @author zzz
 */
public class MobileMaskingHandler extends MaskingHandler {

	@Override
	public String getMaskingValue(String value) {
		// 中间4位脱敏
		return value.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
	}

}
