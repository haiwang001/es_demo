package com.bjxczy.core.web.configration;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.bjxczy.core.web.interceptor.LoginUserArgumentResolver;
import com.bjxczy.core.web.interceptor.TenantInterceptor;
import com.bjxczy.core.web.interceptor.UserInformationInterceptor;

public class DefaultWebMvcConfig implements WebMvcConfigurer {

	@Bean
	public LoginUserArgumentResolver loginUserArgumentResolver() {
		return new LoginUserArgumentResolver();
	}
	
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
		resolvers.add(loginUserArgumentResolver());
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new UserInformationInterceptor()).addPathPatterns("/**");
		registry.addInterceptor(new TenantInterceptor()).addPathPatterns("/**");
	}

}
