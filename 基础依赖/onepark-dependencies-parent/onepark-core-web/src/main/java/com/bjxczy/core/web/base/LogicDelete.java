package com.bjxczy.core.web.base;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableLogic;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogicDelete implements Serializable {
	
	private static final long serialVersionUID = -1310986384663549213L;
	
	@TableLogic(value = "0", delval = "1")
	private Integer delFlag;

}
