package com.bjxczy.core.web.log;

import java.util.HashMap;
import java.util.Map;

public class LogContextHolder {

	private static final ThreadLocal<Map<String, Object>> CONTEXT = new ThreadLocal<>();

	private LogContextHolder() {
	}

	private static void init() {
		if (null == CONTEXT.get()) {
			CONTEXT.set(new HashMap<>());
		}
	}

	public static void put(String key, Object value) {
		init();
		CONTEXT.get().put(key, value);
	}

	public static Object get(String key) {
		Map<String, Object> map = getContextMap();
		return null == map ? null : map.get(key);
	}

	public static Map<String, Object> getContextMap() {
		return CONTEXT.get() == null ? null : CONTEXT.get();
	}

	public static void clear() {
		CONTEXT.remove();
	}

}
