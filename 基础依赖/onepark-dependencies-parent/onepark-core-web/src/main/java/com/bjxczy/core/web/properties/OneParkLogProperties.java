package com.bjxczy.core.web.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@ConfigurationProperties(prefix = "onepark.log")
public class OneParkLogProperties {
	
	private Boolean enable = false;
	
	private Boolean showLog = false;

}
