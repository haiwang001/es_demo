package com.bjxczy.core.web.privacy;

import java.util.List;

import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.utils.SysUserUtils;

import lombok.Data;

@Data
public class MaskingConfig {
	
	private Integer value;
	
	private List<String> roles;
	
	private boolean rolesContains(UserInformation user) {
		if (user == null) {
			return false;
		}
		return roles.stream().anyMatch(e -> user.getRoles().contains(e));
	}
	
	private boolean extracted(UserInformation user, Integer value) {
		boolean bool = this.value == value;
		if (SysUserUtils.isAdmin(user)) {
			bool = false;
		}
		if (rolesContains(user)) {
			bool = false;
		}
		return bool;
	}

	/**
	 * 判断是否需后端脱敏
	 * @param user
	 * @return
	 */
	public boolean needMasking(UserInformation user) {
		return extracted(user, 3);
	}

	/**
	 * 判断是否需前端脱敏
	 * @param user
	 * @return
	 */
	public Boolean frontendMasking(UserInformation user) {
		return extracted(user, 2);
	}

}
