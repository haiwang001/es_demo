package com.bjxczy.core.web.mybatis;

import java.sql.SQLException;
import java.util.Date;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;

import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.bjxczy.core.web.base.LogicDelete;
import com.bjxczy.core.web.base.UserOperator;
import com.bjxczy.onepark.common.context.LoginUserContextHolder;

public class UserOperatorInterceptor implements InnerInterceptor {

	@Override
	public void beforeUpdate(Executor executor, MappedStatement ms, Object parameter) throws SQLException {
		if (parameter instanceof LogicDelete) {
			LogicDelete entity = (LogicDelete) parameter;
			if (ms.getSqlCommandType() == SqlCommandType.INSERT) {
				entity.setDelFlag(0);
			}
		}
		if (parameter instanceof UserOperator) {
			UserOperator entity = (UserOperator) parameter;
			String operator = LoginUserContextHolder.getValue(u -> u.getStaffName());
			// 读取登录用户信息，并设置操作人
			if (operator != null && entity.getOperator() == null) {
				entity.setOperator(operator);
			}
			switch (ms.getSqlCommandType()) {
			case INSERT:
				entity.setCreateTime(new Date());
				entity.setUpdateTime(new Date());
				break;
			case UPDATE:
			case DELETE:
				entity.setUpdateTime(new Date());
				break;
			default:
				break;
			}
		}
	}

}
