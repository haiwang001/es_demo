package com.bjxczy.onepark.common;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.manager.HikvisionApiProxy;
import com.bjxczy.onepark.common.manager.HikvisionBody;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;

@SpringBootTest
public class HikApiProxyTest {

	@Autowired
	private HikvisionApiProxy proxy;
	@Autowired
	private HikArtemisHttpUtils hikArtemisHttpUtils;

	@Test
	public void getDeviceTest() {
		String resp = proxy.post("/artemis/api/resource/v1/cameras").setBody("pageNo", 1).setBody("pageSize", 100)
				.setBody("treeCode", 0).execute().toResponseString();
		System.out.println(resp);
	}
	
	@Test
	public void querySub() {
		HikvisionBody body = proxy.post("/artemis/api/eventService/v1/eventSubscriptionView").execute()
				.toHikvisionBody();
		System.out.println(body.getData());
	}

	@Test
	public void cancel() {
		List<Integer> cancel = Arrays.asList(131330, 131588);
		String resp = proxy.post("/artemis/api/eventService/v1/eventUnSubscriptionByEventTypes")
				.setBody("eventTypes", cancel)
				.execute()
				.toResponseString();
		System.out.println(resp);
	}

}
