package com.bjxczy.onepark.car.service;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.model.car.CarAlarmInfo;
import com.bjxczy.onepark.common.model.car.ParkBlackWhiteListInfo;

public interface BlackListService {
    JSONObject removeBlackList(CarAlarmInfo info);
    JSONObject createBlackList(CarAlarmInfo info);
    JSONObject testInterface(JSONObject jsonObject);
}
