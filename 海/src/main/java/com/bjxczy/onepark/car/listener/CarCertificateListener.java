package com.bjxczy.onepark.car.listener;

import com.bjxczy.core.rabbitmq.supports.CarBaseListener;
import com.bjxczy.onepark.car.service.CarCertificateService;
import com.bjxczy.onepark.common.model.car.CarCertificate;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Auther: Administrator
 * @Date: 2023/3/27 0027 14:06
 * @Description: CarCertificateListener
 * @Version 1.0.0
 */
@Component
public class CarCertificateListener extends CarBaseListener {

    @Autowired
    private CarCertificateService carCertificateService;

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "certificate_add_car_certificate",durable = "true"),
                    key = ADD_CAR_CERTIFICATE_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME,type = ExchangeTypes.DIRECT)

            )
    })
    @Override
    public void addCarCertificateHandler(CarCertificate carCertificate, Channel channel, Message message) {
        this.confirm(()->{carCertificateService.addCarCertificate(carCertificate); return true;},channel,message);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "certificate_del_car_certificate",durable = "true"),
                    key = DEL_CAR_CERTIFICATE_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME,type = ExchangeTypes.DIRECT)

            )
    })
    @Override
    public void delCarCertificateHandler(CarCertificate carCertificate, Channel channel, Message message) {
        this.confirm(()->{carCertificateService.delCarCertificate(carCertificate);return true;},channel,message);
    }
}
