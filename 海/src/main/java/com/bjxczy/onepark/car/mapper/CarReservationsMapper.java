package com.bjxczy.onepark.car.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.car.entity.CarReservationsPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Auther: Administrator
 * @Date: 2023/3/23 0023 10:21
 * @Description: CarReservationsMapper
 * @Version 1.0.0
 */
@Mapper
public interface CarReservationsMapper extends BaseMapper<CarReservationsPo> {
}
