package com.bjxczy.onepark.car.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.onepark.car.entity.BlackListPO;
import com.bjxczy.onepark.car.mapper.BlackListMapper;
import com.bjxczy.onepark.car.service.BlackListService;
import com.bjxczy.onepark.common.model.car.CarAlarmInfo;
import com.bjxczy.onepark.common.model.car.ParkBlackWhiteListInfo;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class BlackListServiceImpl implements BlackListService {


    private static final String ARTEMIS = "/artemis";
    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;
    @Autowired
    private BlackListMapper blackListMapper;


    @Override
    public JSONObject removeBlackList(CarAlarmInfo info) {
        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/alarmCar/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        //查询中间表,获取海康id
        BlackListPO blacklist = blackListMapper.selectOne(Wrappers.<BlackListPO>lambdaQuery().eq(BlackListPO::getLocalId, info.getId()));
        if (ObjectUtils.isEmpty(blacklist)){
            return new JSONObject();
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("alarmSyscodes",blacklist.getHikId());
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康删除车辆黑名单:{}",resJSONObject.getString("msg"));
            //避免mq重复消费，只打日志
            //throw new RuntimeException("海康删除车辆黑名单失败，返回" + resJSONObject.getString("msg"));
        }else {
            log.info("海康删除车辆黑名单:"+resJSONObject);
            blackListMapper.delete(Wrappers.<BlackListPO>lambdaQuery().eq(BlackListPO::getLocalId, info.getId()));
        }
        return resJSONObject;
    }

    @Override
    public JSONObject createBlackList(CarAlarmInfo info) {
        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/alarmCar/addition";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(info));
        jsonObject.remove("id");
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康添加车辆黑名单:{}",resJSONObject.getString("msg"));
            //throw new RuntimeException("海康添加车辆黑名单失败，返回" + resJSONObject.getString("msg"));
        }else {
            log.info("海康添加车辆黑名单:"+resJSONObject);

            String string = resJSONObject.getJSONObject("data").getString("alarmSyscode");
            BlackListPO blackListPO = new BlackListPO();
            blackListPO.setLocalId(info.getId());
            blackListPO.setHikId(string);
            blackListMapper.insert(blackListPO);
        }
        return resJSONObject;
    }

    @Override
    public JSONObject testInterface(JSONObject jsonObject) {

        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/car/charge";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康添加车证:{}",resJSONObject.getString("msg"));
            //throw new RuntimeException("海康添加车证失败，返回" + resJSONObject.getString("msg"));
        }else {
            log.info("海康添加车证:"+resJSONObject);
        }
        return resJSONObject;
    }

}
