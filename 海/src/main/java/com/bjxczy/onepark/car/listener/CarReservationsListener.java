package com.bjxczy.onepark.car.listener;

import com.bjxczy.core.rabbitmq.supports.CarBaseListener;
import com.bjxczy.onepark.car.service.VehicleManagementService;
import com.bjxczy.onepark.common.model.car.CarReservations;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Auther: qinyuan
 * @Date: 2023/3/27  13:23
 * @Description: 临时车监听
 * @Version 1.0.0
 */
@Component
public class CarReservationsListener extends CarBaseListener {

    @Autowired
    private VehicleManagementService vehicleManagementService;

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "certificate_add_car_reservations",durable = "true"),
                    key = ADD_CAR_RESERVATIONS_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME,type = ExchangeTypes.DIRECT)

            )
    })
    @Override
    public void addCarReservationsHandler(CarReservations carReservations, Channel channel, Message message) {
        this.confirm(()->{vehicleManagementService.addCarReservations(carReservations); return true;},channel,message);
    }
    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "certificate_del_car_reservations",durable = "true"),
                    key = DEL_CAR_RESERVATIONS_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME,type = ExchangeTypes.DIRECT)

            )
    })
    @Override
    public void delCarReservationsHandler(CarReservations carReservations, Channel channel, Message message) {
        this.confirm(()->{vehicleManagementService.delCarReservations(carReservations); return true;},channel,message);
    }
}
