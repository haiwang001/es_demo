package com.bjxczy.onepark.car.listener;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.supports.CarBaseListener;
import com.bjxczy.onepark.car.service.BlackListService;
import com.bjxczy.onepark.common.model.car.CarAlarmInfo;
import com.bjxczy.onepark.common.model.car.ParkBlackWhiteListInfo;
import com.bjxczy.onepark.common.model.face.FaceInformation;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BlackListListener  extends CarBaseListener {

    @Autowired
    private BlackListService blackListService;


    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "certificate_create_blackList_queue", durable = "true"),
                    // routing key
                    key = CREATE_BLACKLIST_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void createBlackListEvent(CarAlarmInfo info, Channel channel, Message message) {
        log.info("[CREATE_BLACKLIST_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
            blackListService.createBlackList(info);
            this.confirm(()->true,channel,message);
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "certificate_remove_blackList_queue", durable = "true"),
                    // routing key
                    key = REMOVE_BLACKLIST_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void removeBlackListEvent(CarAlarmInfo info, Channel channel, Message message) {
        log.info("[REMOVE_BLACKLIST_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        blackListService.removeBlackList(info);
        this.confirm(()->true,channel,message);
    }
}
