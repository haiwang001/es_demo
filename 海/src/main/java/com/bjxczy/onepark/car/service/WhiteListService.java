package com.bjxczy.onepark.car.service;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.model.car.ParkBlackWhiteListInfo;

public interface WhiteListService {
    JSONObject removeWhiteList(ParkBlackWhiteListInfo info);
    JSONObject createWhiteList(ParkBlackWhiteListInfo info);
    JSONObject updateWhiteList(ParkBlackWhiteListInfo info);
}
