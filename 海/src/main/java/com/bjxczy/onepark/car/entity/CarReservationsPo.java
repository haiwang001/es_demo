package com.bjxczy.onepark.car.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Auther: qinyuan
 * @Date: 2023/3/23 10:15
 * @Description: 车辆预约关联实体
 * @Version 1.0.0
 */
@Data
@TableName("park_car_reservations")
public class CarReservationsPo {
    /**
    * 描述：业务id
    * 是否必填：true
    **/
    private Integer localId;
    /**
    * 描述：海康返回预约单号
    * 是否必填：true
    **/
    private String hikId;
}
