package com.bjxczy.onepark.car.listener;

import com.bjxczy.core.rabbitmq.supports.CarBaseListener;
import com.bjxczy.onepark.car.service.VehicleManagementService;
import com.bjxczy.onepark.common.model.car.HikCarInfo;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Auther: Administrator
 * @Date: 2023/3/27 0027 13:57
 * @Description: HikCarListener
 * @Version 1.0.0
 */
@Component
public class HikCarListener extends CarBaseListener {

    @Autowired
    private VehicleManagementService vehicleManagementService;

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "certificate_add_hik_car",durable = "true"),
                    key = ADD_HIK_CAR_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME,type = ExchangeTypes.DIRECT)

            )
    })
    @Override
    public void addHikCarHandler(HikCarInfo hikCarInfo, Channel channel, Message message) {
        this.confirm(()->{vehicleManagementService.addHikCar(hikCarInfo);return true;},channel,message);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "certificate_del_hik_car",durable = "true"),
                    key = DEL_HIK_CAR_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME,type = ExchangeTypes.DIRECT)

            )
    })
    @Override
    public void delHikCarHandler(HikCarInfo hikCarInfo, Channel channel, Message message) {
        this.confirm(()->{vehicleManagementService.delHikCar(hikCarInfo);return true;},channel,message);
    }
}
