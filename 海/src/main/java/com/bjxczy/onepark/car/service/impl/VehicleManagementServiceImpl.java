package com.bjxczy.onepark.car.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.onepark.car.entity.CarReservationsPo;
import com.bjxczy.onepark.car.entity.HikCarInfoPo;
import com.bjxczy.onepark.car.mapper.CarReservationsMapper;
import com.bjxczy.onepark.car.mapper.HikCarInfoMapper;
import com.bjxczy.onepark.car.service.VehicleManagementService;
import com.bjxczy.onepark.common.model.car.CarCertificate;
import com.bjxczy.onepark.common.model.car.CarReservations;
import com.bjxczy.onepark.common.model.car.HikCarInfo;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: Administrator
 * @Date: 2023/3/22 0022 14:16
 * @Description: VehicleManagementServiceImpl
 * @Version 1.0.0
 */
@Service
@Slf4j
public class VehicleManagementServiceImpl implements VehicleManagementService {

    private static final String ARTEMIS = "/artemis";
    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;

    @Autowired
    private CarReservationsMapper carReservationsMapper;

    @Autowired
    private HikCarInfoMapper hikCarInfoMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    /**
     * @Author qinyuan
     * @Description 新增临时放行车辆
     * @Date 13:38 2023/3/22
     * @Param carCertificate
     * @return JSONObject
     **/
    @Override
    public JSONObject addCarReservations(CarReservations carReservations) {
        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/parkingSpace/reservations/addition";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(carReservations));
        jsonObject.remove("id");
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code")) || !("0".equals(resJSONObject.getString("code")))) {
            log.error("海康新增临时放行车辆:{}", resJSONObject.getString("msg"));
            //throw new RuntimeException("海康新增临时放行车辆失败，返回" + resJSONObject.getString("msg"));
        }else {
            log.info("海康新增临时放行车辆:" + resJSONObject);
            //存储海康预约单号和临时车辆id的关联关系
            String string = resJSONObject.getJSONObject("data").getString("reserveOrderNo");
            CarReservationsPo carReservationsPo = new CarReservationsPo();
            carReservationsPo.setLocalId(carReservations.getId());
            carReservationsPo.setHikId(string);
            carReservationsMapper.insert(carReservationsPo);
        }
        return resJSONObject;
    }

    /**
     * @return JSONObject
     * @Author qinyuan
     * @Description 删除临时放行车辆
     * @Date 13:38 2023/3/22
     * @Param carCertificate
     **/
    @Override
    public JSONObject delCarReservations(CarReservations carReservations) {
        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/parkingSpace/reservations/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        CarReservationsPo selectOne = carReservationsMapper.selectOne(Wrappers.<CarReservationsPo>lambdaQuery().eq(CarReservationsPo::getLocalId, carReservations.getId()));
        if (ObjectUtils.isEmpty(selectOne)){
            return new JSONObject();
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("reserveOrderNo", selectOne.getHikId());
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code")) && !("0".equals(resJSONObject.getString("code")))) {
            log.error("海康删除临时放行车辆:{}", resJSONObject.getString("msg"));
            //throw new RuntimeException("海康删除临时放行车辆失败，返回" + resJSONObject.getString("msg"));
        }else {
            log.info("海康删除临时放行车辆:" + resJSONObject);
            carReservationsMapper.delete(Wrappers.<CarReservationsPo>lambdaQuery().eq(CarReservationsPo::getLocalId, carReservations.getId()));
        }
        return resJSONObject;
    }

    /**
     * @return JSONObject
     * @Author qinyuan
     * @Description 添加海康车辆--海康平台提供的是批量接口，此版本没有批量需求，故将单个实体传到list中进行车辆添加
     * @Date 13:38 2023/3/22
     * @Param hikCarInfo
     **/
    @Override
    public JSONObject addHikCar(HikCarInfo hikCarInfo) {
        final String VechicleDataApi = ARTEMIS + "/api/resource/v1/vehicle/batch/add";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(hikCarInfo);
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonArray.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code")) && (CollectionUtils.isNotEmpty(resJSONObject.getJSONObject("data").getJSONArray("failures")))) {
            log.error("海康添加车辆:{}", resJSONObject.getString("msg"));
            //throw new RuntimeException("海康添加车辆失败，返回" + resJSONObject.getString("msg"));
        }else {
            log.info("海康添加车辆:" + resJSONObject);
            JSONArray successJsonArray = resJSONObject.getJSONObject("data").getJSONArray("successes");
            for (Object o : successJsonArray) {
                JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(o));
                HikCarInfoPo hikCarInfoPo = new HikCarInfoPo();
                hikCarInfoPo.setHikId(jsonObject.getString("vehicleId"));
                hikCarInfoPo.setLocalId(jsonObject.getInteger("clientId"));
                hikCarInfoMapper.insert(hikCarInfoPo);
            }
        }
        return resJSONObject;
    }

    /**
     * @return JSONObject
     * @Author qinyuan
     * @Description 删除海康车辆--海康平台提供的是批量接口，此版本没有批量需求，故将单个实体传到list中进行车辆删除
     * @Date 13:38 2023/3/22
     * @Param hikCarInfo
     **/
    @Override
    public JSONObject delHikCar(HikCarInfo hikCarInfo) {
        final String VechicleDataApi = ARTEMIS + "/api/resource/v1/vehicle/batch/delete";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        HikCarInfoPo hikCarInfoPo = hikCarInfoMapper.selectOne(Wrappers.<HikCarInfoPo>lambdaQuery().eq(HikCarInfoPo::getLocalId, hikCarInfo.getClientId()));
        if (ObjectUtils.isEmpty(hikCarInfoPo)){
            return new JSONObject();
        }
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(hikCarInfoPo.getHikId());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("vehicleIds",jsonArray);
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code")) && (CollectionUtils.isNotEmpty(resJSONObject.getJSONArray("data")))) {
            log.error("海康删除车辆:{}", resJSONObject.getString("msg"));
            //throw new RuntimeException("海康删除车辆失败，返回" + resJSONObject.getString("msg"));
        }else {
            //删除本地关联数据
            hikCarInfoMapper.delete(Wrappers.<HikCarInfoPo>lambdaQuery().eq(HikCarInfoPo::getLocalId, hikCarInfo.getClientId()));
            log.info("海康删除车辆:" + resJSONObject);
        }
        return resJSONObject;
    }

    @Override
    public JSONObject editHikCar(HikCarInfo hikCarInfo) {
        final String VechicleDataApi = ARTEMIS + "/api/resource/v1/vehicle/single/update";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        HikCarInfoPo hikCarInfoPo = hikCarInfoMapper.selectOne(Wrappers.<HikCarInfoPo>lambdaQuery().eq(HikCarInfoPo::getLocalId, hikCarInfo.getClientId()));
        if (ObjectUtils.isEmpty(hikCarInfoPo)){
            return new JSONObject();
        }
        hikCarInfo.setVehicleId(hikCarInfoPo.getHikId());
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(hikCarInfo));
        jsonObject.remove("clientId");
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code")) && (CollectionUtils.isNotEmpty(resJSONObject.getJSONArray("data")))) {
            log.error("海康编辑车辆:{}", resJSONObject.getString("msg"));
            //throw new RuntimeException("海康编辑车辆失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康编辑车辆:" + resJSONObject);
        return resJSONObject;
    }

    /**
     * @return JSONObject
     * @Author qinyuan
     * @Description 查询停车库剩余车位数
     * @Date 13:38 2023/3/22
     * @Param carCertificate
     **/
    @Override
    public JSONObject queryCarport(CarCertificate carCertificate) {
        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/park/remainSpaceNum";

        JSONObject parm = new JSONObject();
        parm.put("parkSyscode", carCertificate.getParkSyscode());
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, parm.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康查询停车库剩余车位数:{}", resJSONObject.getString("msg"));
            //throw new RuntimeException("海康查询停车库剩余车位数失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康查询停车库剩余车位数:" + resJSONObject);
        return resJSONObject;
    }

    @Override
    public JSONObject queryParkCode() {
        final String VechicleDataApi = ARTEMIS + "/api/resource/v1/park/parkList";
        JSONObject parm = new JSONObject();
        parm.put("parkIndexCodes", null);
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, parm.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康车库查询:{}", resJSONObject.getString("msg"));
            //throw new RuntimeException("海康查询车库失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康查询停车库:" + resJSONObject);
        return resJSONObject;
    }

}
