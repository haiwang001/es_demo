package com.bjxczy.onepark.car.interfaces.feign;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.feign.client.car.HikCarPortFeignClient;
import com.bjxczy.onepark.car.service.VehicleManagementService;
import com.bjxczy.onepark.common.model.car.CarCertificate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Auther: qinyuan
 * @Date: 2023/3/31 0031 17:53
 * @Description: 海康查询车库车位信息
 * @Version 1.0.0
 */
@Controller
@ResponseBody
public class HikCarPortFeignController implements HikCarPortFeignClient {

    @Autowired
    private VehicleManagementService vehicleManagementService;
    @Override
    public JSONObject queryCarport(CarCertificate carCertificate) {
        return vehicleManagementService.queryCarport(carCertificate);
    }
}
