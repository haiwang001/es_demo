package com.bjxczy.onepark.car.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("park_black_list")
public class BlackListPO {
    private Integer localId;
    private String hikId;
    private Integer tenantId;
}
