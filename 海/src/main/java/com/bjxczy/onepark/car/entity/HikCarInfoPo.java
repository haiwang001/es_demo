package com.bjxczy.onepark.car.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Auther: qinyuan
 * @Date: 2023/3/23 0023 14:45
 * @Description: 海康车辆关联表
 * @Version 1.0.0
 */
@Data
@TableName("park_car_info")
public class HikCarInfoPo {
    /**
     * 描述：业务id
     * 是否必填：true
     **/
    private Integer localId;
    /**
     * 描述：海康返回预约单号
     * 是否必填：true
     **/
    private String hikId;
}
