package com.bjxczy.onepark.car.service;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.model.car.CarCertificate;
import com.bjxczy.onepark.common.model.car.CarReservations;
import com.bjxczy.onepark.common.model.car.HikCarInfo;

/**
 * @Auther: qinyuan
 * @Date: 2023/3/22 14:14
 * @Description: 临时车辆管理
 * @Version 1.0.0
 */
public interface VehicleManagementService {
    JSONObject addCarReservations(CarReservations carReservations);
    JSONObject delCarReservations(CarReservations carReservations);
    JSONObject addHikCar(HikCarInfo hikCarInfo);
    JSONObject delHikCar(HikCarInfo hikCarInfo);
    JSONObject editHikCar(HikCarInfo hikCarInfo);
    JSONObject queryCarport(CarCertificate carCertificate);

    JSONObject queryParkCode();
}
