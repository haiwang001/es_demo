package com.bjxczy.onepark.car.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("park_white_list")
public class WhiteListPO {
    private Integer localId;
    private String hikId;
    private Integer tenantId;
}
