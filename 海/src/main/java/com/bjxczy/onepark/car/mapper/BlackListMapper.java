package com.bjxczy.onepark.car.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.car.entity.BlackListPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BlackListMapper extends BaseMapper<BlackListPO> {
}
