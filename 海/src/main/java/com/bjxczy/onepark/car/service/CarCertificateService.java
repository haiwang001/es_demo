package com.bjxczy.onepark.car.service;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.model.car.CarCertificate;

public interface CarCertificateService {
        JSONObject addCarCertificate(CarCertificate carCertificate);
        JSONObject delCarCertificate(CarCertificate carCertificate);
        JSONObject editCarCertificate(CarCertificate carCertificate);
}
