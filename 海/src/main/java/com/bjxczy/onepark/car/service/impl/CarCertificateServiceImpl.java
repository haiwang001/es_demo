package com.bjxczy.onepark.car.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.car.service.CarCertificateService;
import com.bjxczy.onepark.common.model.car.CarCertificate;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class CarCertificateServiceImpl implements CarCertificateService {

    private static final String ARTEMIS = "/artemis";
    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;
    
    /**
    * @Author qinyuan
    * @Description 新增车证
    * @Date 13:38 2023/3/22
    * @Param carCertificate
    * @return JSONObject
    **/
    @Override
    public JSONObject addCarCertificate(CarCertificate carCertificate){
        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/car/charge";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, JSONObject.toJSONString(carCertificate), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康添加车证:{}",resJSONObject.getString("msg"));
            //throw new RuntimeException("海康添加车证失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康添加车证:"+resJSONObject);
        return resJSONObject;
    }
    /**
     * @Author qinyuan
     * @Description 删除车证
     * @Date 13:38 2023/3/22
     * @Param carCertificate
     * @return JSONObject
     **/
    @Override
    public JSONObject delCarCertificate(CarCertificate carCertificate) {
        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/car/charge/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, JSONObject.toJSONString(carCertificate), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康删除车证:{}",resJSONObject.getString("msg"));
            //throw new RuntimeException("海康删除车证失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康删除车证:"+resJSONObject);
        return resJSONObject;
    }
    /**
     * @Author qinyuan
     * @Description 修改车证套餐
     * @Date 13:38 2023/3/22
     * @Param carCertificate
     * @return JSONObject
     **/
    @Override
    public JSONObject editCarCertificate(CarCertificate carCertificate) {
        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/car/charge";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, JSONObject.toJSONString(carCertificate), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康修改车证套餐:{}",resJSONObject.getString("msg"));
            //throw new RuntimeException("海康修改车证套餐失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康修改车证套餐:"+resJSONObject);
        return resJSONObject;
    }
}
