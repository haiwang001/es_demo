package com.bjxczy.onepark.car.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.car.mapper.WhiteListMapper;
import com.bjxczy.onepark.car.service.WhiteListService;
import com.bjxczy.onepark.common.model.car.ParkBlackWhiteListInfo;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class WhiteListServiceImpl implements WhiteListService {


    private static final String ARTEMIS = "/artemis";
    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;
    @Autowired
    private WhiteListMapper whiteListMapper;


    @Override
    public JSONObject removeWhiteList(ParkBlackWhiteListInfo info) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("parkSyscode",info.getParkCode());
        jsonObject.put("plateNo",info.getCarNumber());

        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/alarmCar/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康取消车辆包期白名单:{}",resJSONObject.getString("msg"));
            //throw new RuntimeException("海康取消车辆白名单失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康取消车辆白名单:"+resJSONObject);
        return resJSONObject;
    }

    @Override
    public JSONObject createWhiteList(ParkBlackWhiteListInfo info) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ");
        String beginTime = null;
        String endTime = null;
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ");
            Date parse = format1.parse(info.getEndDate());
            Date parse1 = format1.parse(info.getBeginDate());
            endTime = format.format(parse);
            beginTime = format.format(parse1);
        } catch (ParseException e) {
            e.printStackTrace();
        }



        JSONObject jsonObject = new JSONObject();
        jsonObject.put("parkSyscode",info.getParkCode());
        jsonObject.put("plateNo",info.getCarNumber());
        jsonObject.put("startTime",beginTime);
        jsonObject.put("endTime",endTime);

        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/car/charge";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康车辆包期白名单:{}",resJSONObject.getString("msg"));
            //throw new RuntimeException("海康添加车辆白名单失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康添加车辆白名单:"+resJSONObject);
        return resJSONObject;
    }

    @Override
    public JSONObject updateWhiteList(ParkBlackWhiteListInfo info) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ");
        String beginTime = null;
        String endTime = null;
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ");
            Date parse = format1.parse(info.getEndDate());
            Date parse1 = format1.parse(info.getBeginDate());
            endTime = format.format(parse);
            beginTime = format.format(parse1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("parkSyscode",info.getParkCode());
        jsonObject.put("plateNo",info.getCarNumber());
        jsonObject.put("startTime",beginTime);
        jsonObject.put("endTime",endTime);

        final String VechicleDataApi = ARTEMIS + "/api/pms/v1/car/charge";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康修改车辆包期白名单:{}",resJSONObject.getString("msg"));
            //throw new RuntimeException("海康修改车辆白名单失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康修改车辆白名单:"+resJSONObject);
        return resJSONObject;
    }
}
