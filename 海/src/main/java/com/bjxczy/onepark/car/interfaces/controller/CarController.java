package com.bjxczy.onepark.car.interfaces.controller;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.car.service.BlackListService;
import com.bjxczy.onepark.car.service.CarCertificateService;
import com.bjxczy.onepark.car.service.VehicleManagementService;
import com.bjxczy.onepark.common.model.car.CarAlarmInfo;
import com.bjxczy.onepark.common.model.car.CarCertificate;
import com.bjxczy.onepark.common.model.car.CarReservations;
import com.bjxczy.onepark.common.model.car.HikCarInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@ApiResourceController
public class CarController {
    @Autowired
    private BlackListService blackListService;

    @Autowired
    private VehicleManagementService vehicleManagementService;

    @Autowired
    private CarCertificateService carCertificateService;

    @PostMapping("/hik/car/createBlackList")
    public JSONObject createBlackList(@RequestBody CarAlarmInfo info){
        return blackListService.createBlackList(info);
    }
    /**
    * @Author qinyuan
    * @Description 调用海康查询停车库剩余车位数
    * @Date 17:38 2023/3/22
    * @Param carCertificate
    * @return JSONObject
     * parkSyscode			停车库唯一标识码
     * parkName			    停车库名称
     * parentParkSyscode	父停车库唯一标识
     * totalPlace			停车库车位总数
     * totalPermPlace		停车库固定车位总数
     * totalReservePlace	停车库预约车位总数
     * leftPlace			停车库车位剩余数
     * leftPermPlace		停车库固定车位剩余数
     * leftReservePlace	    停车库预约车位剩余数
    **/
    @PostMapping("/hik/car/queryCarport")
    public JSONObject queryCarport(@RequestBody CarCertificate carCertificate ){
        return vehicleManagementService.queryCarport(carCertificate);
    }


    /*----------------------------------------------------------------------------
    *                             以下为测试接口所需
    * ----------------------------------------------------------------------------
    * */
    @PostMapping("/hik/car/addCarReservations")
    public void addCarReservations(@RequestBody CarReservations carReservations ){
        vehicleManagementService.addCarReservations(carReservations);
    }

    @PostMapping("/hik/car/delCarReservations")
    public void delCarReservations(@RequestBody CarReservations carReservations ){
        vehicleManagementService.delCarReservations(carReservations);
    }

    @PostMapping("/hik/car/addHikCar")
    public void addHikCar(@RequestBody HikCarInfo hikCarInfo ){
        vehicleManagementService.addHikCar(hikCarInfo);
    }

    @PostMapping("/hik/car/editHikCar")
    public void editHikCar(@RequestBody HikCarInfo hikCarInfo ){
        vehicleManagementService.editHikCar(hikCarInfo);
    }

    @PostMapping("/hik/car/delHikCar")
    public void delHikCar(@RequestBody HikCarInfo hikCarInfo ){
        vehicleManagementService.delHikCar(hikCarInfo);
    }

    @PostMapping("/hik/car/addCarCertificate")
    public void addCarCertificate(@RequestBody CarCertificate carCertificate ){
        carCertificateService.addCarCertificate(carCertificate);
    }

    @PostMapping("/hik/car/delCarCertificate")
    public void delCarCertificate(@RequestBody CarCertificate carCertificate ){
        carCertificateService.delCarCertificate(carCertificate);
    }

    @PostMapping("/hik/car/removeBlackList")
    public void removeBlackList(@RequestBody CarAlarmInfo info ){
        blackListService.removeBlackList(info);
    }

    @PostMapping("/hik/car/queryParkCode")
    public JSONObject queryParkCode(){
        return vehicleManagementService.queryParkCode();
    }


}
