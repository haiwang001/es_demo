package com.bjxczy.onepark.car.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.car.entity.HikCarInfoPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Auther: Administrator
 * @Date: 2023/3/23 0023 14:51
 * @Description: HikCarInfoMapper
 * @Version 1.0.0
 */
@Mapper
public interface HikCarInfoMapper extends BaseMapper<HikCarInfoPo> {
}
