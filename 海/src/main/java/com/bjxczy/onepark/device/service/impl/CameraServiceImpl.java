package com.bjxczy.onepark.device.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.onepark.common.manager.HikvisionApiProxy;
import com.bjxczy.onepark.common.manager.HikvisionApiProxy.HikvisionRequestBuilder;
import com.bjxczy.onepark.common.manager.HikvisionBody;
import com.bjxczy.onepark.common.model.hikvision.GetPlaybackURLsDTO;
import com.bjxczy.onepark.common.model.hikvision.GetPreviewURLsDTO;
import com.bjxczy.onepark.common.model.security.CameraInformation;
import com.bjxczy.onepark.common.model.security.CameraOnlineInfo;
import com.bjxczy.onepark.common.model.security.QueryCameraOnLineCommand;
import com.bjxczy.onepark.common.resp.Payload;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.device.service.CameraService;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.ListUtil;

@Service
public class CameraServiceImpl implements CameraService {

	@Autowired
	private HikvisionApiProxy apiProxy;
	@Autowired
	private DictService dictService;

	@Override
	public List<CameraInformation> cameras() {
		// 接口文档：https://open.hikvision.com/docs/docId?productId=5c67f1e2f05948198c909700&version=%2F60df74fdc6f24041ac3d2d7f81c32325&tagPath=API%E5%88%97%E8%A1%A8-%E8%A7%86%E9%A2%91%E4%B8%9A%E5%8A%A1-%E8%A7%86%E9%A2%91%E8%B5%84%E6%BA%90#a11a856d
		HikvisionBody body = apiProxy.post("/artemis/api/resource/v1/cameras")
				.setBody("pageNo", 1)
				.setBody("pageSize", 1000)
				.setBody("treeCode", 0)
				.execute().toHikvisionBody();
		JSONArray list = body.getData().getJSONArray("list");
		List<CameraInformation> infoList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			JSONObject obj = list.getJSONObject(i);
			CameraInformation info = new CameraInformation();
			info.setCameraName(obj.getString("cameraName"));
			info.setCameraIndexCode(obj.getString("cameraIndexCode"));
			info.setCameraType(obj.getInteger("cameraType"));
			info.setCameraTypeName(obj.getString("cameraTypeName"));
			infoList.add(info);
		}
		return infoList;
	}

	@Override
	public List<CameraOnlineInfo> cameraOnLine(QueryCameraOnLineCommand command) {
		List<String> codes = command.getIndexCodes();
		List<CameraOnlineInfo> data = new ArrayList<>();
		// 分割List，接口限制每次最多查询500条数据
		List<List<String>> splitList = ListUtil.split(codes, 500);
		for (List<String> subList : splitList) {
			HikvisionBody body = apiProxy.post("/artemis/api/nms/v1/online/camera/get")
					.setBody("pageNo", 1)
					.setBody("pageSize", 500)
					.setBody("indexCodes", subList)
					.execute().toHikvisionBody();
			JSONArray rlist = body.getData().getJSONArray("list");
			for (int i = 0; i < rlist.size(); i++) {
				JSONObject obj = rlist.getJSONObject(i);
				data.add(new CameraOnlineInfo(obj.getString("indexCode"), obj.getInteger("online")));
			}
		}
		return data;
	}

	@Override
	public R<Payload> previewURLs(GetPreviewURLsDTO dto) {
		Map<String, Object> bodys = BeanUtil.beanToMap(dto);
		HikvisionRequestBuilder builder = apiProxy.post("/artemis/api/video/v2/cameras/previewURLs");
		builder.setBodys(bodys);
		HikvisionBody response = builder.execute().toHikvisionBody();
		R<Payload> result = response.toRPayload(dictService, "CameraError");
		return result;
	}

	@Override
	public R<Payload> playbackURLs(GetPlaybackURLsDTO dto) {
		Map<String, Object> bodys = BeanUtil.beanToMap(dto);
		HikvisionRequestBuilder builder = apiProxy.post("/artemis/api/video/v2/cameras/playbackURLs");
		builder.setBodys(bodys);
		HikvisionBody response = builder.execute().toHikvisionBody();
		R<Payload> result = response.toRPayload(dictService, "CameraError");
		return result;
	}

}
