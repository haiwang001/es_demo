package com.bjxczy.onepark.device.interfaces.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bjxczy.onepark.common.model.hikvision.GetPlaybackURLsDTO;
import com.bjxczy.onepark.common.model.hikvision.GetPreviewURLsDTO;
import com.bjxczy.onepark.common.model.security.CameraInformation;
import com.bjxczy.onepark.common.model.security.CameraOnlineInfo;
import com.bjxczy.onepark.common.model.security.QueryCameraOnLineCommand;
import com.bjxczy.onepark.common.resp.Payload;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.device.service.CameraService;

@Controller
@ResponseBody
@RequestMapping("/resource")
public class CameraController {

	@Autowired
	private CameraService cameraService;

	@GetMapping("/cameras")
	public List<CameraInformation> cameras() {
		return cameraService.cameras();
	}

	@PostMapping("/cameras/online/get")
	public List<CameraOnlineInfo> cameraOnLine(@RequestBody QueryCameraOnLineCommand command) {
		return cameraService.cameraOnLine(command);
	}
	
	@PostMapping("/cameras/previewURLs")
	public R<Payload> previewURLs(@RequestBody GetPreviewURLsDTO dto) {
		return cameraService.previewURLs(dto);
	}
	
	@PostMapping("cameras/playbackURLs")
	public R<Payload> playbackURLs(@RequestBody GetPlaybackURLsDTO dto) {
		return cameraService.playbackURLs(dto);
	}

}
