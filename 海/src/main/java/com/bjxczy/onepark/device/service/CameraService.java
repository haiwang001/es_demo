package com.bjxczy.onepark.device.service;

import java.util.List;

import com.bjxczy.onepark.common.model.hikvision.GetPlaybackURLsDTO;
import com.bjxczy.onepark.common.model.hikvision.GetPreviewURLsDTO;
import com.bjxczy.onepark.common.model.security.CameraInformation;
import com.bjxczy.onepark.common.model.security.CameraOnlineInfo;
import com.bjxczy.onepark.common.model.security.QueryCameraOnLineCommand;
import com.bjxczy.onepark.common.resp.Payload;
import com.bjxczy.onepark.common.resp.R;

public interface CameraService {

	List<CameraInformation> cameras();

	List<CameraOnlineInfo> cameraOnLine(QueryCameraOnLineCommand command);

	R<Payload> previewURLs(GetPreviewURLsDTO dto);

	R<Payload> playbackURLs(GetPlaybackURLsDTO dto);

}
