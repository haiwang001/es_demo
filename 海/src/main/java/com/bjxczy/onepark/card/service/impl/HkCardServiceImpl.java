package com.bjxczy.onepark.card.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.onepark.card.service.HkCardService;
import com.bjxczy.onepark.common.cons.HikApi;
import com.bjxczy.onepark.common.handle.HikSendHandle;
import com.bjxczy.onepark.common.manager.HikvisionApiProxy;
import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.bjxczy.onepark.common.model.voucher.CardInfoTransmit;
import com.bjxczy.onepark.face.entity.StaffFacePO;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikDeviceReturnDto;
import com.google.common.collect.ImmutableList;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Log4j2
public class HkCardServiceImpl implements HkCardService {
    @Autowired
    private HikvisionApiProxy apiProxy;


    @Override
    public void addHikCardEvent(CardInfoTransmit info) {

        Map<String, Object> param = new HashMap<>();
        param.put("cardList", ImmutableList.of(info));
        JSONObject jsonObject = apiProxy.post(HikApi.ARTEMIS + "/api/cis/v1/card/bindings").setBody(param).execute().toJSONObject();
        log.info("[ 开通卡片 海康 ] -> {} ,{}", info, jsonObject);
    }

    @Override
    public void delHikCardEvent(CardInfoTransmit info) {
        Map<String, Object> param = new HashMap<>();
        param.put("cardNumber", info.getCardNo());
        param.put("personId", info.getPersonId());
        JSONObject jsonObject = apiProxy.post(HikApi.ARTEMIS + "/api/cis/v1/card/deletion").setBody(param).execute().toJSONObject();
        log.info("[ 退卡 海康 ] -> {} ,{}", info, jsonObject);
    }
}
