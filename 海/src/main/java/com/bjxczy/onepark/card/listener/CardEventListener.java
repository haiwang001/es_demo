package com.bjxczy.onepark.card.listener;

import com.bjxczy.core.rabbitmq.supports.CardBaseListener;
import com.bjxczy.onepark.card.service.HkCardService;
import com.bjxczy.onepark.common.model.voucher.CardInfoTransmit;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class CardEventListener extends CardBaseListener {

    @Autowired
    private HkCardService cordService;

    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(name = "voucher_to_hik_add_card_sync_queue", durable = "true"),
            key = ADD_HIK_CARD_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME
            ))})
    @Override
    public void addHikCardEvent(CardInfoTransmit info, Channel channel, Message message) {
        try {
            log.info("[订阅事件 -> ADD_CARD_EVENT] = {}", info);
            cordService.addHikCardEvent(info);
            this.confirm(() -> true, channel, message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "voucher_to_hik_del_card_sync_queue", durable = "true"),
                    key = DEL_HIK_CARD_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME)
            )})
    @Override
    public void delHikCardEvent(CardInfoTransmit info, Channel channel, Message message) {
        try {
            log.info("[订阅事件 -> DEL_CARD_EVENT] = {}", info);
            cordService.delHikCardEvent(info);
            this.confirm(() -> true, channel, message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
