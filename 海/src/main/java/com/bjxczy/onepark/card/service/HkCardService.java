package com.bjxczy.onepark.card.service;


import com.bjxczy.onepark.common.model.voucher.CardInfoTransmit;

public interface HkCardService {

    void addHikCardEvent(CardInfoTransmit info);

    void delHikCardEvent(CardInfoTransmit info);
}
