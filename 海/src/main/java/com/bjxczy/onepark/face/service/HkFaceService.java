package com.bjxczy.onepark.face.service;

import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;

import java.util.HashMap;

public interface HkFaceService {
    void addFaceHik(FaceInfoTransmit info) throws Exception;

    void updateFaceHik(FaceInfoTransmit info) throws Exception;

    void deleteFaceHik(FaceInfoTransmit info) throws Exception;
}
