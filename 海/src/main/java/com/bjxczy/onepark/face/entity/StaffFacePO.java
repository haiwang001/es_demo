package com.bjxczy.onepark.face.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("staff_face")
public class StaffFacePO {
    private Integer staffId;
    private String hkFaceId;

    public StaffFacePO(Integer staffId, String hkFaceId) {
        this.staffId = staffId;
        this.hkFaceId = hkFaceId;
    }

    public StaffFacePO() {
    }
}
