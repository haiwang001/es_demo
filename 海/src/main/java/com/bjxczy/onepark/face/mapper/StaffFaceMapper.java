package com.bjxczy.onepark.face.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.face.entity.StaffFacePO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StaffFaceMapper extends BaseMapper<StaffFacePO> {


}
