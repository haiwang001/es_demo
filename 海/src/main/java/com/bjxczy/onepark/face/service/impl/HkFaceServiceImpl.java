package com.bjxczy.onepark.face.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.onepark.common.cons.HikApi;
import com.bjxczy.onepark.common.manager.HikvisionApiProxy;
import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.bjxczy.onepark.face.entity.StaffFacePO;
import com.bjxczy.onepark.face.mapper.StaffFaceMapper;
import com.bjxczy.onepark.common.handle.HikSendHandle;
import com.bjxczy.onepark.face.service.HkFaceService;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikDeviceReturnDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Log4j2
public class HkFaceServiceImpl implements HkFaceService {
    @Autowired
    private HikvisionApiProxy apiProxy;
    @Autowired
    private StaffFaceMapper staffFaceMapper;

    @Override
    public void addFaceHik(FaceInfoTransmit info) throws Exception {
        log.info("[ insertFaceHik ] -> {}", info);
        Map<String, Object> param = new HashMap<>();
        param.put("personId", info.getStaffId());
        param.put("faceData", HikSendHandle.getImageBase(info.getFaceImg()));
        JSONObject resp = apiProxy.post(HikApi.HAIKANG_DEVICE_FACE_ADD_API).setBody(param).execute().toJSONObject();
        HikDeviceReturnDto dto = JSON.parseObject(resp.toString(), HikDeviceReturnDto.class);
        if (dto.getCode().equals("0x00052301")) {
            updateFaceHik(info);
        }else {
            staffFaceMapper.insert(new StaffFacePO(info.getStaffId(),dto.getData().getFaceId()));
        }
    }

    @Override
    public void updateFaceHik(FaceInfoTransmit info) throws Exception {
        log.info("[ updateFaceHik ] -> {}", info);
        LambdaQueryWrapper<StaffFacePO> wp = new LambdaQueryWrapper<>();
        List<StaffFacePO> staffFacePOS = staffFaceMapper.selectList(wp.eq(StaffFacePO::getStaffId, info.getStaffId()));
        if (staffFacePOS!=null){
            StaffFacePO staffFacePO = staffFacePOS.get(0);
            Map<String, Object> param = new HashMap<>();
            param.put("faceData", HikSendHandle.getImageBase(info.getFaceImg()));
            param.put("faceId", staffFacePO.getHkFaceId());
            apiProxy.post(HikApi.ARTEMIS+"/api/resource/v1/face/single/update").setBody(param).execute().toJSONObject();
        }

    }

    @Override
    public void deleteFaceHik(FaceInfoTransmit info) {
        log.info("[ deleteFaceHik ] -> {}", info);
        LambdaQueryWrapper<StaffFacePO> wp = new LambdaQueryWrapper<>();
        List<StaffFacePO> staffFacePOS = staffFaceMapper.selectList(wp.eq(StaffFacePO::getStaffId, info.getStaffId()));
        if (staffFacePOS!=null){
            StaffFacePO staffFacePO = staffFacePOS.get(0);
            Map<String, Object> param = new HashMap<>();
            param.put("faceId", staffFacePO.getHkFaceId());
            apiProxy.post(HikApi.ARTEMIS+"/api/resource/v1/face/single/delete").setBody(param).execute().toJSONObject();
            staffFaceMapper.delete(new QueryWrapper<StaffFacePO>().eq("hk_face_id",staffFacePO.getHkFaceId()));
        }

    }
}
