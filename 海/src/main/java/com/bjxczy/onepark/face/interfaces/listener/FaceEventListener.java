package com.bjxczy.onepark.face.interfaces.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjxczy.core.rabbitmq.supports.FaceBaseListener;
import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.bjxczy.onepark.face.service.HkFaceService;
import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;


@Component
@Slf4j
public class FaceEventListener extends FaceBaseListener {

    @Autowired
    private HkFaceService faceService;

    /**
     * 凭证 的人脸 同步到海康平台
     *
     * @param info
     * @param channel
     * @param message
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(name = "voucher_to_hik_add_face_sync_queue", durable = "true"),
            key = ADD_HIK_FACE_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME
            ))})
    @Override
    public void addFaceEvent(FaceInfoTransmit info, Channel channel, Message message) {
        try {
            log.info("[订阅事件 -> ADD_FACE_EVENT] = {}", info);
            faceService.addFaceHik(info);
            this.confirm(() -> true, channel, message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "voucher_to_hik_put_face_sync_queue", durable
                            = "true"),
                    key = PUT_HIK_FACE_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME)
            )
    })
    @Override
    public void putFaceEvent(FaceInfoTransmit info, Channel channel, Message message) {
        try {
            log.info("[订阅事件 -> PUT_FACE_EVENT] = {}", info);
            faceService.updateFaceHik(info);
            this.confirm(() -> true, channel, message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "voucher_to_hik_del_face_sync_queue", durable = "true"),
                    key = DEL_HIK_FACE_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME)
            )
    })
    @Override
    public void delFaceEvent(FaceInfoTransmit info, Channel channel, Message message) {
        try {
            log.info("[订阅事件 -> DEL_FACE_EVENT] = {}", info);
            faceService.deleteFaceHik(info);
            this.confirm(() -> true, channel, message);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
