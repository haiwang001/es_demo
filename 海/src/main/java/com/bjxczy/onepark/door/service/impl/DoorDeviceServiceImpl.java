package com.bjxczy.onepark.door.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.event.door.HikAuthorityDeviceDTO;
import com.bjxczy.core.rabbitmq.event.door.HikAuthorityPayload;
import com.bjxczy.onepark.common.config.HikConfiguration;
import com.bjxczy.onepark.common.cons.HikApi;
import com.bjxczy.onepark.common.enums.PlatformTypeCode;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import com.bjxczy.onepark.common.utils.json.JsonUtils;
import com.bjxczy.onepark.door.service.DoorDeviceService;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikDeviceReturnDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/7/4 12:24
 */
@Log4j2
@Service
public class DoorDeviceServiceImpl implements DoorDeviceService {

    @Autowired
    private HikArtemisHttpUtils hikArtemisHttpUtils;
    @Autowired
    private HikConfiguration HikConfig;

    @Override
    public CreateVisitorAndDoorVo hikDoorCtrlEvent(HikAuthorityPayload payload) {
        // 添加权限配置
        createAuthority(payload);
        // 快捷下载
        String taskId = downloadQuick(payload);
        // 查询结果
        return queryResult(taskId);
    }

    @Override
    public CreateVisitorAndDoorVo ClearHikDoorAuthorizationEvent(HikAuthorityPayload payload) {
        // 删除权限配置
        clearAuthority(payload);
        // 快捷下载
        String taskId = downloadQuick(payload);
        // 查询结果
        return queryResult(taskId);
    }


    private CreateVisitorAndDoorVo queryResult(String taskId) {
        CreateVisitorAndDoorVo vo = new CreateVisitorAndDoorVo();
        vo.setType(PlatformTypeCode.PLATFORM_TYPE_CODE.getMsg());// 平台
        HashMap<String, Object> taskIdMap = new HashMap<>();
        taskIdMap.put("taskId", taskId);
        boolean switchStart = true;
        while (switchStart) {
            JSONObject jsonObject = hikArtemisHttpUtils.doPostStringArtemis(HikApi.getpath(HikApi.HAIKANG_DEVICE_TASK_PROGRESS_API), JsonUtils.toJsonString(taskIdMap), null, null, "application/json", 3000);
            HikDeviceReturnDto dto = JSON.parseObject(jsonObject.toString(), HikDeviceReturnDto.class);
            if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
                if (dto.getData().getIsDownloadFinished()) {
                    if (dto.getData().getResourceDownloadProgress().get(0).getDownloadResult().toString().equals("0")) {
                        log.info("[海康授权任务结果查询]  成功  code{} ", dto.getData().getResourceDownloadProgress().get(0).getDownloadResult().toString());
                        vo.setCode(0);
                        vo.setMsg("授权成功!");
                    } else if (dto.getData().getResourceDownloadProgress().get(0).getDownloadResult().toString().equals("2")) {
                        vo.setCode(0);
                        vo.setMsg("授权成功!");
                    } else {
                        String msg = "";
                        if ("0x1540300a".equals(dto.getData().getResourceDownloadProgress().get(0).getErrorCode())) {
                            msg = "设备不在线或网络不通";
                        }
                        if ("0x15403007".equals(dto.getData().getResourceDownloadProgress().get(0).getErrorCode())) {
                            msg = "请确认人员是否存在卡片和人脸信息";
                        }
                        log.error("[海康授权任务结果查询]  失败 errCode {}", dto.getData().getResourceDownloadProgress().get(0).getErrorCode());
                        log.error("[海康授权任务结果查询]  失败  msg {}", msg);
                        vo.setCode(-1);
                        vo.setMsg(msg);
                    }
                    switchStart = false;
                }
            } else {
                switchStart = false;
                log.info("[海康授权任务结果查询]  失败 结束循环");
            }
        }
        return vo;
    }

    private HashMap<String, Object> configuration(HikAuthorityPayload Payload) {
        HashMap<String, Object> obj1 = new HashMap<>();
        HashMap<String, Object> personDatas = new HashMap<>();
        personDatas.put("indexCodes", Payload.getStaffList());
        personDatas.put("personDataType", "person");
        obj1.put("personDatas", new HashMap[]{personDatas});
        obj1.put("resourceInfos", configurationResource(Payload.getDoorList()));
        return obj1;
    }

    private String downloadQuick(HikAuthorityPayload payload) {
        HashMap<String, Object> AUTHDOWNLOAD = new HashMap<>();
        AUTHDOWNLOAD.put("taskType", HikApi.HIK_EQUIPMENT_EQUIPMENT_FACE_MACHINE); // 人脸
        AUTHDOWNLOAD.put("resourceInfos", configurationResource(payload.getDoorList()));
        JSONObject jsonObject = hikArtemisHttpUtils.doPostStringArtemis(HikApi.getpath(HikApi.HAIKANG_DEVICE_CONFIGURATION_SHORTCUT_API), JsonUtils.toJsonString(AUTHDOWNLOAD), null, null, "application/json", 3000);
        HikDeviceReturnDto dto = JSON.parseObject(jsonObject.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
            log.info("[ 海康-闸机-权限配置快捷下载 success ] TaskId {}", dto.getData().getTaskId());
            return dto.getData().getTaskId();
        }
        return null;
    }

    private void createAuthority(HikAuthorityPayload payload) {
        HashMap<String, Object> json = configuration(payload);
        JSONObject jsonObject = hikArtemisHttpUtils.doPostStringArtemisByTagId(HikApi.getpath(HikApi.HAIKANG_DEVICE_AUTH_CONFIG_ADD_API), JsonUtils.toJsonString(json), null, null, "application/json", 3000, HikConfig.getTagId());
        HikDeviceReturnDto dto = JSON.parseObject(jsonObject.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
            log.info("[ 海康-闸机-添加权限配置 success ] TaskId {}", dto.getData().getTaskId());
        }
    }

    private void clearAuthority(HikAuthorityPayload payload) {
        HashMap<String, Object> json = configuration(payload);
        JSONObject jsonObject = hikArtemisHttpUtils.doPostStringArtemisByTagId(HikApi.getpath(HikApi.HAIKANG_DEVICE_AUTH_CONFIG_DEL_API), JsonUtils.toJsonString(json), null, null, "application/json", 3000, HikConfig.getTagId());
        HikDeviceReturnDto dto = JSON.parseObject(jsonObject.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
            log.info("[ 海康-闸机-删除权限配置 success ] TaskId {}", dto.getData().getTaskId());
        }
    }


    /**
     * 封装设备信息
     *
     * @param list 设备code列表
     * @return 设备通道对象列表
     */
    private ArrayList<HashMap<String, Object>> configurationResource(List<HikAuthorityDeviceDTO> list) {
        ArrayList<HashMap<String, Object>> arrayList = new ArrayList<>();
        for (HikAuthorityDeviceDTO dto : list) {
            HashMap<String, Object> resourceInfos = new HashMap<>(); // 设备通道对象列表
            resourceInfos.put("resourceIndexCode", dto.getDeviceCode());
            resourceInfos.put("resourceType", "acsDevice");
            resourceInfos.put("channelNos", new String[]{getChannelNo(dto.getDeviceName())});
            arrayList.add(resourceInfos);
        }
        return arrayList;
    }

    /**
     * 获取设备通道号
     *
     * @param Name 设备名
     * @return 设备通道号
     */
    private String getChannelNo(String Name) {
        HashMap<String, Object> json = new HashMap<>();
        json.put("pageNo", 1);
        json.put("pageSize", 100);
        json.put("name", Name);
        JSONObject hikResponse = hikArtemisHttpUtils.doPostStringArtemis(HikApi.getpath(HikApi.HAIKANG_DEVICE_RESOURCE_DOOR_API), JsonUtils.toJsonString(json), null, null, "application/json", 3000);
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
            return dto.getData().getList().get(0).getChannelNo();
        }
        return null;
    }
}
