package com.bjxczy.onepark.door.interfaces.controller;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.door.service.DoorInAndOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Auther: qinyuan
 * @Date: 2023/6/25 0025 10:17
 * @Description: DoorInAndOutController
 * @Version 1.0.0
 */
@ApiResourceController
@RequestMapping("/doorInAndOut")
public class DoorInAndOutController {

    @Autowired
    private DoorInAndOutService doorInAndOutService;


    /**
     * @return
     * @Author qinyuan
     * @Description 海康门禁通行事件回调
     * @Date 10:20 2023/6/25 0025
     * @Param
     **/
    @PostMapping(value = "/inAndOutDoorEventReceive", name = "海康门禁通行事件回调")
    public void inAndOutParkEventReceive(@RequestBody JSONObject jsonObject) {
        doorInAndOutService.inAndOutParkEventReceive(jsonObject);
    }

    /**
     *海康门禁人脸识别失败回调
     **/
    @PostMapping(value = "/inAndOutDoorFailureEventReceive", name = "海康门禁人脸识别失败回调")
    public void inAndOutDoorFailureEventReceive(@RequestBody JSONObject jsonObject) {
        doorInAndOutService.inAndOutDoorFailureEventReceive(jsonObject);
    }

}
