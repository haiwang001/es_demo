package com.bjxczy.onepark.door.dto;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/4/6 9:48
 */
@Data
public class Params {
    private String ability;
    private List<HikEvent> events;
    private String sendTime;
}
