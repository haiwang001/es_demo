package com.bjxczy.onepark.door.dto;


import com.bjxczy.onepark.common.utils.DateUtil;
import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/6 9:46
 *
 * 海康人脸返回成功 接收类型
 */
@Data
public class FaceDiscernSucceedDto {
    private String method;
    private Params params;
    private String openTime = DateUtil.now();
}
