package com.bjxczy.onepark.door.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.door.service.DoorInAndOutService;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Auther: qinyuan
 * @Date: 2023/6/25 0025 10:23
 */
@Log4j2
@Service
public class DoorInAndOutServiceImpl implements DoorInAndOutService {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void inAndOutParkEventReceive(JSONObject jsonObject) {
    }

    @Override
    public void inAndOutDoorFailureEventReceive(JSONObject jsonObject) {

    }
}
