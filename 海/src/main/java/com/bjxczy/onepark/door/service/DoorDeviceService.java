package com.bjxczy.onepark.door.service;


import com.bjxczy.core.rabbitmq.event.door.HikAuthorityPayload;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;

/*
 *@Author wlw
 *@Date 2023/7/4 12:24
 */
public interface DoorDeviceService {
    CreateVisitorAndDoorVo hikDoorCtrlEvent(HikAuthorityPayload payload);

    CreateVisitorAndDoorVo ClearHikDoorAuthorizationEvent(HikAuthorityPayload payload);
}
