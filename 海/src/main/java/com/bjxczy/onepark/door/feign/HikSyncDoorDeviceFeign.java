package com.bjxczy.onepark.door.feign;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.feign.client.door.BaseHikSyncDoorDeviceFeignClient;
import com.bjxczy.onepark.common.cons.HikApi;
import com.bjxczy.onepark.common.model.door.HikDoorDeviceInformation;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import com.bjxczy.onepark.common.utils.MD5Utils;
import com.bjxczy.onepark.common.utils.json.JsonUtils;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikDeviceReturnDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/7/3 17:16
 */
@Log4j2
@Controller
@ResponseBody
public class HikSyncDoorDeviceFeign implements BaseHikSyncDoorDeviceFeignClient {

    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;

    @Override
    public List<HikDoorDeviceInformation> getDoorInformation() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("pageNo", 1);
        map.put("pageSize", 1000);
        map.put("resourceType", "acsDevice");// 门禁控制器
        ArrayList<HikDoorDeviceInformation> list = new ArrayList<>();
        JSONObject hikResponse = hikArtemisHttpUtils.doPostStringArtemis(HikApi.getpath(HikApi.HAIKANG_DEVICE_LIST_API), JsonUtils.toJsonString(map), null, null, "application/json", 3000);
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode())) {
            dto.getData().getList().forEach(el -> {
                list.add(new HikDoorDeviceInformation(el.getName(), el.getIndexCode(), el.getOnline(), el.getIp()));
            });//设备列表
        }
        return list;
    }

    @Override
    public Integer getDoorOnline(String ip) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("ip", MD5Utils.transcoding(ip, StandardCharsets.UTF_16BE, StandardCharsets.UTF_8).trim()); // 设备ip
        JSONObject hikResponse = hikArtemisHttpUtils.doPostStringArtemis(HikApi.getpath(HikApi.HAIKANG_DEVICE_ONLINE_API), JsonUtils.toJsonString(map), null, null, "application/json", 3000);
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode())) {
            if (dto.getData().getList() == null) {
                return null;
            }
            return dto.getData().getList().get(0).getOnline();
        }
        return null;
    }

}