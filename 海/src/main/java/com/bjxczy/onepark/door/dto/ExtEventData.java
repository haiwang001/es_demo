package com.bjxczy.onepark.door.dto;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/6 10:36
 */
@Data
public class ExtEventData {
    private String ExtEventPersonNo; // 员工编号
    private String ExtEventPictureURL; // 抓拍照片
    private String ExtEventCode;   // 人脸认证事件
    private Integer ExtEventInOut; // 1 进 0 出
    private ExtEventIdentityCardInfo ExtEventIdentityCardInfo; // 员工编号

    private String ExtEventCardNo;//卡号
}
