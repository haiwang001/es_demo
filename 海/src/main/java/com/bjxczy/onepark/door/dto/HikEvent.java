package com.bjxczy.onepark.door.dto;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/6 10:01
 */
@Data
public class HikEvent {
    private String srcParentIndex;  // 设备编码
    private Integer status;  // 卡点状态
    private ExtEventData data;

    private String happenTime;//发生时间
}
