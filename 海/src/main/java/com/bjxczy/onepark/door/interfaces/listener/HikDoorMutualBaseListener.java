package com.bjxczy.onepark.door.interfaces.listener;


import com.bjxczy.core.rabbitmq.event.door.*;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.onepark.door.service.DoorDeviceService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/*
 *@Author wlw
 *@Date 2023/7/4 12:16
 */
@Component
@Slf4j
public class HikDoorMutualBaseListener extends DoorMutualBaseListener {
    @Resource
    private DoorDeviceService service;



    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmphik_add_door_Authority_event", durable = "true"),
                    key = HIK_DOOR_AUTHORIZATION,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME))})
    @Override
    public void hikDoorCtrlEvent(HikAuthorityPayload payload, Channel channel, Message message) {
        //  service.hikDoorCtrlEvent(payload);
        super.hikDoorCtrlEvent(payload, channel, message);
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "remove_hik_door_authorization_event", durable = "true"),
                    key = REMOVE_HIK_DOOR_CTRL_AUTHORIZATION,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME))})
    @Override
    public void ClearHikDoorAuthorizationEvent(HikAuthorityPayload payload, Channel channel, Message message) {
        if (payload.getDoorList()==null){
            super.hikDoorCtrlEvent(payload, channel, message);
            return;
        }
        //service.ClearHikDoorAuthorizationEvent(payload);
        super.hikDoorCtrlEvent(payload, channel, message);
    }

}
