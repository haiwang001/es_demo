package com.bjxczy.onepark.door.interfaces.listener;


import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.core.rabbitmq.event.door.*;
import com.bjxczy.core.rabbitmq.rpc.AsyncReplyHandler;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.onepark.common.enums.PlatformTypeCode;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.door.service.DoorDeviceService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
 *@Author wlw
 *@Date 2023/7/6 19:35
 */
@Component
@Slf4j
public class DoorAsyncListener extends BaseRabbitListener {
    @Resource
    private DoorDeviceService service;

    @Autowired
    private AsyncReplyHandler replyHandler;
    /**
     * 异步授权
     *
     * @param autu
     * @param channel
     * @param message
     */
    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmp_hik_door_create_event_queue", durable = "true"),  // 持久化队列
                    key = DoorMutualBaseListener.UPDATE_DOOR_CTRL_AUTHORIZATION,
                    exchange = @Exchange(value = DoorMutualBaseListener.DEFAULT_EXCAHNGE_NAME) // 绑定的交换机
            )
    })
    public void doorEventHandler(UpdateAuthorityPayload autu, Channel channel, Message message) {
        HikAuthorityPayload payload = new HikAuthorityPayload();
        // 员工信息
        ArrayList<String> list = new ArrayList<>();
        autu.getStaffList().forEach(el -> list.add(el.getStaffId().toString()));
        payload.setStaffList(list);
        // 设备信息
        List<HikAuthorityDeviceDTO> Device = new ArrayList<>();
        Map<String, List<AuthorityDeviceDTO>> collect = autu.getDoorList().stream().collect(Collectors.groupingBy(AuthorityDeviceDTO::getDeviceType));
        collect.get(PlatformTypeCode.PLATFORM_FACE_MACHINE_TYPE_CODE.getMsg()).forEach(el -> Device.add(new HikAuthorityDeviceDTO(el.getDeviceCode(), el.getDeviceName())));
        if (list.isEmpty()||Device.isEmpty()) {
            return;
        }
        payload.setDoorList(Device);
        CreateVisitorAndDoorVo vo = service.hikDoorCtrlEvent(payload);
        //消息回复
        log.info("[异步海康门禁授权] 授权结果{}", vo);
        replyHandler.reply(message, vo);
        this.confirm(() -> true, channel, message);
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmp_hik_door_del_event_queue", durable = "true"),  // 持久化队列
                    key = DoorMutualBaseListener.REMOVE_DOOR_CTRL_AUTHORIZATION,
                    exchange = @Exchange(value = DoorMutualBaseListener.DEFAULT_EXCAHNGE_NAME) // 绑定的交换机
            )
    })
    public void doorDelEventHandler(RemoveAuthorityPayload autu, Channel channel, Message message) {
        HikAuthorityPayload payload = new HikAuthorityPayload();
        if (autu.getStaffList().isEmpty()||autu.getDoorList().isEmpty()) {
            return;
        }
        // 员工信息
        ArrayList<String> list = new ArrayList<>();
        autu.getStaffList().forEach(el -> list.add(el.getStaffId().toString()));
        payload.setStaffList(list);
        // 设备信息
        List<HikAuthorityDeviceDTO> Device = new ArrayList<>();
        Map<String, List<AuthorityDeviceDTO>> collect = autu.getDoorList().stream().collect(Collectors.groupingBy(AuthorityDeviceDTO::getDeviceType));
        List<AuthorityDeviceDTO> authorityDeviceDTOS = collect.get(PlatformTypeCode.PLATFORM_FACE_MACHINE_TYPE_CODE.getMsg());
        if (list.isEmpty()||authorityDeviceDTOS.isEmpty()) {
            return;
        }
        authorityDeviceDTOS.forEach(el ->
                Device.add(new HikAuthorityDeviceDTO(el.getDeviceCode(), el.getDeviceName())));
        payload.setDoorList(Device);
        log.info("[异步取消授权] 授权信息{}", payload);
        CreateVisitorAndDoorVo vo = service.ClearHikDoorAuthorizationEvent(payload);
        //消息回复
        replyHandler.reply(message, vo);
        this.confirm(() -> true, channel, message);
    }
}
