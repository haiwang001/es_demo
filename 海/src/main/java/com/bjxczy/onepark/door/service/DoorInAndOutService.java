package com.bjxczy.onepark.door.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @Auther: qinyuan
 * @Date: 2023/6/25 0025 10:23
 * @Description: DoorInAndOutService
 * @Version 1.0.0
 */
public interface DoorInAndOutService {
    void inAndOutParkEventReceive(JSONObject jsonObject);

    void inAndOutDoorFailureEventReceive(JSONObject jsonObject);
}
