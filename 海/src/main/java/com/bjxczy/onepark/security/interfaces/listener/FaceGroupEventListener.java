package com.bjxczy.onepark.security.interfaces.listener;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.supports.FaceBaseListener;
import com.bjxczy.onepark.common.model.face.FaceInformation;
import com.bjxczy.onepark.security.service.FaceDisposeService;
import com.bjxczy.onepark.security.service.FaceService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FaceGroupEventListener extends FaceBaseListener {

    @Autowired
    private FaceService faceService;
    @Autowired
    private FaceDisposeService faceDisposeService;

    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "create_facegroup_event_queue", durable = "true"),
                    // routing key
                    key = CREATE_FACEGROUP_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void createFacegroupEvent(FaceInformation info, Channel channel, Message message) {
        log.info("[CREATE_FACEGROUP_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        JSONObject json = new JSONObject();
        json.put("name",info.getGroupName());
        json.put("id",info.getGroupId());
        json.put("description",info.getGroupName());
        faceService.groupFaceAdd(json);
        this.confirm(()->true,channel,message);
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "update_facegroup_event", durable = "true"),
                    // routing key
                    key = UPDATE_FACEGROUP_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void updateFacegroupEvent(FaceInformation info, Channel channel, Message message) {
        log.info("[UPDATE_FACEGROUP_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        JSONObject json = new JSONObject();
        json.put("indexCode",info.getGroupId());
        json.put("name",info.getGroupName());
        json.put("description",info.getDescription());
        faceService.groupFaceUpdate(json);
        this.confirm(()->true,channel,message);
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "remove_facegroup_event", durable = "true"),
                    // routing key
                    key = REMOVE_FACEGROUP_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void removeFaceGroupEvent(FaceInformation info, Channel channel, Message message) {
        log.info("[REMOVE_FACEGROUP_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        JSONObject json = new JSONObject();
        json.put("id",info.getGroupId());
        faceService.groupFaceDelete(json);
        this.confirm(()->true,channel,message);
    }


    /**
     * 添加人脸
     */
    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "create_face_event_queue", durable = "true"),
                    // routing key
                    key = CREATE_FACE_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void createFaceEvent(FaceInformation info, Channel channel, Message message) {
        log.info("[CREATE_FACE_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
            faceDisposeService.createFaceEvent(info);
            this.confirm(()->true,channel,message);
    }

    /**
     * 删除人脸
     */
    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "remove_face_event_queue", durable = "true"),
                    // routing key
                    key = REMOVE_FACE_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void removeFaceEvent(FaceInformation info, Channel channel, Message message) {
        log.info("[REMOVE_FACE_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        faceDisposeService.removeFaceEvent(info);
        this.confirm(()->true,channel,message);
    }

    /**
     * 修改人脸
     */
    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "edit_face_event_queue", durable = "true"),
                    // routing key
                    key = UPDATE_FACE_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void updateFaceEvent(FaceInformation info, Channel channel, Message message) {
        log.info("[UPDATE_FACE_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        faceDisposeService.editFaceEvent(info);
        this.confirm(()->true,channel,message);
    }



}
