package com.bjxczy.onepark.security.interfaces.controller;


import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.security.task.CameraRecordTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HikTaskController {

    @Autowired
    private CameraRecordTask cameraRecordTask;


    @GetMapping("/hik/runTask/record")
    public JSONObject queryFace(){
        cameraRecordTask.runTask();
        return new JSONObject();
    }
}
