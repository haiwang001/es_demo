package com.bjxczy.onepark.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("video_recognition")
public class RecognitionPO {
    private Integer recognitionId;
    private String hikRecognitionId;
}
