package com.bjxczy.onepark.security.interfaces.controller;


import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.security.entity.Cameras;
import com.bjxczy.onepark.security.service.HikDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class HikDeviceController {

    @Autowired
    private HikDeviceService hikDeviceService;

    @PostMapping("/hik/cameras")
    public List<Cameras> deviceList(@RequestBody  JSONObject jsonObject){
        return hikDeviceService.cameras(jsonObject);
    }

    @PostMapping("/hik/cameraPlayUrl")
    public JSONObject cameraPlayUrl(@RequestBody  JSONObject jsonObject){
        return hikDeviceService.getPlayUrl(jsonObject);
    }

    @PostMapping("/hik/getRecordFileList")
    public JSONObject getRecordFileList(@RequestBody  JSONObject jsonObject){
        return hikDeviceService.getRecordFileList(jsonObject);
    }
    @PostMapping("/hik//controlling")
    public Boolean controlling(@RequestBody JSONObject jsonBody) {
        return hikDeviceService.controlling(jsonBody);
    }
    @GetMapping("/hik/configInfo")
    public JSONObject getThirdPartyInfo(){
        return hikDeviceService.getThirdPartyInfo();
    };

    @PostMapping("/hik/cameraPlaybackUrl")
    public JSONObject cameraPlaybackUrl(@RequestBody  JSONObject jsonObject){
        return hikDeviceService.getPlaybackUrl(jsonObject);
    }

    @PostMapping("/hik/startRecord")
    public JSONObject startRecord(@RequestBody  JSONObject jsonObject){
        return hikDeviceService.getStartRecord(jsonObject);
    }

    @PostMapping("/hik/stopRecord")
    public JSONObject stopRecord(@RequestBody  JSONObject jsonObject){
        return hikDeviceService.getStopRecord(jsonObject);
    }

    @PostMapping("/hik/getFaceGroup")
    public JSONObject getFaceGroup(){
        return hikDeviceService.getFaceGroup();
    }

    @PostMapping("/hik/recognition")
    public JSONObject recognition(@RequestBody JSONObject jsonObject){
        return hikDeviceService.recognition(jsonObject);
    }


    @PostMapping("/hik/previewURLs")
    public JSONObject previewURLs(@RequestBody JSONObject jsonObject){
        return hikDeviceService.previewURLs(jsonObject);
    }

    @PostMapping("/hik/getCamera")
    public JSONObject getCamera(@RequestBody JSONObject jsonObject){
        return hikDeviceService.getCamera(jsonObject);
    }


}
