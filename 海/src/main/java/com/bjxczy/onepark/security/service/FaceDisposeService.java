package com.bjxczy.onepark.security.service;

import com.bjxczy.onepark.common.model.face.FaceInformation;

public interface FaceDisposeService {
    void createFaceEvent(FaceInformation info);
    void removeFaceEvent(FaceInformation info);
    void editFaceEvent(FaceInformation info);
}
