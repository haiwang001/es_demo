package com.bjxczy.onepark.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.security.entity.GroupFacePO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GroupFaceMapper extends BaseMapper<GroupFacePO> {
}
