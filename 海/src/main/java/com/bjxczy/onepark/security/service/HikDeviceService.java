package com.bjxczy.onepark.security.service;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.security.entity.Cameras;

import java.util.List;

public interface HikDeviceService {

    List<Cameras> cameras(JSONObject jsonObject);
    JSONObject getPlayUrl(JSONObject jsonBody);

    JSONObject getRecordFileList(JSONObject jsonObject);

    Boolean controlling(JSONObject jsonBody);

    JSONObject getThirdPartyInfo();

    JSONObject getPlaybackUrl(JSONObject jsonObject);

    JSONObject getStartRecord(JSONObject jsonObject);

    JSONObject getStopRecord(JSONObject jsonObject);

    JSONObject getFaceGroup();

    JSONObject recognition(JSONObject jsonObject);

    JSONObject previewURLs(JSONObject jsonObject);

    JSONObject getCamera(JSONObject jsonObject);

    JSONObject fetchRecognitionResource();
}
