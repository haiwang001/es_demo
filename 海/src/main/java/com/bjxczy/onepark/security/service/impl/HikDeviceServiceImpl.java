package com.bjxczy.onepark.security.service.impl;

import cn.hutool.core.map.MapBuilder;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.config.HikConfiguration;
import com.bjxczy.onepark.common.config.HikStreamVideoConfig;
import com.bjxczy.onepark.common.handle.HikSendHandle;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import com.bjxczy.onepark.security.entity.Cameras;
import com.bjxczy.onepark.security.service.HikDeviceService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class HikDeviceServiceImpl implements HikDeviceService {

    private static final String ARTEMIS = "/artemis";
    @Autowired
    private HikSendHandle hikSendHandle;
    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;
    @Autowired
    HikStreamVideoConfig hikStreamVideoConfig;
    @Autowired(required = false)
    HikConfiguration hikConfiguration;
    @Override
    public  List<Cameras> cameras(JSONObject jsonObject) {
        jsonObject.put("pageNo",1);
        jsonObject.put("pageSize",1000);
        jsonObject.put("treeCode", 0);
        final String VechicleDataApi = ARTEMIS + "/api/resource/v1/cameras";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 8000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康API获取设备列表调用失败:{}",resJSONObject.getString("msg"));
            throw new RuntimeException("获取海康设备列表失败，返回" + resJSONObject.getString("msg"));
        }
        JSONArray list = resJSONObject.getJSONObject("data").getJSONArray("list");
        List<Cameras> listmaps = JSONObject.parseArray(list.toJSONString(), Cameras.class);
        Optional.ofNullable(listmaps).ifPresent(cmarList ->{
            if (cmarList.size() > 500 ){
                List<List<Cameras>> partition = Lists.partition(cmarList, 500);
                partition.forEach( cList->{
                    List<String> indexCodes = cList.stream().map(item -> {
                        return item.getCameraIndexCode();
                    }).collect(Collectors.toList());
                    JSONObject pm = new JSONObject();
                    pm.put("indexCodes",indexCodes);
                    pm.put("pageNo",1);
                    pm.put("pageSize",1000);
                    Map<String, String> statusPath = new HashMap<String, String>(2) {
                        {
                            put("https://", ARTEMIS + "/api/nms/v1/online/camera/get");
                        }
                    };
                    try {
                        JSONObject statusResp = hikArtemisHttpUtils.doPostStringArtemis(statusPath, pm.toJSONString(), null, null, "application/json", 3000);
                        JSONArray statusList = statusResp.getJSONObject("data").getJSONArray("list");
                        cList.forEach(item->{
                            for (int i = 0; i < statusList.size(); i++) {
                                Map data = (Map) statusList.get(i);
                                String indexCode = String.valueOf(data.get("indexCode"));
                                if (indexCode.equals(item.getCameraIndexCode())){
                                    item.setStatus((Integer) data.get("online"));
                                    break;
                                }
                            }
                        });
                    }catch (Exception ex){
                        log.info("[获取摄像头在线状态异常]");
                        ex.printStackTrace();
                    }
                });
            }else{
                List<String> indexCodes = cmarList.stream().map(item -> {
                    return item.getCameraIndexCode();
                }).collect(Collectors.toList());
                JSONObject pm = new JSONObject();
                pm.put("indexCodes",indexCodes);
                pm.put("pageNo",1);
                pm.put("pageSize",1000);
                Map<String, String> statusPath = new HashMap<String, String>(2) {
                    {
                        put("https://", ARTEMIS + "/api/nms/v1/online/camera/get");
                    }
                };
                try {
                    JSONObject statusResp = hikArtemisHttpUtils.doPostStringArtemis(statusPath, pm.toJSONString(), null, null, "application/json", 3000);
                    JSONArray statusList = statusResp.getJSONObject("data").getJSONArray("list");
                    cmarList.forEach(item->{
                        for (int i = 0; i < statusList.size(); i++) {
                            Map data = (Map) statusList.get(i);
                            String indexCode = String.valueOf(data.get("indexCode"));
                            if (indexCode.equals(item.getCameraIndexCode())){
                                item.setStatus((Integer) data.get("online"));
                                break;
                            }
                        }
                    });
                }catch (Exception ex){
                    log.info("[获取摄像头在线状态异常]");
                    ex.printStackTrace();
                }
            }
        });
        return listmaps;
    }

    @Override
    public JSONObject getPlayUrl(JSONObject jsonBody) {
        final String VechicleDataApi = ARTEMIS + "/api/video/v2/cameras/previewURLs";
//        final String VechicleDataApi = ARTEMIS + "/api/video/v1/cameras/previewURLs";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
//        jsonBody.put("protocol", "rtsp");
//        if (jsonBody.containsKey("protocol")) {
//            if("rtsp".equals(jsonBody.getString("protocol")))
//                jsonBody.put("expand", "streamform=rtp");
//            else jsonBody.put("expand","transcod e=1&videotype=h264");
//        } else {
//            jsonBody.put("protocol", hikStreamVideoConfig.getProtocol());
//        }
        jsonBody.put("streamType", hikStreamVideoConfig.getStreamType());
        jsonBody.put("transmode", hikStreamVideoConfig.getTransmode());
        log.info("[ 请求参数 ]:{}",jsonBody);
        JSONObject resObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonBody.toJSONString(), null, null, "application/json", 3000);// post请求application/json类型参数
        if (!(resObject.containsKey("code") && ("0".equals(resObject.getString("code"))))) {
            log.error("获取海康实时播放流数据失败:{}",resObject.getString("msg"));
            throw new RuntimeException("获取海康实时播放流数据失败" + resObject.getString("msg"));
        }
        Map data = (Map) resObject.get("data");
        jsonBody.clear();
        jsonBody.put("url",data.get("url"));
        return jsonBody;
    }

    @Override
    public JSONObject getRecordFileList(JSONObject jsonBody) {


        Map<String, String> path = new HashMap<String, String>(2) {
            {
                this.put("https://", "/artemis/api/video/v2/cameras/playbackURLs");
            }
        };
        String cameraIndexCode = jsonBody.getString("cameraIndexCode");
//        Map<String, String> configMap = (Map)jsonBody.getObject("configMap", Map.class);
//        jsonBody.put("streamType", configMap.get("recordStreamType"));
//        jsonBody.put("protocol", configMap.get("recordProtocol"));
//        jsonBody.put("recordLocation", configMap.get("recordLocation"));
//        jsonBody.put("transmode", configMap.get("recordTransmode"));
//        jsonBody.put("expand", configMap.get("recordExpand"));
        jsonBody.remove("pageNo");
        jsonBody.remove("pageSize");
        Long beginTime = jsonBody.getLong("beginTime");
        Long endTime = jsonBody.getLong("endTime");
        jsonBody.put("beginTime", DateFormatUtils.format(beginTime, "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"));
        jsonBody.put("endTime", DateFormatUtils.format(endTime, "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"));
        JSONObject jsonObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonBody.toJSONString(), (Map)null, (String)null, "application/json", 3000);
        return jsonObject;
    }

    @Override
    public Boolean controlling(JSONObject jsonBody) {
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                this.put("https://", "/artemis/api/video/v1/ptzs/controlling");
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonBody.toJSONString(), (Map)null, (String)null, "application/json", 3000);
        if (resJSONObject.containsKey("code") && "0".equals(resJSONObject.getString("code"))) {
            return true;
        } else {
            log.error("获取海康云台控制失败:{}", resJSONObject.getString("msg"));
            return false;
        }
    }

    @Override
    public JSONObject getThirdPartyInfo() {
        JSONObject result = new JSONObject();
        result.put("url",hikConfiguration.getHost());
        result.put("userName",hikConfiguration.getAppKey());
        result.put("password",hikConfiguration.getAppSecret());
        result.put("playId",3);
        return result;
    }
    @Override
    public JSONObject getPlaybackUrl(JSONObject jsonBody) {
            log.info("[ getPlaybackUrl ] 入参: {}",jsonBody);
            Integer recordLocation = 0;
            Map<String, Object> param = MapBuilder.create(new HashMap<String, Object>())
                    .put("cameraIndexCodes", new ArrayList<String>(){{
                        add(jsonBody.getString("cameraIndexCode"));
                    }}).build();
        JSONObject manualRecordResp = hikArtemisHttpUtils.doPostStringArtemis(
             new HashMap<String, String>(2) {
                    {
                        put("https://", ARTEMIS+"/api/video/v1/manualRecord/taskId/search");
                    }
                },JSONObject.toJSONString(param), null, null, "application/json", 3000);// post请求application/json类型参数
        log.info("[ 查询摄像头数据-> {}]",manualRecordResp);
        try {
            if (!Objects.isNull(manualRecordResp)) {
                Map data = (Map) manualRecordResp.get("data");
                if (!Objects.isNull(data)) {
                    List manualRecordTask = (List)data.get("manualRecordTask");
                    Map manualRecord = (Map)manualRecordTask.get(0);
                    recordLocation = (Integer)manualRecord.get("type");
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        final String VechicleDataApi = ARTEMIS + "/api/video/v1/cameras/playbackURLs";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        jsonBody.put("protocol", "ws");
        jsonBody.put("recordLocation", recordLocation);
        Long beginTime = jsonBody.getLong("beginTime");
        Long endTime = jsonBody.getLong("endTime");
        jsonBody.put("beginTime", DateFormatUtils.format(beginTime, "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"));
        jsonBody.put("endTime", DateFormatUtils.format(endTime, "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"));
        log.info("[ getPlaybackUrl ] 请求海康，封装入参: {}",jsonBody);
        JSONObject resObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonBody.toJSONString(), null, null, "application/json", 3000);// post请求application/json类型参数
        if (!(resObject.containsKey("code") && ("0".equals(resObject.getString("code"))))) {
            log.error("获取海康回放流数据失败:{}",resObject.getString("msg"));
            throw new RuntimeException("获取海康回放流数据失败" + resObject.getString("msg"));
        }
        log.info("获取海康回放流数据成功:{}",resObject.toJSONString());
        Map data = (Map) resObject.get("data");
        JSONObject jsonObject = new JSONObject(data);
        return jsonObject;
    }

    @Override
    public JSONObject getStartRecord(JSONObject jsonBody) {
        final String VechicleDataApi = ARTEMIS + "/api/video/v1/manualRecord/start";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        jsonBody.put("recordType", 6);
        jsonBody.put("type", 1);
        JSONObject resObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonBody.toJSONString(), null, null, "application/json", 3000);// post请求application/json类型参数
        if (!(resObject.containsKey("code") && ("0".equals(resObject.getString("code"))))) {
            log.error("获取海康开始手动录像数据失败:{}",resObject.getString("msg"));
            throw new RuntimeException("获取海康开始手动录像数据失败" + resObject.getString("msg"));
        }
        Map data = (Map) resObject.get("data");
        jsonBody.clear();
        jsonBody.put("taskID",data.get("taskID"));
        return jsonBody;
    }

    @Override
    public JSONObject getStopRecord(JSONObject jsonBody) {
        final String VechicleDataApi = ARTEMIS + "/api/video/v1/manualRecord/stop";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        jsonBody.put("type", 1);
        JSONObject resObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonBody.toJSONString(), null, null, "application/json", 3000);// post请求application/json类型参数
        if (!(resObject.containsKey("code") && ("0".equals(resObject.getString("code"))))) {
            log.error("获取海康停止手动录像数据失败:{}",resObject.getString("msg"));
            throw new RuntimeException("获取海康停止手动录像数据失败" + resObject.getString("msg"));
        }
//        Map data = (Map) resObject.get("data");
//        jsonBody.clear();
//        jsonBody.put("taskID",data.get("taskID"));
        return resObject;
    }

    @Override
    public JSONObject getFaceGroup() {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face/group";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject jsonObject = new JSONObject();
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康API获取人脸分组:{}",resJSONObject.getString("msg"));
            throw new RuntimeException("获取海康入人脸分组列表失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康人脸分组列表:"+resJSONObject);
        return resJSONObject;
    }

    @Override
    public JSONObject recognition(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/resource/recognition";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject previewURLs(JSONObject jsonObject) {
        final String previewURLsDataApi = ARTEMIS + "/api/video/v2/cameras/previewURLs";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", previewURLsDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject getCamera(JSONObject jsonObject) {
        final String previewURLsDataApi = ARTEMIS + "/api/irds/v2/region/nodesByParams";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", previewURLsDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject fetchRecognitionResource() {
        Map<String, String> fetchRecognitionResourcePath = new HashMap<String, String>(2) {
                {
                    put("https://", "/artemis/api/v2/resource/fetchRecognitionResource");
                }
            };
            JSONObject fetchRecognitionResourceResp = hikArtemisHttpUtils.doPostStringArtemis(fetchRecognitionResourcePath, new JSONObject().toJSONString(), null, null, "application/json", 3000);
            log.info("[ 获取行为分析识别资源集合 ]:{}",fetchRecognitionResourceResp);
            if (!(fetchRecognitionResourceResp.containsKey("code") && ("0".equals(fetchRecognitionResourceResp.getString("code"))))) {
                log.error("获取行为分析识别资源集合:{}",fetchRecognitionResourceResp.getString("msg"));
            }
            return fetchRecognitionResourceResp;
    }

}
