package com.bjxczy.onepark.security.interfaces.controller;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.security.service.AlarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlarmController {
    @Autowired
    private AlarmService alarmService;

    /**
     * 海康按事件类型订阅事件
     * @param jsonObject
     * @return
     */
    @PostMapping("/alarm/eventSubscriptionView")
    public JSONObject eventSubscriptionView(@RequestBody JSONObject jsonObject){
        return alarmService.eventSubscriptionView(jsonObject);
    }

    /**
     * 海康按事件类型取消订阅
     * @param jsonObject
     * @return
     */
    @PostMapping("/alarm/eventUnSubscriptionByEventTypes")
    public JSONObject eventUnSubscriptionByEventTypes(@RequestBody JSONObject jsonObject){
        return alarmService.eventUnSubscriptionByEventTypes(jsonObject);
    }

    /**
     * 查询事件订阅信息
     * @return
     */
    @PostMapping("/alarm/querySubscription")
    public JSONObject querySubscription(){
        return alarmService.querySubscription();
    }
}
