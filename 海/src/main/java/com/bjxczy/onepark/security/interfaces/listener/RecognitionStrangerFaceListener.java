package com.bjxczy.onepark.security.interfaces.listener;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.supports.FaceBaseListener;
import com.bjxczy.onepark.common.model.face.FaceInformation;
import com.bjxczy.onepark.common.model.face.RecognitionFaceInfo;
import com.bjxczy.onepark.security.service.RecognitionFaceService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RecognitionStrangerFaceListener extends FaceBaseListener {

    @Autowired
    private RecognitionFaceService recognitionFaceService;

    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "bmpsecurity_add_recognitionBlackFace_event", durable = "true"),
                    // routing key
                    key = ADD_RECOGNITIONBLACKFACE_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void addRecognitionBlackFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
        log.info("[ADD_RECOGNITIONBLACKFACE_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        this.confirm(()-> {
            try {
                recognitionFaceService.addBlackRecognitionPlan(info);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            return true;
            },channel,message);
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "bmpsecurity_upd_recognitionBlackFace_event", durable = "true"),
                    // routing key
                    key = UPD_RECOGNITIONBLACKFACE_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void updRecognitionBlackFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
        log.info("[UPD_RECOGNITIONBLACKFACE_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        this.confirm(()->{
            try {
                recognitionFaceService.updateBlackRecognitionPlan(info);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            return true;
        },channel,message);
    }



    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "bmpsecurity_del_recognitionBlackFace_event", durable = "true"),
                    // routing key
                    key = DEL_RECOGNITIONBLACKFACE_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void delRecognitionBlackFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
        log.info("[DEL_RECOGNITIONBLACKFACE_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        this.confirm(()-> {
            try {
                recognitionFaceService.deleteBlackRecognitionPlan(info);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            return true;
        },channel,message);
    }




    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "bmpsecurity_add_recognitionStrangerFace_event", durable = "true"),
                    // routing key
                    key = ADD_RECOGNITIONSTRANGERFACE_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void addRecognitionStrangerFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
        log.info("[ADD_RECOGNITIONSTRANGERFACE_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        this.confirm(()-> {
            try {
                recognitionFaceService.addWhiteRecognitionPlan(info);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            return true;
        },channel,message);
    }


    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "bmpsecurity_upd_recognitionStrangerFace_event", durable = "true"),
                    // routing key
                    key = UPD_RECOGNITIONSTRANGERFACE_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void updRecognitionStrangerFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
        log.info("[UPD_RECOGNITIONSTRANGERFACE_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        this.confirm(()-> {
            try {
                recognitionFaceService.updateWhiteRecognitionPlan(info);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            return true;
        },channel,message);
    }



    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "bmpsecurity_del_recognitionStrangerFace_event", durable = "true"),
                    // routing key
                    key = DEL_RECOGNITIONSTRANGERFACE_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT)
            )
    })
    public void delRecognitionStrangerFaceEvent(RecognitionFaceInfo info, Channel channel, Message message) {
        log.info("[DEL_RECOGNITIONSTRANGERFACE_EVENT] message={}", info);
        // 消息确认，当抛出异常时，消息将重新加入队列
        this.confirm(()->{
            try {
                recognitionFaceService.deleteWhiteRecognitionPlan(info);
            }catch (Exception ex){
                ex.printStackTrace();
            }
            return true;
        },channel,message);
    }
}
