package com.bjxczy.onepark.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("video_face")
public class FacePO {
    private Integer localFaceId;
    private String hikFaceId;
    private Integer tenantId;
    private String hikGroupFaceId;
}
