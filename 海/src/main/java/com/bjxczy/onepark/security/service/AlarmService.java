package com.bjxczy.onepark.security.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

public interface AlarmService {

    JSONObject eventSubscriptionView(@RequestBody JSONObject jsonObject);

    JSONObject eventUnSubscriptionByEventTypes(@RequestBody JSONObject jsonObject);

    JSONObject querySubscription();
}
