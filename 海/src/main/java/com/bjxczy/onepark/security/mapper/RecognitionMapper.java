package com.bjxczy.onepark.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.security.entity.RecognitionPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RecognitionMapper extends BaseMapper<RecognitionPO> {
}
