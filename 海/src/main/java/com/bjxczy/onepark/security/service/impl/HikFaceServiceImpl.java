package com.bjxczy.onepark.security.service.impl;

import cn.hutool.core.map.MapBuilder;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import com.bjxczy.onepark.security.entity.FacePO;
import com.bjxczy.onepark.security.entity.GroupFacePO;
import com.bjxczy.onepark.security.entity.RecognitionPO;
import com.bjxczy.onepark.security.mapper.FaceMapper;
import com.bjxczy.onepark.security.mapper.GroupFaceMapper;
import com.bjxczy.onepark.security.mapper.RecognitionMapper;
import com.bjxczy.onepark.security.service.HikFaceService;
import com.hikvision.artemis.sdk.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class HikFaceServiceImpl implements HikFaceService {

    private static final String ARTEMIS = "/artemis";
    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;
    @Autowired
    private GroupFaceMapper groupFaceMapper;
    @Autowired
    private FaceMapper faceMapper;

    @Autowired
    private RecognitionMapper recognitionMapper;
    @Override
    public JSONObject queryFaceList(JSONObject jsonObject) {
        if(Objects.isNull(jsonObject.getInteger("pageNo")) || Objects.isNull(jsonObject.getInteger("pageSize"))){
            jsonObject.put("pageNo",1);
            jsonObject.put("pageSize",1000);
        }
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject addBlackRecognitionPlan(JSONObject jsonObject) {
        log.info("[ 新增重点人员识别计划：{} ]",jsonObject);
        List<String> faceGroupIndexCodes =(List<String>) jsonObject.getJSONArray("faceGroupIndexCodes");
        List<String> list = new ArrayList<>();
        for (String faceGroupIndexCode : faceGroupIndexCodes) {
            GroupFacePO groupFacePO = groupFaceMapper.selectOne(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getLocalGroupFaceId, faceGroupIndexCode));
            list.add(groupFacePO.getHikGroupFaceId());
        }
        jsonObject.put("faceGroupIndexCodes",list);


        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/black/addition";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);


        return resJSONObject;
    }

    @Override
    public JSONObject deleteBlackRecognitionPlan(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/black/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject getBlackRecognitionPlan(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/black";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject restartBlackRecognitionPlan(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/black/restart";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject updateBlackRecognitionPlan(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/black/update";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject addWhiteRecognitionPlan(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/white/addition";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject deleteWhiteRecognitionPlan(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/white/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject getWhiteRecognitionPlan(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/white";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject restartWhiteRecognitionPlan(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/white/restart";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject updateWhiteRecognitionPlan(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/white/update";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        return resJSONObject;
    }

    @Override
    public JSONObject getFacePic(JSONObject jsonObject) {
        log.info("[ getFacePic ]:{}",jsonObject);
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/application/picture";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };

        JSONObject params = new JSONObject();
        params.put("url",jsonObject.get("url"));
        log.info("[ getFacePic ]-params:{}",params);
//        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        Response response = hikArtemisHttpUtils.doPostResponse(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("[ getFacePic ]-Response:{}",response);
        if (response.getStatusCode() == 302){
            Map<String, String> headers = response.getHeaders();
            String url = headers.get("Location");
            String nowUrl = replaceDomainAndPort("10.156.97.245", "6040", url);
            url = nowUrl.replace("https","http");
            return new JSONObject(MapBuilder.create(new HashMap<String,Object>())
                    .put("data",url).build());
        }
        String responseResult = hikArtemisHttpUtils.getResponseResult(response);
        if (StringUtils.isNotBlank(responseResult)){
            return JSONObject.parseObject(responseResult);
        }
        return new JSONObject();
    }
    private String replaceDomainAndPort(String domain,String port,String url){
        String url_bak = "";
        if(url.indexOf("//") != -1 ){
            String[] splitTemp = url.split("//");
            url_bak = splitTemp[0]+"//";
            if(port != null){
                url_bak = url_bak + domain+":"+port;
            }else{
                url_bak = url_bak + domain;
            }

            if(splitTemp.length >=1 && splitTemp[1].indexOf("/") != -1){
                String[] urlTemp2 = splitTemp[1].split("/");
                if(urlTemp2.length > 1){
                    for(int i = 1;i < urlTemp2.length; i++){
                        url_bak = url_bak +"/"+urlTemp2[i];
                    }
                }
                System.out.println("url_bak:"+url_bak);
            }else{
                System.out.println("url_bak:"+url_bak);
            }
        }
        return url_bak;
    }
    @Override
    public JSONObject queryHighFrequency(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/event/high_frequency/search";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("按条件查询高频人员识别事件返回:{}", resJSONObject);
        return resJSONObject;
    }
    @Override
    public JSONObject queryBlack(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/event/black/search";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("按条件查询重点人员识别事件返回:{}", resJSONObject);
        return resJSONObject;
    }
    @Override
    public JSONObject queryStranger(JSONObject jsonObject) {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/event/stranger/search";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("按条件查询陌生人员识别事件返回:{}", resJSONObject);
        return resJSONObject;
    }

    @Override
    public JSONObject getLocalGroupId(JSONObject jsonObject) {
        GroupFacePO groupFacePO = groupFaceMapper.selectOne(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getHikGroupFaceId, jsonObject.getString("hikGroupId")));
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("localGroupId",groupFacePO.getLocalGroupFaceId());
        return jsonObject1;
    }

    @Override
    public JSONObject getLocalFaceId(JSONObject jsonObject) {
        FacePO hikFaceId = faceMapper.selectOne(Wrappers.<FacePO>lambdaQuery().eq(FacePO::getHikFaceId, jsonObject.getString("hikFaceId")));
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("localFaceId",hikFaceId.getLocalFaceId());
        return jsonObject1;
    }

    @Override
    public JSONObject updFace(JSONObject jsonObject) {
        log.info("[ 调用海康接口修改人脸 ]:{}",jsonObject);
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face/single/update";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("调用海康接口修改人脸返回:{}", resJSONObject);
        return resJSONObject;

    }

    @Override
    public JSONObject getPlanStatus(JSONObject jsonObject) {
        Integer id = (Integer)jsonObject.get("id");
//        RecognitionPO recognitionPO = recognitionMapper.selectById(id);
        RecognitionPO recognitionPO = recognitionMapper.selectOne(Wrappers.<RecognitionPO>lambdaQuery().eq(RecognitionPO::getRecognitionId,id));
        //重点人员
         String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/black/detail";
         if (jsonObject.getString("type").equals("WHITE_LIST")){
             //陌生人员
             VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/white/detail";
         }
        String finalVechicleDataApi = VechicleDataApi;
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", finalVechicleDataApi);
            }
        };
        if (null == recognitionPO){
            jsonObject.put("status",0);
            return jsonObject;
        }
        try {
            JSONObject params = new JSONObject();
            params.put("indexCode",recognitionPO.getHikRecognitionId());
            JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, params.toJSONString(), null, null, "application/json", 3000);
            log.info("调用海康接口查询计划状态返回:{}", resJSONObject);
            if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
                jsonObject.put("status",0);
            }else{
                if (null !=resJSONObject.get("data")) {
                    Map<String,Object> data = (Map<String,Object>)resJSONObject.get("data");
                    String status = String.valueOf(data.get("status"));
                    if (status.equals("SUCCESS")){
                        jsonObject.put("status",1);
                    }else if (status.equals("RUNNING")){
                        jsonObject.put("status",2);
                    }else{
                        jsonObject.put("status",0);
                    }
                }
            }
        }catch (Exception ex){
            jsonObject.put("status",0);
            ex.printStackTrace();
        }
        return jsonObject;
    }

}
