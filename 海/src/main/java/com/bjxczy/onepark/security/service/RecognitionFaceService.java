package com.bjxczy.onepark.security.service;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.model.face.RecognitionFaceInfo;

public interface RecognitionFaceService {
    boolean addBlackRecognitionPlan(RecognitionFaceInfo info);
    boolean deleteBlackRecognitionPlan(RecognitionFaceInfo info);
    boolean updateBlackRecognitionPlan(RecognitionFaceInfo info);


    boolean addWhiteRecognitionPlan(RecognitionFaceInfo info);
    boolean deleteWhiteRecognitionPlan(RecognitionFaceInfo info);
    boolean updateWhiteRecognitionPlan(RecognitionFaceInfo info);
}
