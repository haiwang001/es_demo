package com.bjxczy.onepark.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.security.entity.FacePO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FaceMapper extends BaseMapper<FacePO> {
}
