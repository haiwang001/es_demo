package com.bjxczy.onepark.security.task;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.model.security.CameraInformation;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import com.bjxczy.onepark.common.utils.RedisUtils;
import com.bjxczy.onepark.security.feign.client.BmpSecurityManagerFeignClient;
import com.google.common.collect.Lists;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
/**
 * 定时下发摄像头到ai分析
 */
@Slf4j
public class AiAnalysisTask {


    private static final String ANALYSIS_POSITION_KEY = "ANALYSIS_POSITION";

    private static final String ANALYSIS_PLAN_KEY = "ANALYSIS_PLAN";

    private static final Integer PARTITION_CNT = 30;

    private static final String PLAN_TYPE = "behaviorAnalysisServer";

    private static final String PLAN_NAME = "AI分析轮询计划";

    @Autowired
    private HikArtemisHttpUtils hikArtemisHttpUtils;

    @Autowired
    private BmpSecurityManagerFeignClient bmpSecurityManagerFeignClient;


    @XxlJob("aiAnalysisTask")
    public void runTask() {
        try {
            Integer position = 1;
            if (!Objects.isNull(RedisUtils.get(ANALYSIS_POSITION_KEY))) {
                position = (Integer) RedisUtils.get(ANALYSIS_POSITION_KEY) + 1;
            }
            List<CameraInformation> cameraInformations = bmpSecurityManagerFeignClient.allCameras();
            List<List<CameraInformation>> partition = Lists.partition(cameraInformations, PARTITION_CNT);
            if (position >= partition.size()) {
//                RedisUtils.set(ANALYSIS_POSITION_KEY, 1);
                position = 1;
            }
            RedisUtils.set(ANALYSIS_POSITION_KEY, position);
            List<CameraInformation> partitionCameraList = partition.get(position - 1);
            // 获取行为分析识别资源集合 1
            Map<String, String> fetchRecognitionResourcePath = new HashMap<String, String>(2) {
                {
                    put("https://", "/artemis/api/v2/resource/fetchRecognitionResource");
                }
            };
            JSONObject fetchRecognitionResourceResp = hikArtemisHttpUtils.doPostStringArtemis(fetchRecognitionResourcePath, new JSONObject().toJSONString(), null, null, "application/json", 3000);
            log.info("[ 获取行为分析识别资源集合 ]:{}", fetchRecognitionResourceResp);
            if (!(fetchRecognitionResourceResp.containsKey("code") && ("0".equals(fetchRecognitionResourceResp.getString("code"))))) {
                log.error("获取行为分析识别资源集合:{}", fetchRecognitionResourceResp.getString("msg"));
                return;
            }
            Map<String, Object> data = (Map<String, Object>) fetchRecognitionResourceResp.get("data");
            List<Map<String, Object>> behaviorAnalysisServerList = (List<Map<String, Object>>) data.get("behaviorAnalysisServer");

            if (behaviorAnalysisServerList.size() < 1) {
                log.error("============无识别资源============");
                return;
            }
            // 获取一个智能分析资源
            Map<String, Object> behaviorAnalysisServer = behaviorAnalysisServerList.get(0);
            //下发行为分析计划 2
            JSONObject behaviorParam = new JSONObject();
            //默认添加路径
            String pathUri = "/artemis/api/v2/plan/behavior/add";
            boolean isUpdate = !Objects.isNull(RedisUtils.get(ANALYSIS_PLAN_KEY));
            if (isUpdate) {
                // 计划存在就修改
                behaviorParam.put("indexCode", RedisUtils.get(ANALYSIS_PLAN_KEY));
                pathUri = "/artemis/api/v1/plan/behavior/modify";
            }
            String finalPathUri = pathUri;
            Map<String, String> behaviorAddPath = new HashMap<String, String>(2) {
                {
                    put("https://", finalPathUri);
                }
            };
            JSONObject recognitionPipe = new JSONObject();

            //recognitionPipe 识别资源参数
            recognitionPipe.put("resType", PLAN_TYPE);
            recognitionPipe.put("resIndexCode", behaviorAnalysisServer.get("indexCode"));


            behaviorParam.put("name", PLAN_NAME);
            behaviorParam.put("templateCode", "everytimetemplate");
            behaviorParam.put("recognitionPipe", recognitionPipe);
            behaviorParam.put("cameraIndexCodes", partitionCameraList.stream().map(item -> {
                return item.getCameraIndexCode();
            }).collect(Collectors.toList()));
            JSONObject behaviorAddResp = hikArtemisHttpUtils.doPostStringArtemis(behaviorAddPath, behaviorParam.toJSONString(), null, null, "application/json", 3000);
            log.info("[ {}行为分析计划 ]:{}", isUpdate ? "修改" : "添加", behaviorAddResp);
            if (!(behaviorAddResp.containsKey("code") && ("0".equals(behaviorAddResp.getString("code"))))) {
                log.error("[ {}行为分析计划 ]:{}", isUpdate ? "修改" : "添加", behaviorAddResp.getString("msg"));
                return;
            }
            if (!isUpdate) {
                String planCode = (String) behaviorAddResp.get("data");
                RedisUtils.set(ANALYSIS_PLAN_KEY, planCode);
            }
            // ------end --------
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
