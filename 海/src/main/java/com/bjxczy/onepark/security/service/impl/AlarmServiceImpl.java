package com.bjxczy.onepark.security.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import com.bjxczy.onepark.security.service.AlarmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class AlarmServiceImpl implements AlarmService {

    private static final String ARTEMIS = "/artemis";
    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;

    @Override
    public JSONObject eventSubscriptionView(JSONObject jsonObject) {
        JSONObject jsonParams = new JSONObject();
        List<Integer> list =(List<Integer>) jsonObject.getJSONArray("list");
        jsonParams.put("eventTypes",list);
        jsonParams.put("eventDest",jsonObject.get("eventDest"));
        jsonParams.put("subType",2);
        System.out.println(jsonParams.toJSONString());
        final String VechicleDataApi = ARTEMIS + "/api/eventService/v1/eventSubscriptionByEventTypes";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonParams.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康API推送事件告警调用失败:{}",resJSONObject.getString("msg"));
            throw new RuntimeException("海康推送事件告警失败，返回" + resJSONObject.getString("msg"));
        }
        return resJSONObject;
    }

    @Override
    public JSONObject eventUnSubscriptionByEventTypes(JSONObject jsonObject) {
        JSONObject jsonParams = new JSONObject();
        List<Integer> list =(List<Integer>) jsonObject.get("list");
        jsonParams.put("eventTypes",list);
        final String VechicleDataApi = ARTEMIS + "/api/eventService/v1/eventUnSubscriptionByEventTypes";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonParams.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康API按事件类型取消订阅调用失败:{}",resJSONObject.getString("msg"));
            throw new RuntimeException("海康API按事件类型取消订阅调用失败，返回" + resJSONObject.getString("msg"));
        }
        return resJSONObject;
    }

    @Override
    public JSONObject querySubscription() {
        final String VechicleDataApi = ARTEMIS + "/api/eventService/v1/eventSubscriptionView";
        JSONObject jsonObject = new JSONObject();
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = this.hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康API查询订阅事件调用失败:{}",resJSONObject.getString("msg"));
            throw new RuntimeException("海康查询订阅事件失败，返回" + resJSONObject.getString("msg"));
        }
        return resJSONObject;
    }


}
