package com.bjxczy.onepark.security.service;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

public interface FaceService {

    JSONObject getFaceGroup();

    JSONObject groupFaceAdd(JSONObject jsonObject);

    JSONObject groupFaceUpdate(JSONObject jsonObject);

    JSONObject groupFaceDelete(JSONObject jsonObject);

}
