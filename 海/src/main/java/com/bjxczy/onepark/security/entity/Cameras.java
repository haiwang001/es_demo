package com.bjxczy.onepark.security.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cameras {


    private String altitude;
    private String cameraIndexCode;
    private String cameraName;
    private Integer cameraType;
    private String cameraTypeName;
    private String capabilitySet;
    private String capabilitySetName;
    private String intelligentSet;
    private String intelligentSetName;
    private String channelNo;
    private String channelType;
    private String channelTypeName;
    private String createTime;
    private String encodeDevIndexCode;
    private String encodeDevResourceType;
    private String encodeDevResourceTypeName;
    private String gbIndexCode;
    private String installLocation;
    private String keyBoardCode;
    private String latitude;
    private String longitude;
    private Integer pixel;
    private Integer ptz;
    private Integer ptzController;
    private String ptzControllerName;
    private String ptzName;
    private String recordLocation;
    private String recordLocationName;
    private String regionIndexCode;
    private Integer status;
    private String statusName;
    private Integer transType;
    private String transTypeName;
    private String treatyTypeName;
    private String treatyType;
    private String viewshed;
    private String updateTime;
}
