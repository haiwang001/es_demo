package com.bjxczy.onepark.security.interfaces.controller;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.utils.RedisUtils;
import com.bjxczy.onepark.security.service.HikDeviceService;
import com.bjxczy.onepark.security.task.AiAnalysisTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@ApiResourceController
@RequestMapping("/hik")
public class GetHikDataController {



    @Autowired
    private AiAnalysisTask aiAnalysisTask;

    @Autowired
    private HikDeviceService hikDeviceService;
    @GetMapping("/hik/fetchRecognitionResource")
    public JSONObject fetchRecognitionResource(){
        return hikDeviceService.fetchRecognitionResource();
    }



    @GetMapping("/aiAnalysisRedisData")
    public JSONObject aiAnalysisRedisData(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ANALYSIS_POSITION", RedisUtils.get("ANALYSIS_POSITION"));
        jsonObject.put("ANALYSIS_PLAN",RedisUtils.get("ANALYSIS_PLAN"));

        return jsonObject;
    }

    @GetMapping("/aiAnalysisRunTask")
    public JSONObject aiAnalysisRunTask(){
        aiAnalysisTask.runTask();
        return new JSONObject();
    }

}
