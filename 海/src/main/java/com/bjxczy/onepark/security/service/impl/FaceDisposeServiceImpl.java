package com.bjxczy.onepark.security.service.impl;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.onepark.common.config.HikConfiguration;
import com.bjxczy.onepark.common.config.HikStreamVideoConfig;
import com.bjxczy.onepark.common.model.face.FaceInformation;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import com.bjxczy.onepark.security.entity.FacePO;
import com.bjxczy.onepark.security.entity.GroupFacePO;
import com.bjxczy.onepark.security.mapper.FaceMapper;
import com.bjxczy.onepark.security.mapper.GroupFaceMapper;
import com.bjxczy.onepark.security.service.FaceDisposeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class FaceDisposeServiceImpl implements FaceDisposeService {

    private static final String ARTEMIS = "/artemis";

    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;
    @Autowired
    HikStreamVideoConfig hikStreamVideoConfig;
    @Autowired(required = false)
    HikConfiguration hikConfiguration;

    @Autowired
    private FaceMapper faceMapper;
    @Autowired
    private GroupFaceMapper groupFaceMapper;

    @Override
    public void createFaceEvent(FaceInformation info) {
        GroupFacePO groupFacePO = groupFaceMapper.selectOne(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getLocalGroupFaceId, info.getGroupId()));
        if (groupFacePO == null){
            return;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("faceGroupIndexCode",groupFacePO.getHikGroupFaceId());

        Map<String, Object> facemap = new HashMap<>();
        facemap.put("name",info.getPersonName());
        facemap.put("sex",info.getSex() == 0?"UNKNOWN":info.getSex());
        facemap.put("certificateType","111");
        facemap.put("certificateNum",info.getCardId());
        jsonObject.put("faceInfo",facemap);

        Map<String, Object> map = new HashMap<>();
//        map.put("faceUrl",info.getFacepath());
        map.put("faceBinaryData",info.getFacepath());
        jsonObject.put("facePic",map);
        log.info("海康添加人脸参数{}",jsonObject);
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face/single/addition";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("海康添加人脸返回"+resObject);
        if (!(resObject.containsKey("code") && ("0".equals(resObject.getString("code"))))) {
            log.error("海康添加人脸失败:{}",resObject.getString("msg"));
//            throw new RuntimeException("海康添加人脸失败" + resObject.getString("msg"));
        }
        //同步中间表
        JSONObject data = resObject.getJSONObject("data");
        FacePO facePO = new FacePO();
        facePO.setHikFaceId(data.getString("indexCode"));
        facePO.setLocalFaceId(info.getId());
        facePO.setHikGroupFaceId(groupFacePO.getHikGroupFaceId());
        faceMapper.insert(facePO);
    }

    @Override
    public void removeFaceEvent(FaceInformation info) {
        FacePO facePO = faceMapper.selectOne(Wrappers.<FacePO>lambdaQuery().eq(FacePO::getLocalFaceId, info.getId()));
        if (facePO == null){
            return;
        }
        JSONObject jsonObject = new JSONObject();
        List<String> strings = new ArrayList<>();
        strings.add(facePO.getHikFaceId());
        jsonObject.put("indexCodes",strings);
        jsonObject.put("faceGroupIndexCode",facePO.getHikGroupFaceId());

        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("海康删除人脸返回"+resObject);
        if (!(resObject.containsKey("code") && ("0".equals(resObject.getString("code"))))) {
            log.error("海康删除人脸失败:{}",resObject.getString("msg"));
//            throw new RuntimeException("海康删除人脸失败" + resObject.getString("msg"));
        }
        //同步中间表
        faceMapper.delete(Wrappers.<FacePO>lambdaQuery().eq(FacePO::getLocalFaceId, info.getId()));
    }

    @Override
    public void editFaceEvent(FaceInformation info) {
        FacePO facePO = faceMapper.selectOne(Wrappers.<FacePO>lambdaQuery().eq(FacePO::getLocalFaceId, info.getId()));
        if (facePO == null){
            return;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("indexCode",facePO.getHikFaceId());
        Map<String, Object> facemap = new HashMap<>();
        facemap.put("name",info.getPersonName());
        facemap.put("sex",info.getSex() == 0?"UNKNOWN":info.getSex());
        facemap.put("certificateType","111");
        facemap.put("certificateNum",info.getCardNum());
        jsonObject.put("faceInfo",facemap);
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(info.getFacepath())){
            if (info.getFacepath().split(",").length > 1){
                String[] split = info.getFacepath().split(",");
                map.put("faceBinaryData",split[1]);
            }else{
                map.put("faceBinaryData",info.getFacepath());
            }
        }
        jsonObject.put("facePic",map);
        log.info("海康修改人脸参数{}",jsonObject);

        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face/single/update";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("海康修改人脸返回"+resObject);
        if (!(resObject.containsKey("code") && ("0".equals(resObject.getString("code"))))) {
            log.error("海康修改人脸失败:{}",resObject.getString("msg"));
//            throw new RuntimeException("海康修改人脸失败" + resObject.getString("msg"));
        }
    }
}
