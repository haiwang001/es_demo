package com.bjxczy.onepark.security.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.onepark.common.model.face.RecognitionFaceInfo;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import com.bjxczy.onepark.security.entity.GroupFacePO;
import com.bjxczy.onepark.security.entity.RecognitionPO;
import com.bjxczy.onepark.security.feign.client.BmpSecurityManagerFeignClient;
import com.bjxczy.onepark.security.mapper.GroupFaceMapper;
import com.bjxczy.onepark.security.mapper.RecognitionMapper;
import com.bjxczy.onepark.security.service.RecognitionFaceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class RecognitionFaceServiceImpl implements RecognitionFaceService {

    private static final String RRICODE = "402282482f9d42478497c0ce4bf3adfd";
    @Autowired
    private BmpSecurityManagerFeignClient bmpSecurityManagerFeignClient;
    private static final String ARTEMIS = "/artemis";
    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;
    @Autowired
    private RecognitionMapper recognitionMapper;
    @Autowired
    private GroupFaceMapper groupFaceMapper;

    @Override
    public boolean addBlackRecognitionPlan(RecognitionFaceInfo info) {
        log.info("[ addBlackRecognitionPlan：添加重点人员识别计划======= ]:{}", info);
        String string = JSONObject.toJSONString(info);
        JSONObject jsonObject = JSONObject.parseObject(string);
        String[] cameraIndexCodes = jsonObject.getString("cameraIndexCodes").split(",");
        jsonObject.put("cameraIndexCodes", cameraIndexCodes);
        List<String> faceGroupIndexCodes = new ArrayList<>();
        String[] faceGroupIndexCodeString = jsonObject.getString("faceGroupIndexCodes").split(",");
        for (String faceGroupIndexCode : faceGroupIndexCodeString) {
            GroupFacePO groupFacePO = groupFaceMapper.selectOne(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getLocalGroupFaceId, faceGroupIndexCode));
            faceGroupIndexCodes.add(groupFacePO.getHikGroupFaceId());
        }
        jsonObject.put("faceGroupIndexCodes", faceGroupIndexCodes);

        String[] split = info.getRecognitionResourceIndexCodes().split(",");
        //识别资源
        List<String> objects = new ArrayList<>();
        objects.add(RRICODE);
//        jsonObject.put("recognitionResourceIndexCodes",split);
        jsonObject.put("recognitionResourceIndexCodes", objects);
        // 时间模板
        List<Map<String, Object>> timeBlockList = info.getTimeBlockList();
        jsonObject.put("timeBlockList", timeBlockList);
        //测试用数据
//        List<Object> list1 = new ArrayList<>();
//        Map<String, Object> map1 = new HashMap<>();
//        map1.put("startTime","08: 00");
//        map1.put("endTime","22: 00");
//        list1.add(map1);
//
//        List<Object> list = new ArrayList<>();
//        Map<String, Object> map = new HashMap<>();
//        map.put("dayOfWeek",4);
//        map.put("timeRange",list1);
//        list.add(map);
//
//        List<Object> list2 = new ArrayList<>();
//        Map<String, Object> map2 = new HashMap<>();
//        map2.put("startTime","08: 00");
//        map2.put("endTime","22: 00");
//        list2.add(map2);
//
//        Map<String, Object> weekmap = new HashMap<>();
//        weekmap.put("dayOfWeek",5);
//        weekmap.put("timeRange",list2);
//        list.add(weekmap);
//        jsonObject.put("timeBlockList",list);
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/black/addition";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        try {
            log.info("海康API添加重点人员识别计划调用参数信息:{}", jsonObject);
            JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
            log.info("海康API添加重点人员识别计划返回信息:{}", resJSONObject);
            Integer status = 0; // 1成功0失败
            if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
                log.error("海康API添加重点人员识别计划调用失败:{}", resJSONObject.getString("msg"));
//            throw new RuntimeException("海康API添加重点人员识别计划调用失败，返回" + resJSONObject.getString("msg"));
            }
            // 0失败 1 成功 2下发中
            RecognitionPO recognitionPO = new RecognitionPO();
            recognitionPO.setRecognitionId(info.getId());
            if (StringUtils.isNotBlank(resJSONObject.getString("data"))) {
                status = 1;
            }
            if (status == 1) {
                recognitionPO.setHikRecognitionId(resJSONObject.getString("data"));
                recognitionMapper.insert(recognitionPO);
            } else {
                bmpSecurityManagerFeignClient.update(info.getId(), 0);
            }
        } catch (Exception ex) {
            bmpSecurityManagerFeignClient.update(info.getId(), 0);
            ex.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean deleteBlackRecognitionPlan(RecognitionFaceInfo info) {
        log.info("[deleteBlackRecognitionPlan 删除重点人员]:{}", info);
        RecognitionPO recognitionPO = recognitionMapper.selectOne(Wrappers.<RecognitionPO>lambdaQuery().eq(RecognitionPO::getRecognitionId, info.getId()));
        if (recognitionPO == null) {
            return false;
        }
//        info.setIndexCode(recognitionPO.getHikRecognitionId());

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("indexCodes", new ArrayList<String>() {{
            add(recognitionPO.getHikRecognitionId());
        }});

        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/black/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("海康API删除重点人员识别计划返回信息:{}", resJSONObject);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康API删除重点人员识别计划调用失败:{}", resJSONObject.getString("msg"));
            throw new RuntimeException("海康API删除重点人员识别计划调用失败，返回" + resJSONObject.getString("msg"));
        }
        //删除关联表数据
        recognitionMapper.delete(Wrappers.<RecognitionPO>lambdaQuery().eq(RecognitionPO::getRecognitionId, info.getId()));
        return true;
    }

    @Override
    public boolean updateBlackRecognitionPlan(RecognitionFaceInfo info) {
        log.info("[updateBlackRecognitionPlan 修改重点人员]:{}", info);
        RecognitionPO recognitionPO = recognitionMapper.selectOne(Wrappers.<RecognitionPO>lambdaQuery().eq(RecognitionPO::getRecognitionId, info.getId()));
        if (recognitionPO == null) {
            return false;
        }
        info.setIndexCode(recognitionPO.getHikRecognitionId());
        String string = JSONObject.toJSONString(info);
        JSONObject jsonObject = JSONObject.parseObject(string);
        String[] cameraIndexCodes = jsonObject.getString("cameraIndexCodes").split(",");
        jsonObject.put("cameraIndexCodes", cameraIndexCodes);
        List<String> faceGroupIndexCodes = new ArrayList<>();
        String[] faceGroupIndexCodeString = jsonObject.getString("faceGroupIndexCodes").split(",");
        for (String faceGroupIndexCode : faceGroupIndexCodeString) {
            GroupFacePO groupFacePO = groupFaceMapper.selectOne(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getLocalGroupFaceId, faceGroupIndexCode));
            faceGroupIndexCodes.add(groupFacePO.getHikGroupFaceId());
        }
        jsonObject.put("faceGroupIndexCodes", faceGroupIndexCodes);
//        String[] split = info.getRecognitionResourceIndexCodes().split(",");
//        jsonObject.put("recognitionResourceIndexCodes",split);
        //识别资源
        List<String> objects = new ArrayList<>();
        objects.add(RRICODE);
        jsonObject.put("recognitionResourceIndexCodes", objects);

        // 时间模板
        List<Map<String, Object>> timeBlockList = info.getTimeBlockList();
        jsonObject.put("timeBlockList", timeBlockList);
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/black/update";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };

        try {

            Integer status = 0;
            log.error("海康API修改重点人员识别计划参数:{}", jsonObject);
            JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
            log.info("海康API修改重点人员识别计划返回信息:{}", resJSONObject);
            if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
                log.error("海康API修改重点人员识别计划调用失败:{}", resJSONObject.getString("msg"));
//            throw new RuntimeException("海康API修改重点人员识别计划调用失败，返回" + resJSONObject.getString("msg"));
            }
            if (StringUtils.isNotBlank(resJSONObject.getString("data"))) {
                status = 1;
            }
            if (status == 0){
                bmpSecurityManagerFeignClient.update(info.getId(), 0);
            }
        } catch (Exception ex) {
            bmpSecurityManagerFeignClient.update(info.getId(), 0);
            ex.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean addWhiteRecognitionPlan(RecognitionFaceInfo info) {
        log.info("[deleteWhiteRecognitionPlan 添加陌生人员计划]:{}", info);
        String string = JSONObject.toJSONString(info);
        JSONObject jsonObject = JSONObject.parseObject(string);
        String[] cameraIndexCodes = jsonObject.getString("cameraIndexCodes").split(",");
        jsonObject.put("cameraIndexCodes", cameraIndexCodes);
        List<String> faceGroupIndexCodes = new ArrayList<>();
        String[] faceGroupIndexCodeString = jsonObject.getString("faceGroupIndexCodes").split(",");
        for (String faceGroupIndexCode : faceGroupIndexCodeString) {
            GroupFacePO groupFacePO = groupFaceMapper.selectOne(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getLocalGroupFaceId, faceGroupIndexCode));
            faceGroupIndexCodes.add(groupFacePO.getHikGroupFaceId());
        }
        jsonObject.put("faceGroupIndexCodes", faceGroupIndexCodes);

        //识别资源
//        String[] split = info.getRecognitionResourceIndexCodes().split(",");
//        jsonObject.put("recognitionResourceIndexCodes",split);
        //识别资源
        List<String> objects = new ArrayList<>();
        objects.add(RRICODE);
        jsonObject.put("recognitionResourceIndexCodes", objects);


        // 时间模板
        List<Map<String, Object>> timeBlockList = info.getTimeBlockList();
        jsonObject.put("timeBlockList", timeBlockList);

        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/white/addition";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        try {
            Integer status = 0;
            log.info("海康API添加陌生人员识别计划调用参数信息:{}", jsonObject);
            JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
            log.info("海康API添加陌生人员识别计划返回信息:{}", resJSONObject);
            if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
                log.error("海康API添加陌生人员识别计划调用失败:{}", resJSONObject.getString("msg"));
//            throw new RuntimeException("海康API添加陌生人员识别计划调用失败，返回" + resJSONObject.getString("msg"));
            }
            RecognitionPO recognitionPO = new RecognitionPO();
            recognitionPO.setRecognitionId(info.getId());
            if (StringUtils.isNotBlank(resJSONObject.getString("data"))) {
                status = 1;
            }
            if (status == 1) {
                recognitionPO.setHikRecognitionId(resJSONObject.getString("data"));
//                bmpSecurityManagerFeignClient.update(info.getId(),status);
                recognitionMapper.insert(recognitionPO);
            } else {
                bmpSecurityManagerFeignClient.update(info.getId(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            bmpSecurityManagerFeignClient.update(info.getId(), 0);
        }
        return true;
    }

    @Override
    public boolean deleteWhiteRecognitionPlan(RecognitionFaceInfo info) {
        log.info("[deleteWhiteRecognitionPlan 删除陌生人员计划]:{}", info);
        RecognitionPO recognitionPO = recognitionMapper.selectOne(Wrappers.<RecognitionPO>lambdaQuery().eq(RecognitionPO::getRecognitionId, info.getId()));
        if (recognitionPO == null) {
            return false;
        }
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/white/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("indexCodes", new ArrayList<String>() {{
            add(recognitionPO.getHikRecognitionId());
        }});

        log.info("海康API删除陌生人员识别计划参数:{}", jsonObject);
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, JSONObject.toJSONString(info), null, null, "application/json", 3000);
        log.info("海康API删除陌生人员识别计划返回信息:{}", resJSONObject);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康API删除陌生人员识别计划调用失败:{}", resJSONObject.getString("msg"));
            throw new RuntimeException("海康API删除陌生人员识别计划调用失败，返回" + resJSONObject.getString("msg"));
        }
        //删除关联表数据
        recognitionMapper.delete(Wrappers.<RecognitionPO>lambdaQuery().eq(RecognitionPO::getRecognitionId, info.getId()));
        return true;
    }

    @Override
    public boolean updateWhiteRecognitionPlan(RecognitionFaceInfo info) {
        log.info("[updateWhiteRecognitionPlan 修改陌生人员计划]:{}", info);
        RecognitionPO recognitionPO = recognitionMapper.selectOne(Wrappers.<RecognitionPO>lambdaQuery().eq(RecognitionPO::getRecognitionId, info.getId()));
        if (recognitionPO == null) {
            return false;
        }
        info.setIndexCode(recognitionPO.getHikRecognitionId());
        String string = JSONObject.toJSONString(info);
        JSONObject jsonObject = JSONObject.parseObject(string);
        String[] cameraIndexCodes = jsonObject.getString("cameraIndexCodes").split(",");
        jsonObject.put("cameraIndexCodes", cameraIndexCodes);
        List<String> faceGroupIndexCodes = new ArrayList<>();
        String[] faceGroupIndexCodeString = jsonObject.getString("faceGroupIndexCodes").split(",");
        for (String faceGroupIndexCode : faceGroupIndexCodeString) {
            GroupFacePO groupFacePO = groupFaceMapper.selectOne(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getLocalGroupFaceId, faceGroupIndexCode));
            faceGroupIndexCodes.add(groupFacePO.getHikGroupFaceId());
        }
        jsonObject.put("faceGroupIndexCodes", faceGroupIndexCodes);
        //识别资源
//        String[] split = info.getRecognitionResourceIndexCodes().split(",");
//        jsonObject.put("recognitionResourceIndexCodes",split);

        //识别资源
        List<String> objects = new ArrayList<>();
        objects.add(RRICODE);
        jsonObject.put("recognitionResourceIndexCodes", objects);

        // 时间模板
        List<Map<String, Object>> timeBlockList = info.getTimeBlockList();
        jsonObject.put("timeBlockList", timeBlockList);
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/plan/recognition/white/update";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };

        try {
        Integer status = 0;
        log.error("海康API修改陌生人员识别计划参数:{}", jsonObject);
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("海康API修改陌生人员识别计划返回信息:{}", jsonObject);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("修改:{}", resJSONObject.getString("msg"));
//            throw new RuntimeException("海康API陌生重点人员识别计划调用失败，返回" + resJSONObject.getString("msg"));
        }
            if (StringUtils.isNotBlank(resJSONObject.getString("data"))) {
                status = 1;
            }
            if (status == 0){
                bmpSecurityManagerFeignClient.update(info.getId(), 0);
            }
        }catch (Exception ex){
            bmpSecurityManagerFeignClient.update(info.getId(), 0);
            ex.printStackTrace();
        }
        return true;
    }
}
