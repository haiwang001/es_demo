package com.bjxczy.onepark.security.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bjxczy.onepark.common.config.HikConfiguration;
import com.bjxczy.onepark.common.config.HikStreamVideoConfig;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import com.bjxczy.onepark.security.entity.FacePO;
import com.bjxczy.onepark.security.entity.GroupFacePO;
import com.bjxczy.onepark.security.mapper.FaceMapper;
import com.bjxczy.onepark.security.mapper.GroupFaceMapper;
import com.bjxczy.onepark.security.service.FaceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class FaceServiceImpl implements FaceService {

    private static final String ARTEMIS = "/artemis";

    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;
    @Autowired
    HikStreamVideoConfig hikStreamVideoConfig;
    @Autowired(required = false)
    HikConfiguration hikConfiguration;
    @Autowired
    private GroupFaceMapper groupFaceMapper;
    @Autowired
    private FaceMapper faceMapper;

    @Override
    public JSONObject getFaceGroup() {
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face/group";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject jsonObject = new JSONObject();
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (!(resJSONObject.containsKey("code") && ("0".equals(resJSONObject.getString("code"))))) {
            log.error("海康API获取人脸分组:{}",resJSONObject.getString("msg"));
            throw new RuntimeException("获取海康入人脸分组列表失败，返回" + resJSONObject.getString("msg"));
        }
        log.info("海康人脸分组列表:"+resJSONObject);
        return resJSONObject;
    }

    @Override
    public JSONObject groupFaceAdd(JSONObject jsonObject) {
        if (StringUtils.isBlank(jsonObject.getString("description"))){
            jsonObject.put("description",jsonObject.getString("name"));
        }
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face/group/single/addition";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        log.info("海康添加分组返回"+resObject);
        if (!(resObject.containsKey("code") && ("0".equals(resObject.getString("code"))))) {
            log.error("海康添加人脸库失败:{}",resObject.getString("msg"));
            throw new RuntimeException("海康添加人脸库失败" + resObject.getString("msg"));
        }
        //添加中间表
        GroupFacePO groupFacePO = new GroupFacePO();
        JSONObject data = resObject.getJSONObject("data");
        groupFacePO.setHikGroupFaceId(data.getString("indexCode"));
        groupFacePO.setLocalGroupFaceId(jsonObject.getInteger("id"));
        groupFaceMapper.insert(groupFacePO);
        return resObject;
    }

    @Override
    public JSONObject groupFaceUpdate(JSONObject jsonObject) {
        if (StringUtils.isBlank(jsonObject.getString("description"))){
            jsonObject.put("description",jsonObject.getString("name"));
        }
        GroupFacePO groupFacePO = groupFaceMapper.selectOne(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getLocalGroupFaceId, jsonObject.getInteger("indexCode")));
        jsonObject.put("indexCode",groupFacePO.getHikGroupFaceId());
        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face/group/single/update";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (resJSONObject.containsKey("code") && "0".equals(resJSONObject.getString("code"))) {
            log.info("修改海康人脸分组成功:{}", resJSONObject);
            return resJSONObject;
        } else {
            log.error("修改海康人脸分组失败:{}", resJSONObject.getString("msg"));
            throw new RuntimeException("修改海康人脸分组失败，返回" + resJSONObject.getString("msg"));
        }
    }

    @Override
    public JSONObject groupFaceDelete(JSONObject jsonObject) {
        GroupFacePO groupFacePO = groupFaceMapper.selectOne(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getLocalGroupFaceId, jsonObject.getInteger("id")));
        List<String> strings = new ArrayList<>();
        strings.add(groupFacePO.getHikGroupFaceId());
        jsonObject.put("indexCodes",strings);

        final String VechicleDataApi = ARTEMIS + "/api/frs/v1/face/group/batch/deletion";
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", VechicleDataApi);
            }
        };
        JSONObject resJSONObject = hikArtemisHttpUtils.doPostStringArtemis(path, jsonObject.toJSONString(), null, null, "application/json", 3000);
        if (resJSONObject.containsKey("code") && "0".equals(resJSONObject.getString("code"))) {
            groupFaceMapper.delete(Wrappers.<GroupFacePO>lambdaQuery().eq(GroupFacePO::getLocalGroupFaceId, jsonObject.getInteger("id")));
            log.info("删除海康人脸分组成功:{}", resJSONObject);
            //删除人脸中间表中关于人脸分组的记录
            faceMapper.delete(Wrappers.<FacePO>lambdaQuery().eq(FacePO::getHikGroupFaceId,groupFacePO.getHikGroupFaceId()));
        } else {
            log.error("删除海康人脸分组失败:{}", resJSONObject.getString("msg"));
            throw new RuntimeException("删除海康人脸分组失败，返回" + resJSONObject.getString("msg"));
        }
        return resJSONObject;
    }
}
