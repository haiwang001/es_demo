package com.bjxczy.onepark.security.task;


import cn.hutool.core.map.MapBuilder;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.handle.HikSendHandle;
import com.bjxczy.onepark.common.model.security.CameraInformation;
import com.bjxczy.onepark.security.feign.client.BmpSecurityManagerFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Slf4j
@Component
public class CameraRecordTask {

    //    定时录像 	0
    //    移动侦测 	1
    //    报警触发 	2
    //    报警触发或移动侦测 	3
    //    报警触发和移动侦测 	4
    //    命令触发 	5
    //    手动录像 	6
    //    智能录像 	7
    //    PIR报警 	10
    //    无线报警 	11
    //    呼救报警 	12
    //    全部事件 	13
    //    智能交通事件 	14
    //    越界侦测 	15
    //    区域入侵 	16
    //    声音异常 	17
    //    场景变更侦测 	18
    //    智能侦测（越界侦测\区域入侵\人脸侦测\声音异常\场景变更侦测） 	19
    //    人脸侦测 	20
    //    信号量 	21
    //    回传 	22
    //    回迁录像 	23
    //    遮挡 	24
    //    pos录像 	25
    //    进入区域侦测 	26
    //    离开区域侦测 	27
    //    徘徊侦测 	28
    //    人员聚集侦测 	29
    //    快速运动侦测 	30
    //    停车侦测 	31
    //    物品遗留侦测 	32
    //    物品拿取侦测 	33
    //    火点侦测 	34
    //    防破坏侦测 	35
    //    船只检测 	36
    //    测温预警 	37
    //    测温报警 	38
    //    打架斗殴报警 	39
    //    起身检测 	40
    //    瞌睡检测 	41
    //    温差报警 	42
    //    离线测温报警 	43
    //    防区报警 	44
    //    紧急求助 	45
    //    业务咨询 	46
    //    起身检测 	47
    //    折线攀高 	48
    //    如厕超时 	49
    //    奔跑检测 	50
    //    滞留检测 	51
    private static final Integer RECORD_TYPE = 6;
    @Autowired
    private HikSendHandle hikSendHandle;
    @Autowired
    private BmpSecurityManagerFeignClient bmpSecurityManagerFeignClient;


//    @Scheduled(cron = "0 20 01 * * ?")
//    @PostConstruct
    public void runTask() {
        log.info("[ CameraRecordTask ]====================> begin");
        //获取所有摄像头设备
        List<CameraInformation> cameraInformations = bmpSecurityManagerFeignClient.allCameras();
        cameraInformations.forEach(item -> {
            Map<String, Object> param = MapBuilder.create(new HashMap<String, Object>())
                    .put("cameraIndexCode", item.getCameraIndexCode()).build();
//            param.put("type", 1);
            JSONObject resp = hikSendHandle.post2("/api/video/v1/manualRecord/status",
                        JSONObject.toJSONString(param)
                );
            log.info("[ 设备：{} 录像状态-> {}]",item.getCameraName(),resp);
            //查看设备是否开启了录像
            if (!Objects.isNull(resp)) {
                Map data = (Map) resp.get("data");
                if (!Objects.isNull(data)) {
                    Integer taskStatus = (Integer) data.get("taskStatus");
                    //手动录像状态,0：未接入 1：正常执行 2：异常
                    if (taskStatus == 1) {
                        return ;
                    }
                }
                //手动录像
                param.put("recordType", RECORD_TYPE);
//                param.put("type", 1);
                JSONObject startResp = hikSendHandle.post2("/api/video/v1/manualRecord/start",
                        JSONObject.toJSONString(param)
                );
                log.info("[ 设备：{} 手动录像-> {}]",item.getCameraName(),startResp);
            }
        });
        log.info("[ CameraRecordTask ]====================> end");
    }

//    @Override
//    public void run(String... args) throws Exception {
//        this.runTask();
//    }
}
