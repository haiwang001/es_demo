package com.bjxczy.onepark.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("video_group_face")
public class GroupFacePO {
    private Integer localGroupFaceId;
    private String hikGroupFaceId;
    private Integer tenantId;
}
