package com.bjxczy.onepark.security.interfaces.controller;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.security.service.HikFaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HikFaceController {
    @Autowired
    private HikFaceService hikFaceService;
    //按条件批量查询人脸
    @PostMapping("/hik/queryFaceList")
    public JSONObject queryFace(@RequestBody JSONObject jsonObject){
       return hikFaceService.queryFaceList(jsonObject);
    }


    //修改人脸
    @PostMapping("/hik/updFace")
    public JSONObject updFace(@RequestBody JSONObject jsonObject){
        return hikFaceService.updFace(jsonObject);
    }

    //新增新增重点人员识别计划
    @PostMapping("/hik/addBlackRecognitionPlan")
    public JSONObject addBlackRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.addBlackRecognitionPlan(jsonObject);
    }
    //删除重点人员识别计划
    @PostMapping("/hik/deleteBlackRecognitionPlan")
    public JSONObject deleteBlackRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.deleteBlackRecognitionPlan(jsonObject);
    }
    //获取重点人员识别计划
    @PostMapping("/hik/getBlackRecognitionPlan")
    public JSONObject getBlackRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.getBlackRecognitionPlan(jsonObject);
    }
    //重新下发重点人员识别计划
    @PostMapping("/hik/restartBlackRecognitionPlan")
    public JSONObject restartBlackRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.restartBlackRecognitionPlan(jsonObject);
    }
    //修改重点人员识别计划
    @PostMapping("/hik/updateBlackRecognitionPlan")
    public JSONObject updateBlackRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.updateBlackRecognitionPlan(jsonObject);
    }



    //新增黑名单人员识别计划
    @PostMapping("/hik/addWhiteRecognitionPlan")
    public JSONObject addWhiteRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.addWhiteRecognitionPlan(jsonObject);
    }
    //删除黑名单人员识别计划
    @PostMapping("/hik/deleteWhiteRecognitionPlan")
    public JSONObject deleteWhiteRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.deleteWhiteRecognitionPlan(jsonObject);
    }
    //获取黑名单人员识别计划
    @PostMapping("/hik/getWhiteRecognitionPlan")
    public JSONObject getWhiteRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.getWhiteRecognitionPlan(jsonObject);
    }
    //重新下发黑名单人员识别计划
    @PostMapping("/hik/restartWhiteRecognitionPlan")
    public JSONObject restartWhiteRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.restartWhiteRecognitionPlan(jsonObject);
    }
    //修改黑名单人员识别计划
    @PostMapping("/hik/updateWhiteRecognitionPlan")
    public JSONObject updateWhiteRecognitionPlan(@RequestBody JSONObject jsonObject){
        return hikFaceService.updateWhiteRecognitionPlan(jsonObject);
    }
    //获取人脸图片
    @PostMapping("/hik/getFacePic")
    public JSONObject getFacePic(@RequestBody JSONObject jsonObject){
        return hikFaceService.getFacePic(jsonObject);
    }

    //按条件查询高频人员识别事件
    @PostMapping("/hik/queryHighFrequency")
    public JSONObject queryHighFrequency(@RequestBody JSONObject jsonObject){
        return hikFaceService.queryHighFrequency(jsonObject);
    }
    //按条件查询重点人员事件
    @PostMapping("/hik/queryBlack")
    public JSONObject queryBlack(@RequestBody JSONObject jsonObject){
        return hikFaceService.queryBlack(jsonObject);
    }
    //按条件查询高频人员识别事件
    @PostMapping("/hik/queryStranger")
    public JSONObject queryStranger(@RequestBody JSONObject jsonObject){
        return hikFaceService.queryStranger(jsonObject);
    }


    @PostMapping("/hik/getLocalGroupId")
    public JSONObject getLocalGroupId(@RequestBody JSONObject jsonObject){
        return hikFaceService.getLocalGroupId(jsonObject);
    }

    @PostMapping("/hik/getLocalFaceId")
    public JSONObject getLocalFaceId(@RequestBody JSONObject jsonObject){
        return hikFaceService.getLocalFaceId(jsonObject);
    }

    @PostMapping("/hik/getPlanStatus")
    public JSONObject getPlanStatus(@RequestBody JSONObject jsonObject){
        return hikFaceService.getPlanStatus(jsonObject);
    }

}
