package com.bjxczy.onepark.security.feign.client;

import com.bjxczy.core.feign.client.security.CamerasFeign;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("bmpsecurity")
public interface BmpSecurityManagerFeignClient extends CamerasFeign {
}
