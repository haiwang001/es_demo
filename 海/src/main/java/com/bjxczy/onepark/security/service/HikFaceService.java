package com.bjxczy.onepark.security.service;

import com.alibaba.fastjson.JSONObject;

public interface HikFaceService {
    JSONObject queryFaceList(JSONObject jsonObject);


    JSONObject addBlackRecognitionPlan(JSONObject jsonObject);
    JSONObject deleteBlackRecognitionPlan(JSONObject jsonObject);
    JSONObject getBlackRecognitionPlan(JSONObject jsonObject);
    JSONObject restartBlackRecognitionPlan(JSONObject jsonObject);
    JSONObject updateBlackRecognitionPlan(JSONObject jsonObject);


    JSONObject addWhiteRecognitionPlan(JSONObject jsonObject);
    JSONObject deleteWhiteRecognitionPlan(JSONObject jsonObject);
    JSONObject getWhiteRecognitionPlan(JSONObject jsonObject);
    JSONObject restartWhiteRecognitionPlan(JSONObject jsonObject);
    JSONObject updateWhiteRecognitionPlan(JSONObject jsonObject);

    JSONObject getFacePic(JSONObject jsonObject);

    JSONObject queryHighFrequency(JSONObject jsonObject);
    JSONObject queryBlack(JSONObject jsonObject);
    JSONObject queryStranger(JSONObject jsonObject);

    JSONObject getLocalGroupId(JSONObject jsonObject);

    JSONObject getLocalFaceId(JSONObject jsonObject);

    JSONObject updFace(JSONObject jsonObject);

    JSONObject getPlanStatus(JSONObject jsonObject);
}
