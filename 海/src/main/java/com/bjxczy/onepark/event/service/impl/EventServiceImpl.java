package com.bjxczy.onepark.event.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.event.security.EventVssPayload;
import com.bjxczy.core.rabbitmq.supports.SecurityBaseListener;
import com.bjxczy.onepark.event.config.EventProperties;
import com.bjxczy.onepark.event.publish.CarCrossParkEventPublisher;
import com.bjxczy.onepark.event.publish.FaceAuthenticationEventPublisher;
import com.bjxczy.onepark.event.publish.FaceEventPublisher;
import com.bjxczy.onepark.event.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class EventServiceImpl implements EventService {

	@Autowired
	private EventProperties properties;
	@Autowired
	private FaceEventPublisher faceEventPublisher;
	@Autowired
	private FaceAuthenticationEventPublisher faceAuthenticationEventPublisher;

	@Autowired
	private CarCrossParkEventPublisher carCrossParkEventPublisher;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	/**
	 * 海康事件处理 
	 * event_vss：视频事件 
	 * event_acs：门禁事件 
	 * event_mpc：园区卡口事件 
	 * event_pms：停车场事件
	 * event_face：人脸监控事件
	 */
	@Override
	public void eventRcv(JSONObject json) {
		if (properties.getShowLog()) {
			log.info("[EventRcv] body={}", json);
		}
		JSONObject params = json.getJSONObject("params");
		switch (params.getString("ability")) {
		case "event_vss":
			eventVssHandler(params);
			break;
		case "event_acs":
			faceAuthenticationEventPublisher.eventFaceHandler(json);
			break;
		case "event_pms":
			carCrossParkEventPublisher.inAndOutParkEventReceive(json);
			break;
		case "event_face_recognition":
			faceEventPublisher.eventFaceHandler(params);
			break;
		default:
			if (properties.getShowLog()) {
				log.info("[EventRcv] unkonw event={}", json);
			}
			break;
		}
	}
	
	public static Date toHappenDate(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		try {
			return sdf.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * AI视频事件处理
	 * @param params
	 */
	private void eventVssHandler(JSONObject params) {
		JSONArray events = params.getJSONArray("events");
		List<EventVssPayload> list = new ArrayList<>();
		for (int i = 0; i < events.size(); i++) {
			JSONObject obj = events.getJSONObject(i);
			EventVssPayload item = obj.toJavaObject(EventVssPayload.class);
			item.setHappenTime(toHappenDate(obj.getString("happenTime")));
			list.add(item);
		}
		rabbitTemplate.convertAndSend(SecurityBaseListener.DEFAULT_EXCAHNGE_NAME, SecurityBaseListener.VSS_EVENT, list);
	}

}
