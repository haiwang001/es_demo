package com.bjxczy.onepark.event.interfaces.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.event.service.EventService;

@ApiResourceController
@RequestMapping("/event")
public class EventController {
	
	@Autowired
	private EventService eventService;
	
	@PostMapping(value = "/rcv", name = "海康统一事件回调")
	public void eventRcv(@RequestBody JSONObject json) {
		eventService.eventRcv(json);
	}

}
