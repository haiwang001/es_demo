package com.bjxczy.onepark.event.publish;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.model.car.HikInAndOutRecord;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static com.bjxczy.core.rabbitmq.supports.CarBaseListener.DEFAULT_EXCAHNGE_NAME;
import static com.bjxczy.core.rabbitmq.supports.CarBaseListener.IN_AND_OUT_RECORD_EVENT;

/**
 * @Auther: Administrator
 * @Date: 2023/7/5 0005 10:14
 * @Description: CarCrossParkEventPublisher
 * @Version 1.0.0
 */
@Component
@Slf4j
public class CarCrossParkEventPublisher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void inAndOutParkEventReceive(JSONObject jsonObject) {
        log.error("------------------------------------------------------------------------------------------------");
        log.error("订阅海康车辆入场放行事件报文：{}",jsonObject);
        log.error("------------------------------------------------------------------------------------------------");
        //将海康报文转换为系统需要的实体
        if (ObjectUtils.isEmpty(jsonObject)||!(jsonObject.containsKey("method"))){
            log.error("订阅海康车辆入场放行事件报文：{}",jsonObject);
            throw new RuntimeException("订阅海康车辆入场放行事件报文出错" + jsonObject);
        }
        //外层对象
        JSONObject params = jsonObject.getJSONObject("params");
        JSONArray events = params.getJSONArray("events");
        //数组判断并遍历进行内容的赋值及推送
        if (CollectionUtils.isNotEmpty(events)){
            for (Object event : events) {
                HikInAndOutRecord hikInAndOutRecord = new HikInAndOutRecord();
                String eventSting = JSONObject.toJSONString(event);
                JSONObject eventJsonObject = JSONObject.parseObject(eventSting);
                hikInAndOutRecord.setParkEventType(String.valueOf(eventJsonObject.getInteger("eventType")));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                try {
                    hikInAndOutRecord.setSaveTime(sdf.parse(eventJsonObject.getString("happenTime")));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
                JSONObject data = eventJsonObject.getJSONObject("data");
                hikInAndOutRecord.setPlateNumber(data.getString("plateNo"));
                hikInAndOutRecord.setInOrOut(data.getInteger("inoutType"));
                hikInAndOutRecord.setParkId(data.getString("parkIndex"));
                hikInAndOutRecord.setDeviceId(data.getString("gateIndex"));
                hikInAndOutRecord.setDeviceName(data.getString("gateName"));
                hikInAndOutRecord.setExtra(eventSting);
                log.error("------------------------------------------------------------------------------------------------");
                log.error("车辆进出记录mq发送消息-",hikInAndOutRecord.toString());
                log.error("------------------------------------------------------------------------------------------------");
                rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME,IN_AND_OUT_RECORD_EVENT,hikInAndOutRecord);
            }
        }
    }
}
