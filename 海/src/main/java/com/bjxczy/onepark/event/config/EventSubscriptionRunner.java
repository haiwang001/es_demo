package com.bjxczy.onepark.event.config;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.manager.HikvisionApiProxy;
import com.bjxczy.onepark.common.manager.HikvisionBody;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EventSubscriptionRunner implements ApplicationListener<WebServerInitializedEvent> {

	@Autowired
	private EventProperties properties;
	@Autowired
	private HikvisionApiProxy apiProxy;

	private static final String BASE_PATH = "/artemis";

	@Override
	public void onApplicationEvent(WebServerInitializedEvent event) {
		// 查询已订阅事件
		HikvisionBody body = apiProxy.post(BASE_PATH + "/api/eventService/v1/eventSubscriptionView").execute()
				.toHikvisionBody();
		JSONArray detail = body.getData().getJSONArray("detail");
		List<Integer> subscribed = new ArrayList<>(), codes = properties.getCodes();
		for (int i = 0; i < detail.size(); i++) {
			JSONObject obj = detail.getJSONObject(i);
			if (properties.getCallbackUrl().equals(obj.getString("eventDest"))) {
				subscribed = obj.getJSONArray("eventTypes").toJavaList(Integer.class);
			}
		}
		// 比对订阅事件
		final List<Integer> temp = subscribed;
		List<Integer> cancel = subscribed.stream().filter(e -> codes.indexOf(e) == -1).collect(Collectors.toList());
		List<Integer> sub = codes.stream().filter(e -> temp.indexOf(e) == -1).collect(Collectors.toList());
		// 取消订阅
		if (cancel.size() > 0) {
			String respCancel = apiProxy.post(BASE_PATH + "/api/eventService/v1/eventUnSubscriptionByEventTypes")
					.setBody("eventTypes", cancel).execute().toResponseString();
			log.info("cancel subscribe resp={}", respCancel);
		}
		// 新增订阅
		if (sub.size() > 0) {
			String respSub = apiProxy.post(BASE_PATH + "/api/eventService/v1/eventSubscriptionByEventTypes")
					.setBody("eventTypes", sub)
					.setBody("eventDest", properties.getCallbackUrl())
					.setBody("subType", 2) // 订阅原始事件
					.execute().toResponseString();
			log.info("subscribe resp={}", respSub);
		}
	}

}
