package com.bjxczy.onepark.event.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import lombok.Data;

@Data
@RefreshScope
@ConfigurationProperties("hikvision-event")
public class EventProperties {
	
	private String callbackUrl;
	
	private List<Integer> codes;
	
	private Boolean showLog = false;

}
