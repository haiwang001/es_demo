package com.bjxczy.onepark.event.publish;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.event.door.InOrOutRecordPayload;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.onepark.common.manager.HikvisionApiService;
import com.bjxczy.onepark.door.dto.FaceDiscernSucceedDto;
import com.bjxczy.onepark.door.dto.HikEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
@Slf4j
public class FaceAuthenticationEventPublisher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private HikvisionApiService hikService;

    /**
     * @param params
     */
    public void eventFaceHandler(JSONObject params) {
        FaceDiscernSucceedDto dto = JSON.parseObject(params.toString(), FaceDiscernSucceedDto.class);
        if (CollectionUtils.isNotEmpty(dto.getParams().getEvents())) {
            for (HikEvent event : dto.getParams().getEvents()) {
                //转换实体-转换为mq实体
                InOrOutRecordPayload payload = new InOrOutRecordPayload();
                //员工编号
                payload.setBusinessId(event.getData().getExtEventPersonNo());
                payload.setDeviceCode(event.getSrcParentIndex());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
                try {
                    payload.setTime(sdf.parse(event.getHappenTime()));
                } catch (ParseException e) {
                    log.error("解析海康报文时间失败",e);
                }
                if (event.getData().getExtEventCode().equals("196893")) {//人脸成功
                    payload.setEventPictrue(getLocalUrl(event.getData().getExtEventPictureURL()));
                    payload.setEventType("FACE_SUCCESS");
                }
                if (event.getData().getExtEventCode().equals("197151")) {//人脸失败
                    payload.setEventPictrue(getLocalUrl(event.getData().getExtEventPictureURL()));
                    payload.setEventType("FACE_FAIL");
                }
                if (event.getData().getExtEventCode().equals("198914")) {//刷卡成功
                    payload.setCardNum(event.getData().getExtEventCardNo());
                    payload.setEventType("CARD_SUCCESS");
                }
                rabbitTemplate.convertAndSend(DoorMutualBaseListener.DEFAULT_EXCAHNGE_NAME,
                        DoorMutualBaseListener.PUSH_IN_OR_OUT_RECORD, payload);
            }
        }
    }

    /**
     * 安全查询海康图片BASE64地址
     * @param url
     * @return
     */
    private String getLocalUrl(String url) {
        try {
            return hikService.getFileUrlByHikvision(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
