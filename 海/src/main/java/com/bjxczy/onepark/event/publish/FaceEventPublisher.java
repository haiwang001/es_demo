package com.bjxczy.onepark.event.publish;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.event.security.EventFacePayload;
import com.bjxczy.core.rabbitmq.event.security.EventStrangerPayload;
import com.bjxczy.core.rabbitmq.supports.SecurityBaseListener;
import com.bjxczy.onepark.common.manager.HikvisionApiService;

import cn.hutool.core.bean.BeanUtil;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class FaceEventPublisher extends SecurityBaseListener {
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	@Autowired
	private HikvisionApiService hikService;
	
	/**
	 * 人脸布控事件处理
	 * @param params
	 */
	public void eventFaceHandler(JSONObject params) {
		JSONArray events = params.getJSONArray("events");
		List<EventFacePayload> faces = new ArrayList<>();
		List<EventStrangerPayload> stangers = new ArrayList<>();
		for (int i = 0; i < events.size(); i++) {
			JSONObject obj = events.getJSONObject(i);
			String eventType = obj.getString("eventType");
			if ("1644175361".equals(eventType)) {
				// 重点人员识别
				faces.add(toFaceEventPayload(obj));
			} else if ("1644171265".equals(eventType)) {
				// 陌生人识别
				stangers.add(toStangerEventPayload(obj));
			} else {
				log.info("[EventRcv] unknow face event! eventType={}", eventType);
			}
		}
		if (faces.size() > 0) {
			rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME, FACE_EVENT, faces);
		}
		if (stangers.size() > 0) {
			rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME, STANGER_EVENT, stangers);
		}
	}
	
	/**
	 * 安全查询海康图片BASE64地址
	 * @param url
	 * @return
	 */
	private String getLocalUrl(String url) {
		return hikService.getFileUrlByHikvision(url);
	}

	/**
	 * 海康数据重点人员识别数据转换
	 * @param event
	 * @return
	 */
	private EventFacePayload toFaceEventPayload(JSONObject event) {
		EventFacePayload data = toEventPayload(new EventFacePayload(), event);
		// 填充图片
		if (StringUtils.isNotBlank(data.getBkgUrl())) {
			data.setBkgUrl(getLocalUrl(data.getBkgUrl()));
		}
		if (StringUtils.isNotBlank(data.getFaceUrl())) {
			data.setFaceUrl(getLocalUrl(data.getFaceUrl()));
		}
		if (StringUtils.isNotBlank(data.getFacePicUrl())) {
			data.setFacePicUrl(getLocalUrl(data.getFacePicUrl()));
		}
		return data;
	}
	
	/**
	 * 海康数据陌生人识别数据转换
	 * @param event
	 * @return
	 */
	private EventStrangerPayload toStangerEventPayload(JSONObject event) {
		EventStrangerPayload data = toEventPayload(new EventStrangerPayload(), event);
		// 填充图片
		if (StringUtils.isNotBlank(data.getBkgUrl())) {
			data.setBkgUrl(getLocalUrl(data.getBkgUrl()));
		}
		if (StringUtils.isNotBlank(data.getFaceUrl())) {
			data.setFaceUrl(getLocalUrl(data.getFaceUrl()));
		}
		return data;
	}
	
	private <T> T toEventPayload(T payload, JSONObject event) {
		payload = BeanUtil.fillBeanWithMap(event, payload, true);
		JSONObject data = event.getJSONObject("data"),
				faceRecognitionResult = data.getJSONObject("faceRecognitionResult"),
				snap = faceRecognitionResult.getJSONObject("snap");
		// 写入识别字段
		payload = BeanUtil.fillBeanWithMap(snap, payload, true);
		// 陌生人识别不存在此字段
		JSONArray faceMatch = faceRecognitionResult.getJSONArray("faceMatch");
		if (faceMatch != null && faceMatch.size() > 0) {
			// 写入匹配字段，默认取第一个（识别度最高的人脸）XXX
			JSONObject one = faceMatch.getJSONObject(0);
			payload = BeanUtil.fillBeanWithMap(one, payload, true);
		}
		return payload;
	}

}
