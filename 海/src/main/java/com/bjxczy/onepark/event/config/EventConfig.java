package com.bjxczy.onepark.event.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventConfig {
	
	@Bean
	public EventProperties eventProperties() {
		return new EventProperties();
	}
	
	@Bean
	public EventSubscriptionRunner eventSubscriptionRunner() {
		return new EventSubscriptionRunner();
	}

}
