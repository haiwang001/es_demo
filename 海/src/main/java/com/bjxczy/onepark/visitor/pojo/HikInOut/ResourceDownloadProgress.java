package com.bjxczy.onepark.visitor.pojo.HikInOut;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/9 15:12
 */
@Data
public class ResourceDownloadProgress {
    private Integer downloadResult;// 下载结果（当下载结束时有值，0:成功,1:失败，2：部分失败）
    private String  errorCode;// 下载错误码，参考附录E.2.3 出入控制权限错误码
}
