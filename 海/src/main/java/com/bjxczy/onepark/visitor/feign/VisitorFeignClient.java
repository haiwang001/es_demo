package com.bjxczy.onepark.visitor.feign;


import com.bjxczy.core.feign.client.visitor.BaseHikVisitorFeignClient;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.common.utils.MD5Utils;
import com.bjxczy.onepark.visitor.service.HkVisitorService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.nio.charset.StandardCharsets;


/*
 *@Author wlw
 *@Date 2023/6/19 17:43
 */
@Log4j2
@Controller
@ResponseBody
public class VisitorFeignClient implements BaseHikVisitorFeignClient {
    @Autowired
    private HkVisitorService hkVisitorService;

    @Override
    public CreateVisitorAndDoorVo createVisitor(String receptionistId,
                                                   String visitStartTime,
                                                   String visitEndTime,
                                                   String visitorName,
                                                   String mobile,
                                                   String gender,
                                                   String visitorPhoto) {
        try {
            return hkVisitorService.createVisitor(receptionistId,
                    visitStartTime, visitEndTime, visitorName,
                    mobile, gender, MD5Utils.transcoding(visitorPhoto, StandardCharsets.UTF_16BE,StandardCharsets.UTF_8),null);
        } catch (Exception e) {
            log.error("新增访客异常  访客信息:{}", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Boolean getVisitorRecords(String visitorName, String visitStartTimeBegin, String visitStartTimeEnd,String orderId) {
        try {
            return hkVisitorService.getVisitorRecords(visitorName, visitStartTimeBegin, visitStartTimeEnd,orderId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}