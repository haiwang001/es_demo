package com.bjxczy.onepark.visitor.pojo.HikInOut;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/6 10:36
 */
@Data
public class ExtEventData {
    private String ExtEventPersonNo; // 员工编号
    private Integer ExtEventInOut; // 员工编号  1 进 0 出
    private ExtEventIdentityCardInfo ExtEventIdentityCardInfo; // 员工编号
}
