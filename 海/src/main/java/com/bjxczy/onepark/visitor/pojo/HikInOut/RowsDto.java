package com.bjxczy.onepark.visitor.pojo.HikInOut;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/12 18:04
 */
@Data
public class RowsDto {
    private String eventTime;      // 事件时间ISO格式，参考附录B ISO8601时间格式说明 触发的时间
    private String visitStartTime; // 来访时间ISO格式，参考附录B ISO8601时间格式说明 触发的时间
    private String visitEndTime;   // 结束时间ISO格式，参考附录B ISO8601时间格式说明 触发的时间
    private String orderId;   // 访客id
    private String visitorName;   // 访客姓名
    private String beVisitPersonName;   // 接访人
    private String deviceIndexCode;   // 设备编码
}
