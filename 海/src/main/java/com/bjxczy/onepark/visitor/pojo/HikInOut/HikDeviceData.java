package com.bjxczy.onepark.visitor.pojo.HikInOut;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/3/31 16:28
 */
@Data
public class HikDeviceData {
    private Integer total;
    private String faceId; //人脸Id
    private String faceUrl; //人脸图片Url
    private String personId; //人员ID personId
    private Integer pageNo;
    private Integer pageSize;
    private List<HikGuardInfo> list;
    private String taskId; //下载任务唯一标识
    /**
     * 人脸图片检测结果
     * true-合格
     * false-不合格
     */
    private Boolean checkResult;
    private Boolean isDownloadFinished; //下载是否结束 true 下载结束 false 未结束
    private List<ResourceDownloadProgress> resourceDownloadProgress;
    private List<RowsDto> rows;
    private String orderId; // 访客预约成功的id 对应人员编号
    private String organizationCode; // 组织查询
}
