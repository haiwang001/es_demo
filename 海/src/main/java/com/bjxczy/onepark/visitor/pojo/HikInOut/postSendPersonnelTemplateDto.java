package com.bjxczy.onepark.visitor.pojo.HikInOut;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/3 16:44
 */
@Data
public class postSendPersonnelTemplateDto {
    private String state;
    private String stateLabel;
    private String ip;
    private String code;
}
