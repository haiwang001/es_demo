package com.bjxczy.onepark.visitor.listener;


import com.bjxczy.core.rabbitmq.base.BaseRabbitListener;
import com.bjxczy.core.rabbitmq.rpc.AsyncReplyHandler;
import com.bjxczy.core.rabbitmq.supports.VisitorBaseListener;
import com.bjxczy.onepark.common.enums.PlatformTypeCode;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorInformation;
import com.bjxczy.onepark.common.model.visitor.DeleteVisitorInformation;
import com.bjxczy.onepark.visitor.service.HkVisitorService;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
 *@Author wlw Async
 *@Date 2023/6/24 10:54
 */
@Log4j2
@Service
public class AsyncVisitorListener extends BaseRabbitListener {

    @Autowired
    private AsyncReplyHandler replyHandler;
    @Autowired
    private HkVisitorService hkVisitorService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmp_visitor_create_event_queue", durable = "true"),  // 持久化队列
                    key = VisitorBaseListener.CREATE_VISITOR_EVENT,
                    exchange = @Exchange(value = VisitorBaseListener.DEFAULT_EXCAHNGE_NAME) // 绑定的交换机
            )
    })
    public void visitorEventHandler(CreateVisitorInformation po, Channel channel, Message message) {
        try {
            CreateVisitorAndDoorVo visitor = hkVisitorService.createVisitor(po.getReceptionistId().toString(),
                    po.getVisitStartTime(), po.getVisitEndTime(), po.getVisitorName(),
                    po.getMobile(), po.getGender(), po.getVisitorPhoto(),po.getGroup());
            visitor.setType(PlatformTypeCode.PLATFORM_TYPE_CODE.getMsg());
            visitor.setId(po.getId());
            //replyHandler.reply(message, visitor);
            rabbitTemplate.convertAndSend(VisitorBaseListener.DEFAULT_EXCAHNGE_NAME, VisitorBaseListener.DEL_VISITOR_EVENT_MSG,visitor );
            this.confirm(() -> true, channel, message);
        } catch (Exception e) {
            CreateVisitorAndDoorVo vo = new CreateVisitorAndDoorVo(-1, "海康平台连接失败!", po.getId());
            vo.setType(PlatformTypeCode.PLATFORM_TYPE_CODE.getMsg());

            vo.setId(po.getId());
           // replyHandler.reply(message, vo);
            rabbitTemplate.convertAndSend(VisitorBaseListener.DEFAULT_EXCAHNGE_NAME, VisitorBaseListener.DEL_VISITOR_EVENT_MSG,vo );
            e.printStackTrace();
            this.confirm(() -> true, channel, message);
        }
    }
}
