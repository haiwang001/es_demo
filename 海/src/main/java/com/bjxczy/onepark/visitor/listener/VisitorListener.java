package com.bjxczy.onepark.visitor.listener;


import com.bjxczy.core.rabbitmq.supports.VisitorBaseListener;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorInformation;
import com.bjxczy.onepark.common.model.visitor.DeleteVisitorInformation;
import com.bjxczy.onepark.visitor.service.HkVisitorService;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
 *@Author wlw
 *@Date 2023/6/24 10:54
 */
@Log4j2
@Component
public class VisitorListener extends VisitorBaseListener {

    @Autowired
    private HkVisitorService hkVisitorService;

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "visitor_delete_queue", durable = "true"),
                    key = DEL_VISITOR_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void deleteVisitor(DeleteVisitorInformation delete, Channel channel, Message message) {
        hkVisitorService.deleteVisitor(delete.getOrderId());
        this.confirm(() -> true, channel, message);
    }
}
