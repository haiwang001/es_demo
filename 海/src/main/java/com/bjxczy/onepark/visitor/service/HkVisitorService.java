package com.bjxczy.onepark.visitor.service;


import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;

import java.util.List;

public interface HkVisitorService {

    CreateVisitorAndDoorVo createVisitor(String receptionistId,
                                         String visitStartTime,
                                         String visitEndTime,
                                         String visitorName,
                                         String mobile,
                                         String gender,
                                         String visitorPhoto, List<String> group) throws Exception;

    boolean deleteVisitor(String orderId) ;

    boolean getVisitorRecords(String visitorName, String visitStartTimeBegin, String visitStartTimeEnd,String orderId) throws Exception;
}
