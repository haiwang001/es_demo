package com.bjxczy.onepark.visitor.service.impl;

import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.common.cons.HikApi;
import com.bjxczy.onepark.common.manager.HikvisionApiProxy;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.common.utils.DateUtil;
import com.bjxczy.onepark.common.utils.hik.StaffUtils;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikDeviceReturnDto;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikGuardInfo;
import com.bjxczy.onepark.visitor.service.HkVisitorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
@Slf4j
public class HkVisitorServiceImpl implements HkVisitorService {

    @Autowired
    private HikvisionApiProxy apiProxy;

    @Override
    public boolean getVisitorRecords(String visitorName, String visitStartTimeBegin, String visitStartTimeEnd, String orderId) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("visitorName", visitorName);
        map.put("visitStartTimeBegin", DateUtil.dateToISO8601(visitStartTimeBegin)); //到访时间
        map.put("visitStartTimeEnd", DateUtil.dateToISO8601(visitStartTimeEnd)); //结束时间
        map.put("orderId", orderId); //访客id
        map.put("pageNo", 1);
        map.put("pageSize", 1000);
        String hikResponse = apiProxy.post(HikApi.HAIKANG_VISITING_RECORDS_API).setBody(map).execute().toResponseString();
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse, HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode())) {
            return dto.getData().getList().size()==0;
        }
        return false;
    }

    @Override
    public boolean deleteVisitor(String orderId) {
        List<String> arrayList = new ArrayList<>();
        arrayList.add(AppointmentRecord(orderId));
        String hikResponse = apiProxy.post(HikApi.HAIKANG_VISITOR_APPOINTMENT_CANCEL_API).setBody(arrayList).execute().toResponseString();
        apiProxy.post(HikApi.HAIKANG_VISITOR_OUT_API).setBody("orderId", orderId).execute().toResponseString();
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse, HikDeviceReturnDto.class);
        if (dto != null && "0".equals(Objects.requireNonNull(dto).getCode())) {
            log.info("访客结束---> {}", dto);
            return true;
        } else {
            log.info("取消预约 失败  ---> {}", dto);
            return false;
        }
    }


    /**
     * @param orderId
     * @return
     * @throws Exception
     */
    private String AppointmentRecord(String orderId) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("pageNo", 1);
        map.put("pageSize", 10);
        map.put("orderId", orderId); // 使用记录起来访客id
        String hikResponse = apiProxy.post(HikApi.HAIKANG_VISITOR_APPOINTMENT_RECORDS_API).setBody(map).execute().toResponseString();
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse, HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode())) {
            if (dto.getData() != null) {
                return dto.getData().getList().get(0).getAppointRecordId();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * *
     * * @param receptionistId 接访人id
     * * @param visitStartTime 到访时间
     * * @param visitEndTime   结束时间
     * * @param visitorName    访客名称
     * * @param mobile         访客手机号
     * * @param gender         访客性别
     * * @param visitorPhoto   访客照片
     * * @return
     * * @throws Exception
     */
    @Override
    public CreateVisitorAndDoorVo createVisitor(
            String receptionistId,
            String visitStartTime,
            String visitEndTime,
            String visitorName,
            String mobile,
            String gender,
            String visitorPhoto, List<String> group) throws Exception {
        HashMap<String, Object> json = new HashMap<>();
        json.put("receptionistId", receptionistId); // c72756eb3e024086af4d60f35ffbd8d3    info.getStaffId()
        json.put("visitStartTime", DateUtil.dateToISO8601(visitStartTime)); // 来访时间
        json.put("visitEndTime", DateUtil.dateToISO8601(visitEndTime)); // 离开时间
        HashMap<String, Object> visitorInfo = new HashMap<>();
        visitorInfo.put("visitorName", visitorName);
        visitorInfo.put("phoneNo", mobile);
        visitorInfo.put("gender", gender);
        visitorInfo.put("visitorPhoto", StaffUtils.getFileBase64String(visitorPhoto));
        json.put("visitorInfo", visitorInfo);
        HashMap<String, Object> visitorPermissionSet = new HashMap<>();
        visitorPermissionSet.put("privilegeGroupIds", group);// 访客权限组
        json.put("visitorPermissionSet", visitorPermissionSet);
        json.put("visitPurpose", "visitPurpose"); // 访问事由
        json.put("personNum", 1);  // 访问人数
        String hikResponse = apiProxy.post(HikApi.HAIKANG_FACE_APPOINTMENT_REGISTRATION_API).setBody(json).execute().toResponseString();
        HikDeviceReturnDto dto = null;
        try {
            dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
            log.info("访客预约免登记结果解析后 {}", dto);
            if ("0".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(0, dto.getData().getOrderId());
            }
            if ("0x05314023".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(-1, "证件号或者手机号和其他访客冲突!");
            }
            if ("0x0531f010".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(-1, "访客数量超过限制!");
            }
            if ("0x0531f002".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(-1, "预计离开时间必须晚于当前时间!");
            }
            if ("0x0531100f".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(-1, "访问结束时间少于现在!");
            }
            if ("0x0531f00e".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(-1, "接访人不存在!");
            }
            if ("0x0531f025".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(-1, "人像识别失败!");
            }
            if ("0x0240100a".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(-1, "访客参数异常!");
            }
            if ("0x0531401d".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(-1, "访客{" + visitorName + "}存在其他有效预约或登记!");
            }
            if ("0x02401033".equals(dto.getCode())) {
                return new CreateVisitorAndDoorVo(-1, "海康平台连接失败!");
            }
        } catch (Exception e) {
            return new CreateVisitorAndDoorVo(-1, "访客{" + visitorName + "}存在其他有效预约或登记!");
        }
        log.error("访客预约失败 ---> {}", dto);
        return new CreateVisitorAndDoorVo(-1, dto.getMsg());
    }

    /**
     * 获取权限组
     *
     * @return
     * @throws Exception
     */
    private List<String> getPrivilegeGroup() throws Exception {
        HashMap<String, Object> json = new HashMap<>();
        json.put("pageNo", 1);
        json.put("pageSize", 1000);
        String hikResponse = apiProxy.post(HikApi.HAIKANG_PRIVILEGE_GROUP_API).setBody(json).execute().toResponseString();
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode())) {
            List<String> list = new ArrayList<>(dto.getData().getList().size());
            for (HikGuardInfo hikGuardInfo : dto.getData().getList()) {
                list.add(hikGuardInfo.getPrivilegeGroupId());
            }
            return list;
        } else {
            throw new Exception("获取权限组失败 错误码" + dto.getCode());
        }
    }

}
