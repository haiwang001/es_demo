package com.bjxczy.onepark.visitor.pojo.HikInOut;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/3/31 16:24
 *
 * 海康设备信息
 */
@Data
public class HikGuardInfo {
    private String name;// 设备名称
    private String resourceType;// 设备类型
    private String indexCode;// 设备编号
    private String parentIndexCode;// 父设备编号 查询门禁点 door 需要

    private String manufacturer;// 制造商 品牌
    private String devTypeCode;// 设备类型编码
    private String ip;//
    private Integer online;// 是否在线状态
    private String channelNo;// 设备通道号

    private String privilegeGroupId;// 权限组ID, 用于访客登记时对访客授权
    private String appointRecordId;// 权限ID, 用于访客取消预约

    private String regionIndexCode;// 区域索引码
    private String regionName;// 区域节点名称
    private Integer disOrder;// 说订单
    private Integer netZoneId;// net安全区域Id
    private Integer faceCapacity;// 人脸识别上限
    private String comId;// 未知
    private String regionPath;// 未知
    private String dataVersion;// 数据版本
    private String doorCapacity;// 门的能力
    private String isCascade;// 是级联
    private String updateTime;// 修改时间
    private String sort;// 排序
    private String userName;// 创建人
    private String treatyType;// 协议类型
    private String cardCapacity;// 卡识别上限
    private String fingerCapacity;// 指纹识别上限
    private Integer port;// 端口
    private String createTime;// 创建时间
    private String acsReaderVerifyModeAbility;// 阅读器验证模式能力
    // 访客记录字段
    private String orderId;// orderId	string	False	访客登记时的登记id，可以用来做访客签离。此id如访客登记返回的id一致
}
