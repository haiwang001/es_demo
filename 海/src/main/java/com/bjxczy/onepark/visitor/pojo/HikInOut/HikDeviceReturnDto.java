package com.bjxczy.onepark.visitor.pojo.HikInOut;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/3/31 15:43 HikGuardInfo
 */
@Data
public class HikDeviceReturnDto {
    private String msg;
    private String code;
    private HikDeviceData data;
}