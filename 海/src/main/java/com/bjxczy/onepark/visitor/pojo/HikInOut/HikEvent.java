package com.bjxczy.onepark.visitor.pojo.HikInOut;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/6 10:01
 */
@Data
public class HikEvent {
    private String srcParentIndex;  // 设备编码
    private ExtEventData data;
}
