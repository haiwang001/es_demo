package com.bjxczy.onepark.visitor.pojo.HikInOut;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/10 14:07
 */
@Data
public class IpAndNameAndTypeDto {
    private String ip;
    private String name;
    private String type;

    public IpAndNameAndTypeDto(String ip, String name, String type) {
        this.ip = ip;
        this.name = name;
        this.type = type;
    }
}
