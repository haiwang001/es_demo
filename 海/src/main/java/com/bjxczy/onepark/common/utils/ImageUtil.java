package com.bjxczy.onepark.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import cn.hutool.http.HttpUtil;
import lombok.SneakyThrows;

import javax.xml.bind.DatatypeConverter;

public class ImageUtil {
	
	@SneakyThrows
	public static File base64ToFile(String prefix, String suffix, String base64Str) {
		File file = File.createTempFile(prefix, suffix);
		try (FileOutputStream fos = new FileOutputStream(file)) {
            byte[] decoder= DatatypeConverter.parseBase64Binary(base64Str);
            fos.write(decoder);
		}
		return file;
	}
	
	@SneakyThrows
	public static File imageUrlToFile(String prefix, String suffix, String imageUrl) {
		File destFile = File.createTempFile(prefix, suffix);
		HttpUtil.downloadFile(imageUrl, destFile);
        return destFile;
	}
	
	public static MultipartFile fileToMultipartFile(File file) {
        FileItem fileItem = createFileItem(file);
        MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
        return multipartFile;
    }

    private static FileItem createFileItem(File file) {
        FileItemFactory factory = new DiskFileItemFactory(16, null);
        FileItem item = factory.createItem("textField", "text/plain", true, file.getName());
        int bytesRead = 0;
        byte[] buffer = new byte[8192];
        try {
            FileInputStream fis = new FileInputStream(file);
            OutputStream os = item.getOutputStream();
            while ((bytesRead = fis.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return item;
    }

}
