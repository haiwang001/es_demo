package com.bjxczy.onepark.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.config.HikConfiguration;
import com.hikvision.artemis.sdk.Client;
import com.hikvision.artemis.sdk.Request;
import com.hikvision.artemis.sdk.Response;
import com.hikvision.artemis.sdk.enums.Method;
import com.hikvision.artemis.sdk.util.MessageDigestUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jisen
 * @date 2020/2/5 17:43
 */
@Component
@Slf4j
public class HikArtemisHttpUtils {

    @Autowired(required = false)
    HikConfiguration hikConfiguration;

    private static final Logger logger = LoggerFactory.getLogger(HikArtemisHttpUtils.class);
    private static final List<String> CUSTOM_HEADERS_TO_SIGN_PREFIX = new ArrayList();
    private static final String SUCC_PRE = "2";

    public HikArtemisHttpUtils() {
    }

    public Response doPostResponse(Map<String, String> path, String body, Map<String, String> querys, String accept, String contentType, int timeout) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            JSONObject responseStr = null;
            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }

                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                } else {
                    headers.put("Content-Type", "application/text;charset=UTF-8");
                }

                CUSTOM_HEADERS_TO_SIGN_PREFIX.clear();
                Request request = new Request(Method.POST_STRING, httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setQuerys(querys);
                request.setStringBody(body);
                Response response = Client.execute(request);
                return response;
            } catch (Exception e) {
                logger.error("the Artemis PostString Request is failed[doPostStringArtemis]" + e.getMessage());
                throw new RuntimeException("海康post接口调用错误！");
            }
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }

    public String doGetArtemis(Map<String, String> path, Map<String, String> querys, String accept, String contentType, int timeout) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            String responseStr = null;

            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }

                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                } else {
                    headers.put("Content-Type", "application/text;charset=UTF-8");
                }

                CUSTOM_HEADERS_TO_SIGN_PREFIX.clear();
                Request request = new Request(Method.GET, httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setQuerys(querys);
                Response response = Client.execute(request);
                responseStr = getResponseResult(response);
            } catch (Exception var9) {
                logger.error("the Artemis GET Request is failed[doGetArtemis]", var9);
            }

            return responseStr;
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }

    public HttpResponse doGetResponse(Map<String, String> path, Map<String, String> querys, String accept, String contentType, int timeout) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            HttpResponse httpResponse = null;

            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }

                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                } else {
                    headers.put("Content-Type", "application/text;charset=UTF-8");
                }

                CUSTOM_HEADERS_TO_SIGN_PREFIX.clear();
                Request request = new Request(Method.GET_RESPONSE, httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setQuerys(querys);
                Response response = Client.execute(request);
                httpResponse = response.getResponse();
            } catch (Exception var9) {
                logger.error("the Artemis GET Request is failed[doGetArtemis]", var9);
            }

            return httpResponse;
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }

    public String doPostFormArtemis(Map<String, String> path, Map<String, String> paramMap, Map<String, String> querys, String accept, String contentType, int timeout) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            String responseStr = null;

            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }

                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                } else {
                    headers.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                }

                CUSTOM_HEADERS_TO_SIGN_PREFIX.clear();
                Request request = new Request(Method.POST_FORM, httpSchema + httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setQuerys(querys);
                request.setBodys(paramMap);
                Response response = Client.execute(request);
                responseStr = getResponseResult(response);
            } catch (Exception var10) {
                logger.error("the Artemis PostForm Request is failed[doPostFormArtemis]", var10);
            }

            return responseStr;
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }

    public JSONObject doPostStringArtemisByTagId(Map<String, String> path, String body, Map<String, String> querys, String accept, String contentType, int timeout,String tagId) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            JSONObject responseStr = null;
            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }
                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                } else {
                    headers.put("Content-Type", "application/text;charset=UTF-8");
                }
                headers.put("tagId", tagId);
                CUSTOM_HEADERS_TO_SIGN_PREFIX.clear();
                Request request = new Request(Method.POST_STRING, httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setQuerys(querys);
                request.setStringBody(body);
                Response response = Client.execute(request);
                responseStr = JSONObject.parseObject(getResponseResult(response));
            } catch (Exception e) {
                logger.error("the Artemis PostString Request is failed[doPostStringArtemis]" + e.getMessage());
                throw new RuntimeException("海康post接口调用错误！");
            }
            return responseStr;
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }


    public JSONObject doPostStringArtemis(Map<String, String> path, String body, Map<String, String> querys, String accept, String contentType, int timeout) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            JSONObject responseStr = null;
            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }
                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                } else {
                    headers.put("Content-Type", "application/text;charset=UTF-8");
                }
                CUSTOM_HEADERS_TO_SIGN_PREFIX.clear();
                Request request = new Request(Method.POST_STRING, httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setQuerys(querys);
                request.setStringBody(body);
                Response response = Client.execute(request);
                responseStr = JSONObject.parseObject(getResponseResult(response));
            } catch (Exception e) {
                logger.error("the Artemis PostString Request is failed[doPostStringArtemis]" + e.getMessage());
                throw new RuntimeException("海康post接口调用错误！");
            }
            return responseStr;
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }



    public String doPostBytesArtemis(Map<String, String> path, byte[] bytesBody, Map<String, String> querys, String accept, String contentType, int timeout) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            String responseStr = null;

            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }

                if (bytesBody != null) {
                    headers.put("Content-MD5", MessageDigestUtil.base64AndMD5(bytesBody));
                }

                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                } else {
                    headers.put("Content-Type", "application/text;charset=UTF-8");
                }

                CUSTOM_HEADERS_TO_SIGN_PREFIX.clear();
                Request request = new Request(Method.POST_BYTES, httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setQuerys(querys);
                request.setBytesBody(bytesBody);
                Response response = Client.execute(request);
                responseStr = getResponseResult(response);
            } catch (Exception var10) {
                logger.error("the Artemis PostBytes Request is failed[doPostBytesArtemis]", var10);
            }

            return responseStr;
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }

    public String doPutStringArtemis(Map<String, String> path, String body, String accept, String contentType, int timeout) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            String responseStr = null;

            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }

                if (StringUtils.isNotBlank(body)) {
                    headers.put("Content-MD5", MessageDigestUtil.base64AndMD5(body));
                }

                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                } else {
                    headers.put("Content-Type", "application/text;charset=UTF-8");
                }

                CUSTOM_HEADERS_TO_SIGN_PREFIX.clear();
                Request request = new Request(Method.PUT_STRING, httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setStringBody(body);
                Response response = Client.execute(request);
                responseStr = getResponseResult(response);
            } catch (Exception var9) {
                logger.error("the Artemis PutString Request is failed[doPutStringArtemis]", var9);
            }

            return responseStr;
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }

    public String doPutBytesArtemis(Map<String, String> path, byte[] bytesBody, String accept, String contentType, int timeout) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            String responseStr = null;

            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }

                if (bytesBody != null) {
                    headers.put("Content-MD5", MessageDigestUtil.base64AndMD5(bytesBody));
                }

                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                } else {
                    headers.put("Content-Type", "application/text;charset=UTF-8");
                }

                CUSTOM_HEADERS_TO_SIGN_PREFIX.clear();
                Request request = new Request(Method.PUT_BYTES, httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setBytesBody(bytesBody);
                Response response = Client.execute(request);
                responseStr = getResponseResult(response);
            } catch (Exception var9) {
                logger.error("the Artemis PutBytes Request is failed[doPutBytesArtemis]", var9);
            }

            return responseStr;
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }

    public String doDeleteArtemis(Map<String, String> path, Map<String, String> querys, String accept, String contentType, int timeout) {
        String httpSchema = (String) path.keySet().toArray()[0];
        if (httpSchema != null && !StringUtils.isEmpty(httpSchema)) {
            String responseStr = null;

            try {
                Map<String, String> headers = new HashMap();
                if (StringUtils.isNotBlank(accept)) {
                    headers.put("Accept", accept);
                } else {
                    headers.put("Accept", "*/*");
                }

                if (StringUtils.isNotBlank(contentType)) {
                    headers.put("Content-Type", contentType);
                }

                Request request = new Request(Method.DELETE, httpSchema + hikConfiguration.getHost(), (String) path.get(httpSchema), hikConfiguration.getAppKey(), hikConfiguration.getAppSecret(), timeout);
                request.setHeaders(headers);
                request.setSignHeaderPrefixList(CUSTOM_HEADERS_TO_SIGN_PREFIX);
                request.setQuerys(querys);
                Response response = Client.execute(request);
                responseStr = getResponseResult(response);
            } catch (Exception var9) {
                logger.error("the Artemis DELETE Request is failed[doDeleteArtemis]", var9);
            }

            return responseStr;
        } else {
            throw new RuntimeException("http和https参数错误httpSchema: " + httpSchema);
        }
    }

    public String getResponseResult(Response response) {
        log.info("[海康：response ]：{}", JSONObject.toJSONString(response));
        String responseStr = null;
        int statusCode = response.getStatusCode();
        if (String.valueOf(statusCode).startsWith("2")) {
            responseStr = response.getBody();
            logger.info("the Artemis Request is Success,statusCode:" + statusCode + " SuccessMsg:" + response.getBody());
        } else {
            String msg = response.getErrorMessage();
            responseStr = response.getBody();
            logger.error("the Artemis Request is Failed,statusCode:" + statusCode + " errorMsg:" + msg);
        }

        return responseStr;
    }
}
