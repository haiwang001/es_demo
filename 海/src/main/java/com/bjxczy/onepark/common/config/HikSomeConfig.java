package com.bjxczy.onepark.common.config;


import com.bjxczy.core.rabbitmq.rpc.AsyncReplyHandler;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
  异步mq 消息恢复处理
 *@Author wlw
 *@Date 2023/7/5 11:10
 */
@Configuration
public class HikSomeConfig {

    @Bean
    public MessageConverter messageConverter() {
        return new SimpleMessageConverter();
    }

    @Bean
    public AsyncReplyHandler replyAsyncHandler() {
        return new AsyncReplyHandler(messageConverter());
    }
}
