package com.bjxczy.onepark.common.cons;


import java.util.HashMap;
import java.util.Map;

/*
 *@Author wlw
 *@Date 2023/6/21 13:52
 */
public class HikApi {
    public static final String ARTEMIS = "/artemis";
    // 获取海康的设备列表
    public static final String HAIKANG_DEVICE_LIST_API = ARTEMIS + "/api/irds/v2/deviceResource/resources";
    // 海康设备添加人脸
    public static final String HAIKANG_DEVICE_FACE_ADD_API = ARTEMIS + "/api/resource/v1/face/single/add";
    // 获取海康的设备状态的信息列表
    public static final String HAIKANG_DEVICE_ONLINE_API = ARTEMIS + "/api/nms/v1/online/acs_device/get";
    // 出入权限配置添加
    public static final String HAIKANG_DEVICE_AUTH_CONFIG_ADD_API = ARTEMIS + "/api/acps/v1/auth_config/add";
    // 出入权限配置删除
    public static final String HAIKANG_DEVICE_AUTH_CONFIG_DEL_API = ARTEMIS + "/api/acps/v1/auth_config/delete";
    // 根据出入权限配置快捷下载
    public static final String HAIKANG_DEVICE_CONFIGURATION_SHORTCUT_API = ARTEMIS + "/api/acps/v1/authDownload/configuration/shortcut";
    //  海康设备下发人脸下载任务创建
    public static final String HAIKANG_DEVICE_PORTRAIT_RELEASE_API = ARTEMIS + "/api/acps/v1/authDownload/data/addition";
    //  该接口用于开始一次下载任务，只能由创建任务的组件触发。权限下载完成后会自动结束下载任务。
    public static final String HAIKANG_DEVICE_TASK_START_API = ARTEMIS + "/api/acps/v1/authDownload/task/start";
    // 下载任务查询
    public static final String HAIKANG_DEVICE_TASK_PROGRESS_API = ARTEMIS + "/api/acps/v1/authDownload/task/progress";
    // 访客来访记录 v2
    public static final String HAIKANG_VISITING_RECORDS_API = ARTEMIS + "/api/visitor/v2/visiting/records";
    // 访客出入事件  使用此事件来查询出入记录
    public static final String HAIKANG_TURNOVER_SEARCH_API = ARTEMIS + "/api/visitor/v1/event/turnover/search";
    // 新增海康设备人脸
    public static final Integer HAIKANG_DEVICE_PORTRAIT_RELEASE_API_TYPE_ADD = 0;
    // 删除海康设备人脸
    public static final Integer HAIKANG_DEVICE_PORTRAIT_RELEASE_API_TYPE_DEL = 2;
    //  海康事件订阅
    public static final String HAIKANG_EVENTSERVICE_API = ARTEMIS + "/api/eventService/v1/eventSubscriptionByEventTypes";
    //  海康人脸识别成功事件
    public static final Integer HAIKANG_IDENTIFICATION_SUCCESS = 196893;
    public static final String HIK_CALLBACK_CONTROLLER = "/HikCallBack";
    public static final String HIK_CALLBACK_CONTROLLER_FACE_DISCERN_SUCCEED = "/faceDiscernSucceed";
    //  人脸评分
    public static final String HAIKANG_FACE_PICTURE_CHECK_API = ARTEMIS + "/api/frs/v1/face/picture/check";
    //  预约免登记
    public static final String HAIKANG_FACE_APPOINTMENT_REGISTRATION_API = ARTEMIS + "/api/visitor/v1/appointment/registration";
    // 访客签离
    public static final String HAIKANG_VISITOR_OUT_API = ARTEMIS + "/api/visitor/v1/visitor/out";
    // 查询访客预约记录v2
    public static final String HAIKANG_VISITOR_APPOINTMENT_RECORDS_API = ARTEMIS + "/api/visitor/v2/appointment/records";
    // 取消预约访客
    public static final String HAIKANG_VISITOR_APPOINTMENT_CANCEL_API = ARTEMIS + "/api/visitor/v1/appointment/cancel";
    //  查询访客权限组
    public static final String HAIKANG_PRIVILEGE_GROUP_API = ARTEMIS + "/api/visitor/v1/privilege/group";
    public static final Integer HIK_EQUIPMENT_EQUIPMENT_CONTROLLER = 1; // 控制器下载类型刷卡机
    public static final Integer HIK_EQUIPMENT_EQUIPMENT_FACE_MACHINE = 5; // 控制器下载类型刷卡机

    public static Map<String, String> getpath(String url) {
        Map<String, String> path = new HashMap<String, String>(2) {{
            put("https://", url);
        }};
        return path;
    }
    /**
     * 根据资源类型分页获取资源列表，主要用于资源信息的全量同步。
     * 接口适配产品版本
     * 综合安防管理平台iSecure Center V1.3及以上版本
     * 接口地址
     * /api/irds/v2/deviceResource/resources
     * 参数名称	数据类型	是否必须	参数描述
     * pageNo	number	True	当前页码
     * pageSize	number	True	分页大小
     * resourceType	string	True	资源类型,
     * 门禁控制器	acsDevice
     * 门禁点	    door
     * 门禁读卡器	reader
     */
    public static final String HAIKANG_DEVICE_RESOURCE_DOOR_API = "/artemis/api/resource/v2/door/search";

    /**
     * 创建下载任务，以异步任务方式下载出入控制权限。适用于综合大楼、学校、医院等批量权限下载的场景。
     * 创建下载任务，使得业务组件与出入控制权限服务建立一次异步下载的通道。
     * 通过向下载任务中添加数据接口添加待下载的数据，包含资源、人员信息；可分多次调用该接口批量添加下载数据。
     * 任务的操作权限由创建的业务组件控制，包含开始下载任务，终止下载任务，删除下载任务。
     * 对已经开始的下载任务，可通过查询下载任务进度接口查询任务的总体下载进度和每个资源的下载进度信息。
     * 一个下载任务最大支持100个设备的卡权限下载或者100个通道的人脸。
     * 新创建的下载任务有效期7天，在7天内未操作开始下载的任务将自动清理。
     * 接口地址
     * <p>
     * /api/acps/v1/authDownload/task/addition
     * 请求参数
     * <p>
     * 参数名称	数据类型	是否必须	参数描述
     * taskType	number	True	下载任务类型
     * 1：卡片
     * 4：人脸
     * 请求参数举例
     * <p>
     * json
     * {
     * "taskType": 1
     * }
     */
    public static final String HAIKANG_DEVICE_CREATE_AUTHDOWNLOAD_API = "/artemis/api/acps/v1/authDownload/task/addition";
}
