package com.bjxczy.onepark.common.utils;


import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class CacheUtils {

    /**
     * 缓存最大个数
     */
    private static final Integer CACHE_MAX_NUMBER = 10000;
    /**
     * 当前缓存个数
     */
    private static Integer CURRENT_SIZE = 0;

    /**
     * 缓存对象
     */
    private static final ConcurrentMap<String, Object> cacheMap = new ConcurrentHashMap<>();
    /**
     * 这个记录了缓存使用的最后一次的记录，最近使用的在最前面
     */
    private static final List<String> cacheUseList = new LinkedList<>();

    /**
     * 指定缓存失效时间
     */
    public static void setCache(String cacheKey, Object cacheValue) {
        checkSize();
        saveCacheUseLog(cacheKey);
        CURRENT_SIZE = CURRENT_SIZE + 1;
        cacheMap.put(cacheKey, cacheValue);

    }

    /**
     * 获取缓存
     */
    public static Object getCache(String cacheKey) {
        if (checkCache(cacheKey)) {
            saveCacheUseLog(cacheKey);
            return cacheMap.get(cacheKey);
        }
        return null;
    }

    /**
     * 判断key是否存在
     */
    public static boolean isExist(String cacheKey) {
        return checkCache(cacheKey);
    }


    /**
     * 删除所有缓存
     */
    public static void clear() {
        cacheMap.clear();
        CURRENT_SIZE = 0;
    }

    /**
     * 删除某个缓存
     */
    public static void deleteCache(String cacheKey) {
        Object remove = cacheMap.remove(cacheKey);
        if (remove != null) {
            CURRENT_SIZE = CURRENT_SIZE - 1;
        }
    }


    /**
     * 判断缓存在不在,过没过期
     */
    private static boolean checkCache(String cacheKey) {
        Object cacheObj = cacheMap.get(cacheKey);
        if (cacheObj == null) {
            return false;
        }
        return true;
    }

    /**
     * 检查大小
     * 当当前大小如果已经达到最大大小
     * 首先删除过期缓存，如果过期缓存删除过后还是达到最大缓存数目
     * 删除最久未使用缓存
     */
    private static void checkSize() {
        if (CURRENT_SIZE >= CACHE_MAX_NUMBER) {
            deleteLRU();
        }
    }

    /**
     * 删除最近最久未使用的缓存
     */
    private static void deleteLRU() {
        String cacheKey = cacheUseList.remove(cacheUseList.size() - 1);
        deleteCache(cacheKey);
    }

    /**
     * 保存缓存的使用记录,只能一个线程获得，其他需等待
     * 使用时间更新 先进后出
     */
    private static synchronized void saveCacheUseLog(String cacheKey) {
        cacheUseList.remove(cacheKey);
        cacheUseList.add(0, cacheKey);
    }
}
