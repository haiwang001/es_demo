package com.bjxczy.onepark.common.utils;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

/**
 * @author jisen
 * @date 2020/3/11 16:04
 */
public class ListPageUtil {


    public static Object buildPage(List data,int currentPage,int pageSize){
        JSONObject jsonObject = new JSONObject();
        if (CollectionUtils.isEmpty(data)){
            jsonObject.put("total",0);
            jsonObject.put("pageNo",currentPage);
            jsonObject.put("pageSize",pageSize);
            jsonObject.put("list",Collections.emptyList());
            return jsonObject;
        }

        jsonObject.put("total",data.size());
        jsonObject.put("pageNo",currentPage);
        jsonObject.put("pageSize",pageSize);

        int fromIndex = (currentPage - 1) * pageSize;
        if (fromIndex >= data.size()) {
            jsonObject.put("list",Collections.emptyList());
            return jsonObject;//空数组
        }
        if(fromIndex<0){
            jsonObject.put("list",Collections.emptyList());
            return jsonObject;//空数组
        }

        int toIndex = currentPage * pageSize;
        if (toIndex >= data.size()) {
            toIndex = data.size();
        }

        jsonObject.put("list",data.subList(fromIndex, toIndex));
        return jsonObject;
    }

    public static Object buildPage(List data,int currentPage,int pageSize,int total){
        JSONObject jsonObject = new JSONObject();
        if (CollectionUtils.isEmpty(data)){
            jsonObject.put("total",0);
            jsonObject.put("pageNo",currentPage);
            jsonObject.put("pageSize",pageSize);
            jsonObject.put("list",Collections.emptyList());
            return jsonObject;
        }

        jsonObject.put("total",total);
        jsonObject.put("pageNo",currentPage);
        jsonObject.put("pageSize",pageSize);
        jsonObject.put("list",data);
        return jsonObject;
    }

    public static Object buildAllNumPage(List data){
        JSONObject jsonObject = new JSONObject();
        if (CollectionUtils.isEmpty(data)){
            jsonObject.put("total",0);
            jsonObject.put("pageNo",0);
            jsonObject.put("pageSize",0);
            jsonObject.put("list",Collections.emptyList());
            return jsonObject;
        }
        jsonObject.put("total",data.size());
        jsonObject.put("pageNo",1);
        jsonObject.put("pageSize",data.size());
        jsonObject.put("list",data);
        return jsonObject;
    }

}
