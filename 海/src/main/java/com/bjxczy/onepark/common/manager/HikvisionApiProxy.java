package com.bjxczy.onepark.common.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.config.HikConfiguration;
import com.hikvision.artemis.sdk.Client;
import com.hikvision.artemis.sdk.Request;
import com.hikvision.artemis.sdk.Response;
import com.hikvision.artemis.sdk.enums.Method;

import cn.hutool.core.map.MapBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

@Log4j2
@Component
public class HikvisionApiProxy {

    @Autowired
    private HikConfiguration config;

    private final static Integer timeout = 10 * 1000;

    public HikvisionRequestBuilder get(String path) {
        return new HikvisionRequestBuilder(Method.GET_RESPONSE, path);
    }

    public HikvisionRequestBuilder post(String path) {
        return new HikvisionRequestBuilder(Method.POST_STRING, path);
    }

    public class HikvisionRequestBuilder {

        @Getter
        private Request request;

        @Getter
        private Response response;

        @Setter
        @Getter
        private Map<String, String> headers;

        @Setter
        @Getter
        private Map<String, String> querys;

        @Setter
        @Getter
        private Map<String, Object> bodys;

        @Setter
        @Getter
        private List<String> listBody;

        public HikvisionRequestBuilder(Method method, String path) {
            this.request = new Request(method, "https://" + config.getHost(), path, config.getAppKey(), config.getAppSecret(), timeout);
            this.headers = MapBuilder.create(new HashMap<String, String>())
                    .put("Accept", "*/*")
                    .put("Content-Type", "application/json")
                    .build();
            this.querys = new HashMap<>();
            this.bodys = new HashMap<>();
        }

        public HikvisionRequestBuilder setHeader(String key, String value) {
            this.headers.put(key, value);
            return this;
        }

        public HikvisionRequestBuilder setQuery(String key, String value) {
            this.querys.put(key, value);
            return this;
        }

        public HikvisionRequestBuilder setBody(String key, Object value) {
            this.bodys.put(key, value);
            return this;
        }

        public HikvisionRequestBuilder setBody(Map<String, Object> body) {
            this.bodys = body;
            return this;
        }

        public HikvisionRequestBuilder setBody(List<String> body) {
            this.listBody = body;
            return this;
        }

        @SneakyThrows
        public HikvisionRequestBuilder execute() {
            this.request.setHeaders(headers);
            this.request.setQuerys(querys);
            this.request.setStringBody(JSON.toJSONString(listBody!=null?listBody:bodys));
            this.response = Client.execute(this.request);
            log.info("[body] {}",this.request.getStringBody());
            return this;
        }

        public String toResponseString() {
            if (this.response == null) {
                throw new NullPointerException("未执行execute()");
            }
            String str = null;
            int statusCode = response.getStatusCode();
            if (statusCode == 200) {
                str = this.response.getBody();
            } else {
                throw new RuntimeException("请求异常：statusCode=" + this.response.getStatusCode() + ", message=" + this.response.getErrorMessage());
            }
            return str;
        }

        public JSONObject toJSONObject() {
            String str = toResponseString();
            return JSONObject.parseObject(str);
        }

        public HikvisionBody toHikvisionBody() {
            return toJSONObject().toJavaObject(HikvisionBody.class);
        }

    }

}
