package com.bjxczy.onepark.common.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@Component
@ConfigurationProperties(prefix = "hikvideo-stream")
@Data
public class HikStreamVideoConfig {

    private Integer streamType;
    private String protocol;
    private Integer transmode;
    private String expand;

}
