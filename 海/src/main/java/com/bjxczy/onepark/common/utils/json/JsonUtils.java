package com.bjxczy.onepark.common.utils.json;

import com.google.gson.Gson;

import java.util.ArrayList;


public class JsonUtils {
    public static String toJsonString(Object o) {
        return new Gson().toJson(o);
    }
    public static ArrayList toBean(String json) {
        return new Gson().fromJson(json, ArrayList.class);
    }
    public static ArrayList<Integer> toBeanArrInt(String json) {
        return new Gson().fromJson(json, ArrayList.class);
    }
}
