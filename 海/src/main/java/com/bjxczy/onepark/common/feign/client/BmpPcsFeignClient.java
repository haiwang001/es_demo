package com.bjxczy.onepark.common.feign.client;

import org.springframework.cloud.openfeign.FeignClient;

import com.bjxczy.core.feign.client.pcs.FileFeignClient;

@FeignClient("pcs")
public interface BmpPcsFeignClient extends FileFeignClient {

}
