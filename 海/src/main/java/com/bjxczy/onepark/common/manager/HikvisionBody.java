package com.bjxczy.onepark.common.manager;

import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.onepark.common.model.common.DictInformation;
import com.bjxczy.onepark.common.resp.Payload;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.common.resp.ResultEnum;

import lombok.Data;

@Data
public class HikvisionBody {
	
	private String code;
	
	private String msg;
	
	private JSONObject data;
	
	public boolean isSuccess() {
		return Objects.equals(code, "0");
	}
	
	public R<Payload> toRPayload() {
		return new R<>(this.isSuccess() ? 0 : 1, this.msg, null, data.toJavaObject(Payload.class));
	}
	
	public R<Payload> toRPayload(DictService service, String groupCode) {
		R<Payload> result = new R<Payload>(ResultEnum.FAIL, null, null);
		if (isSuccess()) {
			result = R.success(data.toJavaObject(Payload.class));
		} else {
			Map<String, DictInformation> valueKeyMap = service.valueKeyMap(groupCode);
			if (valueKeyMap.containsKey(code)) {
				result.setMessage(valueKeyMap.get(code).getLabel());
			} else if (StringUtils.isNotBlank(msg )) {
				result.setMessage(msg);
			}
		}
		return result;
	}

}
