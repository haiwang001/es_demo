package com.bjxczy.onepark.common.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "hikvision")
public class HikConfiguration {
    private String host;
    private String appKey;
    private String appSecret;
    private String tagId;
}
