package com.bjxczy.onepark.common.manager;

import java.io.File;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.bjxczy.onepark.common.feign.client.BmpPcsFeignClient;
import com.bjxczy.onepark.common.utils.ImageUtil;
import com.hikvision.artemis.sdk.Response;

import cn.hutool.core.lang.UUID;

@Component
public class HikvisionApiService {

	@Autowired
	private HikvisionApiProxy proxy;
	@Autowired
	private BmpPcsFeignClient fileClient;

	private static final String BASE_URL = "/artemis/api";

	/**
	 * 获取海康图片Base64编码图片
	 * @param url ISC平台图片地址
	 * @return
	 */
	public String getHikvisionBase64Picture(String url) {
		Response response = proxy.post(BASE_URL + "/frs/v1/application/picture").setBody("url", url).execute().getResponse();
		int statusCode = response.getStatusCode();
		if (statusCode == 302) {
			url = response.getHeader("Location");
			return url;
		}
		return response.getBody();
	}
	
	public String getFileUrlByHikvision(String url) {
		return getFileUrlByHikvision(url, ".jpg");
	}
	
	/**
	 * 自动上传图片至fastdfs并返回URL
	 * @param url ISC平台图片地址
	 * @return
	 */
	public String getFileUrlByHikvision(String url, String suffix) {
		String resp = getHikvisionBase64Picture(url);
		File file = null;
		if (StringUtils.startsWith(resp, "http")) {
			file = ImageUtil.imageUrlToFile(UUID.fastUUID().toString(), suffix, resp);
		} else {
			file = ImageUtil.base64ToFile(UUID.fastUUID().toString(), suffix, resp);
		}
		MultipartFile multipartFile = ImageUtil.fileToMultipartFile(file);
		String fastdfsUrl = fileClient.fileUpload(multipartFile);
		// 删除临时文件
		file.delete();
		return fastdfsUrl;
	}

}
