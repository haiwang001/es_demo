package com.bjxczy.onepark.common.handle;


import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.utils.HikArtemisHttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Component
@Slf4j
public class HikSendHandle {
    private static final String ARTEMIS = "/artemis";
    private static final Integer TIMEOUT=3000;
    @Autowired
    HikArtemisHttpUtils hikArtemisHttpUtils;

    /**
     *
     * @param uri qpi路径
     * @param json 参数
     * @return 海康resp
     */
    public JSONObject post(String uri,String json){
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", ARTEMIS + uri);
            }
        };
        JSONObject hikResponse = hikArtemisHttpUtils.doPostStringArtemis(path, json, null, null, "application/json", TIMEOUT);
        if (!Objects.isNull(hikResponse)){
            if ("0".equals(hikResponse.get("code"))){
                return hikResponse;
            }
        }
        log.info("[ 调用海康服务失败 ] -> {}",hikResponse);
        throw new ResultException("调用海康服务异常!");
    }


    public JSONObject post2(String uri,String json){
        Map<String, String> path = new HashMap<String, String>(2) {
            {
                put("https://", ARTEMIS + uri);
            }
        };
        JSONObject hikResponse = hikArtemisHttpUtils.doPostStringArtemis(path, json, null, null, "application/json", TIMEOUT);
       return  hikResponse;
    }




    /**
     *
     * @param src
     * @return 获取图片base64
     * @throws Exception
     */
    public static String getImageBase(String src) throws Exception {
        URL url = new URL(src);
        InputStream in = null;
        byte[] bytes = null;
        try {
            in = url.openConnection().getInputStream();//远程文件
            bytes = IOUtils.toByteArray(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(bytes);
    }
}
