package com.bjxczy.onepark.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.dict.JdbcDictServiceImpl;

@Configuration
public class DictConfig {
	
	@Bean
	public DictService dictService() {
		return new JdbcDictServiceImpl("hik_dict");
	}

}
