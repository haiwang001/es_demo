package com.bjxczy.onepark.common.utils.hik;
import org.apache.commons.codec.binary.Base64;
import java.io.*;
import java.net.URL;

public class StaffUtils {
    /**
     * 文件File类型转byte[]
     *
     * @param file
     * @return
     */
    private static byte[] fileToByte(File file) {
        byte[] fileBytes = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            fileBytes = new byte[(int) file.length()];
            fis.read(fileBytes);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileBytes;
    }


    /**
     * @param url 远端文件Url
     * @return File
     */
    public static String getFileBase64String(String url) {
        //对本地文件命名
        String fileName = url.substring(url.lastIndexOf("."), url.length());
        File file = null;
        URL urlfile;
        try {
            // 创建一个临时路径
            file = File.createTempFile("file", fileName);
            //下载
            urlfile = new URL(url);
            try (InputStream inStream = urlfile.openStream();
                 OutputStream os = new FileOutputStream(file);) {
                int bytesRead = 0;
                byte[] buffer = new byte[8192];
                while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
            }
        } catch (Exception ignored) {
        }
        return Base64.encodeBase64String(fileToByte(file));
    }
}
