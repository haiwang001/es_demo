package com.bjxczy.onepark.common.utils.hik;


import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.config.HikConfiguration;
import com.bjxczy.onepark.common.cons.HikApi;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.utils.json.JsonUtils;
import com.bjxczy.onepark.common.utils.spring.SpringContextUtil;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikDeviceReturnDto;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikGuardInfo;
import com.bjxczy.onepark.visitor.pojo.HikInOut.RowsDto;
import com.bjxczy.onepark.visitor.pojo.HikInOut.postSendPersonnelTemplateDto;
import com.bjxczy.onepark.visitor.pojo.vo.FaceAppointmentRegistrationVo;
import lombok.extern.log4j.Log4j2;
import com.alibaba.fastjson.JSON;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

@Log4j2
public class HikSendUtils {
    private static final HikConfiguration hikConfiguration;

    static {

        hikConfiguration = SpringContextUtil.getBean(HikConfiguration.class);
    }


    /*public static String HIkEventService(String url) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("eventTypes", new int[]{HikApi.HAIKANG_IDENTIFICATION_SUCCESS});
        map.put("eventDest", url);
        log.info("海康人脸通行事件订阅  ---> {}", map);
        JSONObject hikResponse = HikHttpUtils.postJSON(HikApi.HAIKANG_EVENTSERVICE_API, JsonUtils.toJsonString(map));
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("OK")) {
            log.info("海康事件订阅成功 ---> {}", dto.getMsg());
            return dto.getMsg();
        } else {
            log.error("海康事件订阅失败  ---> {}", dto);
            throw new Exception("海康事件订阅失败 " + dto);
        }
    }


    *//**
     * 海康设备同步人脸
     *
     * @param json
     * @return
     * @throws Exception
     *//*

    public static boolean faceAdd(HashMap<String, String> json) throws Exception {
        log.info("海康设备添加人脸信息  ---> {}", json);
        JSONObject hikResponse = HikHttpUtils.postJSON(HikApi.HAIKANG_DEVICE_FACE_ADD_API, JsonUtils.toJsonString(json));
        assert hikResponse != null;
        HikDeviceReturnDto hikDeviceReturnDto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
        if (hikDeviceReturnDto.getCode().equals("0")) {
            log.info("海康设备 给人添加人脸成功  ");
            return true;
        }
        if (hikDeviceReturnDto.getCode().equals("0x00052301")) {
            log.info("海康设备 人脸已存在");
            return true;
        }
        if (hikDeviceReturnDto.getCode().equals("0x00072002")) {
            log.info("海康设备 人脸图大小超过200KB");
            return false;
        }
        log.info("海康设备 给人添加人脸返回  " + hikDeviceReturnDto);
        return false;
    }

    *//**
     * @param url  qpi路径
     * @param json 参数
     * @return 消息
     *//*

    public static postSendPersonnelTemplateDto postSendPersonnelTemplate(String url, String json) throws Exception {
        log.info("海康创建 下载任务/开启任务---> {}", json);
        JSONObject hikResponse = HikHttpUtils.postJSON(url, json);
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
        postSendPersonnelTemplateDto map = new postSendPersonnelTemplateDto();
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
            map.setState(dto.getMsg());
            map.setStateLabel("成功");
        } else {
            map.setState("fail");
            map.setStateLabel(dto.getMsg());
        }
        return map;
    }


    public static String shortcut(String url, String json) throws Exception {
        log.info("出入权限配置快捷下载 ---> {}", json);
        JSONObject hikResponse = HikHttpUtils.postJSON(url, json);
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);//guard
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
            log.info("出入权限配置快捷下载成功  ---> {}", dto.getData().getTaskId());
            return dto.getData().getTaskId();
        } else {
            log.info("出入权限配置快捷下载失败错误码  ---> {}", dto.getCode());
            throw new Exception("出入权限配置快捷下载失败错误码 " + dto.getCode());
        }
    }

    public static String createAuthdownLoad(String url, String json) throws Exception {
        log.info("出入权限配置  ---> {}", json + "  ---  " + hikConfiguration.getTagId());
        JSONObject hikResponse = HikHttpUtils.postJSON(url, json, hikConfiguration.getTagId());
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);//guard
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
            log.info("出入权限配置成功  ---> {}", dto.getData().getTaskId());
            return dto.getData().getTaskId();
        } else {
            log.info("出入权限配置失败错误码  ---> {}", dto.getCode());
            throw new Exception("出入权限配置失败错误码 " + dto.getCode());
        }
    }


    public static HikDeviceReturnDto taskProgress(String url, String json) throws Exception {
        log.info("海康-查询结果---> {}", json);
        JSONObject hikResponse = HikHttpUtils.postJSON(url, json);
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
            log.info("海康-查询结果成功 ---> {}", dto.getData().getTaskId());
            return dto;
        } else {
            log.info("海康-查询结果失败 错误码  ---> {}", dto.getCode());
            throw new Exception("海康-查询结果失败 错误码  " + dto.getCode());
        }
    }





    public static List<RowsDto> postGetVisitorLog(String url, String json) throws Exception {
        JSONObject hikResponse = HikHttpUtils.postJSON(url, json);
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);//guard
        if ("0".equals(dto.getCode())) {
            log.info("获取访客记录列表  ---> {}", dto.getData().getRows());
            return dto.getData().getRows();
        } else {
            log.info("获取访客记录列表失败 错误码  ---> {}", dto.getCode());
            throw new Exception("获取访客记录列表失败 错误码 " + dto.getCode());
        }
    }


    *//**
     * @param url
     * @param json
     * @return
     * @throws Exception
     *//*
    public static boolean facePictureCheckApi(String url, String json) throws Exception {
        log.info("海康人脸评测  ---> {}", json);
        JSONObject hikResponse = HikHttpUtils.postJSON(url, json);
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);
        if ("0".equals(dto.getCode())) {
            return true;// 成功
        }
        if ("0x1f902302".equals(dto.getCode())) {
            log.info("海康人脸评测失败 ---> 人脸算法无授权");
            throw new ResultException(" 内部服务错误! ");
        }
        log.info("海康人脸评测失败 ---> " + hikResponse);
        return false; // 失败
    }



    private static HashMap<String, Object> setMapper(int i, String name, String visitStartTimeBegin, String visitStartTimeEnd) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("visitorName", name);
        map.put("visitStartTimeBegin", DateUtil.dateToISO8601(visitStartTimeBegin)); //到访时间
        map.put("visitStartTimeEnd", DateUtil.dateToISO8601(visitStartTimeEnd)); //结束时间
        map.put("pageNo", i);
        map.put("pageSize", 1000);
        return map;
    }




    *//**
     * @param json
     * @return
     * @throws Exception
     *//*
    public static Integer postGetOnline(String json) throws Exception {
        JSONObject hikResponse = HikHttpUtils.postJSON(HikApi.HAIKANG_DEVICE_ONLINE_API, json);
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);//guard
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("Operation succeeded")) {
            log.info("获取设备状态  ---> {}", dto.getData().getList().get(0).getOnline());
            return dto.getData().getList().get(0).getOnline();
        } else {
            log.info("获取设备状态 失败 错误码  ---> {}", dto.getCode());
            throw new Exception("获取设备状态 失败 错误码" + dto.getCode());
        }
    }


    *//**
     * @param url
     * @param json
     * @return
     * @throws Exception
     *//*
    public static String postGetChannelNo(String url, String json) throws Exception {
        JSONObject hikResponse = HikHttpUtils.postJSON(url, json);
        assert hikResponse != null;
        HikDeviceReturnDto dto = JSON.parseObject(hikResponse.toString(), HikDeviceReturnDto.class);//guard
        if ("0".equals(dto.getCode()) && dto.getMsg().equals("success")) {
            log.info("获取指定设备的通道号  ---> {}", dto.getData().getList().get(0).getChannelNo());
            return dto.getData().getList().get(0).getChannelNo();
        } else {
            log.info("获取指定设备的通道号失败 错误码---> {}", dto.getCode());
            throw new Exception("获取指定设备的通道号失败 错误码" + dto.getCode());
        }
    }




    *//**
     * 0x0531f002	访问结束时间少于现在
     * 预计离开时间应晚于预计来访时间	调整预计离开时间入参，改成晚于预计来访时间	0x0531f003	访问结束时间少于访问开始时间
     * 访客{0}在来访时段内已有其他有效预约或登记	返回的消息中data属性带了访客姓名，检查该访客的证件号码或者手机号码在预约时间范围内存在相同的记录	0x0531401d	时间冲突
     * 访客列表内访客信息过多，请检查确认	检查访客信息列表，不能超过128	0x0531f010	访客数量超过限制
     * 访客信息列表中存在相同的手机号码或证件号码	检查访客信息列表中是否存在相同的手机号码或者证件号码	0x05314023	访客信息重复
     * 业务通用错误码	根据参数无法查找到正确记录信息	检查输入参数是否有误	0x0531f00e	必需的参数 \$\$ 找不到相应的记录。
     *
     * @param src
     * @return
     * @throws Exception
     *//*

    public static String getImageBase(String src) throws Exception {
        URL url = new URL(src);
        InputStream in = null;
        byte[] bytes = null;
        try {
            in = url.openConnection().getInputStream();//远程文件
            bytes = IOUtils.toByteArray(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(bytes);
    }
*/

}
