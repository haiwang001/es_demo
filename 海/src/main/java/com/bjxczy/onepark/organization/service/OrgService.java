package com.bjxczy.onepark.organization.service;


import com.bjxczy.onepark.common.model.organization.OrganizationInformation;

public interface OrgService {

    void addOrgHik(OrganizationInformation information);
    void updateOrgHik(OrganizationInformation information);
    void deleteOrgHik(OrganizationInformation information);
    Boolean isOrgHik(Integer orgId);
}
