package com.bjxczy.onepark.organization.interfaces.listener;

import com.bjxczy.core.rabbitmq.supports.OrganizationBaseListener;
import com.bjxczy.onepark.common.model.organization.OrganizationInformation;
import com.bjxczy.onepark.organization.service.OrgService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrgEventListener extends OrganizationBaseListener {

    @Autowired
    private OrgService orgService;


    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "organization_add_org_queue", durable
                            = "true"),
                    // routing key
                    key = CREATE_ORGANIZATION_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME )
            )
    })
    @Override
    public void createOrganizationEventHandler(OrganizationInformation information, Channel channel, Message message) {
        try {
            log.info("[订阅事件 -> CREATE_ORG_EVENT] = {}",information);
            orgService.addOrgHik(information);
            this.confirm(()->true,channel,message);
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }



    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "organization_update_org_queue", durable
                            = "true"),
                    key = UPDATE_ORGANIZATION_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME )
            )
    })
    @Override
    public void updateOrganizationEventHandler(OrganizationInformation information, Channel channel, Message message) {

        try {
            log.info("[订阅事件 -> UPDATE_ORG_EVENT] = {}",information);
            orgService.updateOrgHik(information);
            this.confirm(()->true,channel,message);
        }catch (Exception exception){
            exception.printStackTrace();
        }

    }
    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "organization_remove_org_queue", durable = "true"),
                    // routing key
                    key = REMOVE_ORGANIZATION_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME )
            )
    })
    @Override
    public void removeOrganizationEventHandler(OrganizationInformation information, Channel channel, Message message) {
        try {
            log.info("[订阅事件 -> REMOVE_ORG_EVENT] = {}",information);
            orgService.deleteOrgHik(information);
            this.confirm(()->true,channel,message);
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }
}
