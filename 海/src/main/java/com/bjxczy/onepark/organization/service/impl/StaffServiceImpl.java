package com.bjxczy.onepark.organization.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.cons.HikApi;
import com.bjxczy.onepark.common.handle.HikSendHandle;
import com.bjxczy.onepark.common.manager.HikvisionApiProxy;
import com.bjxczy.onepark.common.manager.HikvisionBody;
import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.organization.service.StaffService;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikDeviceReturnDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Service
@Slf4j
public class StaffServiceImpl implements StaffService {

    @Autowired
    private HikvisionApiProxy apiProxy;
    @Autowired
    HikSendHandle hikSendHandle;

    @Override
    public void addStaffHik(StaffInformation staffInfomation) {
        log.info("[ addStaffHik ] -> {}", staffInfomation);
        Map<String, Object> param = new HashMap<>();
        param.put("personId", staffInfomation.getId());
        param.put("personName", staffInfomation.getFldName());
        param.put("gender", Objects.isNull(staffInfomation.getGender()) ? 0 : staffInfomation.getGender());
        //组织id
        param.put("orgIndexCode", staffInfomation.getOrganizationId());
        param.put("phoneNo", staffInfomation.getMobile());
        param.put("jobNo", staffInfomation.getEmployeeNo());
        JSONObject post = hikSendHandle.post("/api/resource/v2/person/single/add", JSONObject.toJSONString(param));
        log.info("[res] {}", post);
    }

    @Override
    public void updateStaffHik(StaffInformation staffInfomation) {
        log.info("[ updateStaffHik ] -> {}", staffInfomation);
        Map<String, Object> param = new HashMap<>();
        param.put("personId", staffInfomation.getId());
        param.put("personName", staffInfomation.getFldName());
        param.put("gender", staffInfomation.getGender());
        param.put("phoneNo", staffInfomation.getMobile());
        param.put("email", staffInfomation.getEmail());
        param.put("jobNo", staffInfomation.getEmployeeNo());
        param.put("orgIndexCode", staffInfomation.getOrganizationId());// 修组织
        HikvisionBody body = apiProxy.post(HikApi.ARTEMIS + "/api/resource/v1/person/single/update")
                .setBody(param)
                .execute().toHikvisionBody();

    }

    @Override
    public void deleteStaffHik(RemoveStaffInfo removeStaffInfo) {
        log.info("[ removeStaffHik ] -> {}", removeStaffInfo);
        Map<String, Object> param = new HashMap<>();
        param.put("personIds",
                new ArrayList<Object>() {{
                    add(removeStaffInfo.getId());
                }});
        HikvisionBody body = apiProxy.post(HikApi.ARTEMIS + "/api/resource/v1/person/batch/delete")
                .setBody(param)
                .execute().toHikvisionBody();
        log.info("[res] {}", body);
    }

    @Override
    public Boolean isStaffHik(Integer staffId) {
        Map<String, Object> param = new HashMap<>();
        param.put("paramName", "personId");
        param.put("paramValue", new ArrayList<Object>() {{
            add(staffId);
        }});
        String body = apiProxy.post(HikApi.ARTEMIS + "/api/resource/v1/person/condition/personInfo")
                .setBody(param)
                .execute().toResponseString();
        HikDeviceReturnDto dto = JSON.parseObject(body, HikDeviceReturnDto.class);
        log.info("[ isStaffHik ] -> {}, {}", staffId, dto);
        if (CollectionUtils.isNotEmpty(dto.getData().getList())) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public String isStaffHik(String phoneNo) {
        Map<String, Object> param = new HashMap<>();
        param.put("phoneNo", phoneNo);
        String body = apiProxy.post(HikApi.ARTEMIS + "/api/resource/v1/person/phoneNo/personInfo")
                .setBody(param).execute().toResponseString();
        HikDeviceReturnDto dto = JSON.parseObject(body, HikDeviceReturnDto.class);
        log.info("[ 手机号查询员工 ] -> 手机号{}, 结果{}", phoneNo, dto);
        if (dto.getData() == null) {
            return null;
        }
        return dto.getData().getPersonId();
    }
}
