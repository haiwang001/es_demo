package com.bjxczy.onepark.organization.interfaces.feign;

import com.bjxczy.core.feign.client.hikvision.HikOrgAndStaffFeignClient;
import com.bjxczy.onepark.organization.service.OrgService;
import com.bjxczy.onepark.organization.service.StaffService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Auther: Administrator
 * @Date: 2023/7/13 0013 15:25
 * @Description: HikOrgAndStaffFeignController
 * @Version 1.0.0
 */
@Log4j2
@Controller
@ResponseBody
public class HikOrgAndStaffFeignController implements HikOrgAndStaffFeignClient {

    @Autowired
    private OrgService orgService;

    @Autowired
    private StaffService staffService;
    @Override
    public Boolean isOrgHik(@RequestParam("orgId") Integer orgId){
        return orgService.isOrgHik(orgId);
    }
    @Override
    public Boolean isStaffHik(@RequestParam("staffId") Integer staffId){
        return staffService.isStaffHik(staffId);
    }
}
