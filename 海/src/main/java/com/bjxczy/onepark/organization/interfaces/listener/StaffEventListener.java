package com.bjxczy.onepark.organization.interfaces.listener;

import com.bjxczy.core.rabbitmq.supports.OrganizationBaseListener;
import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.organization.service.StaffService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
@Slf4j
public class StaffEventListener extends OrganizationBaseListener {


    @Autowired
    private StaffService staffService;

    @RabbitListener(bindings = {
            @QueueBinding(
                        // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "organization_add_staff_event_queue", durable
                            = "true"),
                        // routing key
                    key = CREATE_STAFF_EVENT,
                        // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME )
            )
    })
    @Override
    public void createStaffEventHandler(StaffInformation staffInfomation, Channel channel, Message message) {
        log.info("[订阅事件 -> CREATE_STAFF_EVENT] = {}",staffInfomation);
        try{
            staffService.addStaffHik(staffInfomation);
            this.confirm(()->true,channel,message);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "organization_update_staff_event_queue", durable
                            = "true"),
                    // routing key
                    key = UPDATE_STAFF_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME )
            )
    })
    @Override
    public void updateStaffEventHandler(StaffInformation staffInfomation, Channel channel, Message message) {
        log.info("[订阅事件 -> UPDATE_STAFF_EVENT] = {}",staffInfomation);
        try{
            staffService.updateStaffHik(staffInfomation);
            this.confirm(()->true,channel,message);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @RabbitListener(bindings = {
            @QueueBinding(
                    // 队列定义，建议使用应用名作为前缀，避免重复，持久化队列
                    value = @Queue(name = "organization_remove_staff_event_queue", durable
                            = "true"),
                    // routing key
                    key = REMOVE_STAFF_EVENT,
                    // 绑定的交换机
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME )
            )
    })
    @Override
    public void removeStaffEventHandler(RemoveStaffInfo removeStaffInfo, Channel channel, Message message) {
        log.info("[订阅事件 -> DELETE_STAFF_EVENT] = {}",removeStaffInfo);
        try{
            staffService.deleteStaffHik(removeStaffInfo);
            this.confirm(()->true,channel,message);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }



}
