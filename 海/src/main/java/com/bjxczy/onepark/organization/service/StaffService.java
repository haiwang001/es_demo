package com.bjxczy.onepark.organization.service;

import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;

public interface StaffService {
    void addStaffHik(StaffInformation staffInfomation);

    void updateStaffHik(StaffInformation staffInfomation);

    void deleteStaffHik(RemoveStaffInfo removeStaffInfo);

    Boolean isStaffHik(Integer staffId);

    String isStaffHik(String phoneNo);
}
