package com.bjxczy.onepark.organization.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bjxczy.onepark.common.cons.HikApi;
import com.bjxczy.onepark.common.handle.HikSendHandle;
import com.bjxczy.onepark.common.manager.HikvisionApiProxy;
import com.bjxczy.onepark.common.manager.HikvisionBody;
import com.bjxczy.onepark.common.model.organization.OrganizationInformation;
import com.bjxczy.onepark.organization.service.OrgService;
import com.bjxczy.onepark.visitor.pojo.HikInOut.HikDeviceReturnDto;
import com.google.common.collect.ImmutableList;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


@Service
@Slf4j
public class OrgServiceImpl implements OrgService {

    @Autowired
    private HikvisionApiProxy apiProxy;
    @Autowired
    HikSendHandle hikSendHandle;

    @Override
    public void addOrgHik(OrganizationInformation information) {
        log.info("[ addOrgHik ] -> {}",information);
        Map<String, Object> param = new HashMap<>();
        param.put("orgIndexCode", information.getId());
        param.put("orgName", information.getFldName());
        param.put("parentIndexCode", information.getParentId() == -1 ?"root000000":information.getParentId());
        hikSendHandle.post("/api/resource/v1/org/batch/add", JSONObject.toJSONString(ImmutableList.of(param)));

    }

    @Override
    public void updateOrgHik(OrganizationInformation information) {
        log.info("[ updateOrgHik ] -> {}",information);
        Map<String, Object> param = new HashMap<>();
        param.put("orgIndexCode",information.getId());
        param.put("orgName",information.getFldName());
        HikvisionBody body = apiProxy.post(HikApi.ARTEMIS+"/api/resource/v1/org/single/update")
                .setBody(param)
                .execute().toHikvisionBody();
    }

    @Override
    public void deleteOrgHik(OrganizationInformation information) {
        log.info("[ deleteOrgHik ] -> {}",information);
        Map<String, Object> param = new HashMap<>();
        param.put("indexCodes",new ArrayList<Object>(){{
            add(information.getId());
        }});
        HikvisionBody body = apiProxy.post(HikApi.ARTEMIS+"/api/resource/v1/org/batch/delete")
                .setBody(param)
                .execute().toHikvisionBody();
    }

    @Override
    public Boolean isOrgHik(Integer orgId) {
        log.info("[ isOrgHik ] -> {}",orgId);
        Map<String, Object> param = new HashMap<>();
        param.put("orgIndexCodes",new ArrayList<Object>(){{
            add(orgId);
        }});
        String body = apiProxy.post(HikApi.ARTEMIS + "/api/resource/v1/org/orgIndexCodes/orgInfo").setBody(param).execute().toResponseString();
        HikDeviceReturnDto dto = JSON.parseObject(body, HikDeviceReturnDto.class);
        if (CollectionUtils.isNotEmpty(dto.getData().getList())){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
