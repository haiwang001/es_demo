# 会议模板-API开发文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录 |
| --------- | ------ | ---------- | ------------ |
| v0.0.1    | 温良伟 | 2023.02.14 | 初始化文档   |

## 一、表结构

 ###  会议模板  (tbl_meeting_template)    MeetingTemplate 

 | 数据库字段 |   代码字段    | 字段类型      | 是否必填   | 描述     | 
   | -----------| ------------  | ------------ | -------- | -------- |
    | template_id      | templateId  | bigint(20)   | 是       | 主键ID   |
    | template_name     |templateName    | varchar(20)   | 是       | 模板名称   |
    | template_state     |templateState    | Boolean   | 是       | 模板状态   |
    | create_time | createTime| varchar(255) | 否        | 创建时间 |
    | update_time| updateTime| varchar(255) | 否        | 修改时间 |
    | operator |operator| varchar(255) | 否        | 操作人 |
    | del_flag| delFlag | varchar(10) | 否        | 删除标记 |


    
###  会议模板和物品绑定表  tbl_meeting_template_goods    MeetingTemplateGoods 

   | 数据库字段 |   代码字段    | 字段类型      | 是否必填   | 描述     | 
   | -----------| ------------  | ------------ | -------- | -------- |
  | id      | id  | bigint(20)   | 是       | 主键ID   |
  | template_id      | templateId  | bigint(20)   | 是       | 模板id   |
  | goods_name     |goodsName    | varchar(20)   | 是       | 物品名称   |
  | amount     |amount    | varchar(20)   | 是       | 物品数量   |
  | uni_valence     |uniValence    | varchar(20)   | 是       | 物品单价   |
  | del_flag| delFlag | varchar(10) | 否        | 删除标记 |
  

## 二、接口
### 1，首页

#### 1.1  新增模板

| URL    | http://host:port/MeetingTemplate/add |
 | --------- |  ------------ |
 |Method | post       |             |

请求参数：

| 参数     | 类型    | 说明       |
| -------- | ------ | ---------- |
| templateName   | 字符串 | 模板名称 | 
| templateState   | 布尔 | 模板状态 | 
| operator   | 字符串 | 操作人 | 
| goodsList   | array | 物品数组 | 
| goodsId   | 字符串 | 物品id | 
| goodsName   | 字符串 | 物品名称 | 
| amount   | 字符串 | 数量 | 
| uniValence   | 浮点数 | 单价 | 

请求：
```
{
    "templateName":"模板名称",
    "templateState":false,
    "operator":"wlw",
    "goodsList":[
        {
           "goodsId":"id",
           "goodsName":"未知物品",
           "amount":30,
           "uniValence":66.66
        },
         {
"goodsId":"id",
           "goodsName":"未知物品",
           "amount":30,
           "uniValence":66.66
        }
        ,
         {
"goodsId":"id",
           "goodsName":"未知物品",
           "amount":30,
           "uniValence":66.66
        }
    ]
}
```

返回


#### 1.2 修改模板状态

描述：修改模板状态

| URL    | http://host:port/MeetingTemplate/updateState |
 | --------- |  ------------ |
 |Method | pud       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| templateId   | String |  标记                     |
| templateState   | 布尔 |  状态                     |


```json
{
    "templateId": "模板标记",
    "templateState": "模板状态"
}
```
返回示例：
```json
{
  "code": 0,
  "message": "请求成功！",
  "data": {}
}
```

#### 1.3  删除模板

描述： 添加对应分组的物品 

| URL    | http://host:port/MeetingTemplate/delete |
 | --------- |  ------------ |
 |Method | delete       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| ids  | 数组 元素是字符串  |  模板id   |

请求：
```
{
  "ids":["1fe3192f63734465a4cc52c5520669ac"]
}
```

返回示例：
```
{
  "code": 0,
  "message": "请求成功！",
  "data": {}
}
```
#### 1.4  模板编辑

描述： 模板编辑 

| URL    | http://host:port/MeetingTemplate/update/{templateId} |
 | --------- |  ------------ |
 |Method | put       |             |

请求参数：

| 参数     | 类型    | 说明       |
| -------- | ------ | ---------- |
| templateName   | 字符串 | 模板名称 | 
| templateState   | 布尔 | 模板状态 | 
| operator   | 字符串 | 操作人 | 
| goodsList   | array | 物品数组 | 
| goodsId   | 字符串 | 物品id | 
| goodsName   | 字符串 | 物品名称 | 
| amount   | 字符串 | 数量 | 
| uniValence   | 浮点数 | 单价 | 


请求：
```
{
    "templateName":"模板名称",
    "templateState":false,
    "operator":"wlw",
    "goodsList":[
        {
           "goodsId":"id",
           "goodsName":"未知物品",
           "amount":30,
           "uniValence":66.66
        },
         {
"goodsId":"id",
           "goodsName":"未知物品",
           "amount":30,
           "uniValence":66.66
        }
        ,
         {
"goodsId":"id",
           "goodsName":"未知物品",
           "amount":30,
           "uniValence":66.66
        }
    ]
}
```

#### 1.5  模板列表查询

描述： 模板编辑 

| URL    | http://host:port/MeetingTemplate/list|
 | --------- |  ------------ |
 |Method | get       |             |

返回参数：

| 参数     | 类型    | 说明       |
| -------- | ------ | ---------- |
| list | 数组 | 会议模板列表 |
| templateId | 字符串 | 会议模板标记 |
| templateName | 字符串 | 会议模板名称 |
| templateState | 布尔 | 会议模板状态 |
| goodsList | 数组 | 会议模板下的物品 |
| amount | 数字 | 物品数量 |
| uniValence | 数字 | 单价 |


返回：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"total": 1,
		"list": [
			{
				"templateId": "1fe3192f63734465a4cc52c5520669ac",
				"templateName": "xiehcengzhiyuan",
				"templateState": false,
				"goodsList": [
					{
						"id": 17,
						"templateId": "1fe3192f63734465a4cc52c5520669ac",
						"delFlag": 0,
						"goodsId": null,
						"goodsName": "未知物品",
						"amount": 30,
						"uniValence": 66
					},
					{
						"id": 18,
						"templateId": "1fe3192f63734465a4cc52c5520669ac",
						"delFlag": 0,
						"goodsId": null,
						"goodsName": "未知物品2",
						"amount": 30,
						"uniValence": 66
					},
					{
						"id": 19,
						"templateId": "1fe3192f63734465a4cc52c5520669ac",
						"delFlag": 0,
						"goodsId": null,
						"goodsName": "未知物品3",
						"amount": 30,
						"uniValence": 66
					}
				]
			}
		],
		"pageNum": 1,
		"pageSize": 1,
		"size": 1,
		"startRow": 0,
		"endRow": 0,
		"pages": 1,
		"prePage": 0,
		"nextPage": 0,
		"isFirstPage": true,
		"isLastPage": true,
		"hasPreviousPage": false,
		"hasNextPage": false,
		"navigatePages": 8,
		"navigatepageNums": [
			1
		],
		"navigateFirstPage": 1,
		"navigateLastPage": 1
	}
}
```
