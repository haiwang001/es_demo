# 会议物品-API开发文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录 |
| --------- | ------ | ---------- | ------------ |
| v0.0.1    | 温良伟 | 2023.02.14 | 初始化文档   |

## 一、表结构

###  会议物品类型 分组  (meeting_goods_type)  MeetingGoodsType  

 | 数据库字段 |   代码字段    | 字段类型      | 是否必填   | 描述     | 
 | -----------| ------------  | ------------ | -------- | -------- |
 |   type_id   | typeId    | bigint(20)   | 是       | 主键ID   |
 | type_name | typeName | varchar(100)   | 是       | 物品类型名称  |
 | del_flag| delFlag | varchar(10) | 否        | 删除标记 |
 | operator |operator| varchar(255) | 否        | 操作人 |


 ###  会议物品  (meeting_goods)  MeetingGoods
  | 数据库字段 |   代码字段    | 字段类型      | 是否必填   | 描述     | 
  | -----------| ------------  | ------------ | -------- | -------- |
  | goods_id      | goodsId  | bigint(20)   | 是       | 主键ID   |
  | type_id     | typeId  | bigint(20)   | 是       | 绑定的分组id   |
  | type_name     |typeName    | varchar(20)   | 是       | 类型名称   |
  | goods_name     |goodsName    | varchar(20)   | 是       | 物品名称   |
  | uni_valence     |uniValence    | varchar(20)   | 是       | 单价   |
  | remark     |remark    | varchar(20)   | 是       | 备注信息   |
  | create_time | createTime| varchar(255) | 否        | 创建时间 |
  | operator |operator| varchar(255) | 否        | 操作人 |
  | del_flag| delFlag | varchar(10) | 否        | 删除标记 |
  | belong| belong | varchar(255) | 否        | 物品归属   会议室物品  参会人物品 |

## 二、接口
### 1，首页

#### 1.1  分组列表


| URL    | http://host:port/GoodsType/list/typeName值 |
 | --------- |  ------------ |
 |Method | GET       |             |
| URL    | http://127.0.0.1:8011/GoodsType/list/办公 |
请求参数：

| 参数     | 类型    | 说明       |
| -------- | ------ | ---------- |
| typeName   | string | 名称 | 

返回参数：

| 参数     | 类型    | 说明       |
| -------- | ------ | ---------- |
| typeId   | int | id | 
| typeName   | string | 名称 | 
| operator   | string | 操作人 | 


返回：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [
		{
			"typeId": 1,
			"typeName": "办公用具",
			"delFlag": 0,
			"operator": "lcb"
		}
	]
}
```

#### 1.2 新增物品类型

描述：新增会议室

| URL    | http://host:port/GoodsType/add |
 | --------- |  ------------ |
 |Method | post       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| tName   | String |  名称                     |
| operator   | String |  操作人                     |

```
{
    "typeName":"办公用具",
    "operator":"lcb"
}
```


返回参数：

返回示例：
```
{
  "code": 0,
  "message": "请求成功！",
  "exception": "",
  "data": 0
}

{
	"code": 500,
	"message": "系统异常",
	"exception": "改物品分类已经存在",
	"data": null
}
```



#### 1.3  物品类型删除

描述： 物品类型删除 

| URL    | http://host:port/GoodsType/delete/{typeId} |
 | --------- |  ------------ |
 |Method | post       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| typeId  | int   |     分组id   |

返回示例：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1
}
```
#### 1.4  物品分类编辑

描述： 物品分类编辑 

| URL    | http://host:port/GoodsType/update |
 | --------- |  ------------ |
 |Method | update       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| typeId  |  int   |  修改的id   |
|typeName    | varchar(20)   |  物品名称   |
```
{
    "typeName": "办公",
    "typeId": "1"
}
```

返回示例：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1
}
```

############################################################################################################################################

#### 1.4  物品新增

描述： 添加对应分组的物品编辑 

| URL    | http://127.0.0.1:8011/Goods/add |
 | --------- |  ------------ |
 |Method | post       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| goodsName  | varchar(20)   |  物品名字   |
| uniValence  | 小数   |  单价   |
| typeId  | 整数   |  分类id   |
| remark  | 字符串   |  备注   |
| operator  | 字符串   |  操作人   |

```
{
    "goodsName": "笔记本",
    "uniValence":666.66,
    "typeId": "1",
    "remark":"66666",
    "operator":"wlw"
}
```
返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
返回示例：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1
}

{
	"code": 500,
	"message": "系统异常",
	"exception": "物品已经在此分类已经存在",
	"data": null
}
```

#### 1.5  物品查询

描述： 物品查询列表 暂无条件 

| URL    | http://127.0.0.1:8011/Goods/list |
 | --------- |  ------------ |
 |Method | get       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |

```
{}
```
返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |
| goodsId  | 整数   |  物品标记   |
| typeId  | 整数   |  物品类型标记   |
| typeName  | 字符串   |  物品分类名称   |
| goodsName  | 字符串   |  物品名称   |
| uniValence  | 小数   |  单价   |
| remark  | 字符串   |  备注   |

返回示例：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"total": 1,
		"list": [
			{
				"goodsId": 1,
				"typeId": 1,
				"typeName": "办公",
				"goodsName": "笔记本",
				"uniValence": 666,
				"remark": "66666",
				"createTime": "2023-02-16 18:06:44"
			}
		],
		"pageNum": 1,
		"pageSize": 1,
		"size": 1,
		"startRow": 0,
		"endRow": 0,
		"pages": 1,
		"prePage": 0,
		"nextPage": 0,
		"isFirstPage": true,
		"isLastPage": true,
		"hasPreviousPage": false,
		"hasNextPage": false,
		"navigatePages": 8,
		"navigatepageNums": [
			1
		],
		"navigateFirstPage": 1,
		"navigateLastPage": 1
	}
}
```



#### 1.5  物品删除

描述： 物品查询列表 暂无条件 

| URL    | http://127.0.0.1:8011/Goods/del |
 | --------- |  ------------ |
 |Method | get       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| ids  | 数组   |  物品id   |

```
{
    "ids":[1,2,3]
}
```

返回 0 删除成功
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 0
}
```



#### 1.6  下载上传的模板

描述： 下载上传的模板 

| URL    | http://host:port/Goods/downloadTemplate |
 | --------- |  ------------ |
 |Method | get         |             |


响应一个文件

#### 1.6  导出数据

描述： 导出数据 

| URL    | http://host:port/Goods/downloadData |
 | --------- |  ------------ |
 |Method | get         |             |


响应一个文件


#### 1.7  导入数据

描述： 导入数据 

| URL    | http://host:port/Goods/import |
 | --------- |  ------------ |
 |Method | post         |             |

 请求参数：
上传一个.excl


#### 1.8  物品编辑

描述：  

| URL    | http://127.0.0.1:8011/Goods/update/{id值} |
 | --------- |  ------------ |
 |Method | put       |             |
 |URL | http://127.0.0.1:8011/Goods/update/25       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| goodsName  | varchar(20)   |  物品名字   |
| uniValence  | 小数   |  单价   |
| typeId  | 整数   |  分类id   |
| remark  | 字符串   |  备注   |
| operator  | 字符串   |  操作人   |

```
{
    "goodsName": "笔记本",
    "uniValence":666.66,
    "typeId": 357516,
    "remark":"66666",
    "operator":""
}
```
返回示例：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1
}
```