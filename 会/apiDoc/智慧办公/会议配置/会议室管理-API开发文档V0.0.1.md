# 会议室管理-API开发文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录 |
| --------- | ------ | ---------- | ------------ |
| v0.0.1    | 温良伟 | 2023.02.14 | 初始化文档   |

## 一、表结构

###  会议室管理表  (conference_room)   ConferenceRoom

 | 数据库字段 |   代码字段    | 字段类型      | 是否必填   | 描述     | 
| -----------| ------------  | ------------ | -------- | -------- |
| c_id | cId        | varchar(255)   | 是       | 主键ID   |
| room_name | roomName | varchar(100)   | 是       | 会议室名称   |
| address_id  |addressId | varchar(100)   | 是       | 局址id   |
| address_label  |addressLabel | varchar(100)   | 是       | 局址展示   |
| room_type | roomType | varchar(100)   | 是       | 会议室分类   |
| money | money | varchar(100)   | 是       | 会议室金额   |
| open_date  |openDate | varchar(100)   | 否       | 会议室开放时间 前 |
| end_date | endDate | varchar(100)   | 否         | 会议结开放时间 后  |
| room_state | roomState | varchar(100)   | 否         | 会议室状态  1启用  2未启用  |
| hygiene_state | hygieneState | varchar(100)   | 否         | 会议室卫生状态  1正在使用  2等待清扫 3 可以使用  |
| intend_ant_name | intendAntName | varchar(100)   | 是         | 管理员名称   |
| intend_ant_id | intendAntId | varchar(100)   | 是         | 管理员id   |
| img_list  |imgList | varchar(100)   | 否         |      会议室图片集合   |  
| create_time | createTime| varchar(255) | 是        | 创建时间 |
| update_time| updateTime| varchar(255) |是        | 修改时间 |
| operator |operator| varchar(255) | 是       | 操作人 |
| del_flag| delFlag | varchar(10) | 是        | 删除标记 |
| capacity| capacity | varchar(255) | 是        | 会议室容纳人数 |
| half_day_money| halfDayMoney | double | 是        | 会议室半天金额 |
| one_day_money | oneDayMoney | double | 是        | 会议室一天金额 |

###   conference_room_facility(会议室设备绑定表)  ConferenceRoomFacility 
 | 字段         |    代码字段    | 字段类型      | 是否必填   | 描述     | 
| ----------- | ------------ | ------------ | -------- | -------- |
| c_id        |cId |  varchar(200)   | 是         | 绑定的会议室id   |
| name      |name   | varchar(100)   | 是     | 设备名称   |
| money     |money    | double   | 是           | 设备金额   |
| number     |number    | int(100)   | 是       | 设备数量   |

## 二、接口
### 1，首页

#### 1.1 会议室列表

| URL    | http://host:port/ConferenceRoom/list |
 | --------- |  ------------ |
 |Method | GET       |             |

请求参数：

| 参数     | 类型    | 说明       |
| -------- | ------ | ---------- |
| pageNum   | Integer | 页数 | 
| pageSize  | Integer | 每页条数 | 
| addressId | String  | 局址id |
| openDate | date | 会议室开放时间  yyyy-MM-dd HH:mm  |
| endDate | date | 会议室开放截止时间  yyyy-MM-dd HH:mm  |
| roomState | Integer  | 会议室状态 |
| hygieneState | String  | 会议室清扫 |


请求参数：

| 参数     | 类型    | 说明       |
| -------- | ------ | ---------- |
| facilityList   | 数组 | 返回设备的列表数组 | 
| imgList  | 数组 | 图片路径数组 | 
| intendAntNames  | 数组 | 管理员名字数组 | 
| intendAntId  | 数组 | 管理员id数组 | 



返回：  其中  intendAntIds  intendAntName urlList  为不可用数据
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"total": 3,
		"list": [
			{
				"createTime": "2023-02-16 11:50:25",
				"updateTime": "2023-02-16 11:50:25",
				"delFlag": 0,
				"operator": "wlw",
				"roomName": "会议室一",
				"addressId": "949c2d805ef74ea3b5b0dfd9522d117a",
				"addressLabel": "西区-304-test",
				"roomType": "T字型",
				"money": 2000,
				"openDate": "2021-09-01 11:39:11",
				"endDate": "2021-09-02 11:39:11",
				"roomState": 2,
				"hygieneState": "使用中",
				"intendAntIds": null,
				"intendAntId": "[13156,13155]",
				"intendAntNames": [
					"周丽",
					"王婧"
				],
				"intendAntName": "[\"周丽\",\"王婧\"]",
				"imgList": [
					"6666",
					"7777"
				],
				"urlList": "[\"6666\",\"7777\"]",
				"facilityList": [
					{
						"name": "会议桌",
						"money": 777.55,
						"number": 1,
						"cid": "05d2b1254d414595bd0c2bbbbb0be2ad"
					}
				],
				"cid": "05d2b1254d414595bd0c2bbbbb0be2ad"
			},
			{
				"createTime": "2023-02-16 09:51:44",
				"updateTime": "2023-02-16 13:08:27",
				"delFlag": 0,
				"operator": "wlw",
				"roomName": "会议室测试",
				"addressId": "31055b5e456b492c8459e1283921cb7f",
				"addressLabel": "西区-304-test",
				"roomType": "t字型",
				"money": 5566,
				"openDate": "2021-09-01 11:39:11",
				"endDate": "2021-09-01 11:39:11",
				"roomState": 1,
				"hygieneState": "待清扫",
				"intendAntIds": null,
				"intendAntId": "[13156,13155]",
				"intendAntNames": [
					"周丽",
					"王婧"
				],
				"intendAntName": "[\"周丽\",\"王婧\"]",
				"imgList": [
					"6666",
					"99999999"
				],
				"urlList": "[\"6666\",\"99999999\"]",
				"facilityList": [
					{
						"name": "会议桌66666666666666",
						"money": 7778888.55,
						"number": 1,
						"cid": "82a6eff0c2e64415b1c9eb88435a9cef"
					}
				],
				"cid": "82a6eff0c2e64415b1c9eb88435a9cef"
			},
			{
				"createTime": "2023-02-16 09:55:41",
				"updateTime": "2023-02-16 09:55:41",
				"delFlag": 0,
				"operator": "wlw",
				"roomName": "会议室名称",
				"addressId": "31055b5e456b492c8459e1283921cb7f",
				"addressLabel": "西区-304-test",
				"roomType": "口字型",
				"money": 55.55,
				"openDate": "2021-09-01 11:39:11",
				"endDate": "2021-09-01 11:39:11",
				"roomState": 1,
				"hygieneState": "待清扫",
				"intendAntIds": null,
				"intendAntId": "[13156,13155]",
				"intendAntNames": [
					"周丽",
					"王婧"
				],
				"intendAntName": "[\"周丽\",\"王婧\"]",
				"imgList": [
					"6666",
					"7777"
				],
				"urlList": "[\"6666\",\"7777\"]",
				"facilityList": [
					{
						"name": "会议桌",
						"money": 777.55,
						"number": 1,
						"cid": "cc3d48c1af1f4b3c90f2e2f42d46adba"
					}
				],
				"cid": "cc3d48c1af1f4b3c90f2e2f42d46adba"
			}
		],
		"pageNum": 1,
		"pageSize": 3,
		"size": 3,
		"startRow": 0,
		"endRow": 2,
		"pages": 1,
		"prePage": 0,
		"nextPage": 0,
		"isFirstPage": true,
		"isLastPage": true,
		"hasPreviousPage": false,
		"hasNextPage": false,
		"navigatePages": 8,
		"navigatepageNums": [
			1
		],
		"navigateFirstPage": 1,
		"navigateLastPage": 1
	}
}
```

#### 1.2 新增会议室

描述：新增会议室

| URL    | http://host:port/ConferenceRoom/add |
 | --------- |  ------------ |
 |Method | post       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| roomName   | String |  会议室名称                     |
| addressId  | Integer |  局址id                 |
| money  | double |  会议室金额                |
| roomType  | String |  分类                |
| openDate  | String |  开放时间    yyyy-MM-dd HH:mm              |
| endDate  | String |   结束时间      yyyy-MM-dd HH:mm             |
| roomState  | number | 使用状态     1 禁用 2使用              |
| intendAntIds  | list | 管理员id                   |
| imgList  | list | 图片                   |

请求参数
```
{
	"roomName": "会议室名称",
	"addressId": "31055b5e456b492c8459e1283921cb7f",
	"roomType": "口字型",
	"money": 55.55,
	"openDate": "2021-09-01 11:39:11",
	"endDate": "2021-09-01 11:39:11",
	"roomState": 1,
	"intendAntIds": [13156,13155],
	"operator": "wlw",
	"imgList": ["6666", "7777"],
	"facilityList": [{
		"name": "会议桌",
		"money": 777.55,
		"number": 1
	}]
}
```


返回参数：

| 参数 | 类型 | 说明     |
| ---- | ---- | -------- |

返回示例：
```
{
  "code": 0,
  "message": "请求成功！",
  "data": {}
}

{
	"code": 500,
	"message": "系统异常",
	"exception": "查询不到区域信息",
	"data": null
}

{
	"code": 500,
	"message": "系统异常",
	"exception": "该区域下存在同名会议室添加失败",
	"data": null
}
```

#### 1.3 编辑会议室

描述： 编辑会议室

| URL    | http://host:port/ConferenceRoom/update/cId值 |
 | --------- |  ------------ |
 |Method | Put       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| roomName   | String |  会议室名称                     |
| addressId  | Integer |  局址id                 |
| money  | double |  会议室金额                |
| roomType  | String |  分类                |
| openDate  | String |  开放时间    yyyy-MM-dd HH:mm             |
| endDate  | String |   结束时间      yyyy-MM-dd HH:mm             |
| roomState  | number | 使用状态     1 禁用 2使用              |
| intendAntIds  | list | 管理员id                   |
| imgList  | list | 图片                   |

bmpoffice/ConferenceRoom/update/82a6eff0c2e64415b1c9eb88435a9cef
json 请求
```
{
    "roomName": "会议室测试",
	"addressId": "31055b5e456b492c8459e1283921cb7f",
	"roomType": "t字型",
	"money": 5566,
	"openDate": "2021-09-01 11:39:11",
	"endDate": "2021-09-01 11:39:11",
	"roomState": 1,
	"intendAntIds": [13156,13155],
	"operator": "wlw",
	"imgList": ["6666", "99999999"],
	"facilityList": [{
		"name": "会议桌66666666666666",
		"money": 777.55,
		"number": 1
	}]
}
```
返回示例：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1
}
```



#### 1.4 修改状态

描述：通过会议室id 修改使用状态

| URL    | http://127.0.0.1:8011/ConferenceRoom/pudRoomState/cId值 |
 | --------- |  ------------ |
 |Method | Put       |             |

请求参数：

| 参数      | 类型    |  说明                     |
| --------- | ------- |  ------------------------ |
| cId   | String |  会议室标记                     |
| RoomState  | boolean | 使用状态   true启用 false禁用                |

```
{
    "RoomState":false
}
```

返回示例：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 0
}
```

#### 1.5  字典查询

描述：字典查询

| URL    | http://dev.xconepark.cn/api/bmpoffice/dict/list/值 |
 | --------- |  ------------ |
 |Method | get       |             |

请求参数：

| 值        |  说明                     |
| ---------  |  ------------------------ |
| hygieneState     |  返回启用/禁用                     |
| roomState     |  返回使用/未清扫                     |
| remindTime     |  返回提醒时长                 |
| minTime     |  返回会议时长                 |


#### 1.6  文件上传

描述：文件上传

| URL    | http://dev.xconepark.cn/api/bmpoffice/base/upload |
 | --------- |  ------------ |
 |Method | post       |             |

请求参数： 一个文件

返回示例：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"url": "http://192.165.1.62:9881/group1/M00/00/1C/wKUBPmPt2RuAO7lfAAIQPgKzRYI82.jpeg"
	}
}
```