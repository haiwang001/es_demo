# 会议二期 -API开发文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录 |
| --------- | ------ | ---------- | ------------ |
| v0.0.1    | 温良伟 | 2023.04.23  | 初始化文档   |
| v0.0.2    | 温良伟 | 2023.04.25  | 会议图表查询   |
| v0.0.3    | 温良伟 | 2023.04.26  | 人脸信息查询   |

## 接口

### 1、   增加水牌/人名牌

#### 1.1、 增加水牌

| URL    | http://host:port/waterCard/add |
| ------ | --------------------------------- |
| Method | post                               |

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| name           | string |  模板名称     |      
| url            | string |  模板地址     |     
| fontColor      | string |  字体颜色     |     
| fontList       | string |  字体列表     |     
| + text         | string |  文本        |     
| + size         | num |  像素值       |     


 ```json
{
    "name": "李四",
    "url": "5646546464645",
    "fontColor": "#fff",
    "fontList": [
        {
            "text": "65495466464646464646464656466664",
            "size": 16
        },
        {
            "text": "777",
            "size": 16
        }
    ]
}
 ```


#### 1.2、 增加人名牌

| URL    | http://host:port/nameCard/add |
| ------ | --------------------------------- |
| Method | post                               |

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| name           | string |  模板名称     |      
| url            | string |  模板地址     |     
| fontColor      | string |  字体颜色     |     
| fontList       | string |  字体列表     |     
| + text         | string |  文本        |     
| + size         | num |  像素值       |     


 ```json
{
    "name": "李四",
    "url": "5646546464645",
    "fontColor": "#fff",
    "fontList": [
        {
            "text": "65495466464646464646464656466664",
            "size": 16
        }
    ]
}
 ```


### 2、   查询水牌/人名牌

#### 1.1、 查询水牌

| URL    | http://host:port/waterCard/list |
| ------ | --------------------------------- |
| Method | get                               |

无请求参数: 

返回数据:

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| cardId           | string |  id     |      
| type           | string |  类型 1 水牌 2 人名牌     |      
| createTime           | string |  创建时间     |      
| operator           | string |  操作人     |      
| name           | string |  模板名称     |      
| url            | string |  模板地址     |     
| fontColor      | string |  字体颜色     |     
| fontList       | string |  字体列表     |     
| + cardId         | string |  绑定的模板id        |     
| + text         | string |  文本        |     
| + size         | num |  像素值       |    
 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [
		{
			"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
			"type": "1",
			"createTime": "2023-04-23 15:13:52",
			"name": "李四",
			"url": "5646546464645",
			"fontColor": "#fff",
			"operator": "温良伟",
			"delFlag": 0,
			"fontList": [
				{
					"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
					"text": "65495466464646464646464656466664",
					"size": 16
				},
				{
					"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
					"text": "777",
					"size": 16
				}
			]
		},
		{
			"cardId": "f4f02c68-4dee-47f1-b5f2-f12db51a0209",
			"type": "1",
			"createTime": "2023-04-23 16:22:42",
			"name": "李四",
			"url": "5646546464645",
			"fontColor": "#fff",
			"operator": "温良伟",
			"delFlag": 0,
			"fontList": [
				{
					"cardId": "f4f02c68-4dee-47f1-b5f2-f12db51a0209",
					"text": "666",
					"size": 16
				},
				{
					"cardId": "f4f02c68-4dee-47f1-b5f2-f12db51a0209",
					"text": "777",
					"size": 16
				}
			]
		}
	],
	"success": true
}
 ```

#### 1.2、 查询人名牌

| URL    | http://host:port/nameCard/list |
| ------ | --------------------------------- |
| Method | get                               |

无请求参数: 

返回数据:

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| cardId           | string |  id     |      
| type           | string |  类型 1 水牌 2 人名牌     |      
| createTime           | string |  创建时间     |      
| operator           | string |  操作人     |      
| name           | string |  模板名称     |      
| url            | string |  模板地址     |     
| fontColor      | string |  字体颜色     |     
| fontList       | string |  字体列表     |     
| + cardId         | string |  绑定的模板id        |     
| + text         | string |  文本        |     
| + size         | num |  像素值       |    
 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [
		{
			"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
			"type": "1",
			"createTime": "2023-04-23 15:13:52",
			"name": "李四",
			"url": "5646546464645",
			"fontColor": "#fff",
			"operator": "温良伟",
			"delFlag": 0,
			"fontList": [
				{
					"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
					"text": "65495466464646464646464656466664",
					"size": 16
				},
				{
					"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
					"text": "777",
					"size": 16
				}
			]
		},
		{
			"cardId": "f4f02c68-4dee-47f1-b5f2-f12db51a0209",
			"type": "1",
			"createTime": "2023-04-23 16:22:42",
			"name": "李四",
			"url": "5646546464645",
			"fontColor": "#fff",
			"operator": "温良伟",
			"delFlag": 0,
			"fontList": [
				{
					"cardId": "f4f02c68-4dee-47f1-b5f2-f12db51a0209",
					"text": "666",
					"size": 16
				},
				{
					"cardId": "f4f02c68-4dee-47f1-b5f2-f12db51a0209",
					"text": "777",
					"size": 16
				}
			]
		}
	],
	"success": true
}
 ```


### 3、   删除水牌/人名牌

#### 1.1、 删除水牌

| URL    | http://host:port/waterCard/del/{cardId} |
| ------ | --------------------------------- |
| Method | delete                             |

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1,
	"success": true
}
 ```

#### 1.2、 删除人名牌

| URL    | http://host:port/nameCard/del/{cardId} |
| ------ | --------------------------------- |
| Method | delete                             |

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1,
	"success": true
}
 ```



### 4、   修改水牌/人名牌

#### 1.1、 修改水牌

| URL    | http://host:port/waterCard/put |
| ------ | --------------------------------- |
| Method | Put                             |

请求数据:

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| cardId           | string |  id     |      
| type             | string |  类型 1 水牌 2 人名牌     |      
| createTime       | string |  创建时间     |      
| operator         | string |  操作人     |      
| name           | string |  模板名称     |      
| url            | string |  模板地址     |     
| fontColor      | string |  字体颜色     |     
| fontList       | string |  字体列表     |     
| + cardId         | string |  绑定的模板id        |     
| + text         | string |  文本        |     
| + size         | num |  像素值       |   

 ```json
{
			"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
			"type": "1",
			"createTime": "2023-04-23 15:13:52",
			"name": "李四",
			"url": "5646546464645",
			"fontColor": "#fff",
			"operator": "温良伟",
			"delFlag": 0,
			"fontList": [
				{
					"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
					"text": "65495466464646464646464656466664",
					"size": 16
				},
				{
					"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
					"text": "777",
					"size": 16
				}
			]
		}
 ```

#### 1.1、 修改人名牌

| URL    | http://host:port/nameCard/put |
| ------ | --------------------------------- |
| Method | Put                             |

请求数据:

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| cardId           | string |  id     |      
| type             | string |  类型 1 水牌 2 人名牌     |      
| createTime       | string |  创建时间     |      
| operator         | string |  操作人     |      
| name           | string |  模板名称     |      
| url            | string |  模板地址     |     
| fontColor      | string |  字体颜色     |     
| fontList       | string |  字体列表     |     
| + cardId         | string |  绑定的模板id        |     
| + text         | string |  文本        |     
| + size         | num |  像素值       |   

 ```json
{
			"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
			"type": "1",
			"createTime": "2023-04-23 15:13:52",
			"name": "李四",
			"url": "5646546464645",
			"fontColor": "#fff",
			"operator": "温良伟",
			"delFlag": 0,
			"fontList": [
				{
					"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
					"text": "65495466464646464646464656466664",
					"size": 16
				},
				{
					"cardId": "ec22bbcc-09da-4570-87fe-2f1f2a1ccdc6",
					"text": "777",
					"size": 16
				}
			]
		}
 ```


### 5、   会议图表

#### 1.1、 查询公司的预约总览

| URL    | http://host:port/base/statistics |
| ------ | --------------------------------- |
| Method | get                             |

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| pageNum           | num |       |      
| pageSize             | num |       |      
| name                | string |  公司名称     |    

响应

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| enterprise            | string |   公司名     |      
| count                 | num    |      预约次数 |      
| actuallyPaid          | double |  总金额      |  
| totalTime             | string |  总时间      |  
| minTime               | null   |             |  

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"recordsCount": 28,
		"pages": 3,
		"datalist": [
			{
				"enterprise": "太原SDF",
				"count": 1,
				"minTime": null,
				"actuallyPaid": 1888,
				"totalTime": "1小时 15分钟"
			},
			{
				"enterprise": "某某某",
				"count": 1,
				"minTime": null,
				"actuallyPaid": 50,
				"totalTime": "1小时 0分钟"
			},
			{
				"enterprise": "师德师风",
				"count": 1,
				"minTime": null,
				"actuallyPaid": 0,
				"totalTime": "1小时 30分钟"
			},
			{
				"enterprise": "北京协成致远",
				"count": 2,
				"minTime": null,
				"actuallyPaid": 66,
				"totalTime": "0小时 30分钟"
			},
			{
				"enterprise": "二",
				"count": 1,
				"minTime": null,
				"actuallyPaid": 12312,
				"totalTime": "0小时 45分钟"
			},
			{
				"enterprise": "科技经济",
				"count": 2,
				"minTime": null,
				"actuallyPaid": 22222,
				"totalTime": "1小时 0分钟"
			},
			{
				"enterprise": "西安SFSDF",
				"count": 1,
				"minTime": null,
				"actuallyPaid": 6666,
				"totalTime": "1小时 45分钟"
			},
			{
				"enterprise": "213",
				"count": 1,
				"minTime": null,
				"actuallyPaid": 0,
				"totalTime": "1小时 0分钟"
			},
			{
				"enterprise": "上海SFSD有限公司",
				"count": 1,
				"minTime": null,
				"actuallyPaid": 0,
				"totalTime": "1小时 45分钟"
			},
			{
				"enterprise": "辽宁史蒂夫",
				"count": 1,
				"minTime": null,
				"actuallyPaid": 1500,
				"totalTime": "3小时 30分钟"
			}
		],
		"currentRecordsCount": 10,
		"pageSize": 10,
		"pageNum": 1,
		"navigatePageNums": [
			1,
			2,
			3
		]
	},
	"success": true
}
 ```  

#### 1.2、 查询公司的详情总览

| URL    | http://host:port/base/companyView |
| ------ | --------------------------------- |
| Method | get                             |

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| name                | string |  公司名称     |    

响应

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| basic            | arr |   基础信息     |       
| + enterprise            | String |   所属企业     |       
| + count            | Integer |   预约次数     |       
| + minTime            | Integer |   会议时长     |       
| + actuallyPaid            | Double |   实际金额     |       
| + totalTime            | Double |   总时长展示     |       
| expense            | arr |   费用图表     |       
| + node            | String |   时间节点     |  
| + conference            | Double |   会议消费     |  
| + eat            | Double |   吃饭消费   0 = null  |  
| + live            | Double |   住宿消费  0 = null   |  
| company            | arr |   公司维度     |     
| + meetingId            | String |  会议id   |  
| + serialNumber            | String |  会议编号   |  
| + roomName            | String |  会议室名称   |  
| + meetingTheme            | String |  会议主题   |  
| + useState            | String |  使用状态   |  
| + openDate            | String |  会议开始时间   |  
| + live            | String |  住需求   |  
| + eat            | String |  吃需求   |  
| personnel            | arr |   人员维度     |    
| + name            | String |  人名   |  
| + mobile            | String |  手机   |  
| + number            | String |  参会次数   |  

 ``` json 
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"company": [
			{
				"meetingId": "04c2274a45b84fbd899b56b1a267a1d8",
				"serialNumber": "HY202304211627513d3",
				"roomName": "萨达",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-01-01 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "0832b59815524f1996c651b6f14328e7",
				"serialNumber": "HY20230320145136f16",
				"roomName": "点点滴滴1",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-20 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "159b231b65584331923eeacdcaf2d814",
				"serialNumber": "HY2023031314385718a",
				"roomName": "第五会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-14 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "191e73c91c6c456cb46a65cee1eaeae8",
				"serialNumber": "HY20230313142902ec2",
				"roomName": "第三会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-13 15:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "1e586ee5a2d5499187527084c3c01172",
				"serialNumber": "HY20230311115816fcd",
				"roomName": "第二会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-11 10:43:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "21b0df6bcedf4b248c01a6f44eded7ad",
				"serialNumber": "HY20230310180159839",
				"roomName": "第三会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-10 23:49:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "2408dad6abb24673a1897d7be504317c",
				"serialNumber": "HY20230315164630b83",
				"roomName": "123",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-16 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "2874c3af1f8b4f3587469c6417df2f25",
				"serialNumber": "HY20230314152838d11",
				"roomName": "第一会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-14 15:05:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "2eb5f48693e0410f8ab1fe0d6249f011",
				"serialNumber": "HY202303101040112a9",
				"roomName": "第二会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-10 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "419af77508a84e6c890c733f3c48a14f",
				"serialNumber": "HY202302271102145a1",
				"roomName": "第四会议室",
				"meetingTheme": "666",
				"useState": null,
				"openDate": "2023-02-28 11:20:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "47531532dd55418aadb5383838439531",
				"serialNumber": "HY20230317151511b97",
				"roomName": "点点滴滴1",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-17 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "5009e4d5c4384bbdb10240a1f08a3a5f",
				"serialNumber": "HY20230418150137150",
				"roomName": "萨达",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-04-20 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "51c7ed8d145a4f29a37163f15a7c47db",
				"serialNumber": "HY20230313141632886",
				"roomName": "第四会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-14 17:22:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "51ffff67a8da4cdc91a5f0f9efc3c68f",
				"serialNumber": "HY20230313144031e73",
				"roomName": "会议室测试",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-13 16:28:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "598c398ae8a04f65aca9425d8934df5d",
				"serialNumber": "HY20230310150906f4e",
				"roomName": "第三会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-17 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "5c7cdb0f26c44e6da5c9d8f102ffdd5e",
				"serialNumber": "HY20230313144119815",
				"roomName": "会议室名称",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-13 14:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "5e107b7164c143f4bfd91c82a626b822",
				"serialNumber": "HY20230310180252e93",
				"roomName": "会议室名称",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-10 23:52:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "5f4716f866e14f4da3f348bdffd362c8",
				"serialNumber": "HY20230315163956645",
				"roomName": "点点滴滴1",
				"meetingTheme": "221",
				"useState": null,
				"openDate": "2023-03-16 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "645f1af98fde4c1394649977dc5f179c",
				"serialNumber": "HY20230313141432d11",
				"roomName": "6666",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-15 17:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "65b6a9b753e9472380a1f74ed34060c4",
				"serialNumber": "HY20230310134856781",
				"roomName": "第四会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-11 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "7c7f0a6ffc804b2eac507d1ec1ed1e05",
				"serialNumber": "HY20230310134337f9d",
				"roomName": "第五会议室",
				"meetingTheme": "会议1",
				"useState": null,
				"openDate": "2023-03-11 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "7f24c6f781534dc2965350d683d6f764",
				"serialNumber": "HY20230310145104f9b",
				"roomName": "第二会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-11 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "80a526c89edd4edeac61792fb4b15fda",
				"serialNumber": "HY2023031517431631f",
				"roomName": "萨达",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-15 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "84cc2b67e5bb4976bed8c6b7325a888d",
				"serialNumber": "HY20230315174247af7",
				"roomName": "点点滴滴1",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-15 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "8badee8b0cca4fb59c4cde74fcfc2f8a",
				"serialNumber": "HY202303101752308b9",
				"roomName": "第三会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-31 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "8bd7dbabdba04183a4c6ce68fe8c907a",
				"serialNumber": "HY20230313144920e49",
				"roomName": "会议室测试",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-13 15:55:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "8d322443fb234505b8d13186825f88ac",
				"serialNumber": "HY202303101505574ad",
				"roomName": "第三会议室",
				"meetingTheme": "的 的",
				"useState": null,
				"openDate": "2023-03-11 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "9320533fcaf545b1bd24f0d20fdb6937",
				"serialNumber": "HY202303131355226ad",
				"roomName": "第三会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-23 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "9744ae3e39024c40a06529b1b40ad7a9",
				"serialNumber": "HY2023041815171825a",
				"roomName": "123",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-04-18 15:19:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "a6c78834012c4e2fb876580badde8a7c",
				"serialNumber": "HY20230313143217820",
				"roomName": "第二会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-14 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "a83b04c8b5e148f4bbafa32d25c18ffb",
				"serialNumber": "HY202303131404045f3",
				"roomName": "第四会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-31 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "b1a9383cc42b4666b8c0cd2ae646a626",
				"serialNumber": "HY2023031018073675f",
				"roomName": "第二会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-11 11:44:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "b26793446aa74b549a93e927e171e510",
				"serialNumber": "HY20230313143230b5d",
				"roomName": "会议室一",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-14 17:16:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "b55fa05c3f2f45ec8faee5ddea0ca774",
				"serialNumber": "HY20230310175150b60",
				"roomName": "第一会议室",
				"meetingTheme": "221",
				"useState": null,
				"openDate": "2023-03-18 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "c60678aa2dee49c997ae780d953bb879",
				"serialNumber": "HY20230310150826837",
				"roomName": "第二会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-18 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "c6ad10eedab84561a751136f9da299de",
				"serialNumber": "HY202303151630328df",
				"roomName": "123131451231",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-15 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "d051673d89f8469f8e121f889f927f16",
				"serialNumber": "HY2023031516104436d",
				"roomName": "测试",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-15 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "d5e6596863a74ad7b2a69d6b20bf52ef",
				"serialNumber": "HY20230310110037e50",
				"roomName": "第一会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-10 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "da8851f5db9446d598d06f585cfc8748",
				"serialNumber": "HY20230315165556757",
				"roomName": "萨达",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-15 16:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "dda494c1037143eab4b77065e12e3e4f",
				"serialNumber": "HY20230310175104aad",
				"roomName": "第三会议室",
				"meetingTheme": "221",
				"useState": null,
				"openDate": "2023-03-14 17:25:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "dfac9e960011413fbc4163554d204cc2",
				"serialNumber": "HY2023031110363446b",
				"roomName": "第二会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-11 10:10:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "e39bee2418cd4cb29271eca6d23a64dc",
				"serialNumber": "HY202304191516226bd",
				"roomName": "第一会议室",
				"meetingTheme": "233",
				"useState": null,
				"openDate": "2023-04-19 15:16:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "e8787f01048f4682a6cf7cee2d4ba3cc",
				"serialNumber": "HY20230311130111069",
				"roomName": "第五会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-11 13:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "e8c85a7fb4ef407a9ee4321d5c268256",
				"serialNumber": "HY20230316085352b0d",
				"roomName": "点点滴滴1",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-16 09:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "f173490d5ab04c3d849ccd7244ef7b91",
				"serialNumber": "HY20230310100035793",
				"roomName": "第四会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-10 00:00:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "f5d90e5655c9413eb1a420451edebae2",
				"serialNumber": "HY20230314161857799",
				"roomName": "第六会议室",
				"meetingTheme": "无主题",
				"useState": null,
				"openDate": "2023-03-14 16:20:00",
				"live": null,
				"eat": null
			},
			{
				"meetingId": "f7716d7be8b14b59bf751d3daabfaed7",
				"serialNumber": "HY202303101344438f8",
				"roomName": "第一会议室",
				"meetingTheme": "会议一",
				"useState": null,
				"openDate": "2023-03-11 00:00:00",
				"live": null,
				"eat": null
			}
		],
		"personnel": [
			{
				"name": " 21 ",
				"mobile": " 15010666429 ",
				"number": 1
			},
			{
				"name": "李晨博",
				"mobile": "13137480326",
				"number": 1
			},
			{
				"name": "test01",
				"mobile": "13192697401",
				"number": 1
			},
			{
				"name": "刘笑含",
				"mobile": "13261856528",
				"number": 1
			},
			{
				"name": "于宝旺",
				"mobile": "13820322666",
				"number": 1
			},
			{
				"name": "1",
				"mobile": "15010101010",
				"number": 1
			},
			{
				"name": "测试",
				"mobile": "15010301010",
				"number": 2
			},
			{
				"name": "李军",
				"mobile": "15010301296",
				"number": 8
			},
			{
				"name": "21",
				"mobile": "15010666429",
				"number": 5
			},
			{
				"name": "张洪国",
				"mobile": "15612166039",
				"number": 1
			},
			{
				"name": "admin",
				"mobile": "15632121059",
				"number": 1
			},
			{
				"name": "ce",
				"mobile": "15632121094",
				"number": 4
			},
			{
				"name": "wlw",
				"mobile": "158",
				"number": 2
			},
			{
				"name": "asdasd",
				"mobile": "15812312312",
				"number": 1
			},
			{
				"name": "付宇玲",
				"mobile": "18001030971",
				"number": 21
			},
			{
				"name": "居心燕",
				"mobile": "18001172002",
				"number": 15
			},
			{
				"name": "王涛",
				"mobile": "18001172024",
				"number": 1
			},
			{
				"name": "王婧",
				"mobile": "18001172055",
				"number": 4
			},
			{
				"name": "郭思伽",
				"mobile": "18001172071",
				"number": 4
			},
			{
				"name": "郝宪燕",
				"mobile": "18001172072",
				"number": 18
			},
			{
				"name": "周丽",
				"mobile": "18500556986",
				"number": 4
			},
			{
				"name": "栗寅",
				"mobile": "18910230899",
				"number": 1
			}
		],
		"basic": {
			"enterprise": "测试企业",
			"count": 47,
			"minTime": null,
			"actuallyPaid": 7522.65333,
			"totalTime": "55小时 30分钟"
		},
		"expense": [
			{
				"node": "2023-03-31",
				"conference": 0,
				"eat": 0,
				"live": 0
			},
			{
				"node": "2023-04-18",
				"conference": 2323,
				"eat": 0,
				"live": 0
			},
			{
				"node": "2023-04-19",
				"conference": 0,
				"eat": 0,
				"live": 0
			},
			{
				"node": "2023-04-20",
				"conference": 2323,
				"eat": 0,
				"live": 0
			}
		]
	},
	"success": true
}
 ```  
#### 1.3、 查询访客或者员工的手机号照片等信息

| URL    | http://host:port/base/attendTheMeeting |
| ------ | --------------------------------- |
| Method | get                             |

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| type                | string |  face/visitor   园区通行人脸/访客     |  

响应  SeatTable

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| name                | string |  名字     |  
| mobile              | string |  手机号     |  
| url                 | string |  照片地址     |  

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [
		{
			"name": "小贾",
			"mobile": "15239864152",
			"url": "http://192.165.1.62:9881/group1/M00/00/2D/wKUBPmQ8xEuAfVKQAAELKS55Kj0967.jpg"
		},
		{
			"name": "访客测试",
			"mobile": "18230201157",
			"url": "http://192.165.1.62:9881/group1/M00/00/2F/wKUBPmQ-GMeABmpEAAFplblNS5k704.jpg"
		}
	],
	"success": true
}
 ```  
  
  
  #### 1.4、 会议详情信息导出
  
  | URL    | http://127.0.0.1:8011/base/downloadExcel |
  | ------ | --------------------------------- |
  | Method | get                             |
  
  | 参数           | 类型   | 说明     |    
  | ------------- | ------ | -------- |
  | type                | string |  1/2   公司/参会人员 维度 ！！！字符串类型     |  
  | name                | string |  公司企业     |  
  
  
  #### 1.5、 会议签到查看按钮
  
  | URL    | http://127.0.0.1:8011/Meeting/signIn |
  | ------ | --------------------------------- |
  | Method | get                             |
  请求
  
   | 参数           | 类型   | 说明     |    
   | ------------- | ------ | -------- |
   | meetingId     | string |  会议主键   |  
   
  响应
  
   | 参数           | 类型   | 说明     |    
   | ------------- | ------ | -------- |
   | openDate        | string |  会议开始时间   |  
   | number        | string |  总人数   |  
   | failSignIn        | arr |  未签到   |  
   |  + name        | string |  未签到人名   |  
   | signIn        | arr |  签到   |  
   |  + name        | string |  签到人名   |  
   |  + signInDate   | string |  签到时间   |  

      
      
```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"number": 0,
		"signIn": [],
		"failSignIn": [],
		"openDate": null
	},
	"success": true
}
```  


### 6、   附加功能

#### 1.1、 校验会议室是否冲突

| URL    | http://host:port/Meeting/conflictDate |
| ------ | --------------------------------- |
| Method | get                             |

| 参数           | 类型                  | 说明                |    
| ------------- | -------------------- |-------------------- |
| roomId        | string               |    会议室id                 |      
| openDate      | string               |    开始时间    yyyy-MM-dd HH:mm:ss 格式            |      
| endDate       | string               |    结束时间    yyyy-MM-dd HH:mm:ss 格式            |    
| meetingId     | string               |    会议id     用于编辑会议                          |    

响应:

```json
{
	"code": 0,
	"message": null,
	"exception": null,
	"data": true,
	"success": false
}  
```  
```json
{
	"code": 1,
	"message": "该时间段会议室正在使用中!",
	"exception": null,
	"data": null,
	"success": false
}  
```  


#### 1.2、 校验参会人是否冲突

| URL    | http://host:port/Meeting/conflictPeople              |
| ------ | ----------------------------------------------------- |
| Method | get                                                    |

| 参数           | 类型                  | 说明                       |    
| ------------- | -------------------- |----------------------------|
| name          | string               |    参会人姓名                 |      
| openDate      | string               |    开始时间    yyyy-MM-dd HH:mm:ss 格式            |      
| endDate       | string               |    结束时间    yyyy-MM-dd HH:mm:ss 格式            |    
| mobile        | string               |    参会人手机号                              |    

响应:

```json
{
	"code": 0,
	"message": null,
	"exception": null,
	"data": true,
	"success": false
}  
```  
```json
{
	"code": 1,
	"message": "李晨博在2023-05-27 08:00:00 - 2023-05-27 12:00:00 参加了其他会议!",
	"exception": null,
	"data": null,
	"success": false
}  
```  