# 会议室图表统计-API开发文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录 |
| --------- | ------ | ---------- | ------------ |
| v0.0.1    | 温良伟 | 2023.02.14 | 初始化文档   |


#### 1.1 查询各个会议室的使用次数  饼图

 | URL    | http://host:port/statistics/pie |
 | --------- |  ------------ |
 |Method | get       |             |

请求参数：
    
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
    | regionId |  varchar(20) | 园区id |
   | timing |  varchar(20) | 过去的时间 1周 1月.. |    
  | openDate |  varchar(20) | 时间 |
  | endDate |  varchar(20) | 时间 |

返回示例：
```json
{
  "code": 0,
  "message": "请求成功！",
  "data": "待定"
}
```    
 

#### 1.2 查询某一个会议室的所有会议的使用次数  柱状图

 | URL    | http://host:port/statistics/histogram |
 | --------- |  ------------ |
 |Method | get       |             |
 
请求参数：
    
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
   | regionId |  varchar(20) | 园区id |
   | timing |  varchar(20) | 过去的时间 1周 1月.. |    
  | openDate |  varchar(20) | 时间 |
  | endDate |  varchar(20) | 时间 |

   
返回示例：
```json
{
  "code": 0,
  "message": "请求成功！",
  "data": "待定"
}
```    
 
 #### 1.3 通过获取最近一周的 所有会议室预约次数 折线图
 
  | URL    | http://host:port/statistics/lineChart |
  | --------- |  ------------ |
  |Method | get       |             |
  
请求参数：
    
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
   
返回示例：
```json
{
  "code": 0,
  "message": "请求成功！",
  "data": "待定"
}
```       
     