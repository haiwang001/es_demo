# 会议查询-API开发文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录 |
| --------- | ------ | ---------- | ------------ |
| v0.0.1    | 温良伟 | 2023.02.14 | 初始化文档   |
| v0.0.2    | 温良伟 | 2023.05.08 | 会议二期字段更新   |

## 一、表结构

###  会议表  (meeting_info) 

| 数据库字段                 |   代码字段             | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ----------------------| ----------------------  | ---------------------- | ---------------- |
| meeting_id               |  meetingId            | varchar(20)             | 是                     | 主键ID           |
| meeting_source           |  MeetingSource        | boolean                 | 是                     | 会议来源          |
| appointment_name         | appointmentName	   | varchar(20)             | 是                     | 预约会议的人名    |
| appointment_mobile       | appointmentMobile	   | varchar(20)             | 是                     | 预约会议的手机号  |
| open_date                | openDate	           | varchar(20)             | 是                     | 会议开始时间      |
| min_time                 | minTime	           | varchar(20)             | 是                     | 是               |  
| number_of_people         | NumberOfPeople	       | varchar(20)             | 是                     | 会议人数         |
| enterprise               | enterprise	           | varchar(20)             | 是                     | 所属企业         |
| room_id                  | roomId	               | varchar(20)             | 是                     | 会议室id  |
| room_name                | roomName	           | varchar(20)             | 是                     | 会议室名称  |
| meeting_theme            | MeetingTheme	       | varchar(20)             | 是                     | 会议主题  |
| room_scope               | roomScope             | int(100)                | 否                     | 会议使用者分类 默认0 内部会议 1外部人员会议  |
| room_type                | roomType              | varchar(100)            | 是                     | 会议室类型   |
| prepay                   | prepay                | varchar(100)            | 是                     | 预付金额   |
| actually_paid            | actuallyPaid          | varchar(100)            | 是                     | 实付金额   |
| is_sign_in               | isSignIn              | varchar(100)            | 是                     | 是否签到   |
| remind_time              | remindTime            | varchar(100)            | 是                     | 提醒时间   |
| is_accommodation         | isAccommodation       | varchar(100)            | 是                     | 住宿需求状态   |
| goods_list               | goodsList             | array(100)              | 是                     | 会议物品列表   |
| push_method              | pushMethod            | int(100)                | 是                     |  推送方式  1个人消息 2个人代办 3短信  |
| notifier_list            | notifierList          | array(100)              | 是                     |  会议通知的人列表  |
| replenish_notifier_list  | replenishNotifierList | array(100)              | 是                     |  会议成员补录  |
| remark                   | remark                | array(100)              | 是                     |  备注信息  |
| serial_Number            | serialNumber          | varchar(20)             | 是                     | 会议编号   | 
| create_time              | createTime            | varchar(255)            | 是                     | 创建时间 |
| update_time              | updateTime            | varchar(255)            |是                      | 修改时间 |
| operator                 |operator               | varchar(255)            | 是                     | 操作人 |
| del_flag                 | delFlag               | varchar(10)             | 是                     | 删除标记 |
| use_state                | useState              | varchar(10)             | 是                     | 使用状态   |
| stars                    |stars                  | bigint(20)              | 是                     | 星星数量   |
| comment                  |comment                | varchar(20)             | 是                     | 评价   |
| stayArea                 |stay_area              | varchar(20)             | 否                     | 住宿的园区id   |
| stayAreaLabel            |stay_area_label        | varchar(20)             | 否                     | 住宿的园区展示   |
| housingNum               |housing_num            | number                  | 否                     | 房间数量   |
| housingType              |housing_type           | number                  | 否                     | 房间类型  1 标间 2 大床房 3 商务房  |
| occupancyDate            |occupancy_date         | date                    | 否                     | 入住时间  |
| accommodationAmount      |accommodation_amount   | Double                  | 否                     | 住宿金额  |
| haveMealNum              |have_meal_num          | number                  | 否                     | 就餐人数  |
| dailyMeal                |daily_meal             | json                    | 否                     | 1 早  2 中  3 晚  |
| eatAmount                |eat_amount             | Double                  | 否                     | 餐饮金额  |
###   meeting_member(会议人员表)  
| 数据库字段                |   代码字段              | 字段类型                 | 是否必填                 | 描述              | 
| ----------------------  | ---------------------- | ----------------------  | ----------------------  | ---------------- |
| id                      |id                      | bigint(20)              | 是                      | 主键ID   |
| del_flag                | delFlag                | number(10)              | 是                      | 删除标记 默认0 |
| meeting_id              |meetingId               | varchar(255)            | 是                      | 绑定会议id   |
| typeDisplay             |type_display            | varchar(20)             | 是                      | 人员分类 内部/外部 |
| imgUrl                  |img_url                 | varchar(255)            | 是                      | 参会人照片路径 |
| signIn                  |sign_in                 | number                  | 是                      | 签到状态  0/1  未签到/签到  默认0 |
| signInDate              |sign_in_date            | date                    | 是                      | 签到时间 |
| name                    |name                    | varchar(100)            | 是                      | 参会人名   |
| mobile                  |mobile                  | varchar(100)            | 是                      | 参会人手机号   |
| seatCode                |seat_code               | varchar(255)            | 是                      | 参会人座位编号  |
| faceUrl                 |face_url                | varchar(255)            | 是                     | 在会管的人脸照片路径 用于删除  |
| deviceIndexCode         |device_index_code       | varchar(255)            | 是                     | 在会管的设备 编码 用于删除   |
| deleteRecord            |delete_record           | number                  | 是                     | 会管平台人员删除状态   0 未删除  1 删除 避免重复执行删除  |
| notice                  |notice                  | varchar(100)            | 是                     |  发送通知   已发送/未发送  |

###   meeting_info_goods(会议物品表)   
| 数据库字段                |   代码字段              | 字段类型                | 是否必填                | 描述              | 
| ----------------------  | ---------------------- | ----------------------  | ---------------------- | ---------------- |
| id                      |id                      | bigint(20)              | 是                     | 主键ID   |
| m_id                    |mId                     | bigint(20)              | 是                     | 绑定的会议id   |
| goods_name              |goodsName               | varchar(20)             | 是                     | 物品名称   |
| goods_num               |goodsNum                | varchar(20)             | 是                     | 物品数量   |
| goods_uni_valence       |goodsUniValence         | varchar(20)             | 是                     | 物品单价   |

## 二、接口
### 1，首页

#### 1.1 会议新增

 | URL    | http://host:port/Meeting/add |
 | --------- |  ------------ |
 |Method | post       |             |
 
 请求参数：
 
 | 参数                 | 类型             | 说明              |
 | ----------------    | ---------------- | ---------------- |
 | appointmentName     |  varchar(20)     | 预约人 |
 | appointmentMobile   |  varchar(20)     | 预约人手机号 |
 | openDate            |  date            | 会议时间 |
 | endDate             |  date            | 结束时间 |
 | NumberOfPeople      |  varchar(20)     | 会议人数 |
 | enterprise          |  varchar(20)     | 企业名称 |
 | roomId              |  varchar(20)     | 会议室编号 |
 | roomName            |  varchar(20)     | 会议室名称 |
 | MeetingTheme        |  varchar(20)     | 会议主题 |
 | roomScope           |  varchar(20)     | 会议室类型 |
 | roomType            |  varchar(20)     | 会议分类 |
 | prepay              |  varchar(20)     | 预付金额 |
 | actuallyPaid        |  varchar(20)     | 实付金额 |
 | isSignIn            |  varchar(20)     | 是否签到 |
 | remindTime          |  varchar(20)     | 会议提醒时间 |
 | isAccommodation     |  boo             | 食宿需求 |
 | goodsList           |  array           | 会议物品列表 |
 | replenishNotifierList |  array           | 参会人员 |
 | + seatCode          |  string          | 座位编码 |
 | + typeDisplay       |  string          | 分类展示 |
 | + name              |  string          | 名字 |
 | + mobile            |  string          | 手机号 |
 | + imgUrl            |  string          | 人像路径 |
 | pushMethod          |  varchar(20)     | 推送方式 |
 | remark              |  varchar(20)     | 备注 |
 | housingType         |  num             | 住宿房屋类型  1 标间 2 大床房 3 商务房 |
 | housingNum          |  num             | 预订房屋数量 |
 | haveMealNum         |  num             | 就餐人数 |
 | stayArea            |  num             | 住宿的园区id |
 | stayAreaLabel       |  字符串           | 住宿的园区展示 |
 | dailyMealMap        |  map             | 1早 TRUE/false 2中TRUE/false 3晚 TRUE/false  false不吃 true吃   |
 

```
{
  "meetingSource": 1,
  "appointmentName": "张三",
  "appointmentMobile": "158",
  "openDate": "2023-02-20 12:15:00",
  "endDate": "2023-02-20 18:30:00",
  "numberOfPeople": 23,
  "enterprise": "北京协成致远",
  "roomId": "e796b551fee84226a72f77e805767a98",
  "roomName": "第二会议室",
  "meetingTheme": "国庆主题",
  "roomScope": true,
  "roomType": "t",
  "prepay": 66.66,
  "actuallyPaid": 66.00,
  "isSignIn": false,
  "remindTime": 2,
  "pushMethod": 2,
  "remark": "备注",
  "operator": "wlw",
  "goodsList": [
    {
      "goodsId": 6666,
      "goodsName": "未知物品",
      "amount": 30,
      "uniValence": 66.66
    }
  ]
  "replenishNotifierList": [
    {
      "seatCode": "A-04",
      "typeDisplay": "内部",
      "name": "张三",
      "mobile": "158",
      "imgUrl": "56464646"
    },
    {
      "seatCode": "A-09",
      "typeDisplay": "内部",
      "name": "张三",
      "mobile": "158",
      "imgUrl": "56464646"
    }
  ]
}
```

返回示例：
```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 0
}
```

#### 1.1  移动端列表查询

 | URL    | http://host:port/Meeting/mobileList |
 | --------- |  ------------ |
 |Method | get       |             |
 
 
 请求参数：
 
 | 参数     | 类型    | 说明       |
 | -------- | ------ | ---------- |
 | staffId |  varchar(20) | 员工id  必传|
 | useState |  num | 1 未开始 2进行中  3已结束 4 已经取消 | 
 
 
 
 ``` json
1
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"msg": "员工参数错误！或者服务器内部错误! 请联系管理员",
		"code": 500
	},
	"success": true
}
2
 {
 	"code": 0,
 	"message": "请求成功！",
 	"exception": null,
 	"data": {
 		"recordsCount": 2,
 		"pages": 1,
 		"datalist": [
 			{
 				"meetingTheme": "11",
 				"roomName": "第三会议室",
 				"openDate": "2023-03-09 11:31:00",
 				"appointmentName": "贾",
 				"useState": 4
 			},
 			{
 				"meetingTheme": "11",
 				"roomName": "第三会议室",
 				"openDate": "2023-03-02 11:31:00",
 				"appointmentName": "贾",
 				"useState": 3
 			}
 		],
 		"currentRecordsCount": 2,
 		"pageSize": 10,
 		"pageNum": 1,
 		"navigatePageNums": [
 			1
 		]
 	},
 	"success": true
 }

3
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"msg": "请789绑定会议预约手机号",
		"code": 500
	},
	"success": true
}
 ```

#### 1.2 下载人员模板文件
 | URL    | http://host:port/base/excelModel |
 | --------- |  ------------ |
 |Method | get       |             |
 
 相应一个文件

#### 1.3  上传excel 
 | URL    | http://host:port/Meeting/import |
 | --------- |  ------------ |
 |Method | get       |             |
 
  返回示例：
  ```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [
		{
			"name": "张三",
			"mobile": 158
		},
		{
			"name": "李四",
			"mobile": 159
		}
	]
}
  ```

#### 1.4 会议编辑

 | URL    | http://host:port/Meeting/update |
 | --------- |  ------------ |
 |Method | put       |             |
 
 
 请求参数：
 
 | 参数     | 类型    | 说明       |
 | -------- | ------ | ---------- |
 | meetingId |  varchar(20) | 会议id |
 | appointmentName |  varchar(20) | 预约人 |
 | appointmentMobile |  varchar(20) | 预约人手机号 |
 | openDate |  varchar(20) | 会议时间 |
 | minTime |  varchar(20) | 会议时长 |
 | NumberOfPeople |  varchar(20) | 会议人数 |
 | enterprise |  varchar(20) | 企业名称 |
 | roomId |  varchar(20) | 会议室编号 |
 | roomName |  varchar(20) | 会议室名称 |
 | MeetingTheme |  varchar(20) | 会议主题 |
 | roomScope |  varchar(20) | 会议室类型 |
 | roomType |  varchar(20) | 会议分类 |
 | prepay |  varchar(20) | 预付金额 |
 | actuallyPaid |  varchar(20) | 实付金额 |
 | isSignIn |  varchar(20) | 是否签到 |
 | remindTime |  varchar(20) | 会议提醒时间 |
 | isAccommodation |  array | 食宿需求 |
 | goodsList |  array | 会议物品列表 |
 | meetingPeoples |  array | 会议通知人列表 |
 | pushMethod |  varchar(20) | 推送方式 |
 | MemberList |  array | 会议通知人补录列表 |
 | remark |  varchar(20) | 备注 |

```
{
    "meetingId":"bd80ac9ae3ae4835a6ccfa69fed304e7",
    "meetingSource":true,
    "appointmentName":"预约人",
    "appointmentMobile":"158",
    "openDate":"2023-02-20 11:50:43",
    "minTime":15,
    "numberOfPeople":23,
    "enterprise":"北京协成致远",
    "roomId":"会议室id",
    "roomName":"会议室名",
    "meetingTheme":"国庆主题",
    "roomScope":true,
   "roomType":"t",
   "prepay":66.66,
   "actuallyPaid":66.00,
   "isSignIn":false,
   "remindTime":2,
   "isAccommodation":false,
   "pushMethod":2,
   "remark":"备注",
   "operator":"wlw",
   "goodsList":[
        {
           "goodsId":6666,
           "goodsName":"未知物品",
           "amount":30,
           "uniValence":66.66
        }
   ],
   "notifierList" :[
     {
        "name":"张三",
        "mobile":"158"
     },
        {
        "name":"张三",
        "mobile":"158"
     }
   ],
   "replenishNotifierList":[
       {
        "name":"张三",
        "mobile":"158"
     },
        {
        "name":"张三",
        "mobile":"158"
     }

   ]
}
```
 
 返回示例：
 ```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1
}
 ```
 
 #### 1.5 会议状态修改 
 
描述:  前端 未开始0    只能点击取消 传入状态4
           已经开始1  只能点击提前结束 传入3
 
  | URL    | http://host:port/Meeting/updateState |
  | --------- |  ------------ |
  |Method | put       |             |
  
请求参数：
    
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
   | meetingId |  varchar(20) | 会议id |
   | useState |  varchar(20) | 会议状态 |
   
 请求：
 ```
{
    "meetingId":"7d4c4e2b075f4ef9bc206d334d93711e",
    "useState":4
}
 ```
 返回示例：
 ```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1
}
 ```   
   
  
#### 1.6 会议添加评论
  
   | URL    | http://host:port/Meeting/comment |
   | --------- |  ------------ |
   |Method | post       |             | 
   
请求参数：
    
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
   | meetingId | 字符串 | 会议id |
   | stars | 整数 | 星星数量 |   
   | comment |  字符串 | 评价消息 |   

 请求：
 ```
{
    "meetingId":"7d4c4e2b075f4ef9bc206d334d93711e",
        "stars":4,
    "comment":"会议很完美"
}
 ```
 返回示例：
 ```
{
    	"code": 0,
    	"message": "请求成功！",
    	"exception": null,
    	"data": 1
}
 ```   

   
   
####  1.7 获取会议列表 
  
   | URL    | http://host:port/Meeting/list |
   | --------- |  ------------ |
   |Method | get       |             |  
   |URL | http://127.0.0.1:8011/Meeting/list|  
   
请求参数：
    
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
   | pageNum |  整数 | 分页 |
   | pageSize |  整数 | 分页 |
   | openDate |  varchar(20) | 会议开始时间 |
   | roomScope |  varchar(20) | 会议使用者分类 默认2 内部会议 1外部人员会议|    
   | appointmentName |  varchar(20) | 预约会议人 |    
   | meetingSource |  varchar(20) |  会议来源  1 系统 2 EO   3 移动端|    
   | useState |  整数 | 使用状态  1 未开始 2进行中  3已结束 4 已经取消 |    
     
      
请求：
 ```
```
返回:
  
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
  | appointmentName |  varchar(20) | 预约人 |
  | appointmentMobile |  varchar(20) | 预约人手机号 |
  | openDate |  varchar(20) | 会议时间 |
  | minTime |  varchar(20) | 会议时长 |
  | NumberOfPeople |  varchar(20) | 会议人数 |
  | enterprise |  varchar(20) | 企业名称 |
  | roomId |  varchar(20) | 会议室编号 |
  | roomName |  varchar(20) | 会议室名称 |
  | MeetingTheme |  varchar(20) | 会议主题 |
  | roomScope |  varchar(20) | 会议室类型 |
  | roomType |  varchar(20) | 会议分类 |
  | prepay |  varchar(20) | 预付金额 |
  | actuallyPaid |  varchar(20) | 实付金额 |
  | isSignIn |  varchar(20) | 是否签到 |
  | remindTime |  varchar(20) | 会议提醒时间 |
  | isAccommodation |  array | 食宿需求 |
  | goodsList |  array | 会议物品列表 |
  | meetingPeoples |  array | 会议通知人列表 |
  | pushMethod |  varchar(20) | 推送方式 |
  | MemberList |  array | 会议通知人补录列表 |
  | remark |  varchar(20) | 备注 |
 
 ```
 {
 	"code": 0,
 	"message": "请求成功！",
 	"exception": null,
 	"data": {
 		"total": 1,
 		"list": [
 			{
 				"meetingSourceLabel": "系统",
 				"roomScopeLabel": "内部",
 				"useStateLabel": "已结束",
 				"meetingId": "bd80ac9ae3ae4835a6ccfa69fed304e7",
 				"meetingSource": true,
 				"serialNumber": "HY20230220162426691",
 				"appointmentName": "wiwl",
 				"appointmentMobile": "158",
 				"openDate": "2023-02-20 12:15:44",
 				"endDate": "2023-02-20 12:30:44",
 				"minTime": 15,
 				"numberOfPeople": 23,
 				"enterprise": "北京协成致远",
 				"roomId": "会议室id",
 				"roomName": "会议室名",
 				"meetingTheme": "春节年会主题",
 				"roomScope": true,
 				"roomType": "t",
 				"prepay": 66.66,
 				"actuallyPaid": 66,
 				"isSignIn": false,
 				"remindTime": "2",
 				"isAccommodation": false,
 				"pushMethod": 2,
 				"remark": "备注",
 				"useState": 3,
 				"stars": null,
 				"comment": null,
 				"notifierList": [
 					{
 						"id": 26,
 						"meetingId": "bd80ac9ae3ae4835a6ccfa69fed304e7",
 						"delFlag": 0,
 						"name": "张三",
 						"mobile": "158",
 						"source": true
 					}
 				],
 				"replenishNotifierList": [
 					{
 						"id": 25,
 						"meetingId": "bd80ac9ae3ae4835a6ccfa69fed304e7",
 						"delFlag": 0,
 						"name": "张三",
 						"mobile": "158",
 						"source": false
 					}
 				],
 				"goodsList": [
 					{
 						"id": 7,
 						"meetingId": "bd80ac9ae3ae4835a6ccfa69fed304e7",
 						"delFlag": 0,
 						"goodsId": 6666,
 						"goodsName": "未知物品",
 						"amount": 30,
 						"uniValence": 66.66
 					},
 					{
 						"id": 8,
 						"meetingId": "bd80ac9ae3ae4835a6ccfa69fed304e7",
 						"delFlag": 0,
 						"goodsId": 6666,
 						"goodsName": "未知物品",
 						"amount": 30,
 						"uniValence": 66.66
 					}
 				]
 			}
 		],
 		"pageNum": 1,
 		"pageSize": 1,
 		"size": 1,
 		"startRow": 0,
 		"endRow": 0,
 		"pages": 1,
 		"prePage": 0,
 		"nextPage": 0,
 		"isFirstPage": true,
 		"isLastPage": true,
 		"hasPreviousPage": false,
 		"hasNextPage": false,
 		"navigatePages": 8,
 		"navigatepageNums": [
 			1
 		],
 		"navigateFirstPage": 1,
 		"navigateLastPage": 1
 	}
 }
```   
#### 1.8 获取会议列表
  
   | URL    | http://host:port/Meeting/downloadData |
   | --------- |  ------------ |
   |Method | get       |             |
   
请求参数：
    
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
   | openDate |  varchar(20) | 会议开始时间 |
   | roomScope |  varchar(20) | 会议使用者分类 默认2 内部会议 1外部人员会议|    
   | appointmentName |  varchar(20) | 预约会议人 |    
   | meetingSource |  varchar(20) |  会议来源  true 系统 false EO |    
   | useState |  整数 | 使用状态  1 未开始 2进行中  3已结束 4 已经取消 |    
      
请求和列表一样不带分页参数  

返回一个文件


#### 1.9  会议室热力图

   | URL    | http://host:port/base/InfoList |
   | --------- |  ------------ |
   |Method | get       |             |
   
请求参数：
    
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
   | openDate |  日期 | 开始时间 年/月/日 |
   | roomName |  字符串 | 会议室名字 |



返回参数：
    
  | 参数     | 类型    | 说明       |
  | -------- | ------ | ---------- |
   | data |  map映射 |  k为会议室名 v为时间节点   |   
   | v |  map映射 |  k为时间节点 0-47  v为状态 0是空闲 1 未开始 进行中  3已结束 4已取消  5上一个会议的时长范围  |   
   
  返回示例：
  ```
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"第二会议室": {
			"0": "0",
			"1": "0",
			"2": "0",
			"3": "0",
			"4": "0",
			"5": "0",
			"6": "0",
			"7": "0",
			"8": "0",
			"9": "0",
			"10": "0",
			"11": "0",
			"12": "0",
			"13": "0",
			"14": "0",
			"15": "0",
			"16": "0",
			"17": "0",
			"18": "0",
			"19": "0",
			"20": "0",
			"21": "0",
			"22": "0",
			"23": "0",
			"24": "3",
			"25": "0",
			"26": "0",
			"27": "0",
			"28": "0",
			"29": "0",
			"30": "0",
			"31": "0",
			"32": "0",
			"33": "0",
			"34": "0",
			"35": "0",
			"36": "0",
			"37": "0",
			"38": "0",
			"39": "0",
			"40": "0",
			"41": "0",
			"42": "0",
			"43": "0",
			"44": "0",
			"45": "0",
			"46": "0",
			"47": "0"
		},
		"第三会议室": {
			"0": "0",
			"1": "0",
			"2": "0",
			"3": "0",
			"4": "0",
			"5": "0",
			"6": "0",
			"7": "0",
			"8": "0",
			"9": "0",
			"10": "0",
			"11": "0",
			"12": "0",
			"13": "0",
			"14": "0",
			"15": "0",
			"16": "0",
			"17": "0",
			"18": "0",
			"19": "0",
			"20": "0",
			"21": "0",
			"22": "4",
			"23": "5",
			"24": "5",
			"25": "0",
			"26": "0",
			"27": "0",
			"28": "0",
			"29": "0",
			"30": "0",
			"31": "0",
			"32": "0",
			"33": "0",
			"34": "0",
			"35": "0",
			"36": "0",
			"37": "0",
			"38": "0",
			"39": "0",
			"40": "0",
			"41": "0",
			"42": "0",
			"43": "0",
			"44": "0",
			"45": "0",
			"46": "0",
			"47": "0"
		},
		"第一会议室": {
			"0": "0",
			"1": "0",
			"2": "0",
			"3": "0",
			"4": "0",
			"5": "0",
			"6": "0",
			"7": "0",
			"8": "0",
			"9": "0",
			"10": "0",
			"11": "0",
			"12": "0",
			"13": "0",
			"14": "0",
			"15": "0",
			"16": "0",
			"17": "0",
			"18": "0",
			"19": "0",
			"20": "0",
			"21": "0",
			"22": "0",
			"23": "0",
			"24": "0",
			"25": "0",
			"26": "1",
			"27": "5",
			"28": "5",
			"29": "0",
			"30": "0",
			"31": "0",
			"32": "0",
			"33": "0",
			"34": "0",
			"35": "0",
			"36": "0",
			"37": "0",
			"38": "0",
			"39": "0",
			"40": "0",
			"41": "0",
			"42": "0",
			"43": "0",
			"44": "0",
			"45": "0",
			"46": "0",
			"47": "0"
		},
		"第四会议室": {
			"0": "0",
			"1": "0",
			"2": "0",
			"3": "0",
			"4": "0",
			"5": "0",
			"6": "0",
			"7": "0",
			"8": "0",
			"9": "0",
			"10": "0",
			"11": "0",
			"12": "0",
			"13": "0",
			"14": "0",
			"15": "0",
			"16": "0",
			"17": "0",
			"18": "0",
			"19": "0",
			"20": "0",
			"21": "0",
			"22": "0",
			"23": "0",
			"24": "0",
			"25": "0",
			"26": "0",
			"27": "0",
			"28": "0",
			"29": "0",
			"30": "0",
			"31": "0",
			"32": "0",
			"33": "0",
			"34": "0",
			"35": "0",
			"36": "0",
			"37": "0",
			"38": "1",
			"39": "5",
			"40": "5",
			"41": "0",
			"42": "0",
			"43": "0",
			"44": "0",
			"45": "0",
			"46": "0",
			"47": "0"
		}
	}
}
  ```    
 