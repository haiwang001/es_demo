package com.bjxczy.onepark.hikmodule.api;


import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.utils.date.DateUtil;
import com.bjxczy.onepark.common.utils.json.JsonUtils;
import com.bjxczy.onepark.common.utils.http.HttpClientUtil;
import com.bjxczy.onepark.common.utils.file.ImgUtils;
import com.bjxczy.onepark.hikmodule.pojo.vo.DeviceVo;
import com.bjxczy.onepark.hikmodule.pojo.vo.HikReturnDto;
import com.bjxczy.onepark.hikmodule.pojo.vo.HikReturnFaceDto;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/5/20 13:12
 */
@Log4j2
public class SignInApi {
    /**
     * @param faceUrl         人脸路径
     * @param DeviceIndexCode 设备id
     * @param mobile          人员编号 手机号代替
     * @param name            人员姓名
     */
    public static void clearFace(String faceUrl, String DeviceIndexCode, String mobile, String name) {
        log.info("--- 会议 --- 删除照片 : 开始");
        HashMap<String, Object> map = new HashMap<>(11);
        map.put("faceURL", faceUrl);
        map.put("deviceIndexCode", DeviceIndexCode);
        map.put("fpid", mobile);
        map.put("name", name);
        map.put("bornTime", "1990-01-01");
        map.put("gender", "unknown");
        map.put("faceLibType", "blackFD");
        map.put("fdid", "1");
        map.put("operateType", "byTerminal");
        map.put("faceType", "normalFace");
        map.put("transfer", true);
        HikReturnDto object = null;
        String s = "";
        try {
            log.info("--- 会议过期 --- 清除照片 参数: " + map);
            s = HttpClientUtil.postJson(SysConst.HIK_SMART_ROOM_DEL_FACE_API, JsonUtils.toJsonString(map));
            object = JSON.parseObject(s, HikReturnDto.class);
            if (object.getStatusCode().equals("1")) {
                log.info("--- 会议过期 --- 清除照片 成功: " + object.getStatusString());
            } else {
                log.error("--- 会议过期 --- 清除照片 失败: 失败信息" + s);
            }
        } catch (IOException e) {
            log.error("--- 会议过期 ---  清除照片 异常: " + e.getMessage());
        }
    }

    /**
     * 删除人员
     *
     * @param DeviceIndexCode 设备id
     * @param mobile          人员编号 手机号代替
     */
    public static void clearPeople(String DeviceIndexCode, String mobile) {
        log.info("--- 会议 --- 删除人员 : 开始");
        HashMap<String, Object> map = new HashMap<>(5);
        map.put("deviceIndexCode", DeviceIndexCode);
        HashMap<String, Object> userInfoDelCond = new HashMap<>();
        userInfoDelCond.put("operateType", "byTerminal");
        ArrayList<HashMap<String, Object>> arrayList = new ArrayList<>();
        HashMap<String, Object> employeeNoList = new HashMap<>();
        employeeNoList.put("employeeNo", mobile);
        arrayList.add(employeeNoList);
        userInfoDelCond.put("employeeNoList", arrayList);
        map.put("userInfoDelCond", userInfoDelCond);
        HikReturnDto object = null;
        String s = "";
        try {
            log.info("--- 会议过期 --- 清除人员 参数: " + map);
            s = HttpClientUtil.postJson(SysConst.HIK_SMART_ROOM_DEL_USER_API, JsonUtils.toJsonString(map));
            object = JSON.parseObject(s, HikReturnDto.class);
            if (object.getStatusCode().equals("1")) {
                log.info("--- 会议过期 --- 清除人员 成功: " + object.getStatusString());
            } else {
                log.error("--- 会议过期 --- 清除人员 失败: 失败信息" + s);
            }
        } catch (IOException e) {
            log.error("--- 会议过期 ---  清除人员 异常: " + e.getMessage());
        }
    }

    /**
     * 增加人员
     *
     * @param mobile   人员编号 手机号
     * @param name     人员姓名
     * @param deviceId 设备id
     */
    public static void InputPersonnel(String mobile, String name, String deviceId) {
        log.info("--- 会议 --- 增加人员 : 开始");
        HashMap<String, Object> map = new HashMap<>();
        map.put("deviceIndexCode", deviceId);
        HashMap<String, Object> userInfo = new HashMap<>();
        userInfo.put("employeeNo", mobile); // 参会人员编号  用手机号代替
        userInfo.put("name", name); // 参会人姓名
        userInfo.put("roomNumber", 0);
        userInfo.put("userVerifyMode", "face");
        userInfo.put("gender", "unknown");
        userInfo.put("operateType", "byTerminal");
        userInfo.put("numOfCard", 0);
        userInfo.put("closeDelayEnabled", true);
        userInfo.put("checkUser", false);
        userInfo.put("maxOpenDoorTime", 0);
        userInfo.put("OpenDoorTime", 0);
        userInfo.put("floorNumber", 0);
        userInfo.put("localULRight", false);
        userInfo.put("userType", "normal");
        userInfo.put("numOfFP", 0);
        userInfo.put("doubleLockRight", false);
        userInfo.put("numOfFace", 0);
        map.put("userInfo", userInfo);
        log.info("--- 会议 --- 添加参会人员" + map);
        HikReturnDto object = null;
        try {
            String s = HttpClientUtil.postJson(SysConst.HIK_SMART_ROOM_ADD_USER_API, JsonUtils.toJsonString(map));
            object = JSON.parseObject(s, HikReturnDto.class);
            if (object.getStatusString().equals("OK")) {
                log.info("--- 会议 --- 一体机设备 增加人员成功: " + object.getStatusString());
            } else {
                if (object.getSubStatusCode().equals("deviceUserAlreadyExist")) {
                    log.info("--- 会议 --- 一体机设备 人员已经存在: OK");
                } else {
                    log.error("--- 会议 --- 一体机设备 人员增加失败: 失败错误码" + object.getErrorCode());
                    log.error("--- 会议 --- 一体机设备 人员增加失败: 失败错误信息1" + object.getErrorMsg());
                    log.error("--- 会议 --- 一体机设备 人员增加失败: 失败错误信息2" + object.getSubStatusCode());
                }
            }
        } catch (IOException e) {
            log.error("--- 会议 --- 一体机设备 人员增加异常: " + e.getMessage());
            e.printStackTrace();
        }
    }


    /**
     * 人员设备照片三者绑定
     *
     * @param mobile   人员编号 手机号
     * @param name     人员姓名
     * @param deviceId 设备id
     * @param url      照片路径
     */
    public static void binding(String mobile, String name, String deviceId, String url) {
        log.info("--- 会议 --- 人员 照片 设备 三者绑定 : 开始");
        HashMap<String, Object> map = new HashMap<>(11);
        map.put("faceURL", url);
        map.put("deviceIndexCode", deviceId);
        map.put("name", name);
        map.put("fpid", mobile);//手机代替
        map.put("fdid", 1);
        map.put("faceType", "normalFace");
        map.put("operateType", "byTerminal");
        map.put("faceLibType", "blackFD");
        map.put("gender", "unknown");
        map.put("bornTime", "1990-01-01");
        map.put("transfer", true);
        HikReturnDto object = null;
        try {
            String s = HttpClientUtil.postJson(SysConst.HIK_SMART_ROOM_ADD_FACE_DATA_RECORD_API, JsonUtils.toJsonString(map));
            object = JSON.parseObject(s, HikReturnDto.class);
            if (object.getStatusString().equals("OK")) {
                log.info("--- 会议 --- 人员绑定 : " + object.getStatusString());
            } else {
                log.error("--- 会议 --- 人员绑定 失败: 失败错误码" + object.getErrorCode());
                log.error("--- 会议 --- 人员绑定 失败: 失败错误信息1" + object.getErrorMsg());
                log.error("--- 会议 --- 人员绑定 失败: 失败错误信息2" + object.getSubStatusCode());
            }
        } catch (IOException e) {
            log.error("--- 会议 ---  人员绑定 异常: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 上报人脸
     *
     * @param url    本系统图片路径
     * @param mobile 人员编号 作为照片名字
     * @return 会管图片路径 用于删除
     */
    public static String addFace(String url, String mobile) {
        log.info("--- 会议 --- 添加人脸 : 开始");
        HashMap<String, Object> map = new HashMap<>();
        map.put("picBase64", ImgUtils.getFileBase64ToString(url));
        map.put("fileName", mobile + ".jpg");
        HikReturnFaceDto object = null;
        try {
            String s = HttpClientUtil.postJson(SysConst.HIK_SMART_ROOM_ADD_FACE_API, JsonUtils.toJsonString(map));
            object = JSON.parseObject(s, HikReturnFaceDto.class);
            if (object.getCode() == 0) {
                log.info("--- 会议 --- 上报人脸到会管  成功" + object.getMsg());
                return object.getData();
            } else {
                log.error("--- 会议 --- 上报人脸到会管 失败 错误信息--json展示>> " + object);
            }
        } catch (IOException e) {
            log.error("--- 会议 --- 上报人脸到会管异常 " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param MeetingId       会议id
     * @param RoomName        会议室名字
     * @param AppointmentName 预约人
     * @param MeetingTheme    会议标题
     * @param OpenDate        开始时间
     * @param EndDate         结束时间
     * @param employee        人员编号 多个,隔开
     * @param roomId          会议室id
     */
    public static void CreateMeeting(String MeetingId,
                                     String RoomName,
                                     String AppointmentName,
                                     String MeetingTheme,
                                     Date OpenDate,
                                     Date EndDate,
                                     StringBuilder employee,
                                     String roomId) {
        log.info("--- 会议 --- 创建会议 : 开始");
        HashMap<String, Object> map = new HashMap<>();
        map.put("roomId", roomId);
        map.put("meetingId", MeetingId);
        map.put("name", RoomName);
        map.put("members", employee);
        map.put("moderator", AppointmentName);
        map.put("title", MeetingTheme);
        map.put("signType", "0"); // 会议签到
        map.put("laterSignTime", "100");
        map.put("startSignTime", "100");
        map.put("startTime", DateUtil.dateToISO8601(DateUtil.format(OpenDate, "yyyy-MM-dd HH:mm:ss")));
        map.put("endTime", DateUtil.dateToISO8601(DateUtil.format(EndDate, "yyyy-MM-dd HH:mm:ss")));
        HikReturnDto object = null;
        try {
            log.info("--- 会议 --- 创建会议 参数: " + map);
            String s = HttpClientUtil.postJson(SysConst.HIK_MEETING_CREATE_API, JsonUtils.toJsonString(map));
            object = JSON.parseObject(s, HikReturnDto.class);
            if (object.getCode() == 0) {
                log.info("--- 会议 --- 创建会议 成功: " + object.getStatusString());
            } else if (object.getCode() == 100002) {
                log.error("--- 会议 --- 创建会议 失败: 失败原因" + object.getMsg());
            } else {
                log.error("--- 会议 --- 创建会议 失败: 失败错误码" + object.getErrorCode());
                log.error("--- 会议 --- 创建会议 失败: 失败错误信息1" + object.getErrorMsg());
                log.error("--- 会议 --- 创建会议 失败: 失败错误信息2" + object.getSubStatusCode());
            }
        } catch (IOException e) {
            log.error("--- 会议 ---  创建会议 异常: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     * @return  全部设备
     */
    public static List<DeviceVo> selectRoomDevice() {
        log.info("--- 会议 --- 查询一体机设备 : 开始");
        HashMap<String, Object> map = new HashMap<>();
        map.put("bind", true);
        HikReturnDto object = null;
        try {
            String s = HttpClientUtil.get(SysConst.HIK_SMART_ROOM_DEVICE_SEARCH_API, JsonUtils.toJsonString(map));
            object = JSON.parseObject(s, HikReturnDto.class);
            if (object.getCode() == 0) {
                log.info("--- 会议 --- 查询一体机设备 设备查询成功" + object.getData());
            } else {
                log.error("--- 会议 --- 查询一体机设备 设备查询失败 错误码" + object.getCode());
                log.error("--- 会议 --- 查询一体机设备 设备查询失败 错误描述" + object.getMsg());
            }
        } catch (Exception e) {
            log.error("--- 会议 --- 查询一体机设备 设备查询异常" + e.getMessage());
            return null;
        }
        return object.getData().getDevices();
    }

    /**
     * @param url 海康推送给本平台数据的地址
     */
    public static void subscription(String url) {
        log.info("--- 会议 --- 订阅签到事件 : 开始");
        HashMap<String, String> map = new HashMap<>();
        map.put("eventUrl", url); // 海康推送数据的地址
        map.put("eventType", "SignInOutEvent"); // 签到签出事件只有一种类型
        HikReturnDto object = null;
        String s = "";
        try {
            s = HttpClientUtil.post(map, SysConst.HIK_SMART_ROOM_EVENT_API, new HashMap<>(), "application/x-www-form-urlencoded");
            object = JSON.parseObject(s, HikReturnDto.class);
            if (object.getCode() == 0) {
                log.info("海康事件订阅  -- 会议签到 订阅--> 成功" + object);
            } else {
                log.error("海康事件订阅  -- 会议签到 订阅--> 失败" + object);
            }
        } catch (Exception e) {
            log.error("--- 海康事件订阅 ---  异常: " + s);
            e.printStackTrace();
        }
    }
}
