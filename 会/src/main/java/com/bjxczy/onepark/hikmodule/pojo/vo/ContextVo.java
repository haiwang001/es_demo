package com.bjxczy.onepark.hikmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/5/15 18:35  海康签到 返回
 */
@Data
public class ContextVo {
    private String employeeNo;// 员工编号 本系统手机号
    private String dateTime;// 签到时间
    private String meetingId;// 会议id

    /**
     *
     * @param employeeNo  手机号
     * @param dateTime 签到时间
     * @param meetingId 会议id
     */
    public ContextVo(String employeeNo,  String dateTime, String meetingId) {
        this.employeeNo = employeeNo;
        this.dateTime = dateTime;
        this.meetingId = meetingId;
    }

    public ContextVo() {
    }
}
