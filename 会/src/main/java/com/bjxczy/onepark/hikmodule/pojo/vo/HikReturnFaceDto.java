package com.bjxczy.onepark.hikmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/5/15 17:08
 */
@Data
public class HikReturnFaceDto {
    private Integer code;// 返回状态
    private String msg;// 返回消息
    private String data;// 图片路径
}
