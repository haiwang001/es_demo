package com.bjxczy.onepark.hikmodule.pojo.vo;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/5/15 15:59
 */
@Data
public class DeviceVo {
    private String deviceType;
    private String deviceId; //设备编号
    private String deviceName;//设备名
    private List<String> roomId;//绑定的会议室id
}
