package com.bjxczy.onepark.hikmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/5/15 15:56
 */
@Data
public class HikReturnDto {
    private Integer code;// 返回状态
    private String msg;// 返回消息
    private DataVo data;// 返回消息

    private String errorCode;// 错误码
    private String errorMsg;// 错误
    private String statusCode;// 状态
    private String statusString;// 状态   OK
    private String subStatusCode;// 状态   ok
}
