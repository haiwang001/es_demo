package com.bjxczy.onepark.hikmodule.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/*
 *@Author wlw
 *@Date 2023/5/13 15:50   海康回调 和访问路径配置
 */
@Component
@Data
@ConfigurationProperties(prefix = "hik")
public class HikConference {
    private String hostService;// 访问海康的地址
    private String receipt; // 海康访问我们的地址
}
