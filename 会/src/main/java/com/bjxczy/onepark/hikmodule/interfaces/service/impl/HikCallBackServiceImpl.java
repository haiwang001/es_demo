package com.bjxczy.onepark.hikmodule.interfaces.service.impl;


import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.hikmodule.interfaces.service.HikCallBackService;
import com.bjxczy.onepark.hikmodule.pojo.vo.ContextVo;
import com.bjxczy.onepark.meeting.entity.MeetingMember;
import com.bjxczy.onepark.meeting.mapper.MeetingMemberMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/*
 *@Author wlw
 *@Date 2023/5/25 10:11
 */
@Log4j2
@Service
public class HikCallBackServiceImpl implements HikCallBackService {
    @Resource
    private MeetingMemberMapper MemberMapper;

    /**
     * @param map key context 签到信息  key eventType 事件
     */
    @Override
    public void signIn(Map map) {
        if ("signInEvent".equals(map.get("eventType"))) { //签到事件
            ContextVo context = JSON.parseObject(JSON.toJSONString(map.get("context")), ContextVo.class);
            MeetingMember member = MemberMapper.selectByMobileAndMeetingId(context.getEmployeeNo(), context.getMeetingId());
            if (member != null) {
                log.info("查询到签到人员: " + member);
                if (member.getSignIn() == 0) {
                    log.info("签到人员第一次签到: " + member.getSignIn());
                    MemberMapper.updateDate(context);
                } else {
                    log.info("签到人员以签到: " + member.getSignIn());
                }
            } else {
                log.info("查询到签到人员: 未找到" + member);
            }
        }
    }
}
