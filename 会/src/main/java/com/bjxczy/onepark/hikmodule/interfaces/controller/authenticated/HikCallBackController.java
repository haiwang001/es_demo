package com.bjxczy.onepark.hikmodule.interfaces.controller.authenticated;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.hikmodule.interfaces.service.HikCallBackService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Map;

/*
 *@Author wlw
 *@Date 2023/5/15 18:26
 */
@ApiResourceController
@RequestMapping(SysConst.HIK_CALLBACK_CONTROLLER) //HikCallBack
@Log4j2
public class HikCallBackController {

    @Resource
    private HikCallBackService Hik;

    @PostMapping(value = SysConst.HIK_CALLBACK_SIGNIN, name = "参会签到")
    public Object signIn(@RequestBody Map map) {
        log.info("参会签到 ------------ 回调成功" + map + "");
        Hik.signIn(map);
        return 1;
    }
}
