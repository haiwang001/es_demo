package com.bjxczy.onepark.hikmodule.interfaces.listener;


import com.bjxczy.onepark.hikmodule.api.SignInApi;
import com.bjxczy.onepark.hikmodule.config.HikConference;
import com.bjxczy.onepark.common.constant.SysConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/*
 *@Author wlw
 *@Date 2023/5/15 18:11
 */
@Component
@Slf4j
public class HikEventListener implements ApplicationListener<WebServerInitializedEvent> {

    @Resource
    private HikConference Hik;

    /**
     * 项目启动订阅海康签到事件
     *         public Integer PROJECT_PORT;
     *         this.PROJECT_PORT = webServerInitializedEvent.getWebServer().getPort();// 本次项目的端口
     *
     * @param webServerInitializedEvent
     */
    @Override
    public void onApplicationEvent(WebServerInitializedEvent webServerInitializedEvent) {
        String url = Hik.getReceipt() + "/" + SysConst.HIK_CALLBACK_CONTROLLER + "/" + SysConst.HIK_CALLBACK_SIGNIN;
        System.err.println(url);
        // 订阅海康签到
        SignInApi.subscription(url);
    }
}
