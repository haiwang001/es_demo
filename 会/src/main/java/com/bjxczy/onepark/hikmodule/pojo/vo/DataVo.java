package com.bjxczy.onepark.hikmodule.pojo.vo;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/5/15 15:58
 */
@Data
public class DataVo {
    private List<DeviceVo> devices;
}
