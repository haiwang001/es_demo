package com.bjxczy.onepark.screen.service.impl;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.common.utils.date.DateUtil;
import com.bjxczy.onepark.meeting.mapper.MeetingInfoMapper;
import com.bjxczy.onepark.meeting.pojo.vo.DateNodeVo;
import com.bjxczy.onepark.room.mapper.ConferenceRoomMapper;
import com.bjxczy.onepark.screen.mapper.MeetingScreenMapper;
import com.bjxczy.onepark.screen.pojo.vo.MeetingRoomUseVo;
import com.bjxczy.onepark.screen.pojo.vo.MeetingStatisticsVo;
import com.bjxczy.onepark.screen.service.MeetingScreenService;
import com.google.common.collect.ImmutableBiMap;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/13 9:32
 */
@Service
public class MeetingScreenServiceImpl implements MeetingScreenService {

    @Resource
    private MeetingScreenMapper mapper;
    @Resource
    private ConferenceRoomMapper RoomMapper;
    @Resource
    private MeetingInfoMapper meetingInfoMapper;

    @Override
    public Object meetingRoomSignage() {
        int roomSize = RoomMapper.selectByMap(ImmutableBiMap.of("del_flag", 0)).size();
        int integer = meetingInfoMapper.selectSizeByUseState().size();
        return new MeetingRoomUseVo(roomSize, integer, roomSize - integer, String.format("%.2f", (double) integer / roomSize * 100));
    }

    @Override
    public Object meetingLiUnderWay(String day, Integer pageNum, Integer pageSize) {
        return meetingInfoMapper.selectByUseState(new Page<>(pageNum, pageSize), 2);
    }

    @Override
    public Object meetingStatistics(int days) {
        List<DateNodeVo> listNode = DateUtil.LastWeek(days);//获取近四天的节点 yyyy-mm-dd 节点
        for (DateNodeVo nodeVo : listNode) {
            nodeVo.setY(meetingInfoMapper.getDateNode(nodeVo.getX()));
        }
        return new MeetingStatisticsVo(
                listNode.get(0).getY(),
                meetingInfoMapper.getDateNodeByMonths(DateUtil.getYearAndMonth()),
                meetingInfoMapper.selectInfoByNotStarted(DateUtil.getYearAndMonth()),
                listNode);
    }
}
