package com.bjxczy.onepark.screen.service;

public interface MeetingScreenService {
    Object meetingRoomSignage();

    Object meetingStatistics(int days);

    Object meetingLiUnderWay(String day, Integer pageNum, Integer pageSize);
}
