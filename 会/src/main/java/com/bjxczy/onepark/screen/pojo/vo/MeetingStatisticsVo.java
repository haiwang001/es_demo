package com.bjxczy.onepark.screen.pojo.vo;


import com.bjxczy.onepark.meeting.pojo.vo.DateNodeVo;
import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/16 9:25
 */
@Data
public class MeetingStatisticsVo {
    private Long today; // 今日会议
    private Long count; // 总会议
    private Long notYetStarted; // 未进行的会议
    private List<DateNodeVo> data; // 节点数据

    public MeetingStatisticsVo() {
    }

    public MeetingStatisticsVo(Long today, Long count, Long notYetStarted, List<DateNodeVo> data) {
        this.today = today;
        this.count = count;
        this.notYetStarted = notYetStarted;
        this.data = data;
    }
}
