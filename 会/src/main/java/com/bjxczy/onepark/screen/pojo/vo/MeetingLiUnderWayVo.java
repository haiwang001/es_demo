package com.bjxczy.onepark.screen.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/16 9:51
 */
@Data
public class MeetingLiUnderWayVo {
    private String roomName;
    private String serialNumber;
    private String endDate;
    private String state;
    private String operator;
}
