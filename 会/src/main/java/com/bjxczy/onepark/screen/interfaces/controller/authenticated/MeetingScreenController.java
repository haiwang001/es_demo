package com.bjxczy.onepark.screen.interfaces.controller.authenticated;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.screen.service.MeetingScreenService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/*
 *@Author wlw
 *@Date 2023/6/13 9:30  大屏 会议数据接口
 */
@Log4j2
@ApiResourceController
@RequestMapping("meetingScreen")
public class MeetingScreenController {

    @Resource
    private MeetingScreenService service;

    /**
     * 会议室看板
     *
     * @return 所有会议室数量 使用的会议室数量 和 未使用的数量
     */
    @GetMapping(value = "/meetingRoomUse", name = "大屏 - 会议室实时看板")
    public R<?> meetingRoomSignage() {
        return R.success(service.meetingRoomSignage());
    }


    /**
     * 会议统计
     *
     * @return
     */
    @GetMapping(value = "/meetingStatistics" ,name = "大屏 - 会议统计")
    public R<?> meetingStatistics(int days) {
        return R.success(service.meetingStatistics(days));
    }

    /**
     * 今日 的会议列表
     *
     * @return
     */
    @GetMapping(value = "/meetingLiUnderWay",name = "大屏 - 会议列表")
    public R<?> meetingLiUnderWay(String day, Integer pageNum, Integer pageSize) {
        return R.success(service.meetingLiUnderWay(day,pageNum,pageSize));
    }

}
