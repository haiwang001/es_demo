package com.bjxczy.onepark.screen.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/15 17:25
 */
@Data
public class MeetingRoomUseVo {
    private Integer count;//所有会议室
    private Integer inUse;//使用中的
    private Integer notInUse;//未使用的
    private String utilizationRate;// 使用率

    public MeetingRoomUseVo(Integer count, Integer inUse, Integer notInUse, String utilizationRate) {
        this.count = count;
        this.inUse = inUse;
        this.notInUse = notInUse;
        this.utilizationRate = utilizationRate;
    }
}
