package com.bjxczy.onepark.publicmodule.service;

import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;

public interface MsgTemplateService {
    void subscriberNotice(MeetingInfo info, UserInformation user);

    void participateNotice(MeetingInfo info, Integer staffId, String text);

    void updateCommissionItem(Integer status, String text);
}
