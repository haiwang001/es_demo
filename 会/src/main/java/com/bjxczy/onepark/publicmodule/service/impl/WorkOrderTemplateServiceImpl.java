package com.bjxczy.onepark.publicmodule.service.impl;


import cn.hutool.core.collection.CollectionUtil;
import com.bjxczy.core.web.workflow.WorkOrderService;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.workflow.DescribeItem;
import com.bjxczy.onepark.common.model.workflow.PushOrderPayload;
import com.bjxczy.onepark.common.model.workflow.TextDescribeValue;
import com.bjxczy.onepark.common.utils.date.DateUtil;
import com.bjxczy.onepark.meeting.config.WorkOrderConfig;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;
import com.bjxczy.onepark.meeting.mapper.MeetingInfoMapper;
import com.bjxczy.onepark.publicmodule.service.WorkOrderTemplateService;
import com.bjxczy.onepark.room.entity.ConferenceRoom;
import com.bjxczy.onepark.room.mapper.ConferenceRoomMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/12 16:15
 */
@Log4j2
@Service
public class WorkOrderTemplateServiceImpl implements WorkOrderTemplateService {
    @Resource
    private WorkOrderService workOrderService;
    @Resource
    private ConferenceRoomMapper RoomMapper;
    @Resource
    private WorkOrderConfig workOrderConfig;
    @Resource
    private MeetingInfoMapper mapper;
    /**
     * 会议工单
     *
     * @param info
     */
    @Override
    public void TriggerWorkOrder(MeetingInfo info, UserInformation user, ConferenceRoom conferenceRoom) {
        List<DescribeItem> items = CollectionUtil.newArrayList();
        DescribeItem d_0 = new DescribeItem("会议预约人", new TextDescribeValue(info.getAppointmentName() + ""));
        DescribeItem d_1 = new DescribeItem("联系方式", new TextDescribeValue(info.getAppointmentMobile() + ""));
        DescribeItem d_2 = new DescribeItem("开始时间", new TextDescribeValue(DateUtil.format(info.getOpenDate(), "yyyy-MM-dd HH:mm") + ""));
        DescribeItem d_3 = new DescribeItem("结束时间", new TextDescribeValue(DateUtil.format(info.getEndDate(), "yyyy-MM-dd HH:mm") + ""));
        DescribeItem d_4 = new DescribeItem("会议人数", new TextDescribeValue(info.getNumberOfPeople() + ""));
        DescribeItem d_5 = new DescribeItem("所属企业", new TextDescribeValue(info.getEnterprise() + ""));
        DescribeItem d_6 = new DescribeItem("会议室", new TextDescribeValue(info.getRoomName() + ""));
        DescribeItem d_7 = new DescribeItem("会议主题", new TextDescribeValue(info.getMeetingTheme() + ""));
        DescribeItem d_8 = new DescribeItem("会议室类型", new TextDescribeValue(RoomMapper.selectBy(info.getRoomType()) + ""));
        DescribeItem d_10 = new DescribeItem("会议备注", new TextDescribeValue(info.getRemark() + " "));
        DescribeItem d_13 = new DescribeItem("工单备注", new TextDescribeValue(info.getWorkRemark() + " "));
        DescribeItem d_9 = null;
        if (info.getRoomScope()) {
            d_9 = new DescribeItem("会议分类", new TextDescribeValue("外部会议"));
        } else {
            d_9 = new DescribeItem("会议分类", new TextDescribeValue("外部会议"));
        }
        items.add(d_0);
        items.add(d_1);
        items.add(d_2);
        items.add(d_3);
        items.add(d_4);
        items.add(d_5);
        items.add(d_6);
        items.add(d_7);
        items.add(d_8);
        items.add(d_9);
        items.add(d_10);
        items.add(d_13);
        PushOrderPayload payload = new PushOrderPayload();
        payload.setId(info.getWorkOrderId()); // 工单id
        payload.setDescribes(items); // 描述
        payload.setInitiatorId(user.getStaffId()); // 发起人
        payload.setInitiatorName(user.getStaffName()); //发起人名
        payload.setAreaId(Integer.parseInt(conferenceRoom.getRegionId()));//区域位置
        payload.setTenantId(conferenceRoom.getBuildingId());
        payload.setSpaceId(conferenceRoom.getSpaceId());
        String setByRegionName = RoomMapper.setRegionNameByRegionId(conferenceRoom.getRegionId());
        String getBuildingName = RoomMapper.getBuildingNameByBuildingId(conferenceRoom.getBuildingId());
        String getSpaceId = RoomMapper.getSpaceNameBySpaceId(conferenceRoom.getSpaceId());
        payload.setPosition(setByRegionName + "/" + getBuildingName + "/" + getSpaceId);// 区域位置
        payload.setDescription("已通过,请根据信息创建");
        workOrderService.pushOrder(payload);
    }


    /**
     * 餐饮工单
     *
     * @param info
     */
    @Override
    public void cateringWorkOrder(MeetingInfo info, UserInformation user, ConferenceRoom conferenceRoom) {
        List<DescribeItem> items = CollectionUtil.newArrayList();
        DescribeItem d_0 = new DescribeItem("用餐地点", new TextDescribeValue(info.getHaveMealLocation()));
        items.add(d_0);
        DescribeItem d_1 = new DescribeItem("用餐形式", new TextDescribeValue(mapper.getDictionary_have_meal_type(info.getHaveMealType())));
        items.add(d_1);
        DescribeItem d_2 = new DescribeItem("用餐时间", new TextDescribeValue(DateUtil.format(info.getOpenDate(), "yyyy-MM-dd")));
        items.add(d_2);
        DescribeItem d_9 = new DescribeItem("用餐人数", new TextDescribeValue(info.getNumberOfPeople().toString()));
        items.add(d_9);
        HashMap<String, Boolean> map = info.getDailyMealMap();
        if (map.get("1")) {
            DescribeItem d_3 = new DescribeItem("早餐", new TextDescribeValue(info.getHaveMealTime().getMorningOne() + " - " + info.getHaveMealTime().getMorningTwo()));
            items.add(d_3);
        }
        if (map.get("2")) {
            DescribeItem d_4 = new DescribeItem("午餐", new TextDescribeValue(info.getHaveMealTime().getNoonOne() + " - " + info.getHaveMealTime().getNoonTwo()));
            items.add(d_4);
        }
        if (map.get("3")) {
            DescribeItem d_5 = new DescribeItem("晚餐", new TextDescribeValue(info.getHaveMealTime().getEveningOne() + " - " + info.getHaveMealTime().getEveningTwo()));
            items.add(d_5);
        }
        // 备注允许null
        DescribeItem d_6 = new DescribeItem("用餐备注", new TextDescribeValue(info.getHaveMealRemark() + " "));
        items.add(d_6);
        DescribeItem d_7 = new DescribeItem("initiatorId", new TextDescribeValue(user.getStaffId().toString()));
        items.add(d_7);
        DescribeItem d_8 = new DescribeItem("initiatorName", new TextDescribeValue(user.getStaffName()));
        items.add(d_8);
        log.info("餐饮工单数据" + items);
        PushOrderPayload payload = new PushOrderPayload();
        payload.setId(workOrderConfig.getStayId()); // 工单id
        payload.setDescribes(items); // 描述
        payload.setInitiatorId(user.getStaffId()); // 发起人
        payload.setInitiatorName(user.getStaffName()); //发起人名
        payload.setAreaId(Integer.parseInt(conferenceRoom.getRegionId()));//区域位置
        payload.setTenantId(conferenceRoom.getBuildingId());
        payload.setSpaceId(conferenceRoom.getSpaceId());
        String setByRegionName = RoomMapper.setRegionNameByRegionId(conferenceRoom.getRegionId());
        String getBuildingName = RoomMapper.getBuildingNameByBuildingId(conferenceRoom.getBuildingId());
        String getSpaceId = RoomMapper.getSpaceNameBySpaceId(conferenceRoom.getSpaceId());
        payload.setPosition(setByRegionName + "/" + getBuildingName + "/" + getSpaceId);// 区域位置
        payload.setDescription("已通过,请根据信息创建");
        workOrderService.pushOrder(payload);
    }

    /**
     * 酒店工单
     *
     * @param info
     */
    @Override
    public void hotelWorkOrder(MeetingInfo info, UserInformation user, ConferenceRoom conferenceRoom) {
        List<DescribeItem> items = CollectionUtil.newArrayList();
        DescribeItem d_0 = new DescribeItem("住宿园区", new TextDescribeValue(info.getStayAreaLabel()));
        items.add(d_0);
        info.getAccommodationList().forEach(el -> {
            DescribeItem d_1 = new DescribeItem("住宿房型", new TextDescribeValue(mapper.getHousingType(el.getHousingType()) + "  数量 " + el.getHousingNum()));
            items.add(d_1);
        });
        DescribeItem d_2 = new DescribeItem("住宿时间", new TextDescribeValue(DateUtil.format(info.getOpenDate(), "yyyy-MM-dd")));
        items.add(d_2);
        // 备注允许null
        DescribeItem d_3 = new DescribeItem("住宿备注", new TextDescribeValue(info.getHotelRemark() + " "));
        items.add(d_3);
        DescribeItem d_11 = new DescribeItem("initiatorId", new TextDescribeValue(user.getStaffId().toString()));
        items.add(d_11);
        DescribeItem d_12 = new DescribeItem("initiatorName", new TextDescribeValue(user.getStaffName()));
        items.add(d_12);
        log.info("餐饮工单数据" + items);
        PushOrderPayload payload = new PushOrderPayload();
        payload.setId(workOrderConfig.getHotelId()); // 工单id
        payload.setDescribes(items); // 描述
        payload.setInitiatorId(user.getStaffId()); // 发起人
        payload.setInitiatorName(user.getStaffName()); //发起人名
        payload.setAreaId(Integer.parseInt(conferenceRoom.getRegionId()));//区域位置
        payload.setTenantId(conferenceRoom.getBuildingId());
        payload.setSpaceId(conferenceRoom.getSpaceId());
        String setByRegionName = RoomMapper.setRegionNameByRegionId(conferenceRoom.getRegionId());
        String getBuildingName = RoomMapper.getBuildingNameByBuildingId(conferenceRoom.getBuildingId());
        String getSpaceId = RoomMapper.getSpaceNameBySpaceId(conferenceRoom.getSpaceId());
        payload.setPosition(setByRegionName + "/" + getBuildingName + "/" + getSpaceId);// 区域位置
        payload.setDescription("已通过,请根据信息创建");
        workOrderService.pushOrder(payload);
    }


    /**
     * 清扫   会议室所在地
     * 会议室名字
     * 会议结束时间
     *
     * @param info
     */
    @Override
    public void distributeSweep(MeetingInfo info) {
        List<DescribeItem> items = new ArrayList<>();
        DescribeItem d_0 = new DescribeItem("会议所在园区", new TextDescribeValue(RoomMapper.selectById(info.getRoomId()).getAddressLabel()));
        DescribeItem d_1 = new DescribeItem("会议室名称", new TextDescribeValue(info.getRoomName()));
        DescribeItem d_2 = new DescribeItem("会议结束时间", new TextDescribeValue(DateUtil.format(info.getEndDate(), "yyyy-MM-dd HH:mm")));
        items.add(d_0);
        items.add(d_1);
        items.add(d_2);
        DescribeItem d_12 = new DescribeItem("initiatorName", new TextDescribeValue(info.getOperator()));
        items.add(d_12);
        log.info("清扫工单数据" + items);
        PushOrderPayload payload = new PushOrderPayload();
        payload.setId(workOrderConfig.getSweepId()); // 工单id
        payload.setDescribes(items); // 描述
        payload.setInitiatorName(info.getOperator()); //发起人名
        ConferenceRoom conferenceRoom = RoomMapper.selectById(info.getRoomId());
        payload.setAreaId(Integer.parseInt(conferenceRoom.getRegionId()));//区域位置
        payload.setTenantId(conferenceRoom.getBuildingId());
        payload.setSpaceId(conferenceRoom.getSpaceId());
        String setByRegionName = RoomMapper.setRegionNameByRegionId(conferenceRoom.getRegionId());
        String getBuildingName = RoomMapper.getBuildingNameByBuildingId(conferenceRoom.getBuildingId());
        String getSpaceId = RoomMapper.getSpaceNameBySpaceId(conferenceRoom.getSpaceId());
        payload.setPosition(setByRegionName + "/" + getBuildingName + "/" + getSpaceId);// 区域位置
        payload.setDescription("已通过,请根据信息创建");
        workOrderService.pushOrder(payload);
    }
}
