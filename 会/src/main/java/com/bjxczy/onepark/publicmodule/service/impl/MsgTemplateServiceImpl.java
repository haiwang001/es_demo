package com.bjxczy.onepark.publicmodule.service.impl;


import com.bjxczy.core.web.message.MessageService;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.workbench.DealMessageInformation;
import com.bjxczy.onepark.common.model.workbench.PushMessageInformation;
import com.bjxczy.onepark.common.utils.date.DateUtil;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;
import com.bjxczy.onepark.publicmodule.service.MsgTemplateService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;

/*
 *@Author wlw
 *@Date 2023/6/12 16:34
 */
@Service
public class MsgTemplateServiceImpl implements MsgTemplateService {
    /**
     * 消息发送
     */
    @Resource
    private MessageService messageService;

    /**
     * 开启代办
     *
     * @param info
     * @param user
     */
    @Override
    public void subscriberNotice(MeetingInfo info, UserInformation user) {
        String msgTemplate = "创建会议成功,详情如下:\n";
        msgTemplate += "会议室: " + info.getRoomName() + "\n";
        msgTemplate += "会议主题: " + info.getMeetingTheme() + "\n";
        msgTemplate += "会议时间: " + DateUtil.dateToChineseyyyyMMdd(info.getOpenDate()) + DateUtil.dateToChineseHHmm(info.getOpenDate()) + "-" + DateUtil.dateToChineseHHmm(info.getEndDate());
        PushMessageInformation msg = new PushMessageInformation();
        msg.setStaffId(user.getStaffId());
        msg.setBusinessId(SysConst.BUSINESS_ID);
        msg.setEventCode(SysConst.CREATE_MEETING);
        msg.setEventType("1");
        msg.setMessageType("info");
        msg.setAutoClose(true);
        msg.setTitle("智慧会议");
        HashMap<String, Object> map = new HashMap<>();
        map.put("allowClick", "always");
        msg.setExtra(map);
        msg.setContent(msgTemplate);
        messageService.pushMessage(msg);
    }

    @Override
    public void participateNotice(MeetingInfo info, Integer staffId, String text) {
        String msgTemplate = "您有一个【" + info.getMeetingTheme() + "】主题的会议, 于【" + DateUtil.dateToChinese(info.getOpenDate()) + "】开始, 地点【" + info.getRoomName() + "】, " + text;
        PushMessageInformation msg = new PushMessageInformation();
        msg.setStaffId(staffId);
        msg.setBusinessId(SysConst.BUSINESS_ID);
        msg.setEventCode(SysConst.CREATE_MEETING);
        msg.setEventType("2");
        msg.setMessageType("info");
        msg.setAutoClose(true);
        msg.setTitle("智慧会议");
        HashMap<String, Object> map = new HashMap<>();
        map.put("allowClick", "always");
        msg.setExtra(map);
        msg.setContent(msgTemplate);
        messageService.pushMessage(msg);
    }

    /**
     * 更新代办
     *
     * @param status
     * @param text
     */
    @Override
    public void updateCommissionItem(Integer status, String text) {
        messageService.dealMessage(new DealMessageInformation(SysConst.CREATE_MEETING, SysConst.BUSINESS_ID, status, text));
    }
}
