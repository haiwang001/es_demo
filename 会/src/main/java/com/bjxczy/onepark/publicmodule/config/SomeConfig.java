package com.bjxczy.onepark.publicmodule.config;


import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.dict.JdbcDictServiceImpl;
import com.bjxczy.onepark.common.constant.SysConst;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 *@ClassName SomeConfig
 *@Author 温良伟
 *@Date 2023/2/16 14:45
 *@Version 1.0
 */
@Configuration
public class SomeConfig {
    @Bean
    public DictService dictService() {
        return new JdbcDictServiceImpl(SysConst.DICT_TABLE_NAME); // 事先创建的字典表名称，建议写入配置文件@Value方式引入
    }
}
