package com.bjxczy.onepark.publicmodule.interfaces.feign.client;


import com.bjxczy.core.feign.client.voucher.BaseVoucherFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

/*
 *@Author wlw
 *@Date 2023/5/8 9:09  海康人脸校验接口服务
 */
@FeignClient("bmpvoucher")
public interface FaceFileFeignClient extends BaseVoucherFeignClient {
}
