package com.bjxczy.onepark.publicmodule.service;

import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;
import com.bjxczy.onepark.room.entity.ConferenceRoom;

public interface WorkOrderTemplateService {
    void TriggerWorkOrder(MeetingInfo info, UserInformation user, ConferenceRoom conferenceRoom);

    void cateringWorkOrder(MeetingInfo info, UserInformation user, ConferenceRoom conferenceRoom);

    void hotelWorkOrder(MeetingInfo info, UserInformation user, ConferenceRoom conferenceRoom);

    void distributeSweep(MeetingInfo info);
}
