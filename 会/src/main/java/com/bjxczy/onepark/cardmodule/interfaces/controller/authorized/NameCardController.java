package com.bjxczy.onepark.cardmodule.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.cardmodule.entity.MeetingCard;
import com.bjxczy.onepark.cardmodule.pojo.dto.BatchDownloadDto;
import com.bjxczy.onepark.cardmodule.service.MeetingCardService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/*
 *@Author wlw
 *@Date 2023/4/23 14:07 人名牌
 */
@Log4j2
@ApiResourceController
@RequestMapping("nameCard")
public class NameCardController {
    @Resource
    private MeetingCardService service;

    @PostMapping(value = "add", name = "新增人名牌")
    public R<?> addCard(@RequestBody MeetingCard card, @LoginUser(isFull = true) UserInformation user) {
        card.setOperator(user.getStaffName());
        card.setType(SysConst.TYPE_NAME_CARD);// 先增加类型 用于区分
        log.info("新增人名牌: {}", card);
        return R.success(service.addCard(card));
    }

    @GetMapping(value = "list", name = "人名牌列表")
    public R<?> list() {
        return R.success(service.list(SysConst.TYPE_NAME_CARD));
    }

    @DeleteMapping(value = "del/{id}", name = "人名牌-删除")
    public R<?> del(@PathVariable("id") String id) {
        return R.success(service.delete(id));
    }

    @PutMapping(value = "put", name = "人名牌-修改")
    public R<?> put(@RequestBody MeetingCard card) {
        return R.success(service.put(card));
    }


    @PostMapping(value = "/batchDownload", name = "批量下载")
    public void batchDownload(HttpServletResponse response,
                              @RequestBody BatchDownloadDto dto) {
        System.err.println(SysConst.stamp);
        service.batchDownload(response, dto);
    }
}
