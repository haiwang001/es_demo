package com.bjxczy.onepark.cardmodule.pojo.dto;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/5/11 15:14
 */
@Data
public class TextDto {
    private String text;
    private int width;
    private int height;
}
