package com.bjxczy.onepark.cardmodule.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.cardmodule.entity.MeetingCard;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/4/23 15:06
 */
@Mapper
public interface MeetingCardMapper extends BaseMapper<MeetingCard> {
    @Select("select count(name)\n" +
            "from m_meeting_card\n" +
            "where del_flag = 0\n" +
            "  and type = #{type}\n" +
            "  and name = #{name} order by create_time desc")
    Integer selectBy(@Param("type") String type, @Param("name") String name);

    @Select("select *\n" +
            "from m_meeting_card\n" +
            "where del_flag = 0\n" +
            "  and type = #{type}  order by create_time desc")
    List<MeetingCard> selectByType(@Param("type") String type);
}
