package com.bjxczy.onepark.cardmodule.pojo.dto;


import lombok.Data;

import java.awt.*;

/*
 *@Author wlw
 *@Date 2023/5/11 15:20
 */
@Data
public class ImageDTO {
    //文字内容
    private String text;
    //字体颜色和透明度
    private Color color;
    //字体和大小
    private Font font;
    private int width;
    private int height;

    public ImageDTO(String text, Color color, Font font, int width, int height) {
        this.text = text;
        this.color = color;
        this.font = font;
        this.width = width;
        this.height = height;
    }
}
