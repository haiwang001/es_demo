package com.bjxczy.onepark.cardmodule.service.impl;


import com.alibaba.nacos.common.utils.UuidUtils;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.utils.file.ImgUtils;
import com.bjxczy.onepark.cardmodule.entity.MeetingCard;
import com.bjxczy.onepark.cardmodule.mapper.CardFontMapper;
import com.bjxczy.onepark.cardmodule.mapper.MeetingCardMapper;
import com.bjxczy.onepark.cardmodule.pojo.dto.BatchDownloadDto;
import com.bjxczy.onepark.cardmodule.pojo.dto.ImageDTO;
import com.bjxczy.onepark.cardmodule.service.MeetingCardService;
/*import com.spire.doc.Document;
import com.spire.doc.Section;*/
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipOutputStream;

/*
 *@Author wlw
 *@Date 2023/4/23 14:48
 */
@Service
public class MeetingCardServiceImpl implements MeetingCardService {

    @Resource
    private CardFontMapper fontMapper;
    @Resource
    private MeetingCardMapper cardMapper;

    @Transactional(rollbackFor = ResultException.class)
    @Override
    public Object addCard(MeetingCard card) {
        String uid = UuidUtils.generateUuid();
        card = setMeetingCard(card, uid);
        // 校验 模板名称 同类型 同名 不能添加 超过0 表示存在同名模板
        if (cardMapper.selectBy(card.getType(), card.getName()) > 0) {
            throw new ResultException(card.getName() + "已存在");
        }
        // 入库数据
        card.getFontList().forEach(el -> {
            fontMapper.insert(el);
        });
        return cardMapper.insert(card);
    }

    public MeetingCard setMeetingCard(MeetingCard card, String uid) {
        card.setCreateTime(new Date());
        card.setCardId(uid);
        card.getFontList().forEach(el -> el.setCardId(uid));
        return card;
    }

    @Transactional(rollbackFor = ResultException.class)
    @Override
    public Object list(String type) {
        List<MeetingCard> list = cardMapper.selectByType(type);
        list.forEach(el -> el.setFontList(fontMapper.selectByCardId(el.getCardId())));
        return list;
    }

    @Override
    public Object delete(String id) {
        fontMapper.deleteByCardId(id);
        return cardMapper.deleteById(id);
    }

    @Override
    public Object put(MeetingCard card) {
        // 校验 模板名称 同类型 同名 不能添加 超过0 表示存在同名模板
        if (cardMapper.selectBy(card.getType(), card.getName()) > 0) {
            throw new ResultException(card.getName() + "已存在");
        }
        // 删除 文本
        fontMapper.deleteByCardId(card.getCardId());
        // 从新插入文本
        card.getFontList().forEach(el -> {
            el.setCardId(card.getCardId());
            fontMapper.insert(el);
        });
        // 修改本次记录
        return cardMapper.updateById(card);
    }

    @Override
    public void wordDownload(HttpServletResponse response, BatchDownloadDto dto) {
        ZipOutputStream zos = null;
        try {
            zos = new ZipOutputStream(response.getOutputStream());
            //获取数据集合；
            ZipOutputStream finalZos = zos;
            dto.getList().forEach(el -> {
                ArrayList<ImageDTO> list = new ArrayList<>();
                Font font = new Font("微软雅黑", Font.PLAIN, dto.getFontSize());
                Color color = new Color(dto.getR(), dto.getG(), dto.getB(), dto.getA());
                list.add(new ImageDTO(el.getText(), color, font, el.getWidth(), el.getHeight()));
                // 得到文件对象
               /* File file = ImgUtils.writeImage(dto.getBgUrl(), list);
                if (file != null) {
                    try {
                        // 给压缩包添加名字条目
                        finalZos.putNextEntry(new ZipEntry(file.getName()));
                        //将文件的二进制流，加入zip压缩包
                        finalZos.write(Files.readAllBytes(file.toPath()));
                        //结束当前ZipEntry
                        finalZos.closeEntry();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }*/
            });
            // 关闭 zip流 否则winrar解压打不开
            zos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void batchDownload(HttpServletResponse response, BatchDownloadDto dto) {
        try {
            XWPFDocument document = new XWPFDocument();
            dto.getList().forEach(el -> {
                ArrayList<ImageDTO> list = new ArrayList<>();
                Font font = new Font("方正魏碑简体", Font.PLAIN, dto.getFontSize());
                Color color = new Color(dto.getR(), dto.getG(), dto.getB(), dto.getA());
                list.add(new ImageDTO(el.getText(), color, font, el.getWidth(), el.getHeight()));
                // 得到文件对象
                XWPFParagraph paragraph = document.createParagraph();
                XWPFRun run = paragraph.createRun();
                try {
                    run.addPicture(ImgUtils.writeImage(dto.getBgUrl(), list, false), XWPFDocument.PICTURE_TYPE_PNG, "1.png", Units.toEMU(415), Units.toEMU(240));
                    run.addPicture(ImgUtils.writeImage(dto.getBgUrl(), list, true), XWPFDocument.PICTURE_TYPE_PNG, "1.png", Units.toEMU(415), Units.toEMU(240));
                } catch ( Exception e) {
                    e.printStackTrace();
                }
            });
            OutputStream outputStream = response.getOutputStream();
            document.write(outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
