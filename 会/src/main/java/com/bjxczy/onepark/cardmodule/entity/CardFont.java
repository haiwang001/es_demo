package com.bjxczy.onepark.cardmodule.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/23 13:32
 * 牌的文本设置 大小
 */
@Data
@TableName("m_card_font")
public class CardFont {
    @TableId
    private String cardId; // 模板主键 uuid card_id
    private String text; // 文本内容
    private Integer size; // 字号大小
    private String fontColor; // 字体颜色 font_color   字体颜色可调   v1.3.7
}
