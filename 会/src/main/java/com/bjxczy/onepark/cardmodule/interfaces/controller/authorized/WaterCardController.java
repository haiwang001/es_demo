package com.bjxczy.onepark.cardmodule.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.cardmodule.entity.MeetingCard;
import com.bjxczy.onepark.cardmodule.service.MeetingCardService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/*
 *@Author wlw
 *@Date 2023/4/23 14:07 水牌
 */
@Log4j2
@ApiResourceController
@RequestMapping("waterCard")
public class WaterCardController {
    @Resource
    private MeetingCardService service;

    @PostMapping(value = "add", name = "新增水牌")
    public R<?> addCard(@RequestBody MeetingCard card, @LoginUser(isFull = true) UserInformation user) {
        card.setOperator(user.getStaffName());
        card.setType(SysConst.TYPE_WATER_CARD);// 先增加类型 用于区分
        log.info("新增水牌: {}", card);
        return R.success(service.addCard(card));
    }


    @GetMapping(value = "list", name = "水牌列表")
    public R<?> list() {
        return R.success(service.list(SysConst.TYPE_WATER_CARD));
    }


    @DeleteMapping(value = "del/{id}", name = "水牌-删除")
    public R<?> del(@PathVariable("id") String id) {
        return R.success(service.delete(id));
    }

    @PutMapping(value = "put", name = "水牌-修改")
    public R<?> put(@RequestBody MeetingCard card) {
        return R.success(service.put(card));
    }

}
