package com.bjxczy.onepark.cardmodule.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.cardmodule.entity.CardFont;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

/*
 *@Author wlw
 *@Date 2023/4/23 15:07
 */
@Mapper
public interface CardFontMapper extends BaseMapper<CardFont> {
    @Select("select *\n" +
            "from m_card_font\n" +
            "where card_id = #{cardId}")
    ArrayList<CardFont> selectByCardId(@Param("cardId") String cardId);

    @Delete("delete from m_card_font\n" +
            "where card_id = #{cardId}")
    Integer deleteByCardId(@Param("cardId") String id);
}
