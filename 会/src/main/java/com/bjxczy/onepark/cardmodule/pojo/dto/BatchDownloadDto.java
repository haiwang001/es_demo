package com.bjxczy.onepark.cardmodule.pojo.dto;


import lombok.Data;

import java.util.ArrayList;

/*
 *@Author wlw
 *@Date 2023/5/11 15:13
 */
@Data
public class BatchDownloadDto {
    private String bgUrl;
    private ArrayList<TextDto> list;
    private int fontSize;
    private int r;
    private int g;
    private int b;
    private int a;
}
