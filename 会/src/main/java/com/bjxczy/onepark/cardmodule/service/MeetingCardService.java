package com.bjxczy.onepark.cardmodule.service;

import com.bjxczy.onepark.cardmodule.entity.MeetingCard;
import com.bjxczy.onepark.cardmodule.pojo.dto.BatchDownloadDto;

import javax.servlet.http.HttpServletResponse;

public interface MeetingCardService {
    Object addCard(MeetingCard card);

    Object list(String type);

    Object delete(String id);

    Object put(MeetingCard card);

    void batchDownload(HttpServletResponse response, BatchDownloadDto dto);

    void wordDownload(HttpServletResponse response, BatchDownloadDto dto);
}
