package com.bjxczy.onepark.cardmodule.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;

/*
 *@Author wlw
 *@Date 2023/4/23 13:23
 */
@Data
@TableName("m_meeting_card")
public class MeetingCard {
    @TableId
    private String cardId; // 模板主键 uuid card_id
    private String type; // 分类    1 人名牌  2  水牌
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;  //create_time

    private String name; // 模板名称
    private String url; // 牌的路径
    private String fontColor; // 字体颜色 font_color

    private String operator;
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag; // del_flag
    @TableField(exist = false) // 不是数据库字段：
    private ArrayList<CardFont> fontList;// 字体列表
}
