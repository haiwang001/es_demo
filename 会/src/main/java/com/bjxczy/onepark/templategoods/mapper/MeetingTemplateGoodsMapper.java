package com.bjxczy.onepark.templategoods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.templategoods.entity.MeetingTemplateGoods;
import com.bjxczy.onepark.templategoods.pojo.vo.MeetingTemplateGoodsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MeetingTemplateGoodsMapper extends BaseMapper<MeetingTemplateGoods> {


   @Select("select  b.goods_name,a.uni_valence,b.img_url,b.type_name,b.goods_id,a.amount,b.belong\n" +
           "from tbl_meeting_template_goods a\n" +
           "         left join tbl_meeting_goods b on a.goods_id = b.goods_id\n" +
           "where a.del_flag = 0\n" +
           "  and a.template_id = #{templateId}")
   List<MeetingTemplateGoodsVo> selectByTemplateId(@Param("templateId") String templateId);
}
