package com.bjxczy.onepark.templategoods.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/*
 *@ClassName MeetingTemplateGoods
 *@Author 温良伟
 *@Date 2023/2/17 15:21
 *@Version 1.0 会议模板和物品绑定表
 */
@Data
@TableName("tbl_meeting_template_goods")
public class MeetingTemplateGoods {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String templateId;
    private Integer delFlag;
    private Integer goodsId;
    private String goodsName;
    private Integer amount;
    private Double uniValence;
}
