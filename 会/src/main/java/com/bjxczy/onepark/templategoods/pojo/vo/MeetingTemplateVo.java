package com.bjxczy.onepark.templategoods.pojo.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import java.util.List;

/*
 *@ClassName MeetingTemplateVo
 *@Author 温良伟
 *@Date 2023/2/17 17:06
 *@Version 1.0
 */
@Data
public class MeetingTemplateVo {
    @TableId()
    private String templateId;
    private String templateName;
    private Boolean templateState;
    @TableField(exist = false) // 不是数据库字段：
    private List<MeetingTemplateGoodsVo> goodsList;
}
