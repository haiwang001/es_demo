package com.bjxczy.onepark.templategoods.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/27 14:25
 */
@Data
public class MeetingTemplateGoodsVo {
    private String goodsId;
    private String goodsName;
    private String imgUrl;
    private String typeName;
    private String uniValence;
    private String amount;
    private String belong; // 物品归属
}
