package com.bjxczy.onepark.templategoods.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
import java.util.List;

/*
 *@ClassName MeetingTemplate
 *@Author 温良伟
 *@Date 2023/2/17 15:18
 *@Version 1.0 会议模板表
 */
@Data
@TableName("tbl_meeting_template")
public class MeetingTemplate {
    @TableId()
    private String templateId;
    private String templateName;
    private Boolean templateState;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    private String operator;
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;

    @TableField(exist = false) // 不是数据库字段：
    private List<MeetingTemplateGoods> goodsList;
}
