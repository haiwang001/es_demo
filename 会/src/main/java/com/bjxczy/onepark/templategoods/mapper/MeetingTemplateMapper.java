package com.bjxczy.onepark.templategoods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.templategoods.entity.MeetingTemplate;
import com.bjxczy.onepark.templategoods.pojo.dto.MeetingTemplateDto;
import com.bjxczy.onepark.templategoods.pojo.vo.MeetingTemplateVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface MeetingTemplateMapper extends BaseMapper<MeetingTemplate> {

    @Update("update  tbl_meeting_template set template_state = #{dto.templateState} where template_id = #{dto.templateId} ")
    int updateByDto(@Param("dto") MeetingTemplateDto dto);

    @Select("select  * from  tbl_meeting_template where del_flag = 0 order by create_time desc ")
    List<MeetingTemplateVo> selectListByDto(@Param("dto")MeetingTemplateDto dto);

    @Select("select  * from  tbl_meeting_template where del_flag = 0 and template_state = true order by create_time desc ")
    List<MeetingTemplateVo> enableTemplate();
}
