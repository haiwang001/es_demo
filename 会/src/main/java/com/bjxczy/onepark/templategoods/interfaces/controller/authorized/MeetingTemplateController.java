package com.bjxczy.onepark.templategoods.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.templategoods.entity.MeetingTemplate;
import com.bjxczy.onepark.templategoods.pojo.dto.MeetingTemplateDto;
import com.bjxczy.onepark.templategoods.service.MeetingTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/*
 *@ClassName MeetingTemplateController
 *@Author 温良伟
 *@Date 2023/2/17 15:33
 *@Version 1.0 
 */
@ApiResourceController
@RequestMapping("MeetingTemplate")
public class MeetingTemplateController {

    @Autowired
    private MeetingTemplateService service;

    @PostMapping(value = "add", name = "新增会议模板")
    public R<?> addMeetingTemplate(@RequestBody MeetingTemplate template) throws Exception {
            return R.success(service.addMeetingTemplate(template));
    }

    @PutMapping(value = "updateState", name = "修改状态")
    public R<?> updateState(@RequestBody MeetingTemplateDto dto) {
            return R.success(service.updateState(dto));
    }

    /**/

    @GetMapping(value = "enableTemplate", name = "查询列表")
    public R<?> enableTemplate() {
            return R.success(service.enableTemplate());
    }

    @GetMapping(value = "list", name = "查询列表")
    public R<?> queryList(MeetingTemplateDto dto) throws Exception {
            return R.success(service.queryList(dto));
    }


    @PutMapping(value = "update/{templateId}", name = "模板编辑")
    public R<?> update(@PathVariable("templateId") String templateId,
                       @RequestBody MeetingTemplate template) throws Exception {
            template.setTemplateId(templateId);
            return R.success(service.update(template));
    }

    @DeleteMapping(value = "delete", name = "删除模板")
    public R<?> delete(@RequestBody MeetingTemplateDto dto) throws Exception {
            return R.success(service.delete(dto.getIds()));
    }

}
