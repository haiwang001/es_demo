package com.bjxczy.onepark.templategoods.service;

import com.bjxczy.onepark.templategoods.entity.MeetingTemplate;
import com.bjxczy.onepark.templategoods.pojo.dto.MeetingTemplateDto;

import java.util.List;


public interface MeetingTemplateService {
    int addMeetingTemplate(MeetingTemplate template) throws Exception;

    int updateState(MeetingTemplateDto dto);

    Object queryList(MeetingTemplateDto dto);

    int update(MeetingTemplate template) throws Exception;

    int delete(List<String> ids) throws Exception;

    Object enableTemplate();
}
