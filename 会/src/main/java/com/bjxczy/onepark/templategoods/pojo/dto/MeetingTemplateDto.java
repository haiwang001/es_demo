package com.bjxczy.onepark.templategoods.pojo.dto;


import com.bjxczy.onepark.publicmodule.pojo.dto.BasePageDTO;
import lombok.Data;

import java.util.List;

/*
 *@ClassName MeetingTemplateDto
 *@Author 温良伟
 *@Date 2023/2/17 16:38
 *@Version 1.0
 */
@Data
public class MeetingTemplateDto extends BasePageDTO {
    private String templateId;
    private Boolean templateState;
    private List<String> ids;
}
