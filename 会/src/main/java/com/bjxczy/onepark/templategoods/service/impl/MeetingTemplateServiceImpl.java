package com.bjxczy.onepark.templategoods.service.impl;


import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.templategoods.entity.MeetingTemplate;
import com.bjxczy.onepark.templategoods.entity.MeetingTemplateGoods;
import com.bjxczy.onepark.templategoods.mapper.MeetingTemplateGoodsMapper;
import com.bjxczy.onepark.templategoods.mapper.MeetingTemplateMapper;
import com.bjxczy.onepark.templategoods.pojo.dto.MeetingTemplateDto;
import com.bjxczy.onepark.templategoods.pojo.vo.MeetingTemplateVo;
import com.bjxczy.onepark.common.utils.sys.PageUtils;
import com.bjxczy.onepark.common.utils.uid.UuId;
import com.bjxczy.onepark.templategoods.service.MeetingTemplateService;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/*
 *@ClassName MeetingTemplateServiceImpl
 *@Author 温良伟
 *@Date 2023/2/17 15:37
 *@Version 1.0
 */
@Service
public class MeetingTemplateServiceImpl implements MeetingTemplateService {

    @Autowired
    private MeetingTemplateMapper mapper;

    @Autowired
    private MeetingTemplateGoodsMapper goodsMapper;

    @Override
    public Object enableTemplate() {
        List<MeetingTemplateVo> list = mapper.enableTemplate();
        for (MeetingTemplateVo vo : list) {
            vo.setGoodsList(goodsMapper.selectByTemplateId(vo.getTemplateId()));
        }
        return list;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int delete(List<String> ids) throws Exception {
        int i = -1;
        for (String id : ids) {
            MeetingTemplate select = mapper.selectById(id);
            if (select.getTemplateState()) {
                throw new ResultException("模板使用中 无法删除");
            }
            i = mapper.deleteById(id);
            goodsMapper.deleteByMap(ImmutableMap.of("template_id", id));
        }
        return i;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int update(MeetingTemplate template) throws Exception {
        template.setUpdateTime(new Date());
        mapper.updateById(template);
        // 查看模板是否存在
        List<MeetingTemplateGoods> list = template.getGoodsList();
        goodsMapper.deleteByMap(ImmutableMap.of("template_id", template.getTemplateId())); // 删除
        // 清除调原来的绑定数据从新添加
        for (MeetingTemplateGoods goods : list) {
            goods.setDelFlag(0);
            goods.setTemplateId(template.getTemplateId());
            List<MeetingTemplateGoods> select = goodsMapper.selectByMap(
                    ImmutableMap.of("template_id", template.getTemplateId(),
                            "goods_name", goods.getGoodsName(),
                            "del_flag", 0));
            if (select.size() == 0) {
                goods.setGoodsName(goods.getGoodsName());
                goodsMapper.insert(goods);
            } else {
                throw new ResultException("数据异常 信息存在重复");
            }
        }
        return 0;
    }

    @Override
    public Object queryList(MeetingTemplateDto dto) {
        List<MeetingTemplateVo> list = mapper.selectListByDto(dto);
        for (MeetingTemplateVo vo : list) {
            vo.setGoodsList(goodsMapper.selectByTemplateId(vo.getTemplateId()));
        }
        return PageUtils.Page(dto, list);
    }

    @Override
    public int updateState(MeetingTemplateDto dto) {
        dto.setTemplateState(!dto.getTemplateState());//修改状态
        return mapper.updateByDto(dto);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int addMeetingTemplate(MeetingTemplate template) throws Exception {
        String id = UuId.getId();
        template.setTemplateId(id);
        template.setCreateTime(new Date());
        template.setUpdateTime(new Date());
        template.setDelFlag(0);
        // 查看模板是否存在
        List<MeetingTemplate> byMap = mapper.selectByMap(ImmutableMap.of("template_name", template.getTemplateName()));
        if (byMap.size() > 1)
            throw new ResultException("模板已存在无法添加");
        int i = mapper.insert(template);
        List<MeetingTemplateGoods> list = template.getGoodsList();// 已经存在模板物品
        for (MeetingTemplateGoods goods : list) {
            goods.setDelFlag(0);
            goods.setTemplateId(id);
            goodsMapper.insert(goods);
        }
        return i;
    }
}
