package com.bjxczy.onepark;

import com.bjxczy.onepark.common.constant.SysConst;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/* <script src="https://cdn.staticfile.org/axios/0.18.0/axios.min.js"></script>*/

/*  按照页面拆分独立小功能模块 */
@Log4j2
@EnableCaching
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        System.err.println(SysConst.stamp);
    }
}
