package com.bjxczy.onepark.room.service;

import com.bjxczy.onepark.room.pojo.dto.RoomStateAndIdDto;
import com.bjxczy.onepark.room.entity.ConferenceRoom;
import com.bjxczy.onepark.room.pojo.dto.ConferenceRoomDto;
import com.bjxczy.onepark.room.pojo.vo.ConferenceRoomVo;

import java.util.List;

public interface ConferenceRoomService {

    int addConferenceRoom(ConferenceRoom conferenceRoom) throws Exception;

    int PutConferenceRoom(ConferenceRoom conferenceRoom) throws Exception;

    Object ConferenceRooPage(ConferenceRoomDto dto);

    int pudRoomState(String cId,RoomStateAndIdDto dto);

    List<ConferenceRoomVo> getRoom();

    Object startList();

    Object del(String cId);

    Object mobilestartList();
}
