package com.bjxczy.onepark.room.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.room.pojo.vo.BuildingVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface BuildingMapper extends BaseMapper<BuildingVo> {
    @Select("select  a.id area_id,a.name area_name,b.id building_id,b.name building_name,c.id layer_id,c.name layer_name  from\n" +
            "        authority.tbl_building_floor c , authority.tbl_building b ,authority.tbl_area a\n" +
            " where c.tenant_id = b.id and b.area_id = a.id and c.id = #{addressId}")
    BuildingVo getBuildingFloor(@Param("addressId") String addressId);

    @Select("select  a.id area_id,a.name area_name,b.id building_id,b.name building_name from\n" +
            "        authority.tbl_building b ,authority.tbl_area a\n" +
            " where  b.area_id = a.id and b.id = #{addressId}")
    BuildingVo getBuilding(@Param("addressId") String spaceId);

    @Select("select  a.id area_id,a.name area_name  from\n" +
            "        authority.tbl_area a  where  a.id = #{addressId}")
    BuildingVo getArea(@Param("addressId") String spaceId);
}
