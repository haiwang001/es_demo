package com.bjxczy.onepark.room.pojo.vo;


import lombok.Data;

/*
 *@ClassName Building
 *@Author 温良伟
 *@Date 2023/2/15 18:07
 *@Version 1.0
 */
@Data
public class BuildingVo {

    private String areaId; // 区域id
    private String areaName; // 区域名字
    private String buildingId; // 楼 编号或者名字
    private String buildingName; // 楼 编号或者名字
    private String layerId; //  具体某一层 编号或者名字
    private String layerName; //  具体某一层 编号或者名字

}
