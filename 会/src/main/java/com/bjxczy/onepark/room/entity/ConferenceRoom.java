package com.bjxczy.onepark.room.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/*
 *@ClassName ConferenceRoom
 *@Author 温良伟
 *@Date 2023/2/14 17:52
 *@Version 1.0
 * 会议室
 */
@Data
@TableName("tbl_conference_room")
public class  ConferenceRoom implements Serializable {

    private static final long serialVersionUID = -6531070848290724626L;
    @TableId
    private String cId; // 主键ID
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;//创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;//修改时间
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;//删除标记
    private String operator;//操作人
    private String roomName;//会议室名称
    private String addressId;//局址id
    private String regionId; // 区域id region_id
    private String buildingId; // 楼层 building_id
    private String spaceId; // 层数 space_id

    private String addressLabel;//局址展示
    private String roomType;//会议室类型
    private Double money;//会议室金额
    private Double halfDayMoney;//会议室半天金额  half_day_money
    private Double oneDayMoney;//会议室一天金额   one_day_money
    @TableField( updateStrategy = FieldStrategy.IGNORED)
    private String openDate;//会议室开放时间前
    @TableField( updateStrategy = FieldStrategy.IGNORED)
    private String endDate;//会议室开放时间后
    private Integer roomState;//会议室状态  2启用 1禁用
    private String hygieneState;//会议室卫生状态 1 未清扫 2已清扫
    private String capacity; // 会议室容纳人数

    @TableField(exist = false) // 不是数据库字段：
    private List<Integer> intendAntIds;//管理员id
    private String intendAntId;//管理员id

    @TableField(exist = false) // 不是数据库字段：
    private List<String> intendAntNames;//管理员名称 持久化
    private String intendAntName;//管理员名称 持久化

    @TableField(exist = false) // 不是数据库字段：
    private List<String> imgList;//会议室图片集合
    private String urlList;//路径集合 持久化

    @TableField(exist = false) // 不是数据库字段：
    private List<ConferenceRoomFacility> facilityList;//会议室设备

}
