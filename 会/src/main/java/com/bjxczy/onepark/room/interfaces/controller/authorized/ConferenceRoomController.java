package com.bjxczy.onepark.room.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.room.entity.ConferenceRoom;
import com.bjxczy.onepark.room.pojo.dto.ConferenceRoomDto;
import com.bjxczy.onepark.room.pojo.dto.RoomStateAndIdDto;
import com.bjxczy.onepark.room.service.ConferenceRoomService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/*
 *@ClassName ConferenceRoomCountller
 *@Author 温良伟
 *@Date 2023/2/15 12:02
 *@Version 1.0
 */
@ApiResourceController
@RequestMapping("ConferenceRoom")
public class ConferenceRoomController {
    @Resource
    private ConferenceRoomService service;


    @PostMapping(value = "add", name = "新增会议室")
    public R<?> addConferenceRoom(@RequestBody ConferenceRoom conferenceRoom) throws Exception {
        return R.success(service.addConferenceRoom(conferenceRoom));
    }


    @PutMapping(value = "update/{cId}", name = "编辑会议室")
    public R<?> PutConferenceRoom(
            @PathVariable("cId") String cId,
            @RequestBody ConferenceRoom conferenceRoom) throws Exception {
        conferenceRoom.setCId(cId);
        return R.success(service.PutConferenceRoom(conferenceRoom));
    }


    @GetMapping(value = "list", name = "会议室列表")
    public R<?> ConferenceRooPage(ConferenceRoomDto dto) {
        return R.success(service.ConferenceRooPage(dto));
    }

    @GetMapping(value = "startList", name = "开启的会议室")
    public R<?> startList() {
        return R.success(service.startList());
    }

    @GetMapping(value = "mobilestartList", name = "开启的会议室")
    public R<?> mobilestartList() {
        return R.success(service.mobilestartList());
    }

    @PutMapping(value = "pudRoomState/{cId}", name = "修改状态")
    public R<?> pudRoomState(@PathVariable("cId") String cId,
                             @RequestBody RoomStateAndIdDto dto) {
        return R.success(service.pudRoomState(cId, dto));
    }


    @DeleteMapping(value = "del/{cId}", name = "删除会议")
    public R<?> del(@PathVariable("cId") String cId) {
        return R.success(service.del(cId));
    }
}
