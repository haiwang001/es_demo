package com.bjxczy.onepark.room.service.impl;


import cn.hutool.core.date.DateTime;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.utils.date.DateTools;
import com.bjxczy.onepark.common.utils.json.JsonUtils;
import com.bjxczy.onepark.common.utils.sys.PageUtils;
import com.bjxczy.onepark.common.utils.uid.UuId;
import com.bjxczy.onepark.room.entity.ConferenceRoom;
import com.bjxczy.onepark.room.entity.ConferenceRoomFacility;
import com.bjxczy.onepark.room.mapper.BuildingMapper;
import com.bjxczy.onepark.room.mapper.ConferenceRoomFacilityMapper;
import com.bjxczy.onepark.room.mapper.ConferenceRoomMapper;
import com.bjxczy.onepark.room.pojo.dto.ConferenceRoomDto;
import com.bjxczy.onepark.room.pojo.dto.RoomStateAndIdDto;
import com.bjxczy.onepark.room.pojo.vo.BuildingVo;
import com.bjxczy.onepark.room.pojo.vo.ConferenceRoomVo;
import com.bjxczy.onepark.room.service.ConferenceRoomService;
import com.bjxczy.onepark.publicmodule.interfaces.feign.client.QueryStaffServer;
import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 *@ClassName 会议室
 *@Author 温良伟
 *@Date 2023/2/15 16:02
 *@Version 1.0
 */
@Service
public class ConferenceRoomServiceImpl implements ConferenceRoomService {
    @Resource
    private ConferenceRoomMapper mapper;
    @Resource
    private ConferenceRoomFacilityMapper FacilityMapper;
    @Resource
    private QueryStaffServer StaffServer;
    @Resource
    private BuildingMapper Building;

    @Override
    public Object del(String cId) {
        ConferenceRoom room = mapper.selectById(cId);
        if (2 == room.getRoomState()) {
            throw new ResultException("会议室启动中无法删除!");
        }
        return mapper.deleteById(cId);
    }

    @Override
    public List<ConferenceRoomVo> getRoom() {
        return mapper.getRoom();
    }

    @Override
    public int pudRoomState(String cId, RoomStateAndIdDto dto) {
        ConferenceRoom room = mapper.selectById(cId);
        if (room.getEndDate() != null && !"".equals(room.getEndDate())) {
            long current = new Date().getTime();
            if (current >= new DateTime(room.getEndDate()).getTime()) {
                room.setOpenDate(null);
                room.setEndDate(null);
            }
            if (new DateTime(room.getOpenDate()).getTime() > current) {
                throw new ResultException("开放时间未开始！无法启用");
            }
        }
        room.setRoomState(dto.isRoomState() ? 2 : 1);
        room.setHygieneState(dto.isRoomState() ? "使用中" : "禁用中");
        return mapper.updateById(room);
    }

    @Override
    public Object mobilestartList() {
        return toConferenceRoomVoLsit(mapper.startListBy());
    }

    private List<ConferenceRoomVo> toConferenceRoomVoLsit(List<ConferenceRoomVo> list) {
        for (ConferenceRoomVo vo : list) {
            vo.setImgList(JsonUtils.toBean(vo.getUrlList()));//返回图片
            vo.setIntendAntNames(JsonUtils.toBean(vo.getIntendAntName()));//返回名称
            ArrayList<Integer> integers = new Gson().fromJson(vo.getIntendAntId(), new TypeToken<List<Integer>>() {
            }.getType());
            vo.setIntendAntIds(integers);//返回id
            vo.setFacilityList(FacilityMapper.selectBycId(vo.getCId()));//返回设备对象
            vo.setTimeInterval(vo.getOpenDate() == null ? "无限制" : new DateTime(vo.getOpenDate()).toString(DateTools.FORMAT_02) + " ── " + new DateTime(vo.getEndDate()).toString(DateTools.FORMAT_02));
        }
        return list;
    }

    @Override
    public Object startList() {
        return toConferenceRoomVoLsit(mapper.startList());
    }

    @Override
    public Object ConferenceRooPage(ConferenceRoomDto dto) {
        List<ConferenceRoomVo> list = mapper.selectListByDto(dto);
        return PageUtils.Page(dto, toConferenceRoomVoLsit(list));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int PutConferenceRoom(ConferenceRoom conferenceRoom) throws Exception {
        conferenceRoom.setUpdateTime(new Date());//修改时间
        conferenceRoom.setRoomName(conferenceRoom.getRoomName().replaceAll("\\s*", ""));
        conferenceRoom.setHygieneState(conferenceRoom.getRoomState() == 1 ? "禁用中" : "使用中");//清扫状态描述
        conferenceRoom = setAddressLabel(conferenceRoom);
        // 封装 管理员姓名
        ArrayList<String> list = new ArrayList<>();
        //查询组织架构员工
        List<Integer> intendAntIds = conferenceRoom.getIntendAntIds();
        for (Integer id : intendAntIds) {
            try {
                StaffInfoFine staff = StaffServer.getStaffById(id);
                list.add(staff.getFldName());
            } catch (RuntimeException e) {
                throw new ResultException("系统内部远程调用异常 服务器可能未启动" + e.toString());
            }
        }
        //id 字符串转
        conferenceRoom.setIntendAntId(JsonUtils.toJsonString(conferenceRoom.getIntendAntIds()));
        //封装成字符串存储
        conferenceRoom.setIntendAntName(JsonUtils.toJsonString(list));
        // 图片路径封装字符串
        conferenceRoom.setUrlList(JsonUtils.toJsonString(conferenceRoom.getImgList()));
        // 删除之前关联的物品
        int i = FacilityMapper.deleteBycId(conferenceRoom.getCId());
        // 持久化会议室物品
        List<ConferenceRoomFacility> facilities = conferenceRoom.getFacilityList();
        for (ConferenceRoomFacility facility : facilities) {
            facility.setCId(conferenceRoom.getCId());//封装绑定会议室id
            FacilityMapper.insert(facility);
        }
        ConferenceRoom select = mapper.selectById(conferenceRoom.getCId());
        mapper.deleteByCId(conferenceRoom.getCId());
        conferenceRoom.setCreateTime(select.getCreateTime());
        conferenceRoom.setDelFlag(select.getDelFlag());
        conferenceRoom.setIntendAntNames(select.getIntendAntNames());
        return mapper.insert(conferenceRoom);
    }

    public ConferenceRoom setAddressLabel(ConferenceRoom conferenceRoom) {
        try {
            BuildingVo building = Building.getBuildingFloor(conferenceRoom.getAddressId());
            if (building != null) {
                String str = building.getAreaName() + "-" + building.getBuildingName() + "-" + building.getLayerName();
                conferenceRoom.setAddressLabel(str);
                conferenceRoom.setRegionId(building.getAreaId());
                conferenceRoom.setBuildingId(building.getBuildingId());
                conferenceRoom.setSpaceId(building.getLayerId());
                return conferenceRoom;
            }
        } catch (Exception e) {
            // e.printStackTrace();
        }

        try {
            BuildingVo building1 = Building.getBuilding(conferenceRoom.getAddressId());
            if (building1 != null) {
                String str = building1.getAreaName() + "-" + building1.getBuildingName();
                conferenceRoom.setAddressLabel(str);
                conferenceRoom.setRegionId(building1.getAreaId());
                conferenceRoom.setBuildingId(building1.getBuildingId());
                return conferenceRoom;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            BuildingVo area = Building.getArea(conferenceRoom.getAddressId());
            if (area != null) {
                conferenceRoom.setAddressLabel(area.getAreaName());
                conferenceRoom.setRegionId(area.getAreaId());
                return conferenceRoom;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conferenceRoom;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int addConferenceRoom(ConferenceRoom conferenceRoom) throws Exception {
        conferenceRoom.setCId(UuId.getId()); //
        conferenceRoom.setCreateTime(new Date());
        conferenceRoom.setUpdateTime(new Date());
        conferenceRoom.setDelFlag(0);
        conferenceRoom.setRoomName(conferenceRoom.getRoomName().replaceAll("\\s*", ""));
        conferenceRoom.setHygieneState(conferenceRoom.getRoomState() == 1 ? "禁用中" : "使用中");//清扫状态描述
        // 获取局址 拼接
        conferenceRoom = setAddressLabel(conferenceRoom);
        // 封装 管理员姓名
        ArrayList<String> list = new ArrayList<>();
        //查询组织架构员工
        List<Integer> intendAntIds = conferenceRoom.getIntendAntIds();
        for (Integer id : intendAntIds) {
            StaffInfoFine staff = StaffServer.getStaffById(id);
            list.add(staff.getFldName());
        }
        //id 字符串转
        conferenceRoom.setIntendAntId(JsonUtils.toJsonString(conferenceRoom.getIntendAntIds()));
        //封装成字符串存储
        conferenceRoom.setIntendAntName(JsonUtils.toJsonString(list));
        // 图片路径封装字符串
        conferenceRoom.setUrlList(JsonUtils.toJsonString(conferenceRoom.getImgList()));
        // 持久化会议室物品
        List<ConferenceRoomFacility> facilities = conferenceRoom.getFacilityList();
        for (ConferenceRoomFacility facility : facilities) {
            facility.setCId(conferenceRoom.getCId());//封装绑定会议室id
            FacilityMapper.insert(facility);
        }
        // 持久化前要先判断是否存在同样的会议室
        List<ConferenceRoom> roomList = mapper.selectByMap(ImmutableMap.of(
                "room_name", conferenceRoom.getRoomName(),
                "address_id", conferenceRoom.getAddressId(),
                "address_label", conferenceRoom.getAddressLabel()));
        if (roomList.size() >= 1) {
            throw new ResultException("该区域下存在同名会议室添加失败");
        }
        return mapper.insert(conferenceRoom);
    }


}
