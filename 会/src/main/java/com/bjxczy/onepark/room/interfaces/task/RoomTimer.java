package com.bjxczy.onepark.room.interfaces.task;


import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.onepark.room.entity.ConferenceRoom;
import com.bjxczy.onepark.room.mapper.ConferenceRoomMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Log4j2
@Configuration
@EnableScheduling
public class RoomTimer {
    @Resource
    private ConferenceRoomMapper RoomMapper; // 联动待清扫

    @Scheduled(fixedDelay = 1000 * 60)
    public void sayWord() {
        init();
    }

    private void init() {
        QueryWrapper<ConferenceRoom> wrapper = new QueryWrapper<>();
        wrapper.eq("del_flag", 0).eq("room_state", 2);
        List<ConferenceRoom> list = RoomMapper.selectList(wrapper);
        for (ConferenceRoom room : list) {
            if (room.getEndDate() != null && !"".equals(room.getEndDate())) {
                long expire = new DateTime(room.getEndDate()).getTime();
                if (new Date().getTime()>=expire) {// 修改会为禁用
                    RoomMapper.updateRoomStateById(room.getCId(),1);
                }
            }
        }
    }
}
