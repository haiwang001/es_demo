package com.bjxczy.onepark.room.entity;



import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/*
 *@ClassName ConferenceRoomFacility
 *@Author 温良伟
 *@Date 2023/2/14 18:06
 *@Version 1.0
 * 会议室的设备
 */
@Data
@TableName("tbl_conference_room_facility")
public class ConferenceRoomFacility implements Serializable {
    private static final long serialVersionUID = -4525496928538837922L;
    private  String cId;//绑定的会议室id
    private  String name;//设备名称
    private  Double money;//设备金额
    private  Integer number;//设备数量

}
