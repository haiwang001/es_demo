package com.bjxczy.onepark.room.pojo.dto;


import com.bjxczy.onepark.publicmodule.pojo.dto.BasePageDTO;
import lombok.Data;

/*
 *@ClassName ConferenceRoomDto
 *@Author 温良伟
 *@Date 2023/2/16 12:44
 *@Version 1.0
 */
@Data
public class ConferenceRoomDto extends BasePageDTO {
    private String addressId;//局址id
    private String openDate;//会议室开放时间前
    private String endDate;//会议室开放时间后
    private Integer roomState;//会议室状态
    private String hygieneState;//会议室卫生状态 1 未清扫 2已清扫 描述
    private String roomType;//会议室类型
}
