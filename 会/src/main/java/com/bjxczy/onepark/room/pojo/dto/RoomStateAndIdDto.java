package com.bjxczy.onepark.room.pojo.dto;


import lombok.Data;

/*
 *@ClassName RoomStateAndID
 *@Author 温良伟
 *@Date 2023/2/16 15:42
 *@Version 1.0
 */
@Data
public class RoomStateAndIdDto {
    private boolean roomState;
}
