package com.bjxczy.onepark.room.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.room.entity.ConferenceRoom;
import com.bjxczy.onepark.room.pojo.dto.ConferenceRoomDto;
import com.bjxczy.onepark.room.pojo.vo.ConferenceRoomVo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ConferenceRoomMapper extends BaseMapper<ConferenceRoom> {

    @Select("select a.*,b.name roomTypeLabel \n" +
            "from tbl_conference_room a \n" +
            "    left join m_seat_table b on  a.room_type =  b.id\n" +
            "                        where a.del_flag = 0\n" +
            "                           and if(#{dto.addressId} != '', \n" +
            "                               a.region_id like concat('%', #{dto.addressId}, '%')   \n" +
            "                            or a.building_id = concat('', #{dto.addressId}, '')\n" +
            "                            or a.space_id = concat('', #{dto.addressId}, '')  \n" +
            "                              ,true)\n" +
            "                          and if(#{dto.roomState} != '', a.room_state = concat('', #{dto.roomState}, ''), true)\n" +
            "                          and if(#{dto.hygieneState} != '' , a.hygiene_state like concat('%', #{dto.hygieneState}, '%'), true)\n" +
            "                          and if(#{dto.roomType} != '' , a.room_type = concat('', #{dto.roomType}, ''), true)\n" +
            "                          and if(#{dto.openDate} != '', DATE_FORMAT(a.open_date, '%Y-%m-%d %H:%i') >=  #{dto.openDate}, true)\n" +
            "                          and if(#{dto.endDate} != '', DATE_FORMAT(a.open_date, '%Y-%m-%d %H:%i') <=  #{dto.endDate}, true)\n" +
            "                        group by a.c_id  order by a.create_time desc")
    List<ConferenceRoomVo> selectListByDto(@Param("dto") ConferenceRoomDto dto);

/*    @Update("update  tbl_conference_room set hygiene_state =#{hygiene_state}, room_state = #{room_state} where c_id =#{cId}")
    int updateById(@Param("cId") String cId, @Param("room_state") Integer room_state, @Param("hygiene_state") String hygiene_state);*/

    @Delete("delete  from  tbl_conference_room where c_id =  #{cId}")
    int deleteByCId(@Param("cId") String cId);

    @Select("select  c_id,room_name from tbl_conference_room where  del_flag = 0 and room_state = true  order by create_time desc")
    List<ConferenceRoomVo> getRoom();

    @Update("update  tbl_conference_room set hygiene_state =#{msg} where c_id =#{roomId}")
    int updateHygieneState(@Param("roomId") String roomId, @Param("msg") String msg);

    @Select("select a.*,b.name roomTypeLabel from tbl_conference_room a left join m_seat_table b on a.room_type = b.id\n" +
            "                        where a.del_flag = 0 and a.room_state = 2\n" +
            "                        group by a.c_id  order by a.hygiene_state desc")
    List<ConferenceRoomVo> startList();


    @Select("select a.*,b.name roomTypeLabel from tbl_conference_room a left join m_seat_table b on a.room_type = b.id\n" +
            "                                    where a.del_flag = 0 and a.room_state = 2 and a.room_type = '999999'\n" +
            "                                    group by a.c_id  order by a.hygiene_state desc")
    List<ConferenceRoomVo> startListBy();

    @Select("select  name from  m_seat_table where del_flag = 0 and id = #{roomType} ")
    String selectBy(@Param("roomType") String roomType);


    @Select("select  name  from  authority.tbl_area where id = #{regionId}")
    String setRegionNameByRegionId(@Param("regionId") String regionId);

    @Select("select  name  from  authority.tbl_building where id = #{buildingId}")
    String getBuildingNameByBuildingId(@Param("buildingId") String buildingId);

    @Select("select  name  from  authority.tbl_building_floor where id = #{spaceId}")
    String getSpaceNameBySpaceId(@Param("spaceId") String spaceId);

    @Update("update tbl_conference_room set  room_state = #{i},hygiene_state='禁用中' where c_id = #{cId}")
    void updateRoomStateById(@Param("cId") String cId, @Param("i") int i);

}
