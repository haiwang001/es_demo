package com.bjxczy.onepark.room.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.room.entity.ConferenceRoomFacility;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ConferenceRoomFacilityMapper extends BaseMapper<ConferenceRoomFacility> {
    // 清除之前存储的物品绑定
    @Delete("delete from office.tbl_conference_room_facility  where c_id =#{cid}")
    int deleteBycId(@Param("cid") String cId);

    @Select("select *from tbl_conference_room_facility where c_id = #{cid}")
    List<ConferenceRoomFacility> selectBycId(@Param("cid") String cId);
}
