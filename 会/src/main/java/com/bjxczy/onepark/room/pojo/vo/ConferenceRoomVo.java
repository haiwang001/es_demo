package com.bjxczy.onepark.room.pojo.vo;


import com.bjxczy.onepark.room.entity.ConferenceRoom;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *@ClassName ConferenceRoomVo
 *@Author 温良伟
 *@Date 2023/2/16 12:45
 *@Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ConferenceRoomVo  extends ConferenceRoom {
    private static final long serialVersionUID = -6768039265257299554L;
    private String timeInterval;//会议室开放时间区间
    private String roomTypeLabel;//会议室开放时间区间
}
