package com.bjxczy.onepark.common.utils.json;


import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/*
 *@ClassName JsonUtiles
 *@Description TODO
 *@Author 温良伟
 *@Date 2023/2/15 16:51
 *@Version 1.0
 */
public class JsonUtils {

    public static String toJsonString(Object o) {
        return new Gson().toJson(o);
    }

    public static ArrayList toBean(String json) {
        return new Gson().fromJson(json, ArrayList.class);
    }

    public static ArrayList<Integer> toBeanArrInt(String json) {
        return new Gson().fromJson(json, ArrayList.class);
    }

    public static HashMap toBeanMap(String json) {
        return new Gson().fromJson(json, HashMap.class);
    }

    public static void main(String[] args) {
        HashMap<Integer, Boolean> map = new HashMap<>();
        map.put(1, true);
        map.put(2, false);
        map.put(3, false);
        String s = toJsonString(map);
        System.out.println(s);
        HashMap hashMap = toBeanMap(s);
        System.out.println(hashMap);
    }
}
