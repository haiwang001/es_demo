package com.bjxczy.onepark.common.utils.file;


import com.alibaba.nacos.api.utils.StringUtils;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.utils.sys.StaffUtils;
import com.bjxczy.onepark.cardmodule.pojo.dto.ImageDTO;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.entity.ContentType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/*
 *@Author wlw
 *@Date 2023/5/11 9:40
 */


@Log4j2
public class ImgUtils {

    /**
     * 编辑图片,往指定位置添加文字
     *
     * @param srcImgPath :源图片路径
     * @param list       :文字集合
     * @param reverse    : true 正常  false 镜像翻转
     */
    public static InputStream writeImage(String srcImgPath, ArrayList<ImageDTO> list, boolean reverse) {
        try {
            Image srcImg = null;//文件转化为图片
            try {
                srcImg = ImageIO.read(StaffUtils.getFileBase64File(srcImgPath));
            } catch (IOException e) {
                throw new ResultException("请选择背景图片！");
            }
            int srcImgWidth = srcImg.getWidth(null);//获取图片的宽
            int srcImgHeight = srcImg.getHeight(null);//获取图片的高
            //添加文字:
            BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufImg.createGraphics();
            g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
            for (ImageDTO imgDTO : list) {
                g.setColor(imgDTO.getColor());                                  //根据图片的背景设置水印颜色
                g.setFont(imgDTO.getFont());                                    //设置字体
                g.drawString(imgDTO.getText(),
                        (srcImgWidth - imgDTO.getWidth()) / 2,                //左右居中
                        (srcImgHeight + imgDTO.getHeight() / 2) - 100);
            }
            g.dispose();
            if (!reverse) {
                bufImg = modifyImageRatio(bufImg, 180);
            }
            // BufferedImage 转字节数组
            byte[] bytes = bufferedImageToByteArray(bufImg);
            log.info("bytes " + Arrays.toString(bytes));

            return new ByteArrayInputStream(bytes);
            // 字节数组 转 InputStream 流
          /*  InputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

            // InputStream 流 转  MultipartFile
            MultipartFile fileResult = new MockMultipartFile("file", filename,
            ContentType.APPLICATION_OCTET_STREAM.toString(), byteArrayInputStream);

            // MultipartFile 转 File
            return MultipartFileToFile(fileResult, prefix);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static BufferedImage modifyImageRatio(BufferedImage mini, int ratio) {
        int src_width = mini.getWidth();
        int src_height = mini.getHeight();
        //针对图片旋转重新计算图的宽*高
        Rectangle rect_des = CalcRotatedSize(new Rectangle(new Dimension(
                src_width, src_height)), ratio);
        //设置生成图片的宽*高，色彩度
        BufferedImage res = new BufferedImage(rect_des.width, rect_des.height, BufferedImage.TYPE_INT_RGB);
        //创建画布
        Graphics2D g2 = res.createGraphics();
        res = g2.getDeviceConfiguration().createCompatibleImage(rect_des.width, rect_des.height, Transparency.TRANSLUCENT);
        g2 = res.createGraphics();
        //重新设定原点坐标
        g2.translate((rect_des.width - src_width) / 2,
                (rect_des.height - src_height) / 2);
        //执行图片旋转，rotate里包含了translate，并还原了原点坐标
        g2.rotate(Math.toRadians(ratio), src_width / 2, src_height / 2);
        g2.drawImage(mini, null, null);
        g2.dispose();
        return res;
    }


    /**
     * 对图片进行旋转
     *
     * @param src 被旋转图片
     * @param deg 旋转角度
     * @return 旋转后的图片
     */
    public static BufferedImage Rotate(Image src, int deg) {
        int src_width = src.getWidth(null);
        int src_height = src.getHeight(null);
        // 计算旋转后图片的尺寸
        Rectangle rect_des = CalcRotatedSize(new Rectangle(new Dimension(src_width, src_height)), deg);
        BufferedImage res = new BufferedImage(rect_des.width, rect_des.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = res.createGraphics();
        // 进行转换
        g2.translate((rect_des.width - src_width) / 2, (rect_des.height - src_height) / 2);
        g2.rotate(Math.toRadians(deg), src_width / 2, src_height / 2);
        g2.drawImage(src, null, null);
        return res;
    }

    /**
     * 将BufferedImage转换为byte[]
     *
     * @param image
     * @return
     */
    public static byte[] bufferedImageToByteArray(BufferedImage image) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "png", os);
        return os.toByteArray();
    }

    /**
     * @param multiFile 转换前
     * @param prefix    后缀
     * @return
     */
    public static File MultipartFileToFile(MultipartFile multiFile, String prefix) {
        // 获取文件名
        String fileName = multiFile.getOriginalFilename();
        // 若须要防止生成的临时文件重复,能够在文件名后添加随机码
        try {
            assert fileName != null;
            File file = File.createTempFile(fileName, prefix);
            multiFile.transferTo(file);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String formatFullMisCode(String misCode) {
        if (StringUtils.isBlank(misCode)) {
            return null;
        } else if (misCode.contains("E")) {
            return misCode;
        }
        return "E00" + misCode;
    }

    /**
     * 文件File类型转byte[]
     *
     * @param file
     * @return
     */
    private static byte[] fileToByte(File file) {
        byte[] fileBytes = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            fileBytes = new byte[(int) file.length()];
            fis.read(fileBytes);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileBytes;
    }


    /**
     * @param url 远端文件Url
     * @return File
     */
    public static String getFileBase64ToString(String url) {
        //对本地文件命名
        String fileName = url.substring(url.lastIndexOf("."), url.length());
        File file = null;
        URL urlfile;
        try {
            // 创建一个临时路径
            file = File.createTempFile("file", fileName);
            //下载
            urlfile = new URL(url);
            try (InputStream inStream = urlfile.openStream();
                 OutputStream os = new FileOutputStream(file);) {
                int bytesRead = 0;
                byte[] buffer = new byte[8192];
                while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
            }
        } catch (Exception ignored) {
        }
        return Base64.encodeBase64String(fileToByte(file));
    }


    public static File getFileBase64ToFile(String url) {
        //对本地文件命名
        String fileName = url.substring(url.lastIndexOf("."), url.length());
        File file = null;
        URL urlfile;
        try {
            // 创建一个临时路径
            file = File.createTempFile("file", fileName);
            //下载
            urlfile = new URL(url);
            try (InputStream inStream = urlfile.openStream();
                 OutputStream os = new FileOutputStream(file);) {
                int bytesRead = 0;
                byte[] buffer = new byte[8192];
                while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
            }
        } catch (Exception ignored) {
        }
        return file;
    }


    /**
     * @param filePath 原图
     */
    public static MultipartFile compressionByUrl(String filePath, Float multiplier) {
        try {
            File file = StaffUtils.getFileBase64File(filePath);
            return compression(file, multiplier);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param multiFile 原图
     */
    public static MultipartFile compressionByMultipartFile(MultipartFile multiFile, Float multiplier) {
        try {
            File file = MultipartFileToFile(multiFile);
            assert file != null;
            return compression(file, multiplier);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static MultipartFile compression(File file, Float multiplier) throws IOException {
        BufferedImage image = ImageIO.read(file);
        // 得到指定Format图片的writer（迭代器）
        Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpg");
        // 得到writer
        ImageWriter writer = iter.next();
        // 得到指定writer的输出参数设置(ImageWriteParam )
        ImageWriteParam iwp = writer.getDefaultWriteParam();
        // 设置可否压缩
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        // 设置压缩质量参数
        iwp.setCompressionQuality(multiplier);
        iwp.setProgressiveMode(ImageWriteParam.MODE_DISABLED);
        ColorModel colorModel = ColorModel.getRGBdefault();
        // 指定压缩时使用的色彩模式
        iwp.setDestinationType(
                new javax.imageio.ImageTypeSpecifier(colorModel, colorModel.createCompatibleSampleModel(16, 16)));
        // 开始打包图片，写入byte[]
        // 取得内存输出流
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        IIOImage iIamge = new IIOImage(image, null, null);
        // 此处因为ImageWriter中用来接收write信息的output要求必须是ImageOutput
        // 通过ImageIo中的静态方法，得到byteArrayOutputStream的ImageOutput
        writer.setOutput(ImageIO.createImageOutputStream(byteArrayOutputStream));
        writer.write(null, iIamge, iwp);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        // 字节数组 转 InputStream 流
        InputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        // InputStream 流 转  MultipartFile
        return new MockMultipartFile("file", file.getName(),
                ContentType.APPLICATION_OCTET_STREAM.toString(), byteArrayInputStream);

    }

    public static void main(String[] args) {
        try {

            new File("http://192.165.1.62:9881/group1/M00/00/B4/wKUBPmSIK32AIWsWAAFplblNS5k750.jpg");
            new File("http://192.165.1.166:6040/pic?3d00=8002l5a-do391a*23d2f6f7-5915b98b9*012s=**613==tp*6318=7682661*7286=3l0o6*657-=85de45pi18bo=0-fe0000");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param multiFile 转换前
     * @return
     */
    public static File MultipartFileToFile(MultipartFile multiFile) {
        // 获取文件名
        String fileName = multiFile.getOriginalFilename();
        // 若须要防止生成的临时文件重复,能够在文件名后添加随机码
        try {
            assert fileName != null;
            File file = File.createTempFile(fileName, ".jpg");
            multiFile.transferTo(file);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 计算旋转后的图片
     *
     * @param src   被旋转的图片
     * @param angel 旋转角度
     * @return 旋转后的图片
     */
    public static Rectangle CalcRotatedSize(Rectangle src, int angel) {
        // 如果旋转的角度大于90度做相应的转换
        if (angel >= 90) {
            if (angel / 90 % 2 == 1) {
                int temp = src.height;
                src.height = src.width;
                src.width = temp;
            }
            angel = angel % 90;
        }

        double r = Math.sqrt(src.height * src.height + src.width * src.width) / 2;
        double len = 2 * Math.sin(Math.toRadians(angel) / 2) * r;
        double angel_alpha = (Math.PI - Math.toRadians(angel)) / 2;
        double angel_dalta_width = Math.atan((double) src.height / src.width);
        double angel_dalta_height = Math.atan((double) src.width / src.height);

        int len_dalta_width = (int) (len * Math.cos(Math.PI - angel_alpha
                - angel_dalta_width));
        int len_dalta_height = (int) (len * Math.cos(Math.PI - angel_alpha
                - angel_dalta_height));
        int des_width = src.width + len_dalta_width * 2;
        int des_height = src.height + len_dalta_height * 2;
        return new Rectangle(new Dimension(des_width, des_height));
    }
}
