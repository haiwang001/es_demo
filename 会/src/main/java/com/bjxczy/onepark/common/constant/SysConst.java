package com.bjxczy.onepark.common.constant;


import com.bjxczy.onepark.common.utils.spring.SpringContextUtil;
import com.bjxczy.onepark.hikmodule.config.HikConference;

import java.net.InetAddress;
import java.net.UnknownHostException;

/*
 *@ClassName SysConst
 *@Date 2023/1/19 17:21
 *@Version 1.0
 */
public class SysConst {
    public static final Integer NEGATIVE_INTEGER = -1;// 负整数
    public static final Integer POSITIVE_INTEGER = 1;//  正整数
    // 字典表名字
    public static final String DICT_TABLE_NAME = "tbl_office_dict";
    public static final String BUSINESS_ID = "office_meeting_module";
    public static final String CREATE_MEETING = "create_meeting_event";
    // 文件名称
    public static final String MEETING_FILE_NAME = "会议信息";
    public static final String MEETING_SIGN_IN = "会议签到";
    public static final String GOODS_FILE_NAME = "会议室物品信息";
    public static final String CREW_FILE_NAME = "会议人员模板";
    public static final String TYPE_WATER_CARD = "1";
    public static final String TYPE_NAME_CARD = "2";
    public static final String HTTPS = "https://";
    public static final String HTTP = "http://";

    public static final Integer EARLIER_DATE = NEGATIVE_INTEGER * 15;// 提前 15 时间


    public static String HIK_IP_PORT;
    public static String LOCAL_HOST;

    static {
        HIK_IP_PORT = SpringContextUtil.getBean(HikConference.class).getHostService();
        try {// 内部访问主机ip
            LOCAL_HOST = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public static final String HIK_CALLBACK_CONTROLLER = "HikCallBack";
    public static final String HIK_CALLBACK_SIGNIN = "signIn";
    // 查询海康会议签到一体机api  get
    public static final String HIK_SMART_ROOM_DEVICE_SEARCH_API = HTTP + HIK_IP_PORT + "/smartRoom/v1/device/search";
    // 签到签出事件订阅  post
    public static final String HIK_SMART_ROOM_EVENT_API = HTTP + HIK_IP_PORT + "/smartRoom/v1/subEvent";
    // 会议一体机增加用户  post
    public static final String HIK_SMART_ROOM_ADD_USER_API = HTTP + HIK_IP_PORT + "/isupserver/api/mas/v1/meet/addUserInfo";
    // 会议一体机删除用户  post
    public static final String HIK_SMART_ROOM_DEL_USER_API = HTTP + HIK_IP_PORT + "/isupserver/api/mas/v1/meet/deleteUserInfo";
    // 会议一体机增加人脸 to 会议管理人脸库  post
    public static final String HIK_SMART_ROOM_ADD_FACE_API = HTTP + HIK_IP_PORT + "/isupserver/api/mas/v1/meet/uploadFacePictureByPicBase64";
    // 会议一体机删除人脸  post
    public static final String HIK_SMART_ROOM_DEL_FACE_API = HTTP + HIK_IP_PORT + "/isupserver/api/mas/v1/meet/deleteFaceDataRecord";
    // 绑定 人脸和设备和人员  post
    public static final String HIK_SMART_ROOM_ADD_FACE_DATA_RECORD_API = HTTP + HIK_IP_PORT + "/isupserver/api/mas/v1/meet/addFaceDataRecord";
    // 创建会议室  post
    public static final String HIK_MEETING_CREATE_API = HTTP + HIK_IP_PORT + "/smartRoom/v1/meeting/create";
    // 清除人员未完成


    // 固定死的工单名称
    public static final String HOTEL_NAME = "酒店工单";
    public static final String HOUSING_NAME = "餐饮工单";
    public static final String CONFERENCE_NAME = "会议工单";


    public static final String stamp =
            "　   ┏┓　 ┏┓+ +\n" +
                    " 　┏┛┻━━━┛┻┓ + +\n" +
                    " 　┃　　　　　　　┃ 　\n" +
                    " 　┃　　　━　　　┃ ++ + + +\n" +
                    "  ████━████ ┃+\n" +
                    " 　┃　　　　　　　┃ +\n" +
                    " 　┃　　　┻　　　┃\n" +
                    " 　┃　　　　　　　┃ + +\n" +
                    " 　┗━┓　　　┏━┛\n" +
                    " 　　　┃　　　┃　　　　　　　　　　　\n" +
                    " 　　　┃　　　┃ + + + +\n" +
                    " 　　　┃　　　┃\n" +
                    " 　　　┃　　　┃ +  \n" +
                    " 　　　┃　　　┃  +  　　\n" +
                    " 　　　┃　　　┃　　+　  成功　　　　　　　　\n" +
                    " 　　　┃　 　　┗━━━┓ + +\n" +
                    " 　　　┃ 　　　　　　　┣┓\n" +
                    " 　　　┃ 　　　　　　　┏┛\n" +
                    " 　　　┗┓┓┏━┳┓┏┛ + + + +\n" +
                    " 　　　　┃┫┫　┃┫┫\n " +
                    " 　　　　┗┻┛　┗┻┛+ + + +";
}
