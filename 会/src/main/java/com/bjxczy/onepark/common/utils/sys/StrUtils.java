package com.bjxczy.onepark.common.utils.sys;


import com.bjxczy.onepark.common.model.organization.DeptNameInfo;

import java.util.ArrayList;
import java.util.List;

/*
 *@ClassName StringUtils
 *@Description TODO
 *@Author 温良伟
 *@Date 2023/1/19 15:16
 *@Version 1.0
 */
public class StrUtils {




    public static void main(String[] args) {
        ArrayList<DeptNameInfo> arrayList = new ArrayList<>();
        DeptNameInfo deptNameInfo1 = new DeptNameInfo();
        deptNameInfo1.setId(6);
        deptNameInfo1.setDeptName("第三级部门");

        DeptNameInfo deptNameInfo2 = new DeptNameInfo();
        deptNameInfo2.setId(5);
        deptNameInfo2.setDeptName("第二级部门");

        DeptNameInfo deptNameInfo3 = new DeptNameInfo();
        deptNameInfo3.setId(5);
        deptNameInfo3.setDeptName("第一级部门");
        arrayList.add(deptNameInfo3);
        arrayList.add(deptNameInfo2);
        arrayList.add(deptNameInfo1);


        List<DeptNameInfo> deptNameInfos = DeptNameInfo(arrayList);
        System.out.println(deptNameInfos);
        for (DeptNameInfo deptNameInfo : deptNameInfos) {
            String[] split = deptNameInfo.getDeptName().split("/");
            for (int i = 0; i <= 3; i++) {
                String s = split[i];

            }
        }

    }

    public static List<DeptNameInfo> DeptNameInfo(List<DeptNameInfo> list) {
        String str = "";
        for (int i = 0; i < list.size(); i++) {
            DeptNameInfo el = list.get(i);
            if (list.size() - 1 == i) {
                str = str + el.getDeptName();
                list.clear();
                DeptNameInfo deptNameInfo = new DeptNameInfo();
                deptNameInfo.setId(el.getId());
                deptNameInfo.setDeptName(str);
                list.add(deptNameInfo);
            } else {
                str = str + el.getDeptName() + "/";
            }
        }
        return list;
    }
}
