package com.bjxczy.onepark.common.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;

public class DateTools {
    public final static String FORMAT_01 = "yyyy-MM-dd HH:mm:ss";
    public final static String yyyy_MM_dd_HHmm = "yyyy-MM-dd HH:mm";

    public final static String FORMAT_02 = "yyyy-MM-dd";

    public final static String FORMAT_03 = "yyyyMMdd";

    public final static String FORMAT_04 = "yyyy-MM-dd'T'HH:mm:ss";

    public final static String FORMAT_05 = "yyyyMMddHHmmss";
    public final static String FORMAT_06 = "yyyyMMddHHmm";


    /**
     * 日期格式化
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String format(Date date, String pattern) {
        String returnValue = "";
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            returnValue = df.format(date);
        }
        return (returnValue);
    }

    /**
     * 字符串时间格式化
     *
     * @param strDate
     * @param patternOld
     * @param patternNew
     * @return
     */
    public static String format(String strDate, String patternOld, String patternNew) {
        String returnValue = "";
        SimpleDateFormat dfOld = new SimpleDateFormat(patternOld);
        Date date;
        try {
            date = dfOld.parse(strDate);
            SimpleDateFormat dfNew = new SimpleDateFormat(patternNew);
            returnValue = dfNew.format(date);
        } catch (ParseException e) {
            e.getMessage();
            return null;
        }
        return (returnValue);
    }

    /**
     * 将字符串类型的日期转化成UTC时间格式 精确到grade等级，如grade=1精确到毫秒，grade=1000精确到秒
     *
     * @param strDate
     * @param pattern
     * @param grade
     * @return String [返回类型说明]
     * @throws throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
//    public static String parseTime(String strDate, String pattern, long grade) {
//        SimpleDateFormat df = new SimpleDateFormat(pattern);
//        Date date;
//        try {
//            date = df.parse(strDate);
//            long time = date.getTime() / grade;
//            return time + "";
//        } catch (ParseException e) {
//            log.error(e.getMessage(), e);
//            return null;
//        }
//    }

    /**
     * 日期格式化
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String format(String date, String pattern) {
        String returnValue = "";
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            returnValue = df.format(Long.parseLong(date));
        }
        return (returnValue);
    }

    /**
     * 将字符串类型的日期转化成Date类型
     *
     * @param strDate
     * @param pattern
     * @return
     */
    public static Date parse(String strDate, String pattern) {
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = df.parse(strDate);
            return date;
        } catch (ParseException e) {
            e.getMessage();
            return null;
        }
    }
    public static Date parseDate(Date date, String pattern) {
        String format = format(date, pattern);
        try {
            date = parse(format,FORMAT_01);
            return date;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }




    /**
     * 将UTC字符串类型的日期转化成CST的Date类型
     *
     * @param UTCStr
     * @param pattern
     * @return
     */
    public static Date UTCToCST(String UTCStr, String pattern) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            date = sdf.parse(UTCStr);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) + 8);
            //calendar.getTime() 返回的是Date类型，也可以使用calendar.getTimeInMillis()获取时间戳
            return calendar.getTime();
        } catch (ParseException e) {
            e.getMessage();
            return null;
        }
    }


    /**
     * 获取时间戳
     */
    public static String getTimeString() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        return df.format(calendar.getTime());
    }

    /**
     * 获取当前时间Date
     * @return
     */
    public static Date getTimeDate() throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String format = df.format(date);
        return df.parse(format);
    }
    /**
     * 获取当前时间Date
     * @return
     */
    public static String getTimeToString()  {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String format = df.format(date);
        return format;
    }
    /**
     * 获取日期年份
     *
     * @param date 日期
     * @return
     */
    public static String getYear(Date date) {
        return format(date, "yyyy-MM-dd HH:mm:ss").substring(0, 4);
    }

    /**
     * 按默认格式的字符串距离今天的天数
     *
     * @param date 日期字符串
     * @return
     */
    public static int countDays(String date) {
        long t = Calendar.getInstance().getTime().getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(parse(date, "yyyy-MM-dd HH:mm:ss"));
        long t1 = c.getTime().getTime();
        return (int) (t / 1000 - t1 / 1000) / 3600 / 24;
    }

    /**
     * 按用户格式字符串距离今天的天数
     *
     * @param date   日期字符串
     * @param format 日期格式
     * @return
     */
    public static int countDays(String date, String format) {
        long t = Calendar.getInstance().getTime().getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(parse(date, format));
        long t1 = c.getTime().getTime();
        return (int) (t / 1000 - t1 / 1000) / 3600 / 24;
    }

    /**
     * 获取某天凌晨时间 <一句话功能简述> <功能详细描述>
     *
     * @param date
     * @return Date [返回类型说明]
     * @throws throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static Date getMorning(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        return calendar.getTime();
    }

    /**
     * 获取当前时间的时间戳，精确到毫秒 <功能详细描述>
     *
     * @return String [返回类型说明]
     * @throws throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static String getCurrentTimeMillis() {
        return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
    }

    /**
     * 获取多少天之后的日期 <功能详细描述>
     *
     * @param startDate
     * @param days
     * @return String [返回类型说明]
     * @throws throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static String getAfterDate(String startDate, int days) {
        Date date;
        Calendar cal = Calendar.getInstance();
        try {
            date = (new SimpleDateFormat("yyyy-MM-dd")).parse(startDate);

            cal.setTime(date);
            cal.add(Calendar.DATE, days);
        } catch (ParseException e) {
            e.getMessage();
        }

        return (new SimpleDateFormat("yyyy-MM-dd")).format(cal.getTime());
    }

    /**
     * 日期比较 <功能详细描述>
     *
     * @param startDate
     * @param endDate
     * @return String [返回类型说明]
     * @throws throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
//    public static boolean compareDate(String startDate, String endDate, String format) {
//        Date newDate = strDateToDate(startDate, format);
//        Date newDate2 = strDateToDate(endDate, format);
//        return compareDate(newDate, newDate2);
//    }

    /**
     * 将字符转换为日期类型 <功能详细描述>
     *
     * @param strDate
     * @param sourceFormat
     * @return Date [返回类型说明]
     * @throws throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
//    public static Date strDateToDate(String strDate, String sourceFormat) {
//        SimpleDateFormat dateFormat = new SimpleDateFormat(sourceFormat);
//        Date date;
//        try {
//            date = dateFormat.parse(strDate);
//            return date;
//        } catch (ParseException e) {
//            log.error(e.getMessage(), e);
//            return null;
//        }
//
//    }

    /**
     * 通过时间秒毫秒数判断两个时间的间隔
     *
     * @param a
     * @param b
     * @return
     * @throws ParseException
     */
    public static int differentDaysByMillisecond(Long a, Long b) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String d1 = format.format(a);

        String d2 = format.format(b);
        Date date1 = format.parse(d1);

        Date date2 = format.parse(d2);
        int days = (int) ((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
        return days;
    }

    /**
     * 判断时间大小 <功能详细描述>
     *
     * @param date1
     * @param date2
     * @return boolean [返回类型说明]
     * @throws throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
//    public static boolean compareDate(Date date1, Date date2) {
//        if (date1.compareTo(date2) > 0) {
//            return true;
//        } else {
//            return false;
//        }
//    }

    /**
     * 获取当前时间 <功能详细描述>
     *
     * @return String [返回类型说明]
     * @throws throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static String now() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date());
    }

    /**
     * 获取几分钟后 <功能详细描述>
     *
     * @param date
     * @param minute
     * @return String [返回类型说明]
     * @throws throws [违例类型] [违例说明]
     * @see [类、类#方法、类#成员]
     */
    public static String afterMinute(String date, int minute) {
        Calendar cale = Calendar.getInstance();
        cale.add(Calendar.MINUTE, minute);
        Date tasktime = cale.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(tasktime);
    }

    /**
     * 根据秒获取当前时间
     */
    public static String getTime(Integer s) {

        String hour = Integer.toString(s / 3600);
        String second = Integer.toString((s % 3600) / 60);
        if (second.length() < 2) {
            second = "0" + second;
        }
        String date = hour + ":" + second;
        return date;
    }

    /**
     * lzx 获取当前日期是星期几<br>
     *
     * @param
     * @return 当前日期是星期几
     */
    public static String getWeekOfDate() {
        Date date = new Date();
        String[] weekDays = {"0", "1", "2", "3", "4", "5", "6"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    /**
     * 获取传入日期是星期几<br>
     *
     * @param
     * @return 当前日期是星期几
     */
    public static String getWeekOfDate(String strDay) {
        Date dt = parse(strDay, FORMAT_02);
        String[] weekDays = {"7", "1", "2", "3", "4", "5", "6"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    /**
     * lzx
     *
     * @param we 必须是 二进制的 字符串 0100001
     * @return
     */
    public static Boolean alaerAy(String we) {
        Boolean bool = false;
        for (int i = 0; i < we.length(); i++) {
            if (Integer.parseInt(DateTools.getWeekOfDate()) == i) {
                if (we.charAt(i) == '1') {
                    bool = true;
                }
                break;
            }
        }
        return bool;
    }

    /**
     * lzx 根据时间戳 判断当前是否产生告警信息
     *
     * @param atartTime 开始时间
     * @param andTime   结束时间
     * @param eTime     nat推送的时间戳
     * @return
     */
    public static Boolean teimAlar(Long atartTime, Long andTime, Long eTime) {
        Boolean bool = false;
        if (atartTime < andTime) {
            if (atartTime < eTime && eTime < andTime) {
                bool = true;
            } else if (atartTime.equals(eTime) || andTime.equals(eTime)) {
                bool = true;
            }

        } else if (atartTime > andTime) {
            if (andTime < eTime || eTime < atartTime) {
                bool = false;
            } else {
                bool = true;
            }
        } else if (atartTime.equals(andTime)) {
            if (atartTime.equals(eTime)) {
                bool = true;
            } else {
                bool = false;
            }
        }
        return bool;
    }


    /**
     * 获取当前周的周一和周日的日期
     *
     * @return
     */
    public static String getMonToSunOfWeek() {

        SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd");//设置日期格式
        Calendar cld = Calendar.getInstance(Locale.CHINA);
        cld.setFirstDayOfWeek(Calendar.MONDAY);//以周一为首日
        cld.setTimeInMillis(System.currentTimeMillis());//当前时间
        cld.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);//周一
        String mon = df.format(cld.getTime());
        cld.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);//周日
        String sun = df.format(cld.getTime());
        return mon + "-" + sun;
    }

    /**
     * 获取当前日期
     *
     * @param
     * @return
     */
    public static String getCurrentDate() {
        Date date = new Date();
        return format(date, FORMAT_03);
    }

    public static String getCurrentDate(String format) {
        Date date = new Date();
        return format(date, format);
    }

    public static String getNextNDaysDateTime(int N) {
        Date date = getNdaysAfter(new Date(), N);
        return format(date, FORMAT_04);
    }

    public static Date getNdaysAfter(Date date, int N) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, +N);//+1今天的时间加一天
        date = calendar.getTime();
        return date;
    }

    public static List<String> getDayByMonth(String queryMonth) {
        List list = new ArrayList();
        try {

            Calendar calendar = Calendar.getInstance(Locale.CHINA);
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM");
            Date date = sdf2.parse(queryMonth);
            calendar.setTime(date);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.getActualMaximum(Calendar.DATE);
            for (int i = 1; i <= day; i++) {
                String aDate = null;
                if (month < 10 && i < 10) {
                    aDate = String.valueOf(year) + "-0" + month + "-0" + i;
                }
                if (month < 10 && i >= 10) {
                    aDate = String.valueOf(year) + "-0" + month + "-" + i;
                }
                if (month >= 10 && i < 10) {
                    aDate = String.valueOf(year) + "-" + month + "-0" + i;
                }
                if (month >= 10 && i >= 10) {
                    aDate = String.valueOf(year) + "-" + month + "-" + i;
                }

                list.add(aDate);
            }
        } catch (ParseException e) {

        }
        return list;
    }

    /**
     * 将只有小时分钟的字符串转化为昨天对应时间戳
     * 秒—>*天*时*分*秒
     *
     * @param time
     * @return
     */
    public static Long getTimeStamp(String time) {
        Long timeStamp = 0L;
        try {
            String day = getCurrentDate(FORMAT_02);
            String yesterDay = getAfterDate(day, -1);
            String formatDay = yesterDay + " " + time + ":00";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = format.parse(formatDay);
            timeStamp = date.getTime();
        } catch (Exception e) {

        }
        return timeStamp;
    }

    /**
     * 将时间戳转化为对应只有小时分钟的字符串
     * 秒—>*天*时*分*秒
     *
     * @param timeStamp
     * @return
     */
    public static String stampToString(Long timeStamp) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Date date = new Date(timeStamp);
        String day = df.format(date);
        return day;
    }

    public static String longToString(String pattern, Long time) {
        Date date = new Date(time);
        SimpleDateFormat sd = new SimpleDateFormat(pattern);
        return sd.format(date);
    }
    /**
     * 获取当天0点 ----00:00:00
     * @return
     */
    public static Long getTodayStartTime(){
        LocalDateTime now = LocalDateTime.now();
        return LocalDateTime.of(now.toLocalDate(), LocalTime.MIN).toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

    /**
     * 获取当天24点----23:59:59
     * @return
     */
    public static Long getTodayEndTime(){
        LocalDateTime now = LocalDateTime.now();
        return  LocalDateTime.of(now.toLocalDate(), LocalTime.MAX).toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }
}
