package com.bjxczy.onepark.common.utils.poi;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Excel导入导出工具类
 *
 * @author dcr_yuefeng
 * @since 2022/7/1 11:12
 */
public class ExcelUtils {
    /**
     * 解析实体类
     *
     * @param clazz 实体类
     */
    private static List<List<String>> parseHead(Class clazz) {
        Field[] fields = clazz.getDeclaredFields();
        List<List<String>> heads = new ArrayList<>();

        for (Field field : fields) {
            List<String> head = new ArrayList<>();
            Excel apiAnnotation = field.getAnnotation(Excel.class);
            if (apiAnnotation != null) {
                head.add(apiAnnotation.name());
                heads.add(head);
            }
        }
        return heads;
    }


    /**
     * 设置web响应输出的文件名称
     *
     * @param response web响应
     * @param fileName 导出文件名称
     */
    private static void setResponseHeader(HttpServletResponse response, String fileName) {
        response.reset();
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        try {
            response.setHeader("content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName + ".xls", "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.getStackTrace();
        }
        response.setCharacterEncoding("UTF-8");
    }

    /**
     * 导出
     *
     * @param response  响应
     * @param fileName  响应文件名
     * @param sheetName sheet耶名称
     * @param data      导出的数据
     * @param tClass    导出的实体
     * @throws IOException /
     */
    public static void export(HttpServletResponse response, String fileName, String sheetName, List<?> data, Class<?> tClass) throws IOException {
        setResponseHeader(response, fileName);
        ExportParams exportParams = new ExportParams();
        // 设置sheet得名称
        exportParams.setSheetName(sheetName);
        exportParams.setDataHandler(new ExeclDataHandler());
        Map<String, Object> exportMap = new HashMap<>();
        // title的参数为ExportParams类型，目前仅仅在ExportParams中设置了sheetName
        exportMap.put("title", exportParams);
        // 模版导出对应得实体类型
        exportMap.put("entity", tClass);
        // sheet中要填充得数据
        exportMap.put("data", data);

        List<Map<String, Object>> sheetsList = new ArrayList<>();
        sheetsList.add(exportMap);
        Workbook workbook = ExcelExportUtil.exportExcel(sheetsList, ExcelType.HSSF);
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();

    }


}