package com.bjxczy.onepark.common.utils.uid;

import cn.hutool.core.lang.UUID;
import com.bjxczy.core.web.base.ValueObject;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

public class UuId extends ValueObject<UuId> {


    public static String getId() {
        return new UuId().getValue();
    }

    private static final long serialVersionUID = 3669836782315745781L;

    @Getter
    @Setter
    private String value;

    public UuId() {
        this.value = UUID.fastUUID().toString().replace("-", "");
    }

    public UuId(@NotBlank String uuid) {
        this.value = uuid;
    }

    @Override
    protected boolean equalsTo(UuId other) {
        return getValue().equals(other.getValue());
    }

}
