package com.bjxczy.onepark.common.utils.poi;

import cn.afterturn.easypoi.handler.impl.ExcelDataHandlerDefaultImpl;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Hyperlink;

/**
 * @author dcr_yuefeng
 * @since 2022/7/4 15:13
 */
public class ExeclDataHandler extends ExcelDataHandlerDefaultImpl<FormData> {
    @Override
    public Hyperlink getHyperlink(CreationHelper creationHelper, FormData obj, String name, Object value) {
        Hyperlink hyperlink = creationHelper.createHyperlink(HyperlinkType.URL);
        hyperlink.setLabel(name);
        hyperlink.setAddress((String) value);
        if(((String) value).startsWith("http")){
            return hyperlink;
        }
        return null;
    }
}
