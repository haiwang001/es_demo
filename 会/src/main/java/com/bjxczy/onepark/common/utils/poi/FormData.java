package com.bjxczy.onepark.common.utils.poi;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @author dcr_yuefeng
 * @since 2022/7/4 15:13
 */
@Data
public class FormData {
    private Long id;
    private Integer formItemId;
    private Long submitId;
    @TableField(exist = false)
    @Excel(name = "表单项名称")
    private String formItemName;
    @Excel(name = "表单内容",isHyperlink = true,width = 100)
    private String content;
    private String fileName;
    private String createTime;
    private String originItemName;
    private Integer originItemType;
    private Integer originItemId;
    @TableField(exist = false)
    private Integer formItemExists;
}
