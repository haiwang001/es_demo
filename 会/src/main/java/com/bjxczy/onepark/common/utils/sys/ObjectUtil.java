package com.bjxczy.onepark.common.utils.sys;


import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * 工具类
 */
public class ObjectUtil {

    /**
     * 对象判空
     *
     * @param target 判空对象
     * @return 结果。空则为true；非空为false
     */
    public static boolean isNullOrEmpty(Object target) {
        if (target == null) {
            return true;
        }

        if (target instanceof Integer && target == null) {
            return true;
        }

        if (target instanceof Long && target == null) {
            return true;
        }

        if (target instanceof String && "".equals(((String) target).trim())) {
            return true;
        }

        if (target instanceof List && ((List) target).isEmpty()) {
            return true;
        }

        if (target instanceof Set && ((Set) target).isEmpty()) {
            return true;
        }

        if (target instanceof Map && ((Map) target).isEmpty()) {
            return true;
        }

        return false;
    }


    public static boolean isNullOrEmpty(Object... target) {
        for (Object obj : target) {
            if (isNullOrEmpty(obj)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 转换为Long
     *
     * @param obj
     * @return
     */
    public static Long toLong(Object obj) {
        if (null == obj || !isNumber(obj.toString())) {
            throw new NumberFormatException("参数为null或不是数字类型！");
        }

        if (obj instanceof String) {
            return Long.valueOf((String) obj);
        }
        if (obj instanceof Integer) {
            return Long.valueOf(obj.toString());
        }
        if (obj instanceof Long) {
            return (Long) obj;
        }
        return null;
    }

    /**
     * 转换为Integer
     *
     * @param obj
     * @return
     */
    public static Integer toInt(Object obj) {
        if (null == obj || !isNumber(obj.toString())) {
            throw new NumberFormatException("参数为null或不是数字类型！");
        }

        if (obj instanceof String) {
            return Integer.valueOf((String) obj);
        }
        if (obj instanceof Long) {
            return Integer.valueOf(obj.toString());
        }
        if (obj instanceof Integer) {
            return (Integer) obj;
        }
        return null;
    }

    public static boolean isNumber(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

}
