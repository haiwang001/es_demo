package com.bjxczy.onepark.goodsmodule.pojo.dto;


import com.bjxczy.onepark.publicmodule.pojo.dto.BasePageDTO;
import lombok.Data;

import java.util.List;

/*
 *@ClassName MeetingGoodsDto
 *@Author 温良伟
 *@Date 2023/2/17 9:06
 *@Version 1.0 保留
 */
@Data
public class MeetingGoodsDto extends BasePageDTO {

    private List<Integer> ids;
    private String typeId;

    public void setTypeId(String typeId) {
        this.typeId = typeId.replaceAll("\\s*", "");
    }
}
