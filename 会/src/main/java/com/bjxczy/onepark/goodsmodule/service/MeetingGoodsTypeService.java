package com.bjxczy.onepark.goodsmodule.service;

import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.goodsmodule.entity.MeetingGoodsType;
import com.bjxczy.onepark.goodsmodule.pojo.dto.MeetingGoodsTypeDto;

public interface MeetingGoodsTypeService {
    int addGoodsType(MeetingGoodsType goodsType,UserInformation user) throws Exception;

    int delGoodsType(Integer typeId) throws Exception;

    Object GoodsTypeList(String typeName);

    int putGoodsType(MeetingGoodsTypeDto dto, UserInformation user);
}
