package com.bjxczy.onepark.goodsmodule.pojo.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/*
 *@ClassName MeetingGoodsTemplateExcelVo
 *@Author 温良伟
 *@Date 2023/2/17 9:55
 *@Version 1.0
 * 下载的模板
 */
@Data
public class MeetingGoodsExcelVo {
    private Integer goodsId;
    @Excel(name = "物品名称", width = 18, replace = {"_null"})
    private String goodsName;
    @Excel(name = "物品分类", width = 18, replace = {"_null"})
    private String typeName;
    @Excel(name = "物品单价(元)", width = 18, replace = {"_null"})
    private Double uniValence;
    @Excel(name = "物品归属(会议室物品/参会人员物品)", width = 30, replace = {"_null"})
    private String belong;
    @Excel(name = "备注说明", width = 18, replace = {"_null"})
    private String remark;
}
