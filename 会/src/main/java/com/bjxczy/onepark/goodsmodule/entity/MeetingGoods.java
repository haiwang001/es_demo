package com.bjxczy.onepark.goodsmodule.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/*
 *@ClassName MeetingGoods
 *@Author 温良伟
 *@Date 2023/2/16 17:35
 *@Version 1.0
 */
@TableName("tbl_meeting_goods")
@Data
public class MeetingGoods {
    @TableId(type = IdType.AUTO)
    private Integer goodsId;
    private Integer typeId;
    private String goodsName;
    private String belong; // 物品归属  会议室物品  参会人物品
    private Double uniValence;
    private String remark;
    private String typeName;
    private String imgUrl;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    private String operator;
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;
}
