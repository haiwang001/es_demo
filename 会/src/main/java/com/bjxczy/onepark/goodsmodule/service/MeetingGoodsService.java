package com.bjxczy.onepark.goodsmodule.service;

import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.goodsmodule.entity.MeetingGoods;
import com.bjxczy.onepark.goodsmodule.pojo.dto.MeetingGoodsDto;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public interface MeetingGoodsService {
    int addGoodsType(MeetingGoods goods,UserInformation user) throws Exception ;

    Object MeetingGoodsPage(MeetingGoodsDto dto);

    int deleteMeetingGoods(MeetingGoodsDto dto);

    void downloadData(HttpServletResponse response,MeetingGoodsDto dto)throws Exception;

    HashMap<String, Object> uploadingExcel(MultipartFile file, UserInformation user)throws Exception ;

    int updateGoodsType(MeetingGoods goods,UserInformation user) ;
}
