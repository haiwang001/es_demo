package com.bjxczy.onepark.goodsmodule.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.goodsmodule.entity.MeetingGoodsType;
import com.bjxczy.onepark.goodsmodule.pojo.dto.MeetingGoodsTypeDto;
import com.bjxczy.onepark.goodsmodule.service.MeetingGoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/*
 *@ClassName MeetingGoodsTypeController
 *@Date 2023/2/16 16:26
 *@Version 1.0
 */
@ApiResourceController
@RequestMapping("GoodsType")
public class MeetingGoodsTypeController {

    @Autowired
    private MeetingGoodsTypeService service;

    @PostMapping(value = "add", name = "新增物品类型")
    public R<?> addConferenceRoom(@RequestBody MeetingGoodsType GoodsType,@LoginUser(isFull = true) UserInformation user) throws Exception {
        return R.success(service.addGoodsType(GoodsType,user));
    }

    @DeleteMapping(value = "delete/{typeId}", name = "删除物品类型")
    public R<?> delConferenceRoom(@PathVariable("typeId") Integer typeId) throws Exception {
        return R.success(service.delGoodsType(typeId));
    }

    @GetMapping(value = "list", name = "物品类型列表")
    public R<?> ConferenceRoomList(String typeName) throws Exception {
        return R.success(service.GoodsTypeList(typeName));
    }

    @PutMapping(value = "put", name = "物品类型名称修改")
    public R<?> putConferenceRoom(@RequestBody MeetingGoodsTypeDto dto,@LoginUser(isFull = true) UserInformation user) {
        return R.success(service.putGoodsType(dto,user));
    }

}
