package com.bjxczy.onepark.goodsmodule.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/*
 *@ClassName MeetingGoodsType
 *@Author 温良伟
 *@Date 2023/2/16 16:18
 *@Version 1.0
 */
@Data
@TableName("tbl_meeting_goods_type")
public class MeetingGoodsType {
    @TableId(type = IdType.AUTO)
    private Integer typeId;
    private String typeName;

    public void setTypeName(String typeName) {
        this.typeName = typeName.replaceAll("\\s*","");
    }

    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;
    private String operator;
}
