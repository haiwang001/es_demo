package com.bjxczy.onepark.goodsmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/5/13 9:52
 */
@Data
public class UploadingExcelVo {
    private String name;
    private String msg;
    private String cause;
    private Double uniValence; //价格
    private String typeName;

    public UploadingExcelVo(String name, String msg, String cause,Double uniValence,String typeName) {
        this.name = name;
        this.msg = msg;
        this.cause = cause;
        this.uniValence = uniValence;
        this.typeName = typeName;
    }
}
