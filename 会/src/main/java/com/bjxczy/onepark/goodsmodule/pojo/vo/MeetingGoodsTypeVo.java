package com.bjxczy.onepark.goodsmodule.pojo.vo;


import lombok.Data;

/*
 *@ClassName MeetingGoodsTypeVo
 *@Author 温良伟
 *@Date 2023/2/16 17:04
 *@Version 1.0
 */
@Data
public class MeetingGoodsTypeVo {
    private Integer typeId;
    private String typeName;
}
