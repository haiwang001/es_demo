package com.bjxczy.onepark.goodsmodule.service.impl;


import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.utils.poi.ExcelUtils;
import com.bjxczy.onepark.common.utils.sys.PageUtils;
import com.bjxczy.onepark.goodsmodule.entity.MeetingGoods;
import com.bjxczy.onepark.goodsmodule.entity.MeetingGoodsType;
import com.bjxczy.onepark.goodsmodule.mapper.MeetingGoodsMapper;
import com.bjxczy.onepark.goodsmodule.mapper.MeetingGoodsTypeMapper;
import com.bjxczy.onepark.goodsmodule.pojo.dto.MeetingGoodsDto;
import com.bjxczy.onepark.goodsmodule.pojo.vo.MeetingGoodsExcelVo;
import com.bjxczy.onepark.goodsmodule.pojo.vo.UploadingExcelVo;
import com.bjxczy.onepark.goodsmodule.service.MeetingGoodsService;
import com.google.common.collect.ImmutableMap;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/*
 *@ClassName MeetingGoodsServiceImpl
 *@Author 温良伟
 *@Date 2023/2/16 17:51
 *@Version 1.0
 */
@Service
public class MeetingGoodsServiceImpl implements MeetingGoodsService {


    @Resource
    private MeetingGoodsMapper mapper;

    @Resource
    private MeetingGoodsTypeMapper Typemapper;

    @Override
    public int updateGoodsType(MeetingGoods goods,UserInformation user) {
        MeetingGoodsType meetingGoodsType = Typemapper.selectById(goods.getTypeId());
        goods.setTypeName(meetingGoodsType.getTypeName());
        goods.setOperator(user.getStaffName());
        return mapper.updateById(goods);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HashMap<String, Object> uploadingExcel(MultipartFile file, UserInformation user) throws Exception {
        if (!Objects.requireNonNull(file.getOriginalFilename()).contains(SysConst.GOODS_FILE_NAME))
            throw new ResultException("请使用下载的模板文件进行导入数据");
        HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream());//创建表
        HSSFSheet sheet = workbook.getSheetAt(0); //获取第一张表
        //获取所有类型
        List<MeetingGoodsType> types = Typemapper.selectByMap(new HashMap<>());
        // 获取所有信息
        List<MeetingGoods> list = setBeans(types, sheet);
        HashMap<String, Object> map = new HashMap<>();
        ArrayList<UploadingExcelVo> list1 = new ArrayList<>();
        list.forEach(el -> {
            // 先判断是否重复
            if (isRepeat(el)) {
                if (!isTypeRepeat(types, el)) {//不包含该类型
                    int typeId = (int) (Math.random() * (int) (Math.random() * 1000000));
                    // 增加类型
                    MeetingGoodsType type = new MeetingGoodsType();
                    type.setDelFlag(0);
                    type.setOperator(user.getStaffName());
                    type.setTypeName(el.getTypeName());
                    type.setTypeId(typeId);
                    Typemapper.insertType(type);
                    el.setTypeId(typeId);
                }
                if (el.getTypeName() == null || "".equals(el.getTypeName() + "")) {
                    list1.add(new UploadingExcelVo(el.getGoodsName(), "失败", "物品分类未填写", el.getUniValence(), el.getTypeName()));  // 数据完全重复 返回去
                } else if (el.getUniValence() == null || "".equals(el.getUniValence() + "")) {
                    list1.add(new UploadingExcelVo(el.getGoodsName(), "失败", "单价未填写", el.getUniValence(), el.getTypeName()));  // 数据完全重复 返回去
                } else if (el.getBelong() == null || "".equals(el.getBelong() + "")) {
                    list1.add(new UploadingExcelVo(el.getGoodsName(), "失败", "会议分类未填写", el.getUniValence(), el.getTypeName()));  // 数据完全重复 返回去
                } else {
                    if (el.getBelong().equals("会议室物品") || el.getBelong().equals("参会人员物品")){
                        int insert = mapper.insert(el);
                        if (insert == 1) {
                            list1.add(new UploadingExcelVo(el.getGoodsName(), "成功", " - ", el.getUniValence(), el.getTypeName()));  // 数据完全重复 返回去
                        }
                    }else {
                        list1.add(new UploadingExcelVo(el.getGoodsName(), "失败", "会议分类填写错误", el.getUniValence(), el.getTypeName()));  // 数据完全重复 返回去
                    }
                }
            } else {
                list1.add(new UploadingExcelVo(el.getGoodsName(), "失败", "数据重复", el.getUniValence(), el.getTypeName()));  // 数据完全重复 返回去
            }
        });
        map.put("data", list1);
        return map;
    }

    /**
     * 类型是否一致
     * 如果数据的类型包含在原来的列中
     */
    private boolean isTypeRepeat(List<MeetingGoodsType> types, MeetingGoods el) {
        for (MeetingGoodsType typeEl : types) {
            if (typeEl.getTypeName().equals(el.getTypeName())) {//
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否一样
     *
     * @param el
     */
    private Boolean isRepeat(MeetingGoods el) {
        return mapper.selectByMap(ImmutableMap.of(
                "type_name", el.getTypeName(),
                "goods_name", el.getGoodsName(),
                "del_flag", 0)).size() == 0;
    }

    public List<MeetingGoods> setBeans(List<MeetingGoodsType> types, HSSFSheet sheet) {
        List<MeetingGoods> list = new ArrayList<>(sheet.getLastRowNum());
        for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
            HSSFRow row = sheet.getRow(rowIndex);
            MeetingGoods goods = new MeetingGoods();
            try {
                goods.setGoodsName(row.getCell(0).getStringCellValue());
            } catch (Exception e) {
                goods.setGoodsName(row.getCell(0).getNumericCellValue() + "");
            }
            try {
                goods.setTypeName(row.getCell(1).getStringCellValue());
            } catch (Exception e) {
                goods.setTypeName(row.getCell(1).getNumericCellValue() + "");
            }
            try {
                goods.setUniValence(row.getCell(2).getNumericCellValue());
            } catch (IllegalStateException e) {
                goods.setUniValence(Double.parseDouble(row.getCell(2).getStringCellValue()));
            }
            try {
                if (row.getCell(3) != null) {
                    goods.setBelong(row.getCell(3).getStringCellValue());
                }
            } catch (Exception e) {
                goods.setBelong(row.getCell(3).getNumericCellValue() + "");
            }
            try {
                if (row.getCell(4) != null) {
                    goods.setRemark(row.getCell(4).getStringCellValue());
                }
            } catch (Exception e) {
                goods.setRemark(row.getCell(4).getNumericCellValue() + "");
            }
            goods.setCreateTime(new Date());
            goods.setDelFlag(0);
            goods.setOperator("import");
            list.add(goods);
        }
        for (MeetingGoodsType type : types) {
            for (MeetingGoods goods : list) {
                if (type.getTypeName().equals(goods.getTypeName())) {
                    goods.setTypeId(type.getTypeId());
                }
            }
        }
        return list;
    }

    @Override
    public void downloadData(HttpServletResponse response, MeetingGoodsDto dto) throws Exception {
        List<MeetingGoodsExcelVo> list = mapper.selectExcelAllByDto();
        List<MeetingGoodsExcelVo> relList = new ArrayList<>();
        if (dto.getIds().size() > 0) {
            List<MeetingGoodsExcelVo> finalRelList = relList;
            dto.getIds().forEach(id -> list.forEach(el -> {
                if (el.getGoodsId().equals(id)) {
                    finalRelList.add(el);
                }
            }));
        } else {
            relList = list;
        }
        ExcelUtils.export(response,
                SysConst.GOODS_FILE_NAME,
                SysConst.GOODS_FILE_NAME,
                relList, MeetingGoodsExcelVo.class);
    }

    @Override
    public int deleteMeetingGoods(MeetingGoodsDto dto) {
        List<Integer> ids = dto.getIds();
        AtomicInteger i = new AtomicInteger();
        ids.forEach(id -> i.set(mapper.deleteById(id)));
        return i.get();
    }

    @Override
    public Object MeetingGoodsPage(MeetingGoodsDto dto) {
        return PageUtils.Page(dto, mapper.selectAllByDto(dto));
    }

    @Override
    public int addGoodsType(MeetingGoods goods,UserInformation user) throws Exception {
        List<MeetingGoods> meetingGoods = mapper.selectByMap(ImmutableMap.of("type_id", goods.getTypeId(),"goods_name", goods.getGoodsName()));
        if (meetingGoods.size() <= 0) {
            MeetingGoodsType meetingGoodsType = Typemapper.selectById(goods.getTypeId());
            goods.setTypeName(meetingGoodsType.getTypeName());
            goods.setCreateTime(new Date());
            goods.setOperator(user.getStaffName());
            return mapper.insert(goods);
        }
        throw new ResultException("物品已经在此分类已经存在");
    }


}
