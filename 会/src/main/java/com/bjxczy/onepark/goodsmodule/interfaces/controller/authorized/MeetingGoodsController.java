package com.bjxczy.onepark.goodsmodule.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.common.utils.poi.ExcelUtils;
import com.bjxczy.onepark.goodsmodule.entity.MeetingGoods;
import com.bjxczy.onepark.goodsmodule.pojo.dto.MeetingGoodsDto;
import com.bjxczy.onepark.goodsmodule.pojo.vo.MeetingGoodsExcelVo;
import com.bjxczy.onepark.goodsmodule.service.MeetingGoodsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/*
 *@ClassName MeetingGoodsController
 *@Date 2023/2/16 17:50
 *@Version 1.0
 */
@ApiResourceController
@RequestMapping("Goods")
public class MeetingGoodsController {

    @Resource
    private MeetingGoodsService service;

    @PostMapping(value = "add", name = "新增物品")
    public R<?> addMeetingGoods(@RequestBody MeetingGoods Goods, @LoginUser(isFull = true) UserInformation user) throws Exception {
        return R.success(service.addGoodsType(Goods,user));
    }

    @PutMapping(value = "update/{goodsId}", name = "修改物品")
    public R<?> updateMeetingGoods(@PathVariable("goodsId") Integer goodsId,
                                   @RequestBody MeetingGoods Goods, @LoginUser(isFull = true) UserInformation user) throws Exception {
        Goods.setGoodsId(goodsId);
        return R.success(service.updateGoodsType(Goods,user));
    }

    @GetMapping(value = "list", name = "物品列表")
    public R<?> MeetingGoodsPage(MeetingGoodsDto dto) throws Exception {
        return R.success(service.MeetingGoodsPage(dto));
    }


    @DeleteMapping(value = "del", name = "物品删除")
    public R<?> delMeetingGoods(@RequestBody MeetingGoodsDto dto) throws Exception {
        return R.success(service.deleteMeetingGoods(dto));
    }


    @GetMapping(value = "downloadTemplate", name = "下载模板")
    public void downloadTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ExcelUtils.export(response,
                SysConst.GOODS_FILE_NAME,
                SysConst.GOODS_FILE_NAME,
                new ArrayList<>(0), MeetingGoodsExcelVo.class);
    }


    @PostMapping(value = "downloadData", name = "下载物品信息")
    public void downloadData(HttpServletRequest request,
                             HttpServletResponse response,
                             @RequestBody MeetingGoodsDto dto) throws Exception {
        service.downloadData(response, dto);
    }


    /**
     * 得到文件 转换成对象
     * 是否重复 数据
     * 物品类型不存在的数据
     * 入库 数据
     */
    @PostMapping(value = "/import", name = "物品类型导入")
    public R<?> uploadingExcel(@RequestParam("file") MultipartFile file,
                               @LoginUser(isFull = true) UserInformation user) throws Exception {
        return R.success(service.uploadingExcel(file,user));
    }

}
