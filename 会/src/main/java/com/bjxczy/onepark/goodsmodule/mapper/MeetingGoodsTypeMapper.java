package com.bjxczy.onepark.goodsmodule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.goodsmodule.entity.MeetingGoodsType;
import com.bjxczy.onepark.goodsmodule.pojo.dto.MeetingGoodsTypeDto;
import com.bjxczy.onepark.goodsmodule.pojo.vo.MeetingGoodsTypeVo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface MeetingGoodsTypeMapper extends BaseMapper<MeetingGoodsType> {

    @Select("select  count(*) from  tbl_meeting_goods_type where del_flag=0 and type_name = #{typeName}")
    int selectByTypeName(@Param("typeName") String typeName);

    @Select("select  * from  tbl_meeting_goods_type where del_flag = 0   " +
            "and  if(#{typeName} != '' or #{typeName} is not null , type_name like concat('%', #{typeName}, '%') , true)")
    List<MeetingGoodsTypeVo> selectListByTypeName(@Param("typeName") String typeName);

    @Update("update tbl_meeting_goods_type set type_name = #{dto.typeName} where  type_id = #{dto.typeId}")
    int updateByDto(@Param("dto") MeetingGoodsTypeDto dto);

    @Insert("insert into tbl_meeting_goods_type values (#{type.typeId},#{type.typeName},0,#{type.operator})")
    int insertType(@Param("type")MeetingGoodsType type);
}
