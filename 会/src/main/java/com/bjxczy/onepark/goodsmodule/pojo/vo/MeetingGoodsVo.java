package com.bjxczy.onepark.goodsmodule.pojo.vo;


import lombok.Data;

/*
 *@ClassName MeetingGoodsVo
 *@Author 温良伟
 *@Date 2023/2/17 9:08
 *@Version 1.0  MeetingCcard
 */
@Data
public class MeetingGoodsVo {
    private Integer goodsId;
    private Integer typeId;
    private String typeName;
    private String goodsName;
    private Double uniValence;
    private String remark;
    private String imgUrl;
    private String belong; // 物品归属
}
