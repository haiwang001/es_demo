package com.bjxczy.onepark.goodsmodule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.goodsmodule.entity.MeetingGoods;
import com.bjxczy.onepark.goodsmodule.pojo.dto.MeetingGoodsDto;
import com.bjxczy.onepark.goodsmodule.pojo.vo.MeetingGoodsExcelVo;
import com.bjxczy.onepark.goodsmodule.pojo.vo.MeetingGoodsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MeetingGoodsMapper extends BaseMapper<MeetingGoods> {
    @Select("select a.belong,a.goods_id,a.img_url, a.goods_name, a.uni_valence, a.remark, b.type_name, b.type_id\n" +
            "from tbl_meeting_goods a\n" +
            "         left join tbl_meeting_goods_type b on a.type_id = b.type_id\n" +
            "where a.del_flag = 0\n" +
            "  and if(#{dto.typeId} is not null, a.type_id = #{dto.typeId}, true)\n" +
            "order by a.create_time desc")
    List<MeetingGoodsVo> selectAllByDto(@Param("dto") MeetingGoodsDto dto);

    @Select("select * from tbl_meeting_goods where del_flag = 0 order by create_time desc")
    List<MeetingGoodsExcelVo> selectExcelAllByDto();

    @Select("select * from tbl_meeting_goods where del_flag = 0 and type_id = #{typeId}")
    List<MeetingGoods> selectByTypeId(@Param("typeId") Integer typeId);
}
