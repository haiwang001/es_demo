package com.bjxczy.onepark.goodsmodule.service.impl;


import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.goodsmodule.pojo.dto.MeetingGoodsTypeDto;
import com.bjxczy.onepark.goodsmodule.service.MeetingGoodsTypeService;
import com.bjxczy.onepark.goodsmodule.entity.MeetingGoodsType;
import com.bjxczy.onepark.goodsmodule.mapper.MeetingGoodsMapper;
import com.bjxczy.onepark.goodsmodule.mapper.MeetingGoodsTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
 *@ClassName MeetingGoodsTypeImpl
 *@Author 温良伟
 *@Date 2023/2/16 16:31
 *@Version 1.0
 */
@Service
public class MeetingGoodsTypeServiceImpl implements MeetingGoodsTypeService {
    @Autowired
    private MeetingGoodsTypeMapper mapper;

    @Autowired
    private MeetingGoodsMapper GoodsMapper;

    @Override
    public int putGoodsType(MeetingGoodsTypeDto dto, UserInformation user) {
        return mapper.updateByDto(dto);
    }

    @Override
    public Object GoodsTypeList(String typeName) {
        return mapper.selectListByTypeName(typeName);
    }

    @Override
    public int delGoodsType(Integer typeId) throws Exception {
        // 删除类型前需要保证该类型下没有物品
        if (GoodsMapper.selectByTypeId(typeId).size() <= 0) {
            return mapper.deleteById(typeId);
        }
        throw new ResultException("该分类存在物品 无法删除!");
    }

    @Override
    public int addGoodsType(MeetingGoodsType goodsType, UserInformation user) throws Exception {
        if (mapper.selectByTypeName(goodsType.getTypeName()) <= 0) {
            goodsType.setOperator(user.getStaffName());
            return mapper.insert(goodsType);
        }
        throw new ResultException("该物品分类已经存在");
    }
}
