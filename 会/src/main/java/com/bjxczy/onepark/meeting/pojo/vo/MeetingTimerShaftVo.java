package com.bjxczy.onepark.meeting.pojo.vo;


import lombok.Data;
import java.util.Date;

/*
 *@ClassName TimerShaft
 *@Author 温良伟
 *@Date 2023/2/21 14:58
 *@Version 1.0
 */
@Data
public class MeetingTimerShaftVo {
    private String roomName;//会议室名称
    private Date openDate;//会议开始时间
    private Date endDate;//会议结束时间
    private Integer useState;//使用状态  1 未开始 进行中  3已结束
    private Integer minTime;//会议时长
}
