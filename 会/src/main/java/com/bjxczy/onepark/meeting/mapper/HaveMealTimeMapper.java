package com.bjxczy.onepark.meeting.mapper;



import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.meeting.entity.HaveMealTime;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/*
 *@Author wlw
 *@Date 2023/5/16 14:49
 */
@Mapper
public interface HaveMealTimeMapper extends BaseMapper<HaveMealTime> {
    @Select("select  * from m_have_meal_time where meeting_id = #{meetingId}")
    HaveMealTime selectBy(@Param("meetingId") String meetingId);
}
