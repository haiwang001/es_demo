package com.bjxczy.onepark.meeting.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/4/26 10:30 座次表
 */
@Data
@TableName("m_seat_table")
public class SeatTable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer pId;
    private String name;
    private String data;
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag; //del_flag

    @TableField(exist = false) // 不是数据库字段：
    List<SeatTable> list = new ArrayList<>();

}
