package com.bjxczy.onepark.meeting.pojo.vo;


import lombok.Data;
/*
 *@ClassName mobileMeetingInfoVo
 *@Author 温良伟
 *@Date 2023/3/9 18:02
 *@Version 1.0
 */
@Data
public class MobileMeetingInfoVo {
    private String meetingTheme;//会议主题
    private String roomName;//会议室名称
    private String openDate;//会议开始时间
    private String appointmentName;//会议预约人
    private Integer useState;//使用状态  1 未开始 2进行中  3已结束 4 已经取消
    private String endDate;//会议结束时间
    private String company;//所属企业
    private String mobile ;//手机号
    private String roomType;//会议室类型
    private Integer number;//人数
}
