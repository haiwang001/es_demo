package com.bjxczy.onepark.meeting.pojo.vo;


import lombok.Data;


/*
 *@Author wlw
 *@Date 2023/5/22 12:15
 */
@Data
public class MeetingDateVo {
    private String meetingTheme;
    private String useState;
    private String date;
    private String meetingId; // 会议id
}
