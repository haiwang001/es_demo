package com.bjxczy.onepark.meeting.service;


import com.bjxczy.onepark.meeting.entity.SeatTable;

/*
 *@Author wlw
 *@Date 2023/4/26 10:59
 */
public interface SeatTableService {
    int put(SeatTable table);

    Object tree(SeatTable table);

    Object list(SeatTable table);
}
