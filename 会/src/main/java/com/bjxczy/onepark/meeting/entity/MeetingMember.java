package com.bjxczy.onepark.meeting.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/*
 *@ClassName MeetingMember
 *@Author 温良伟
 *@Date 2023/2/20 10:02
 *@Version 1.0
 * 参会人信息 绑定
 */
@Data
@TableName("tbl_meeting_member")
public class MeetingMember {
    @TableId(type = IdType.AUTO)
    private Integer id;
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;
    private String meetingId; // 会议id
    private String seatCode; // 座位码  seat_code
    private String typeDisplay; // 人员分类展示  type_display
    private String imgUrl; // 人像照片  img_url
    private Integer signIn;//sign_in 签到    0 未签到  1签到
    private Date signInDate;//sign_in_date 签到时间
    private Boolean source; // 来源  系统内选择  或者 人工录  暂不不维护
    private String name;  // 参会人名
    private String mobile;  // 参会人手机号  代替员工编号

    private String faceUrl;  // 在会管的人脸照片路径 用于删除 face_url
    private String deviceIndexCode;  // 在会管的设备 编码 用于删除 device_index_code
    private Integer deleteRecord;  //  delete_record 会管平台人员删除状态   0 未删除  1 删除 避免重复执行删除
    private String notice;  //  手动 发送通知   已发送  未发送
    private Integer autoNotice;  // 自动发送    1 已发送  0 未发送   auto_notice

 }
