package com.bjxczy.onepark.meeting.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.meeting.entity.Accommodation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AccommodationMapper extends BaseMapper<Accommodation> {

    @Select("select * from m_accommodation where meeting_id = #{meetingId}")
    List<Accommodation> selectBy(@Param("meetingId") String meetingId);
}
