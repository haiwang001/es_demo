package com.bjxczy.onepark.meeting.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.meeting.entity.MeetingMember;
import com.bjxczy.onepark.hikmodule.pojo.vo.ContextVo;
import com.bjxczy.onepark.meeting.pojo.vo.SignInVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface MeetingMemberMapper extends BaseMapper<MeetingMember> {
    @Update("update tbl_meeting_member\n" +
            "set sign_in  = 1,\n" +
            "    sign_in_date = #{context.dateTime}\n" +
            "where del_flag = 0\n" +
            "  and meeting_id = #{context.meetingId}\n" +
            "  and mobile = #{context.employeeNo}")
    Integer updateDate(@Param("context") ContextVo context);

    @Update("update tbl_meeting_member set delete_record = 1 where id = #{id}")
    void updateDeleteRecordById(@Param("id") Integer id);

    @Update("update tbl_meeting_member\n" +
            "set face_url = #{url},\n" +
            "        device_index_code = #{deviceId}\n" +
            "where del_flag = 0\n" +
            "  and name =   #{name}\n" +
            "  and mobile = #{mobile}\n" +
            "  and meeting_id = #{meetingId}")
    void updateInfoByName(@Param("meetingId") String meetingId,
                          @Param("mobile") String mobile,
                          @Param("name") String name,
                          @Param("url") String url,
                          @Param("deviceId") String deviceId);

    @Select("select *\n" +
            "from tbl_meeting_member\n" +
            "where del_flag = 0\n" +
            "and meeting_id = #{meetingId}")
    List<MeetingMember> selectByMeetingId(@Param("meetingId") String meetingId);


    @Select("select name,mobile\n" +
            "from tbl_meeting_member\n" +
            "where del_flag = 0\n" +
            "and meeting_id = #{meetingId}")
    List<SignInVo> selectSignInVoByMeetingId(@Param("meetingId") String meetingId);

    @Select("select id\n" +
            "from organization.tbl_staff\n" +
            "where del_flag = 0\n" +
            "and fld_name = #{name} and mobile = #{mobile} group by fld_name,mobile")
    Integer inside(@Param("name") String name, @Param("mobile") String mobile);

    @Update("update office.tbl_meeting_member set notice = #{member.notice} where id = #{member.id}")
    void updateNoticeBYId(@Param("member") MeetingMember member);

    @Select("select *\n" +
            "from tbl_meeting_member\n" +
            "where del_flag = 0\n" +
            "and meeting_id = #{id}\n" +
            "and name = #{name}\n" +
            "and mobile = #{mobile}")
    MeetingMember selectByNameAndMobileAndMeetingId(
            @Param("name") String name, @Param("mobile") String mobile,
            @Param("id") String id);


    @Select("select *\n" +
            "from tbl_meeting_member\n" +
            "where del_flag = 0\n" +
            "and meeting_id = #{id}\n" +
            "and mobile = #{mobile}")
    MeetingMember selectByMobileAndMeetingId(@Param("mobile") String mobile,
                                             @Param("id") String id);
}
