package com.bjxczy.onepark.meeting.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;


/*
 *@ClassName meetingInfoGoods
 *@Author 温良伟
 *@Date 2023/2/20 9:54
 *@Version 1.0
 * 本次会议所需物品记录
 */
@Data
@TableName("tbl_meeting_info_goods")
public class MeetingInfoGoods {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String meetingId; // 会议id
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;
    private Integer goodsId; //物品id
    private String goodsName;//物品名
    private Integer amount; // 数量
    private Double uniValence; //单价


    @TableField(exist = false) // 不是数据库字段：
    private String typeName;
    @TableField(exist = false) // 不是数据库字段：
    private String imgUrl;
    @TableField(exist = false) // 不是数据库字段：
    private String belong; // 物品归属
}
