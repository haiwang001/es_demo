package com.bjxczy.onepark.meeting.service.impl;


import cn.hutool.core.lang.Validator;
import com.bjxczy.onepark.common.utils.date.DateUtil;
import com.bjxczy.onepark.common.utils.json.JsonUtils;
import com.bjxczy.onepark.common.utils.poi.ExcelUtils;
import com.bjxczy.onepark.common.utils.sys.PageUtils;
import com.bjxczy.onepark.common.utils.uid.UuId;
import com.bjxczy.onepark.hikmodule.pojo.vo.ContextVo;
import com.bjxczy.onepark.hikmodule.pojo.vo.DeviceVo;
import com.bjxczy.onepark.meeting.mapper.AccommodationMapper;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.hikmodule.api.SignInApi;
import com.bjxczy.onepark.common.utils.file.ImgUtils;
import com.bjxczy.onepark.common.utils.poi.WorkBook;
import com.bjxczy.onepark.meeting.mapper.HaveMealTimeMapper;
import com.bjxczy.onepark.meeting.mapper.MeetingInfoGoodsMapper;
import com.bjxczy.onepark.meeting.mapper.MeetingInfoMapper;
import com.bjxczy.onepark.meeting.mapper.MeetingMemberMapper;
import com.bjxczy.onepark.meeting.pojo.vo.*;
import com.bjxczy.onepark.publicmodule.service.MsgTemplateService;
import com.bjxczy.onepark.publicmodule.service.WorkOrderTemplateService;
import com.bjxczy.onepark.room.entity.ConferenceRoom;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;
import com.bjxczy.onepark.meeting.entity.MeetingInfoGoods;
import com.bjxczy.onepark.meeting.entity.MeetingMember;
import com.bjxczy.onepark.meeting.pojo.dto.MeetingInfoDto;
import com.bjxczy.onepark.meeting.pojo.dto.NoticeDto;
import com.bjxczy.onepark.publicmodule.interfaces.feign.client.FileServiceFaceFeign;
import com.bjxczy.onepark.meeting.service.MeetingInfoService;
import com.bjxczy.onepark.room.mapper.ConferenceRoomMapper;
import com.google.common.collect.ImmutableMap;
import lombok.extern.log4j.Log4j2;
import org.apache.http.entity.ContentType;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.PictureData;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
public class MeetingInfoServiceImpl implements MeetingInfoService {
    @Resource
    private MeetingInfoMapper mapper;
    @Resource
    private ConferenceRoomMapper RoomMapper;
    @Resource
    private MeetingInfoGoodsMapper GoodsMapper;
    @Resource
    private MeetingMemberMapper MemberMapper;
    @Resource
    private WorkOrderTemplateService OrderService;

    @Override
    public Object getSignInById(String meetingId) {
        HashMap<String, Object> hashMap = new HashMap<>();
        // 查询签到
        List<SignInVo> signInVos = mapper.selectSignIn(meetingId);
        // 未签到
        List<String> list = mapper.selectFailSignIn(meetingId);
        // 查询日期
        hashMap.put("openDate", mapper.selectDateBymeetingId(meetingId));
        // 总人数
        hashMap.put("number", list.size() + signInVos.size());
        hashMap.put("signIn", signInVos);
        hashMap.put("failSignIn", list);
        return hashMap;
    }

    @Override
    public void downloadExcel(String type, String name, HttpServletResponse response) {
        try {
            if ("1".equals(type)) {
                ExcelUtils.export(response, "【" + name + "】会议信息", "【" + name + "】会议信息", mapper.selectCompanyByName(name), MeetingCompanyVo.class);
            }
            if ("2".equals(type)) {
                ExcelUtils.export(response, "【" + name + "】参会人员信息", "【" + name + "】参会人员信息", mapper.getMemberById(name), MeetingMemberVo.class);
            }
        } catch (IOException e) {
            log.error("会议统计详情导出失败 " + e.getMessage());
            throw new ResultException("会议详情导出失败!");
        }
    }

    /**
     * @param type 访客  visitor  园区通行人脸 face
     * @return
     */
    @Override
    public Object getInfo(String type) {
        if ("face".equals(type)) {
            return mapper.selectFace();
        }
        if ("visitor".equals(type)) {
            return mapper.selectVisitor();
        }
        throw new ResultException("请输入type face 员工 visitor访客");
    }

    @Override
    public Object look(String name) {
        MeetingInfoDto dto = new MeetingInfoDto();
        dto.setName(name);
        // 基本信息
        List<MeetingStatisticsVo> list = getMeetingStatisticsVo(dto);
        // 数据列表
        List<MeetingCompanyVo> companyVos = mapper.selectCompanyByName(name);
        // 查询所有的参会人员
        List<MeetingMemberVo> memberVos = mapper.getMemberById(name);
        /*  查询 前30天的会议   MeetingAmount */
        List<MeetingAmountVo> voList = mapper.selectByNameAndDate(name, onceUponATime(2));// 根据会议的预约公司分组
        HashMap<String, Object> map = new HashMap<>();
        map.put("basic", list.get(0));// 基础信息
        map.put("expense", voList);// 费用
        map.put("company", companyVos);// 公司维度
        map.put("personnel", memberVos);// 人员维度
        return map;
    }

    @Override
    public Object statistics(MeetingInfoDto dto) {
        String date = onceUponATime(dto.getTimIng());
        if (!"".equals(date)) {
            dto.setOpenDate(date);
            dto.setEndDate(DateUtil.format(new Date(), "yyyy-MM-dd") + " 59:59:59");
        }
        return PageUtils.Page(dto, getMeetingStatisticsVo(dto));
    }

    private List<MeetingStatisticsVo> getMeetingStatisticsVo(MeetingInfoDto dto) {
        List<MeetingStatisticsVo> list = mapper.selectByName(dto);// 根据会议的预约公司分组
        Map<String, List<MeetingStatisticsVo>> map = list.stream().collect(Collectors.groupingBy(MeetingStatisticsVo::getEnterprise));
        List<MeetingStatisticsVo> data = new ArrayList<>(map.size());
        map.forEach((key, li) -> data.add(new MeetingStatisticsVo(key, li.size(), getTotalTime(li), getTotalMoney(li))));
        return data;
    }

    private String getTotalTime(List<MeetingStatisticsVo> data) {
        Integer count = 0; //总时长
        Integer hour = 60; //60分钟为一小时
        for (MeetingStatisticsVo vo : data) {
            if (vo.getMinTime() != null) {
                count += vo.getMinTime();
            }
        }
        int i = count / hour; //小时
        int i1 = count % hour;//分钟
        return i + "小时 " + i1 + "分钟";
    }

    /**
     * 获取 总金额
     *
     * @param data
     * @return
     */
    private Double getTotalMoney(List<MeetingStatisticsVo> data) {
        Double count = 0D; //总金额
        for (MeetingStatisticsVo vo : data) {
            if (vo.getActuallyPaid() != null) {
                count += vo.getActuallyPaid();
            }
        }
        return count;
    }

    @Override
    public Object lineChart(MeetingInfoDto dto) {
        dto.setTimIng(1); // 默认设置为7天
        String date = onceUponATime(dto.getTimIng());
        if (!"".equals(date)) {
            dto.setOpenDate(date);
            dto.setEndDate(DateUtil.format(new Date(), "yyyy-MM-dd") + " 59:59:59");
        }
        List<MeetingInfoVo> list = mapper.lineChart(dto);// 查询最近七天数据
        list.forEach(el -> {// 时间全部成年月日
            String format1 = DateUtil.format(el.getOpenDate(), "MM-dd");
            el.setGroupTime(format1);
        });
        Map<String, List<MeetingInfoVo>> map = list.stream().collect(Collectors.groupingBy(MeetingInfoVo::getGroupTime));

        List<DateNodeVo> node = DateUtil.setWeekNode();
        node.forEach(el -> {
            map.forEach((kk, vv) -> {
                if (el.getX().equals(kk)) {
                    el.setY((long) vv.size());
                }
            });
        });
        return node;
    }

    @Override
    public Object pie(MeetingInfoDto dto) {
        String date = onceUponATime(dto.getTimIng());
        if (!"".equals(date)) {
            dto.setOpenDate(date);
            dto.setEndDate(DateUtil.format(new Date(), "yyyy-MM-dd") + " 59:59:59");
        }
        List<MeetingInfoVo> list = mapper.pieByDto(dto);
        Map<String, List<MeetingInfoVo>> map = list.stream().collect(Collectors.groupingBy(MeetingInfoVo::getRoomName));
        Map<String, Integer> iMap = new HashMap<>();
        map.forEach((k, v) -> {
            iMap.put(k, v.size());
        });
        return iMap;
    }

    @Override
    public Object histogram(MeetingInfoDto dto) {
        String date = onceUponATime(dto.getTimIng());
        if (!"".equals(date)) {
            dto.setOpenDate(date);
        }
        List<MeetingInfoVo> list = mapper.histogramByDto(dto);
        Map<Integer, List<MeetingInfoVo>> map = list.stream().collect(Collectors.groupingBy(MeetingInfoVo::getStars));
        Map<Integer, Integer> iMap = setHistogramNode(5);
        map.forEach((k, v) -> iMap.forEach((kk, vv) -> {
            if (Objects.equals(kk, k)) {
                iMap.put(k, v.size());
            }
        }));
        return iMap;
    }

    public Map<Integer, Integer> setHistogramNode(int histogramNode) {
        Map<Integer, Integer> iMap = new HashMap<>(histogramNode);
        for (int i = 1; i <= histogramNode; i++) {
            iMap.put(i, 0);
        }
        return iMap;
    }


    private static String onceUponATime(Integer TimIng) {
        if (null == TimIng || TimIng == 0) {
            return "";
        }
        int day = -1;
        if (TimIng == 1) {  //提前一周
            day += 7;
        }
        if (TimIng == 2) {  //提前一个月
            day += DateUtil.getTotalMonthDate();
        }
        if (TimIng == 3) {  //提前三个月
            day += DateUtil.getSeason();
        }
        if (TimIng == 4) {  //提前一年
            day += DateUtil.lengthOfYear(DateUtil.getYearNum());
        }
        if (day != -1) {
            day = (60 * 24 * day) * -1;
            return DateUtil.format(DateUtil.setDateTime(new Date(), day), "yyyy-MM-dd") + " 00:00:00";
        } else {
            return "";
        }
    }

    private HashMap<String, Map<Integer, String>> setNode(Map<String, List<MeetingTimerShaftVo>> map) {
        HashMap<String, Map<Integer, String>> map1 = new HashMap<>();
        map.forEach((k, v) -> { // 通过会议室名称分组后得到的数据 k 会议室名字  v 会议室
            Map<Integer, String> no = DateUtil.node(); //用于更改节点
            v.forEach(el -> { //
                Integer Open = initialIndex(el.getOpenDate()); // 开始索引
                Integer End = initialIndex(el.getEndDate()); // 结束索引
                for (int i = Open; i < End; i++) {
                    no.put(i, el.getUseState().toString());
                }
                map1.put(k, no);
            });
        });
        return map1;
    }

    /**
     * 获取起始索引
     *
     * @param openDate
     * @return
     */
    private Integer initialIndex(Date openDate) {
        int hh = Integer.parseInt(DateUtil.format(openDate, "HH"));
        int mm = Integer.parseInt(DateUtil.format(openDate, "mm"));
        Map<Integer, String> node = DateUtil.node(); //展开所有节点 用于对比
        for (int i = 0; i < node.size(); i++) {
            String s = node.get(i);// 获取所有节点
            int h = Integer.parseInt(s.split("~")[0].split(":")[0]);// 获取小时
            int m = Integer.parseInt(s.split("~")[0].split(":")[1]);// 获取分钟
            if (hh == h) {
                if (m == 30) {//前一个节点
                    if (mm < m) {
                        return i - 1;
                    }
                    if (mm >= 30) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }


    /**
     * 查询会议室的时间轴
     *
     * @param dto
     * @return
     */
    @Override
    public Object getInfoList(MeetingInfoDto dto) {
        dto.setOpenDate(dto.getOpenDate().split(" ")[0]); // 查询某一天的日期

        List<MeetingTimerShaftVo> list = mapper.getInfoListByDto(dto);
        list = sort(list);
        // 根据属性分组
        Map<String, List<MeetingTimerShaftVo>> map = list.stream().collect(Collectors.groupingBy(MeetingTimerShaftVo::getRoomName));
        // 封装节点数据
        HashMap<String, Map<Integer, String>> node01 = setNode(map);
        // 清除无用的时间数据
        node01.forEach((k, v) -> {
            v.forEach((vk, vv) -> {
                if (vv.length() > 3) {
                    v.put(vk, "0");
                }
            });
        });
        // 前端需要列表
        return mapToList(node01);
    }

    private ArrayList<Object> mapToList(HashMap<String, Map<Integer, String>> node01) {
        String key = "node";
        ArrayList<Object> list = new ArrayList<>();
        node01.forEach((k, y) -> {
            HashMap<String, Object> map = new HashMap<>();
            y.forEach((kk, yy) -> {
                map.put(key + kk, yy);
            });
            map.put("name", k);
            list.add(map);
        });
        return list;
    }


    private List<MeetingTimerShaftVo> sort(List<MeetingTimerShaftVo> list) {
        MeetingTimerShaftVo[] array = list.toArray(new MeetingTimerShaftVo[list.size()]);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                long time = array[j].getOpenDate().getTime();
                long time1 = array[j + 1].getOpenDate().getTime();
                if (time > time1) {
                    MeetingTimerShaftVo q = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = q;
                }
            }
        }
        return Arrays.asList(array);
    }

    @Override
    public void downloadData(HttpServletResponse response, MeetingInfoDto dto) throws Exception {
        ExcelUtils.export(response, SysConst.MEETING_FILE_NAME, SysConst.MEETING_FILE_NAME, mapper.selectAllByDto(dto), MeetingInfoExcelVo.class);
    }

    @Override
    public int setComment(MeetingInfoDto dto) {
        return mapper.updateCommentById(dto);
    }


    @Override
    public Object mobileList(Integer useState, String staffName) {
        return mapper.selectListByUseStateAndStaffName(useState, staffName);
    }


    /**
     * 会议餐饮时间映射
     */
    @Resource
    private HaveMealTimeMapper haveMealTimeMapper;
    /**
     * 会议住宿映射
     */
    @Resource
    private AccommodationMapper accommodationMapper;

    @Override
    public Object queryList(MeetingInfoDto dto) {
        List<MeetingInfoVo> list = mapper.selectListByDto(dto);
        if (dto.getCreateDateSort() != null) {
            if (dto.getCreateDateSort()) {
                list.sort(Comparator.comparing(MeetingInfoVo::getCreateTime)); // 从小达到 升序
            }
        }
        if (dto.getOpenDateSort() != null) {
            if (dto.getOpenDateSort()) {
                list.sort(Comparator.comparing(MeetingInfoVo::getOpenDate)); // 从小达到 升序
            }
        }
        if (dto.getEndDateSort() != null) {
            if (dto.getEndDateSort()) {
                list.sort(Comparator.comparing(MeetingInfoVo::getEndDate)); // 从小达到 升序
            }
        }
        if (dto.getIsDesc() != null) {
            if (dto.getIsDesc()) {
                Collections.reverse(list); // 从大到小 逆序
            }
        }

        list.forEach(el -> {
            el.setDailyMealMap(JsonUtils.toBeanMap(el.getDailyMeal()));
            el.setOpenDateLabel(DateUtil.format(el.getOpenDate(), "yyyy-MM-dd HH:mm"));
            el.setEndDateLabel(DateUtil.format(el.getEndDate(), "yyyy-MM-dd HH:mm"));
            HashMap<String, Object> map = new HashMap<>(5);
            map.put("meeting_id", el.getMeetingId());
            map.put("del_flag", 0);
            el.setGoodsList(GoodsMapper.selectByMeetingId(el.getMeetingId()));
            HashMap<String, Object> map1 = new HashMap<>(5);
            map1.put("meeting_id", el.getMeetingId());
            map1.put("source", true);
            map1.put("del_flag", 0);
            HashMap<String, Object> map2 = new HashMap<>(5);
            map2.put("meeting_id", el.getMeetingId());
            map2.put("source", false);
            map2.put("del_flag", 0);
            el.setReplenishNotifierList(MemberMapper.selectByMap(map2));
            el.setAccommodationList(accommodationMapper.selectBy(el.getMeetingId()));
            el.setHaveMealTime(haveMealTimeMapper.selectBy(el.getMeetingId()));
        });
        // 需要恢复成对象
        return PageUtils.Page(dto, list);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateState(MeetingInfoDto dto) throws Exception {
        // 需要 联动会议室 设置为未清扫
        if (dto.getUseState() == 3 || dto.getUseState() == 4) { // 联动会议室状
            MeetingInfo select = mapper.selectById(dto.getMeetingId());//通过会议室id修改为待清扫
            RoomMapper.updateHygieneState(select.getRoomId(), "待清扫");
        }
        int update = mapper.updateById(new MeetingInfo(dto.getMeetingId(), dto.getUseState()));
        if (update == 1) {
            // 发送取消通知
            cancelmeeting(dto);
            // 更新代办状态
            msgTemplateService.updateCommissionItem(1, "已取消!");
            // 清除取消人员在会管的信息
            MemberMapper.selectByMeetingId(dto.getMeetingId()).forEach(this::clearInfo);
        }
        return update;
    }

    public void clearInfo(MeetingMember el) {
        if (el.getFaceUrl() != null && el.getDeviceIndexCode() != null && el.getDeleteRecord() == 0) {
            // 清除人员
            SignInApi.clearPeople(el.getDeviceIndexCode(), el.getMobile());
            // 清除照片
            SignInApi.clearFace(el.getFaceUrl(), el.getDeviceIndexCode(), el.getMobile(), el.getName());
            //  修改为已删除状态 避免重复
            MemberMapper.updateDeleteRecordById(el.getId());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int update(MeetingInfo info) throws Exception {
        info = updateSetMeetingInfo(info);
        HashMap<String, Object> map = new HashMap<>(1);
        map.put("meeting_id", info.getMeetingId());
        //删除之前变更 触发海康的人员变更  未完成
        GoodsMapper.deleteByMap(map);
        MemberMapper.deleteByMap(map);
        int i = mapper.updateById(info);
        info.getGoodsList().forEach(el -> {
            GoodsMapper.insert(el);
        });
        info.getReplenishNotifierList().forEach(el -> {
            MemberMapper.insert(el);
        });
        return i;
    }


    private MeetingInfo updateSetMeetingInfo(MeetingInfo info) throws Exception {
        String s = info.getAppointmentName().replaceAll("\\s*", "");
        if ("".equals(s)) {
            throw new ResultException("预约人姓名数据错误");
        }
        info.setAppointmentName(s);
        // 会议编号
        info.setUpdateTime(new Date());
        //参会人员
        for (MeetingMember el : info.getReplenishNotifierList()) {
            el.setDelFlag(0);
            el.setMeetingId(info.getMeetingId());
            el.setSource(false);
        }
        //会议物品
        for (MeetingInfoGoods el : info.getGoodsList()) {
            el.setDelFlag(0);
            el.setMeetingId(info.getMeetingId());
        }
        return info;
    }

    @Override
    public List<MeetingMemberExcelVo> uploadingExcel(MultipartFile file) throws Exception {
        if (!Objects.requireNonNull(file.getOriginalFilename()).contains(SysConst.CREW_FILE_NAME))
            throw new ResultException("请使用下载的模板文件进行导入数据");
        HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream());//创建表
        HSSFSheet sheet = workbook.getSheetAt(0); //获取第一张表
        return setBeans(sheet);
    }

    @Override
    public List<MeetingMemberExcelVo> importUploadingExcel(MultipartFile file, String openDate, String endDate) throws Exception {
        List<MeetingMemberExcelVo> list = uploadingExcel(file);
        for (MeetingMemberExcelVo vo : list) {
            if (null == vo.getMsg() || vo.getMsg().equals("")) {//没有错误信息
                try {
                    conflictPeople(vo.getName(), vo.getMobile(), openDate, endDate);
                } catch (Exception e) {
                    vo.setMsg(e.getMessage());
                }
            }
        }
        return list;
    }

    @Override
    public Object importSignIn(MultipartFile file, String id) throws Exception {
        if (!Objects.requireNonNull(file.getOriginalFilename()).contains(SysConst.MEETING_SIGN_IN))
            throw new ResultException("请使用下载的模板文件进行导入数据");
        return setImportSignInBeans(WorkBook.getHSSFSheet(file), id);
    }

    public Object setImportSignInBeans(HSSFSheet sheet, String id) {
        List<SignInVo> list = new ArrayList<>(sheet.getLastRowNum());
        for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
            HSSFRow row = sheet.getRow(rowIndex);
            String name = null;
            try {
                name = row.getCell(0).getStringCellValue();
            } catch (Exception e) {
                try {
                    SignInVo vo = new SignInVo("", null, DateUtil.now(), "签到失败! 请使用下载的签到表数据签到");
                    list.add(vo);
                    break;
                } catch (Exception exception) {
                    break;
                }
            }
            String mobile = null;
            try {
                mobile = row.getCell(1).getStringCellValue();
            } catch (Exception e) {
                SignInVo vo = new SignInVo("", "", DateUtil.now(), "签到失败! 请使用下载的签到表数据签到");
                list.add(vo);
                break;
            }
            MeetingMember member = MemberMapper.selectByNameAndMobileAndMeetingId(name, mobile, id);
            if (member != null) {
                if (member.getSignIn() == 0) { //未签到更新 已签到的不执行
                    String s = row.getCell(2) + "";
                    if (s.contains("是") || s.contains("已") || s.contains("已签到")) {
                        Integer integer = MemberMapper.updateDate(new ContextVo(mobile, DateUtil.now(), id));
                        if (integer == 1) {
                            list.add(new SignInVo(name, mobile, DateUtil.now(), "签到成功!"));
                        } else {
                            list.add(new SignInVo(name, mobile, DateUtil.now(), "签到失败!"));
                        }
                    } else {
                        list.add(new SignInVo(name, mobile, DateUtil.now(), "未签到!"));
                    }
                } else {
                    list.add(new SignInVo(name, mobile, DateUtil.now(), "已签到!"));
                }
            } else {
                list.add(new SignInVo(name, mobile, DateUtil.now(), "签到失败! 人员不在此会议"));
            }
        }
        return list;
    }

    public List<MeetingMemberExcelVo> setBeans(HSSFSheet sheet) throws Exception {
        List<MultipartFileVo> files = new ArrayList<>(); // 文件
        try {
            files = getPictures1(sheet);
        } catch (Exception e) {
            log.error("图片转换异常" + e.getMessage());
            throw new ResultException("照片读取失败!");
        }
        List<MeetingMemberExcelVo> list = new ArrayList<>(sheet.getLastRowNum());
        for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
            HSSFRow row = sheet.getRow(rowIndex);
            MeetingMemberExcelVo vo = new MeetingMemberExcelVo();
            String name = null;
            if (row.getCell(0) != null && !"".equals(row.getCell(0) + "")) {
                name = row.getCell(0).getStringCellValue();
                vo.setName(name); // 获取姓名
            } else {
                vo.setName(name); // 获取姓名
                vo.setMsg("姓名未填写"); // 获取姓名
            }
            String mobile = "";
            try {
                DecimalFormat format = new DecimalFormat("0");
                mobile = format.format(row.getCell(1).getNumericCellValue());
            } catch (Exception e) {
                vo.setMobile(null);
                vo.setMsg("手机号格式错误!");
            }
            if (!Validator.isMobile(mobile)) {
                vo.setMobile(mobile);
                vo.setMsg("手机号格式错误!");
            } else {
                vo.setMobile(mobile);
            }
            if (row.getCell(3) != null) {
                vo.setType(row.getCell(3).getStringCellValue());
            } else {
                vo.setType("外部");
            }
            try {
                vo.setCode(row.getCell(4).getStringCellValue());
            } catch (Exception e) {
                try {
                    DecimalFormat format = new DecimalFormat("0");
                    vo.setCode(format.format(row.getCell(4).getNumericCellValue()));
                } catch (Exception exception) {
                    vo.setCode(null);
                }
            }
            list.add(vo);
        }
        if (list.size() != files.size()) {
            throw new ResultException("照片和人名数量不一致!");
        }
        try {
            for (int i = 0; i < files.size(); i++) {
                MultipartFileVo fileVo = files.get(i);
                if (fileVo.getMsg() != null) {
                    list.get(i).setMsg(fileVo.getMsg());
                } else {
                    list.get(i).setImgUrl(fileServiceFaceFeign.fileUpload(fileVo.getMultipartFile()));
                }
               /*
                更换为校验的图片接口
                R<?> upload = FaceFile.upload(multipartFile);
                if ("文件类型非法，无法上传".equals(upload.getMessage())) {
                    list.get(i).setMsg("请删除错误数据列!");
                }
                list.get(i).setImgUrl(upload.getMessage());*/
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResultException("服务器连接超时! 请重新上传");
        }
        return list;
    }

    @Override
    public boolean conflictPeople(String name, String mobile, String openDate, String endDate) {
        int conflictPeople = mapper.conflictPeople(name, mobile, openDate, endDate);
        if (conflictPeople != 0) {
            throw new ResultException(name + "在" + openDate + " - " + endDate + " 参加了其他会议!");
        }
        return true;
    }

    @Override
    public Boolean conflictDate(String roomId, String openDate, String endDate, String meetingId) throws Exception {
        if ("".equals(roomId) || null == roomId ||
                "".equals(endDate) || null == endDate ||
                "".equals(openDate) || null == openDate) {
            throw new ResultException("参数空!");
        }
        ConferenceRoom room = RoomMapper.selectById(roomId);
        if (room.getRoomState() == 1) {
            throw new ResultException("会议室已被禁用!");
        }
        List<MeetingInfo> infos = mapper.selectByOpenDate(roomId, openDate, endDate, meetingId);
        if (infos.size() != 0) {
            throw new ResultException("该时间段会议室正在使用中!");
        }
        return true;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Object addMeetingInfo(MeetingInfo info, UserInformation user) throws Exception {
        info = setMeetingInfo(info); // 封装 会议信息
        if (conflictDate(info.getRoomId(), info.getOpenDate().toString(), info.getEndDate().toString(), null)) {
            //查询当前会议室的局址;
            List<ConferenceRoom> map1 = RoomMapper.selectByMap(
                    ImmutableMap.of("room_name", info.getRoomName(), "del_flag", 0)
            );
            if (map1.size() != 0) {
                info.setRegionId(map1.get(0).getRegionId());
            }
            // 需要判断食宿需求
            info.setDailyMeal(JsonUtils.toJsonString(info.getDailyMealMap()));//转换成json存储 早 中 晚
            if (info.getMeetingSource() == 3 || info.getMeetingSource() == 2) { // 判断是否是移动端 不需要增加通知人员和参会人员会议物品
                info.setRuleAccounting("按小时");
                return mapper.insert(info);
            }
            info.getGoodsList().forEach(el -> GoodsMapper.insert(el)); // 会议物品
            info.getReplenishNotifierList().forEach(el -> MemberMapper.insert(el)); // 参会人员
            //  查询会议室
            ConferenceRoom conferenceRoom = RoomMapper.selectById(info.getRoomId());
            if (info.getIsWorkOrder()) {
                OrderService.TriggerWorkOrder(info, user, conferenceRoom); // 工单会议可选
            }
            if (info.getStayState()) {
                OrderService.cateringWorkOrder(info, user, conferenceRoom);    //关联餐饮工单
            }
            if (info.getAmountState()) {
                OrderService.hotelWorkOrder(info, user, conferenceRoom);  //关联酒店工单
            }
            if (info.getIsSignIn()) {
                try {
                    signInMachine(info);   //  下发签到一体机
                } catch (Exception e) {
                    log.error("[一体机授权异常] {}", e.getMessage());
                }
            }
            // 入库餐饮时间
            if (info.getHaveMealTime() != null)
                haveMealTimeMapper.insert(info.getHaveMealTime());
            //  住宿房间入库
            if (info.getAccommodationList() != null)
                info.getAccommodationList().forEach(el -> accommodationMapper.insert(el));
            int insert = mapper.insert(info);
            if (insert == 1) {
                // 给预订人发送代办
                msgTemplateService.subscriberNotice(info, user);
                // 发送参会通知
                inform(info);
            }
            return info.getMeetingId();
        }
        return null;
    }

    @Resource
    private MsgTemplateService msgTemplateService;

    /**
     * 会议创建自动触发
     *
     * @param meetingInfo 全量的会议信息
     */
    private void inform(MeetingInfo meetingInfo) {
        for (MeetingMember member : meetingInfo.getReplenishNotifierList()) {
            // 信息接收人
            Integer inside = MemberMapper.inside(member.getName(), member.getMobile());
            if (inside != null) {// 表示内部员工
                msgTemplateService.participateNotice(meetingInfo, inside, "请及时参加!");
            }
            // 发送短息未完成
        }
    }

    /**
     * 手动触发
     *
     * @param dto
     */
    @Override
    public void notice(NoticeDto dto) {
        List<MeetingMember> memberList = MemberMapper.selectByMeetingId(dto.getMeetingId());
        // 发送内部 判断是否是内部
        MeetingInfo meetingInfo = mapper.selectById(dto.getMeetingId());
        for (MeetingMember member : memberList) {
            Integer inside = MemberMapper.inside(member.getName(), member.getMobile());
            if (inside != null) {// 表示内部员工
                msgTemplateService.participateNotice(meetingInfo, inside, "请及时参加!");
                // 修改为已发送
                member.setNotice("已发送");
                MemberMapper.updateNoticeBYId(member);
            }
            // 发送短息未完成
        }
    }

    /**
     * 取消会议发送
     *
     * @param dto
     */
    private void cancelmeeting(MeetingInfoDto dto) {
        // 参会人
        List<MeetingMember> memberList = MemberMapper.selectByMeetingId(dto.getMeetingId());
        // 查找会议 用到会议的信息
        MeetingInfo meetingInfo = mapper.selectById(dto.getMeetingId());
        for (MeetingMember member : memberList) {
            Integer inside = MemberMapper.inside(member.getName(), member.getMobile());
            if (inside != null) {// 表示内部员工
                msgTemplateService.participateNotice(meetingInfo, inside, "已取消!");
            }
            // 发送短息未完成
        }
    }


    /**
     * 消息发送
     */
   /* @Resource
    private MessageService messageService;*/
    private void signInMachine(MeetingInfo info) {
        Objects.requireNonNull(SignInApi.selectRoomDevice()).forEach(dev -> {  //  查询设备
            this.createMeeting(info, dev);   // 给设备创建会议
        });
    }


    private void createMeeting(MeetingInfo info, DeviceVo dev) {
        List<MeetingMember> list = info.getReplenishNotifierList();
        // 参会人 多人 ,拼接起来
        StringBuilder employee = new StringBuilder();
        for (MeetingMember p : list) {
            // 录入人 到会议管理平台
            SignInApi.InputPersonnel(p.getMobile(), p.getName(), dev.getDeviceId());
            // 人脸信息 上传到会议管理平台
            String url = SignInApi.addFace(p.getImgUrl(), p.getMobile());
            // 设备  人  照片 三者绑定
            SignInApi.binding(p.getMobile(), p.getName(), dev.getDeviceId(), url);
            MemberMapper.updateInfoByName(info.getMeetingId(), p.getMobile(), p.getName(), url, dev.getDeviceId());
            // 拼接参会人 158...,18...,... 格式
            if (!list.get(list.size() - 1).getMobile().equals(p.getMobile())) {// 不是最后一个元素
                employee.append(p.getMobile()).append(",");
            } else {
                employee.append(p.getMobile());
            }
        }
        // 创建会议
        SignInApi.CreateMeeting(info.getMeetingId(), info.getRoomName(), info.getAppointmentName(), info.getMeetingTheme(), info.getOpenDate(), info.getEndDate(), employee, dev.getRoomId().get(0));
    }

    /**
     * 封装数据
     *
     * @param info
     * @return
     */
    public MeetingInfo setMeetingInfo(MeetingInfo info) throws Exception {
        String s = info.getAppointmentName().replaceAll("\\s*", "");
        if ("".equals(s)) {
            throw new ResultException("预约人姓名数据错误");
        }
        info.setAppointmentName(s); // 预约人
        info.setMeetingId(UuId.getId()); // 记录id
        info.setCreateTime(new Date());
        info.setUpdateTime(new Date());
        info.setDelFlag(0);
        info.setUseState(1); // 默认状态 1 未开始 2 进行中 3 已结束
        if (info.getHaveMealTime() != null) {
            info.getHaveMealTime().setMeetingId(info.getMeetingId());
        }
        if (info.getAccommodationList() != null) {
            info.getAccommodationList().forEach(el -> {
                el.setMeetingId(info.getMeetingId());
            });
        }
        // 会议编号
        info.setSerialNumber("HY" + DateUtil.format(new Date(), "yyyyMMddHHmmss") + UuId.getId().substring(0, 3));
        info.setMinTime((int) (info.getEndDate().getTime() - info.getOpenDate().getTime()) / 60000);
        if (info.getMeetingSource() == 3 || info.getMeetingSource() == 2) { // 判断是否是移动端 / eo单
            return info;
        }
        // 参会人员
        for (MeetingMember el : info.getReplenishNotifierList()) {
            el.setDelFlag(0);
            el.setMeetingId(info.getMeetingId());
            el.setSource(false);
            el.setNotice("未发送");
        }
        //会议物品
        for (MeetingInfoGoods el : info.getGoodsList()) {
            el.setDelFlag(0);
            el.setMeetingId(info.getMeetingId());
        }
        return info;
    }

    @Resource
    private FileServiceFaceFeign fileServiceFaceFeign;

    /**
     * 获取图片转换成为输入流
     *
     * @param sheet
     * @return
     * @throws IOException
     */
    public static List<MultipartFileVo> getPictures1(HSSFSheet sheet) throws Exception {
        double bytes = 1024D;
        Integer max = 200;
        ArrayList<MultipartFileVo> map = new ArrayList<>();
        List<HSSFShape> list = sheet.getDrawingPatriarch().getChildren();
        for (HSSFShape shape : list) {
            if (shape instanceof HSSFPicture) {
                HSSFPicture picture = (HSSFPicture) shape;
                PictureData pdata = picture.getPictureData();
                InputStream inputStream = new ByteArrayInputStream(pdata.getData());//转换成输入流
                MultipartFile fileResult = new MockMultipartFile("file", Math.random() + ".jpg",
                        ContentType.APPLICATION_OCTET_STREAM.toString(), inputStream);
                if (fileResult.getSize() / bytes > max) {// 大于200kb 开启压缩
                    log.info("压缩前:" + fileResult.getSize() / bytes);
                    fileResult = ImgUtils.compressionByMultipartFile(fileResult, 0.5F);
                    log.info("压缩后:" + fileResult.getSize() / bytes);
                }
                if (fileResult.getSize() / bytes > max) { //压缩后大于200kb  压缩倍率是 压缩0.5倍
                    map.add(new MultipartFileVo(0, null, "压缩后照片超" + max + "过kb 导入失败"));
                } else {
                    map.add(new MultipartFileVo(1, fileResult, null));
                }
            }
        }
        return map;
    }


    @Override
    public void downloadSignIn(HttpServletResponse response, String id) {
        try {
            ExcelUtils.export(response, SysConst.MEETING_SIGN_IN, SysConst.MEETING_SIGN_IN, MemberMapper.selectSignInVoByMeetingId(id), SignInVo.class);
        } catch (IOException e) {
            throw new ResultException("签到表下载失败！");
        }
    }

    @Override
    public Object calendar(String date, String name) {
        List<MeetingDateNodeVo> node = mapper.getNode("".equals(date + "") ? DateUtil.getYearAndMonth() : date);
        node.forEach(el -> el.setData(mapper.getNodeDate(el.getNode(), name)));
        return node;
    }
}