package com.bjxczy.onepark.meeting.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.meeting.entity.MeetingInfoGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MeetingInfoGoodsMapper extends BaseMapper<MeetingInfoGoods> {


    @Select("select a.*, b.img_url,b.type_name,b.belong\n" +
            "from tbl_meeting_info_goods a\n" +
            "         left join tbl_meeting_goods b on a.goods_id = b.goods_id\n" +
            "where a.del_flag = 0 and a.meeting_id = #{meetingId}")
    List<MeetingInfoGoods> selectByMeetingId(@Param("meetingId")String meetingId);
}
