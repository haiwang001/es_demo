package com.bjxczy.onepark.meeting.service;

import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;
import com.bjxczy.onepark.meeting.pojo.dto.MeetingInfoDto;
import com.bjxczy.onepark.meeting.pojo.dto.NoticeDto;
import com.bjxczy.onepark.meeting.pojo.vo.MeetingMemberExcelVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface MeetingInfoService {
    Object addMeetingInfo(MeetingInfo info, UserInformation user) throws Exception;

    List<MeetingMemberExcelVo> uploadingExcel(MultipartFile file) throws Exception;

    List<MeetingMemberExcelVo> importUploadingExcel(MultipartFile file, String openDate, String endDate) throws Exception;

    int update(MeetingInfo info) throws Exception;

    int updateState(MeetingInfoDto dto) throws Exception;

    Object queryList(MeetingInfoDto dto);

    int setComment(MeetingInfoDto dto);

    void downloadData(HttpServletResponse response, MeetingInfoDto dto) throws Exception;

    Object getInfoList(MeetingInfoDto dto);

    Object histogram(MeetingInfoDto dto);

    Object pie(MeetingInfoDto dto);

    Object lineChart(MeetingInfoDto dto);

    Object mobileList(Integer useState, String staffName);

    Object statistics(MeetingInfoDto dto);

    Object look(String name);

    Object getInfo(String type);

    void downloadExcel(String type, String name, HttpServletResponse response);

    Object getSignInById(String meetingId);

    Boolean conflictDate(String roomId, String openDate, String endDate, String meetingId) throws Exception;

    void notice(NoticeDto dto);

    Object importSignIn(MultipartFile file, String id) throws Exception;

    void downloadSignIn(HttpServletResponse response, String id);

    Object calendar(String date, String name);

    boolean conflictPeople(String name, String mobile, String openDate, String endDate);


}
