package com.bjxczy.onepark.meeting.pojo.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

@Data
public class MeetingInfoExcelVo {


    @Excel(name = "会议编号", width = 23, replace = {"_null"})
    private String serialNumber;//序号
    @Excel(name = "会议来源", width = 15, replace = {"_null"})
    private String meetingSourceLabel;//会议来源 // true 系统 false EO

    @Excel(name = "会议室名称", width = 23, replace = {"_null"})
    private String roomName;//会议室名称

    @Excel(name = "会议预约人", width = 15, replace = {"_null"})
    private String appointmentName;//会议预约人

    @Excel(name = "会议主题", width = 18, replace = {"_null"})
    private String meetingTheme;//会议主题

    @Excel(name = "会议分类", width = 15, replace = {"_null"})
    private String roomScopeLabel;//会议使用者分类 默认2 内部会议 1外部人员会议

    @Excel(name = "会议室类型", width = 18, replace = {"_null"})
    private String name;//会议室分类
    @Excel(name = "开始时间", width = 23, replace = {"_null"})
    private String openDate;//会议开始时间
    @Excel(name = "结束时间", width = 23, replace = {"_null"})
    private String endDate;//会议结束时间

    @Excel(name = "会议状态", width = 15, replace = {"_null"})
    private String useStateLabel;

    @Excel(name = "备注说明", width = 18, replace = {"_null"})
    private String remark;//备注信息
}
