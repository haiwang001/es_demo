package com.bjxczy.onepark.meeting.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.meeting.entity.SeatTable;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/4/26 11:00
 */
@Mapper
public interface SeatTableMapper extends BaseMapper<SeatTable> {

    @Update("update m_seat_table set data = #{table.data} where id = #{table.id} and del_flag =0")
    int put(@Param("table") SeatTable table);

    @Select("select id,name from m_seat_table where del_flag=0 and p_id != -1")
    List<SeatTable> selectAll();
}
