package com.bjxczy.onepark.meeting.pojo.vo;


import com.bjxczy.onepark.meeting.entity.Accommodation;
import com.bjxczy.onepark.meeting.entity.HaveMealTime;
import com.bjxczy.onepark.meeting.entity.MeetingInfoGoods;
import com.bjxczy.onepark.meeting.entity.MeetingMember;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/*
 *@ClassName MeetingInfoVo
 *@Author 温良伟
 *@Date 2023/2/20 18:11
 *@Version 1.0
 */
@Data
public class MeetingInfoVo {

    private String meetingSourceLabel;//会议来源 // true 系统 false EO
    private String roomScopeLabel;//会议使用者分类 默认2 内部会议 1外部人员会议
    private String useStateLabel;
    private String meetingId;//会议id uuid
    private Integer meetingSource;//会议来源 // 1 系统 2 EO
    private String serialNumber;//序号

    private String appointmentMobile;//会议预约人手机号
    private String openDateLabel;//会议开始时间展示
    private Date endDate;//会议结束时间
    private String endDateLabel;//会议结束时间展示
    private String addressLabel;//会议室位置        address_label                         v 1.3.7

    private String enterprise;//所属企业
    private Integer minTime;//会议时长
    private Double actuallyPaid;//实际金额
    private Integer numberOfPeople;//会议人数

    private String roomId;//会议室id

    private Boolean roomScope;//会议使用者分类 默认2 内部会议 1外部人员会议
    private String roomType;//会议室分类
    private String roomTypeLabel;//会议室展示
    private Double prepay;//预付金额
    private Boolean isSignIn;//是否签到
    private String remindTime;//提醒时间
    private Boolean isAccommodation;//住宿需求状态
    private String pushMethod;//推送方式  1个人消息 2个人代办 3短信
    private List<Integer> pushMethodLabel;//推送方式  1个人消息 2个人代办 3短信
    private String remark;//备注信息
    private Integer stars;// 评价星数
    private String comment;// 评价

    private Integer housingType; // 住宿房屋类型  1 标间 2 大床房 3 商务房 housing_type
    private Integer housingNum; // 住宿房屋数量 housing_num
    private Integer haveMealNum; // 就餐人的数量 have_meal_num
    private Integer stayArea;// 住宿的园区id stay_area

    private String stayAreaLabel;// 住宿的园区展示 stay_area_label
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date occupancyDate;// 入住时间 occupancy_date
    private String dailyMeal;//用于存储的就餐信息 daily_meal
    private HashMap<String, Boolean> dailyMealMap = new HashMap<>(3);
    private String groupTime;// 分组
    private String ruleAccounting ;//计费规则  rule_accounting
    private List<MeetingMember> replenishNotifierList;//会议成员补录
    private List<MeetingInfoGoods> goodsList;//会议物品列表

    private String meetingTheme;//会议主题
    private String roomName;//会议室名称
    private Date openDate;//会议开始时间
    private String appointmentName;//会议预约人
    private Integer useState;//使用状态  1 未开始 2进行中  3已结束 4 已经取消
    private Double roomPrepay;//会议室金额 room_prepay
    private String workRemark;//工单备注  work_remark
    private Boolean stayState;// 吃状态  stay_state
    private Boolean amountState;// 住房状态 amount_state
    private Boolean isWorkOrder;//工单关联 暂不维护
    private String workOrderType;//工单类型 暂不维护 work_order_type
    private String workOrderId;//工单id 暂不维护  work_order_id

    private String haveMealRemark;// 用餐备注  have_meal_remark                 V1.3.4
    private String haveMealLocation;// 用餐地点  have_meal_location             V1.3.4
    private String haveMealType;// 用餐方式  have_meal_type                     V1.3.4
    private List<Accommodation> accommodationList;//住宿信息列表                 V1.3.4
    private String hotelRemark;   // 住宿备注     hotel_remark                  V1.3.4
    private HaveMealTime haveMealTime;// 用餐时间 have_meal_time                V1.3.4
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String createTime;//创建时间
    private String operator;//操作人
}
