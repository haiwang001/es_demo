package com.bjxczy.onepark.meeting.service.impl;


import com.bjxczy.onepark.meeting.entity.SeatTable;
import com.bjxczy.onepark.meeting.mapper.SeatTableMapper;
import com.bjxczy.onepark.meeting.service.SeatTableService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/4/26 10:59
 */
@Log4j2
@Service
public class SeatTableServiceImpl implements SeatTableService {
    @Resource
    private SeatTableMapper mapper;

    @Override
    public int put(SeatTable table) {
        return mapper.put(table);
    }

    @Override
    public Object list(SeatTable table) {
        return mapper.selectAll();
    }

    @Override
    public Object tree(SeatTable table) {
        List<SeatTable> list = mapper.selectByMap(null);
        return tree(list, -1);
    }

    /**
     * 根节点 -1
     *
     * @param list
     * @param id
     * @return
     */
    private List<SeatTable> tree(List<SeatTable> list, int id) {
        List<SeatTable> table = new ArrayList<>();
        for (SeatTable el : list) {
            if (el.getPId().equals(id)) {
                el.setList(tree(list, el.getId()));
                table.add(el);
            }
        }
        return table;
    }
}
