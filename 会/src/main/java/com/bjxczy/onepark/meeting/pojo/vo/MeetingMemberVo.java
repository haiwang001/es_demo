package com.bjxczy.onepark.meeting.pojo.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/25 14:04
 */
@Data
public class MeetingMemberVo {
    @Excel(name = "姓名", width = 20, replace = {"_null"})
    private String name; // 会议id
    @Excel(name = "手机号", width = 20, replace = {"_null"})
    private String mobile; // 会议id
    @Excel(name = "参会次数", width = 20, replace = {"_null"})
    private Integer number; // 会议id
}
