package com.bjxczy.onepark.meeting.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.meeting.entity.SeatTable;
import com.bjxczy.onepark.meeting.service.SeatTableService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/*
 *@Author wlw
 *@Date 2023/4/26 10:59
 */
@Log4j2
@ApiResourceController
@RequestMapping("seatTable")
public class SeatTableController {
    @Resource
    private SeatTableService service;

    @PutMapping(name = "修改座次表的数据", value = "/put")
    public R<?> put(@RequestBody SeatTable table) {
        return R.success(service.put(table));
    }

    @GetMapping(name = "座次表tree", value = "/tree")
    public R<?> tree(SeatTable table) {
        return R.success(service.tree(table));
    }

    @GetMapping(name = "座次表list", value = "/list")
    public R<?> list(SeatTable table) {
        return R.success(service.list(table));
    }
}
