package com.bjxczy.onepark.meeting.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;
import com.bjxczy.onepark.meeting.pojo.vo.*;
import com.bjxczy.onepark.meeting.pojo.dto.MeetingInfoDto;
import com.bjxczy.onepark.screen.pojo.vo.MeetingLiUnderWayVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface MeetingInfoMapper extends BaseMapper<MeetingInfo> {


    /**
     * 会议时间冲突
     *
     * @param roomId
     * @param openDate
     * @param endDate
     * @param meetingId
     * @return
     */
    @Select("select  * from  tbl_meeting_info where room_id = #{roomId}" +
            "and  NOT ((#{endDate} < open_date) OR (#{openDate} > end_date)) and (use_state = 1 or use_state = 2  or use_state = 5 )" +
            "and if(#{meetingId} !='' ,meeting_id != #{meetingId},true) ")
    List<MeetingInfo> selectByOpenDate(@Param("roomId") String roomId,
                                       @Param("openDate") String openDate,
                                       @Param("endDate") String endDate,
                                       @Param("meetingId") String meetingId);

    /**
     * 参会人冲突
     *
     * @param name
     * @param mobile
     * @param openDate
     * @param endDate
     * @return
     */
    @Select("select count(*)\n" +
            "from tbl_meeting_member a,\n" +
            "     (select meeting_id\n" +
            "      from tbl_meeting_info\n" +
            "      where del_flag = 0\n" +
            "        and NOT ((#{endDate} < open_date) OR (#{openDate} > end_date))\n" +
            "        and (use_state = 1 or use_state = 2 or use_state = 3)) b\n" +
            "where name = #{name}\n" +
            "  and mobile = #{mobile}\n" +
            "  and a.meeting_id = b.meeting_id")
    int conflictPeople(@Param("name") String name, @Param("mobile") String mobile,
                       @Param("openDate") String openDate, @Param("endDate") String endDate);

    /**
     * 导出数据
     *
     * @param dto
     * @return
     */
    @Select("select a.*,\n" +
            "       b.label meetingSourceLabel,\n" +
            "       c.label roomScopeLabel,\n" +
            "       d.label useStateLabel,\n" +
            "       e.name\n" +
            "from\n" +
            "        tbl_meeting_info a,\n" +
            "        tbl_office_dict b,\n" +
            "        tbl_office_dict c,\n" +
            "        tbl_office_dict d,\n" +
            "        m_seat_table e\n" +
            "where  a.del_flag = 0\n" +
            "  and  a.meeting_source = b.value\n" +
            "  and  b.group_code='meeting_source'\n" +
            "  and  a.room_scope = c.value\n" +
            "  and  c.group_code='roomScope'\n" +
            "  and  a.use_state = d.value\n" +
            "  and  d.group_code='useState'\n" +
            "  and  a.room_type = e.id\n" +
            "  and if(#{dto.meetingSource} is not null , a.meeting_source = #{dto.meetingSource}, true)\n" +
            "  and if(#{dto.appointmentName} is not null , a.appointment_name like concat('%',#{dto.appointmentName}, '%'), true)\n" +
            "  and if(#{dto.roomScope} is not null , a.room_scope like concat('%', #{dto.roomScope}, '%'), true)\n" +
            "  and if(#{dto.useState} is not null , a.use_state = #{dto.useState}, true)\n" +
            "  and if(#{dto.openDate} !='' , a.open_date >= #{dto.openDate}, true)\n" +
            "  and if(#{dto.endDate} !=''  , a.open_date <= #{dto.endDate}, true)\n" +
            "group by a.meeting_id order by a.create_time desc")
    List<MeetingInfoExcelVo> selectAllByDto(@Param("dto") MeetingInfoDto dto);

    /**
     * 列表查询
     *
     * @param dto
     * @return
     */
    @Select("select a.*,\n" +
            "       e.name roomTypeLabel,\n" +
            "       b.label meetingSourceLabel,\n" +
            "       c.label roomScopeLabel,\n" +
            "       d.label useStateLabel\n" +
            "from tbl_meeting_info a\n" +
            "         left join tbl_office_dict b on a.meeting_source = b.value and b.group_code = 'meeting_source'\n" +
            "         left join tbl_office_dict c on a.room_scope = c.value and c.group_code = 'roomScope'\n" +
            "         left join tbl_office_dict d on a.use_state = d.value and d.group_code = 'useState'\n" +
            "         left join m_seat_table e on a.room_type = e.id\n" +
            "where a.del_flag = 0\n" +
            "  and if(#{dto.meetingSource} is not null , a.meeting_source = #{dto.meetingSource}, true)\n" +
            "  and if(#{dto.appointmentName} is not null , a.appointment_name like concat('%',#{dto.appointmentName}, '%'), true)\n" +
            "  and if(#{dto.roomScope} is not null , a.room_scope like concat('%', #{dto.roomScope}, '%'), true)\n" +
            "  and if(#{dto.useState} is not null , a.use_state = #{dto.useState}, true)\n" +
            "  and if(#{dto.openDate} !='' , a.open_date >= #{dto.openDate}, true)\n" +
            "  and if(#{dto.endDate} !=''  , a.open_date <= #{dto.endDate}, true)\n" +
            "  and if(#{dto.regionId} !=''  , region_id = #{dto.regionId}, true)\n" +
            "  group by a.meeting_id")
    List<MeetingInfoVo> selectListByDto(@Param("dto") MeetingInfoDto dto);

    /**
     * 更新会议状态
     *
     * @param dto
     * @return
     */
    @Update("update tbl_meeting_info set comment = #{dto.comment}, stars = #{dto.stars} where meeting_id = #{dto.meetingId}")
    int updateCommentById(@Param("dto") MeetingInfoDto dto);

    @Select("select *\n" +
            "from tbl_meeting_info a \n" +
            "where a.del_flag = 0  and use_state !=4\n" +
            "  and if(#{dto.roomName} is not null, a.room_name like concat('%',#{dto.roomName}, '%'), true)\n" +
            "  and if(#{dto.openDate} !='' ,  a.open_date    like concat('%',#{dto.openDate}, '%'), true)\n" +
            "group by meeting_id")
    List<MeetingTimerShaftVo> getInfoListByDto(@Param("dto") MeetingInfoDto dto);


    @Select("select *\n" +
            "from tbl_meeting_info a ,tbl_conference_room b \n" +
            "where a.del_flag = 0 and a.stars is not null  and  a.room_id =b.c_id and b.room_state =2  \n" +
            "  and if(#{dto.roomName} is not null, a.room_name like concat('%',#{dto.roomName}, '%'), true)\n" +
            "  and if(#{dto.openDate} !='' , a.open_date >= #{dto.openDate}, true)\n" +
            "  and if(#{dto.endDate} !=''  , a.open_date <= #{dto.endDate}, true)\n" +
            "  and if(#{dto.regionId} !=''  , a.region_id = #{dto.regionId}, true)\n" +
            "group by meeting_id")
    List<MeetingInfoVo> histogramByDto(@Param("dto") MeetingInfoDto dto);


    @Select("select *\n" +
            "from tbl_meeting_info a ,tbl_conference_room b\n" +
            "where a.del_flag = 0  and  a.room_id =b.c_id and b.room_state =2  \n" +
            "  and if(#{dto.roomName} is not null, a.room_name like concat('%',#{dto.roomName}, '%'), true)\n" +
            "  and if(#{dto.openDate} !='' , a.open_date >= #{dto.openDate}, true)\n" +
            "  and if(#{dto.endDate} !=''  , a.open_date <= #{dto.endDate}, true)\n" +
            "  and if(#{dto.regionId} !=''  , a.region_id = #{dto.regionId}, true)\n" +
            "group by a.meeting_id")
    List<MeetingInfoVo> pieByDto(@Param("dto") MeetingInfoDto dto);

    @Select("select *\n" +
            "from tbl_meeting_info a ,tbl_conference_room b\n" +
            "where a.del_flag = 0  and  a.room_id =b.c_id and b.room_state =2  \n" +
            "  and if(#{dto.roomName} is not null, a.room_name like concat('%',#{dto.roomName}, '%'), true)\n" +
            "  and if(#{dto.openDate} !='' , a.open_date >= #{dto.openDate}, true)\n" +
            "  and if(#{dto.endDate} !=''  , a.open_date <= #{dto.endDate}, true)\n" +
            "  and if(#{dto.regionId} !=''  , a.region_id = #{dto.regionId}, true)\n" +
            "group by a.meeting_id")
    List<MeetingInfoVo> lineChart(@Param("dto") MeetingInfoDto dto);

    @Select("select a.enterprise, a.min_time, a.room_prepay actually_paid\n" +
            "           from tbl_meeting_info a\n" +
            "           where a.del_flag = 0\n" +
            "             and if(#{dto.name} is not null , a.enterprise like concat('%', #{dto.name}, '%'), true)\n" +
            "             and if(#{dto.openDate} !='' , a.open_date >= #{dto.openDate}, true)\n" +
            "             and if(#{dto.endDate} !=''  , a.open_date <= #{dto.endDate}, true)\n" +
            "             and if(#{dto.regionId} !=''  , a.region_id = #{dto.regionId}, true)\n" +
            "           group by a.meeting_id")
    List<MeetingStatisticsVo> selectByName(@Param("dto") MeetingInfoDto dto);


    @Select("select a.meeting_id,\n" +
            "       a.serial_number,\n" +
            "       a.room_name,\n" +
            "       if(a.meeting_theme != '', a.meeting_theme, '无主题') meeting_theme,\n" +
            "       b.label useState,\n" +
            "       date_format(a.open_date, '%Y-%m-%d') openDate,\n" +
            "       if(a.housing_num is not null, '有', ' - ')         live,\n" +
            "       if(a.have_meal_num is not null, '有', ' - ')       eat\n" +
            "from tbl_meeting_info a\n" +
            "         left join tbl_office_dict b on a.use_state = b.value and b.group_code = 'useState'\n" +
            "where a.del_flag = 0\n" +
            "  and if(true, a.enterprise like concat('%', #{name}, '%'), true)\n" +
            "group by a.meeting_id")
    List<MeetingCompanyVo> selectCompanyByName(@Param("name") String name);

    @Select("select a.name, a.mobile, count(a.mobile) number\n" +
            "from (select meeting_id from tbl_meeting_info where enterprise = #{name}) b\n" +
            "         left join tbl_meeting_member a on\n" +
            "    a.meeting_id = b.meeting_id\n" +
            "where a.del_flag = 0\n" +
            "group by a.mobile")
    List<MeetingMemberVo> getMemberById(@Param("name") String name);

    @Select("select date_format(open_date, '%Y-%m-%d') node,\n" +
            "       sum(room_prepay)                 conference,\n" +
            "       sum(eat_amount)                    eat,\n" +
            "       sum(accommodation_amount)          live\n" +
            "from tbl_meeting_info\n" +
            "where del_flag = 0\n" +
            "  and enterprise =  #{name}\n" +
            "  and open_date >=  #{date}\n" +
            "group by date_format(open_date, '%Y-%m-%d')")
    List<MeetingAmountVo> selectByNameAndDate(@Param("name") String name, @Param("date") String date);

    @Select("select staff_name name, mobile, face_img url\n" +
            "from face.face_info\n" +
            "where del_flag = 0")
    List<AttendTheMeetingVo> selectFace();


    @Select("select visitor_name name, telephone mobile, visitor_photo url\n" +
            "from voucher.visitor_info\n" +
            "where del_flag = 0")
    List<AttendTheMeetingVo> selectVisitor();

    @Select("select b.name, sign_in_date\n" +
            "            from  tbl_meeting_member b\n" +
            "            where b.del_flag = 0 and b.sign_in = 1 and b.meeting_id = #{meetingId}")
    List<SignInVo> selectSignIn(@Param("meetingId") String meetingId);

    @Select("select b.name\n" +
            "            from  tbl_meeting_member b\n" +
            "            where b.del_flag = 0 and b.sign_in = 0 and b.meeting_id = #{meetingId}")
    List<String> selectFailSignIn(@Param("meetingId") String meetingId);

    @Select("select date_format(open_date, '%Y-%m-%d %H:%i') open_date\n" +
            "from tbl_meeting_info\n" +
            "where meeting_id = #{meetingId}")
    String selectDateBymeetingId(@Param("meetingId") String meetingId);

    @Select("select  label  from tbl_office_dict where  del_flag = 0 and group_code = 'have_meal_type' and value=#{haveMealType}")
    String getDictionary_have_meal_type(@Param("haveMealType") String haveMealType);

    @Select("select  label  from tbl_office_dict where  del_flag = 0 and group_code = 'housing_type' and value=#{haveMealType}")
    String getHousingType(@Param("haveMealType") Integer housingType);

    @Select("select date_format(open_date, '%Y-%m-%d') node\n" +
            "from tbl_meeting_info\n" +
            "where del_flag = 0\n" +
            "  and date_format(open_date, '%Y-%m') = #{date} \n" +
            "group by date_format(open_date, '%d')")
    List<MeetingDateNodeVo> getNode(@Param("date") String date);

    @Select("select meeting_theme,meeting_id,use_state,CONCAT(date_format(open_date, '%H:%i') , ' - ',date_format(end_date, '%H:%i')) date\n" +
            "from tbl_meeting_info\n" +
            "where del_flag = 0\n" +
            "  and date_format(open_date, '%Y-%m-%d') = date_format(#{node}, '%Y-%m-%d')\n" +
            "  and use_state != 4" +
            "  and if(#{name}!='', room_name like concat('%',#{name},'%'), true) order by open_date desc")
    List<MeetingDateVo> getNodeDate(@Param("node") String node, @Param("name") String name);


    @Select("select meeting_theme,\n" +
            "       a.room_name,\n" +
            "       a.open_date,\n" +
            "       a.appointment_name,\n" +
            "       a.appointment_mobile mobile,\n" +
            "       a.use_state,\n" +
            "       a.end_date,\n" +
            "       tcr.name roomType,\n" +
            "       a.enterprise company,\n" +
            "       a.number_of_people number\n" +
            "from tbl_meeting_info a left join m_seat_table tcr on a.room_type = tcr.id\n" +
            "where a.del_flag = 0\n" +
            "  and a.operator = #{staffName}\n" +
            "  and a.use_state != 4\n" +
            "  and if(#{useState} != '', a.use_state = #{useState}, true)" +
            "  order by open_date desc")
    List<MobileMeetingInfoVo> selectListByUseStateAndStaffName(@Param("useState") Integer useState, @Param("staffName") String staffName);

    @Select("select room_id\n" +
            "from tbl_meeting_info\n" +
            "where del_flag = 0\n" +
            "  and (use_state = 1 or use_state = 2)\n" +
            "group by room_id")
    List<String> selectSizeByUseState();

    @Select("select count(meeting_id)\n" +
            "from tbl_meeting_info\n" +
            "where del_flag = 0\n" +
            "and date_format(open_date, '%Y-%m-%d') = #{node}")
    Long getDateNode(@Param("node") String node);


    @Select("select count(meeting_id)\n" +
            "from tbl_meeting_info\n" +
            "where del_flag = 0\n" +
            "and date_format(open_date, '%Y-%m') = #{node}")
    Long getDateNodeByMonths(@Param("node") String node);

    @Select("select count(meeting_id)\n" +
            "from tbl_meeting_info\n" +
            "where del_flag = 0\n" +
            "  and use_state = 1\n" +
            "  and date_format(open_date, '%Y-%m') = #{node}\n")
    Long selectInfoByNotStarted(@Param("node") String node);

    @Select("select a.room_name, a.serial_number, a.end_date, b.label state, a.operator\n" +
            "from tbl_meeting_info a\n" +
            "         left join tbl_office_dict b on a.use_state = b.value and b.group_code = 'useState'\n" +
            "where a.del_flag = 0 \n" +
            "group by a.meeting_id")
    IPage<MeetingLiUnderWayVo> selectByUseState(Page<MeetingLiUnderWayVo> tPage, @Param("state") int state);
}
