package com.bjxczy.onepark.meeting.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/*
 *@ClassName MeetingInfo
 *@Author 温良伟
 *@Date 2023/2/20 9:05
 *@Version 1.0
 * 会议信息
 */
@Data
@TableName("tbl_meeting_info")
public class MeetingInfo {
    public MeetingInfo() {
    }

    public MeetingInfo(String meetingId,Integer useState) {
        this.meetingId = meetingId;
        this.useState = useState;
    }

    @TableId
    private String meetingId;//会议id uuid
    private Integer meetingSource;//会议来源 // 1 系统 2 EO  3 移动端
    private String serialNumber;//序号

    // 第一部分  预订 信息
    private String appointmentName;//会议预约人
    private String appointmentMobile;//会议预约人手机号
    private Date openDate;//会议开始时间
    private Date endDate;//会议结束时间
    private Integer minTime;//会议时长

    private String staffId; // 员工id  移动端 staff_id
    private String staffCode; // 员工编码 移动端 staff_code
    private String enterprise;//所属企业

    private Integer numberOfPeople;//会议人数
    private String roomId;//会议室id
    private String roomName;//会议室名称
    private String addressLabel;//会议室位置        address_label                         v 1.3.7
    private String meetingTheme;//会议主题
    private Boolean roomScope;//会议使用者分类 默认  0 内部会议 1外部人员会议
    private String roomType;//会议室分类

    private Double prepay;//预付金额
    private Double roomPrepay;//本次会议总金额 room_prepay
    private Double actuallyPaid;//实际金额
    private Boolean isSignIn;//是否签到
    private Boolean isSweep;//是否清扫 is_sweep  默认打开 true  关闭 false
    private String remindTime;//提醒时间
    private String ruleAccounting;//计费规则  rule_accounting

    @TableField(exist = false) // 不是数据库字段：
    private List<MeetingInfoGoods> goodsList;//会议物品列表

    //第二部分  参会人员
    @TableField(exist = false) // 不是数据库字段：
    private List<MeetingMember> replenishNotifierList;//参会人员
    private String pushMethod;//推送方式   个人代办 1 个人短信2   多选   1
    private String remark;//备注信息

    //第三部分  住宿宿需求
    private Integer stayArea;// 住宿的园区id stay_area
    private String stayAreaLabel;// 住宿的园区展示 stay_area_label
    private String hotelRemark;  // 住宿备注     hotel_remark                       V1.3.4
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date occupancyDate;// 入住时间 occupancy_date
    private Double accommodationAmount;//住宿金额  accommodation_amount
    @TableField(exist = false) // 非本表数据
    private List<Accommodation> accommodationList;//住宿信息列表                V1.3.4


    //第四部分  用餐
    @TableField(exist = false) // 非本表数据
    private HaveMealTime haveMealTime;// 用餐时间 have_meal_time                V1.3.4
    private String haveMealRemark;// 用餐备注  have_meal_remark                 V1.3.4
    private String haveMealLocation;// 用餐地点  have_meal_location             V1.3.4
    private String haveMealType;// 用餐方式  have_meal_type                     V1.3.4
    private Integer haveMealNum; // 预计就餐人的数量 have_meal_num
    private String dailyMeal;  //用于存储的就餐信息 daily_meal
    @TableField(exist = false) // 不是数据库字段：
    private HashMap<String, Boolean> dailyMealMap = new HashMap<>(3);     // 1 TRUE 2TRUE 3 TRUE
    private Double eatAmount;//吃饭金额  eat_amount

    /*    第5部分  评价和公共属性 */
    private Integer useState;//使用状态  1 未开始 2进行中  3已结束  4已取消    v1.3.8
    private Integer stars;// 评价星数
    private String comment;// 评价
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;//创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;//修改时间
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;//删除标记
    private String operator;//操作人
    private String regionId; // 区域id region_id
    private Boolean isWorkOrder;//工单关联
    private Boolean isAccommodation;//住宿需求状态
    private String workOrderType;//工单类型  work_order_type
    private String workOrderId;//工单id   work_order_id
    private String workRemark;//工单备注  work_remark
    private Boolean stayState;// 吃状态  stay_state
    private Boolean amountState;// 住房状态 amount_state
}
