package com.bjxczy.onepark.meeting.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/*
 *@Author wlw
 *@Date 2023/5/19 11:58 固定工单配置
 */
@Component
@Data
@ConfigurationProperties(prefix = "work")
public class WorkOrderConfig {
    private String hotelId; // 酒店工单
    private String stayId; //  餐饮工单
    private String meetingId;// 会议工单
    private String sweepId;// 清扫工单
}
