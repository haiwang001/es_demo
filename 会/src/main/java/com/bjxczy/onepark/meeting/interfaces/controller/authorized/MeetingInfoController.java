package com.bjxczy.onepark.meeting.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;
import com.bjxczy.onepark.meeting.pojo.dto.MeetingInfoDto;
import com.bjxczy.onepark.meeting.pojo.dto.NoticeDto;
import com.bjxczy.onepark.meeting.service.MeetingInfoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Log4j2
@ApiResourceController
@RequestMapping("Meeting")
public class MeetingInfoController {
    @Resource
    private MeetingInfoService service;

    /**
     * v1.3.4 释放提前结束或者结束会议的时间
     */
    @GetMapping(name = "会议时间冲突", value = "/conflictDate")
    private R<?> conflictDate(String roomId, String openDate, String endDate, String meetingId) throws Exception {
        return R.success(service.conflictDate(roomId, openDate, endDate, meetingId));
    }

    /**
     * v1.3.4 校验人员是否存其他未结束的会议中  Meeting/conflictPeople
     */
    @GetMapping(name = "会议人员冲突", value = "/conflictPeople")
    private R<?> conflictPeople(String name, String mobile, String openDate, String endDate) throws Exception {
        return R.success(service.conflictPeople(name, mobile, openDate, endDate));
    }

    @PostMapping(name = "新增会议", value = "/add")
    private R<?> addMeetingInfo(@RequestBody MeetingInfo info,
                                @LoginUser(isFull = true) UserInformation user) throws Exception {
        info.setOperator(user.getStaffName());
        return R.success(service.addMeetingInfo(info, user));
    }

    @PostMapping(value = "/import", name = "人员补充")
    public R<?> uploadingExcel(@RequestParam("file") MultipartFile file,
                               @RequestParam("openDate") String openDate,
                               @RequestParam("endDate") String endDate) throws Exception {
        System.err.println(SysConst.stamp);
        return R.success(service.importUploadingExcel(file, openDate, endDate));
    }

    /**
     * 会议人员变更 下发未完成
     *
     * @param info
     * @param user
     * @return
     * @throws Exception
     */
    @PutMapping(value = "/update", name = "会议信息编辑")
    public R<?> update(@RequestBody MeetingInfo info, @LoginUser(isFull = true) UserInformation user) throws Exception {
        info.setOperator(user.getStaffName());
        return R.success(service.update(info));
    }

    /**
     * 前端 未开始1    只能点击取消 传入状态4
     * 已经开始2  只能点击提前结束 传入3
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @PutMapping(value = "/updateState", name = "编辑状态")
    public R<?> update(@RequestBody MeetingInfoDto dto) throws Exception {
        return R.success(service.updateState(dto));
    }

    @GetMapping(value = "/list", name = "会议列表")
    public R<?> queryList(MeetingInfoDto dto) throws Exception {
        return R.success(service.queryList(dto));
    }

    /**
     * 移动端查询我参与的会议
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/mobileList", name = "移动端-我的会议")
    public R<?> mobileList(MeetingInfoDto dto,
                           @LoginUser(isFull = true) UserInformation user) throws Exception {
        return R.success(service.mobileList(dto.getUseState(),user.getStaffName()));
    }

    @PostMapping(value = "/notice", name = "发送通知")
    public void notice(@RequestBody NoticeDto dto) throws Exception {
        service.notice(dto);
    }
    /**
     * 设置评价
     *
     * @param dto
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/comment", name = "评价本次会议")
    public R<?> setComment(@RequestBody MeetingInfoDto dto) throws Exception {
        return R.success(service.setComment(dto));
    }

    @GetMapping(value = "downloadData", name = "下载会议信息")
    public void downloadData(HttpServletRequest request,
                             HttpServletResponse response,
                             MeetingInfoDto dto) throws Exception {
        service.downloadData(response, dto);
    }


    @GetMapping(value = "signIn", name = "查询签到")
    public R<?> signIn(String meetingId) {
        return R.success(service.getSignInById(meetingId));
    }


    @GetMapping(value = "/downloadSignIn/{id}", name = "人员签到表")
    public void downloadSignIn(HttpServletResponse response, @PathVariable("id") String id) {
        service.downloadSignIn(response, id);
    }


    @PostMapping(value = "/importSignIn/{id}", name = "导入签到")
    public R<?> importSignIn(@RequestParam("file") MultipartFile file,
                             @PathVariable("id") String id) throws Exception {
        return R.success(service.importSignIn(file, id));
    }


    @GetMapping(value = "/calendar", name = "日历概览")
    public R<?> calendar(String date, String name) {
        System.err.println(SysConst.stamp);
        return R.success(service.calendar(date, name));
    }

}
