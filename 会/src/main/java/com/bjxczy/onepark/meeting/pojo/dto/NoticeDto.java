package com.bjxczy.onepark.meeting.pojo.dto;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/5/18 14:53
 */
@Data
public class NoticeDto {
    private String meetingId;
    private List<Integer> ids;
}
