package com.bjxczy.onepark.meeting.pojo.vo;


import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/*
 *@Author wlw
 *@Date 2023/5/17 17:10
 */
@Data
public class MultipartFileVo {
    private Integer index;
    private MultipartFile multipartFile;
    private String msg;

    public MultipartFileVo(Integer index, MultipartFile multipartFile, String msg) {
        this.index = index;
        this.multipartFile = multipartFile;
        this.msg = msg;
    }
}
