package com.bjxczy.onepark.meeting.pojo.dto;


import com.bjxczy.onepark.publicmodule.pojo.dto.BasePageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class MeetingInfoDto extends BasePageDTO {
    private Integer staffId; // 员工id 移动端
    private String staffCode; // 员工编码 移动端 未确定
    private Integer useState;//使用状态  1 未开始 2进行中  3已结束  4已经取消
    private String meetingId;//会议id uuid
    private Integer meetingSource;//会议来源 // 1 系统 2 EO  3 移动端
    private String appointmentName;//会议预约人
    private Boolean roomScope;//会议使用者分类 默认2 内部会议 1外部人员会议

    private Integer stars;// 评价星数
    private String comment;// 评价
    private String roomName;//会议室名称

    private Integer timIng;//周 月 季度 年
    private Integer regionId; // 区域id region_id
    private String openDate;//会议开始时间
    private String endDate;//会议结束时间
    private String name;

    private Boolean openDateSort;//开始时间排序         true 开启开始时间排序   false 默认不排序
    private Boolean endDateSort ;//结束时间排序         true 开启结束时间排序   false 默认不排序
    private Boolean createDateSort ;//创建时间排序      true 开启创建时间排序   false 默认不排序
    private Boolean isDesc ;//  上下箭头               true 从大到小  false 从小到大

}
