package com.bjxczy.onepark.meeting.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/26 9:50
 */
@Data
public class AttendTheMeetingVo {
    private String name;
    private String mobile;
    private String url;
}
