package com.bjxczy.onepark.meeting.pojo.vo;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/5/22 12:09
 */
@Data
public class MeetingDateNodeVo {
    private String node;
    private List<MeetingDateVo> data;
}
