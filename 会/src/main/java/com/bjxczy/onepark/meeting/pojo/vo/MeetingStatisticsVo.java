package com.bjxczy.onepark.meeting.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/24 9:39
 *
 * MeetingCompanyVo
 *
 */
@Data
public class MeetingStatisticsVo {
    private String enterprise;//所属企业
    private Integer count;//预约次数
    private Integer minTime;//会议时长
    private Double actuallyPaid;//实际金额
    private String totalTime;//总时长展示
    /**
     *
     * @param enterprise 会议名称
     * @param count 总次数
     * @param totalTime 总时长
     * @param actuallyPaid 金额
     */
    public MeetingStatisticsVo(String enterprise, Integer count, String totalTime, Double actuallyPaid) {
        this.enterprise = enterprise;
        this.count = count;
        this.totalTime = totalTime;
        this.actuallyPaid = actuallyPaid;
    }

    public MeetingStatisticsVo() {
    }
}
