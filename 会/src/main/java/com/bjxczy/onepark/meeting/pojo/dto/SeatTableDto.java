package com.bjxczy.onepark.meeting.pojo.dto;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/26 13:28
 */
@Data
public class SeatTableDto {
    private Integer id;
    private Object data;
}
