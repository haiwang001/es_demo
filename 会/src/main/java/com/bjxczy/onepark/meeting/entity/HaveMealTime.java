package com.bjxczy.onepark.meeting.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/5/16 11:56
 * 用餐时间
 */
@Data
@TableName("m_have_meal_time")
public class HaveMealTime {
    @TableId
    private String meetingId;    //绑定会议 meeting_id

    private String morningOne;// 早上开始 morning_one
    private String morningTwo;// 早上结束 morning_two

    private String noonOne;// 中午开始 noon_one
    private String noonTwo;// 中午结束 noon_two

    private String eveningOne;// 晚上开始  evening_one
    private String eveningTwo;// 晚上结束  evening_two

}
