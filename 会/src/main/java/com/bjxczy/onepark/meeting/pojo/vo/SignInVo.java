package com.bjxczy.onepark.meeting.pojo.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;


/*
 *@Author wlw
 *@Date 2023/5/5 9:53 查询签到的返回
 */
@Data
public class SignInVo {
    @Excel(name = "参会人姓名", width = 18, replace = {"_null"})
    private String name;  // 参会人名
    @Excel(name = "参会人手机", width = 18, replace = {"_null"})
    private String mobile;//sign_in_date 签到时间
    @Excel(name = "签到结果 (签到填写:是，未签到不填写)", width = 40, replace = {"_null"})
    private String result;//签到结果

    private String signInDate;//sign_in_date 签到时间
    private String msg;// 签到失败原因
    private String remind;// excel提醒

    public SignInVo() {
    }

    public SignInVo(String name, String mobile, String signInDate, String msg) {
        this.name = name;
        this.mobile = mobile;
        this.signInDate = signInDate;
        this.msg = msg;
    }
}
