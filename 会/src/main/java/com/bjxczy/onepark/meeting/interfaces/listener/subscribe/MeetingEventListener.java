package com.bjxczy.onepark.meeting.interfaces.listener.subscribe;


import com.bjxczy.core.rabbitmq.supports.WorkflowBaseListener;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.workflow.AddEoMettingInfo;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;
import com.bjxczy.onepark.meeting.service.MeetingInfoService;
import com.bjxczy.onepark.publicmodule.interfaces.feign.client.QueryStaffServer;
import com.bjxczy.onepark.room.entity.ConferenceRoom;
import com.bjxczy.onepark.room.mapper.ConferenceRoomMapper;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/*
 *@Author wlw
 *@Date 2023/6/7 11:04
 */
@Component
@Log4j2
public class MeetingEventListener extends WorkflowBaseListener {

    @Resource
    private MeetingInfoService service;

    @Resource
    private ConferenceRoomMapper  RoomMapper;
    @Resource
    private QueryStaffServer mapper;
    /**
     * 增加eo单会议
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = EOORDER_ADD_METTING_INFO_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void addMettingInfo(AddEoMettingInfo Info, Channel channel, Message message) {
        log.info("eo单会议 -- {}", Info);
        MeetingInfo info = new MeetingInfo();
        info.setRuleAccounting("按小时");
        info.setMeetingTheme(Info.getTopic());
        info.setNumberOfPeople(Info.getPersonCount());
        info.setEnterprise(Info.getDept());
        info.setMeetingSource(2);// eo单
        info.setRoomId(Info.getMetRoomId());
        info.setRoomScope(false);
        ConferenceRoom room = RoomMapper.selectById(Info.getMetRoomId());
        info.setRoomName(room.getRoomName());
        info.setRoomType(room.getRoomType());
        StaffInfoFine staff = mapper.getStaffById(Info.getStaffId());
        info.setAppointmentName(staff.getFldName());
        info.setAppointmentMobile(staff.getMobile());
        info.setOperator(staff.getFldName());
        try {
            info.setOpenDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(Info.getStartTime()));
            info.setEndDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(Info.getEndTime()));
        } catch (ParseException e) {
            e.printStackTrace();
            log.error("eo 单时间转换错误");
        }
        UserInformation information = new UserInformation();
        try {
            service.addMeetingInfo(info,information);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
