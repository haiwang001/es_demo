package com.bjxczy.onepark.meeting.interfaces.task;


import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.utils.date.DateUtil;
import com.bjxczy.onepark.hikmodule.api.SignInApi;
import com.bjxczy.onepark.meeting.entity.MeetingInfo;
import com.bjxczy.onepark.meeting.entity.MeetingMember;
import com.bjxczy.onepark.meeting.service.MeetingInfoService;
import com.bjxczy.onepark.publicmodule.service.MsgTemplateService;
import com.bjxczy.onepark.publicmodule.service.WorkOrderTemplateService;
import com.bjxczy.onepark.room.mapper.ConferenceRoomMapper;
import com.bjxczy.onepark.meeting.mapper.MeetingInfoMapper;
import com.bjxczy.onepark.meeting.mapper.MeetingMemberMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/*
 *@ClassName Timer
 *@Author 温良伟
 *@Date 2023/2/20 17:32
 *@Version 1.0
 */
@Log4j2
@Configuration
@EnableScheduling    //会议进行定时器
public class Timer {
    @Resource
    private ConferenceRoomMapper RoomMapper; // 联动待清扫
    @Resource
    private MeetingInfoService service;
    @Resource
    private MeetingInfoMapper mapper;
    @Resource
    private MeetingMemberMapper MemberMapper;
    @Resource
    private WorkOrderTemplateService OrderService;
    @Resource
    private MsgTemplateService msgTemplateService;

    /**
     * 每2分钟查询1次
     */
    @Scheduled(fixedDelay = 2000 * 60)
    public void sayWord() {
        // 会议状态维护
        autoState();
    }

    /**
     * 维护会议状态
     * 未开启
     * 未开启 可查看签到
     * 会议结束
     */
    private void autoState() {
        List<MeetingInfo> list = mapper.selectByMap(new HashMap<>(0));
        list.forEach(el -> {
            if (el.getUseState() == 1 || el.getUseState() == 2) {
                autoWarn(el); //发送消息
                autoUpdateDate(el); // 修改会议状态
            }
        });
    }

    private void autoUpdateDate(MeetingInfo el) {
        long time = DateUtil.date().getTime();
        long time1 = el.getOpenDate().getTime();
        long time2 = el.getEndDate().getTime();
        if (time >= time1 && time < time2) { //当前时间大于 开始时间 小于结束时间 表示会议开始 会议开始区间
            mapper.updateById(getMeetingInfo(el.getMeetingId(), 2));
        }
        if (time >= time2) { // 会议结束
            // 清除已经过期的会议人员与照片
            MemberMapper.selectByMeetingId(el.getMeetingId()).forEach(this::clearInfo);
            // 会议结束状态 修改代办信息
            msgTemplateService.updateCommissionItem(1, "已结束!");
            // 会议状态结束
            mapper.updateById(getMeetingInfo(el.getMeetingId(), 3));
            if (el.getIsSweep()) { // 清扫工单触发
                log.info("触发清扫工单");
                OrderService.distributeSweep(el);      // 触发清扫工单
            }
            if (el.getRoomScope()) { // 外部会议
                log.info("外部会议-----------");
                // 会议结算代办
            }
        }
    }

    /**
     * 根据提醒时间 发送提醒
     *
     * @param el
     */
    private void autoWarn(MeetingInfo el) {
        if (el.getRemindTime() != null && !"0".equals(el.getRemindTime()) && !"".equals(el.getRemindTime())) { //进入的数据是开启提醒的
            long time = DateUtil.date().getTime();
            long time1 = DateUtil.setDateTime(el.getOpenDate(), SysConst.NEGATIVE_INTEGER * Long.parseLong(el.getRemindTime())).getTime();
            long time2 = el.getOpenDate().getTime();
            if (time >= time2 && time < time1) { // 在发送时间区间
                //  所有的参会人
                List<MeetingMember> memberList = MemberMapper.selectByMeetingId(el.getMeetingId());
                for (MeetingMember member : memberList) {
                    Integer inside = MemberMapper.inside(member.getName(), member.getMobile());
                    if (inside != null && member.getAutoNotice() == 0) {// 表示内部员工  未自动发送
                        msgTemplateService.participateNotice(el, inside, "已创建成功。");
                        member.setAutoNotice(1);// 变更状态
                        MemberMapper.updateById(member);
                    }
                    // 发送短息未完成
                }
            }
        }
    }


    private MeetingInfo getMeetingInfo(String id, Integer UseState) {
        return new MeetingInfo(id, UseState);
    }


    public void clearInfo(MeetingMember el) {
        if (el.getFaceUrl() != null && el.getDeviceIndexCode() != null && el.getDeleteRecord() == 0) {
            // 清除人员
            SignInApi.clearPeople(el.getDeviceIndexCode(), el.getMobile());
            // 清除照片
            SignInApi.clearFace(el.getFaceUrl(), el.getDeviceIndexCode(), el.getMobile(), el.getName());
            //  修改为已删除状态 避免重复
            MemberMapper.updateDeleteRecordById(el.getId());
        }
    }
}
