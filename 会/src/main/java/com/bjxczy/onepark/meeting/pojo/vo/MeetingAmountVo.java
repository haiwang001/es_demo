package com.bjxczy.onepark.meeting.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/25 17:31
 */
@Data
public class MeetingAmountVo {
    private String node; //时间节点
    private double conference; // 会议消费
    private double eat; // 吃饭消费
    private double live; // 住宿消费
}
