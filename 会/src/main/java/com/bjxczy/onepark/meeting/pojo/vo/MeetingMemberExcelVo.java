package com.bjxczy.onepark.meeting.pojo.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/*
 *@ClassName MeetingMemberExcelVo
 *@Author 温良伟
 *@Date 2023/2/20 16:38
 *@Version 1.0
 */
@Data
public class MeetingMemberExcelVo {
    @Excel(name = "名称", width = 18, replace = {"_null"})
    private String name;
    @Excel(name = "手机号", width = 18, replace = {"_null"})
    private String mobile;
    @Excel(name = "照片", width = 18, replace = {"_null"})
    private String imgUrl;
    @Excel(name = "人员分类", width = 18, replace = {"_null"})
    private String type;
    @Excel(name = "座位编号", width = 18, replace = {"_null"})
    private String code;
    @Excel(name = "注意!!! 1、姓名、手机号、照片为必填项  2、照片从上往下依次排列即可 不要插入单元格, 照片应该小于200KB!  3、人员分类不填 默认为外部", width = 40, replace = {"_null"})
    private String msg;
}
