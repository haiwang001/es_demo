package com.bjxczy.onepark.meeting.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/5/16 11:34 参会人 住房需求
 */
@Data
@TableName("m_accommodation")
public class Accommodation {
    @TableId
    private String meetingId;    //绑定会议 meeting_id
    private Integer housingNum;  // 房屋数量 housing_num
    private Integer housingType; // 房屋类型  1 标间 2 大床房 3 商务房 housing_type
}
