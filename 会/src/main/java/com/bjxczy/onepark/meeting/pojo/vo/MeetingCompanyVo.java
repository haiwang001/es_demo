package com.bjxczy.onepark.meeting.pojo.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;


/*
 *@Author wlw
 *@Date 2023/4/24 11:49
 */
@Data
public class MeetingCompanyVo {
    private String meetingId;//会议id uuid
    @Excel(name = "会议编号", width = 20, replace = {"_null"})
    private String serialNumber;//序号
    @Excel(name = "会议室名称", width = 20, replace = {"_null"})
    private String roomName;//会议室名称
    @Excel(name = "会议主题", width = 20, replace = {"_null"})
    private String meetingTheme;//会议主题
    @Excel(name = "会议状态", width = 20, replace = {"_null"})
    private String useState;//使用状态  1 未开始 进行中  3已结束 4已取消
    @Excel(name = "会议时间", width = 20, replace = {"_null"})
    private String openDate;//会议开始时间
    @Excel(name = "餐饮需求", width = 20, replace = {"_null"})
    private String live;//住需求
    @Excel(name = "住宿需求", width = 20, replace = {"_null"})
    private String eat; //吃需求
}
