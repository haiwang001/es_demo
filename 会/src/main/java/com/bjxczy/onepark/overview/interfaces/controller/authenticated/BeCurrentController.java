package com.bjxczy.onepark.overview.interfaces.controller.authenticated;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.common.utils.poi.ExcelUtils;
import com.bjxczy.onepark.meeting.pojo.dto.MeetingInfoDto;
import com.bjxczy.onepark.meeting.pojo.vo.MeetingMemberExcelVo;
import com.bjxczy.onepark.room.service.ConferenceRoomService;
import com.bjxczy.onepark.publicmodule.interfaces.feign.client.FileServiceFaceFeign;
import com.bjxczy.onepark.meeting.service.MeetingInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;



@ApiResourceController
@RequestMapping("base")
public class BeCurrentController {

    @Resource
    private FileServiceFaceFeign FileService;
    @Resource
    private ConferenceRoomService service;
    @Resource
    private MeetingInfoService InfoService;

    /**
     * 文件上传
     *
     * @param file 参数文件
     * @return 返回文件存储地址
     */
    @PostMapping(value = "/upload", name = "文件上传")
    public R<?> qualityJudge(@RequestParam MultipartFile file) {
        return R.success(payload -> payload.set("url", FileService.fileUpload(file)));
    }

    /**
     * 查询所有正在使用的会议室 下拉框
     */
    @GetMapping(value = "/roomList", name = "启用的会议室")
    public R<?> getRoom() {
        return R.success(service.getRoom());
    }

    /**
     * 下载会议人员模板
     */
    @GetMapping(value = "/excelModel", name = "会议人员模板")
    public void getExcelModel(HttpServletResponse response) throws Exception {
        ExcelUtils.export(response,
                SysConst.CREW_FILE_NAME,
                SysConst.CREW_FILE_NAME,
                new ArrayList<>(0), MeetingMemberExcelVo.class);
    }

    @GetMapping(value = "/InfoList", name = "会议室热力图")
    public R<?> getInfoList(MeetingInfoDto dto) {
        return R.success(InfoService.getInfoList(dto));
    }


    @GetMapping(value = "/lineChart", name = "会议室折线图")
    public R<?> lineChart(MeetingInfoDto dto) {
        return R.success(InfoService.lineChart(dto));
    }

    /**
     * 会议室评论的次数
     *
     * @param dto
     * @return
     */
    @GetMapping(value = "/histogram", name = "会议室柱图")
    public R<?> histogram(MeetingInfoDto dto) {
        return R.success(InfoService.histogram(dto));
    }

    @GetMapping(value = "/pie", name = "会议室饼图")
    public R<?> pie(MeetingInfoDto dto) {
        return R.success(InfoService.pie(dto));
    }

    /**
     * @param dto 公司名
     * @return 列表数据
     */
    @GetMapping(value = "/statistics", name = "公司开会统计")
    public R<?> statistics(MeetingInfoDto dto) {
        return R.success(InfoService.statistics(dto));
    }

    /**
     * @param name 公司名
     * @return 图表统计
     */
    @GetMapping(value = "/companyView", name = "某公司详情")
    public R<?> look(String name) {
        return R.success(InfoService.look(name));
    }

    /**
     * @param type face 访客 visitor 园区通行人脸
     * @return 照片
     */
    @GetMapping(value = "/attendTheMeeting", name = "查询有人脸信息")
    public R<?> getInfo(String type) {
        return R.success(InfoService.getInfo(type));
    }

    /**
     * @param type string 1 = 公司维度 2 员工维度
     */
    @GetMapping(value = "/downloadExcel", name = "导出信息")
    public void downloadExcel(String type, String name, HttpServletResponse response) {
        InfoService.downloadExcel(type, name, response);
    }
}

