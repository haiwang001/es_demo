package com.bjxczy.onepark.Inandoutmodule.service.impl;

import cn.hutool.core.date.DateTime;
import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.Inandoutmodule.entity.DoorStaffRecord;
import com.bjxczy.onepark.Inandoutmodule.pojo.vo.DateNodeVo;
import com.bjxczy.onepark.Inandoutmodule.service.OverViewService;
import com.bjxczy.onepark.common.feign.client.BmpDoorDeviceFeignClient;
import com.bjxczy.onepark.common.feign.client.BmpQueryStaffServer;
import com.bjxczy.onepark.common.feign.client.BmpVisitorFeignClient;
import com.bjxczy.onepark.common.model.organization.QueryStaffParams;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.visitor.VisitorDtoInformation;
import com.bjxczy.onepark.common.service.TemplateSqlService;
import com.bjxczy.onepark.common.utils.ConvertUtils;
import com.bjxczy.onepark.common.utils.DateUtil;
import com.bjxczy.onepark.config.ParkConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Date: 2023/7/6 0006 09:44
 * @Version 1.0.0
 */
@Service
@Slf4j
public class OverViewServiceImpl implements OverViewService {

    @Autowired
    private TemplateSqlService templateSqlService;
    @Resource
    private ParkConfiguration park;
    @Resource
    private BmpDoorDeviceFeignClient doorDeviceFeignClient;
    @Resource
    private BmpQueryStaffServer StaffServer;
    @Resource
    private BmpVisitorFeignClient VisitorFeignClient;


    @Override
    public Object toDayFlow(String id) {
        String today = DateTime.now().toString("yyyy-MM-dd");
        String startDate = today + " 00:00:00";
        String endDate = today + " 23:59:59";
        long startTime = DateTime.of(startDate, "yyyy-MM-dd HH:mm:ss").getTime();
        long endTime = DateTime.of(endDate, "yyyy-MM-dd HH:mm:ss").getTime();
        List<DoorStaffRecord> all = getRecordByAreaIdAndTimeSlot(id, startTime, endTime);
        ArrayList<DoorStaffRecord> list = new ArrayList<>();
        all.forEach(list::add);
        // 进入方向的设备查询
        return DataReturnHour(list);
    }

    @Override
    public Object accumulativeFlow(String id) {
        String startDay = getyyyyMMddDate(new Date(), -6);
        String endDay = DateTime.now().toString("yyyy-MM-dd");
        String startDate = startDay + " 00:00:00";
        String endDate = endDay + " 23:59:59";
        long startTime = DateTime.of(startDate, "yyyy-MM-dd HH:mm:ss").getTime();
        long endTime = DateTime.of(endDate, "yyyy-MM-dd HH:mm:ss").getTime();
        List<DoorStaffRecord> all = getRecordByAreaIdAndTimeSlot(id, startTime, endTime);
        ArrayList<DoorStaffRecord> list = new ArrayList<>();
        all.forEach(list::add);
        return DataReturnDay(list);
    }

    @Override
    public Object numberOfPeople(String id) {
        HashMap<String, Object> returnMap = new HashMap<>();
        ArrayList<DoorStaffRecord> recordList = new ArrayList<>();
        String today = DateTime.now().toString("yyyy-MM-dd");
        String startDate = today + " 00:00:00";
        String endDate = today + " 23:59:59";
        long startTime = DateTime.of(startDate, "yyyy-MM-dd HH:mm:ss").getTime();
        long endTime = DateTime.of(endDate, "yyyy-MM-dd HH:mm:ss").getTime();
        List<DoorStaffRecord> all = getRecordByAreaIdAndTimeSlot(id, startTime, endTime);
        recordList.addAll(all);
        //最新记录时间
        if (recordList.size() > 0) {
            String LatestTime = DateTime.of(recordList.get(0).getOpenTime()).toString("yyyy-MM-dd HH:mm:ss");
            returnMap.put("datetime", LatestTime);
        } else {
            returnMap.put("datetime", null);
        }
        // 通过 编号分组去重  识别不同的人数
        Map<String, List<DoorStaffRecord>> map = recordList.stream().collect(Collectors.groupingBy(DoorStaffRecord::getMobile));
        returnMap.put("discernCount", map.size());
        Map<String, List<DoorStaffRecord>> collect = recordList.stream().collect(Collectors.groupingBy(DoorStaffRecord::getComeFrom));
        if (collect.get("1") != null){ // 员工不是null 去重
            Map<String, List<DoorStaffRecord>> collect1 = collect.get("1").stream().collect(Collectors.groupingBy(DoorStaffRecord::getMobile));
            returnMap.put("interiorCount", collect1.size());
        }else{
            returnMap.put("interiorCount",0);
        }
        // 访客数
        if (collect.get("2") != null){ // 访客不是null 去重
            Map<String, List<DoorStaffRecord>> collect1 = collect.get("2").stream().collect(Collectors.groupingBy(DoorStaffRecord::getMobile));
            returnMap.put("visitorCount", collect1.size());
        }else{
            returnMap.put("visitorCount",0);
        }
        // 临时
        Map<String, List<DoorStaffRecord>> direction = recordList.stream().collect(Collectors.groupingBy(DoorStaffRecord::getDirection));
        int onlineNumber = 0;// 在园人数
        try {
            List<DoorStaffRecord> enter = direction.get("进");
            List<DoorStaffRecord> go = direction.get("出");
            onlineNumber = enter.size() - go.size();
            if (onlineNumber <= 0) {
                returnMap.put("onlineNumber", park.getOnlineNumber());// 在园人数
            } else {
                log.info("在园人数0  " + onlineNumber);
                returnMap.put("onlineNumber", onlineNumber + park.getOnlineNumber());// 在园人数
            }
        } catch (Exception e) {
            log.error("在园人数错误  " + onlineNumber);
            returnMap.put("onlineNumber", park.getOnlineNumber());// 在园人数
        }
        returnMap.put("temporaryVisitorCount", 0);
        returnMap.put("residentVisitorCount", 0);
        return returnMap;
    }

    /**
     * @return
     * @Author qinyuan
     * @Description 根据区域id和时间段查询记录
     * @Date 9:51 2023/7/6 0006
     * @Param
     **/
    private List<DoorStaffRecord> getRecordByAreaIdAndTimeSlot(String areaId, Long startDate, Long endDate) {
        List<DoorStaffRecord> records = new ArrayList<>();
        Map<String, Object> params = new HashMap<>();
        params.put("areaId", areaId);
        params.put("startDate", startDate);
        params.put("endDate", endDate);
        List<Map<String, Object>> list = templateSqlService.list("doorStaff/getRecordByAreaIdAndTimeSlot", params);
        //类型转换
        if (CollectionUtils.isNotEmpty(list)) {
            records = list.stream().map(x -> {
                DoorStaffRecord doorStaffRecord = JSON.parseObject(JSON.toJSONString(x), DoorStaffRecord.class);
                ConvertUtils.placeholderToNullString(doorStaffRecord);
                if (StringUtils.isEmpty(doorStaffRecord.getOpenTimeText()) && doorStaffRecord.getOpenTime() != null && doorStaffRecord.getOpenTime() != 0) {
                    doorStaffRecord.setOpenTimeText(DateTime.of(doorStaffRecord.getOpenTime()).toString("yyyy-MM-dd HH:mm:ss"));
                }
                doorStaffRecord.setOpenTimeDay(DateTime.of(doorStaffRecord.getOpenTime()).toString("yyyy-MM-dd"));
                return doorStaffRecord;
            }).collect(Collectors.toList());
        }
        return records;
    }

    private Integer searchEmployee(Set<String> keySet, List<String> num) {
        Integer number = 0;
        for (String s : keySet) {
            if (num.contains(s)) {
                number++;
            }
        }
        return number;
    }

    private List<VisitorDtoInformation> searchVisitor(Set<String> keySet, List<VisitorDtoInformation> visitorAll) {
        List<VisitorDtoInformation> VisitorVo = new ArrayList<>();
        for (String s : keySet) {
            for (VisitorDtoInformation visitorVo : visitorAll) {
                if (visitorVo.getId().equals(s)) { // 访客id相等
                    VisitorVo.add(visitorVo);
                }
            }
        }
        return VisitorVo;
    }

    private HashMap<String, Object> DataReturnHour(List<DoorStaffRecord> list) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("count", list.size());
        // 进入方向的设备查询
        List<String> enter = doorDeviceFeignClient.getDeviceCodeInformation("1");
        ArrayList<DoorStaffRecord> enterList = new ArrayList<>();
        enter.forEach(El -> {
            list.forEach(el -> {
                if (El.equals(el.getDeviceCode())) {
                    enterList.add(el);
                }
            });
        });
        ArrayList<DateNodeVo> dateNodeVos1 = DateUtil.node24();//展开所有节点 用于对比
        ArrayList<DateNodeVo> dateNodeVos2 = DateUtil.node24();//展开所有节点 用于对比
        if (list.size() != 0) {
            // 开门方向的记录
            Map<String, List<DoorStaffRecord>> map1 = enterList.stream().collect(Collectors.groupingBy(DoorStaffRecord::getOpenTimeHour));
            dateNodeVos1.forEach(el -> {
                map1.forEach((k, v) -> {
                    if (k != null && el.getX().split(":")[0].equals(k.split(" ")[1])) {
                        el.setY((long) v.size());
                    }
                });
            });
            list.removeAll(enterList);
            // 反方向
            Map<String, List<DoorStaffRecord>> map2 = list.stream().collect(Collectors.groupingBy(DoorStaffRecord::getOpenTimeHour));
            dateNodeVos2.forEach(el -> {
                map2.forEach((k, v) -> {
                    if (k != null && el.getX().split(":")[0].equals(k.split(" ")[1])) {
                        el.setY((long) v.size());
                    }
                });
            });
        }
        map.put("enterCount", enterList.size());
        map.put("goOutCount", list.size());
        map.put("enterNode", dateNodeVos1);
        map.put("goOutNode", dateNodeVos2);
        return map;
    }

    private Object DataReturnDay(ArrayList<DoorStaffRecord> list) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("count", list.size());
        // 进入方向的设备查询
        List<String> enter = doorDeviceFeignClient.getDeviceCodeInformation("1");
        ArrayList<DoorStaffRecord> enterList = new ArrayList<>();
        enter.forEach(El -> {
            list.forEach(el -> {
                if (El.equals(el.getDeviceCode())) {
                    enterList.add(el);
                }
            });
        });
        List<DateNodeVo> listNode1 = DateUtil.LastWeek(7);//获取一周的节点 yyyy-mm-dd 节点
        List<DateNodeVo> listNode2 = DateUtil.LastWeek(7);//获取一周的节点 yyyy-mm-dd 节点
        if (list.size() != 0) {
            // 开门方向的记录
            Map<String, List<DoorStaffRecord>> map1 = enterList.stream().collect(Collectors.groupingBy(DoorStaffRecord::getOpenTimeDay));
            listNode1.forEach(el -> {
                map1.forEach((k, v) -> {
                    if (el.getX().equals(k)) {
                        el.setY((long) v.size());
                    }
                });
            });
            list.removeAll(enterList);
            // 反方向
            Map<String, List<DoorStaffRecord>> map2 = list.stream().collect(Collectors.groupingBy(DoorStaffRecord::getOpenTimeDay));
            listNode2.forEach(el -> {
                map2.forEach((k, v) -> {
                    if (el.getX().equals(k)) {
                        el.setY((long) v.size());
                    }
                });
            });
        }
        map.put("enterCount", enterList.size());
        map.put("goOutCount", list.size());
        map.put("enterNode", listNode1);
        map.put("goOutNode", listNode2);
        return map;
    }

    private static String getyyyyMMddDate(Date date, int day) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DAY_OF_MONTH, day);
            return format.format(cal.getTime());
        } catch (Exception e) {
            return "";
        }
    }
}
