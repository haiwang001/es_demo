package com.bjxczy.onepark.Inandoutmodule.service;

public interface OverViewService {
    Object toDayFlow(String id);

    Object accumulativeFlow(String id);

    Object numberOfPeople(String id);
}
