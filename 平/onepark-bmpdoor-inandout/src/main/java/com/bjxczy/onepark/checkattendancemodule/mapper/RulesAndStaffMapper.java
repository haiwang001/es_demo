package com.bjxczy.onepark.checkattendancemodule.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.checkattendancemodule.entity.RulesAndStaff;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


import java.util.List;

/*
 *@ClassName RulesAndStaffMapper
 *@Description TODO
 *@Author 温良伟
 *@Date 2023/3/30 14:27
 *@Version 1.0
 */
@Mapper
public interface RulesAndStaffMapper extends BaseMapper<RulesAndStaff> {

    @Select("select staff_id from k_rules_staff where rules_id = #{rulesId}")
    List<Integer> selectByRulesId(@Param("rulesId") String rulesId);
}
