package com.bjxczy.onepark.checkattendancemodule.service;


import com.bjxczy.onepark.checkattendancemodule.pojo.dto.StaffCheckAttendanceDto;

import javax.servlet.http.HttpServletResponse;

public interface StaffCheckAttendanceService {
    Object getListPage(StaffCheckAttendanceDto dto);

    Object details(StaffCheckAttendanceDto dto);

    void download(HttpServletResponse response, StaffCheckAttendanceDto dto);
}
