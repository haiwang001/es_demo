package com.bjxczy.onepark.Inandoutmodule.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * @Auther: Administrator
 * @Date: 2023/7/4 0004 14:55
 * @Description: DoorStaffRecordQueryDTO
 * @Version 1.0.0
 */
@Data
public class DoorStaffRecordQueryDTO extends BasePageDTO{

    /**
    * 描述:综合查询（员工编号/姓名/手机号）
    * 是否必填：
    **/
    private String unifyParams;

    /**
    * 描述：部门id
    * 是否必填：
    **/
    private Integer deptId;

    /**
    * 描述：区域id
    * 是否必填：
    **/
    private Integer areaId;

    /**
    * 描述：楼层名称
    * 是否必填：
    **/
    private String floorName;

    /**
    * 描述：凭证号
    * 是否必填：
    **/
    private String cardNo;

    /**
     * 描述：开门方式：字典（openType）
     * 是否必填：
     **/
    private String openType;

    /**
     * 描述：开门方向：进/出
     * 是否必填：
     **/
    private String direction;

    /**
     * 描述：设备名称
     * 是否必填：
     **/
    private String deviceName;

    /**
     * 描述：开门时间-开始
     * 是否必填：
     **/
    private String openStartTime;

    /**
     * 描述：开门时间-结束
     * 是否必填：
     **/
    private String openEndTime;

    /**
    * 描述：部门ids
    * 是否必填：
    **/
    private List<String> deptIds;

}
