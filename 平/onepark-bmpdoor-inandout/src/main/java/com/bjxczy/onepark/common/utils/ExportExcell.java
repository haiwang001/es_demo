package com.bjxczy.onepark.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class ExportExcell {

    /**
     * 导出文件
     * @param filepath 文件存储路径
     * @param titleCNList 中文标题列表
     * @param titleENList 英文属性名列表
     * @param valueList 内容列表
     * @return
     */
    @SuppressWarnings({ "resource", "deprecation" })
	public static boolean exportFile(File filepath,List<String> titleCNList,List<String> titleENList,ArrayList<HashMap<String,String>> valueList) {
    	FileOutputStream is =null;
    	try {
			is = new FileOutputStream(filepath);
		} catch (FileNotFoundException e1) {
			log.error(e1.getMessage());
			return false;
		}
    	//创建工作簿
		XSSFWorkbook workbook = new XSSFWorkbook();
    	XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFDataFormat format = workbook.createDataFormat();
        //设置文本格式
        cellStyle.setDataFormat(format.getFormat("@"));
    	//创建表格
    	XSSFSheet sheet = workbook.createSheet("数据");
    	//创建标题行
    	XSSFRow row = sheet.createRow(0);
    	XSSFCell cell ;
    	for (int i = 0; i < titleCNList.size(); i++) {
    		cell = row.createCell(i);
    		cell.setCellValue(titleCNList.get(i));
		}
    	//填充内容
    	for (int i = 0; i < valueList.size(); i++) {
    		row = sheet.createRow(i+1);

    		for (int j = 0; j < titleENList.size(); j++) {
        		cell = row.createCell(j);
        		//以文本字符串格式保存长数字,避免科学计数法保存
        		cell.setCellStyle(cellStyle);
        		cell.setCellValue(valueList.get(i).get(titleENList.get(j)));
    		}
		}
    	for (int i = 0; i < titleCNList.size(); i++) {
    		sheet.autoSizeColumn(i);
    	}
    	sheet.autoSizeColumn('C');
    	try {
			workbook.write(is);
		} catch (FileNotFoundException e) {
			log.error(e.getMessage());
			return false;
		} catch (IOException e) {
			log.error(e.getMessage());
			return false;
		}finally {
			if (workbook != null) {
				try {
					workbook.close();
				} catch (IOException e1) {
					log.error("关闭workbook出错：{}", e1.getMessage());
				}
			}
			if(is!=null) {
				try {
					is.close();
				} catch (IOException e1) {
					log.error("关闭输出流出错：{}", e1.getMessage());
				}
			}
		}
    	return true;
    }

	/**
	 * 获取Excel
	 * @param sheetName sheet页的名称
	 * @param headers 表头
	 * @param data 数据
	 * @return
	 */
	public static HSSFWorkbook getExcelFile(String sheetName, List<String> headers, List<String> eName, List<Map<String,String>> data){
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(sheetName);
		//headers表示excel表中第一行的表头
		HSSFRow row = sheet.createRow(0);
		//在excel表中添加表头
		for(int i=0;headers.size()>i;i++){
			HSSFCell cell = row.createCell(i);
			HSSFRichTextString text = new HSSFRichTextString(headers.get(i));
			cell.setCellValue(text);
		}
		for(int rowNum = 1; data!=null && data.size()>rowNum-1; rowNum++){
			//获取本条记录
			Map<String,String> rowData = data.get(rowNum-1);
			HSSFRow row1 = sheet.createRow(rowNum);
			if(rowData==null || rowData.size()==0){
				continue;
			}
			for(int j=0; eName.size()>j; j++){
				row1.createCell(j).setCellValue(rowData.get(eName.get(j)));
			}
		}
		return workbook;
	}


	/**
	 * 获取Excel
	 * @param sheetName sheet页的名称
	 * @param headers 表头
	 * @param data 数据
	 * @return
	 */
	public static XSSFWorkbook getExcelFileXSSF(String sheetName, List<String> headers, List<String> eName, List<Map<String,String>> data){
		//创建工作簿
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFCellStyle cellStyle = workbook.createCellStyle();
		XSSFDataFormat format = workbook.createDataFormat();
		//设置文本格式
		cellStyle.setDataFormat(format.getFormat("@"));
		//创建表格
		XSSFSheet sheet = workbook.createSheet(sheetName);
		//创建标题行
		XSSFRow row = sheet.createRow(0);
		XSSFCell cell ;
		for (int i = 0; i < headers.size(); i++) {
			cell = row.createCell(i);
			cell.setCellValue(headers.get(i));
		}
		//填充内容
		for (int i = 0; i < data.size(); i++) {
			row = sheet.createRow(i+1);
			Map<String,String> valueList = data.get(i);
			for (int j = 0; j < eName.size(); j++) {
				cell = row.createCell(j);
				//以文本字符串格式保存长数字,避免科学计数法保存
				cell.setCellStyle(cellStyle);
				cell.setCellValue(valueList.get(eName.get(j)));
			}
		}
		for (int i = 0; i < headers.size(); i++) {
			// 调整每一列宽度
			sheet.autoSizeColumn(i);
			// 解决自动设置列宽中文失效的问题
			sheet.setColumnWidth(i, sheet.getColumnWidth(i) * 17 / 10);
		}
		return workbook;
	}


	public static List getCourseListByExcel(InputStream in, String fileName) throws Exception {

		List list = new ArrayList<>();

		// 创建excel工作簿
		Workbook work = getWorkbook(in, fileName);
		if (null == work) {
			throw new Exception("创建Excel工作薄为空！");
		}

		Sheet sheet = null;
		Row row = null;
		Cell cell = null;

		for (int i = 0; i < work.getNumberOfSheets(); i++) {

			sheet = work.getSheetAt(i);
			if(sheet == null) {
				continue;
			}

			for (int j = 0; j <= sheet.getLastRowNum(); j++) {
				row = sheet.getRow(j);
				if (0 == j) {
					continue;
				}

				List<Object> li = new ArrayList<>();

				for (int y = 0; y < 21; y++) {
					cell = row.getCell(y);
					// 日期类型转换
//					if(y == 11) {
//						//cell.setCellType(CellType.STRING);
//						double s1 = cell.getNumericCellValue();
//						Date date = HSSFDateUtil.getJavaDate(s1);
//						li.add(date);
//						continue;
//					}
					if (cell == null){
						cell = row.createCell(y);
						cell.setCellValue("");
					}
					li.add(cell);
				}
				list.add(li);
			}
		}
		work.close();
		return list;
	}

	/**
	 * 判断文件格式
	 * @param in
	 * @param fileName
	 * @return
	 */
	private static Workbook getWorkbook(InputStream in, String fileName) throws Exception {

		Workbook book = null;
		String filetype = fileName.substring(fileName.lastIndexOf("."));

		if(".xls".equals(filetype)) {
			book = new HSSFWorkbook(in);
		} else if (".xlsx".equals(filetype)) {
			book = new XSSFWorkbook(in);
		} else {
			throw new Exception("请上传excel文件！");
		}

		return book;
	}
}