package com.bjxczy.onepark.Inandoutmodule.service.impl;


import cn.hutool.core.date.DateTime;
import com.alibaba.fastjson.JSON;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.onepark.Inandoutmodule.entity.DoorStaffRecord;
import com.bjxczy.onepark.Inandoutmodule.pojo.vo.EntryAndExitStatistics;
import com.bjxczy.onepark.Inandoutmodule.pojo.vo.GoThroughEntranceGuardVo;
import com.bjxczy.onepark.Inandoutmodule.pojo.vo.HeadcountVo;
import com.bjxczy.onepark.Inandoutmodule.pojo.vo.Retention02Vo;
import com.bjxczy.onepark.Inandoutmodule.pojo.vo.RetentionVisitorVo;
import com.bjxczy.onepark.Inandoutmodule.pojo.vo.TodayVisitor02Vo;
import com.bjxczy.onepark.Inandoutmodule.service.ScreenService;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.feign.client.BmpVisitorFeignClient;
import com.bjxczy.onepark.common.model.common.DictInformation;
import com.bjxczy.onepark.common.model.visitor.VisitorInformation;
import com.bjxczy.onepark.common.service.TemplateSqlService;
import com.bjxczy.onepark.common.utils.ConvertUtils;
import com.bjxczy.onepark.common.utils.DateUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/*
 *@Author wlw
 *@Date 2023/6/14 15:30
 */
@Service
@Log4j2
public class ScreenServiceImpl implements ScreenService {
    @Autowired
    private TemplateSqlService templateSqlService;
    @Resource
    private BmpVisitorFeignClient VisitorFeignClient;
    @Autowired
    private DictService dictService;
    @Value("${dict.comeFrom}")
    private String comeFrom;

    @Override
    public Object retention02(String day, String id) {
        String startDate = day + " 00:00:00";
        String endDate = day + " 23:59:59";
        long startTime = DateTime.of(startDate, "yyyy-MM-dd HH:mm:ss").getTime();
        long endTime = DateTime.of(endDate, "yyyy-MM-dd HH:mm:ss").getTime();
        List<DoorStaffRecord> list = getRecordByAreaIdAndTimeSlot(id, startTime, endTime);
        Map<String, List<DoorStaffRecord>> staff = list.stream().collect(Collectors.groupingBy(DoorStaffRecord::getStaffName));
        Set<String> keySet = staff.keySet();
        ArrayList<RetentionVisitorVo> vo = new ArrayList<>(staff.size());
        for (String s : keySet) {
            DoorStaffRecord recordDoor = staff.get(s).get(0);
            if (recordDoor.getComeFrom().equals("2")) {// 访客
                if (recordDoor.getDirection().equals("进")) {// 最后一条记录为进 产生滞留
                    VisitorInformation v = VisitorFeignClient.selectVisitorByMobile(recordDoor.getMobile());
                    if (v != null) {
                        StringBuilder date = new StringBuilder();
                        if (StringUtils.isNotEmpty(v.getVisitTime())){
                            date.append(DateUtil.dateToChinese(v.getVisitTime()));
                        }
                        date.append(" - ");
                        if (StringUtils.isNotEmpty(v.getLeaveTime())){
                            date.append(DateUtil.dateToChinese(v.getLeaveTime()));
                        }
                        vo.add(new RetentionVisitorVo(recordDoor.getStaffName(), recordDoor.getCaptrue(), recordDoor.getMobile(),date.toString()));
                    }
                }
            }
        }
        return new Retention02Vo(vo, vo.size());
    }

    @Override
    public Object goThroughEntranceGuard(String day, String id) {
        ArrayList<GoThroughEntranceGuardVo> ret = new ArrayList<>();
        String startDate = day + " 00:00:00";
        String endDate = day + " 23:59:59";
        long startTime = DateTime.of(startDate, "yyyy-MM-dd HH:mm:ss").getTime();
        long endTime = DateTime.of(endDate, "yyyy-MM-dd HH:mm:ss").getTime();
        List<DoorStaffRecord> list = getRecordByAreaIdAndTimeSlot(id, startTime, endTime);
        for (DoorStaffRecord door : list) {
            String s = covertDict(comeFrom, door.getComeFrom());
            door.setComeFrom(s);
            GoThroughEntranceGuardVo vo = new GoThroughEntranceGuardVo(door.getStaffName(),
                    door.getComeFrom(),
                    door.getDirection(),
                    DateTime.of(door.getOpenTime()).toString("HH:mm:ss"),
                    door.getCaptrue(),
                    door.getMobile(),
                    door.getDeviceName(),
                    door.getSpaceName(),
                    door.getOpenTypeText());
            ret.add(vo);
        }
        return ret;
    }

    /*
     * @param day
     * @return
     */
    @Override
    public Object todayVisitor02(String day, String id) {
        // 今日需要来的访客
        List<VisitorInformation> list = VisitorFeignClient.selectVisitorByVisitToday(DateUtil.now(),id != null ? id : "");
        String startDate = day + " 00:00:00";
        String endDate = day + " 23:59:59";
        long startTime = DateTime.of(startDate, "yyyy-MM-dd HH:mm:ss").getTime();
        long endTime = DateTime.of(endDate, "yyyy-MM-dd HH:mm:ss").getTime();
        List<DoorStaffRecord> listDoor = getRecordByAreaIdAndTimeSlot(id, startTime, endTime);
        Map<String, List<DoorStaffRecord>> collect = listDoor.stream().collect(Collectors.groupingBy(DoorStaffRecord::getComeFrom));
        List<DoorStaffRecord> recordDoors = collect.get("2");
        Integer visit = 0, noShow = 0;
        for (VisitorInformation visitor : list) {
            if (isVisit(recordDoors, visitor)) {
                visit++;
            } else {
                noShow++;
            }
        }
        return new TodayVisitor02Vo(visit, noShow);
    }

    private boolean isVisit(List<DoorStaffRecord> recordDoors, VisitorInformation visitor) {
        if (recordDoors == null) {
            return false;
        }
        for (DoorStaffRecord door : recordDoors) {
            if (door.getStaffName().equals(visitor.getVisitorName()) && door.getMobile().equals(visitor.getVisitorMobile())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public HeadcountVo headcount(String day, String id) {
        String startDate = day + " 00:00:00";
        String endDate = day + " 23:59:59";
        long startTime = DateTime.of(startDate, "yyyy-MM-dd HH:mm:ss").getTime();
        long endTime = DateTime.of(endDate, "yyyy-MM-dd HH:mm:ss").getTime();
        List<DoorStaffRecord> list = getRecordByAreaIdAndTimeSlot(id, startTime, endTime);
        int visitor = 0, proprietor = 0, staff = 0;
        Map<String, List<DoorStaffRecord>> mobile = list.stream().collect(Collectors.groupingBy(DoorStaffRecord::getMobile));
        Set<String> keySet = mobile.keySet();
        for (String s : keySet) {
            if (mobile.get(s).get(0).getComeFrom().equals("2")) {
                visitor++;
            } else {
                staff++;
            }
        }
        return new HeadcountVo(visitor, proprietor, staff);
    }

    @Override
    public Object entryAndExitStatistics(String day, String id) {
        String startDate = day + " 00:00:00";
        String endDate = day + " 23:59:59";
        long startTime = DateTime.of(startDate, "yyyy-MM-dd HH:mm:ss").getTime();
        long endTime = DateTime.of(endDate, "yyyy-MM-dd HH:mm:ss").getTime();
        List<DoorStaffRecord> list = getRecordByAreaIdAndTimeSlot(id, startTime, endTime);
        Map<String, List<DoorStaffRecord>> collect = list.stream().collect(Collectors.groupingBy(DoorStaffRecord::getDirection));
        return new EntryAndExitStatistics(getHeadcountVo(collect.get("进")), getHeadcountVo(collect.get("出")));
    }


    private HeadcountVo getHeadcountVo(List<DoorStaffRecord> recordDoors) {
        int visitor = 0, proprietor = 0, staff = 0;
        if (null == recordDoors) {
            return new HeadcountVo(visitor, proprietor, staff);
        }
        for (DoorStaffRecord recordDoor : recordDoors) {
            String s1 = !recordDoor.getMobile().equals(SysConst.DATA_NULL_SHOW) && !recordDoor.getMobile().equals("") ? recordDoor.getMobile() : recordDoor.getEmployeeNo();
            if (s1.equals("")) {
                s1 = "110";
            }
            if (recordDoor.getComeFrom().equals("2")) {
                visitor++;
            } else {
                staff++;
            }
        }
        return new HeadcountVo(visitor, proprietor, staff);
    }

    /**
     * @Author qinyuan
     * @Description 根据时间区域id查询记录
     * @Date 10:03 2023/7/7 0007
     **/
    private List<DoorStaffRecord> getRecordByAreaIdAndTimeSlot(String areaId, Long startDate, Long endDate) {
        List<DoorStaffRecord> records = new ArrayList<>();
        Map<String, Object> params = new HashMap<>();
        params.put("areaId", areaId);
        params.put("startDate", startDate);
        params.put("endDate", endDate);
        List<Map<String, Object>> list = templateSqlService.list("doorStaff/getRecordByAreaIdAndTimeSlot", params);
        //类型转换
        if (CollectionUtils.isNotEmpty(list)) {
            records = list.stream().map(x -> {
                DoorStaffRecord doorStaffRecord = JSON.parseObject(JSON.toJSONString(x), DoorStaffRecord.class);
                return doorStaffRecord;
            }).collect(Collectors.toList());
        }
        //数据处理
        if (CollectionUtils.isNotEmpty(records)){
            for (DoorStaffRecord record : records) {
                ConvertUtils.placeholderToNullString(record);
                if (StringUtils.isEmpty(record.getOpenTimeText())&&record.getOpenTime()!=null&&record.getOpenTime()!=0){
                    record.setOpenTimeText(DateTime.of(record.getOpenTime()).toString("yyyy-MM-dd HH:mm:ss"));
                }
                record.setOpenTimeDay(DateTime.of(record.getOpenTime()).toString("yyyy-MM-dd"));
            }
        }
        return records;
    }
    private String covertDict(String type,String value){
        String ret = null;
        //来源
        List<DictInformation> dfDict = dictService.listDict(type);
        if (CollectionUtils.isNotEmpty(dfDict)&&StringUtils.isNotEmpty(value)){
            for (DictInformation dictInformation : dfDict) {
                if (value.equals(dictInformation.getValue())){
                    ret = dictInformation.getLabel();
                }
            }
        }
        return ret;
    }
}
