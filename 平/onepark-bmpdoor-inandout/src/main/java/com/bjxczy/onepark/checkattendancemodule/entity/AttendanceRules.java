package com.bjxczy.onepark.checkattendancemodule.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

/*
 *@ClassName AttendanceRules
 *@Author 温良伟
 *@Date 2023/3/29 16:54
 *@Version 1.0   考勤规则  RulesAndStaff
 */
@Data
@TableName("k_attendance_rules")
public class AttendanceRules {

    @TableId()
    private String rulesId; // 规则名称 rules_id
    private Integer peopleNumber; // 使用考勤规则的人员数  people_number
    private String rulesName; // 规则名称 rules_name

    private String officeHours; // 上班时间 office_hours
    private String offDutyTime; // 正常下班时间 off_duty_time

    @TableField(exist = false) // 不是数据库字段：
    private List<WeeksRules> weeks; // 考勤周规则 weeks

    @TableField(exist = false) // 不是数据库字段：
    private String weeksLabel; //
    @TableField(exist = false) // 不是数据库字段：
    private String officeHoursLabel; //

    @TableField(exist = false) // 不是数据库字段：
    private List<Integer> ids; // 使用本考勤模板的人
    private String operator; // operator
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime; // create_time
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag; // del_flag
}
