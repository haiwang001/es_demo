package com.bjxczy.onepark.common.feign.client;

import org.springframework.cloud.openfeign.FeignClient;

import com.bjxczy.core.feign.client.door.BaseFaceFeignClient;

@FeignClient("bmpface")
public interface BmpFaceFeignClient extends BaseFaceFeignClient {}
