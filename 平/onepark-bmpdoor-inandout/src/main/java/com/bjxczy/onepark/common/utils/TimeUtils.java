package com.bjxczy.onepark.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Date;

public class TimeUtils {

    /**
     * 返回当前时间，ISO_LOCAL_DATE_TIME格式
     *
     * @return 当前时间，ISO_LOCAL_DATE_TIME格式
     */
    public static String now() {
        return LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    /**
     * 将HH：mm时间换算成秒数
     *
     * @param time HH:mm格式时间
     * @return 距离00:00的秒数，如果参数非法则返回null
     */
    public static Integer hourMinute2Second(String time) {
        if (StringUtils.isBlank(time)) {
            return null;
        }
        try {
            LocalTime localTime = LocalTime.parse(time);
            return localTime.toSecondOfDay();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将秒数换算成HH：mm时间
     *
     * @param second 距离00:00的秒数
     * @return HH：mm时间，如果参数非法则返回null
     */
    public static String second2HourMinute(Integer second) {
        if (second == null) {
            return null;
        }
        LocalTime localTime = LocalTime.ofSecondOfDay(second);
        return localTime.format(DateTimeFormatter.ofPattern("HH:mm"));
    }

    /**
     * 将秒数换算成[hh,mm]时间数组
     *
     * @param second 距离00:00的秒数
     * @return [hh, mm]时间数组，如果参数非法则返回null
     */
    public static int[] second2HourMinuteArray(Integer second) {
        if (second == null) {
            return null;
        }
        LocalTime localTime = LocalTime.ofSecondOfDay(second);
        int[] hourMinute = new int[2];
        hourMinute[0] = localTime.getHour();
        hourMinute[1] = localTime.getMinute();
        return hourMinute;
    }

    /***
     * lzx
     *根据时分获取当天指定时间 的时间 戳
     *      "年: " + now.get(Calendar.YEAR);
     "月: " + (now.get(Calendar.MONTH) + 1) + "";
     "日: " + now.get(Calendar.DAY_OF_MONTH);
     "时: " + now.get(Calendar.HOUR_OF_DAY);
     "分: " + now.get(Calendar.MINUTE);
     "秒: " + now.get(Calendar.SECOND);
     "当前时间毫秒数：" + now.getTimeInMillis();
     now.getTime();
     *
     * @param hourMinute 格式HH:mm
     * @return 秒级的时间戳
     */
    public static Long timeStamp(String hourMinute) {
        if (StringUtils.isBlank(hourMinute)) {
            return null;
        }
        try {
            LocalTime localTime = LocalTime.parse(hourMinute);
            LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(), localTime);
            return localDateTime.atZone(ZoneId.systemDefault()).toEpochSecond();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取 yyyy-MM-dd HH:mm:ss格式的时间
     *
     * @param time 时间，精确到秒
     * @return yyyy-MM-dd HH:mm:ss
     */
    public static String date2String(Date time) {
        return TimeUtils.date2String(time, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * @param time       时间
     * @param formatType 时间格式
     * @return 格式化后的时间字符串
     */
    public static String date2String(Date time, String formatType) {
        LocalDateTime localDateTime = LocalDateTime.
                ofInstant(time.toInstant(), ZoneId.systemDefault());
        return localDateTime.format(DateTimeFormatter.ofPattern(formatType));
    }

    /**
     * 将一种格式的datetime转化为ISO_LOCAL_DATE_TIME格式
     *
     * @param time       datetime字符串
     * @param formatType 原始的时间格式
     * @return
     */
    public static String date2String(String time, String formatType) {
        LocalDateTime localDateTime = LocalDateTime.parse(time,
                DateTimeFormatter.ofPattern(formatType));
        return localDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    /**
     * string类型转换为long类型
     * strTime要转换的String类型的时间
     * formatType时间格式
     * strTime的时间格式和formatType的时间格式必须相同
     */
    public static long stringToLong(String strTime) throws ParseException {
        //String类型转成date类型
        Date date = TimeUtils.stringToDate1(strTime, "yyyy-MM-dd HH:mm:ss");
        if (date == null) {
            return 0;
        } else {
            // date类型转成long类型
            return dateToLong1(date);
        }
    }

    public static Date StringToDate(String time) {
        return TimeUtils.stringToDate1(time, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * @param strTime    时间字符串
     * @param formatType 时间格式
     * @return 时间所对应的Date对象
     */
    public static Date stringToDate1(String strTime, String formatType) {
        try {
            LocalDateTime dateTime = LocalDateTime.parse(strTime,
                    DateTimeFormatter.ofPattern(formatType));
            return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 返回时间戳，毫秒级
     *
     * @param date date对象
     * @return 时间戳，毫秒级
     */
    public static long dateToLong1(Date date) {
        return date.getTime();
    }

    /**
     * 获取当前的时间精确到天
     * 日期格式"yyyy-MM-dd"
     *
     * @return 当前的时间精确到天
     */
    public static String getCurrentTime() {
        return LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    /**
     * 获取当前的时间精确到秒
     * 格式"yyyy-MM-dd HH:mm:ss"
     *
     * @return 获取当前的时间精确到秒，格式"yyyy-MM-dd HH:mm:ss"
     */
    public static String getCurrentDateTime() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * @param date 日期，格式为"yyyy-MM-dd"
     * @return 日期所在的年的周数
     */
    public static int getYear_week(String date) {
        LocalDate localDate = LocalDate.parse(date);
        return localDate.get(WeekFields.ISO.weekOfWeekBasedYear());
    }

    /**
     * 获取日期所在的周数，周一到周日
     *
     * @param date 日期
     * @return
     */
    public static int getWeek(String date) {
        LocalDate localDate = LocalDate.parse(date);
        return localDate.getDayOfWeek().getValue();
    }

    //得到某周开始日期
    public static String getStartDayOfWeekNo(int year, int weekNo) {
        //周数小于10在前面补个0
        String numStr = weekNo < 10 ? "0" + String.valueOf(weekNo) : String.valueOf(weekNo);
        //2019-W01-01获取第一周的周一日期，2019-W02-07获取第二周的周日日期
        String weekDate = String.format("%s-W%s-%s", year, numStr, DayOfWeek.MONDAY);
        LocalDate localDate = LocalDate.parse(weekDate, DateTimeFormatter.ISO_WEEK_DATE);
        return localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    /**
     * 得到某周结束日期
     *
     * @param year   年份
     * @param weekNo 周数
     * @return
     */
    public static String getEndDayOfWeekNo(int year, int weekNo) {
        //周数小于10在前面补个0
        String numStr = weekNo < 10 ? "0" + String.valueOf(weekNo) : String.valueOf(weekNo);
        //2019-W01-01获取第一周的周一日期，2019-W02-07获取第二周的周日日期
        String weekDate = String.format("%s-W%s-%s", year, numStr, DayOfWeek.SUNDAY);
        LocalDate localDate = LocalDate.parse(weekDate, DateTimeFormatter.ISO_WEEK_DATE);
        return localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    /**
     * 根据年月获取该月份的第一天
     *
     * @param year  年份
     * @param month 月份
     * @return 该月份的第一天
     */
    public static String getStartDayOfMonth(int year, int month) {
        YearMonth yearMonth = YearMonth.of(year, month);
        return yearMonth.atDay(1)
                .format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    /**
     * 根据月份获取该月份最后一天的日期
     *
     * @param year  年份
     * @param month 月份
     * @return 该月份最后一天的日期
     */
    public static String getEndDayOfMonth(int year, int month) {
        YearMonth yearMonth = YearMonth.of(year, month);
        return yearMonth.atDay(yearMonth.lengthOfMonth())
                .format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    /**
     * 获取日期所在的月
     *
     * @param day 原始日期
     * @return 月的整数形式
     */
    public static int getMonth(String day) {
        try {
            LocalDate localDate = LocalDate.parse(day);
            return localDate.getMonth().getValue();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取当天剩余的秒数
     *
     * @return 当天剩余的秒数
     */
    public static Integer getRemainSecondsOneDay() {
        LocalTime localTime = LocalTime.now();
        return LocalTime.MAX.toSecondOfDay() - localTime.toSecondOfDay() + 1;
    }

    /**
     * 将给定的日期加减指定日期后返回
     *
     * @param oldDateStr 原始日期
     * @param formatStr  日期格式
     * @param addNum     添加的天数
     * @return 新的日期
     */
    public static String getCacuDateStr(String oldDateStr, String formatStr, int addNum) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatStr);
        LocalDate oldDate = LocalDate.parse(oldDateStr, formatter);
        LocalDate returnDate = oldDate.plusDays(addNum);
        return returnDate.format(formatter);
    }

    /**
     * 将给定的日期加减指定日期后返回
     *
     * @param oldDateStr 原始日期
     * @param formatStr  日期格式
     * @param downNum    减少的天数
     * @return 新的日期
     */
    public static String getDownDateStr(String oldDateStr, String formatStr, int downNum) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatStr);
        LocalDate oldDate = LocalDate.parse(oldDateStr, formatter);
        LocalDate returnDate = oldDate.minusDays(downNum);
        return returnDate.format(formatter);
    }
    
    
    public static String getTimestampByUtc(String utcTime){
        ZonedDateTime zdt  = ZonedDateTime.parse(utcTime);
        LocalDateTime localDateTime = zdt.toLocalDateTime();
        LocalDateTime plusHours = localDateTime.plusHours(8);
        return String.valueOf(plusHours.toEpochSecond(ZoneOffset.of("+8")));    	
    }
    /**
     * ISO8601标准时间输出
     */
    public static String getISO8601Timestamp(LocalDateTime date) {
        ZoneOffset offset = ZoneOffset.of("+08:00");
        OffsetDateTime Date = OffsetDateTime.of(date, offset);

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        return Date.format(formatter2);

    }

    public static String transTimeStampToISO8601(String timeStamp) {
        Timestamp t = new Timestamp(Long.parseLong(timeStamp));
        Date dateT = new Date(t.getTime());

        Instant instant = dateT.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();

        return getISO8601Timestamp(localDateTime);
    }

    //返回秒时间差单位分钟
    public static long getBetweenSecond(String beginTime, String endTime) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
//        String timeBegin=timeFormat.format(beginTime);
//        String timeEnd=timeFormat.format(endTime);
        String timeBegin = beginTime.substring(11, 19);
        String timeEnd = endTime.substring(11, 19);
        try {
            Date btime = timeFormat.parse(timeBegin);
            Date etime = timeFormat.parse(timeEnd);
            return Math.abs(btime.getTime() - etime.getTime()) / 1000 / 60;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}
