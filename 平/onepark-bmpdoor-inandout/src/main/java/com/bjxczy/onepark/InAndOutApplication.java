package com.bjxczy.onepark;

import com.bjxczy.onepark.config.ParkConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import javax.annotation.Resource;

@EnableCaching
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class InAndOutApplication {

    public static void main(String[] args) {
        SpringApplication.run(InAndOutApplication.class, args);
    }

}




