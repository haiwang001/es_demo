package com.bjxczy.onepark.checkattendancemodule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.checkattendancemodule.entity.AttendanceRules;
import com.bjxczy.onepark.checkattendancemodule.pojo.dto.AttendanceRulesDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AttendanceRulesMapper extends BaseMapper<AttendanceRules> {

    @Select("select *\n" +
            "from k_attendance_rules\n" +
            "where del_flag = 0\n" +
            "and if(#{dto.rulesName} is not null, rules_name like concat('%', #{dto.rulesName}, '%'), true)\n" +
            "order by create_time desc")
    List<AttendanceRules> selectByDto(@Param("dto") AttendanceRulesDto dto);

    @Select("select * from k_attendance_rules where del_flag = 0 and rules_id = (select  rules_id  from  k_rules_staff where staff_id =#{staffId})")
    AttendanceRules selectByStaffId(@Param("staffId") String staffId);
}
