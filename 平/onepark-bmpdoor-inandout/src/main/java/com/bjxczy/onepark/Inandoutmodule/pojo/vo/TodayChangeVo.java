package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/16 11:24
 */
@Data
public class TodayChangeVo {
    private List<DateNodeVo> In;
    private List<DateNodeVo> Out;

    public TodayChangeVo(List<DateNodeVo> in, List<DateNodeVo> out) {
        In = in;
        Out = out;
    }
}
