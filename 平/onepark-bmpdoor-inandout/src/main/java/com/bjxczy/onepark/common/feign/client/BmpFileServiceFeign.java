package com.bjxczy.onepark.common.feign.client;


import com.bjxczy.core.feign.client.pcs.FileFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 文件上传接口
 */
@FeignClient("pcs")
public interface BmpFileServiceFeign extends FileFeignClient {}