package com.bjxczy.onepark.checkattendancemodule.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/*
 *@ClassName WeeksRules
 *@Author 温良伟
 *@Date 2023/3/29 17:21
 *@Version 1.0   周规则的绑定
 */
@Data
@TableName("k_weeks_rules")
public class WeeksRules {
    @TableId
    private String rulesId; // 规则id 绑定 rules_id
    private Integer mark; //周几的标记 mark
    private String display; //展示
    private Boolean isCheck; //是否勾选
}
