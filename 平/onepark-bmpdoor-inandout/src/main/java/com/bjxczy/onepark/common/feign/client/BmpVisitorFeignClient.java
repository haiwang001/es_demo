package com.bjxczy.onepark.common.feign.client;

import com.bjxczy.core.feign.client.visitor.BaseVisitorFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("bmpvoucher")
public interface BmpVisitorFeignClient extends BaseVisitorFeignClient {}
