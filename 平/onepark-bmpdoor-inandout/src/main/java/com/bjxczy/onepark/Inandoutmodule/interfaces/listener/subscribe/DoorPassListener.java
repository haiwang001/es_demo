package com.bjxczy.onepark.Inandoutmodule.interfaces.listener.subscribe;

import com.bjxczy.core.rabbitmq.event.door.InOrOutRecordPayload;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.onepark.Inandoutmodule.service.DoorStaffRecordService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description: 门禁通行监听
 */
@Component
@Slf4j
public class DoorPassListener extends DoorMutualBaseListener {

    @Autowired
    private DoorStaffRecordService doorStaffRecordService;

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(name = "bmpinandout_push_record_queue", durable = "true"),
            key = PUSH_IN_OR_OUT_RECORD,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void pushInOrOutRecord(InOrOutRecordPayload payload, Channel channel, Message message) {
        this.confirm(()->{
            doorStaffRecordService.add(payload);
            return true;
            },channel,message);
    }


}
