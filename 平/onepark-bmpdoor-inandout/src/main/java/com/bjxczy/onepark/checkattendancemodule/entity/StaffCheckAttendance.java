package com.bjxczy.onepark.checkattendancemodule.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/*
 *@ClassName StaffCheckAttendance
 *@Author 温良伟
 *@Date 2023/3/30 9:56
 *@Version 1.0   员工考勤总览
-- auto-generated definition
create table k_staff_check_attendance
(
    id             bigint auto_increment
        primary key,
    rules_id       varchar(255)  null,
    rules_name       varchar(255)  null,
    create_time    datetime      null,
    earliest_time    datetime      null,
    latest_time    datetime      null,
    dept_id        varchar(255)  null,
    dept_name      varchar(255)  null,
    del_flag       int default 0 not null,
    is_be_late     int           null comment '是否迟到 1 正常  2迟到',
    is_leave_early int           null comment '是否早退 1 正常  2早退',
    weeks_mark int           null comment '周几',
    employee_mark  varchar(255)           null comment '员工标记',
    name           varchar(255)  null
)
    comment '考勤记录';
     /*    考勤流程
     *   获取 考勤 规则内 的员工 某一个人 今日的通行记录
     *   通过时间去匹配
     *   遍历记录列表 找到该员工对应的考勤规则
     *    把小时转换成数字类型  通行记录的时间 只要是小时小于等于考勤规则的时间  即为或未迟到
     *
     *
 * */
@Data
@TableName("k_staff_check_attendance")
public class StaffCheckAttendance {

    @TableId(type = IdType.AUTO)
    private Long id; // 记录id
    private String rulesId; // 规则id 绑定 rules_id
    private String rulesName; // 规则名称 rules_name
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime; // 生成记录的时间 create_time
    private String name;  // 员工姓名
    private String employeeMark;  // 员工标记 employee_mark

    private String deptId; // 员工部门id dept_id
    private String deptName; // 员工部门名称  dept_name
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag; // del_flag
    @TableField(exist = false) // 不是数据库字段：
    private String isBeLateLabel; //
    @TableField(exist = false) // 不是数据库字段：
    private String isLeaveEarlyLabel; //
    private Integer isBeLate;  // 是否迟到   1 正常  2迟到  is_be_late
    private String earliestTime; // 生成记录的时间 earliest_time
    private Integer isLeaveEarly;  // 是否早退   1 正常  2早退  is_leave_early
    private String latestTime; // 生成记录的时间 latest_time

    private Integer weeksMark; //周几的标记 weeks_mark
}
