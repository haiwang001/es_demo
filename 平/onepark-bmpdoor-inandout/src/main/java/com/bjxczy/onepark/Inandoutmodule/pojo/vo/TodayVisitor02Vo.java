package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/26 18:09
 */
@Data
public class TodayVisitor02Vo {
    private Integer visit , noShow;

    public TodayVisitor02Vo(Integer visit, Integer noShow) {
        this.visit = visit;
        this.noShow = noShow;
    }
}
