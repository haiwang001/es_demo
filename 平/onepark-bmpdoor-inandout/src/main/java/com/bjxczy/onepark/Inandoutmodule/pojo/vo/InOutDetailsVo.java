package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/15 14:14
 */
@Data
public class InOutDetailsVo {
    private Integer index;// 索引
    private String carNumber;// 名称
    private String ParkName;// 入园 / 出园
    private String type;// 楼层
    private String time;// 时间

    public InOutDetailsVo(String carNumber, String parkName, String type, String time, Integer index) {
        if ("".equals(carNumber)) {
            this.carNumber = "未同步";
        } else {
            this.carNumber = carNumber;
        }
        this.ParkName = parkName;
        this.type = type;
        this.time = time;
        this.index = index;
    }
}
