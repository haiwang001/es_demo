package com.bjxczy.onepark.checkattendancemodule.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.checkattendancemodule.entity.StaffCheckAttendance;
import com.bjxczy.onepark.checkattendancemodule.pojo.dto.StaffCheckAttendanceDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/4/13 16:17
 */
@Mapper
public interface StaffCheckAttendanceMapper extends BaseMapper<StaffCheckAttendance> {

    @Select("select a.*, b.label isBeLateLabel, c.label isLeaveEarlyLabel\n" +
            "from k_staff_check_attendance a,\n" +
            "     voucher_dict b,\n" +
            "     voucher_dict c\n" +
            "where a.del_flag = 0\n" +
            "  and a.is_be_late = b.value\n" +
            "  and b.group_code = 'is_be_late'\n" +
            "  and a.is_leave_early = c.value\n" +
            "  and c.group_code = 'is_leave_early'\n" +
            "  and if(#{dto.rulesName} is not null, a.rules_name like concat('%', #{dto.rulesName}, '%'), true)\n" +
            "  and if(#{dto.deptName} is not null, a.dept_name like concat('%', #{dto.deptName}, '%'), true)\n" +
            "  and if(#{dto.month} is not null, a.create_time like concat('%', #{dto.month}, '%'), true)")
    List<StaffCheckAttendance> selectByDto(@Param("dto") StaffCheckAttendanceDto dto);


    @Select("select a.*, b.label isBeLateLabel, c.label isLeaveEarlyLabel\n" +
            "from k_staff_check_attendance a\n" +
            "    left join  voucher_dict b on a.is_be_late = b.value and b.group_code = 'is_be_late'\n" +
            "    left join  voucher_dict c on a.is_leave_early = c.value and c.group_code = 'is_leave_early'\n" +
            "where a.del_flag = 0\n" +
            "  and a.employee_mark = #{employeeMark}\n" +
            "  and if(#{month} is not null, a.create_time like concat('%', #{month}, '%'), true)\n" +
            "group by date_format(a.create_time,\"%Y%m%d\")")
    List<StaffCheckAttendance> selectByEmployee(@Param("employeeMark") String employeeMark, @Param("month") String month);
}
