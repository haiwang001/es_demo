package com.bjxczy.onepark.common.utils.file;


import cn.hutool.http.HttpUtil;
import lombok.SneakyThrows;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.http.entity.ContentType;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/*
 *@Author wlw
 *@Date 2023/6/30 11:55
 */
public class ImgUtils {

    @SneakyThrows
    public static String fileUrlToBase64String(String fileurl) {
        URL url = new URL(fileurl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(5 * 1000);
        InputStream in = connection.getInputStream();
        byte[] bytes = readInputStream(in);
        return Base64.encodeBase64String(bytes);
    }

    private static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }

    @SneakyThrows
    public static MultipartFile fileUrlToMultipartFile(String fileurl) {
        File destFile = File.createTempFile(Math.random() + "", ".jpg");
        HttpUtil.downloadFile(fileurl, destFile);
        return fileToMultipartFile(destFile);
    }

    @SneakyThrows
    public static MultipartFile base64ToFile(String base64Str) {
        File destFile = File.createTempFile(Math.random() + "", ".jpg");
        try (FileOutputStream fos = new FileOutputStream(destFile)) {
            byte[] decoder= DatatypeConverter.parseBase64Binary(base64Str);
            fos.write(decoder);
        }
        return fileToMultipartFile(destFile);
    }



    @SneakyThrows
    public static File imageUrlToFile(String prefix, String suffix, String imageUrl) {
        File file = File.createTempFile(prefix, suffix);
        HttpUtil.downloadFile(imageUrl, file);
        return file;
    }

    public static MultipartFile fileToMultipartFile(File file) {
        return new CommonsMultipartFile(createFileItem(file));
    }

    private static FileItem createFileItem(File file) {
        FileItemFactory factory = new DiskFileItemFactory(16, null);
        FileItem item = factory.createItem("textField", "text/plain", true, file.getName());
        int bytesRead = 0;
        byte[] buffer = new byte[8192];
        try {
            FileInputStream fis = new FileInputStream(file);
            OutputStream os = item.getOutputStream();
            while ((bytesRead = fis.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return item;
    }


}
