package com.bjxczy.onepark.checkattendancemodule.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.checkattendancemodule.pojo.dto.StaffCheckAttendanceDto;
import com.bjxczy.onepark.checkattendancemodule.service.StaffCheckAttendanceService;
import com.bjxczy.onepark.common.resp.R;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/*
 *@Author wlw
 *@Date 2023/4/13 15:57
 */
@Log4j2
@ApiResourceController
@RequestMapping("/attendanceSummary")
public class StaffCheckAttendanceController {
    @Resource
    private StaffCheckAttendanceService service;

    @GetMapping(value = "/list", name = "考勤记录列表")
    public R<?> getListPage(StaffCheckAttendanceDto dto) {
        return R.success(service.getListPage(dto));
    }


    @GetMapping(value = "/details", name = "考勤详情")
    public R<?> details(StaffCheckAttendanceDto dto) {
        return R.success(service.details(dto));
    }


    @GetMapping(value = "/download", name = "考勤详情导出")
    public void download(HttpServletResponse response, StaffCheckAttendanceDto dto) {
        service.download(response,dto);
    }
}
