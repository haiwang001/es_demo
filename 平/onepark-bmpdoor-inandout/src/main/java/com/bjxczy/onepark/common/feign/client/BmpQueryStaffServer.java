package com.bjxczy.onepark.common.feign.client;

import com.bjxczy.core.feign.client.organization.BaseStaffFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("organization")
public interface BmpQueryStaffServer extends BaseStaffFeignClient {}
