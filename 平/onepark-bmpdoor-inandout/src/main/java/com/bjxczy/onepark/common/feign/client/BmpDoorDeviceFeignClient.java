package com.bjxczy.onepark.common.feign.client;

import org.springframework.cloud.openfeign.FeignClient;

import com.bjxczy.core.feign.client.door.BaseDoorDeviceFeignClient;

@FeignClient("bmpvoucher")
public interface BmpDoorDeviceFeignClient extends BaseDoorDeviceFeignClient {}
