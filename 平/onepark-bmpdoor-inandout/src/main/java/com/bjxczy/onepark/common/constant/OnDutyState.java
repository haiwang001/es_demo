package com.bjxczy.onepark.common.constant;

public enum OnDutyState {


    MORNING_NORMAL(1, "正常"),
    MORNING_ERROR(2, "迟到"),
    EVENING_NORMAL(1, "正常"),
    EVENING_ERROR(2, "早退");
    private int code;
    private String msg;

    OnDutyState(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
