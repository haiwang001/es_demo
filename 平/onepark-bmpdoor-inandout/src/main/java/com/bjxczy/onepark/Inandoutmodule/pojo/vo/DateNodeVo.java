package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;


@Data
public class DateNodeVo {
    private String x;
    private Long y;

    public DateNodeVo(String x, Long y) {
        this.x = x;
        this.y = y;
    }
}
