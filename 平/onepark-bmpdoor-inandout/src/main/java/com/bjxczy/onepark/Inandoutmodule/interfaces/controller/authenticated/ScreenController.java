package com.bjxczy.onepark.Inandoutmodule.interfaces.controller.authenticated;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.Inandoutmodule.service.ScreenService;
import com.bjxczy.onepark.common.resp.R;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/*
 *@Author   人员大屏数据展示
 */
@Log4j2
@ApiResourceController
@RequestMapping("/screen")
public class ScreenController {
    @Resource
    private ScreenService service;


    /**
     * 通行态势 - 总人数
     *
     * @return
     */
    @GetMapping(value = "/headCount", name = "[IOC] - 总人数")
    public R<?> headcount(String day, String id) {
        return R.success(service.headcount(day,id));
    }


    /**
     * 通行态势 - 出入统计
     *
     * @return
     */
    @GetMapping(value = "/entryAndExitStatistics", name = "[IOC] - 出入统计")
    public R<?> entryAndExitStatistics(String day, String id) {
        return R.success(service.entryAndExitStatistics(day,id));
    }

    /**
     * 通行态势 - 今日访客
     *
     * @return
     */
    @GetMapping(value = "/todayVisitor/v2", name = "[IOC] - 今日访客v2")
    public R<?> todayVisitor02(String day, String id) {
        return R.success(service.todayVisitor02(day,id));
    }

    /**
     * 通行态势 - 门禁通行
     *
     * @return
     */
    @GetMapping(value = "/goThroughEntranceGuard", name = "[IOC] - 门禁通行")
    public R<?> goThroughEntranceGuard(String day, String id) {
        return R.success(service.goThroughEntranceGuard(day,id));
    }


    /**
     * 通行态势 - 访客滞留
     *
     * @return
     */
    @GetMapping(value = "/retention/v2", name = "[IOC] - 访客滞留v2")
    public R<?> retention02(String day, String id) {
        return R.success(service.retention02(day,id));
    }
}
