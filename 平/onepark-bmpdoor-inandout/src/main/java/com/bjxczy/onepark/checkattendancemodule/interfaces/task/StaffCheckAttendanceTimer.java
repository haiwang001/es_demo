//package com.bjxczy.onepark.checkattendancemodule.interfaces.task;
//
//
//import com.bjxczy.onepark.Inandoutmodule.entity.RecordDoor;
//import com.bjxczy.onepark.Inandoutmodule.repository.EsRecordRepository;
//import com.bjxczy.onepark.checkattendancemodule.entity.AttendanceRules;
//import com.bjxczy.onepark.checkattendancemodule.entity.RulesAndStaff;
//import com.bjxczy.onepark.checkattendancemodule.entity.StaffCheckAttendance;
//import com.bjxczy.onepark.checkattendancemodule.mapper.AttendanceRulesMapper;
//import com.bjxczy.onepark.checkattendancemodule.mapper.RulesAndStaffMapper;
//import com.bjxczy.onepark.checkattendancemodule.mapper.StaffCheckAttendanceMapper;
//import com.bjxczy.onepark.common.constant.OnDutyState;
//import com.bjxczy.onepark.common.feign.client.QueryStaffServer;
//import com.bjxczy.onepark.common.model.organization.StaffInformation;
//import com.bjxczy.onepark.common.utils.DateUtil;
//import lombok.extern.log4j.Log4j2;
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import javax.annotation.Resource;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Date;
//import java.util.List;
//
//
///*
// */
//@Configuration
//@EnableScheduling
//@Log4j2
//public class StaffCheckAttendanceTimer {
//    @Resource
//    private RulesAndStaffMapper rulesAndStaffMapper;
//
//    @Resource
//    private EsRecordRepository repository;
//    @Resource
//    private AttendanceRulesMapper rulesMapper;
//    @Resource
//    private StaffCheckAttendanceMapper staffCheckAttendanceMapper;
//    @Resource
//    private QueryStaffServer staffServer;
//
//    /**
//     *
//     */
//    // @Scheduled(fixedDelay = 1000 * 60 * 2)
//    @Scheduled(cron = "0 55 23 * * ?") //每天晚上凌晨触发
//    public void synchronizationHikGuardInfo() {
//        log.info("考勤日志开始------------------------------------------");
//        // 获取所有需要考勤的员工
//        List<RulesAndStaff> list = rulesAndStaffMapper.selectByMap(null);
//        // 获取今日 时间  yyyy-mm-dd 该员工 获取今日的 所有数据
//        String yyyymmdd = DateUtil.getYearaAndMonthAndDay();
//        log.info("获取考勤时间-------------时间: " + yyyymmdd);
//        list.forEach(el -> {
//            StaffInformation byId = staffServer.findById(Integer.parseInt(el.getStaffId()));
//            log.info("需要考勤的员工-------------名称: " + byId.getFldName());
//            // es 使用员工编号
//            ArrayList<RecordDoor> record = getRecord(yyyymmdd, byId.getEmployeeNo());
//            log.info("需要考勤的员工的通行记录-------------记录: " + record);
//            if (record.size() > 0) {
//                log.info("开始封装考勤信息-------------记录: " + record);
//                AttendanceRules rules = rulesMapper.selectByStaffId(el.getStaffId());
//                Integer early = getHourNum(record).get(0);    // 小时 最下值 0索引
//                Integer evening = getHourNum(record).get(record.size() - 1); //小时 最大值 最长-1
//                List<RecordDoor> earlydoor = getRecordDoor(record, early); // 获取最早小时的所有记录
//                List<RecordDoor> eveningdoor = getRecordDoor(record, evening); // 获取最大时间的所有记录
//                Integer Points1 = getPointsNum(earlydoor).get(0);// 获取分钟 最早的分钟
//                Integer Points2 = getPointsNum(eveningdoor).get(eveningdoor.size() - 1);// 获取最晚的分钟
//                RecordDoor earliestRecord = getRecordDoor(earlydoor, early, Points1); //最早记录
//                RecordDoor lateRecord = getRecordDoor(eveningdoor, evening, Points2); //最晚记录
//                log.info(record.get(0).getStaffName() + " 的考勤  员工id  " + el.getStaffId());
//                log.info("最早记录时间  " + earliestRecord.getStartTime());
//                log.info("最晚记录时间  " + lateRecord.getStartTime());
//                log.info("考勤规则时间  " + rules.getOfficeHours() + " - " + rules.getOffDutyTime());
//                Integer LateState = getLateState(earliestRecord, rules);
//                Integer LeaveEarly = getLeaveEarly(lateRecord, rules);
//                setStaffCheckAttendance(rules, lateRecord, earliestRecord, LateState, LeaveEarly);
//            }
//        });
//    }
//
//    /**
//     * @param rules          考勤规则
//     * @param lateRecord     最早考勤记录
//     * @param earliestRecord 最晚记录
//     * @param lateState      最早状态
//     * @param leaveEarly     最晚状态
//     */
//    private void setStaffCheckAttendance(AttendanceRules rules, RecordDoor lateRecord, RecordDoor earliestRecord, Integer lateState, Integer leaveEarly) {
//        // 考勤记录
//        StaffCheckAttendance attendance = new StaffCheckAttendance();
//        attendance.setRulesName(rules.getRulesName());
//        attendance.setRulesId(rules.getRulesId());
//        attendance.setCreateTime(new Date());
//        attendance.setName(lateRecord.getStaffName());
//        attendance.setDeptId(lateRecord.getDeptId());
//        attendance.setDeptName(lateRecord.getDeptName());
//        attendance.setDelFlag(0);
//        attendance.setEmployeeMark(lateRecord.getMisCode());
//        attendance.setWeeksMark(-1);
//        attendance.setLatestTime(earliestRecord.getStartTime());
//        attendance.setEarliestTime(lateRecord.getStartTime());
//        attendance.setIsBeLate(lateState);
//        attendance.setIsLeaveEarly(leaveEarly);
//        log.info("考勤记录入库-------------记录: " + attendance);
//        staffCheckAttendanceMapper.insert(attendance);
//    }
//
//    /**
//     * @param lateRecord 考勤记录
//     * @param rules      考勤规则
//     * @return 考勤状态
//     */
//    private Integer getLeaveEarly(RecordDoor lateRecord, AttendanceRules rules) {
//        String[] split1 = rules.getOffDutyTime().split(":");
//        //  小时 最晚时间大于规则时间
//        if (Integer.parseInt(lateRecord.getHour()) > Integer.parseInt(split1[0])) {
//            // 提前到来
//            return OnDutyState.EVENING_NORMAL.getCode();
//        } else { // 分钟比规则小 表示提前到来
//            if (Integer.parseInt(lateRecord.getPoints()) > Integer.parseInt(split1[1])) {
//                return OnDutyState.EVENING_NORMAL.getCode();
//            } else {
//                return OnDutyState.EVENING_ERROR.getCode();
//            }
//        }
//    }
//
//    /**
//     * @param lateRecord 考勤记录
//     * @param rules      考勤规则
//     * @return 考勤状态
//     */
//    private Integer getLateState(RecordDoor earliestRecord, AttendanceRules rules) {
//        String[] split = rules.getOfficeHours().split(":");
//        if (Integer.parseInt(earliestRecord.getHour()) < Integer.parseInt(split[0])) {
//            // 提前到来
//            return OnDutyState.MORNING_NORMAL.getCode();
//        } else { // 分钟比规则小 表示提前到来
//            if (Integer.parseInt(earliestRecord.getPoints()) < Integer.parseInt(split[1])) {
//                return OnDutyState.MORNING_NORMAL.getCode();
//            } else {
//                return OnDutyState.MORNING_ERROR.getCode();
//            }
//        }
//    }
//
//    /**
//     * @param earlydoor 全量考勤记录列表
//     * @param early     小时
//     * @param Points    分钟
//     * @return 找到某小时某分钟记录
//     */
//    private RecordDoor getRecordDoor(List<RecordDoor> earlydoor, Integer early, Integer Points) {
//        for (RecordDoor el : earlydoor) {
//            if (el.getHour().equals(early.toString()) && el.getPoints().equals(Points.toString())) {
//                return el;
//            }
//        }
//        return null;
//    }
//
//
//    private List<RecordDoor> getRecordDoor(ArrayList<RecordDoor> record, Integer Hour) {
//        List<RecordDoor> list = new ArrayList<>();
//        for (RecordDoor door : record) {
//            if (door.getHour().equals(Hour.toString())) {
//                list.add(door);
//            }
//        }
//        return list;
//    }
//
//    private ArrayList<RecordDoor> getRecord(String yyyymmdd, String staffId) {
//        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();            //查询对象
//        queryBuilder.must(QueryBuilders.matchQuery("misCode", staffId))
//                    .must(QueryBuilders.matchQuery("YearsMonthDay", yyyymmdd));
//        // 查询 记录
//        Iterable<RecordDoor> search = repository.search(queryBuilder);
//        ArrayList<RecordDoor> list = new ArrayList<>();
//        search.forEach(list::add);
//        return list;
//    }
//
//    private ArrayList<Integer> getHourNum(Iterable<RecordDoor> search) {
//        ArrayList<Integer> arrayList = new ArrayList<>();
//        search.forEach(el -> arrayList.add(Integer.parseInt(el.getHour())));
//        Collections.sort(arrayList);
//        return arrayList;
//    }
//
//    private ArrayList<Integer> getPointsNum(Iterable<RecordDoor> search) {
//        ArrayList<Integer> arrayList = new ArrayList<>();
//        search.forEach(el -> arrayList.add(Integer.parseInt(el.getPoints())));
//        Collections.sort(arrayList);
//        return arrayList;
//    }
//}
