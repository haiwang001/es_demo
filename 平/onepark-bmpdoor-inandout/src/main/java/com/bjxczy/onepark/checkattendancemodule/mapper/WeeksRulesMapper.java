package com.bjxczy.onepark.checkattendancemodule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.checkattendancemodule.entity.WeeksRules;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


import java.util.List;

@Mapper
public interface WeeksRulesMapper extends BaseMapper<WeeksRules> {

    @Select("select *\n" +
            "from k_weeks_rules where rules_id = #{rulesId}")
    List<WeeksRules> selectByRulesId(@Param("rulesId") String rulesId);
    @Select("select *\n" +
            "from k_weeks_rules where rules_id = #{rulesId} and is_check = true")
    List<WeeksRules> selectByRulesIdAndTrue(@Param("rulesId")String rulesId);
}
