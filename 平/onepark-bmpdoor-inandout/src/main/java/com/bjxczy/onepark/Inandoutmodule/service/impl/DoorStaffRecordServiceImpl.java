package com.bjxczy.onepark.Inandoutmodule.service.impl;

import cn.hutool.core.date.DateTime;
import com.alibaba.fastjson.JSON;
import com.bjxczy.core.data.core.IDataRepository;
import com.bjxczy.core.data.util.SqlUtils;
import com.bjxczy.core.rabbitmq.event.door.InOrOutRecordPayload;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.ioc.IOCMessageService;
import com.bjxczy.onepark.Inandoutmodule.entity.DoorStaffRecord;
import com.bjxczy.onepark.Inandoutmodule.pojo.dto.DoorStaffRecordQueryDTO;
import com.bjxczy.onepark.Inandoutmodule.service.DoorStaffRecordService;
import com.bjxczy.onepark.common.constant.SysConst;
import com.bjxczy.onepark.common.feign.client.BmpDoorDeviceFeignClient;
import com.bjxczy.onepark.common.feign.client.BmpVisitorFeignClient;
import com.bjxczy.onepark.common.feign.client.BmpVoucherFeignClient;
import com.bjxczy.onepark.common.feign.client.BmpQueryStaffServer;
import com.bjxczy.onepark.common.model.common.DictInformation;
import com.bjxczy.onepark.common.model.door.DoorDeviceInformation;
import com.bjxczy.onepark.common.model.ioc.PushIOCMessageInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.visitor.VisitorInformation;
import com.bjxczy.onepark.common.model.voucher.StaffVoucherInfo;
import com.bjxczy.onepark.common.service.TemplateSqlService;
import com.bjxczy.onepark.common.utils.ConvertUtils;
import com.bjxczy.onepark.common.utils.ExcelUtils;
import com.bjxczy.onepark.common.utils.ListPageUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @Auther: qinyuan
 * @Date: 2023/7/3 0003 17:12
 * @Description: 门禁进出service
 * @Version 1.0.0
 */
@Service
@Slf4j
public class DoorStaffRecordServiceImpl implements DoorStaffRecordService {

    @Autowired
    @Qualifier("doorAcrossRecordRepository")
    public IDataRepository<DoorStaffRecord, String> repository;
    @Resource
    private IOCMessageService iOCMessageService;
    @Autowired
    private BmpVoucherFeignClient bmpVoucherFeignClient;
    @Autowired
    private BmpDoorDeviceFeignClient doorDeviceClient;
    @Resource
    private BmpVisitorFeignClient visitorFeignClient;
    @Autowired
    private BmpQueryStaffServer queryStaffServer;
    @Autowired
    private TemplateSqlService templateSqlService;
    @Autowired
    private DictService dictService;

    @Value("${dict.comeFrom}")
    private String comeFrom;
    @Value("${door-deivce-type.hik}")
    private String hikDeviceType;
    @Value("${door-deivce-type.hw}")
    private String hwDeviceType;

    @Override
    public void add(InOrOutRecordPayload payload) {
        DoorStaffRecord doorStaffRecord = this.covertPayloadToRecord(payload);
        if (doorStaffRecord != null) {
            List<DoorStaffRecord> records = this.isRepetition(doorStaffRecord.getOpenTime(), doorStaffRecord.getEmployeeNo(), doorStaffRecord.getDeviceCode());
            //不重复则添加否则更新
            if (CollectionUtils.isEmpty(records)){
                repository.save(doorStaffRecord);
            }else {
                for (DoorStaffRecord record : records) {
                    String oldId = record.getId();
                    BeanUtils.copyProperties(doorStaffRecord,record);
                    record.setId(oldId);
                    ConvertUtils.nullStringToplaceholder(record);
                    repository.save(record);
                }
            }
        }
    }

    public void pushIocMsg(DoorStaffRecord doorStaffRecord) {
        if (doorStaffRecord != null) {
            PushIOCMessageInformation information = new PushIOCMessageInformation();
            information.setIocItemKey(SysConst.PERSONNEL_FACE_KEY);
            information.setTitle("门禁识别");
            information.setMessageType("info");
            information.setAutoClose(true);
            HashMap<String, Object> extra = new HashMap<>();
            extra.put("name", doorStaffRecord.getStaffName() == null ? "???" : doorStaffRecord.getStaffName());
            extra.put("mobile", doorStaffRecord.getMobile() == null ? "???" : doorStaffRecord.getMobile());
            String s = covertDict(comeFrom, doorStaffRecord.getComeFrom());
            extra.put("type", s);
            String face = null;
            String card = null;
            if (doorStaffRecord.getOpenType().equals("0")){//为人脸时
                face = doorStaffRecord.getVoucherContent();
            }else if (doorStaffRecord.getOpenType().equals("1")){
                card = doorStaffRecord.getVoucherContent();
            }
            extra.put("face", face);
            extra.put("card",card);
            extra.put("time", DateTime.of(doorStaffRecord.getOpenTime()).toString("yyyy-MM-dd HH:mm:ss"));
            extra.put("deviceName", doorStaffRecord.getDeviceName());
            extra.put("inOrOut", doorStaffRecord.getDirection());
            extra.put("position", doorStaffRecord.getSpaceName());
            information.setExtra(extra);
            iOCMessageService.pushMessage(information);
        }
    }

    @Override
    public Object queryPage(DoorStaffRecordQueryDTO dto, UserInformation sysUser) {
        Object obj = ListPageUtil.buildPage(this.selectList(dto), dto.getPageNum(), dto.getPageSize());
        return obj;
    }

    @Override
    public Boolean exportExcel(HttpServletResponse response, String fileName, DoorStaffRecordQueryDTO dto, UserInformation sysUser) {
        try {
            if ("".equals(fileName) || fileName == null)
                fileName = "通行记录信息";
            ExcelUtils.export(response,
                    fileName.trim(),
                    fileName.trim(),
                    this.selectList(dto), DoorStaffRecord.class);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private List<DoorStaffRecord> selectList(DoorStaffRecordQueryDTO dto) {
        List<DoorStaffRecord> records = new ArrayList<>();
        List<Integer> deptIds = new ArrayList<>();
        if (dto.getDeptId()!=null&&dto.getDeptId()!=0){
            deptIds = queryStaffServer.listOrgChildrenId(dto.getDeptId());
        }
        Map<String,Object> params = new HashMap<>();
        params.put("unifyParams",dto.getUnifyParams());
        params.put("spaceName",dto.getFloorName());
        params.put("cardNo",dto.getCardNo());
        params.put("deviceName",dto.getDeviceName());
        params.put("openType",dto.getOpenType());
        params.put("direction",dto.getDirection());
        if (StringUtils.isNotEmpty(dto.getOpenStartTime())){
            params.put("openStartTime",DateTime.of(dto.getOpenStartTime(), "yyyy-MM-dd HH:mm:ss").getTime());
        }
        if (StringUtils.isNotEmpty(dto.getOpenEndTime())){
            params.put("openEndTime",DateTime.of(dto.getOpenEndTime(), "yyyy-MM-dd HH:mm:ss").getTime());
        }
        if (dto.getAreaId()!=null&&dto.getAreaId()!=0){
            params.put("areaId",String.valueOf(dto.getAreaId()));
        }else {
            params.put("areaId",null);
        }
        if (CollectionUtils.isNotEmpty(deptIds)){
            params.put("deptIds", SqlUtils.toInCondition(deptIds, id -> "'" + id + "'"));
        }else {
            params.put("deptIds",null);
        }
        List<Map<String, Object>> list = templateSqlService.list("doorStaff/queryList", params);
        //类型转换
        if (CollectionUtils.isNotEmpty(list)){
            records = list.stream().map(x->{
                DoorStaffRecord doorStaffRecord = JSON.parseObject(JSON.toJSONString(x), DoorStaffRecord.class);
                return doorStaffRecord;
            }).collect(Collectors.toList());
        }
        //数据处理
        if (CollectionUtils.isNotEmpty(records)){
            for (DoorStaffRecord record : records) {
                ConvertUtils.placeholderToNullString(record);
                if (StringUtils.isEmpty(record.getOpenTimeText())&&record.getOpenTime()!=null&&record.getOpenTime()!=0){
                    record.setOpenTimeText(DateTime.of(record.getOpenTime()).toString("yyyy-MM-dd HH:mm:ss"));
                }
                String s = covertDict(comeFrom, record.getComeFrom());
                record.setComeFrom(s);
                record.setOpenTimeDay(DateTime.of(record.getOpenTime()).toString("yyyy-MM-dd"));
            }
        }
        return records;
    }

    /**
     * @return DoorStaffRecord
     * @Author qinyuan
     * @Description 将mq消息转换为通行记录实体
     * @Date 17:45 2023/7/3 0003
     * @Param payload
     **/
    private DoorStaffRecord covertPayloadToRecord(InOrOutRecordPayload payload) {
        DoorStaffRecord doorStaffRecord = new DoorStaffRecord();
        //设备id判空
        if (StringUtils.isEmpty(payload.getDeviceCode())) {
            return null;
        }
        //获取设备信息
        DoorDeviceInformation device = doorDeviceClient.getDoorInformation(payload.getDeviceCode());
        if (device == null || StringUtils.isEmpty(device.getDeviceType())) {
            return null;
        }
        //设备信息填充
        doorStaffRecord.setDeviceCode(device.getDeviceCode());
        doorStaffRecord.setDeviceName(device.getDeviceName());
        doorStaffRecord.setAreaId(device.getAreaId());
        doorStaffRecord.setTenantId(device.getTenantId());
        doorStaffRecord.setSpaceId(device.getSpaceId());
        doorStaffRecord.setSpaceName(device.getLocation());
        doorStaffRecord.setDirection(device.getDirection() != null && device.getDirection() == 1 ? "进" : "出");
        //人员信息获取
        StaffVoucherInfo staff = null;
        int intId = 0;
        long longId = 0;
        if (StringUtils.isNotEmpty(payload.getBusinessId())) {
            String trim = payload.getBusinessId().trim();
            intId = NumberUtils.toInt(trim);
            longId = NumberUtils.toLong(trim);
        }
        if (intId!=0){
            staff = bmpVoucherFeignClient.getByStaffId(intId);
        }
        //人员信息不为空，则填充记录实体相关信息
        if (staff != null&&staff.getStaffId()!=null&&staff.getStaffId()!=0) {
            doorStaffRecord.setEmployeeNo(staff.getEmployeeNo());
            doorStaffRecord.setStaffName(staff.getStaffName());
            doorStaffRecord.setMobile(staff.getMobile());
            if (staff.getDeptId()!=null&&staff.getDeptId()!=0){
                doorStaffRecord.setDeptId(staff.getDeptId());
            }
            doorStaffRecord.setDeptName(staff.getDeptName());
            if (staff.getRankId()!=null&&staff.getRankId()!=0){
                doorStaffRecord.setRankId(String.valueOf(staff.getRankId()));
            }
            doorStaffRecord.setRankName(staff.getRankName());
            doorStaffRecord.setComeFrom("1");
        }
        VisitorInformation visitor = null;
        if (longId!=0) {
            visitor = visitorFeignClient.selectVisitorByMobile(payload.getBusinessId());
        }
        //访客信息不为空，则填充
        if (visitor != null&&StringUtils.isNotEmpty(visitor.getVisitorName())) {
            doorStaffRecord.setMobile(visitor.getVisitorMobile());
            doorStaffRecord.setStaffName(visitor.getVisitorName());
            doorStaffRecord.setDeptName(visitor.getVisitorWork());
            doorStaffRecord.setReceptionistId(String.valueOf(visitor.getReceptionistId()));
            doorStaffRecord.setReceptionistName(visitor.getStaffName());
            doorStaffRecord.setReceptionistDeptName(visitor.getReceptionistDeptName());
            doorStaffRecord.setComeFrom("2");
        }
        //若访客&人员都不存在，打印日志，返回null
        if ((visitor == null||StringUtils.isEmpty(visitor.getVisitorName())) && (staff == null||staff.getStaffId()==null||staff.getStaffId()==0)) {
            log.error(String.format("海康人脸机识别人员不存在:%s", payload.toString()));
            return null;
        }
        //设置主键-32位uuid
        doorStaffRecord.setId(UUID.randomUUID().toString().replace("-", ""));
        //设置时间
        doorStaffRecord.setOpenTime(payload.getTime().getTime());
        doorStaffRecord.setOpenTimeText(DateTime.of(payload.getTime()).toString("yyyy-MM-dd HH:mm:ss"));
        doorStaffRecord.setOpenTimeHour(DateTime.of(payload.getTime()).toString("yyyy-MM-dd HH"));
        doorStaffRecord.setOpenTimeDay(DateTime.of(payload.getTime()).toString("yyyy-MM-dd"));
        //图片赋值
        doorStaffRecord.setCaptrue(payload.getEventPictrue());
        if (payload.getEventType().equals("FACE_FAIL")){
            doorStaffRecord.setOpenType("0");
            doorStaffRecord.setOpenTypeText("人脸");
            doorStaffRecord.setVoucherContent(payload.getEventPictrue());
            ConvertUtils.nullStringToplaceholder(doorStaffRecord);
            this.pushIocMsg(doorStaffRecord);
            //ren脸失败，不做存储
            return null;
        }else if (payload.getEventType().equals("FACE_SUCCESS")){
            doorStaffRecord.setOpenType("0");
            doorStaffRecord.setOpenTypeText("人脸");
            doorStaffRecord.setVoucherContent(payload.getEventPictrue());
            ConvertUtils.nullStringToplaceholder(doorStaffRecord);
            this.pushIocMsg(doorStaffRecord);
        }else if (payload.getEventType().equals("CARD_SUCCESS")){
            doorStaffRecord.setOpenType("1");
            doorStaffRecord.setOpenTypeText("门禁卡");
            doorStaffRecord.setVoucherContent(payload.getCardNum());
            ConvertUtils.nullStringToplaceholder(doorStaffRecord);
            this.pushIocMsg(doorStaffRecord);
        }

        return doorStaffRecord;
    }

    /**
    * @Author qinyuan
    * @Description 是否是重复数据
    * @Date 17:26 2023/7/4 0004
    **/
    private List<DoorStaffRecord> isRepetition(Long openTime,String employeeNo,String deviceCode){
        List<DoorStaffRecord> records = new ArrayList<>();
        Map<String,Object> params = new HashMap<>();
        params.put("openTime",openTime);
        params.put("employeeNo",employeeNo);
        params.put("deviceCode",deviceCode);
        List<Map<String, Object>> list = null;
        try {
            list = templateSqlService.list("doorStaff/queryByTimeAndEmployeeNoAndDeviceCode", params);
        } catch (Exception e) {
            log.error("查询园区通信记录失败");
        }
        //类型转换
        if (CollectionUtils.isNotEmpty(list)){
            records = list.stream().map(x->{
                DoorStaffRecord doorStaffRecord = JSON.parseObject(JSON.toJSONString(x), DoorStaffRecord.class);
                return doorStaffRecord;
            }).collect(Collectors.toList());
        }
        return records;
    }

    private String covertDict(String type,String value){
        String ret = null;
        //来源
        List<DictInformation> dfDict = dictService.listDict(type);
        if (CollectionUtils.isNotEmpty(dfDict)&&StringUtils.isNotEmpty(value)){
            for (DictInformation dictInformation : dfDict) {
                if (value.equals(dictInformation.getValue())){
                    ret = dictInformation.getLabel();
                }
            }
        }
        return ret;
    }
}
