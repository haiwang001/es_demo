package com.bjxczy.onepark.Inandoutmodule.service;

import com.bjxczy.onepark.Inandoutmodule.pojo.vo.HeadcountVo;

public interface ScreenService {

    Object todayVisitor02(String day,String id);

    HeadcountVo headcount(String day,String id);

    Object entryAndExitStatistics(String day,String id);

    Object goThroughEntranceGuard(String day,String id);

    Object retention02(String day,String id);
}
