package com.bjxczy.onepark.common.feign.client;

import com.bjxczy.core.feign.client.voucher.BaseVoucherFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Auther: Administrator
 * @Date: 2023/7/5 0005 13:43
 * @Description: BmpVoucherFeignClient
 * @Version 1.0.0
 */
@FeignClient("bmpvoucher")
public interface BmpVoucherFeignClient extends BaseVoucherFeignClient {
}
