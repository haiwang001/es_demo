package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/19 14:01
 */
@Data
public class ParkAreaVo {
    private List<DateNodeVo> In;
    private List<DateNodeVo> Out;
    private int staff;
    private int visitor;
    private int proprietor;

    public ParkAreaVo(List<DateNodeVo> in, List<DateNodeVo> out, int staff, int visitor, int proprietor) {
        In = in;
        Out = out;
        this.staff = staff;
        this.visitor = visitor;
        this.proprietor = proprietor;
    }
}
