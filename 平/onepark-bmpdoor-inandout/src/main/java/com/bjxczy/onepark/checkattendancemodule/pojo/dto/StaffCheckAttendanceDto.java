package com.bjxczy.onepark.checkattendancemodule.pojo.dto;


import com.bjxczy.onepark.Inandoutmodule.pojo.dto.BasePageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *@Author wlw
 *@Date 2023/4/13 16:13
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StaffCheckAttendanceDto extends BasePageDTO {
    private String deptId; // 员工部门id dept_id
    private String deptName; // 员工部门id dept_id
    private String rulesName; // 规则名称 rules_name
    private String month; // 月份
    // 考勤详情点击传递
    private String employeeMark;  // 员工标记 employee_mark

}
