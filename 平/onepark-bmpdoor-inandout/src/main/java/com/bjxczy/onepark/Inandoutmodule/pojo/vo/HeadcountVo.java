package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/26 16:10
 */
@Data
public class HeadcountVo {
    private Integer visitor, proprietor, staff;

    public HeadcountVo(Integer visitor, Integer proprietor, Integer staff) {
        this.visitor = visitor;
        this.proprietor = proprietor;
        this.staff = staff;
    }
}
