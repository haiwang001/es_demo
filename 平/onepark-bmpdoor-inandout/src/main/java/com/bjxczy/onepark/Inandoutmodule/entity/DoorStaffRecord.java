package com.bjxczy.onepark.Inandoutmodule.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.bjxczy.core.data.annotation.ModelId;
import com.bjxczy.core.data.annotation.ModelName;
import lombok.Data;

/**
 * @Auther: Administrator
 * @Date: 2023/6/25 0025 11:32
 * @Description: doorStaffRecord
 * @Version 1.0.0
 */
@Data
@ModelName("door_staff_record")
public class DoorStaffRecord {
    /**
    * 描述：uuid
    * 是否必填：
    **/
    @ModelId
    private String id;
    /**
     * 描述：员工编号
     * 是否必填：
     **/
    @Excel(name = "员工编号", width = 18, replace = {"_null"})
    private String employeeNo;
    /**
     * 描述：姓名
     * 是否必填：
     **/
    @Excel(name = "姓名", width = 18, replace = {"_null"})
    private String staffName;
    /**
     * 描述：手机号
     * 是否必填：
     **/
    @Excel(name = "手机号", width = 18, replace = {"_null"})
    private String mobile;
    /**
     * 描述：部门ID
     * 是否必填：
     **/
    private Integer deptId;
    /**
     * 描述：部门名称：XXX公司/XX部门/XX；访客单位
     * 是否必填：
     **/
    @Excel(name = "部门", width = 18, replace = {"_null"})
    private String deptName;
    /**
     * 描述：职级ID
     * 是否必填：
     **/
    private String rankId;
    /**
     * 描述：职级名称
     * 是否必填：
     **/
    private String rankName;
    /**
     * 描述：人员类型：字典：comeFrom
     * 是否必填：
     **/
    @Excel(name = "人员类型", width = 18, replace = {"_null"})
    private String comeFrom;
    /**
     * 描述：开门时间
     * 是否必填：
     **/
    private Long openTime;

    /**
     * 描述：开门时间
     * 是否必填：
     **/
    @Excel(name = "开门时间", width = 18, replace = {"_null"})
    private String openTimeText;
    /**
     * 描述：开门时间格式化：yyyy-MM-dd HH
     * 是否必填：
     **/
    private String openTimeHour;
    /**
     * 描述：开门时间格式化：yyyy-MM-dd
     * 是否必填：
     **/
    private String openTimeDay;
    /**
     * 描述：设备编码
     * 是否必填：
     **/
    private String deviceCode;
    /**
     * 描述：设备名称
     * 是否必填：
     **/
    @Excel(name = "设备名称", width = 18, replace = {"_null"})
    private String deviceName;
    /**
     * 描述：区域ID
     * 是否必填：
     **/
    //@Excel(name = "区域ID", width = 18, replace = {"_null"})
    private String areaId;
    /**
     * 描述：租户ID
     * 是否必填：
     **/
    private String tenantId;
    /**
     * 描述：空间ID
     * 是否必填：
     **/
    private String spaceId;
    /**
     * 描述：区域空间名称：XXXX/XXX/XXX
     * 是否必填：
     **/
    @Excel(name = "位置", width = 18, replace = {"_null"})
    private String spaceName;
    /**
     * 描述：开门方式：字典（openType）
     * 是否必填：
     **/
    private String openType;
    /**
     * 描述：开门方式：人脸/门禁卡/二维码
     * 是否必填：
     **/
    @Excel(name = "开门方式", width = 18, replace = {"_null"})
    private String openTypeText;
    /**
     * 描述：开门方向：进/出
     * 是否必填：
     **/
    @Excel(name = "方向", width = 18, replace = {"_null"})
    private String direction;
    /**
     * 描述：凭证：人脸URL/门禁卡号/二维码
     * 是否必填：
     **/
    @Excel(name = "凭证", width = 18, replace = {"_null"})
    private String voucherContent;
    /**
     * 描述：抓拍图：base64
     * 是否必填：
     **/
    private String captrue;
    /**
     * 描述：体温
     * 是否必填：
     **/
    @Excel(name = "体温检测", width = 18, replace = {"_null"})
    private String bodyTemperature;
    /**
     * 描述：口罩
     * 是否必填：
     **/
    @Excel(name = "口罩状态", width = 18, replace = {"_null"})
    private String wearMask;

    /**
     * 描述：被访人员工ID
     * 是否必填：
     **/
    private String receptionistId;

    /**
     * 描述：被访人姓名
     * 是否必填：
     **/
    @Excel(name = "接访人", width = 18, replace = {"_null"})
    private String receptionistName;

    /**
     * 描述：被访人部门名称：XXX/XXX/XXX
     * 是否必填：
     **/
    private String receptionistDeptName;


}
