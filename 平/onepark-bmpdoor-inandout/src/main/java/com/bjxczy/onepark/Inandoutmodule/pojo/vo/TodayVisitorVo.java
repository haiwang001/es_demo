package com.bjxczy.onepark.Inandoutmodule.pojo.vo;

import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/15 15:29
 */
@Data
public class TodayVisitorVo {
    private String visitor;
    private String person;
    private String recordTime;

    public TodayVisitorVo(String visitor, String person, String recordTime) {
        this.visitor = visitor;
        this.person = person;
        this.recordTime = recordTime;
    }
}
