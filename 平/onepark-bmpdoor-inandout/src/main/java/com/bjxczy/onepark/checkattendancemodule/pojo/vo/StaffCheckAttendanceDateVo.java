package com.bjxczy.onepark.checkattendancemodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/14 10:04
 */
@Data
public class StaffCheckAttendanceDateVo {
    private String time; // 时间
    private Integer beLate; // 迟到状态   1正常  2迟到
    private Integer advance; // 早退状态  1正常  2早退

    public StaffCheckAttendanceDateVo(String time, Integer beLate, Integer advance) {
        this.time = time;
        this.beLate = beLate;
        this.advance = advance;
    }
}
