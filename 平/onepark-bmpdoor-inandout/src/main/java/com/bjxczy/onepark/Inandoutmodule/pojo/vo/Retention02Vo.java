package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/27 14:04
 */
@Data
public class Retention02Vo {
    private List<RetentionVisitorVo> list;
    private Integer count;

    public Retention02Vo(List<RetentionVisitorVo> list, Integer count) {
        this.list = list;
        this.count = count;
    }
}
