package com.bjxczy.onepark.Inandoutmodule.interfaces.listener.subscribe;


import com.bjxczy.core.rabbitmq.supports.ParkPeopleNumberListener;
import com.bjxczy.onepark.config.ParkConfiguration;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/*
 *@Author wlw
 *@Date 2023/5/9 16:51
 */
@Log4j2
@Component
public class ParkPeopleListener extends ParkPeopleNumberListener {


    @Resource
    private ParkConfiguration park;

    /**
     * @param msg 增加在园人数
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = ADD_PEOPLE,
            exchange = @Exchange(value = EXCHANGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void addPeopleEvent(String msg, Channel channel, Message message) {

    }

    /**
     * @param msg 减少 在园人数
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = REDUCTION_PEOPLE,
            exchange = @Exchange(value = EXCHANGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void reductionPeopleEvent(String msg, Channel channel, Message message) {

    }
}
