package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/19 13:59
 */
@Data
public class TodayVisitorListVo {
    private Integer count;
    private List<TodayVisitorVo> data;

    public TodayVisitorListVo(Integer count, List<TodayVisitorVo> data) {
        this.count = count;
        this.data = data;
    }
}
