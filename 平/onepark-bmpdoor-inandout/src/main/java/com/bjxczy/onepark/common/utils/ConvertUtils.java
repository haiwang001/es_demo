package com.bjxczy.onepark.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @Auther: qinyuan
 * @Date: 2023/7/4 0004 16:52
 * @Description: 对象空值转换
 * @Version 1.0.0
 */
@Slf4j
public class ConvertUtils {

    /**
    * @Author qinyuan
    * @Description 将对象中的空字符串转换为特定占位符
    * @Date 17:09 2023/7/4 0004
    * @Param
    * @return
    **/
    public static <T> void nullStringToplaceholder(T obj){
        Class<? extends Object> clazz = obj.getClass();
        // 获取实体类的所有属性，返回Field数组
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            // 可访问私有变量
            field.setAccessible(true);
            // 获取属性类型
            String type = field.getGenericType().toString();
            // 如果type是类类型，则前面包含"class "，后面跟类名
            if ("class java.lang.String".equals(type)) {
                // 将属性的首字母大写
                String methodName = field.getName().replaceFirst(field.getName().substring(0, 1),
                        field.getName().substring(0, 1).toUpperCase());

                try {
                    Method methodGet = clazz.getMethod("get" + methodName);
                    // 调用getter方法获取属性值
                    String str = (String) methodGet.invoke(obj);
                    if (StringUtils.isEmpty(str)){
                        field.set(obj, "-");
                    }
                } catch (Exception e) {
                    log.error("emptyToNull 方法属性转换异常: {}",e.getMessage());
                }
            }
        }
    }


    /**
     * @Author qinyuan
     * @Description 对象中的特定占位符转换为空的字符串
     * @Date 17:09 2023/7/4 0004
     * @Param
     * @return
     **/
    public static <T> void placeholderToNullString(T obj){
        Class<? extends Object> clazz = obj.getClass();
        // 获取实体类的所有属性，返回Field数组
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            // 可访问私有变量
            field.setAccessible(true);
            // 获取属性类型
            String type = field.getGenericType().toString();
            // 如果type是类类型，则前面包含"class "，后面跟类名
            if ("class java.lang.String".equals(type)) {
                // 将属性的首字母大写
                String methodName = field.getName().replaceFirst(field.getName().substring(0, 1),
                        field.getName().substring(0, 1).toUpperCase());

                try {
                    Method methodGet = clazz.getMethod("get" + methodName);
                    // 调用getter方法获取属性值
                    String str = (String) methodGet.invoke(obj);
                    if (StringUtils.isNotEmpty(str)&&str.equals("-")){
                        field.set(obj, null);
                    }
                } catch (Exception e) {
                    log.error("emptyToNull 方法属性转换异常: {}",e.getMessage());
                }
            }
        }
    }

}
