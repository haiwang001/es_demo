package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/19 13:55
 */
@Data
public class RetentionVo {
    private Integer count;
    private Integer duration;
    private List<TodayVisitorVo> data;

    public RetentionVo(Integer count, Integer duration, List<TodayVisitorVo> data) {
        this.count = count;
        this.duration = duration;
        this.data = data;
    }
}
