package com.bjxczy.onepark.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zx
 * @package com.cmiot.onepark.video.hikface.util
 * @date 2021/12/14
 * @description 描述
 */
@Component
@Slf4j
public class HikfaceApiUtil {

    private static Map<String, String> config;



    public static void setConfig(Map<String, String> config) {
        HikfaceApiUtil.config = config;
    }
}
