package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;


@Data
public class RecordDoorExcelVo {
    @Excel(name = "员工编号", width = 18, replace = {"_null"})
    private String misCode;//访客姓名 Y
    @Excel(name = "姓名", width = 18, replace = {"_null"})
    private String staffName;//访客姓名 Y
}
