package com.bjxczy.onepark.common.constant;



public class SysConst {
    // 空数据展示
    public static final String DATA_NULL_SHOW = " - ";
    public static final String EXCHANGE_NAME = "bmpface_default_exchange";
    public static final String HIK_CALLBACK_CONTROLLER = "/HikCallBack";
    public static final String HIK_CALLBACK_CONTROLLER_FACE_DISCERN_SUCCEED = "/faceDiscernSucceed";
    public static final String PERSONNEL_FACE_EVENT  = "PERSONNEL_FACE_EVENT";
    public static final String PERSONNEL_FACE_KEY  = "entranceGuard";
}
