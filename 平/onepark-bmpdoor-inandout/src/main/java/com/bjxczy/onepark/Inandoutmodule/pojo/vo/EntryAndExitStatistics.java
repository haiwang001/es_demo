package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/26 16:33
 */
@Data
public class EntryAndExitStatistics {
    private HeadcountVo in;
    private HeadcountVo out;

    public EntryAndExitStatistics(HeadcountVo in, HeadcountVo out) {
        this.in = in;
        this.out = out;
    }

    public EntryAndExitStatistics() {
    }
}
