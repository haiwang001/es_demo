package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/27 9:29
 */
@Data
public class GoThroughEntranceGuardVo {
    private String name;
    private String type;
    private String direction;
    private String time; // hh:mm:ss
    private String face;
    private String mobile;
    private String deviceName;
    private String position;
    private String openingMode;

    public GoThroughEntranceGuardVo(String name, String type, String direction, String time, String face, String mobile, String deviceName, String position, String openingMode) {
        this.name = name;
        this.type = type;
        this.direction = direction;
        this.time = time;
        this.face = face;
        this.mobile = mobile;
        this.deviceName = deviceName;
        this.position = position;
        this.openingMode = openingMode;
    }
}
