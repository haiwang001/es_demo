package com.bjxczy.onepark.Inandoutmodule.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.Inandoutmodule.pojo.dto.DoorStaffRecordQueryDTO;
import com.bjxczy.onepark.Inandoutmodule.service.DoorStaffRecordService;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;


/*
 *@ClassName LogController
 *@Author 温良伟
 *@Date 2023/2/1 9:38
 *@Version 1.0
 */
@Log4j2
@ApiResourceController
@RequestMapping("/InAndOutRecord")
public class MovementRecordController {
    @Autowired
    private DoorStaffRecordService Service;

    @GetMapping(value = "/list", name = "es通行记录")
    public R getList(DoorStaffRecordQueryDTO dto,
                     @LoginUser(isFull = true) UserInformation sysUser) {
        return R.success(Service.queryPage(dto, sysUser));
    }

    /**
     * 导出
     *
     * @param fileName 导出文件名称
     * @param dto      导出条件
     */
    @GetMapping(value = "/export", name = "通行记录信息导出")
    public void exportExcel(HttpServletResponse response,
                            String fileName,
                            DoorStaffRecordQueryDTO dto, @LoginUser(isFull = true) UserInformation sysUser) {
        Service.exportExcel(response, fileName, dto, sysUser);
    }
}
