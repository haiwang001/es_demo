package com.bjxczy.onepark.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/*
 *@ClassName HikConfiguration
 *@Author 温良伟
 *@Date 2023/3/31 11:41
 *@Version 1.0
 */
@Component
@Data
@ConfigurationProperties(prefix = "park")
public class ParkConfiguration {
    private Integer onlineNumber; // 园区人数配置
    private String hikImgFile; // 海康图片服务地址
}
