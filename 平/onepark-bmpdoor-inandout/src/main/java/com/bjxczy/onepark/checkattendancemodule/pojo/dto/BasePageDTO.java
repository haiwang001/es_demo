package com.bjxczy.onepark.checkattendancemodule.pojo.dto;

import lombok.Data;

@Data
public class BasePageDTO {

    private Integer pageNum = 1;

    private Integer pageSize = 10;

}
