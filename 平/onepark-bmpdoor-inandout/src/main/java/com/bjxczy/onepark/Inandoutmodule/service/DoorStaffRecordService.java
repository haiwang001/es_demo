package com.bjxczy.onepark.Inandoutmodule.service;

import com.bjxczy.core.rabbitmq.event.door.InOrOutRecordPayload;
import com.bjxczy.onepark.Inandoutmodule.pojo.dto.DoorStaffRecordQueryDTO;
import com.bjxczy.onepark.common.model.user.UserInformation;

import javax.servlet.http.HttpServletResponse;

/**
 * @Auther: Administrator
 * @Date: 2023/7/3 0003 17:12
 * @Description: DoorStaffRecordService
 * @Version 1.0.0
 */
public interface DoorStaffRecordService {
    void add(InOrOutRecordPayload payload);

    Object queryPage(DoorStaffRecordQueryDTO dto, UserInformation sysUser);

    Boolean exportExcel(HttpServletResponse response, String fileName, DoorStaffRecordQueryDTO dto, UserInformation sysUser);
}
