package com.bjxczy.onepark.checkattendancemodule.service.impl;


import com.bjxczy.onepark.checkattendancemodule.entity.AttendanceRules;
import com.bjxczy.onepark.checkattendancemodule.entity.StaffCheckAttendance;
import com.bjxczy.onepark.checkattendancemodule.mapper.AttendanceRulesMapper;
import com.bjxczy.onepark.checkattendancemodule.mapper.StaffCheckAttendanceMapper;
import com.bjxczy.onepark.checkattendancemodule.mapper.WeeksRulesMapper;
import com.bjxczy.onepark.checkattendancemodule.pojo.dto.StaffCheckAttendanceDto;
import com.bjxczy.onepark.checkattendancemodule.pojo.vo.StaffCheckAttendanceDateVo;
import com.bjxczy.onepark.checkattendancemodule.pojo.vo.StaffCheckAttendanceExcelVo;
import com.bjxczy.onepark.checkattendancemodule.pojo.vo.StaffCheckAttendanceVo;
import com.bjxczy.onepark.checkattendancemodule.service.StaffCheckAttendanceService;
import com.bjxczy.onepark.common.constant.OnDutyState;
import com.bjxczy.onepark.common.utils.DateUtil;
import com.bjxczy.onepark.common.utils.ExcelUtils;
import com.bjxczy.onepark.common.utils.PageUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
 *@Author wlw
 *@Date 2023/4/13 16:11
 */
@Log4j2
@Service
public class StaffCheckAttendanceServiceImpl implements StaffCheckAttendanceService {

    @Resource
    private StaffCheckAttendanceMapper mapepr;
    @Resource
    private WeeksRulesMapper weeksRulesMapper;
    @Resource
    private AttendanceRulesMapper rulesMapper;

    @Override
    public Object getListPage(StaffCheckAttendanceDto dto) {
        List<StaffCheckAttendance> list = mapepr.selectByDto(dto);
        Map<String, List<StaffCheckAttendance>> map = list.stream().collect(Collectors.groupingBy(StaffCheckAttendance::getName));// 通过员工名称分组
        ArrayList<Object> dataList = new ArrayList<>(map.size());
        map.forEach((name, el) -> {
            StaffCheckAttendance attendance = el.get(0);
            dataList.add(new StaffCheckAttendanceVo(name, attendance.getEmployeeMark(), attendance.getDeptName(), attendance.getRulesName()));
        });
        return PageUtils.Page(dto, dataList);
    }

    @Override
    public void download(HttpServletResponse response, StaffCheckAttendanceDto dto) {
        if (dto.getMonth() == null || dto.getMonth().equals("")) {
            dto.setMonth(DateUtil.getYearaAndMonth());
        }
        List<StaffCheckAttendance> list = mapepr.selectByDto(dto);
        List<StaffCheckAttendanceExcelVo> arr = new ArrayList<>();
        for (StaffCheckAttendance attendance : list) {
            AttendanceRules rules = rulesMapper.selectById(attendance.getRulesId());
            arr.add(new StaffCheckAttendanceExcelVo(attendance.getName(),
                    rules.getRulesName(),
                    DateUtil.format(attendance.getCreateTime(), "yyyy-MM-dd"),
                    attendance.getLatestTime(),
                    attendance.getEarliestTime(),
                    rules.getOfficeHours() + " - " + rules.getOffDutyTime()));
        }
        try {
            ExcelUtils.export(response,
                    dto.getMonth() + "-考勤记录",
                    dto.getMonth() + "-考勤记录",
                    arr, StaffCheckAttendanceExcelVo.class);
        } catch (IOException e) {
                log.info("考勤记录导出异常" + e.getMessage());
                e.printStackTrace();
        }
    }

    @Override
    public Object details(StaffCheckAttendanceDto dto) {
        int num = 5;  // 月的上限周数 默认为5周
        if (dto.getMonth() == null || dto.getMonth().equals("")) {
            dto.setMonth(DateUtil.getYearaAndMonth());
        }
        if (dto.getMonth().startsWith("02", 5)) {
            num = 4; // 2月4周
        }
        log.info("查询日期为  " + dto.getMonth());
        List<StaffCheckAttendance> select = mapepr.selectByEmployee(dto.getEmployeeMark(), dto.getMonth());
        if(select.size()==0){
            return null;
        }
        log.info(select);
        ArrayList<StaffCheckAttendanceDateVo> arrayList = new ArrayList<>();
        //  一周上几天
        int dayNum = weeksRulesMapper.selectByRulesIdAndTrue(select.get(0).getRulesId()).size();
        // 应该出勤天数
        int totalDays = dayNum * num;
        int beLate = 0; // 迟到数
        int leaveEarly = 0; // 早退数
        for (StaffCheckAttendance item : select) {
            arrayList.add(new StaffCheckAttendanceDateVo(DateUtil.format(item.getCreateTime(), "yyyy-MM-dd"), item.getIsBeLate(), item.getIsLeaveEarly()));

            if (OnDutyState.MORNING_ERROR.getCode() == item.getIsBeLate()) {
                beLate++; // 迟到
            }
            if (OnDutyState.EVENING_ERROR.getCode() == item.getIsLeaveEarly()) {
                leaveEarly++; // 早退
            }
        }
        int maxNum = maxNum(beLate, leaveEarly);
        // 查找最大值  迟到和早退 然后用 总记录数减去最大值 结果为 正常出勤的天数
        //  应该出勤的天数 减去 所有出勤记录即为缺勤天数  负数为 加班数

        //  计算
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("beLate", beLate); //迟到
        hashMap.put("leaveEarly", leaveEarly); // 早退
        hashMap.put("normal", select.size() - maxNum);// 正常出勤
        hashMap.put("target", totalDays);// 本月应该出勤
        int i = totalDays - select.size();
        if (i < 0) {
            i = 0;
        }
        hashMap.put("deletion", i); // 缺勤
        hashMap.put("days", arrayList); // 查日期节点
        return hashMap;
    }

    private int maxNum(int beLate, int leaveEarly) {
        if (beLate > leaveEarly) {
            return beLate;
        }
        return leaveEarly;
    }


}
