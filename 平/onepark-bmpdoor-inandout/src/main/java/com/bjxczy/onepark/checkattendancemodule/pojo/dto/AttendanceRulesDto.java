package com.bjxczy.onepark.checkattendancemodule.pojo.dto;


import com.bjxczy.onepark.Inandoutmodule.pojo.dto.BasePageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *@ClassName RecordDoorDto
 *@Author 温良伟
 *@Date 2023/2/1 11:03
 *@Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AttendanceRulesDto extends BasePageDTO {
    private String rulesName; // 规则名称 rules_name

}
