package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;


@Data
public class VisitorVo {
    private String name;
    private String type;
    private String id; // 访客临时唯一id
}
