package com.bjxczy.onepark.Inandoutmodule.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/27 11:27
 */
@Data
public class RetentionVisitorVo {
    String name;
    String face;
    String mobile;
    String date;//  时间区间

    public RetentionVisitorVo(String name, String face, String mobile, String date) {
        this.name = name;
        this.face = face;
        this.mobile = mobile;
        this.date = date;
    }
}
