package com.bjxczy.onepark.checkattendancemodule.pojo.vo;



import lombok.Data;


/*
 *@Author wlw
 *@Date 2023/4/13 16:34
 */
@Data
public class StaffCheckAttendanceVo {
    private String name;  // 员工姓名
    private String employeeMark;  // 员工标记 employee_mark
    private String deptName; // 员工部门名称  dept_name
    private String rulesName; // 规则名称 rules_name

    /**
     *
     * @param name 员工姓名
     * @param employeeMark 员工标记
     * @param deptName 员工部门名称
     * @param rulesName 规则名称
     */
    public StaffCheckAttendanceVo(String name, String employeeMark, String deptName, String rulesName) {
        this.name = name;
        this.employeeMark = employeeMark;
        this.deptName = deptName;
        this.rulesName = rulesName;
    }
}
