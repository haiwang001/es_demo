package com.bjxczy.onepark.Inandoutmodule.pojo.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *@ClassName RecordDoorDto
 *@Author 温良伟
 *@Date 2023/2/1 11:03
 *@Version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RecordDoorDto extends BasePageDTO{
    private Integer pageNum;
    private Integer pageSize;


    private String deviceName; //设备名称
    private String startTime; //开门时间
    private Integer authType; //开门方式
    private Integer inOrOut; //开门方向
    private String cardNo; //凭证
    private String floorName; //楼层
    private String tenantId; //局址id
    private String deptId; //部门id
    private String merge; //多个字段匹配

    public RecordDoorDto(Integer pageNum, Integer pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public RecordDoorDto() {
    }
}
