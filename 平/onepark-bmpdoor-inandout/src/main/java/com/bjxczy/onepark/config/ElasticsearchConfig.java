package com.bjxczy.onepark.config;

import com.bjxczy.core.data.core.IDataRepository;
import com.bjxczy.core.data.core.elasticsearch.ElasticsearchRepository;
import com.bjxczy.onepark.Inandoutmodule.entity.DoorStaffRecord;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName ElasticsearchConfig
 * @Description
 * @Author qinyuan
 * @Date 2023/5/8
 * @Version 1.0
 */
@Configuration
public class ElasticsearchConfig {

    @Bean(name = "doorAcrossRecordRepository")
    public IDataRepository<DoorStaffRecord, String> doorAcrossRecordRepository() {
        return new ElasticsearchRepository<>(DoorStaffRecord.class);
    }

}
