package com.bjxczy.onepark.hanwang.sdk.security;


import java.io.IOException;

/*
 *@ClassName IVectorProvider
 *@Author 温良伟
 *@Date 2023/3/29 14:55
 *@Version 1.0
 */
public interface IVectorProvider {
    void GetVector(int paramInt, byte[] paramArrayOfbyte) throws IOException;
}

