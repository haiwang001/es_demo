package com.bjxczy.onepark.hanwang.sdk.security;


import lombok.Data;

import java.math.BigInteger;

/*
 *@ClassName ECKeyPair
 *@Author 温良伟
 *@Date 2023/3/29 14:42
 *@Version 1.0
 */
@Data
public class ECKeyPair {
    public final BigInteger PrivateKey;

    public final ECPoint PublicKey;

    public ECKeyPair(BigInteger privateKey, ECPoint publicKey) {
        this.PrivateKey = privateKey;
        this.PublicKey = publicKey;
    }
}
