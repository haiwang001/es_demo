package com.bjxczy.onepark.hanwang.core;

import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.springframework.beans.factory.annotation.Autowired;

import com.bjxczy.core.rabbitmq.event.door.AuthorityResultPayload;
import com.bjxczy.onepark.hanwang.pojo.dto.DevCommand;
import com.bjxczy.onepark.hanwang.pojo.dto.ParamsCommand;
import com.bjxczy.onepark.hanwang.publish.ReceiveEventPublisher;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TcpServerHandler extends IoHandlerAdapter {

	@Autowired
	private HanwangProperties properties;
	@Autowired
	private ReceiveEventPublisher publisher;

	private static final String SESSION_KEY_SN = "sn";
	private static final String SESSION_KEY_COMMAND = "command";
	private static final String SESSION_KEY_STAFF_ID = "staffId";
	private static final String SESSION_KEY_CALLBACK = "callback";

	private ConcurrentHashMap<String, ConcurrentLinkedQueue<ParamsCommand>> commandMap = new ConcurrentHashMap<>();

	/**
	 * 添加待执行命令
	 *
	 * @param sn
	 * @param command
	 */
	public void addAuthorityCommand(String sn, ParamsCommand command) {
		if (!commandMap.containsKey(sn)) {
			commandMap.put(sn, new ConcurrentLinkedQueue<>());
		}
		ConcurrentLinkedQueue<ParamsCommand> queue = commandMap.get(sn);
		queue.add(command);
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		if (properties.getShowLog())
			log.info("[TCP Handler] TCP Opend, sessionId={}, ip={}", session.getId(), getIpAddress(session));
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		if (properties.getShowLog())
			log.info("[TCP Handler] TCP Closed, sessionId={}, ip={}", session.getId(), getIpAddress(session));
	}

	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		if (properties.getShowLog())
			log.info("[TCP Handler] sessionId={}, received={}", session.getId(), message);
		DevCommand dcmd = new DevCommand(message);
		if ("RecogniseResult".equals(dcmd.getCommand())) {
			// 注意：保存数据然后进行回复
			publisher.receive(dcmd);
			// 回复确认消息
			session.write(new ParamsCommand("RETURN", "RecogniseResult").put("result", "success").toString());
		} else if ("GetRequest".equals(dcmd.getCommand())) {
			// 设备请求待执行命令
			String sn = dcmd.getParams().getString(SESSION_KEY_SN);
			commandHandler(sn, dcmd, session);
			// 缓存本次连接设备SN
			session.setAttribute(SESSION_KEY_SN, sn);
		} else if ("Return".equals(dcmd.getCommand())) {
			// 连续发送命令
			publisher.receive(dcmd);
			String sn = (String) session.getAttribute(SESSION_KEY_SN);
			if (!commandHandler(sn, dcmd, session)) {
				// 不存在剩余命令时主动关闭连接
				session.closeNow();
			}
		} else if ("Quit".equals(dcmd.getCommand())) {
			session.closeNow();
		}
	}

	/**
	 * 判断是否存在待执行命令
	 *
	 * @param sn
	 * @param dcmd
	 * @return
	 */
	private boolean commandHandler(String sn, DevCommand dcmd, IoSession session) {
		if (commandMap.containsKey(sn)) {
			// 授权结果回调
			if (dcmd.isReturn()) {
				String command = (String) session.getAttribute(SESSION_KEY_COMMAND);
				if (command != null) {
					@SuppressWarnings("unchecked")
					Consumer<AuthorityResultPayload> func = (Consumer<AuthorityResultPayload>) session
							.getAttribute(SESSION_KEY_CALLBACK);
					Integer staffId = (Integer) session.getAttribute(SESSION_KEY_STAFF_ID);
					String reason = dcmd.getParams().getString("reason");
					Integer type = Objects.equals("SetEmployee", command) ? 0 : 1;
					func.accept(new AuthorityResultPayload(sn, staffId, type, dcmd.isSuccess(), reason));
					cleanSession(session);
				}
			}
			// 发送执行命令
			ConcurrentLinkedQueue<ParamsCommand> queue = commandMap.get(sn);
			ParamsCommand command = queue.poll();
			if (command != null) {
				// 将待授权员工ID加入缓存
				if (null != command.getCallback()) {
					Integer staffId = command.getParams().getInteger("id");
					session.setAttribute(SESSION_KEY_COMMAND, command.getCommand());
					session.setAttribute(SESSION_KEY_STAFF_ID, staffId);
					session.setAttribute(SESSION_KEY_CALLBACK, command.getCallback());
				}
				log.info("[TCP Handler] write message, sn={}, command={}", sn, command);
				session.write(command.toString());
				return true;
			}
		}
		return false;
	}

	private void cleanSession(IoSession session) {
		session.removeAttribute(SESSION_KEY_COMMAND);
		session.removeAttribute(SESSION_KEY_STAFF_ID);
		session.removeAttribute(SESSION_KEY_CALLBACK);
	}

	/**
	 * 获取设备IP地址
	 *
	 * @param session
	 * @return
	 */
	private static String getIpAddress(IoSession session) {
		return ((InetSocketAddress) session.getRemoteAddress()).getAddress().getHostAddress();
	}

}
