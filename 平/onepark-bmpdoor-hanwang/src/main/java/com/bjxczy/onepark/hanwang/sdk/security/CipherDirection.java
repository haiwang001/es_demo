package com.bjxczy.onepark.hanwang.sdk.security;


/*
 *@ClassName CipherDirection
 *@Author 温良伟
 *@Date 2023/3/29 14:53
 *@Version 1.0
 */
public enum CipherDirection {
    Encryption, Decryption;
}

