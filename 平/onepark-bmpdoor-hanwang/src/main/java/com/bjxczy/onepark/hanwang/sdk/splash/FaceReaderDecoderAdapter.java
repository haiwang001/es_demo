package com.bjxczy.onepark.hanwang.sdk.splash;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.hanwang.sdk.security.Base64;
import com.bjxczy.onepark.hanwang.sdk.security.ByteArrayWrapper;
import com.bjxczy.onepark.hanwang.sdk.security.CipherDirection;
import com.bjxczy.onepark.hanwang.sdk.security.CipherMode;
import com.bjxczy.onepark.hanwang.sdk.security.ECCurve;
import com.bjxczy.onepark.hanwang.sdk.security.ECPoint;
import com.bjxczy.onepark.hanwang.sdk.security.FpPoint;
import com.bjxczy.onepark.hanwang.sdk.security.PaddingMode;
import com.bjxczy.onepark.hanwang.sdk.security.SM2;
import com.bjxczy.onepark.hanwang.sdk.security.SM2KeyExchangeInformation;
import com.bjxczy.onepark.hanwang.sdk.security.SM3;
import com.bjxczy.onepark.hanwang.sdk.security.SM4;
import com.bjxczy.onepark.hanwang.sdk.security.Utils;
/*
 *@ClassName FaceReaderDecoderAdapter
 *@Author 温良伟
 *@Date 2023/3/29 15:18
 *@Version 1.0
 */
public class FaceReaderDecoderAdapter extends CumulativeProtocolDecoder {
    private static final AttributeKey KEY_DECRYPTOR = new AttributeKey(FaceReaderDecoderAdapter.class, "Decryptor");

    static final AttributeKey KEY_SM2KEYEXCHANGEINFORMATION = new AttributeKey(FaceReaderDecoderAdapter.class, "SM2KeyExchangeInformation");

    static final AttributeKey KEY_SM2OWNERID = new AttributeKey(FaceReaderDecoderAdapter.class, "SM2OwnerId");

    protected final boolean IsPassiveEncryption;

    protected final ManualResetEvent EncryptionReadyEvent;

    public FaceReaderDecoderAdapter(boolean isPassiveEncryption, ManualResetEvent encryptionReadyEvent) {
        this.IsPassiveEncryption = isPassiveEncryption;
        this.EncryptionReadyEvent = encryptionReadyEvent;
        if (encryptionReadyEvent != null)
            encryptionReadyEvent.Reset();
    }

    public FaceReaderDecoderAdapter(boolean isPassiveEncryption) {
        this(isPassiveEncryption, null);
    }

    public FaceReaderDecoderAdapter() {
        this(false, null);
    }

    protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
        if (in.remaining() >= 4) {
            in.mark();
            byte[] SizeByteArray = new byte[4];
            in.get(SizeByteArray);
            int Size = Utils.ToInt32(SizeByteArray, 0, ByteOrder.BIG_ENDIAN);
            if (Size == 0)
                return in.hasRemaining();
            if (in.remaining() >= Size) {
                byte[] Source = new byte[Size];
                in.get(Source);
                SM4 Decryptor = (SM4)session.getAttribute(KEY_DECRYPTOR);
                if (Decryptor != null)
                    Source = Decryptor.TransformFinalBlock(Source, 0, Size);
                String Message = new String(Source, "UTF-8");
                UNION_PARSE_TYPE A = (UNION_PARSE_TYPE)JSON.parseObject(Message, UNION_PARSE_TYPE.class);
                if ("AKE".equals(A.COMMAND) && Decryptor == null) {
                    SetLicense(session, Message);
                } else if ("AKE".equals(A.RETURN) && Decryptor == null) {
                    GetLicense(session, Message);
                } else if (this.IsPassiveEncryption && Decryptor == null) {
                    session.closeNow();
                } else {
                    out.write(Message);
                }
                return in.hasRemaining();
            }
            in.reset();
        }
        return false;
    }

    public void dispose(IoSession session) throws Exception {
        Closeable Decryptor = (Closeable)session.getAttribute(KEY_DECRYPTOR);
        if (Decryptor != null)
            Decryptor.close();
        session.removeAttribute(KEY_DECRYPTOR);
        session.removeAttribute(KEY_SM2KEYEXCHANGEINFORMATION);
        session.removeAttribute(KEY_SM2OWNERID);
        super.dispose(session);
    }

    public static void setSecretKey(IoSession session, byte[] secretKey, int mode) throws IOException {
        SM4 Decryptor = (SM4)session.getAttribute(KEY_DECRYPTOR);
        if (Decryptor != null) {
            Decryptor.close();
            session.removeAttribute(KEY_DECRYPTOR);
        }
        if (secretKey != null) {
            byte[] DerivedVector;
            Decryptor = new SM4(CipherDirection.Decryption, CipherMode.OFB, PaddingMode.PKCS7);
            if (mode == 0 && secretKey.length == 32) {
                DerivedVector = secretKey;
            } else {
                DerivedVector = SM3.KDF(secretKey, 32);
            }
            Decryptor.Initialize(Arrays.copyOfRange(DerivedVector, 0, 16), Arrays.copyOfRange(DerivedVector, 16, 32));
            session.setAttribute(KEY_DECRYPTOR, Decryptor);
        }
    }

    private static void SetLicense(IoSession session, String message) throws IOException {
        REPLY_AKE_TYPE M = new REPLY_AKE_TYPE();
        M.RETURN = "AKE";
        M.PARAM = new PARAM_REPLY_AKE_TYPE();
        M.PARAM.result = "success";
        M.PARAM.sn = (String)session.getAttribute(KEY_SM2OWNERID);
        COMMAND_AKE_TYPE A = (COMMAND_AKE_TYPE)JSON.parseObject(message, COMMAND_AKE_TYPE.class);
        if (A.PARAM == null || A.PARAM.key == null) {
            Random R = new Random();
            int Length = 16 + R.nextInt(17);
            StringBuilder sb = new StringBuilder(Length);
            for (int i = 0; i < Length; ) {
                sb.append(Character.toChars(33 + R.nextInt(94)));
                i++;
            }
            byte[] SecretKey = sb.toString().getBytes("UTF-8");
            try (SM4 sm = new SM4(CipherDirection.Encryption, CipherMode.OFB, PaddingMode.PKCS7)) {
                sm.Initialize(AKE_KEY, AKE_IV);
                M.PARAM.key = new String[] { Base64.Encode(sm.TransformFinalBlock(SecretKey, 0, SecretKey.length)), null };
                session.write(JSON.toJSONString(M));
            }
            FaceReaderProtocolCodecFactory.setEncoderKey(session, SecretKey, 1);
            FaceReaderProtocolCodecFactory.setDecoderKey(session, SecretKey, 1);
        } else {
            SM2KeyExchangeInformation Information = (SM2KeyExchangeInformation)session.getAttribute(KEY_SM2KEYEXCHANGEINFORMATION);
            if (Information == null)
                return;
            SM2 sm2 = FaceReaderProtocolCodecFactory.sm2;
            String PartnerPublicKey = A.PARAM.key[0];
            BigInteger X = new BigInteger(PartnerPublicKey.substring(0, 64), 16);
            BigInteger Y = new BigInteger(PartnerPublicKey.substring(64), 16);
            Information.PartnerPublicKey = (ECPoint)new FpPoint((ECCurve)sm2.mCurve, sm2.mCurve.FromBigInteger(X), sm2.mCurve.FromBigInteger(Y), false);
            Information.PartnerZ = sm2.ComputeZ(A.PARAM.sn.getBytes("UTF-8"), Information.PartnerPublicKey);
            PartnerPublicKey = A.PARAM.key[1];
            X = new BigInteger(PartnerPublicKey.substring(0, 64), 16);
            Y = new BigInteger(PartnerPublicKey.substring(64), 16);
            Information.PartnerR = (ECPoint)new FpPoint((ECCurve)sm2.mCurve, sm2.mCurve.FromBigInteger(X), sm2.mCurve.FromBigInteger(Y), false);
            ByteArrayWrapper SharedKey = new ByteArrayWrapper();
            if (sm2.KeyAgreement(Information, false, 32, SharedKey, false)) {
                session.removeAttribute(KEY_SM2KEYEXCHANGEINFORMATION);
                session.removeAttribute(KEY_SM2OWNERID);
                M.PARAM.key = new String[2];
                StringBuilder sb = new StringBuilder(128);
                sb.append(Utils.ToString(sm2.GetEncoded(Information.PublicKey.getX())));
                sb.append(Utils.ToString(sm2.GetEncoded(Information.PublicKey.getY())));
                M.PARAM.key[0] = sb.toString();
                sb.delete(0, sb.length());
                sb.append(Utils.ToString(sm2.GetEncoded(Information.R.getX())));
                sb.append(Utils.ToString(sm2.GetEncoded(Information.R.getY())));
                M.PARAM.key[1] = sb.toString();
                session.write(JSON.toJSONString(M));
                FaceReaderProtocolCodecFactory.setEncoderKey(session, SharedKey.data, 0);
                FaceReaderProtocolCodecFactory.setDecoderKey(session, SharedKey.data, 0);
            }
        }
    }

    private void GetLicense(IoSession session, String message) throws IOException {
        REPLY_AKE_TYPE A = (REPLY_AKE_TYPE)JSON.parseObject(message, REPLY_AKE_TYPE.class);
        if (!"success".equals(A.PARAM.result))
            return;
        if (Utils.IsNullOrEmpty(A.PARAM.key[1])) {
            byte[] Source = Base64.Decode(A.PARAM.key[0]);
            try (SM4 sm = new SM4(CipherDirection.Decryption, CipherMode.OFB, PaddingMode.PKCS7)) {
                sm.Initialize(AKE_KEY, AKE_IV);
                byte[] SecretKey = sm.TransformFinalBlock(Source, 0, Source.length);
                FaceReaderProtocolCodecFactory.setEncoderKey(session, SecretKey, 1);
                FaceReaderProtocolCodecFactory.setDecoderKey(session, SecretKey, 1);
                this.EncryptionReadyEvent.Set();
            }
        } else {
            SM2KeyExchangeInformation Information = (SM2KeyExchangeInformation)session.getAttribute(KEY_SM2KEYEXCHANGEINFORMATION);
            SM2 sm2 = FaceReaderProtocolCodecFactory.sm2;
            String PartnerPublicKey = A.PARAM.key[0];
            BigInteger X = new BigInteger(PartnerPublicKey.substring(0, 64), 16);
            BigInteger Y = new BigInteger(PartnerPublicKey.substring(64), 16);
            Information.PartnerPublicKey = (ECPoint)new FpPoint((ECCurve)sm2.mCurve, sm2.mCurve.FromBigInteger(X), sm2.mCurve.FromBigInteger(Y), false);
            Information.PartnerZ = sm2.ComputeZ(A.PARAM.sn.getBytes("UTF-8"), Information.PartnerPublicKey);
            PartnerPublicKey = A.PARAM.key[1];
            X = new BigInteger(PartnerPublicKey.substring(0, 64), 16);
            Y = new BigInteger(PartnerPublicKey.substring(64), 16);
            Information.PartnerR = (ECPoint)new FpPoint((ECCurve)sm2.mCurve, sm2.mCurve.FromBigInteger(X), sm2.mCurve.FromBigInteger(Y), false);
            ByteArrayWrapper SharedKey = new ByteArrayWrapper();
            if (sm2.KeyAgreement(Information, true, 32, SharedKey, false)) {
                session.removeAttribute(KEY_SM2KEYEXCHANGEINFORMATION);
                FaceReaderProtocolCodecFactory.setEncoderKey(session, SharedKey.data, 0);
                FaceReaderProtocolCodecFactory.setDecoderKey(session, SharedKey.data, 0);
                this.EncryptionReadyEvent.Set();
            }
        }
    }

    private static final byte[] AKE_KEY = new byte[] {
            -44, 90, 67, -26, 107, 92, -26, 36, -13, 24,
            -92, -84, -50, -81, 95, -23 };

    private static final byte[] AKE_IV = new byte[] {
            -46, 27, 117, -3, 74, 97, -26, -81, 3, -126,
            -101, 117, -127, 22, -116, -20 };
}
