package com.bjxczy.onepark.common.enums;


/**
 * 多平台的授权业务id
 */
public enum PlatformTypeCode {
    PLATFORM_FACE_MACHINE_TYPE_CODE(4, "KingOfHanFaceMachine"),//闸机类型
    PLATFORM_TYPE_CODE(0, "hanwang");//平台码

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    PlatformTypeCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
