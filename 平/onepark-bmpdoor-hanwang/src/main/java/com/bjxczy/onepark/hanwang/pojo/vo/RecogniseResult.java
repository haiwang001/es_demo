package com.bjxczy.onepark.hanwang.pojo.vo;

import lombok.Data;

@Data
public class RecogniseResult {
	
	/**
	 * 设备标识
	 */
	private String sn;
	
	/**
	 * 身份证号码
	 */
	private String id;
	
	private Boolean isUserUpdate;
	
	/**
	 * 姓名
	 */
	private String name;
	
	/**
	 * 设备型号
	 */
	private String type;
	
	/**
	 * 算法版本
	 */
	private String alg_edition;
	
	/**
	 * 人员照片：Base64
	 */
	private String capturejpg;
	
	/**
	 * 匹配得分
	 */
	private String score;
	
	/**
	 * 是否通过：1 - 通过，0 - 不通过
	 */
	private String pass;
	
	/**
	 * 阈值
	 */
	private String threshold;
	
	/**
	 * 开门时间
	 */
	private String time;
	
	/**
	 * 认证方式
	 */
	private String recogType;
	
	/**
	 * 识别方式
	 */
	private String identifyType;
	
	/**
	 * 体温
	 */
	private String animaHeat;
	
	/**
	 * 是否佩戴口罩：1 - 佩戴，2 - 未佩戴
	 */
	private String wearMask;

}
