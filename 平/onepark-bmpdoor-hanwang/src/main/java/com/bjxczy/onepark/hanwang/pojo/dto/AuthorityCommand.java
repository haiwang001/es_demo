package com.bjxczy.onepark.hanwang.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorityCommand {
	
	private String id;
	
	private String name;
	
	private String idCard;
	
	private String jobNum;
	
	private String captruejpg;
	
	
}
