package com.bjxczy.onepark.hanwang.core;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioDatagramAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bjxczy.onepark.hanwang.sdk.splash.FaceReaderProtocolCodecFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class MinaConfig {

	@Autowired
	private HanwangProperties properties;

	private static String LOCAL_HOST;

	static {
		try {
			LOCAL_HOST = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	@Bean("tcpServerHandler")
	public TcpServerHandler tcpServerHandler() {
		return new TcpServerHandler();
	}

	@Bean("udpServerHandler")
	public UdpServerHandler udpServerHandler() {
		return new UdpServerHandler();
	}

	@Bean("TcpIoAcceptor")
	public IoAcceptor ioAcceptor() throws IOException {
		IoAcceptor acceptor = new NioSocketAcceptor(); // tcp报文监听器
		acceptor.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(new FaceReaderProtocolCodecFactory()));
		acceptor.setHandler(tcpServerHandler());
		InetSocketAddress inetSocketAddress = new InetSocketAddress(InetAddress.getByName(LOCAL_HOST),
				properties.getTcpPort());
		acceptor.bind(inetSocketAddress);
		log.info("[TCP Handler] TCP listener started! ip={}, port={}", LOCAL_HOST, properties.getTcpPort());
		return acceptor;
	}

	@Bean("udpIoAcceptor")
	public IoAcceptor udpIoAcceptor() throws IOException {
		IoAcceptor acceptor = new NioDatagramAcceptor();
		acceptor.getFilterChain().addLast("codes", new ProtocolCodecFilter(new FaceReaderProtocolCodecFactory()));
		acceptor.setHandler(udpServerHandler());
		InetSocketAddress inetSocketAddress = new InetSocketAddress(InetAddress.getByName(LOCAL_HOST),
				properties.getUdpPort());
		acceptor.bind(inetSocketAddress);
		log.info("[UDP Handler] UDP listener started! ip={}, port={}", LOCAL_HOST, properties.getUdpPort());
		return acceptor;
	}

}
