package com.bjxczy.onepark.hanwang.sdk.security;


/*
 *@ClassName PaddingMode
 *@Author 温良伟
 *@Date 2023/3/29 14:56
 *@Version 1.0
 */
public enum PaddingMode {
    PKCS7, ANSIX923, ISO10126, ISO7816, Zeros, None;
}
