package com.bjxczy.onepark.hanwang.sdk.security;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
/*
 *@ClassName Base64
 *@Author 温良伟
 *@Date 2023/3/29 14:52
 *@Version 1.0
 */
public class Base64 {
    private static final String DATA_BIN2ASCII = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    private static final int B64_NOT_BASE64 = -13;

    public static String Encode(byte[] binaryData) {
        if (binaryData == null)
            return null;
        int Length = (binaryData.length + 2) / 3 << 2;
        StringBuilder sb = new StringBuilder(Length);
        int Index = 0;
        for (int i = binaryData.length; i > 0; i -= 3) {
            if (i >= 3) {
                int b0 = binaryData[Index++] & 0xFF;
                int b1 = binaryData[Index++] & 0xFF;
                int b2 = binaryData[Index++] & 0xFF;
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(b0 >> 2));
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((b0 << 4 | b1 >> 4) & 0x3F));
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((b1 << 2 | b2 >> 6) & 0x3F));
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(b2 & 0x3F));
            } else {
                int b1, b0 = binaryData[Index++] & 0xFF;
                if (i == 2) {
                    b1 = binaryData[Index++] & 0xFF;
                } else {
                    b1 = 0;
                }
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(b0 >> 2));
                sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((b0 << 4 | b1 >> 4) & 0x3F));
                if (i == 1) {
                    sb.append('=');
                } else {
                    sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(b1 << 2 & 0x3F));
                }
                sb.append('=');
            }
        }
        return sb.toString();
    }

    private static final byte[] DATA_ASCII2BIN = new byte[]{
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -32,
            -16, -1, -1, -15, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -32, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, 62, -1, -14, -1, 63, 52, 53,
            54, 55, 56, 57, 58, 59, 60, 61, -1, -1,
            -1, 0, -1, -1, -1, 0, 1, 2, 3, 4,
            5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
            25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
            29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
            39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
            49, 50, 51, -1, -1, -1, -1, -1};

    public static byte[] Decode(String base64String) throws IOException {
        if (base64String == null || base64String.isEmpty())
            return null;
        int length = base64String.length();
        try (ByteArrayOutputStream memoryStream = new ByteArrayOutputStream((length + 3 >> 2) * 3)) {
            int index = 0;
            int previous = 0;
            for (int i = 0; i < length; i++) {
                int charValue = base64String.charAt(i);
                if (charValue >= 128)
                    throw new IllegalArgumentException();
                int orderValue = DATA_ASCII2BIN[charValue];
                if (orderValue == -1)
                    throw new IllegalArgumentException();
                if ((orderValue | 0x13) != -13)
                    switch (index) {
                        case 0:
                            if (charValue == 61)
                                throw new IllegalArgumentException();
                            previous = orderValue;
                            index++;
                            break;
                        case 1:
                            if (charValue == 61)
                                throw new IllegalArgumentException();
                            memoryStream.write(previous << 2 | orderValue >> 4);
                            previous = orderValue;
                            index++;
                            break;
                        case 2:
                            if (charValue == 61) {
                                index = 0;
                                break;
                            }
                            memoryStream.write(previous << 4 | orderValue >> 2);
                            previous = orderValue;
                            index++;
                            break;
                        default:
                            index = 0;
                            if (charValue == 61)
                                break;
                            memoryStream.write(previous << 6 | orderValue);
                            break;
                    }
            }
            if (index == 0)
                return memoryStream.toByteArray();
            throw new IllegalArgumentException();
        }
    }
}