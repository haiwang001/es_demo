package com.bjxczy.onepark.hanwang.sdk.security;


/*
 *@ClassName SymmetricAlgorithm
 *@Author 温良伟
 *@Date 2023/3/29 15:02
 *@Version 1.0
 */

import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public abstract class SymmetricAlgorithm implements Closeable {
    public final CipherDirection mDirection;

    public final CipherMode mMode;

    public final PaddingMode mPadding;

    public final int mBlockSizeInBytes;

    protected byte[] mInnerIV;

    protected IVectorProvider mVectorProvider;

    protected final byte[] mInputBuffer;

    protected final byte[] mOutputBuffer;

    private final byte[] mLiveIV;

    protected boolean mIsReady;

    protected boolean mIsHomeBlock;

    protected int mRemainderBytes;

    protected int mOrdinal;

    public abstract String getAlgorithmName();

    public abstract int getKeySize();

    public abstract int getKeySizeInBytes();

    public SymmetricAlgorithm(CipherDirection direction, CipherMode mode, PaddingMode padding, int blockSize) {
        this.mInnerIV = null;
        this.mVectorProvider = null;
        this.mIsReady = false;
        this.mIsHomeBlock = true;
        this.mRemainderBytes = 0;
        this.mOrdinal = 0;
        this.mDirection = direction;
        this.mMode = mode;
        this.mPadding = padding;
        this.mBlockSizeInBytes = blockSize;
        this.mInputBuffer = new byte[blockSize << 1];
        this.mOutputBuffer = new byte[blockSize << 1];
        if (CipherMode.ECB.equals(this.mMode) || CipherMode.ECBCTS.equals(this.mMode)) {
            this.mInnerIV = null;
            this.mLiveIV = null;
        } else {
            this.mInnerIV = new byte[blockSize];
            this.mLiveIV = new byte[blockSize];
        }
    }

    public SymmetricAlgorithm(CipherDirection direction, CipherMode mode, PaddingMode padding) {
        this(direction, mode, padding, 16);
    }

    public SymmetricAlgorithm(CipherDirection direction, CipherMode mode) {
        this(direction, mode, PaddingMode.PKCS7, 16);
    }

    public SymmetricAlgorithm(CipherDirection direction) {
        this(direction, CipherMode.CBC, PaddingMode.PKCS7, 16);
    }

    public boolean Initialize(byte[] key, byte[] iv) {
        if (this.mMode == CipherMode.CTR)
            return false;
        if (this.mMode != CipherMode.ECB && this.mMode != CipherMode.ECBCTS) {
            if (iv == null || iv.length != this.mBlockSizeInBytes)
                return false;
            System.arraycopy(iv, 0, this.mInnerIV, 0, this.mBlockSizeInBytes);
        }
        boolean IsOK = (this.mDirection.equals(CipherDirection.Encryption) || this.mMode.equals(CipherMode.CFB) || this.mMode.equals(CipherMode.OFB)) ? SetEncryptKey(key) : SetDecryptKey(key);
        if (!IsOK)
            return false;
        this.mIsHomeBlock = true;
        this.mIsReady = true;
        return true;
    }

    public boolean Initialize(byte[] key, IVectorProvider vectorProvider) {
        if (this.mMode != CipherMode.CTR || vectorProvider == null)
            return false;
        if (!SetEncryptKey(key))
            return false;
        this.mVectorProvider = vectorProvider;
        this.mIsHomeBlock = true;
        this.mIsReady = true;
        return true;
    }

    protected abstract boolean SetEncryptKey(byte[] paramArrayOfbyte);

    protected abstract boolean SetDecryptKey(byte[] paramArrayOfbyte);

    public abstract byte[] getKey();



    protected void Transform(byte[] input, int offset, byte[] output) throws IOException {
        byte[] var10000;
        int i;
        switch(this.mMode) {
            case ECB:
            case ECBCTS:
                this.TransformCore(this.mDirection, input, offset, output);
                break;
            case CBC:
            case CBCCTS:
                if (this.mDirection.equals(CipherDirection.Encryption)) {
                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        var10000 = this.mLiveIV;
                        var10000[i] ^= input[offset++];
                    }

                    this.TransformCore(CipherDirection.Encryption, this.mLiveIV, 0, output);
                    System.arraycopy(output, 0, this.mLiveIV, 0, this.mBlockSizeInBytes);
                } else {
                    this.TransformCore(CipherDirection.Decryption, input, offset, output);

                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        output[i] ^= this.mLiveIV[i];
                    }

                    System.arraycopy(input, offset, this.mLiveIV, 0, this.mBlockSizeInBytes);
                }
                break;
            case CFB:
                this.TransformCore(CipherDirection.Encryption, this.mLiveIV, 0, output);
                if (this.mDirection.equals(CipherDirection.Encryption)) {
                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        output[i] ^= input[offset++];
                    }

                    System.arraycopy(output, 0, this.mLiveIV, 0, this.mBlockSizeInBytes);
                    break;
                } else {
                    System.arraycopy(input, offset, this.mLiveIV, 0, this.mBlockSizeInBytes);

                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        output[i] ^= input[offset++];
                    }

                    return;
                }
            case OFB:
                this.TransformCore(CipherDirection.Encryption, this.mLiveIV, 0, this.mLiveIV);

                for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                    output[i] = (byte)(input[offset++] ^ this.mLiveIV[i]);
                }

                return;
            case PCBC:
                if (this.mDirection.equals(CipherDirection.Encryption)) {
                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        var10000 = this.mLiveIV;
                        var10000[i] ^= input[offset++];
                    }

                    this.TransformCore(CipherDirection.Encryption, this.mLiveIV, 0, output);
                    offset -= this.mBlockSizeInBytes;

                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        this.mLiveIV[i] = (byte)(input[offset++] ^ output[i]);
                    }

                    return;
                } else {
                    this.TransformCore(CipherDirection.Decryption, input, offset, output);

                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        output[i] ^= this.mLiveIV[i];
                    }

                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        this.mLiveIV[i] = (byte)(input[offset++] ^ output[i]);
                    }

                    return;
                }
            case CTR:
                this.mVectorProvider.GetVector(this.mOrdinal++, this.mLiveIV);
                this.TransformCore(CipherDirection.Encryption, this.mLiveIV, 0, output);

                for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                    output[i] ^= input[offset++];
                }
        }

    }

    protected abstract void TransformCore(CipherDirection paramCipherDirection, byte[] paramArrayOfbyte1, int paramInt, byte[] paramArrayOfbyte2);

    protected void TransformTail() throws IOException {
        byte[] var10000;
        int i;
        switch(this.mMode) {
            case ECBCTS:
                i = this.mRemainderBytes - this.mBlockSizeInBytes;
                this.TransformCore(this.mDirection, this.mInputBuffer, 0, this.mOutputBuffer);
                System.arraycopy(this.mOutputBuffer, 0, this.mOutputBuffer, this.mBlockSizeInBytes, i);
                System.arraycopy(this.mInputBuffer, this.mBlockSizeInBytes, this.mOutputBuffer, 0, i);
                this.TransformCore(this.mDirection, this.mOutputBuffer, 0, this.mOutputBuffer);
            case CBC:
            case PCBC:
            default:
                break;
            case CBCCTS:
                i = this.mRemainderBytes - this.mBlockSizeInBytes;
                if (this.mDirection.equals(CipherDirection.Encryption)) {
                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        var10000 = this.mLiveIV;
                        var10000[i] ^= this.mInputBuffer[i];
                    }

                    this.TransformCore(CipherDirection.Encryption, this.mLiveIV, 0, this.mOutputBuffer);
                    System.arraycopy(this.mOutputBuffer, 0, this.mOutputBuffer, this.mBlockSizeInBytes, i);
                    System.arraycopy(this.mInputBuffer, this.mBlockSizeInBytes, this.mLiveIV, 0, i);
                    Arrays.fill(this.mLiveIV, i, this.mBlockSizeInBytes, (byte)0);

                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        var10000 = this.mOutputBuffer;
                        var10000[i] ^= this.mLiveIV[i];
                    }

                    this.TransformCore(CipherDirection.Encryption, this.mOutputBuffer, 0, this.mOutputBuffer);
                    break;
                } else {
                    this.TransformCore(CipherDirection.Decryption, this.mInputBuffer, 0, this.mOutputBuffer);
                    i = this.mBlockSizeInBytes;

                    for(i = 0; i < i; ++i) {
                        var10000 = this.mOutputBuffer;
                        var10000[i] ^= this.mInputBuffer[i++];
                    }

                    System.arraycopy(this.mOutputBuffer, 0, this.mOutputBuffer, this.mBlockSizeInBytes, i);
                    System.arraycopy(this.mInputBuffer, this.mBlockSizeInBytes, this.mOutputBuffer, 0, i);
                    this.TransformCore(CipherDirection.Decryption, this.mOutputBuffer, 0, this.mOutputBuffer);

                    for(i = 0; i < this.mBlockSizeInBytes; ++i) {
                        var10000 = this.mOutputBuffer;
                        var10000[i] ^= this.mLiveIV[i];
                    }

                    return;
                }
            case CFB:
            case OFB:
            case CTR:
                if (this.mMode.equals(CipherMode.CTR)) {
                    this.mVectorProvider.GetVector(this.mOrdinal, this.mLiveIV);
                }

                this.TransformCore(CipherDirection.Encryption, this.mLiveIV, 0, this.mOutputBuffer);

                for(i = 0; i < this.mRemainderBytes; ++i) {
                    var10000 = this.mOutputBuffer;
                    var10000[i] ^= this.mInputBuffer[i];
                }
        }

    }

    public void TransformBlock(byte[] input, int offset, int count, List<Byte> output) throws IOException {
        if (!this.mIsReady)
            throw new UnsupportedOperationException();
        if (input == null || count <= 0)
            return;
        if (this.mIsHomeBlock) {
            this.mRemainderBytes = 0;
            if (this.mMode.equals(CipherMode.CTR)) {
                this.mOrdinal = 0;
            } else if (!this.mMode.equals(CipherMode.ECB) && !this.mMode.equals(CipherMode.ECBCTS)) {
                System.arraycopy(this.mInnerIV, 0, this.mLiveIV, 0, this.mBlockSizeInBytes);
            }
            this.mIsHomeBlock = false;
        }
        int FillBytes = this.mBlockSizeInBytes - this.mRemainderBytes;
        if (this.mRemainderBytes != 0 && ((this.mMode.getValue() < CipherMode.ECBCTS.getValue() && count >= FillBytes) || (this.mMode.getValue() >= CipherMode.ECBCTS.getValue() && count > this.mBlockSizeInBytes + FillBytes))) {
            if (FillBytes > 0)
                System.arraycopy(input, offset, this.mInputBuffer, this.mRemainderBytes, FillBytes);
            Transform(this.mInputBuffer, 0, this.mOutputBuffer);
            int i;
            for (i = 0; i < this.mBlockSizeInBytes; ) {
                output.add(Byte.valueOf(this.mOutputBuffer[i]));
                i++;
            }
            if (this.mRemainderBytes > this.mBlockSizeInBytes) {
                this.mRemainderBytes -= this.mBlockSizeInBytes;
                if (count + this.mRemainderBytes > this.mBlockSizeInBytes << 1) {
                    FillBytes = this.mBlockSizeInBytes - this.mRemainderBytes;
                    System.arraycopy(input, offset, this.mInputBuffer, this.mBlockSizeInBytes + this.mRemainderBytes, FillBytes);
                    Transform(this.mInputBuffer, this.mBlockSizeInBytes, this.mOutputBuffer);
                    for (i = 0; i < this.mBlockSizeInBytes; ) {
                        output.add(Byte.valueOf(this.mOutputBuffer[i]));
                        i++;
                    }
                } else {
                    System.arraycopy(this.mInputBuffer, this.mBlockSizeInBytes, this.mInputBuffer, 0, this.mRemainderBytes);
                }
            }
            if (FillBytes >= 0) {
                offset += FillBytes;
                count -= FillBytes;
                this.mRemainderBytes = 0;
            }
        }
        while ((this.mMode.getValue() < CipherMode.ECBCTS.getValue() && count >= this.mBlockSizeInBytes) || (this.mMode.getValue() >= CipherMode.ECBCTS.getValue() && count > this.mBlockSizeInBytes << 1)) {
            Transform(input, offset, this.mOutputBuffer);
            for (int i = 0; i < this.mBlockSizeInBytes; ) {
                output.add(Byte.valueOf(this.mOutputBuffer[i]));
                i++;
            }
            offset += this.mBlockSizeInBytes;
            count -= this.mBlockSizeInBytes;
        }
        if (count > 0) {
            System.arraycopy(input, offset, this.mInputBuffer, this.mRemainderBytes, count);
            this.mRemainderBytes += count;
        }
    }

    public boolean TransformFinalBlock(byte[] input, int offset, int count, List<Byte> output) throws IOException {
        if (!this.mIsReady)
            throw new UnsupportedOperationException();
        TransformBlock(input, offset, count, output);
        boolean IsOK = true;
        if (this.mMode.equals(CipherMode.CFB) || this.mMode.equals(CipherMode.OFB) || this.mMode.equals(CipherMode.CTR) || this.mMode.getValue() >= CipherMode.ECBCTS.getValue()) {
            if (this.mMode.getValue() >= CipherMode.ECBCTS.getValue() && this.mRemainderBytes <= this.mBlockSizeInBytes) {
                IsOK = false;
            } else if (this.mRemainderBytes > 0) {
                TransformTail();
                for (int i = 0; i < this.mRemainderBytes; ) {
                    output.add(Byte.valueOf(this.mOutputBuffer[i]));
                    i++;
                }
            }
        } else if (this.mDirection.equals(CipherDirection.Encryption)) {
            int Index, i, FillBytes = this.mBlockSizeInBytes - this.mRemainderBytes;
            switch (this.mPadding) {
                case PKCS7:
                    Index = this.mRemainderBytes;
                    for (i = 0; i < FillBytes; ) {
                        this.mInputBuffer[Index++] = (byte)FillBytes;
                        i++;
                    }
                    break;
                case ANSIX923:
                    Index = this.mRemainderBytes;
                    for (i = 1; i < FillBytes; ) {
                        this.mInputBuffer[Index++] = 0;
                        i++;
                    }
                    this.mInputBuffer[this.mBlockSizeInBytes - 1] = (byte)FillBytes;
                    break;
                case ISO10126:
                    this.mInputBuffer[this.mBlockSizeInBytes - 1] = (byte)FillBytes;
                    break;
                case ISO7816:
                    this.mInputBuffer[this.mRemainderBytes] = Byte.MIN_VALUE;
                    Index = this.mRemainderBytes + 1;
                    for (i = 1; i < FillBytes; ) {
                        this.mInputBuffer[Index++] = 0;
                        i++;
                    }
                    break;
                case Zeros:
                    Index = this.mRemainderBytes;
                    for (i = 0; i < FillBytes; ) {
                        this.mInputBuffer[Index++] = 0;
                        i++;
                    }
                    break;
                case None:
                    if (this.mRemainderBytes != 0)
                        IsOK = false;
                    break;
            }
            if (this.mPadding != PaddingMode.None) {
                Transform(this.mInputBuffer, 0, this.mOutputBuffer);
                for (int j = 0; j < this.mBlockSizeInBytes; ) {
                    output.add(Byte.valueOf(this.mOutputBuffer[j]));
                    j++;
                }
            }
        } else if (this.mRemainderBytes != 0) {
            IsOK = false;
        } else {
            int EndIndex;
            int PaddingLen;
            int i;
            switch (this.mPadding) {
                case PKCS7:
                case ANSIX923:
                case ISO10126:
                    EndIndex = output.size() - 1;
                    PaddingLen = ((Byte)output.get(EndIndex)).byteValue();
                    if (PaddingLen >= 1 && PaddingLen <= this.mBlockSizeInBytes) {
                        for (; PaddingLen-- > 0; output.remove(EndIndex--));
                        break;
                    }
                    IsOK = false;
                    break;
                case ISO7816:
                    EndIndex = output.size() - 1;
                    for (i = 0; i < this.mBlockSizeInBytes; i++) {
                        byte ByteValue = ((Byte)output.get(EndIndex)).byteValue();
                        output.remove(EndIndex--);
                        if (ByteValue == 128)
                            break;
                        if (ByteValue != 0) {
                            IsOK = false;
                            break;
                        }
                    }
                    if (i == this.mBlockSizeInBytes)
                        IsOK = false;
                    break;
                case Zeros:
                    EndIndex = output.size() - 1;
                    for (i = 0; i < this.mBlockSizeInBytes;) {
                        if (((Byte)output.get(EndIndex)).byteValue() == 0) {
                            output.remove(EndIndex--);
                            i++;
                        }
                    }
                    if (i == 0)
                        IsOK = false;
                    break;
            }
        }
        this.mIsHomeBlock = true;
        return IsOK;
    }

    public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset) throws IOException {
        if (!this.mIsReady)
            throw new UnsupportedOperationException();
        if (inputBuffer == null || inputCount <= 0 || inputOffset < 0 || inputOffset >= inputBuffer.length)
            return 0;
        if (this.mIsHomeBlock) {
            this.mRemainderBytes = 0;
            if (this.mMode.equals(CipherMode.CTR)) {
                this.mOrdinal = 0;
            } else if (!this.mMode.equals(CipherMode.ECB) && !this.mMode.equals(CipherMode.ECBCTS)) {
                System.arraycopy(this.mInnerIV, 0, this.mLiveIV, 0, this.mBlockSizeInBytes);
            }
            this.mIsHomeBlock = false;
        }
        int nBaseOffset = outputOffset;
        int FillBytes = this.mBlockSizeInBytes - this.mRemainderBytes;
        if (this.mRemainderBytes != 0 && ((this.mMode.getValue() < CipherMode.ECBCTS.getValue() && inputCount >= FillBytes) || (this.mMode.getValue() >= CipherMode.ECBCTS.getValue() && inputCount > this.mBlockSizeInBytes + FillBytes))) {
            if (FillBytes > 0)
                System.arraycopy(inputBuffer, inputOffset, this.mInputBuffer, this.mRemainderBytes, FillBytes);
            Transform(this.mInputBuffer, 0, this.mOutputBuffer);
            System.arraycopy(this.mOutputBuffer, 0, outputBuffer, outputOffset, this.mBlockSizeInBytes);
            outputOffset += this.mBlockSizeInBytes;
            if (this.mRemainderBytes > this.mBlockSizeInBytes) {
                this.mRemainderBytes -= this.mBlockSizeInBytes;
                if (inputCount + this.mRemainderBytes > this.mBlockSizeInBytes << 1) {
                    FillBytes = this.mBlockSizeInBytes - this.mRemainderBytes;
                    System.arraycopy(inputBuffer, inputOffset, this.mInputBuffer, this.mBlockSizeInBytes + this.mRemainderBytes, FillBytes);
                    Transform(this.mInputBuffer, this.mBlockSizeInBytes, this.mOutputBuffer);
                    System.arraycopy(this.mOutputBuffer, 0, outputBuffer, outputOffset, this.mBlockSizeInBytes);
                    outputOffset += this.mBlockSizeInBytes;
                } else {
                    System.arraycopy(this.mInputBuffer, this.mBlockSizeInBytes, this.mInputBuffer, 0, this.mRemainderBytes);
                }
            }
            if (FillBytes >= 0) {
                inputOffset += FillBytes;
                inputCount -= FillBytes;
                this.mRemainderBytes = 0;
            }
        }
        while ((this.mMode.getValue() < CipherMode.ECBCTS.getValue() && inputCount >= this.mBlockSizeInBytes) || (this.mMode.getValue() >= CipherMode.ECBCTS.getValue() && inputCount > this.mBlockSizeInBytes << 1)) {
            Transform(inputBuffer, inputOffset, this.mOutputBuffer);
            System.arraycopy(this.mOutputBuffer, 0, outputBuffer, outputOffset, this.mBlockSizeInBytes);
            outputOffset += this.mBlockSizeInBytes;
            inputOffset += this.mBlockSizeInBytes;
            inputCount -= this.mBlockSizeInBytes;
        }
        if (inputCount > 0) {
            System.arraycopy(inputBuffer, inputOffset, this.mInputBuffer, this.mRemainderBytes, inputCount);
            this.mRemainderBytes += inputCount;
        }
        return outputOffset - nBaseOffset;
    }

    public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount) throws IOException {
        if (!this.mIsReady)
            throw new UnsupportedOperationException();
        if (inputBuffer == null || inputCount <= 0 || inputOffset < 0 || inputOffset >= inputBuffer.length)
            return null;
        byte[] OutputBuffer = new byte[((this.mRemainderBytes + inputCount) / this.mBlockSizeInBytes + 1) * this.mBlockSizeInBytes];
        int OutputOffset = TransformBlock(inputBuffer, inputOffset, inputCount, OutputBuffer, 0);
        if (this.mMode.equals(CipherMode.CFB) || this.mMode.equals(CipherMode.OFB) || this.mMode.equals(CipherMode.CTR) || this.mMode.getValue() >= CipherMode.ECBCTS.getValue()) {
            if (this.mMode.getValue() >= CipherMode.ECBCTS.getValue() && this.mRemainderBytes <= this.mBlockSizeInBytes)
                throw new IOException();
            if (this.mRemainderBytes > 0) {
                TransformTail();
                System.arraycopy(this.mOutputBuffer, 0, OutputBuffer, OutputOffset, this.mRemainderBytes);
                OutputOffset += this.mRemainderBytes;
            }
        } else if (this.mDirection.equals(CipherDirection.Encryption)) {
            int Index, i, FillBytes = this.mBlockSizeInBytes - this.mRemainderBytes;
            switch (this.mPadding) {
                case PKCS7:
                    Index = this.mRemainderBytes;
                    for (i = 0; i < FillBytes; ) {
                        this.mInputBuffer[Index++] = (byte)FillBytes;
                        i++;
                    }
                    break;
                case ANSIX923:
                    Index = this.mRemainderBytes;
                    for (i = 1; i < FillBytes; ) {
                        this.mInputBuffer[Index++] = 0;
                        i++;
                    }
                    this.mInputBuffer[this.mBlockSizeInBytes - 1] = (byte)FillBytes;
                    break;
                case ISO10126:
                    this.mInputBuffer[this.mBlockSizeInBytes - 1] = (byte)FillBytes;
                    break;
                case ISO7816:
                    this.mInputBuffer[this.mRemainderBytes] = Byte.MIN_VALUE;
                    Index = this.mRemainderBytes + 1;
                    for (i = 1; i < FillBytes; ) {
                        this.mInputBuffer[Index++] = 0;
                        i++;
                    }
                    break;
                case Zeros:
                    Index = this.mRemainderBytes;
                    for (i = 0; i < FillBytes; ) {
                        this.mInputBuffer[Index++] = 0;
                        i++;
                    }
                    break;
                case None:
                    if (this.mRemainderBytes != 0)
                        throw new IOException();
                    break;
            }
            if (this.mPadding != PaddingMode.None) {
                Transform(this.mInputBuffer, 0, this.mOutputBuffer);
                System.arraycopy(this.mOutputBuffer, 0, OutputBuffer, OutputOffset, this.mBlockSizeInBytes);
                OutputOffset += this.mBlockSizeInBytes;
            }
        } else {
            int PaddingLen;
            int i;
            if (this.mRemainderBytes != 0)
                throw new IOException();
            switch (this.mPadding) {
                case PKCS7:
                case ANSIX923:
                case ISO10126:
                    PaddingLen = OutputBuffer[OutputOffset - 1];
                    if (PaddingLen >= 1 && PaddingLen <= this.mBlockSizeInBytes) {
                        OutputOffset -= PaddingLen;
                        break;
                    }
                    throw new IOException();
                case ISO7816:
                    for (i = 0; i < this.mBlockSizeInBytes; i++) {
                        byte ByteValue = OutputBuffer[--OutputOffset];
                        if (ByteValue == 128)
                            break;
                        if (ByteValue != 0)
                            throw new IOException();
                    }
                    if (i == this.mBlockSizeInBytes)
                        throw new IOException();
                    break;
                case Zeros:
                    for (i = 0; i < this.mBlockSizeInBytes; i++) {
                        if (OutputBuffer[--OutputOffset] != 0) {
                            OutputOffset++;
                            break;
                        }
                    }
                    if (i == 0)
                        throw new IOException();
                    break;
            }
        }
        this.mIsHomeBlock = true;
        if (OutputOffset < OutputBuffer.length)
            return Arrays.copyOfRange(OutputBuffer, 0, OutputOffset);
        return OutputBuffer;
    }

    public byte[] getIV() {
        return (this.mInnerIV == null) ? null : (byte[])this.mInnerIV.clone();
    }

    public void close() throws IOException {
        if (this.mInnerIV != null)
            Arrays.fill(this.mInnerIV, (byte)0);
        if (this.mLiveIV != null)
            Arrays.fill(this.mLiveIV, (byte)0);
        if (this.mInputBuffer != null)
            Arrays.fill(this.mInputBuffer, (byte)0);
        if (this.mOutputBuffer != null)
            Arrays.fill(this.mOutputBuffer, (byte)0);
    }
}

