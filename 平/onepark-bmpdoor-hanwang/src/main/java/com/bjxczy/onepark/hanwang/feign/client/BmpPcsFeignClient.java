package com.bjxczy.onepark.hanwang.feign.client;

import com.bjxczy.core.feign.client.pcs.FileFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("pcs")
public interface BmpPcsFeignClient extends FileFeignClient {

}
