package com.bjxczy.onepark.hanwang.sdk.splash;


import lombok.Data;

/*
 *@ClassName COMMAND_PCIR_TYPE
 *@Author 温良伟
 *@Date 2023/3/29 15:13
 *@Version 1.0
 */
@Data
public class COMMAND_PCIR_TYPE {
    public String COMMAND;

    public PARAM_COMMAND_PCIR_TYPE PARAM;
}
