package com.bjxczy.onepark.hanwang.interfaces.listener;

import com.bjxczy.core.rabbitmq.event.door.*;
import com.bjxczy.onepark.common.enums.PlatformTypeCode;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjxczy.core.rabbitmq.rpc.AsyncReplyHandler;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.onepark.hanwang.service.AuthorityService;
import com.rabbitmq.client.Channel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@Component
public class DoorMutualListener extends DoorMutualBaseListener {

    @Autowired
    private AuthorityService authorityService;
    @Autowired
    private AsyncReplyHandler replyHandler;


    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmp_hw_door_create_event_queue", durable = "true"),  // 持久化队列
                    key = DoorMutualBaseListener.UPDATE_DOOR_CTRL_AUTHORIZATION,
                    exchange = @Exchange(value = DoorMutualBaseListener.DEFAULT_EXCAHNGE_NAME) // 绑定的交换机
            )
    })
    public void updateDoorEvent(UpdateAuthorityPayload payload, Channel channel, Message message) {
        this.confirm(() -> true, channel, message);
        log.info("[异步授权] 授权信息{}", payload);
        this.confirm(() -> true, channel, message);
        Map<String, List<AuthorityDeviceDTO>> collect = payload.getDoorList().stream().collect(Collectors.groupingBy(AuthorityDeviceDTO::getDeviceType));
        payload.setDoorList(collect.get(PlatformTypeCode.PLATFORM_FACE_MACHINE_TYPE_CODE.getMsg()));
        if (payload.getDoorList() == null ||payload.getDoorList().isEmpty()) {
            return;
        }
        authorityService.updateAuthority(payload, result -> replyHandler.reply(message, result));
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmp_door_del_event_queue", durable = "true"),  // 持久化队列
                    key = DoorMutualBaseListener.REMOVE_DOOR_CTRL_AUTHORIZATION,
                    exchange = @Exchange(value = DoorMutualBaseListener.DEFAULT_EXCAHNGE_NAME) // 绑定的交换机
            )
    })
    public void doorDelEventHandler(RemoveAuthorityPayload payload, Channel channel, Message message) {
        this.confirm(() -> true, channel, message);
        Map<String, List<AuthorityDeviceDTO>> collect = payload.getDoorList().stream().collect(Collectors.groupingBy(AuthorityDeviceDTO::getDeviceType));
        payload.setDoorList(collect.get(PlatformTypeCode.PLATFORM_FACE_MACHINE_TYPE_CODE.getMsg()));
        if (payload.getDoorList() == null || payload.getDoorList().isEmpty()) {
            return;
        }
        authorityService.removeAuthority(payload, result -> {
            replyHandler.reply(message, result);
        });
    }


    @Override
    public void updateDoorCtrlEvent(UpdateAuthorityPayload payload, Channel channel, Message message) {
        this.confirm(() -> true, channel, message);
        authorityService.updateAuthority(payload, result -> {
//			replyHandler.reply(message, result);
        });
    }


    @Override
    public void removeDoorCtrlEvent(RemoveAuthorityPayload payload, Channel channel, Message message) {
        this.confirm(() -> true, channel, message);
        authorityService.removeAuthority(payload, result -> {
//			replyHandler.reply(message, result);
        });
    }

}
