package com.bjxczy.onepark.hanwang.sdk.splash;


import lombok.Data;

import java.math.BigInteger;

import com.bjxczy.onepark.hanwang.sdk.security.Utils;

/*
 *@ClassName SM2UserInformation
 *@Author 温良伟
 *@Date 2023/3/29 15:16
 *@Version 1.0
 */
@Data
public class SM2UserInformation {
    public final String OwnerId;

    public final BigInteger PrivateKey;

    public final BigInteger PublicKeyX;

    public final BigInteger PublicKeyY;

    public SM2UserInformation(String ownerId, String privateKey, String publicKeyX, String publicKeyY) {
        if (Utils.IsNullOrEmpty(ownerId) || Utils.IsNullOrEmpty(privateKey) || privateKey.length() != 64 || Utils.IsNullOrEmpty(publicKeyX) || publicKeyX.length() != 64 || Utils.IsNullOrEmpty(publicKeyY) || publicKeyY.length() != 64)
            throw new IllegalArgumentException();
        this.OwnerId = ownerId;
        this.PrivateKey = new BigInteger(privateKey, 16);
        this.PublicKeyX = new BigInteger(publicKeyX, 16);
        this.PublicKeyY = new BigInteger(publicKeyY, 16);
    }
}
