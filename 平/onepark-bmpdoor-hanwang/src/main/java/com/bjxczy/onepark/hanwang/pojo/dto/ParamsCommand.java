package com.bjxczy.onepark.hanwang.pojo.dto;

import java.util.function.Consumer;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.event.door.AuthorityResultPayload;

import lombok.Getter;

/**
 * @author xczy
 *
 */
public class ParamsCommand {
	
	private JSONObject obj;
	
	private JSONObject params;
	@Getter
	private Consumer<AuthorityResultPayload> callback;
	
	public ParamsCommand(String command) {
		this.obj = new JSONObject();
		this.params = new JSONObject();
		this.obj.put("COMMAND", command);
		this.obj.put("PARAM", this.params);
	}
	
	public ParamsCommand(String name, String command) {
		this.obj = new JSONObject();
		this.params = new JSONObject();
		this.obj.put(name, command);
		this.obj.put("PARAM", this.params);
	}
	
	public ParamsCommand(JSONObject obj) {
		this.obj = obj;
		this.params = obj.getJSONObject("PARAM");
	}
	
	public ParamsCommand setCallback(Consumer<AuthorityResultPayload> callback) {
		this.callback = callback;
		return this;
	}

	public ParamsCommand put(String key, Object value) {
		this.params.put(key, value);
		return this;
	}
	
	public String getCommand() {
		String command = this.obj.getString("COMMAND");
		if (command == null) {
			// hack 汉王参数格式不统一
			String temp = this.obj.getString("RETURN");
			if (temp != null) {
				command = temp;
			}
		}
		return command;
	}
	
	public JSONObject getParams() {
		return this.params;
	}

	@Override
	public String toString() {
		return obj.toJSONString();
	}

}
