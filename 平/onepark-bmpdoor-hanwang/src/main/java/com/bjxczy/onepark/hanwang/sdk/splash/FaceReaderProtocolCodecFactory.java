package com.bjxczy.onepark.hanwang.sdk.splash;


import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

import com.alibaba.fastjson.JSON;
import com.bjxczy.onepark.hanwang.sdk.security.ECCurve;
import com.bjxczy.onepark.hanwang.sdk.security.ECKeyPair;
import com.bjxczy.onepark.hanwang.sdk.security.ECPoint;
import com.bjxczy.onepark.hanwang.sdk.security.FaceReaderEncoderAdapter;
import com.bjxczy.onepark.hanwang.sdk.security.FpPoint;
import com.bjxczy.onepark.hanwang.sdk.security.SM2;
import com.bjxczy.onepark.hanwang.sdk.security.SM2KeyExchangeInformation;
import com.bjxczy.onepark.hanwang.sdk.security.Utils;
/*
 *@ClassName FaceReaderProtocolCodecFactory
 *@Author 温良伟
 *@Date 2023/3/29 14:26
 *@Version 1.0
 */
public class FaceReaderProtocolCodecFactory implements ProtocolCodecFactory {
    static final SM2 sm2 = new SM2();

    private final FaceReaderEncoderAdapter mEncoder;

    private final FaceReaderDecoderAdapter mDecoder;

    public FaceReaderProtocolCodecFactory(boolean isPassiveEncryption, ManualResetEvent encryptionReadyEvent) {
        this.mEncoder = new FaceReaderEncoderAdapter();
        this.mDecoder = new FaceReaderDecoderAdapter(isPassiveEncryption, encryptionReadyEvent);
    }

    public FaceReaderProtocolCodecFactory(boolean isPassiveEncryption) {
        this(isPassiveEncryption, null);
    }

    public FaceReaderProtocolCodecFactory() {
        this(false, null);
    }

    public ProtocolEncoder getEncoder(IoSession session) {
        return this.mEncoder;
    }

    public ProtocolDecoder getDecoder(IoSession session) {
        return (ProtocolDecoder)this.mDecoder;
    }

    static void setEncoderKey(IoSession session, byte[] value, int mode) throws IOException {
        FaceReaderEncoderAdapter.setSecretKey(session, value, mode);
    }

    static void setDecoderKey(IoSession session, byte[] value, int mode) throws IOException {
        FaceReaderDecoderAdapter.setSecretKey(session, value, mode);
    }

    public static void EnablePassiveEncryption(IoSession session, boolean isServer, SM2UserInformation userInfo) throws UnsupportedEncodingException, IOException {
        if (userInfo == null) {
            if (!isServer) {
                COMMAND_AKE_TYPE Command = new COMMAND_AKE_TYPE();
                Command.COMMAND = "AKE";
                session.write(JSON.toJSONString(Command));
            }
        } else {
            SM2KeyExchangeInformation Information = new SM2KeyExchangeInformation();
            Information.PrivateKey = userInfo.PrivateKey;
            Information.PublicKey = (ECPoint)new FpPoint((ECCurve)sm2.mCurve, sm2.mCurve.FromBigInteger(userInfo.PublicKeyX), sm2.mCurve.FromBigInteger(userInfo.PublicKeyY), false);
            ECKeyPair KeyPair = sm2.GetKeyPair();
            Information.r = KeyPair.PrivateKey;
            Information.R = KeyPair.PublicKey;
            Information.Z = sm2.ComputeZ(userInfo.OwnerId.getBytes("UTF-8"), Information.PublicKey);
            session.setAttribute(FaceReaderDecoderAdapter.KEY_SM2KEYEXCHANGEINFORMATION, Information);
            if (isServer) {
                session.setAttribute(FaceReaderDecoderAdapter.KEY_SM2OWNERID, userInfo.OwnerId);
            } else {
                COMMAND_AKE_TYPE Command = new COMMAND_AKE_TYPE();
                Command.COMMAND = "AKE";
                Command.PARAM = new PARAM_COMMAND_AKE_TYPE();
                Command.PARAM.sn = userInfo.OwnerId;
                Command.PARAM.key = new String[2];
                StringBuilder sb = new StringBuilder(128);
                sb.append(Utils.ToString(sm2.GetEncoded(Information.PublicKey.getX())));
                sb.append(Utils.ToString(sm2.GetEncoded(Information.PublicKey.getY())));
                Command.PARAM.key[0] = sb.toString();
                sb.delete(0, sb.length());
                sb.append(Utils.ToString(sm2.GetEncoded(Information.R.getX())));
                sb.append(Utils.ToString(sm2.GetEncoded(Information.R.getY())));
                Command.PARAM.key[1] = sb.toString();
                session.write(JSON.toJSONString(Command));
            }
        }
    }

    public static void EnablePassiveEncryption(IoSession session, boolean isServer) throws IOException {
        EnablePassiveEncryption(session, isServer, null);
    }

    public static void EnablePassiveEncryption(IoSession session) throws IOException {
        EnablePassiveEncryption(session, false, null);
    }
}
