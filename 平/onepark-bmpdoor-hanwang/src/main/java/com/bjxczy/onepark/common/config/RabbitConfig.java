package com.bjxczy.onepark.common.config;

import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bjxczy.core.rabbitmq.rpc.AsyncReplyHandler;

@Configuration
public class RabbitConfig {
	
	@Bean
	public MessageConverter messageConverter() {
		return new SimpleMessageConverter(); // 注意不要重复声明，如同时需要异步及处理时
	}
	
	@Bean
	public AsyncReplyHandler replyAsyncHandler() {
		return new AsyncReplyHandler(messageConverter());
	}

}
