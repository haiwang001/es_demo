package com.bjxczy.onepark.hanwang.sdk.security;

import java.io.Closeable;
import java.io.IOException;
/*
 *@ClassName SM3VectorProvider
 *@Author 温良伟
 *@Date 2023/3/29 14:57
 *@Version 1.0
 */
public class SM3VectorProvider implements IVectorProvider, Closeable {
    protected final SM3 sm3Base;

    public SM3VectorProvider(byte[] Z) {
        this.sm3Base = new SM3();
        this.sm3Base.BlockUpdate(Z);
    }

    public void GetVector(int ordinal, byte[] output) throws IOException {
        try (SM3 sm3 = new SM3(this.sm3Base)) {
            sm3.Update((byte)(ordinal >>> 24 & 0xFF));
            sm3.Update((byte)(ordinal >>> 16 & 0xFF));
            sm3.Update((byte)(ordinal >>> 8 & 0xFF));
            sm3.Update((byte)(ordinal & 0xFF));
            System.arraycopy(sm3.DoFinal(), 0, output, 0, output.length);
        }
    }

    public void close() throws IOException {
        this.sm3Base.close();
    }
}

