package com.bjxczy.onepark.hanwang.service;

import java.util.function.Consumer;

import com.bjxczy.core.rabbitmq.event.door.AuthorityResultPayload;
import com.bjxczy.core.rabbitmq.event.door.RemoveAuthorityPayload;
import com.bjxczy.core.rabbitmq.event.door.UpdateAuthorityPayload;

public interface AuthorityService {

	void updateAuthority(UpdateAuthorityPayload payload, Consumer<AuthorityResultPayload> callback);

	void removeAuthority(RemoveAuthorityPayload payload, Consumer<AuthorityResultPayload> callback);

}
