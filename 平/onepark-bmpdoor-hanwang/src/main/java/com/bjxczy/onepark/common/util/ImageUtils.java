package com.bjxczy.onepark.common.util;

import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class ImageUtils {

	/**
	 * 文件File类型转byte[]
	 *
	 * @param file
	 * @return
	 */
	private static byte[] fileToByte(File file) {
		byte[] fileBytes = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			fileBytes = new byte[(int) file.length()];
			fis.read(fileBytes);
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileBytes;
	}

	/**
	 * 图片转Base64字符串
	 *
	 * @param url 远端文件Url
	 * @return File
	 */
	public static String imgFile2Base64String(String url) {
		// 对本地文件命名
		String fileName = url.substring(url.lastIndexOf("."), url.length());
		File file = null;
		URL urlfile;
		try {
			// 创建一个临时路径
			file = File.createTempFile("file", fileName);
			// 下载
			urlfile = new URL(url);
			try (InputStream inStream = urlfile.openStream(); OutputStream os = new FileOutputStream(file);) {
				int bytesRead = 0;
				byte[] buffer = new byte[8192];
				while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
					os.write(buffer, 0, bytesRead);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Base64.encodeBase64String(fileToByte(file));
	}

	@SneakyThrows
	public static File base64ToFile(String prefix, String suffix, String base64Str) {
		File file = File.createTempFile(prefix, suffix);
		try (FileOutputStream fos = new FileOutputStream(file)) {
			byte[] decoder= DatatypeConverter.parseBase64Binary(base64Str);
			fos.write(decoder);
		}
		return file;
	}

	public static MultipartFile fileToMultipartFile(File file) {
		FileItem fileItem = createFileItem(file);
		MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
		return multipartFile;
	}

	private static FileItem createFileItem(File file) {
		FileItemFactory factory = new DiskFileItemFactory(16, null);
		FileItem item = factory.createItem("textField", "text/plain", true, file.getName());
		int bytesRead = 0;
		byte[] buffer = new byte[8192];
		try {
			FileInputStream fis = new FileInputStream(file);
			OutputStream os = item.getOutputStream();
			while ((bytesRead = fis.read(buffer, 0, 8192)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			os.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return item;
	}
}
