package com.bjxczy.onepark.hanwang.sdk.splash;


import lombok.Data;

/*
 *@ClassName REPLY_PCIR_TYPE
 *@Author 温良伟
 *@Date 2023/3/29 15:15
 *@Version 1.0
 */
@Data
public class REPLY_PCIR_TYPE {
    public String RETURN;

    public PARAM_REPLY_PCIR_TYPE PARAM;
}
