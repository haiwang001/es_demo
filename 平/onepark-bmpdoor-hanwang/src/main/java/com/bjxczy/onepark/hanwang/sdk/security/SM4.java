package com.bjxczy.onepark.hanwang.sdk.security;




import java.io.IOException;
import java.nio.ByteOrder;
import java.util.Arrays;

/*
 *@ClassName SM4
 *@Author 温良伟
 *@Date 2023/3/29 14:47
 *@Version 1.0
 */
public class SM4 extends SymmetricAlgorithm {
    public String getAlgorithmName() {
        return "SM4";
    }

    public int getKeySize() {
        return 128;
    }

    public int getKeySizeInBytes() {
        return 16;
    }

    protected int[] mSK = new int[36];

    protected int mRKOff = 4;

    public SM4(CipherDirection direction, CipherMode mode, PaddingMode padding) {
        super(direction, mode, padding, 16);
    }

    public SM4(CipherDirection direction, CipherMode mode) {
        this(direction, mode, PaddingMode.PKCS7);
    }

    public SM4(CipherDirection direction) {
        this(direction, CipherMode.CBC, PaddingMode.PKCS7);
    }

    protected boolean SetEncryptKey(byte[] key) {
        if (key.length != getKeySizeInBytes())
            return false;
        Utils.IntegerFromByteArray(this.mSK, 0, key, 0, 16, ByteOrder.BIG_ENDIAN);
        this.mSK[0] = this.mSK[0] ^ FK[0];
        this.mSK[1] = this.mSK[1] ^ FK[1];
        this.mSK[2] = this.mSK[2] ^ FK[2];
        this.mSK[3] = this.mSK[3] ^ FK[3];
        for (int i = 0; i < 32; i++) {
            int XT = this.mSK[i + 1] ^ this.mSK[i + 2] ^ this.mSK[i + 3] ^ CK[i];
            XT = S[XT >>> 24] << 24 | (S[XT >> 16 & 0xFF] & 0xFF) << 16 | (S[XT >> 8 & 0xFF] & 0xFF) << 8 | S[XT & 0xFF] & 0xFF;
            this.mSK[i + 4] = this.mSK[i] ^ XT ^ Integer.rotateLeft(XT, 13) ^ Integer.rotateLeft(XT, 23);
        }
        return true;
    }

    protected boolean SetDecryptKey(byte[] key) {
        if (!SetEncryptKey(key))
            return false;
        Utils.Reverse(this.mSK, this.mRKOff, 32);
        return true;
    }

    public byte[] getKey() {
        if (!this.mIsReady)
            throw new UnsupportedOperationException();
        int[] SourceKey = new int[4];
        SourceKey[0] = this.mSK[0] ^ FK[0];
        SourceKey[1] = this.mSK[1] ^ FK[1];
        SourceKey[2] = this.mSK[2] ^ FK[2];
        SourceKey[3] = this.mSK[3] ^ FK[3];
        byte[] SecretKey = new byte[16];
        Utils.IntegerToByteArray(SecretKey, 0, SourceKey, 0, 4, ByteOrder.BIG_ENDIAN);
        Arrays.fill(SourceKey, 0);
        return SecretKey;
    }

    protected void TransformCore(CipherDirection direction, byte[] input, int offset, byte[] output) {
        int[] X = new int[4];
        Utils.IntegerFromByteArray(X, 0, input, offset, 16, ByteOrder.BIG_ENDIAN);
        int Index = this.mRKOff;
        for (int i = 0; i < 32; i++) {
            int XT = X[1] ^ X[2] ^ X[3] ^ this.mSK[Index++];
            XT = S[XT >>> 24] << 24 | (S[XT >> 16 & 0xFF] & 0xFF) << 16 | (S[XT >> 8 & 0xFF] & 0xFF) << 8 | S[XT & 0xFF] & 0xFF;
            XT = X[0] ^ XT ^ Integer.rotateLeft(XT, 2) ^ Integer.rotateLeft(XT, 10) ^ Integer.rotateLeft(XT, 18) ^ Integer.rotateLeft(XT, 24);
            X[0] = X[1];
            X[1] = X[2];
            X[2] = X[3];
            X[3] = XT;
        }
        Utils.Reverse(X);
        Utils.IntegerToByteArray(output, 0, X, 0, 4, ByteOrder.BIG_ENDIAN);
    }

    public void close() throws IOException {
        Arrays.fill(this.mSK, 0);
        super.close();
    }

    private static final byte[] S = new byte[] {
            -42, -112, -23, -2, -52, -31, 61, -73, 22, -74,
            20, -62, 40, -5, 44, 5, 43, 103, -102, 118,
            42, -66, 4, -61, -86, 68, 19, 38, 73, -122,
            6, -103, -100, 66, 80, -12, -111, -17, -104, 122,
            51, 84, 11, 67, -19, -49, -84, 98, -28, -77,
            28, -87, -55, 8, -24, -107, Byte.MIN_VALUE, -33, -108, -6,
            117, -113, 63, -90, 71, 7, -89, -4, -13, 115,
            23, -70, -125, 89, 60, 25, -26, -123, 79, -88,
            104, 107, -127, -78, 113, 100, -38, -117, -8, -21,
            15, 75, 112, 86, -99, 53, 30, 36, 14, 94,
            99, 88, -47, -94, 37, 34, 124, 59, 1, 33,
            120, -121, -44, 0, 70, 87, -97, -45, 39, 82,
            76, 54, 2, -25, -96, -60, -56, -98, -22, -65,
            -118, -46, 64, -57, 56, -75, -93, -9, -14, -50,
            -7, 97, 21, -95, -32, -82, 93, -92, -101, 52,
            26, 85, -83, -109, 50, 48, -11, -116, -79, -29,
            29, -10, -30, 46, -126, 102, -54, 96, -64, 41,
            35, -85, 13, 83, 78, 111, -43, -37, 55, 69,
            -34, -3, -114, 47, 3, -1, 106, 114, 109, 108,
            91, 81, -115, 27, -81, -110, -69, -35, -68, Byte.MAX_VALUE,
            17, -39, 92, 65, 31, 16, 90, -40, 10, -63,
            49, -120, -91, -51, 123, -67, 45, 116, -48, 18,
            -72, -27, -76, -80, -119, 105, -105, 74, 12, -106,
            119, 126, 101, -71, -15, 9, -59, 110, -58, -124,
            24, -16, 125, -20, 58, -36, 77, 32, 121, -18,
            95, 62, -41, -53, 57, 72 };

    private static final int[] FK = new int[] { -1548633402, 1453994832, 1736282519, -1301273892 };

    private static final int[] CK = new int[] {
            462357, 472066609, 943670861, 1415275113, 1886879365, -1936483679, -1464879427, -993275175, -521670923, -66909679,
            404694573, 876298825, 1347903077, 1819507329, -2003855715, -1532251463, -1060647211, -589042959, -117504499, 337322537,
            808926789, 1280531041, 1752135293, -2071227751, -1599623499, -1128019247, -656414995, -184876535, 269950501, 741554753,
            1213159005, 1684763257 };
}
