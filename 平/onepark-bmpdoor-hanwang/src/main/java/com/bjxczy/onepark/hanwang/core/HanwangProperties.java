package com.bjxczy.onepark.hanwang.core;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "hanwang")
public class HanwangProperties {
	
	private Boolean showLog;
	
	private Integer tcpPort;
	
	private Integer udpPort;

}
