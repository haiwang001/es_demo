package com.bjxczy.onepark.hanwang.sdk.security;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.SecureRandom;
/*
 *@ClassName Utils
 *@Author 温良伟
 *@Date 2023/3/29 14:56
 *@Version 1.0
 */
public class Utils {public static boolean IsNullOrEmpty(String value) {
    return (value == null || value.isEmpty());
}

    public static byte[] ToByteArray(long n) throws IOException {
        try(ByteArrayOutputStream stream = new ByteArrayOutputStream(8);

            DataOutputStream out = new DataOutputStream(stream)) {
            out.writeLong(n);
            return stream.toByteArray();
        }
    }

    public static byte[] GenerateRandom(int keySizeInBytes) {
        byte[] Key = new byte[keySizeInBytes];
        (new SecureRandom()).nextBytes(Key);
        return Key;
    }

    public static String ToString(byte[] input, int offset, int count) {
        StringBuilder sb = new StringBuilder(count << 1);
        for (int i = 0; i < count; i++) {
            byte b = input[offset++];
            sb.append(Integer.toHexString((b & 0xF0) >>> 4).toUpperCase());
            sb.append(Integer.toHexString(b & 0xF).toUpperCase());
        }
        return sb.toString();
    }

    public static String ToString(byte[] input) {
        return ToString(input, 0, input.length);
    }

    public static String ToString(byte[] input, int offset) {
        return ToString(input, offset, input.length - offset);
    }

    public static int ToInt32(byte[] value, int startIndex, ByteOrder order) {
        ByteBuffer buffer = ByteBuffer.allocate(4).order(order);
        buffer.put(value, startIndex, 4);
        return buffer.getInt(0);
    }

    public static byte[] GetBytes(int value, ByteOrder order) {
        ByteBuffer buffer = ByteBuffer.allocate(4).order(order);
        buffer.putInt(value);
        return buffer.array();
    }

    public static void IntegerToByteArray(byte[] destination, int destinationIndex, int[] source, int sourceIndex, int count, ByteOrder order) {
        if (order.equals(ByteOrder.LITTLE_ENDIAN)) {
            for (int i = 0; i < count; i++) {
                int n = source[sourceIndex++];
                destination[destinationIndex++] = (byte)(n & 0xFF);
                destination[destinationIndex++] = (byte)(n >> 8 & 0xFF);
                destination[destinationIndex++] = (byte)(n >> 16 & 0xFF);
                destination[destinationIndex++] = (byte)(n >>> 24);
            }
        } else {
            for (int i = 0; i < count; i++) {
                int n = source[sourceIndex++];
                destination[destinationIndex++] = (byte)(n >>> 24);
                destination[destinationIndex++] = (byte)(n >> 16 & 0xFF);
                destination[destinationIndex++] = (byte)(n >> 8 & 0xFF);
                destination[destinationIndex++] = (byte)(n & 0xFF);
            }
        }
    }

    public static void IntegerToByteArray(byte[] destination, int destinationIndex, int source, ByteOrder order) {
        if (order.equals(ByteOrder.LITTLE_ENDIAN)) {
            destination[destinationIndex++] = (byte)(source & 0xFF);
            destination[destinationIndex++] = (byte)(source >> 8 & 0xFF);
            destination[destinationIndex++] = (byte)(source >> 16 & 0xFF);
            destination[destinationIndex++] = (byte)(source >>> 24);
        } else {
            destination[destinationIndex++] = (byte)(source >>> 24);
            destination[destinationIndex++] = (byte)(source >> 16 & 0xFF);
            destination[destinationIndex++] = (byte)(source >> 8 & 0xFF);
            destination[destinationIndex++] = (byte)(source & 0xFF);
        }
    }

    public static void IntegerFromByteArray(int[] destination, int destinationIndex, byte[] source, int sourceIndex, int count, ByteOrder order) {
        if (order.equals(ByteOrder.LITTLE_ENDIAN)) {
            for (int i = 0; i < count; i += 4)
                destination[destinationIndex++] = source[sourceIndex++] & 0xFF | (source[sourceIndex++] & 0xFF) << 8 | (source[sourceIndex++] & 0xFF) << 16 | source[sourceIndex++] << 24;
        } else {
            for (int i = 0; i < count; i += 4)
                destination[destinationIndex++] = source[sourceIndex++] << 24 | (source[sourceIndex++] & 0xFF) << 16 | (source[sourceIndex++] & 0xFF) << 8 | source[sourceIndex++] & 0xFF;
        }
    }

    public static int IntegerFromByteArray(byte[] source, int sourceIndex, ByteOrder order) {
        if (order.equals(ByteOrder.LITTLE_ENDIAN))
            return source[sourceIndex++] & 0xFF | (source[sourceIndex++] & 0xFF) << 8 | (source[sourceIndex++] & 0xFF) << 16 | source[sourceIndex++] << 24;
        return source[sourceIndex++] << 24 | (source[sourceIndex++] & 0xFF) << 16 | (source[sourceIndex++] & 0xFF) << 8 | source[sourceIndex++] & 0xFF;
    }

    public static void Reverse(int[] array) {
        Reverse(array, 0, array.length);
    }

    public static void Reverse(int[] array, int offset, int count) {
        for (int LeftIndex = offset, RightIndex = offset + count - 1; LeftIndex < RightIndex; LeftIndex++, RightIndex--) {
            int value = array[LeftIndex];
            array[LeftIndex] = array[RightIndex];
            array[RightIndex] = value;
        }
    }

    public static byte[] toPrimitives(Byte[] source) {
        byte[] destination = new byte[source.length];
        int i = 0;
        for (Byte b : source)
            destination[i++] = b.byteValue();
        return destination;
    }

    public static byte[] asUnsignedByteArray(BigInteger value) {
        byte[] bytes = value.toByteArray();
        if (bytes[0] == 0) {
            byte[] tmp = new byte[bytes.length - 1];
            System.arraycopy(bytes, 1, tmp, 0, tmp.length);
            return tmp;
        }
        return bytes;
    }

    public static boolean IsZeroForAll(byte[] source) {
        for (byte b : source) {
            if (b != 0)
                return false;
        }
        return true;
    }
}
