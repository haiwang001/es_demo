package com.bjxczy.onepark.hanwang.sdk.security;


import lombok.Data;

/*
 *@ClassName IntegerWrapper
 *@Author 温良伟
 *@Date 2023/3/29 14:35
 *@Version 1.0
 */
@Data
public class IntegerWrapper {
    public int value;
}
