package com.bjxczy.onepark.hanwang.sdk.security;

import java.math.BigInteger;

/*
 *@ClassName SM2KeyExchangeInformation
 *@Author 温良伟
 *@Date 2023/3/29 14:39
 *@Version 1.0
 */
public class SM2KeyExchangeInformation {
    public BigInteger PrivateKey;

    public ECPoint PublicKey;

    public BigInteger r;

    public ECPoint R;

    public byte[] Z;

    public byte[] PartnerZ;

    public ECPoint PartnerPublicKey;

    public ECPoint PartnerR;

    public byte[] S1;

    public byte[] S2;

    public byte[] PartnerS;
}
