package com.bjxczy.onepark.hanwang.core;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.springframework.beans.factory.annotation.Autowired;

import com.bjxczy.onepark.hanwang.pojo.dto.ParamsCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UdpServerHandler extends IoHandlerAdapter {

	@Autowired
	private HanwangProperties properties;

	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		if (properties.getShowLog()) {
			log.info("[UDP Handler] sessionId={}, message={}", session.getId(), message.toString());
		}
		// 回复设备心跳
		session.write(new ParamsCommand("RETURN", "DevStatus")
				.put("result", "success")
				.put("reason", "")
				.put("command","PostRequest").toString());
	}

}
