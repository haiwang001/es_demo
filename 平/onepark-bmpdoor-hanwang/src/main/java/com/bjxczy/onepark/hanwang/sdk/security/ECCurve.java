package com.bjxczy.onepark.hanwang.sdk.security;


import java.math.BigInteger;
import java.util.Arrays;
/*
 *@ClassName ECCurve
 *@Author 温良伟
 *@Date 2023/3/29 14:30
 *@Version 1.0
 */
public abstract class ECCurve {
    protected ECFieldElement a;

    protected ECFieldElement b;

    public abstract int getFieldSize();

    public abstract ECFieldElement FromBigInteger(BigInteger paramBigInteger);

    public abstract ECPoint CreatePoint(BigInteger paramBigInteger1, BigInteger paramBigInteger2, boolean paramBoolean);

    public abstract ECPoint getInfinity();

    protected abstract ECPoint DecompressPoint(int paramInt, BigInteger paramBigInteger);

    public ECFieldElement getA() {
        return this.a;
    }

    public ECFieldElement getB() {
        return this.b;
    }

    public ECPoint DecodePoint(byte[] encoded, int offset, IntegerWrapper handleLength) {
        BigInteger X, Y;
        int FieldSizeInBytes = getFieldSize() + 7 >> 3;
        byte PC = encoded[offset];
        switch (PC) {
            case 0:
                handleLength.value = 1;
                return getInfinity();
            case 2:
            case 3:
                handleLength.value = FieldSizeInBytes + 1;
                if (offset + handleLength.value > encoded.length)
                    return null;
                X = new BigInteger(1, Arrays.copyOfRange(encoded, offset + 1, offset + 1 + FieldSizeInBytes));
                return DecompressPoint(PC & 0x1, X);
            case 4:
            case 6:
            case 7:
                handleLength.value = (FieldSizeInBytes << 1) + 1;
                if (offset + handleLength.value > encoded.length)
                    return null;
                X = new BigInteger(1, Arrays.copyOfRange(encoded, offset + 1, offset + 1 + FieldSizeInBytes));
                Y = new BigInteger(1, Arrays.copyOfRange(encoded, offset + 1 + FieldSizeInBytes, offset + 1 + FieldSizeInBytes + FieldSizeInBytes));
                return new FpPoint(this, FromBigInteger(X), FromBigInteger(Y), false);
        }
        handleLength.value = 0;
        return null;
    }
}
