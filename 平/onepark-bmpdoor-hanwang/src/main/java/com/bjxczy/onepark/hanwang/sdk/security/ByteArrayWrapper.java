package com.bjxczy.onepark.hanwang.sdk.security;


import lombok.Data;

/*
 *@ClassName ByteArrayWrapper
 *@Author 温良伟
 *@Date 2023/3/29 14:40
 *@Version 1.0
 */
@Data
public class ByteArrayWrapper {
    public byte[] data;
}
