package com.bjxczy.onepark.hanwang.interfaces.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.bjxczy.core.rabbitmq.event.door.RemoveAuthorityPayload;
import com.bjxczy.core.rabbitmq.event.door.UpdateAuthorityPayload;
import com.bjxczy.onepark.hanwang.core.TcpServerHandler;
import com.bjxczy.onepark.hanwang.pojo.dto.ParamsCommand;
import com.bjxczy.onepark.hanwang.service.AuthorityService;

@RestController
@RequestMapping("/authority")
public class CommandController {
	
	@Autowired
	private TcpServerHandler tcpHandler;
	@Autowired
	private AuthorityService authorityService;
	
	@PostMapping(value = "/test/command/{sn}", name = "测试命令")
	public void testCommand(@PathVariable("sn") String sn, @RequestBody JSONObject body) {
		tcpHandler.addAuthorityCommand(sn, new ParamsCommand(body));
	}
	
	@PostMapping(value = "/update", name = "更新授权")
	public void updateAuthority(@RequestBody UpdateAuthorityPayload payload) {
		authorityService.updateAuthority(payload, result -> {
			System.out.println(result);
		});
	}
	
	@PostMapping(value = "/remove", name = "删除授权")
	public void removeAuthority(@RequestBody RemoveAuthorityPayload payload) { 
		authorityService.removeAuthority(payload, result -> {
			System.out.println(result);
		});
	}

}
