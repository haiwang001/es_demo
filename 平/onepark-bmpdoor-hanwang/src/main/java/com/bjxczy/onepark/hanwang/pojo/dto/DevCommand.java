package com.bjxczy.onepark.hanwang.pojo.dto;

import java.util.Objects;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class DevCommand {
	
	private JSONObject message;

	public DevCommand(Object message) {
		this.message = JSON.parseObject(message.toString());
	}
	
	public String getCommand() {
		return message.getString("COMMAND");
	}
	
	public JSONObject getParams() {
		return message.getJSONObject("PARAM");
	}
	
	public <T> T toJavaObject(Class<T> clazz) {
		return message.getJSONObject("PARAM").toJavaObject(clazz);
	}
	
	public boolean isReturn() {
		return this.message != null && Objects.equals("Return", this.getCommand());
	}
	
	public boolean isSuccess() {
		boolean success = false;
		if (isReturn()) {
			String result = this.getParams().getString("result");
			success = Objects.equals("success", result);
		}
		return success;
	}

	@Override
	public String toString() {
		return message.toJSONString();
	}

}
