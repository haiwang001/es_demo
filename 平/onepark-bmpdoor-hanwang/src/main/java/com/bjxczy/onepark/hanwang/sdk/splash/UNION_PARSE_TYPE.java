package com.bjxczy.onepark.hanwang.sdk.splash;


import lombok.Data;

/*
 *@ClassName UNION_PARSE_TYPE
 *@Author 温良伟
 *@Date 2023/3/29 15:16
 *@Version 1.0
 */
@Data
public class UNION_PARSE_TYPE {
    public String COMMAND;

    public String RETURN;
}
