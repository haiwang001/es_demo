package com.bjxczy.onepark.hanwang.sdk.splash;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.Arrays;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import com.bjxczy.onepark.hanwang.sdk.security.CipherDirection;
import com.bjxczy.onepark.hanwang.sdk.security.CipherMode;
import com.bjxczy.onepark.hanwang.sdk.security.PaddingMode;
import com.bjxczy.onepark.hanwang.sdk.security.SM3;
import com.bjxczy.onepark.hanwang.sdk.security.SM4;
import com.bjxczy.onepark.hanwang.sdk.security.Utils;
/*
 *@ClassName FaceReaderEncoderAdapter
 *@Author 温良伟
 *@Date 2023/3/29 15:18
 *@Version 1.0
 */
public class FaceReaderEncoderAdapter implements ProtocolEncoder {
    private static final AttributeKey KEY_ENCRYPTOR = new AttributeKey(FaceReaderEncoderAdapter.class, "Encryptor");

    public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
        if (Utils.IsNullOrEmpty((String)message))
            return;
        byte[] Source = message.toString().getBytes("UTF-8");
        SM4 Encryptor = (SM4)session.getAttribute(KEY_ENCRYPTOR);
        if (Encryptor != null)
            Source = Encryptor.TransformFinalBlock(Source, 0, Source.length);
        IoBuffer Buffer = IoBuffer.allocate(Source.length + 4);
        Buffer.put(Utils.GetBytes(Source.length, ByteOrder.BIG_ENDIAN));
        Buffer.put(Source);
        Buffer.flip();
        out.write(Buffer);
    }

    public void dispose(IoSession session) throws Exception {
        Closeable Encryptor = (Closeable)session.getAttribute(KEY_ENCRYPTOR);
        if (Encryptor != null)
            Encryptor.close();
        session.removeAttribute(KEY_ENCRYPTOR);
    }

    public static void setSecretKey(IoSession session, byte[] secretKey, int mode) throws IOException {
        SM4 Encryptor = (SM4)session.getAttribute(KEY_ENCRYPTOR);
        if (Encryptor != null) {
            Encryptor.close();
            session.removeAttribute(KEY_ENCRYPTOR);
        }
        if (secretKey != null) {
            byte[] DerivedVector;
            Encryptor = new SM4(CipherDirection.Encryption, CipherMode.OFB, PaddingMode.PKCS7);
            if (mode == 0 && secretKey.length == 32) {
                DerivedVector = secretKey;
            } else {
                DerivedVector = SM3.KDF(secretKey, 32);
            }
            Encryptor.Initialize(Arrays.copyOfRange(DerivedVector, 0, 16), Arrays.copyOfRange(DerivedVector, 16, 32));
            session.setAttribute(KEY_ENCRYPTOR, Encryptor);
        }
    }
}
