package com.bjxczy.onepark.hanwang.sdk.security;


/*
 *@ClassName CipherMode
 *@Author 温良伟
 *@Date 2023/3/29 14:53
 *@Version 1.0
 */
public enum CipherMode {
    ECB(0),
    CBC(1),
    CFB(2),
    OFB(3),
    PCBC(4),
    CTR(5),
    ECBCTS(100),
    CBCCTS(101);

    private final int value;

    public int getValue() {
        return this.value;
    }

    CipherMode(int value) {
        this.value = value;
    }
}
