package com.bjxczy.onepark.hanwang.service.impl;

import java.util.ArrayList;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjxczy.core.rabbitmq.event.door.AuthorityDeviceDTO;
import com.bjxczy.core.rabbitmq.event.door.AuthorityResultPayload;
import com.bjxczy.core.rabbitmq.event.door.RemoveAuthorityPayload;
import com.bjxczy.core.rabbitmq.event.door.RemoveStaffAuthDTO;
import com.bjxczy.core.rabbitmq.event.door.AddStaffAuthDTO;
import com.bjxczy.core.rabbitmq.event.door.UpdateAuthorityPayload;
import com.bjxczy.onepark.common.util.ImageUtils;
import com.bjxczy.onepark.hanwang.core.TcpServerHandler;
import com.bjxczy.onepark.hanwang.pojo.dto.ParamsCommand;
import com.bjxczy.onepark.hanwang.service.AuthorityService;

@Service
public class AuthorityServiceImpl implements AuthorityService {

	@Autowired
	private TcpServerHandler tcpServerHandler;

	/**
	 * 批量授权
	 */
	@Override
	public void updateAuthority(UpdateAuthorityPayload payload, Consumer<AuthorityResultPayload> callback) {
		for (AuthorityDeviceDTO device : payload.getDoorList()) {
			for (AddStaffAuthDTO staff : payload.getStaffList()) {
				tcpServerHandler.addAuthorityCommand(device.getDeviceCode(), toSetEmployee(device.getDeviceCode(), staff, callback));
			}
		}
	}
	
	/**
	 * 批量删除授权
	 */
	@Override
	public void removeAuthority(RemoveAuthorityPayload payload, Consumer<AuthorityResultPayload> callback) {
		for (AuthorityDeviceDTO device : payload.getDoorList()) {
			for (RemoveStaffAuthDTO staff : payload.getStaffList()) {
				tcpServerHandler.addAuthorityCommand(device.getDeviceCode(), toDeleteEmployee(device.getDeviceCode(), staff, callback));
			}
		}
	}

	/**
	 * 平台下方人员模板
	 * @param sn
	 * @param staff
	 * @param callback 
	 * @return
	 * recogPermission: 设备权限
	 * 0=仅人脸
	 * 1=人脸+IC卡
	 * 2=人脸或IC卡
	 * 3=仅IC卡
	 * 4=人脸+ID卡
	 * 5=人脸或ID卡
	 * 6=IC卡+ID卡
	 * 7=IC卡或ID卡
	 * 8=设备认证（设置中可动态选择，即设备设置中选的是哪一个，就按照哪一个）
	 */
	public ParamsCommand toSetEmployee(String sn, AddStaffAuthDTO staff, Consumer<AuthorityResultPayload> callback) {
		String command = "SetEmployee";
		return new ParamsCommand(command).put("command", command)
				.setCallback(callback)
				.put("id", staff.getStaffId())
				.put("name", staff.getStaffName())
				.put("capturejpg", ImageUtils.imgFile2Base64String(staff.getFaceUrl()))
				.put("face_data", new ArrayList<>())
				.put("userType", 1) // 人员类型, 1=员工，2=访客，3=黑名单，4=业主
				.put("job_num", staff.getEmployeeNo())
				.put("recogPermission", 0)
				.put("icCard", staff.getAccessCode())
				.put("throughStartTime", "2000-01-01 00:00:00")
				.put("throughFinisthTime", "2099-01-01 00:00:00")
				.put("accessCard", staff.getAccessCode());

	}


	/**
	 *  平台删除人员
	 * @param deviceCode
	 * @param staff
	 * @param callback 
	 * @return
	 */
	private ParamsCommand toDeleteEmployee(String deviceCode, RemoveStaffAuthDTO staff,
			Consumer<AuthorityResultPayload> callback) {
		return new ParamsCommand("RETURN", "GetRequest")
				.setCallback(callback)
				.put("command", "DeleteEmployee")
				.put("id", staff.getStaffId());
	}

}
