package com.bjxczy.onepark.hanwang.sdk.splash;


import lombok.Data;

/*
 *@ClassName ManualResetEvent
 *@Author 温良伟
 *@Date 2023/3/29 15:17
 *@Version 1.0
 */
@Data
public class ManualResetEvent {
    private final Object monitor = new Object();

    private volatile boolean open = false;

    public ManualResetEvent(boolean initialState) {
        this.open = initialState;
    }

    public boolean WaitOne() throws InterruptedException {
        synchronized (this.monitor) {
            if (!this.open)
                this.monitor.wait();
            return this.open;
        }
    }

    public boolean WaitOne(long timeout) throws InterruptedException {
        synchronized (this.monitor) {
            if (!this.open)
                this.monitor.wait(timeout);
            return this.open;
        }
    }

    public void Set() {
        synchronized (this.monitor) {
            this.open = true;
            this.monitor.notifyAll();
        }
    }

    public void Reset() {
        this.open = false;
    }
}
