package com.bjxczy.onepark.hanwang.sdk.splash;


import lombok.Data;

/*
 *@ClassName PARAM_COMMAND_AKE_TYPE
 *@Author 温良伟
 *@Date 2023/3/29 15:14
 *@Version 1.0
 */
@Data
public class PARAM_COMMAND_AKE_TYPE {
    public String sn;

    public String[] key;
}
