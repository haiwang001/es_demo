package com.bjxczy.onepark.hanwang.sdk.security;


import lombok.Data;

import java.math.BigInteger;

/*
 *@ClassName ECLicenseKey
 *@Author 温良伟
 *@Date 2023/3/29 14:41
 *@Version 1.0
 */
@Data
public class ECLicenseKey {
    public final BigInteger mKey;

    public final BigInteger mHash;

    public ECLicenseKey(BigInteger key, BigInteger hash) {
        this.mKey = key;
        this.mHash = hash;
    }
}
