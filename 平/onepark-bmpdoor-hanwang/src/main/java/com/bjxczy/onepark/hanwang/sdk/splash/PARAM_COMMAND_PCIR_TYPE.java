package com.bjxczy.onepark.hanwang.sdk.splash;


import lombok.Data;

/*
 *@ClassName PARAM_COMMAND_PCIR_TYPE
 *@Author 温良伟
 *@Date 2023/3/29 15:14
 *@Version 1.0
 */
@Data
public class PARAM_COMMAND_PCIR_TYPE {
    public String id;

    public String name;

    public String sex;

    public String nation;

    public String birth;

    public String address;

    public String police;

    public String validate;

    public String type;

    public String alg_edition;

    public String idcardjpg;

    public String capturejpg;

    public String score;

    public String pass;

    public String threshold;

    public String time;
}
