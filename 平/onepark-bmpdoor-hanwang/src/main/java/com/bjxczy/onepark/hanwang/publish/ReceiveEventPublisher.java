package com.bjxczy.onepark.hanwang.publish;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.lang.UUID;
import com.bjxczy.core.rabbitmq.event.door.BusinessType;
import com.bjxczy.core.rabbitmq.event.door.InOrOutRecordPayload;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.onepark.common.util.ImageUtils;
import com.bjxczy.onepark.hanwang.core.HanwangProperties;
import com.bjxczy.onepark.hanwang.feign.client.BmpPcsFeignClient;
import com.bjxczy.onepark.hanwang.pojo.dto.DevCommand;
import com.bjxczy.onepark.hanwang.pojo.vo.RecogniseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Slf4j
@Component
public class ReceiveEventPublisher extends DoorMutualBaseListener {

    @Autowired
    private HanwangProperties properties;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private BmpPcsFeignClient fileClient;

    public void receive(DevCommand dcmd) {
        if (properties.getShowLog()) {
            log.info("[Received] dcmd={}", dcmd);
        }
        if ("RecogniseResult".equals(dcmd.getCommand())) {
            // 设备上传卡点记录
            RecogniseResult recognise = dcmd.toJavaObject(RecogniseResult.class);
            DateTime datetime = DateTime.of(recognise.getTime(), "yyyy-MM-dd HH:mm:ss");
            InOrOutRecordPayload payload = new InOrOutRecordPayload(BusinessType.STAFF, recognise.getId(),
                    datetime.toJdkDate(), recognise.getSn(), recognise.getCapturejpg(), recognise.getAnimaHeat(),
                    recognise.getWearMask(),"FACE_SUCCESS",null);
            payload.setEventPictrue(uploadTopcsByBase64(recognise.getCapturejpg(),".jpg"));
            log.info("[设备上传卡点记录] payload={}", payload);
            rabbitTemplate.convertAndSend(DEFAULT_EXCAHNGE_NAME, PUSH_IN_OR_OUT_RECORD, payload);
        }
    }

    public String uploadTopcsByBase64(String base64, String suffix) {
        File file = null;
        file = ImageUtils.base64ToFile(UUID.fastUUID().toString(), suffix, base64);
        MultipartFile multipartFile = ImageUtils.fileToMultipartFile(file);
        String fastdfsUrl = fileClient.fileUpload(multipartFile);
        // 删除临时文件
        file.delete();
        return fastdfsUrl;
    }

}
