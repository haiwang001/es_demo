package com.bjxczy.onepark.common.utils;

import java.util.Map;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author zx
 * @package com.cmiot.onepark.video.hikface.util
 * @date 2021/12/14
 * @description 描述
 */
@Component
@Slf4j
public class HikfaceApiUtil {

    private static Map<String, String> config;

    public static void setConfig(Map<String, String> config) {
        HikfaceApiUtil.config = config;
    }
}
