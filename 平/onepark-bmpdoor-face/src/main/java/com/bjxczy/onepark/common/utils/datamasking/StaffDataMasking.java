package com.bjxczy.onepark.common.utils.datamasking;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.user.UserInformation;

public abstract class StaffDataMasking {

    private static final Integer LEADER_ID = 1;

    /**
     * 获取职级字段
     *
     * @return 员工职级
     */
    public abstract Integer getMaskingKey();

    /**
     * 判断职级是否为公司领导
     *
     * @return
     */
    public boolean isLeader() {
        return getMaskingKey() == StaffDataMasking.LEADER_ID;
    }

    private Set<DataMaskingColumn> valueSet() {
        Set<DataMaskingColumn> set = new HashSet<>();
        for (DataMaskingColumn item : DataMaskingColumn.values()) {
            if (item != DataMaskingColumn.UNKOWN) {
                set.add(item);
            }
        }
        return set;
    }

    /**
     * 执行数据脱敏
     *
     * @param sysUser
     * @param includeAdmin
     */
    public void dataMasking(UserInformation sysUser, boolean includeAdmin) {
        // boolean isAdmin = SysUserUtils.isAdmin(sysUser);
        Set<DataMaskingColumn> allColumn = this.valueSet();
        if (includeAdmin) {
            Class<? extends StaffDataMasking> clazz = this.getClass();
            // N 移除忽略字段
            DataMaskingEntity dataMaskingEntity = clazz.getAnnotation(DataMaskingEntity.class);
            if (dataMaskingEntity == null) {
                throw new ResultException("[DataMasking] 实体未添加注解@DataMaskingEntity");
            }
            for (DataMaskingColumn e : dataMaskingEntity.exclude()) {
                allColumn.remove(e);
            }
            // N 判断脱敏字段
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields) {
                DataMaskingField dmfield = f.getAnnotation(DataMaskingField.class);
                if (dmfield != null) {
                    try {
                        DataMaskingHandler handler = dmfield.typeHandler().newInstance();
                        f.setAccessible(true);
                        String value = f.get(this) == null ? null : f.get(this).toString();
                        if (StringUtils.isNotBlank(value)) {
                            // N 领导特殊脱敏
                            value = isLeader() ? handler.getLeaderMaskingValue(value) : handler.getMaskingValue(value);
                            f.set(this, value);
                        }
                        if (dmfield.value() != DataMaskingColumn.UNKOWN) {
                            allColumn.remove(dmfield.value());
                        }
                    } catch (InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (allColumn.size() > 0) {
                throw new ResultException("[DataMasking] 存在需脱敏字段未处理！" + allColumn.toString());
            }
        }
    }
}
