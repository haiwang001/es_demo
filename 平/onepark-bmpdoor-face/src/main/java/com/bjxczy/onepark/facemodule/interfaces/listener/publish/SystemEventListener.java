package com.bjxczy.onepark.facemodule.interfaces.listener.publish;

import com.bjxczy.onepark.common.constant.FaceSysConst;
import com.bjxczy.onepark.facemodule.event.AddFaceEvent;
import com.bjxczy.onepark.facemodule.event.DelFaceEvent;
import com.bjxczy.onepark.facemodule.event.PudFaceEvent;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;


@Log4j2
@Component
public class SystemEventListener {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 人脸新增事件发布
     *
     * @param Event
     */
    @EventListener
    public void addStaffEventHandler(AddFaceEvent Event) {
        rabbitTemplate.convertAndSend(
                FaceSysConst.EXCHANGE_NAME,
                FaceSysConst.ADD_FACE_EVENT,
                Event.getTransmit());
    }

    /**
     * 修改事件发布
     *
     * @param Event
     */
    @EventListener
    public void updateStaffEventHandler(PudFaceEvent Event) {
        rabbitTemplate.convertAndSend(
                FaceSysConst.EXCHANGE_NAME,
                FaceSysConst.PUT_FACE_EVENT,
                Event.getTransmit());
    }

    /**
     * 删除事件发布
     *
     * @param Event
     */
    @EventListener
    public void removeStaffEventHandler(DelFaceEvent Event) {
        rabbitTemplate.convertAndSend(
                FaceSysConst.EXCHANGE_NAME,
                FaceSysConst.DEL_FACE_EVENT,
                Event.getTransmit());
    }


}
