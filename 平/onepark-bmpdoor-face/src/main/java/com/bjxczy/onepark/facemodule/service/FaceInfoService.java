package com.bjxczy.onepark.facemodule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bjxczy.onepark.common.model.door.StaffFaceInformation;
import com.bjxczy.onepark.common.model.organization.OrganizationTree;
import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.facemodule.entity.FaceInfo;
import com.bjxczy.onepark.facemodule.pojo.dto.FaceInfoDto;
import com.bjxczy.onepark.facemodule.pojo.dto.command.CreateFaceCommand;
import com.bjxczy.onepark.facemodule.pojo.dto.command.RemoveFaceCommand;
import com.bjxczy.onepark.facemodule.pojo.dto.command.UpdateFaceCommand;
import com.bjxczy.onepark.facemodule.pojo.vo.FaceExportVo;

import java.util.List;

/**
 * 人脸信息相关接口
 *
 * @Author <a href="mailto:17303233782@163.com">mengxin</a>
 * @Version V1.4.2
 * @Date 2023/06/29
 */
public interface FaceInfoService extends IService<FaceInfo> {

    /**
     * 查询人脸信息列表
     *
     * @param dto 传输对象
     * @return 人脸列表
     */
    R queryFaceListPage(FaceInfoDto dto);

    /**
     * 导出时查询人脸信息
     * 
     * @param dto 条件
     * @return List<FaceInfo>
     */
    List<FaceExportVo> getFaceExportInfo(FaceInfoDto dto);

    /**
     * 根据id查询人脸信息
     *
     * @param id 主键ID
     * @return 人脸信息
     */
    FaceInfo queryFaceById(Integer id);

    /**
     * 根据id查询员工信息
     *
     * @param idList
     */
    List<StaffFaceInformation> listInformationByIds(List<Integer> idList);

    /**
     * 根据员工id查询员工信息
     * 
     * @param staffId
     * @return FaceInfo
     */
    FaceInfo queryFaceInfoByStaffId(Integer staffId);

    /**
     * TODO
     * 
     * @param id
     * @return
     */
    StaffFaceInformation getInformationById(Integer id);

    /**
     * 获取组织机构树
     * 
     * @return List<OrganizationTree>
     */
    List<OrganizationTree> getOrganizationTree();

    /**
     * TODO
     * 
     * @param staffInfomation 用户对象
     * @return 返回操作结果
     */
    boolean updateFaceByStaffId(StaffInformation staffInfomation);

    /**
     * 删除凭证
     * 
     * @param user
     * @return
     */
    R delMyCertificate(UserInformation user);

    /**
     * 通过mobile进行逻辑删除
     *
     * @param RemoveStaffInfo 手机号
     * @return 返回是否成功
     */
    boolean deleteFaceByStaff(RemoveStaffInfo RemoveStaffInfo);

    /**
     * 创建人脸
     *
     * @param command 人脸信息
     * @param user 用户
     * @return 是否成功
     */
    R createFace(CreateFaceCommand command, UserInformation user);

    /**
     * 修改人脸
     *
     * @param command 人脸信息
     * @return
     */
    R updateFace(UpdateFaceCommand command);


    /**
     * 删除人脸信息
     *
     * @param command 删除命令
     */
    R delFace(RemoveFaceCommand command);
}
