package com.bjxczy.onepark.facemodule.pojo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author ：yubaowang @description：
 * @date ：2022/7/27 14:21
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class FaceInfoDto extends BasePageDTO {
    /**
    * 描述：综合查询
    * 是否必填：
    **/
    private String unifyParam;

    /**
    * 描述：职级
    * 是否必填：
    **/
    private Integer rankId;

    /**
    * 描述：部门
    * 是否必填：
    **/
    private Integer deptId;

    /**
    * 描述：部门ids
    * 是否必填：
    **/
    private List<Integer> deptIds;

    /**
    * 描述：人员类型
    * 是否必填：
    **/
    private String comeFrom;
}
