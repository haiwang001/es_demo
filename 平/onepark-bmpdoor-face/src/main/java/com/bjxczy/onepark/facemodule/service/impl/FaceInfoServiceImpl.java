package com.bjxczy.onepark.facemodule.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.onepark.common.model.common.DictInformation;
import com.bjxczy.onepark.common.model.door.StaffFaceInformation;
import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.bjxczy.onepark.common.model.organization.OrganizationTree;
import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.facemodule.entity.FaceInfo;
import com.bjxczy.onepark.facemodule.event.AddFaceEvent;
import com.bjxczy.onepark.facemodule.event.DelFaceEvent;
import com.bjxczy.onepark.facemodule.event.PudFaceEvent;
import com.bjxczy.onepark.facemodule.interfaces.feign.client.OrganizationFeignClient;
import com.bjxczy.onepark.facemodule.interfaces.feign.client.QueryStaffServer;
import com.bjxczy.onepark.facemodule.mapper.FaceInfoMapper;
import com.bjxczy.onepark.facemodule.pojo.dto.FaceInfoDto;
import com.bjxczy.onepark.facemodule.pojo.dto.command.CreateFaceCommand;
import com.bjxczy.onepark.facemodule.pojo.dto.command.RemoveFaceCommand;
import com.bjxczy.onepark.facemodule.pojo.dto.command.UpdateFaceCommand;
import com.bjxczy.onepark.facemodule.pojo.vo.FaceExportVo;
import com.bjxczy.onepark.facemodule.service.FaceInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 人脸信息业务层
 *
 * @Author <a href="mailto:17303233782@163.com">mengxin</a>
 * @Version V1.4.2
 * @Date 2023/06/29
 */
@Service
@Slf4j
public class FaceInfoServiceImpl extends ServiceImpl<FaceInfoMapper, FaceInfo> implements FaceInfoService {

    private static final int ARRAY_INCISE_DEFAULT_SIZE = 100;
    private static final String DEPT_LEVEL_SEPARATOR = "/";
    private final QueryWrapper<FaceInfo> queryWrapper = new QueryWrapper<>();

    @Resource
    private FaceInfoMapper faceInfoMapper;

    @Autowired
    private OrganizationFeignClient organizationFeignClient;

    @Autowired
    private QueryStaffServer staffServer;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private DictService dictService;

    @Value("${dict.come-from}")
    private String comeFromCode;

    /**
     * 查询人脸信息列表
     *
     * @param dto 传输对象
     * @return 人脸列表
     */
    @Override
    public R queryFaceListPage(FaceInfoDto dto) {
        if (dto.getDeptId() != null && dto.getDeptId() != 0) {
            List<Integer> integers = staffServer.listOrgChildrenId(dto.getDeptId());
            dto.setDeptIds(integers);
        }
        IPage<FaceInfo> faceInfoIPage = this.baseMapper.page(dto.toPage(), dto);
        if (CollectionUtils.isNotEmpty(faceInfoIPage.getRecords())) {
            List<DictInformation> dictInformations = dictService.listDict(comeFromCode);
            if (CollectionUtils.isEmpty(dictInformations)) {
                return R.success(faceInfoIPage);
            }
            for (FaceInfo record : faceInfoIPage.getRecords()) {
                for (DictInformation dictInformation : dictInformations) {
                    if (record.getComeFrom()!=null&&String.valueOf(record.getComeFrom()).equals(dictInformation.getValue())) {
                        record.setComeFromName(dictInformation.getLabel());
                    }
                }
            }
        }
        return R.success(faceInfoIPage);
    }

    /**
     * 获取人脸导出信息
     *
     * @param dto 条件
     * @return List<FaceInfo>
     */
    @Override
    public List<FaceExportVo> getFaceExportInfo(FaceInfoDto dto) {
        List<FaceInfo> faceInfos = this.baseMapper.page(dto);
        if (CollectionUtils.isEmpty(faceInfos)) {
            return new ArrayList<>();
        }
        List<FaceExportVo> ret = faceInfos.stream().map(x -> {
            FaceExportVo faceExportVo = new FaceExportVo();
            BeanUtils.copyProperties(x, faceExportVo);
            return faceExportVo;
        }).collect(Collectors.toList());
        List<DictInformation> dictInformations = dictService.listDict(comeFromCode);
        if (CollectionUtils.isEmpty(dictInformations)) {
            return ret;
        }
        for (FaceExportVo faceInfo : ret) {
            for (DictInformation dictInformation : dictInformations) {
                if (faceInfo.getComeFrom()!=null&&String.valueOf(faceInfo.getComeFrom()).equals(dictInformation.getValue())) {
                    faceInfo.setComeFromName(dictInformation.getLabel());
                }
            }
        }
        return ret;
    }

    /**
     * 根据id查询人脸信息
     *
     * @param id 主键ID
     * @return 人脸信息
     */
    @Override
    public FaceInfo queryFaceById(Integer id) {
        FaceInfo byId = this.getById(id);
        List<DictInformation> dictInformations = dictService.listDict(comeFromCode);
        if (CollectionUtils.isEmpty(dictInformations)) {
            return byId;
        }
        for (DictInformation dictInformation : dictInformations) {
            if (byId.getComeFrom()!=null&&String.valueOf(byId.getComeFrom()).equals(dictInformation.getValue())) {
                byId.setComeFromName(dictInformation.getLabel());
            }
        }
        return byId;
    }

    /**
     * 获取组织机构树
     *
     * @return List<OrganizationTree>
     */
    @Override
    public List<OrganizationTree> getOrganizationTree() {
        List<OrganizationTree> orgTreeList = organizationFeignClient.findOrgTree();
        if (CollectionUtils.isEmpty(orgTreeList)) {
            return orgTreeList;
        }
        List<Integer> orgIdLists =  new ArrayList<>();
        List<FaceInfo> faceInfoList = new ArrayList<>();
        this.getOrgIds(orgIdLists,orgTreeList);
        if (CollectionUtils.isNotEmpty(orgIdLists)){
            faceInfoList.addAll(this.list(Wrappers.<FaceInfo>lambdaQuery().eq(FaceInfo::getDelFlag, 0).in(FaceInfo::getDeptId, orgIdLists)));
        }
        // 递归查找所有组织机构节点下的人脸信息
        for (OrganizationTree orgTree : orgTreeList) {
            setChild(orgTree, faceInfoList);
        }
        return orgTreeList;
    }

    public void getOrgIds(List<Integer> ret,List<OrganizationTree> orgs){
        if (CollectionUtils.isNotEmpty(orgs)){
            for (OrganizationTree org : orgs) {
                ret.add(org.getId());
                if (CollectionUtils.isNotEmpty(org.getChildren())){
                    getOrgIds(ret,org.getChildren());
                }
            }
        }
    }

    /**
     * 根据人脸id查询人脸信息并转换为员工信息返回
     *
     * @param id 人脸id
     * @return StaffFaceInformation 员工信息
     */
    @Override
    public StaffFaceInformation getInformationById(Integer id) {
        FaceInfo faceInfo = this.queryFaceById(id);
        return toInformation(faceInfo);
    }

    /**
     * 根据人脸id集合查询对应的人脸信息并转换为员工信息返回
     *
     * @param idList id集合
     * @return List<StaffFaceInformation> 员工信息
     */
    @Override
    public List<StaffFaceInformation> listInformationByIds(List<Integer> idList) {
        List<FaceInfo> list = this.list(Wrappers.<FaceInfo>lambdaQuery().eq(FaceInfo::getDelFlag, 0).in(FaceInfo::getStaffId, idList));
        return list.stream().map(e -> toInformation(e)).collect(Collectors.toList());
    }

    /**
     * 根据员工ID查询凭证信息
     *
     * @param staffId 条件
     * @return FaceInfo
     */
    @Override
    public FaceInfo queryFaceInfoByStaffId(Integer staffId) {
        FaceInfo faceInfo = this.getOne(Wrappers.<FaceInfo>lambdaQuery()
                .eq(FaceInfo::getDelFlag, 0)
                .eq(FaceInfo::getStaffId, staffId));
        return faceInfo;
    }

    /**
     * 根据员工ID修改人脸信息
     *
     * @param staffInfo 用户对象
     * @return boolean 是否成功
     */
    @Override
    public boolean updateFaceByStaffId(StaffInformation staffInfo) {
        FaceInfo faceInfo = queryFaceInfoByStaffId(staffInfo.getId());
        if (faceInfo==null||faceInfo.getId()==0||faceInfo.getId()==null){
            return Boolean.FALSE;
        }
        faceInfo.setEmployeeNo(staffInfo.getEmployeeNo());
        faceInfo.setStaffName(staffInfo.getFldName());
        faceInfo.setMobile(staffInfo.getMobile());
        faceInfo.setDeptId(staffInfo.getOrganizationId());
        faceInfo.setRankId(staffInfo.getRankId());
        faceInfo.setRankName(staffInfo.getRankName());
        faceInfo.setComeFrom(staffInfo.getComeFrom());
        faceInfo.setIdentifyCard(staffInfo.getIdentityCard());
        if (CollectionUtils.isNotEmpty(staffInfo.getDeptNames())) {
            String multiDeptName = staffInfo.getDeptNames().stream().map(x -> {
                return x.getDeptName();
            }).collect(Collectors.joining("/"));
            faceInfo.setDeptName(multiDeptName);
        }
        //人员离职的话删除人脸
        if (staffInfo.getDimission()==1){
            RemoveFaceCommand removeFaceCommand = new RemoveFaceCommand(faceInfo.getId());
            this.delFace(removeFaceCommand);
        }else {
            boolean update = this.updateById(faceInfo);
            // 发布一个修改事件
            if (update) {
                FaceInfoTransmit transmit = new FaceInfoTransmit();
                BeanUtils.copyProperties(faceInfo, transmit);
                transmit.setFldName(faceInfo.getStaffName());
                transmit.setIdentityCard(faceInfo.getIdentifyCard());
                publisher.publishEvent(new PudFaceEvent(this, transmit));
                return Boolean.TRUE;
            }
        }
        return Boolean.TRUE;
    }

    /**
     * 根据手机号删除人脸信息
     *
     * @param RemoveStaffInfo 手机号
     * @return 是否成功
     */
    @Override
    public boolean deleteFaceByStaff(RemoveStaffInfo RemoveStaffInfo) {
        FaceInfo faceInfo = queryFaceInfoByStaffId(RemoveStaffInfo.getId());
        RemoveFaceCommand removeFaceCommand = new RemoveFaceCommand(faceInfo.getId());
        this.delFace(removeFaceCommand);
        return Boolean.TRUE;
    }

    /**
     * 删除门禁卡信息
     *
     * @param user 当前用户信息
     * @return 是否成功
     */
    @Override
    public R delMyCertificate(UserInformation user) {
        FaceInfo faceInfo = this.getOne(Wrappers.<FaceInfo>lambdaQuery()
                .eq(FaceInfo::getDelFlag, 0)
                .eq(FaceInfo::getStaffId, user.getStaffId()));
        boolean b = this.removeById(faceInfo.getId());
        if (b) {
            FaceInfoTransmit transmit = new FaceInfoTransmit();
            BeanUtils.copyProperties(faceInfo, transmit);
            transmit.setFldName(faceInfo.getStaffName());
            transmit.setIdentityCard(faceInfo.getIdentifyCard());
            publisher.publishEvent(new DelFaceEvent(this, transmit));
            return R.success();
        }
        return R.fail();
    }

    /**
     * 查找组织机构下的人脸信息并将人脸信息转换为员工信息赋值给组织机构
     *
     * @param orgTree      组织机构
     * @param faceInfoList 全部的人脸信息
     */
    private void setChild(OrganizationTree orgTree, List<FaceInfo> faceInfoList) {
        orgTree.setStaffs(faceInfoList.stream().filter(v -> orgTree.getId().equals(v.getDeptId())).map(v -> {
            StaffInfoFine staffInfo = new StaffInfoFine();
            BeanUtils.copyProperties(v, staffInfo);
            staffInfo.setId(Convert.toInt(v.getStaffId()));
            staffInfo.setIdentityCard(v.getIdentifyCard());
            staffInfo.setFldName(v.getStaffName());
            List<String> faceImgList = new ArrayList<>(1);
            faceImgList.add(v.getFaceImg());
            staffInfo.setPhoto(faceImgList);
            return staffInfo;
        }).collect(Collectors.toList()));
        List<OrganizationTree> children = orgTree.getChildren();
        if (ObjectUtil.isNotEmpty(children)) {
            for (OrganizationTree child : children) {
                setChild(child, faceInfoList);
            }
        }
    }

    /**
     * 将人脸信息实体转换为员工信息实体
     *
     * @param po 人脸信息实体
     * @return StaffFaceInformation 员工信息实体
     */
    private StaffFaceInformation toInformation(FaceInfo po) {
        StaffFaceInformation info = new StaffFaceInformation();
        BeanUtils.copyProperties(po, info);
        info.setIdentityCard(po.getIdentifyCard());
        info.setOrganizationId(po.getDeptId());
        info.setDeptNames(po.getDeptName());
        info.setFaceUrl(po.getFaceImg());
        return info;
    }

    /**
     * 创建人脸
     *
     * @param command 人脸信息
     * @param user    用户
     * @return 是否成功
     */
    @Override
    public R createFace(CreateFaceCommand command, UserInformation user) {
        //人脸判重
        List<FaceInfo> list = this.list(Wrappers.<FaceInfo>lambdaQuery().eq(FaceInfo::getDelFlag, 0).eq(FaceInfo::getStaffId, command.getStaffId()));
        if (CollectionUtils.isNotEmpty(list)) {
            return R.fail("已存在人脸信息");
        }
        StaffInformation staffInfo = staffServer.findById(command.getStaffId());
        if (StringUtils.isBlank(staffInfo.getEmployeeNo())) {
            return R.fail("员工编号信息空!");
        }
        // 保存人脸信息
        FaceInfo faceInfo = new FaceInfo();
        BeanUtils.copyProperties(staffInfo, faceInfo);
        faceInfo.setId(null);
        faceInfo.setStaffId(command.getStaffId());
        faceInfo.setFaceImg(command.getFaceImg());
        faceInfo.setIdentifyCard(staffInfo.getIdentityCard());
        faceInfo.setStaffName(staffInfo.getFldName());
        faceInfo.setDeptId(staffInfo.getOrganizationId());
        if (CollectionUtils.isNotEmpty(staffInfo.getDeptNames())) {
            String multiDeptName = staffInfo.getDeptNames().stream().map(x -> {
                return x.getDeptName();
            }).collect(Collectors.joining("/"));
            faceInfo.setDeptName(multiDeptName);
        }
        boolean save = this.save(faceInfo);
        if (save) {
            FaceInfoTransmit transmit = new FaceInfoTransmit();
            BeanUtils.copyProperties(faceInfo, transmit);
            transmit.setFldName(faceInfo.getStaffName());
            transmit.setIdentityCard(faceInfo.getIdentifyCard());
            publisher.publishEvent(new AddFaceEvent(this, transmit));
            return R.success();
        }
        return R.fail();
    }

    /**
     * 修改人脸
     *
     * @param command 人脸信息
     * @return 1成功/-1失败
     */
    @Override
    public R updateFace(UpdateFaceCommand command) {
        FaceInfo byId = this.getById(command.getId());
        if (byId == null || byId.getId() == 0 || byId.getId() == null) {
            return R.fail("人脸不存在！");
        }
        //人脸判重
        List<FaceInfo> list = this.list(Wrappers.<FaceInfo>lambdaQuery()
                .eq(FaceInfo::getDelFlag, 0)
                .ne(FaceInfo::getId,byId.getId())
                .eq(FaceInfo::getStaffId, command.getStaffId()));
        if (CollectionUtils.isNotEmpty(list)) {
            return R.fail("已存在人脸！");
        }
        FaceInfo faceInfo = new FaceInfo();
        BeanUtils.copyProperties(byId, faceInfo);
        BeanUtils.copyProperties(command, faceInfo);
        boolean update = this.updateById(faceInfo);
        // 发布一个修改事件
        if (update) {
            FaceInfoTransmit transmit = new FaceInfoTransmit();
            BeanUtils.copyProperties(faceInfo, transmit);
            transmit.setFldName(faceInfo.getStaffName());
            transmit.setIdentityCard(faceInfo.getIdentifyCard());
            publisher.publishEvent(new PudFaceEvent(this, transmit));
            return R.success();
        }
        return R.fail();
    }

    /**
     * 删除人脸信息
     *
     * @param command 删除命令
     */
    @Override
    public R delFace(RemoveFaceCommand command) {
        FaceInfo byId = this.getById(command.getId());
        boolean b = this.removeById(command.getId());
        if (b) {
            FaceInfoTransmit transmit = new FaceInfoTransmit();
            BeanUtils.copyProperties(byId, transmit);
            transmit.setFldName(byId.getStaffName());
            transmit.setIdentityCard(byId.getIdentifyCard());
            publisher.publishEvent(new DelFaceEvent(this, transmit));
            return R.success();
        }
        return R.fail();
    }

}
