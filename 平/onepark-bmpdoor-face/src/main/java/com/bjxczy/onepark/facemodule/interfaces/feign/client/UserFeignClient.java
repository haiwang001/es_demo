package com.bjxczy.onepark.facemodule.interfaces.feign.client;


import com.bjxczy.core.feign.client.user.BaseUserFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

/*
 *@Author wlw
 *@Date 2023/4/16 20:45
 */
@FeignClient("user-server")
public interface UserFeignClient extends BaseUserFeignClient {
}
