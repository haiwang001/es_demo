package com.bjxczy.onepark.common.constant;

/**
 * @Author cx
 * @Date 2020/2/12 17:46
 */
public enum ErrorCodes {
    SUCCESS(105000000, "请求成功"),

    SYSTEM_ERROR(105100000, "系统错误"),

    PARAMETERS_ERROR(105100001, "请求参数错误"),

    VERIFICATION_CODE_ERROR(105100007, "验证码错误"),

    SMS_SEND_FAILE(105100010, "发送短信失败"),

    SMS_FREQUENCY(105100011, "发送短信过频，请稍后再试"),

    OPERATE_AUTHENTICATION_FAIL(11000, "鉴权失败"),

    OPERATE_INSUFFICIENT_PERMISSTION(11001, "权限不够"),

    OPERATE_PARAMETERS_WRONG_FORMAT(12000, "传递参数格式错误"),

    OPERATE_PARAMETERS_MISSING(12001, "缺少关键参数"),

    OPERATE_PARAMETERS_WRONG_VALUE(12002, "参数值错误"),

    DATABASE_DISCONNECTED(13000, "数据库无连接"),

    DATABASE_NO_RELATIVE_DATA(13001, "数据库无相关数据"),

    DATABASE_WRITE_FAIL(13002, "数据库写入失败"),

    DATABASE_DELETE_FAIL(13003, "数据库删除失败"),

    SERVICE_DISCONNECTED(20000, "无法访问服务"),

    SERVICE_OPERATE_FAIL(20001, "服务操作失败");

    private final int value;

    private final String desc;

    ErrorCodes(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public int getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

}
