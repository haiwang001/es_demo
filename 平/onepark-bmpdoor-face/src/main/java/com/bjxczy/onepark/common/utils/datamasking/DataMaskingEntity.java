package com.bjxczy.onepark.common.utils.datamasking;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface DataMaskingEntity {

    DataMaskingColumn[] exclude() default {};

}
