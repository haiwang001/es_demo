package com.bjxczy.onepark.facemodule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.facemodule.entity.FaceInfo;
import com.bjxczy.onepark.facemodule.pojo.dto.FaceInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 人脸信息DAO层
 *
 * @Author <a href="mailto:17303233782@163.com">mengxin</a>
 * @Version V1.4.2
 * @Date 2023/06/29
 */
@Component
@Mapper
public interface FaceInfoMapper extends BaseMapper<FaceInfo> {

    IPage<FaceInfo> page(Page<Object> toPage, @Param("dto") FaceInfoDto dto);

    List<FaceInfo> page(@Param("dto")FaceInfoDto dto);
}
