package com.bjxczy.onepark.facemodule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.bjxczy.core.web.base.UserOperator;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author qinyuan
 * @since 2023-07-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class FaceInfo extends UserOperator implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
    * 描述：员工id
    * 是否必填：
    **/
    private Integer staffId;

    /**
     * 描述：员工编号
     * 是否必填：
     **/
    private String employeeNo;

    /**
     * 描述：员工名称
     * 是否必填：
     **/
    private String staffName;

    /**
     * 描述：手机号
     * 是否必填：
     **/
    private String mobile;

    /**
     * 描述：部门id
     * 是否必填：
     **/
    private Integer deptId;

    /**
     * 描述：部门名称：XXX公司/XX部门/XX
     * 是否必填：
     **/
    private String deptName;

    /**
     * 描述：职级id
     * 是否必填：
     **/
    private Integer rankId;

    /**
     * 描述：职级名称
     * 是否必填：
     **/
    private String rankName;

    /**
     * 描述：人员类型：字典：comeFrom
     * 是否必填：
     **/
    private Integer comeFrom;

    /**
     * 描述：证件号
     * 是否必填：
     **/
    private String identifyCard;

    /**
     * 描述：人脸照片
     * 是否必填：
     **/
    private String faceImg;

    /**
    * 描述：来源名称
    * 是否必填：
    **/
    @TableField(exist = false)
    private String comeFromName;
}
