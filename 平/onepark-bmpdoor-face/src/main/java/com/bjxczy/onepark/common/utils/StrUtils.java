package com.bjxczy.onepark.common.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;

import com.bjxczy.onepark.common.model.organization.DeptNameInfo;

import cn.hutool.core.util.IdcardUtil;
import lombok.extern.log4j.Log4j2;

/*
 * import org.bytedeco.javacv.Frame; import org.bytedeco.javacv.Java2DFrameConverter; import
 * org.bytedeco.javacv.OpenCVFrameConverter; import org.bytedeco.opencv.opencv_core.Mat; import
 * org.bytedeco.opencv.opencv_core.Rect; import org.bytedeco.opencv.opencv_core.RectVector; import
 * org.bytedeco.opencv.opencv_objdetect.CascadeClassifier;
 */

/*
 * import org.bytedeco.javacv.Frame; import org.bytedeco.javacv.Java2DFrameConverter; import
 * org.bytedeco.javacv.OpenCVFrameConverter; import org.bytedeco.opencv.opencv_core.*; import
 * org.bytedeco.opencv.opencv_objdetect.CascadeClassifier;
 *
 * import javax.imageio.ImageIO; import java.awt.image.BufferedImage; import java.io.File; import java.io.IOException;
 *
 * import static org.bytedeco.opencv.global.opencv_imgproc.*;
 */

/**
 * @ClassName StringUtils
 * @Description TODO
 * @Author 温良伟
 * @Date 2023/1/19 15:16
 * @Version 1.0
 */
@Log4j2
public class StrUtils {

    public static File getFile(String url) throws Exception {
        // 对本地文件命名
        String fileName = url.substring(url.lastIndexOf("."), url.length());
        File file = null;

        URL urlfile;
        InputStream inStream = null;
        OutputStream os = null;
        try {
            // 注:这里的后缀可以根据你传入的url的图片类型后缀定义,
            // 这里因为项目业务需求,回调是pdf,所以写死了
            file = File.createTempFile("temp_image", fileName + ".pdf");
            // 下载
            urlfile = new URL(url);
            inStream = urlfile.openStream();
            os = new FileOutputStream(file);

            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != os) {
                    os.close();
                }
                if (null != inStream) {
                    inStream.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    /**
     * 根据身份证号判断性别 只支持 15 和 18位 身份证号
     *
     * @param idNumber 身份证
     * @return -1识别失败 1 男 2 女
     */
    public static Integer getGenderByIdCard(String idNumber) throws IllegalArgumentException {
        if (IdcardUtil.isValidCard(idNumber)) {
            int gender = 0;
            if (idNumber.length() == 18) {
                // 如果身份证号18位，取身份证号倒数第二位
                char c = idNumber.charAt(idNumber.length() - 2);
                gender = Integer.parseInt(String.valueOf(c));
            } else {
                // 如果身份证号15位，取身份证号最后一位
                char c = idNumber.charAt(idNumber.length() - 1);
                gender = Integer.parseInt(String.valueOf(c));
            }
            if (gender % 2 == 1) {
                return 1;
            } else {
                return 2;
            }
        }
        return -1;
    }

    public static List<DeptNameInfo> deptNameInfo(List<DeptNameInfo> list) {
        String str = "";
        for (int i = 0; i < list.size(); i++) {
            DeptNameInfo el = list.get(i);
            if (list.size() - 1 == i) {
                str = str + el.getDeptName();
                list.clear();
                DeptNameInfo deptNameInfo = new DeptNameInfo();
                deptNameInfo.setId(el.getId());
                deptNameInfo.setDeptName(str);
                list.add(deptNameInfo);
            } else {
                str = str + el.getDeptName() + "/";
            }
        }
        return list;
    }
}
