package com.bjxczy.onepark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableCaching
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication()
public class FaceApplication {
    public static void main(String[] args) {
        SpringApplication.run(FaceApplication.class, args);
        System.err.println(" --------------------------人脸模块运行成功--------------------------");
    }

}
