package com.bjxczy.onepark.facemodule.interfaces.feign;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bjxczy.core.feign.client.door.BaseFaceFeignClient;
import com.bjxczy.onepark.common.model.door.StaffFaceInformation;
import com.bjxczy.onepark.facemodule.service.FaceInfoService;

@Controller
@ResponseBody
public class FaceFeignController implements BaseFaceFeignClient {

    @Autowired
    private FaceInfoService faceInfoService;

    @Override
    public StaffFaceInformation getByStaffId(@PathVariable("id") Integer id) {
        return faceInfoService.getInformationById(id);
    }

    @Override
    public List<StaffFaceInformation> listByStaffId(@RequestBody List<Integer> idList) {
        return faceInfoService.listInformationByIds(idList);
    }

}
