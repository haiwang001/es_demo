package com.bjxczy.onepark.facemodule.interfaces.listener.subscribe;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bjxczy.core.rabbitmq.supports.OrganizationBaseListener;
import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.facemodule.service.FaceInfoService;
import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RabbitQueueListener extends OrganizationBaseListener {

    private final FaceInfoService faceInfoService;

    @Autowired
    public RabbitQueueListener(FaceInfoService faceInfoService) {
        this.faceInfoService = faceInfoService;
    }

    /**
     * @param staffInfomation 用户信息
     * @param channel         通道
     * @param message         消息 name 队列名 不要冲突 key 事件名 value 绑定的交换机名
     */
    @RabbitListener(bindings = {@QueueBinding(value = @Queue(name = "bmpface_pudate_face_queue", durable = "true"),
            key = OrganizationBaseListener.UPDATE_STAFF_EVENT,
            exchange = @Exchange(value = OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    public void updateStaffEventHandler(StaffInformation staffInfomation, Channel channel, Message message) {

        try {
            boolean isBoolean = faceInfoService.updateFaceByStaffId(staffInfomation);
            this.confirm(() -> isBoolean, channel, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param RemoveStaffInfo 用户信息
     * @param channel         通道
     * @param message         消息 name 队列名 不要冲突 key 事件名 value 绑定的交换机名
     */
    @RabbitListener(bindings = {@QueueBinding(value = @Queue(name = "bmpface_delete_face_queue", durable = "true"),
            key = OrganizationBaseListener.REMOVE_STAFF_EVENT,
            exchange = @Exchange(value = OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    public void removeStaffEventHandler(RemoveStaffInfo RemoveStaffInfo, Channel channel, Message message) {
        try {
            boolean   isBoolean = faceInfoService.deleteFaceByStaff(RemoveStaffInfo);
            this.confirm(() -> isBoolean, channel, message);
        } catch (Exception e) {
            e.printStackTrace();
            // 当抛出异常时，消息将重新加入队列
        }
    }
}
