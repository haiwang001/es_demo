package com.bjxczy.onepark.facemodule.pojo.dto.command;

import lombok.Data;

/**
 * @author ：yubaowang
 * @date ：2022/12/28 10:29
 */
@Data
public class UpdateFaceCommand {
    private Integer id;
    private String faceImg;
    private Integer staffId;
}