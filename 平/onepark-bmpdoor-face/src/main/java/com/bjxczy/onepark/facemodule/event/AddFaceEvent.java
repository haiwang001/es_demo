package com.bjxczy.onepark.facemodule.event;

import org.springframework.context.ApplicationEvent;

import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * @ClassName AddFaceEvent
 * @Author 温良伟
 * @Date 2023/1/18 16:55
 * @Version 1.0
 */
@Data
@Log4j2
public class AddFaceEvent extends ApplicationEvent {

    private static final long serialVersionUID = 2541631439477755440L;
    private FaceInfoTransmit transmit;

    public AddFaceEvent(Object source, FaceInfoTransmit transmit) {
        super(source);
        this.transmit = transmit;
    }
}
