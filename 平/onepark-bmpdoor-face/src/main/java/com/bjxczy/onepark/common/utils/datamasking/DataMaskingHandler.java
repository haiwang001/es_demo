package com.bjxczy.onepark.common.utils.datamasking;

public abstract class DataMaskingHandler {

    /**
     * 普通员工脱敏
     *
     * @param value 原始值
     * @return 脱敏值
     */
    public String getMaskingValue(String value) {
        return value;
    }

    /**
     * 领导特殊脱敏
     *
     * @param value 原始值
     * @return 脱敏值
     */
    public String getLeaderMaskingValue(String value) {
        return value;
    }

    // public String doDataMasking(UserInformation sysUser, boolean includeAdmin, Integer rankId, String value) {
    // boolean isAdmin = SysUserUtils.isAdmin(sysUser);
    // if (StringUtils.isBlank(value)) {
    // return value;
    // }
    // if (includeAdmin || !isAdmin) {
    // return rankId == 1 ? getLeaderMaskingValue(value) : getMaskingValue(value);
    // }
    // return value;
    // }

}
