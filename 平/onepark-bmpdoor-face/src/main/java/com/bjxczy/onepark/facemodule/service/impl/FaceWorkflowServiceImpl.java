package com.bjxczy.onepark.facemodule.service.impl;

import com.bjxczy.core.web.workflow.IWorkflowService;
import com.bjxczy.core.web.workflow.ProcessPayload;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.common.utils.StrUtils;
import com.bjxczy.onepark.facemodule.interfaces.feign.client.QueryStaffServer;
import com.bjxczy.onepark.facemodule.pojo.dto.command.CreateFaceCommand;
import com.bjxczy.onepark.facemodule.service.FaceInfoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author wlw
 * 
 * @Date 2023/4/16 12:46
 */
@Log4j2
@Service
public class FaceWorkflowServiceImpl implements IWorkflowService {

    private final static String KEY = "Proc_4f1a33965195a054fc9876a9a206220f";
    private final static String img_Key = "staff_image_url";

    @Resource
    private QueryStaffServer StaffServer;
    @Resource
    private FaceInfoService faceInfoService;

    /**
     * staff_gender 性别 1 男 2女 staff_id_card 身份证 staff_image_url 图像 staff_id_card_type 证件类型 身份证类型 1
     * <p>
     * 审批校验，需校验用户提交申请数据是否合理，及业务限制
     *
     * @param key 业务流程DeployId
     * @param payload 员工ID、用户提交变量数据
     * @return
     */
    @Override
    public R<?> validated(String key, ProcessPayload payload) {
        if (FaceWorkflowServiceImpl.KEY.equals(key)) {
            if (null == payload.getVariables().get(FaceWorkflowServiceImpl.img_Key)) {
                throw new ResultException("请上传照片!");
            }
            return R.success(true);
        }
        return null;
    }

    /**
     * 审批通过处理
     *
     * @param key 业务流程DeployId
     * @param payload 员工ID、用户提交变量数据
     */
    @Override
    public R<?> approved(String key, ProcessPayload payload) {
        if (FaceWorkflowServiceImpl.KEY.equals(key)) {
            UserInformation userInformation = new UserInformation();
            userInformation.setStaffId(payload.getStaffId());
            List<String> imgList = (ArrayList<String>)payload.getVariables().get(FaceWorkflowServiceImpl.img_Key);
            String img = imgList.get(0);
            CreateFaceCommand command = new CreateFaceCommand();
            StaffInfoFine byId = StaffServer.getStaffById(payload.getStaffId());
            List<DeptNameInfo> deptNameInfos = StrUtils.deptNameInfo(byId.getDeptNames());
            command.setStaffId(payload.getStaffId());
            command.setFaceImg(img);
            faceInfoService.createFace(command, userInformation);
            return R.success(true);
        }
        return null;
    }

}
