package com.bjxczy.onepark.facemodule.interfaces.feign.client;

import org.springframework.cloud.openfeign.FeignClient;

import com.bjxczy.core.feign.client.pcs.FileFeignClient;

/**
 * 文件上传接口
 */
@FeignClient("pcs")
public interface FileServiceFaceFeign extends FileFeignClient {

}
