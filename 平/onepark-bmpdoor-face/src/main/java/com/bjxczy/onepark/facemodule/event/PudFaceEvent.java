package com.bjxczy.onepark.facemodule.event;

import org.springframework.context.ApplicationEvent;

import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.log4j.Log4j2;

/**
 * @ClassName getListFaceEvent
 * @Author 温良伟
 * @Date 2023/1/18 17:05
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = false)
@Data
@Log4j2
public class PudFaceEvent extends ApplicationEvent {
    private static final long serialVersionUID = -3875464529704429808L;
    private FaceInfoTransmit transmit;

    public PudFaceEvent(Object source, FaceInfoTransmit transmit) {
        super(source);
        this.transmit = transmit;
    }
}
