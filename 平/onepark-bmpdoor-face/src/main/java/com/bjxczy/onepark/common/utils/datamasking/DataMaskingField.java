package com.bjxczy.onepark.common.utils.datamasking;

import java.lang.annotation.*;

import com.bjxczy.onepark.common.utils.datamasking.handler.UnknowDataMaskingHandler;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface DataMaskingField {

    DataMaskingColumn value() default DataMaskingColumn.UNKOWN;

    Class<? extends DataMaskingHandler> typeHandler() default UnknowDataMaskingHandler.class;

}
