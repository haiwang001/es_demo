package com.bjxczy.onepark.facemodule.event;

import org.springframework.context.ApplicationEvent;

import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.log4j.Log4j2;

/**
 * @ClassName DelFaceEvent
 * @Author 温良伟
 * @Date 2023/1/18 17:18
 * @Version 1.0
 */
@EqualsAndHashCode(callSuper = false)
@Data
@Log4j2
public class DelFaceEvent extends ApplicationEvent {
    private static final long serialVersionUID = 2788452775967980875L;
    private FaceInfoTransmit transmit;

    public DelFaceEvent(Object source, FaceInfoTransmit transmit) {
        super(source);
        this.transmit = transmit;
    }
}
