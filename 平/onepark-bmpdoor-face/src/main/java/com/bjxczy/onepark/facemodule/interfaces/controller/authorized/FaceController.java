package com.bjxczy.onepark.facemodule.interfaces.controller.authorized;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.common.utils.ExcelUtil;
import com.bjxczy.onepark.facemodule.interfaces.feign.client.FileServiceFaceFeign;
import com.bjxczy.onepark.facemodule.pojo.dto.FaceInfoDto;
import com.bjxczy.onepark.facemodule.pojo.dto.command.CreateFaceCommand;
import com.bjxczy.onepark.facemodule.pojo.dto.command.RemoveFaceCommand;
import com.bjxczy.onepark.facemodule.pojo.dto.command.UpdateFaceCommand;
import com.bjxczy.onepark.facemodule.service.FaceInfoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * 人脸信息相关接口
 *
 * @Author <a href="mailto:17303233782@163.com">mengxin</a>
 * @Version V1.4.2
 * @Date 2023/06/29
 */
@Log4j2
@ApiResourceController
@RequestMapping("/face")
public class FaceController {

    public static final Map<String, String> EXPORT_HEAD = new HashMap() {
        private static final long serialVersionUID = -7525749307166528284L;

        {
            put("employeeNo", "员工编号");
            put("staffName", "姓名");
            put("mobile", "手机号");
            put("deptName", "部门");
            put("rankName", "职级");
            put("identifyCard", "证件号");
            put("comeFromName", "人员类型");
            put("createTime", "创建时间");
            put("updateTime", "更新时间");
            put("operator", "操作人");
        }
    };
    @Resource
    private FaceInfoService faceInfoService;
    @Resource
    private FileServiceFaceFeign faceServiceFaceFeign;

    /**
     * 查询人脸信息列表
     *
     * @param dto 传输对象
     * @return 人脸列表
     */
    @GetMapping(value = "/queryFacePage", name = "分页查询人脸信息")
    @SysLog(operationType = OperationType.SELECT, operation = "分页查询人脸信息")
    public R<?> queryFaceListPage(FaceInfoDto dto) {
        return faceInfoService.queryFaceListPage(dto);
    }

    /**
     * @param id 通过id查找
     * @return 返回json/
     */
    @GetMapping(value = "/getFaceById", name = "查看人脸详情")
    @SysLog(operationType = OperationType.SELECT, operation = "查看人脸详情")
    public R<?> getFaceById(@RequestParam("id") Integer id) {
        return R.success(faceInfoService.queryFaceById(id));
    }

    /**
     * 查询凭证
     *
     * @return 我的凭证
     */
    @GetMapping(value = "/getOwnFace", name = "获取登录人人脸信息")
    @SysLog(operationType = OperationType.SELECT, operation = "获取登录人人脸信息")
    public R<?> queryFaceInfoByStaffId(@LoginUser(isFull = true) UserInformation user) {
        return R.success(faceInfoService.queryFaceInfoByStaffId(user.getStaffId()));
    }

    /**
     * 获取人脸信息组织机构
     *
     * @return 返回人脸架构列表
     */
    @GetMapping(value = "/treesVague", name = "人脸架构列表")
    @SysLog(operationType = OperationType.SELECT, operation = "人脸架构列表")
    public R<?> treesVague() {
        return R.success(faceInfoService.getOrganizationTree());
    }

    /**
     * 添加人脸信息
     *
     * @param command 新增数据
     * @param user 系统用户
     * @return json信息
     */
    @PostMapping(value = "/addFace", name = "新增人脸")
    @SysLog(operationType = OperationType.INSERT, operation = "新增人脸")
    public R<?> save(@RequestBody CreateFaceCommand command, @LoginUser(isFull = true) UserInformation user) {
        return faceInfoService.createFace(command, user);
    }

    /**
     * 修改人脸信息
     *
     * @param command 数据
     * @return 是否成功
     */
    @PutMapping(value = "/updateFace", name = "编辑人脸")
    @SysLog(operationType = OperationType.UPDATE, operation = "编辑人脸")
    public R<?> update(@RequestBody UpdateFaceCommand command) {
        return faceInfoService.updateFace(command);
    }

    /**
     * 删除我的凭证
     *
     * @return 是否成功
     */
    @DeleteMapping(value = "/delOwnFace", name = "删除登录人人脸信息")
    @SysLog(operationType = OperationType.DELETE, operation = "删除登录人人脸信息")
    public R<?> delMyCertificate(@LoginUser(isFull = true) UserInformation user) {
        return faceInfoService.delMyCertificate(user);
    }

    /**
     * 删除人脸信息
     *
     * @param id 通过id删除
     */
    @DeleteMapping(value = "/delFace", name = "删除人脸")
    @SysLog(operationType = OperationType.DELETE, operation = "删除人脸")
    public void faceDel(@RequestParam("id") Integer id) {
        faceInfoService.delFace(new RemoveFaceCommand(id));
    }

    /**
     * 文件上传
     *
     * @param file 参数文件
     * @return 返回文件存储地址
     */
    @PostMapping(value = "/upload", name = "文件上传")
    @SysLog(operationType = OperationType.INSERT, operation = "文件上传")
    public R<?> uploadFile(@RequestParam("file") MultipartFile file) {
        return R.success(payload -> payload.set("url", faceServiceFaceFeign.fileUpload(file)));
    }

    /**
     * 导出人脸信息
     *
     * @param dto 请求参数
     * @param response 相应
     * @return 返回json
     */
    @PostMapping(value = "/export", name = "导出人脸信息")
    @SysLog(operationType = OperationType.EXPORT, operation = "导出人脸信息")
    public void exportFaceInfo(@RequestBody FaceInfoDto dto, HttpServletResponse response) {
        ExcelUtil.exportXlsxByBean(response, "人脸信息列表", FaceController.EXPORT_HEAD,
            faceInfoService.getFaceExportInfo(dto));
    }
}