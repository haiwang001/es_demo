package com.bjxczy.onepark.facemodule.pojo.dto.command;


import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CreateFaceCommand{
    /*
     * 员工id
     */
    private Integer staffId;
    /**
     * 人脸照片
     */
    private String faceImg;

}