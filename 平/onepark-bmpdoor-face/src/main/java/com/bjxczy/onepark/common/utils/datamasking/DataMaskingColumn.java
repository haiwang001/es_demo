package com.bjxczy.onepark.common.utils.datamasking;

public enum DataMaskingColumn {

    UNKOWN, MOBILE, MIS_CODE, ID_CARD;
}
