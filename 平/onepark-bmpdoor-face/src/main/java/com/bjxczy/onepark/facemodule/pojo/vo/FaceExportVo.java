package com.bjxczy.onepark.facemodule.pojo.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Auther: Administrator
 * @Date: 2023/7/12 0012 15:13
 * @Description: FaceExportVo
 * @Version 1.0.0
 */
@Data
public class FaceExportVo {

    private Integer id;

    /**
     * 描述：员工id
     * 是否必填：
     **/
    private Integer staffId;

    /**
     * 描述：员工编号
     * 是否必填：
     **/
    private String employeeNo;

    /**
     * 描述：员工名称
     * 是否必填：
     **/
    private String staffName;

    /**
     * 描述：手机号
     * 是否必填：
     **/
    private String mobile;

    /**
     * 描述：部门id
     * 是否必填：
     **/
    private Integer deptId;

    /**
     * 描述：部门名称：XXX公司/XX部门/XX
     * 是否必填：
     **/
    private String deptName;

    /**
     * 描述：职级id
     * 是否必填：
     **/
    private Integer rankId;

    /**
     * 描述：职级名称
     * 是否必填：
     **/
    private String rankName;

    /**
     * 描述：人员类型：字典：comeFrom
     * 是否必填：
     **/
    private Integer comeFrom;

    /**
     * 描述：证件号
     * 是否必填：
     **/
    private String identifyCard;

    /**
     * 描述：人脸照片
     * 是否必填：
     **/
    private String faceImg;

    /**
     * 描述：来源名称
     * 是否必填：
     **/
    private String comeFromName;

    /**
    * 描述：创建时间
    * 是否必填：
    **/
    private Date createTime;

    /**
    * 描述：更新时间
    * 是否必填：
    **/
    private Date updateTime;

    /**
    * 描述：操作人
    * 是否必填：
    **/
    private String operator;
}
