package com.bjxczy.onepark.common.constant;

/**
 * @ClassName SystConst
 * @Author 温良伟
 * @Date 2023/1/19 17:21
 * @Version 1.0
 */
public class FaceSysConst {
    public static final String EXCHANGE_NAME = "face_default_exchange";
    public static final String ADD_FACE_EVENT = "add_face_event";
    public static final String PUT_FACE_EVENT = "put_face_event";
    public static final String DEL_FACE_EVENT = "del_face_event";
    public static final String FILE_NAME_FACE = "人脸库导入模板";
    public static final String DICT_TABLE_NAME = "door_dict";
}
