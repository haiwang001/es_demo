package com.bjxczy.onepark.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bjxczy.onepark.facemodule.pojo.dto.BasePageDTO;

/**
 * @ClassName PageUtils
 * @Description TODO
 * @Author 温良伟
 * @Date 2023/2/28 17:40
 * @Version 1.0
 */
public class PageUtils {
    /**
     * 分页 records 数据key
     *
     * @param dto
     * @param list
     * @return
     */
    public static HashMap<String, Object> Page(BasePageDTO dto, List list) {
        ArrayList<Object> recordDoors = new ArrayList<>();
        HashMap<String, Object> Map = new HashMap<>(10);// 数据容器
        System.err.println(list.size());
        for (int i = dto.getPageNum() * dto.getPageSize() - dto.getPageSize(), j = 0; i < list.size(); i++, j++) {
            if (j < dto.getPageSize()) {
                recordDoors.add(list.get(i));

            }
        }

        ArrayList<Integer> in = new ArrayList<>();// 所有页码
        int pages = list.size() / dto.getPageSize() + 1;
        for (int i = 1; i <= pages; i++) {
            in.add(i);
        }
        Map.put("pageNum", dto.getPageNum());// 当前页码
        Map.put("pageSize", dto.getPageSize());// 当前页容量
        Map.put("pages", list.size() / dto.getPageSize() + 1);// 共计页数
        Map.put("navigatePageNums", in);// 共计页数
        Map.put("recordsCount", list.size());// 总记录数
        Map.put("currentRecordsCount", recordDoors.size());// 当前页录数
        Map.put("datalist", recordDoors);// 数据记录
        return Map;
    }
}
