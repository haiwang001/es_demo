package com.bjxczy.onepark.facemodule.config;


import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.dict.JdbcDictServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 *@ClassName DictConfig
 *@Author 温良伟
 *@Date 2023/1/31 15:11
 *@Version 1.0
 */
@Configuration
public class DictConfig {

    @Value("${dict.name}")
    private String name;

    @Bean
    public DictService dictService() {
        // 事先创建的字典表名称 使用配置文件 或者常量
        return new JdbcDictServiceImpl(name);
    }
}
