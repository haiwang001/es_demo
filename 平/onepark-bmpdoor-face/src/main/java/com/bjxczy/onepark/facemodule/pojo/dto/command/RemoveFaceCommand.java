package com.bjxczy.onepark.facemodule.pojo.dto.command;

import lombok.Data;

/**
 * @author ：yubaowang
 * @date ：2022/12/28 10:30
 */
@Data
public class RemoveFaceCommand {
    private Integer id;

    public RemoveFaceCommand(Integer id) {
        this.id = id;
    }
}
