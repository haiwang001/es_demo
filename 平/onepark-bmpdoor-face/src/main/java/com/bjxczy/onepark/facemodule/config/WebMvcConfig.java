package com.bjxczy.onepark.facemodule.config;


import com.bjxczy.core.web.configration.DefaultWebMvcConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

/*
 *@Author wlw
 *@Date 2023/4/16 20:41
 */
@Configuration
public class WebMvcConfig extends DefaultWebMvcConfig {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // TODO 可添加自定义拦截
        super.addInterceptors(registry); // 此行需要保留
    }
}
