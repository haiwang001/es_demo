package com.bjxczy.onepark.facemodule.interfaces.feign.client;

import com.bjxczy.core.feign.client.organization.BaseStaffFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("organization")
public interface QueryStaffServer extends BaseStaffFeignClient {
}
