package com.bjxczy.onepark.staffvoucher.service.impl;


import com.bjxczy.onepark.common.client.BmpFileServiceFaceFeign;

import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.utils.StaffUtils;
import com.bjxczy.onepark.common.utils.img.ImgUtils;
import com.bjxczy.onepark.config.HikConfiguration;
import com.bjxczy.onepark.staffvoucher.service.EsRecordService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/*
 *@Author wlw
 *@Date 2023/4/6 9:42
 */
@Service
@Log4j2
public class EsRecordServiceImpl implements EsRecordService {
    @Resource
    private BmpFileServiceFaceFeign faceServiceFaceFeign;
    @Resource
    private HikConfiguration hik;

    @Override
    public String getBase64(MultipartFile file) {
        return StaffUtils.getFileBase64String(faceServiceFaceFeign.fileUpload(file));
    }

    @Override
    public String fileUpload(MultipartFile file) throws Exception {
        long l = file.getSize() / 1024;
        if ((l) >= 200) {
            file = ImgUtils.compressionByMultipartFile(file);
        }
        assert file != null;
        if ((file.getSize() / 1024) >= 200) { //开启一次压缩
            throw new ResultException(" 请选择小于200KB的人脸! ");
        }
        String url = null;
        try {
            url = faceServiceFaceFeign.fileUpload(file);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResultException("文件类型非法，无法上传");
        }
        log.info("----图片路径为----------upload-----------------" + url);
        if (hik.getRating()) {
            /*if (!faceEvaluation(url)) {
                throw new ResultException(" 未检测到人脸! ");
            }*/
        }
        return url;
    }
}
