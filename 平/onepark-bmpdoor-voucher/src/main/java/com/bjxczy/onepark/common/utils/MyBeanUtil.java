package com.bjxczy.onepark.common.utils;

import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Component
public class MyBeanUtil {

	public static void putNotNullIntoParamsMap(Map<String, Object> params, Object obj) {
		Field[] fields = obj.getClass().getDeclaredFields();
        Field[] declaredFields = obj.getClass().getSuperclass().getDeclaredFields();
        List<Field> fieldList=new ArrayList<>();
        fieldList.addAll(Arrays.asList(declaredFields));
        fieldList.addAll(Arrays.asList(fields));
        if (fieldList != null && fieldList.size()!=0) {
			int j = fieldList.size();
			for (int i = 0; i < j; ++i) {
				Field field = fieldList.get(i);
				String fieldName = null;
				try {
					fieldName = field.getName();

					//排除serialVersionUID
					if("serialVersionUID".equals(fieldName)) {
						continue;
					}

					String firstLetter = fieldName.substring(0, 1).toUpperCase();
					String getter = "get" + firstLetter + fieldName.substring(1);
					Method method = obj.getClass().getMethod(getter, new Class[0]);
					Object value = method.invoke(obj, new Object[0]);
					if (value == null) {
						continue;
					}
					params.put(fieldName, value);
				} catch (Exception e) {
					System.err.println(obj.getClass().getName() + "的" + fieldName + "字段没有get方法！");
					e.printStackTrace();
				}
			}
		}
	}


}
