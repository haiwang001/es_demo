package com.bjxczy.onepark.visitor.service.impl;


import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.ObjectUtil;
import com.bjxczy.core.web.workflow.IWorkflowService;
import com.bjxczy.core.web.workflow.ProcessPayload;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.common.utils.DateUtil;
import com.bjxczy.onepark.visitor.command.AddVisitorCommand;
import com.bjxczy.onepark.common.client.*;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorInfoPo;
import com.bjxczy.onepark.visitor.mapper.VisitorInviteInfoMapper;
import com.bjxczy.onepark.visitor.service.VisitorInfoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Objects;

/*
 *@Author wlw
 *@Date 2023/4/16 21:35
 */
@Log4j2
@Service
public class VisitorIWorkflowServiceImpl implements IWorkflowService {
    private final static String KEY = "Proc_d0a43e4c1ec6881c35ea75af8c661e1e";

    private final static String details = "details";//到访详情说明
    private final static String document_number = "document_number"; //证件号
    private final static String visit_the_park = "visit_the_park";//到访园区
    private final static String face_url = "face_url";// 人脸照片
    private final static String cause = "cause";// 到访事由
    private final static String StartTime = "StartTime";// 到访时间
    private final static String EndTime = "EndTime";// 离开时间
    private final static String card_type = "card_type"; // 证件类型
    private final static String visitor_name = "visitor_name"; // 访客姓名
    private final static String visitor_mobile = "visitor_mobile"; // 访客手机号
    private final static String visit_the_park_label = "visit_the_park_label";//到访园区展示
    private final static String visitor_unit = "visitor_unit";// 访客单位
    @Resource
    private VisitorInviteInfoMapper visitorInviteInfoMapper;
    @Resource
    private BmpQueryStaffServer bmpQueryStaffServer;
    @Resource
    private VisitorInfoService service;
    @Override
    public R<?> approved(String key, ProcessPayload map) {
        if (KEY.equals(key)) {
            ArrayList<String> img = (ArrayList) map.getVariables().get(face_url);
            AddVisitorCommand command = new AddVisitorCommand();
            command.setVisitorName(map.getVariables().get(visitor_name).toString());
            command.setVisitorMobile(map.getVariables().get(visitor_mobile).toString());
            command.setVisitorIdCardType(map.getVariables().get(card_type).toString());
            command.setVisitorIdCard(map.getVariables().get(document_number).toString());
            command.setVisitorPhoto(img.get(0));
            command.setVisitorRemark(map.getVariables().get(details).toString());
            command.setVisitTime(map.getVariables().get(StartTime).toString());
            command.setLeaveTime(map.getVariables().get(EndTime).toString());
            command.setArriveType(Integer.parseInt(map.getVariables().get(cause).toString()));
            command.setReceptionistId(Integer.parseInt(map.getStaffId().toString()));//接访人
            command.setAreaId(map.getVariables().get(visit_the_park).toString().split(",")[0]);
            UserInformation userInformation = new UserInformation();
            userInformation.setStaffName(bmpQueryStaffServer.findById(map.getStaffId()).getFldName());
            try {
                return R.success(service.addVisitor(command, userInformation));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public R<?> validated(String key, ProcessPayload map) {
        if (KEY.equals(key)) {
            String string = map.getVariables().get(visitor_name).toString();
            if (ObjectUtil.isEmpty(string)) {
                throw new ResultException("请输入访客姓名!");
            }
            Object s1 = map.getVariables().get(visit_the_park);
            if (ObjectUtil.isEmpty(s1)) {
                throw new ResultException("请输入到访园区!");
            }
            Object s2 = map.getVariables().get(StartTime);
            Object s21 = map.getVariables().get(EndTime);
            if (ObjectUtil.isEmpty(s2) || ObjectUtil.isEmpty(s21)) {
                throw new ResultException("请选择到访时间!");
            }
            long pre = Objects.requireNonNull(DateUtil.stringToDate(s2.toString() + " 00:00:00")).getTime();
            long day = Objects.requireNonNull(DateUtil.stringToDate(DateUtil.getYearaAndMonthAndDay() + " 00:00:00")).getTime();
            if (pre < day) {
                throw new ResultException("到访时间需要大于当前时间!");
            }
            Object s3 = map.getVariables().get(visitor_mobile);
            if (ObjectUtil.isEmpty(s3)) {
                throw new ResultException("请输入访客手机号!");
            } else {
                if (!Validator.isMobile(s3.toString().trim())) {
                    throw new ResultException("请输入正确手机号!");
                }
            }
            Object s4 = map.getVariables().get(document_number);
            if (ObjectUtil.isEmpty(s4)) {
                throw new ResultException("请输入证件号!");
            }
            Object s5 = map.getVariables().get(card_type);
            if (ObjectUtil.isEmpty(s5)) {
                throw new ResultException("请选择证件类型!");
            }
            Object s6 = map.getVariables().get(cause);
            if (ObjectUtil.isEmpty(s6)) {
                throw new ResultException("请选择到访事由!");
            }
            ArrayList<String> img = (ArrayList) map.getVariables().get(face_url);
            if (img == null) {
                throw new ResultException("请上传照片!");
            }
            String type = map.getVariables().get(card_type).toString().trim();
            String document_number_trim = map.getVariables().get(document_number).toString().trim();
            if ("1".equals(type)) { // 表示是身份证类型
                if (!IdcardUtil.isValidCard(document_number_trim)) {
                    throw new ResultException("请输入正确证件号!");
                }
            }
            VisitorInfoPo visitorInfoPo = new VisitorInfoPo();
            visitorInfoPo.setVisitorName(string);
            visitorInfoPo.setVisitorMobile(s3.toString());
            visitorInfoPo.setVisitorIdCard(s4.toString());
            VisitorVerify(visitorInfoPo);
            return R.success(true);
        }
        return null;
    }
    private void VisitorVerify(VisitorInfoPo po) {
        // 姓名相同  证件号 手机号相同 已经预约过
        if (visitorInviteInfoMapper.selectByName(po) > 0) {
            throw new ResultException("存在其他有效预约 无法预约!");
        } // 姓名不同 证件号 或者 手机号相同 未到访的访客 已结束已取消不在校验
        if (visitorInviteInfoMapper.selectByMobileAndIdCard(po) > 0) {
            throw new ResultException("证件号或者手机号和其他有效访客冲突!");
        }
        long time1 = DateUtil.parseDateTime(DateUtil.getYearaAndMonthAndDay() + " 00:00:00").getTime();//获取 开始时间毫秒值
        long time = DateUtil.parseDateTime(po.getVisitTime()).getTime();//获取 开始时间毫秒值
        long time2 = DateUtil.parseDateTime(po.getLeaveTime()).getTime();//获取 结束时间时间毫秒值
        if (time2 < time1) {
            throw new ResultException("离开时间需要超过现在时间!");
        }
        if (time >= time2) {
            throw new ResultException("离开时间需要大于到访时间!");
        }
        if (time < time1) {
            throw new ResultException("到访时间最早为今日!");
        }
    }

}
