package com.bjxczy.onepark.door.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.door.pojo.dto.AuthorizationRecordDto;
import com.bjxczy.onepark.door.pojo.dto.ParticularsDto;
import com.bjxczy.onepark.door.service.DoorAuthLogService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;


@ApiResourceController
@RequestMapping("/authorizationRecord")
public class DoorAuthLogController {
    @Resource
    private DoorAuthLogService service;

    @GetMapping(value = "/page", name = "pc-授权记录列表")
    public R<?> list(AuthorizationRecordDto dto) {
        return R.success(service.page(dto));
    }

    @GetMapping(value = "/particulars", name = "pc-授权记录详情")
    public R<?> particulars(ParticularsDto dto) {
        return R.success(service.particulars(dto));
    }
}
