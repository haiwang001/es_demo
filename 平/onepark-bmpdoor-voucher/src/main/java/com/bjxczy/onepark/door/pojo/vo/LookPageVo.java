package com.bjxczy.onepark.door.pojo.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/7/10 11:18
 */
@Data
public class LookPageVo {
    @Excel(name = "员工编号", width = 20, replace = {"_null"})
    private String employeeNo;
    private String staffId;
    @Excel(name = "姓名", width = 15, replace = {"_null"})
    private String staffName;
    @Excel(name = "手机号", width = 20, replace = {"_null"})
    private String mobile;
    @Excel(name = "部门", width = 40, replace = {"_null"})
    private String deptName;
    @Excel(name = "设备名称", width = 30, replace = {"_null"})
    private String deviceName;
    @Excel(name = "设备类型", width = 30, replace = {"_null"})
    private String deviceType;
    @Excel(name = "方向", width = 15, replace = {"_null"})
    private String direction;
    @Excel(name = "空间", width = 40, replace = {"_null"})
    private String spaceName;
    @Excel(name = "授权时间", width = 30, replace = {"_null"})
    private String saveTime;
}
