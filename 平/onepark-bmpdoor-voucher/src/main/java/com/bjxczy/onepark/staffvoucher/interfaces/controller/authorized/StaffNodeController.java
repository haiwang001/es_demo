package com.bjxczy.onepark.staffvoucher.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffAddDto;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffDelDto;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffQueryListDto;
import com.bjxczy.onepark.staffvoucher.service.StaffNodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@ApiResourceController
@RequestMapping("/groupNode")
public class StaffNodeController {

    @Autowired
    private StaffNodeService staffNodeService;


    @GetMapping(value = "/list", name = "分组节点列表查询")
    public R<?> getGroupNodeList(StaffQueryListDto dto) throws Exception{
        return R.success(staffNodeService.getGroupNodeList(dto));
    }


    @PostMapping(value = "/add", name = "分组节点新增员工")
    public R<?> addGroupNodeStaff(@RequestBody StaffAddDto dto, @LoginUser(isFull = true) UserInformation sysUser)throws Exception {
        return R.success(staffNodeService.addGroupNodeStaff(dto,sysUser));
    }

    @DeleteMapping(value = "/delete", name = "分组节点删除员工")
    public R<?> deleteGroupNodeStaff(@RequestBody StaffDelDto dto, @LoginUser(isFull = true) UserInformation sysUser)throws Exception {
        return R.success(staffNodeService.DelGroupNodeStaff(dto));
    }

}
