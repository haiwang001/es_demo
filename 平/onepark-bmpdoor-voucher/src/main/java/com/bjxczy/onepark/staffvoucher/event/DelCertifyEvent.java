package com.bjxczy.onepark.staffvoucher.event;


import com.bjxczy.onepark.staffvoucher.entity.AccessCard;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEvent;

/*
 *@Date 2023/1/18 17:18
 */
@Log4j2
@Data
public class DelCertifyEvent extends ApplicationEvent {

    private static final long serialVersionUID = -7885359826857652945L;

    private AccessCard accessCard;

    public DelCertifyEvent(Object source, AccessCard accessCard) {
        super(source);
        this.accessCard = accessCard;
    }
}
