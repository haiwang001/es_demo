package com.bjxczy.onepark.common.client;

import com.bjxczy.core.feign.client.hikvision.HikOrgAndStaffFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("hikvision-adapter")
public interface BmpHikServiceFeign  extends HikOrgAndStaffFeignClient {
}
