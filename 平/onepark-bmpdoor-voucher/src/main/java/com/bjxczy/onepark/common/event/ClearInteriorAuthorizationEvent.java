package com.bjxczy.onepark.common.event;


import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/*
 删除凭证 取消权限
 *@Author wlw
 *@Date 2023/7/4 18:17
 */
@Getter
@ToString
public class ClearInteriorAuthorizationEvent extends ApplicationEvent {
    private static final long serialVersionUID = -2157480294059569771L;
    private Integer staffId;

    public ClearInteriorAuthorizationEvent(Object source, Integer staffId) {
        super(source);
        this.staffId = staffId;
    }
}
