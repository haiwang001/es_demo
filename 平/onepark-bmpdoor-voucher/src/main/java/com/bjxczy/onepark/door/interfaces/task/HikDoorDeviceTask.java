package com.bjxczy.onepark.door.interfaces.task;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.onepark.common.client.BnpBaseHikSyncDoorDeviceFeignClient;
import com.bjxczy.onepark.common.constant.VoucherSysConst;
import com.bjxczy.onepark.common.utils.DateUtil;
import com.bjxczy.onepark.common.utils.MD5Utils;
import com.bjxczy.onepark.door.entity.DoorInfoPo;
import com.bjxczy.onepark.door.mapper.DoorInfoMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;

/*
 *@Author wlw
 *@Date 2023/7/3 16:55
 */
@Configuration
@EnableScheduling
@Log4j2
public class HikDoorDeviceTask {
    @Resource
    private BnpBaseHikSyncDoorDeviceFeignClient HikSyncDoorDevice;
    @Resource
    private DoorInfoMapper mapper;


    /**
     * 每10分钟 定时获取海康门禁设备在线与不在线
     */
    @Scheduled(fixedDelay = 1000 * 60 * 10)
    public void synchronizationHikGuardInfoOnline() {
        mapper.selectList(new QueryWrapper<DoorInfoPo>()
                .eq("device_type", VoucherSysConst.DEVICE_TYPE_HIK)).forEach(el -> {
            String IP = MD5Utils.transcoding(el.getIp() + " ", StandardCharsets.UTF_8, StandardCharsets.UTF_16BE);
            boolean b = HikSyncDoorDevice.getDoorOnline(IP) == 1;
            log.info("[sync hik door device online] {}",b);

            if (b) {
                mapper.updateIsOnLineAndDate(1, el.getDeviceCode(), DateUtil.now());
            } else {
                mapper.updateIsOnLineAndDate(0, el.getDeviceCode(), null);
            }
        });
    }

    /**
     * 获取海康门禁设备
     */
    /*@Scheduled(fixedDelay = 1000 * 60 * 60 * 12)
    public void synchronizationHikGuardInfo() {
        log.info("[sync hik door device]");
        HikSyncDoorDevice.getDoorInformation().forEach(this::synchronizationEquipment);
    }*/
    /*private void synchronizationEquipment(HikDoorDeviceInformation el) {
        DoorInfoPo device_code = mapper.selectOne(new QueryWrapper<DoorInfoPo>().eq("device_code", el.getIndexCode()));
        if (device_code == null) {
            DoorInfoPo info = new DoorInfoPo();
            info.setId(UuidUtils.generateUuid());
            info.setDirection(1);//默认进方向
            info.setCreateTime(new Date());
            info.setUpdateTime(new Date());
            info.setOperator("admin");// 暂无操作人
            info.setPlatformType("海康");
            info.setDeviceType(VoucherSysConst.DEVICE_TYPE_HIK); //海康类型
            info.setDeviceCode(el.getIndexCode());
            info.setDeviceName(el.getName());
            info.setIp(el.getIp());
            mapper.insert(info);
        }
    }*/


}
