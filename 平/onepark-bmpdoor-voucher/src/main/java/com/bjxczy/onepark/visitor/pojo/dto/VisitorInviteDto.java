package com.bjxczy.onepark.visitor.pojo.dto;


import com.bjxczy.onepark.staffvoucher.pojo.dto.BasePageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *@Author wlw
 *@Date 2023/6/20 14:46
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class VisitorInviteDto  extends BasePageDTO {
    private String visitorName;//访客姓名 Y
    private String visitorMobile;//访客手机号
    private String visitorIdCard;//访客证件号码
    private Integer arriveStatus;//访问状态
    private String visitTime;//到访时间 Y
    private String leaveTime;//结束时间 Y
}
