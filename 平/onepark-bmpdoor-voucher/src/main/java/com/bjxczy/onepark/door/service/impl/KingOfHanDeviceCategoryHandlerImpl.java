package com.bjxczy.onepark.door.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.core.device.handler.DeviceCategoryDefinition;
import com.bjxczy.core.device.handler.DeviceCategoryHandler;
import com.bjxczy.onepark.common.constant.VoucherSysConst;
import com.bjxczy.onepark.common.model.device.DeviceInformation;
import com.bjxczy.onepark.common.model.device.DeviceStatus;
import com.bjxczy.onepark.door.entity.DoorInfoPo;
import com.bjxczy.onepark.door.mapper.DoorInfoMapper;
import lombok.extern.log4j.Log4j2;

import javax.annotation.Resource;
import java.util.*;


@DeviceCategoryDefinition(VoucherSysConst.DEVICE_TYPE_KINGOFHAN)
@Log4j2
public class KingOfHanDeviceCategoryHandlerImpl extends DeviceCategoryHandler {
    @Resource
    private DoorInfoMapper mapper;

    @Override
    protected List<DeviceInformation> thirdAll() {
        List<DeviceInformation> list = new ArrayList<>();
        mapper.selectList(new QueryWrapper<DoorInfoPo>()
                .eq("device_type", VoucherSysConst.DEVICE_TYPE_KINGOFHAN)).forEach(el -> {
            DeviceInformation temporary = new DeviceInformation();
            temporary.setDeviceCode(el.getDeviceCode());// 设备编码
            temporary.setDeviceName(el.getDeviceName());// 设备名称
            temporary.setOnlineStatus(el.getOnlineStatus()); // 设备状态
            temporary.setDevicePosition(el.getSpaceName());
            temporary.setSpaceId(el.getSpaceId());
            list.add(temporary);
        });
        log.info(" << 汉王设备上报 >>   {}", list);
        return list;
    }

    @Override
    protected Map<String, DeviceStatus> thirdStatus() {
        HashMap<String, DeviceStatus> map = new HashMap<>();
        mapper.selectList(new QueryWrapper<DoorInfoPo>()
                .eq("device_type", VoucherSysConst.DEVICE_TYPE_HIK)).forEach(el -> {
            DeviceStatus deviceStatus = VoucherSysConst.getDeviceStatus(el.getOnlineStatus());
            if (deviceStatus != null) {
                map.put(el.getId(), deviceStatus);
            }
        });
        log.info(" << 汉王设备状态同步v1 >>   {}", map);
        return map;
    }

    @Override
    protected Map<String, DeviceStatus> thirdStatus(Set<String> set) {
        HashMap<String, DeviceStatus> map = new HashMap<>();
        mapper.selectList(new QueryWrapper<DoorInfoPo>()
                .eq("device_type", VoucherSysConst.DEVICE_TYPE_HIK)).forEach(el -> {
            if (set.contains(el.getDeviceCode())) {
                DeviceStatus deviceStatus = VoucherSysConst.getDeviceStatus(el.getOnlineStatus());
                if (deviceStatus != null) {
                    map.put(el.getId(), deviceStatus);
                }
            }
        });
        log.info(" << 汉王设备状态同步 >>   {}", map);
        return map;
    }
}
