package com.bjxczy.onepark.visitor.entity.visitor;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/7/5 18:31 手动维护访客权限组 权限组合设备关联
 *
 * 手动维护 一个设备一个访客权限组 通过门禁管理 控制访客权限
 *
 */
@Data
@TableName("v_visitor_rights_and_door")
public class VisitorRightsAndDoor {
    /**
     * 访客权限组id privilege_group_id
     */
    private String privilegeGroupId;
    /**
     * 访客权限组名字 privilege_group_name
     */
    private String privilegeGroupName;
    /**
     * 访问预园区id  area_id
     */
    private String areaId;
}
