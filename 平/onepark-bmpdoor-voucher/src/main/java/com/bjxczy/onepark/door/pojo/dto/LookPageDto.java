package com.bjxczy.onepark.door.pojo.dto;


import com.bjxczy.onepark.staffvoucher.pojo.dto.BasePageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *@Author wlw
 *@Date 2023/7/10 11:03
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LookPageDto extends BasePageDTO {
    /**
     * 设备id
     */
    private String deviceId;
    /**
     * 员工编号/姓名/手机号
     */
    private String mixTogether;
    /**
     * 部门id
     */
    private String deptId;
}
