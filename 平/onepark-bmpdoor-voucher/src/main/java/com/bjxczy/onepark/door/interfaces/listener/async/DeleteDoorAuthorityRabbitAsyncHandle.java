package com.bjxczy.onepark.door.interfaces.listener.async;


import com.bjxczy.core.rabbitmq.event.door.AuthorityResultPayload;
import com.bjxczy.core.rabbitmq.rpc.RabbitAsyncComponent;
import com.bjxczy.core.rabbitmq.rpc.RabbitAsyncHandler;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.onepark.common.constant.enums.SyncStatusCode;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Component;


/*
 *@Author wlw  取消授权订阅 响应
 *@Date 2023/7/5 10:56
 */
@Component
@RabbitAsyncComponent(
        value = "DeleteDoorAuthorityRabbitAsyncHandle", // 处理器KEY，同时作为Bean名称
        exchange = DoorMutualBaseListener.DEFAULT_EXCAHNGE_NAME, // 目的交换机（公共组件中定义）
        routingKey = DoorMutualBaseListener.REMOVE_DOOR_CTRL_AUTHORIZATION  // 目的路由（公共组件中定义）
)
public class DeleteDoorAuthorityRabbitAsyncHandle extends RabbitAsyncHandler {
    /**
     * 统一返回
     *
     * @param id
     * @param message
     */
    @Override
    public void receive(Object id, Message message) {
        Object o = this.messageConverter.fromMessage(message);
        if (o instanceof CreateVisitorAndDoorVo){
            System.err.println("海康取消授权结果");
        }
        if (o instanceof AuthorityResultPayload){
            System.err.println("汉王取消授权结果");
        }
    }
}
