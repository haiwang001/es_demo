package com.bjxczy.onepark.door.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.door.entity.DoorAuthLogPo;
import com.bjxczy.onepark.door.pojo.dto.AuthorizationRecordDto;
import com.bjxczy.onepark.door.pojo.vo.DoorPageVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/*
 *@Author wlw
 *@Date 2023/7/4 17:23
 */
@Mapper
public interface DoorAuthLogMapper extends BaseMapper<DoorAuthLogPo> {
    @Select("select *\n" +
            "from d_door_auth_log\n" +
            "where true\n" +
            "  and if(#{dto.description}!='', remark like concat('%', #{dto.description}, '%'), true)\n" +
            "  and if(#{dto.date}!='', date_format(save_time,'%Y-%m-%d %H:%i') = #{dto.date}, true)" +
            "  order by save_time desc")
    IPage<DoorAuthLogPo> pageList(Page<Object> objectPage, @Param("dto") AuthorizationRecordDto dto);
}
