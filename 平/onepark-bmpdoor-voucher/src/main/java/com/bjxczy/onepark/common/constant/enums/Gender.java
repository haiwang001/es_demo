package com.bjxczy.onepark.common.constant.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

@Getter
public enum Gender {

    MALE(1, "男"), FAMLE(2, "女"), UNKONW(0, "未知");

    @EnumValue
    Integer value;

    String label;


    Gender(Integer value, String label) {
        this.value = value;
        this.label = label;
    }

}
