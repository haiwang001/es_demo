package com.bjxczy.onepark.staffvoucher.service;


import org.springframework.web.multipart.MultipartFile;

/*
 *@Author wlw
 *@Date 2023/4/6 9:42
 */
public interface EsRecordService {

    String fileUpload(MultipartFile file)throws Exception;

    String getBase64(MultipartFile file);
}
