package com.bjxczy.onepark.door.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.door.entity.DoorAuthorizationPo;
import com.bjxczy.onepark.door.pojo.dto.LookPageDto;
import com.bjxczy.onepark.door.pojo.vo.DoorPageVo;
import com.bjxczy.onepark.door.pojo.vo.LookPageVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/7/10 10:32
 */
@Mapper
public interface DoorAuthorizationMapper extends BaseMapper<DoorAuthorizationPo> {
    String LookPageSql = "select employee_no,\n" +
            "       staff_id,\n" +
            "       staff_name,\n" +
            "       mobile,\n" +
            "       dept_name,\n" +
            "       device_name,\n" +
            "       device_type,\n" +
            "       if(direction = 1, '进', '出') direction,\n" +
            "       space_name,\n" +
            "       b.save_time\n" +
            "from staff_voucher a\n" +
            "         left join d_door_authorization b on b.voucher_id = a.id\n" +
            "         left join d_door_info c on c.id = b.device_id\n" +
            "         left join organization.tbl_organization d on  a.dept_id =d.id \n" +
            "where b.del_flag =0 and c.id = #{dto.deviceId}\n" +
            "  and if(#{dto.mixTogether} != '', a.employee_no like concat('%', #{dto.mixTogether}, '%')\n" +
            "    or a.staff_name like concat('%', #{dto.mixTogether}, '%')\n" +
            "    or a.mobile like concat('%', #{dto.mixTogether}, '%'), true)\n" +
            "  and if(#{dto.deptId} != '',d.id = #{dto.deptId} or d.parent_id = #{dto.deptId}, true)  group by a.staff_id order by b.save_time";

    @Select(LookPageSql)
    IPage<LookPageVo> LookPage(Page<Object> objectPage, @Param("dto") LookPageDto dto);
    @Select(LookPageSql)
    List<LookPageVo> lookExport(@Param("dto") LookPageDto dto);
}
