package com.bjxczy.onepark.door.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.door.entity.DoorInfoPo;
import com.bjxczy.onepark.door.pojo.dto.DoorPageDto;
import com.bjxczy.onepark.door.pojo.vo.DoorPageVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/25 16:11
 */
@Mapper
public interface DoorInfoMapper extends BaseMapper<DoorInfoPo> {

    @Update("update  d_door_info set area_id = null ,space_id=null,tenant_id=null,space_name=null where device_code= #{deviceCode}")
    void unbindDeviceSpaceByDeviceCode(@Param("deviceCode") String deviceCode);

    @Update("update d_door_info\n" +
            "set online_status = #{doorOnline},\n" +
            "    last_data_time = #{date}\n" +
            "where device_code = #{deviceCode}")
    void updateIsOnLineAndDate(@Param("doorOnline") Integer doorOnline, @Param("deviceCode") String deviceCode, @Param("date") String date);

    @Select("select a.id,\n" +
            "       a.device_type,\n" +
            "       a.device_name,\n" +
            "       a.space_name,\n" +
            "       a.device_code,\n" +
            "       a.platform_type,\n" +
            "       if(a.online_status = 1, '在线', '离线') online_status,\n" +
            "       if(a.direction = 1, '进', '出') direction,\n" +
            "       if(a.staff_permission = 1, true, false) staff_permission,\n" +
            "       if(a.visit_permission = 1, true, false) visit_permission\n" +
            "from d_door_info a\n" +
            "where a.del_flag = 0\n" +
            "  and if(#{dto.mixTogether} != '', a.device_code like concat('%', #{dto.mixTogether} , '%') or a.device_name like concat('%', #{dto.mixTogether} , '%'), true)\n" +
            "  and if(#{dto.areaId} != '', a.area_id = #{dto.areaId}, true)\n" +
            "  and if(#{dto.buildingId} != '', a.tenant_id like concat('%', #{dto.buildingId}, '%'), true)\n" +
            "  and if(#{dto.spaceId} != '', a.space_id like concat('%', #{dto.spaceId}, '%'), true)\n" +
            "  and if(#{dto.unbound}, a.area_id is null, true)\n" +
            "group by a.id order by create_time")
    IPage<DoorPageVo> pageList(Page<Object> objectPage, @Param("dto") DoorPageDto dto);

    @Select("select device_code\n" +
            "from d_door_info\n" +
            "where del_flag = 0\n" +
            "  and direction = #{inOut}")
    List<String> getGoOutDeviceList(@Param("inOut") String inOut);

    @Select("select  device_code  from d_door_info where device_type = #{typeCode} and  del_flag = 0 and staff_permission = 1")
    List<String> selectListByType(@Param("typeCode") String typeCode);
}
