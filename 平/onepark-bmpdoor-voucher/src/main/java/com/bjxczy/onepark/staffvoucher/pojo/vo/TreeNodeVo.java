package com.bjxczy.onepark.staffvoucher.pojo.vo;

import lombok.Data;

import java.util.List;

/*
 *@Date 2023/3/13 9:01
 */
@Data
public class TreeNodeVo {
    private boolean isNoChildren = true;//是否拥有子节点
    private String name; // 节点名称
    private String facilityName;// 设备名称
    private List<TreeNodeVo> children; // 子节点

    public TreeNodeVo(String name) {
        this.name = name;
    }

    public TreeNodeVo() {
    }
}
