package com.bjxczy.onepark.common.constant.enums;


/**
 * 多平台的授权业务id
 */
public enum PlatformTypeCode {
    V(3, "v"),//访客
    P(4, "P"),//凭证
    HIK(0, "hikvision"),
    HANWNAG(1, "hanwang"),
    DDS(2, "dds");

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    PlatformTypeCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
