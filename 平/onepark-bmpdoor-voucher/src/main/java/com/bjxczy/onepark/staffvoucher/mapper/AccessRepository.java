package com.bjxczy.onepark.staffvoucher.mapper;

import com.bjxczy.core.web.base.AbstractRepository;
import com.bjxczy.onepark.staffvoucher.entity.AccessCard;


public interface AccessRepository extends AbstractRepository<AccessCard> {

}
