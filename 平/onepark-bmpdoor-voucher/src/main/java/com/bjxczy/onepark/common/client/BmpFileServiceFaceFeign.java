package com.bjxczy.onepark.common.client;


import com.bjxczy.core.feign.client.pcs.FileFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 文件上传接口
 */
@FeignClient("pcs")
public interface BmpFileServiceFaceFeign extends FileFeignClient {

}