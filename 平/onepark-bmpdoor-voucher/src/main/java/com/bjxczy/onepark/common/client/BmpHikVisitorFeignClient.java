package com.bjxczy.onepark.common.client;

import com.bjxczy.core.feign.client.visitor.BaseHikVisitorFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("hikvision-adapter")
public interface BmpHikVisitorFeignClient extends BaseHikVisitorFeignClient {
}
