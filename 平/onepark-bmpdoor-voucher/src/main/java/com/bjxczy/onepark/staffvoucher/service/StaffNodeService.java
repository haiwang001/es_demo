package com.bjxczy.onepark.staffvoucher.service;

import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffAddDto;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffDelDto;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffQueryListDto;


public interface StaffNodeService {
    int addGroupNodeStaff(StaffAddDto dto , UserInformation sysUser) throws Exception;

    int DelGroupNodeStaff(StaffDelDto dto) throws Exception;

    Object getGroupNodeList(StaffQueryListDto dto) throws Exception;
}
