package com.bjxczy.onepark.common.constant.enums;


/**
 * 访客状态
 */
public enum VisitorStatusCode {
    LONG_ERR(-2, "海康执行异常"),
    LOGIC_ERR(-1, "逻辑异常"),
    HAVE_NOT_STARTED(1, "未到访"),
    NON_ARRIVAL(2, "已迟到"),
    ARRIVED(3, "已到访"),
    CANCEL(4, "已取消"),
    FINISH(5, "已结束");
    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    VisitorStatusCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
