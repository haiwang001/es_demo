package com.bjxczy.onepark.visitor.interfaces.task;


import com.bjxczy.core.rabbitmq.event.door.InOrOutRecordPayload;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.core.rabbitmq.supports.VisitorBaseListener;
import com.bjxczy.onepark.common.client.BmpHikVisitorFeignClient;
import com.bjxczy.onepark.common.constant.enums.VisitorStatusCode;
import com.bjxczy.onepark.common.model.visitor.DeleteVisitorInformation;
import com.bjxczy.onepark.common.model.visitor.VisitorInformation;
import com.bjxczy.onepark.common.utils.DateUtil;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorInfoPo;
import com.bjxczy.onepark.visitor.mapper.VisitorInviteInfoMapper;
import com.google.common.collect.ImmutableBiMap;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/21 10:11  维护访客状态
 */
@Configuration
@EnableScheduling
@Log4j2
public class VisitorTask extends DoorMutualBaseListener {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Resource
    private VisitorInviteInfoMapper mapper;
    @Resource
    private BmpHikVisitorFeignClient hikVisitorFeignClient;


    @RabbitListener(bindings = {@QueueBinding(value = @Queue(name = "bmpinandout_visitor_status_push_record_queue", durable = "true"),
            key = PUSH_IN_OR_OUT_RECORD,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void pushInOrOutRecord(InOrOutRecordPayload payload, Channel channel, Message message) {
        if (payload.getBusinessId().length()==11){
            String orderId = mapper.selectByMobile(payload.getBusinessId());
            if (orderId != null) {
                mapper.updateById(new VisitorInfoPo(orderId, VisitorStatusCode.ARRIVED.getCode()));
            }
        }
    }


    @Scheduled(fixedDelay = 1000 * 60 * 2)
    public void visitorStatus() {
        List<VisitorInfoPo> select = mapper.selectByMap(ImmutableBiMap.of("del_flag", 0));
        select.forEach(el -> {
            if (el.getOrderId() == null) {
                mapper.updateById(new VisitorInfoPo(el.getId(), VisitorStatusCode.FINISH.getCode()));
            } else {
                mapper.updateById(new VisitorInfoPo(el.getId(), getBookVisitorByName(el)));
            }
        });
    }

    private Integer getBookVisitorByName(VisitorInfoPo el) {
        boolean visitorRecords = hikVisitorFeignClient.getVisitorRecords(el.getVisitorName(), el.getVisitTime(), el.getLeaveTime(), el.getOrderId());
        long time = new Date().getTime();// 当前时间
        long getPlanStartTime = DateUtil.parseDateTime(el.getVisitTime()).getTime();//获取 开始时间毫秒值
        long getPlanEndTime = DateUtil.parseDateTime(el.getLeaveTime()).getTime();//获取 结束时间时间毫秒值
        log.info("访客记录 {} 当前时间 {}   预约开始时间 {}    预约结束时间 {} ", visitorRecords, time, el.getVisitTime(), el.getLeaveTime());
        if (time < getPlanStartTime) {
            return VisitorStatusCode.HAVE_NOT_STARTED.getCode();
        }
        if (visitorRecords) {
            if (time > getPlanEndTime) { // 当前时间 在 开始 和结束之间 返回 5 已结束
                return VisitorStatusCode.FINISH.getCode();
            }
            if (time > getPlanStartTime && time < getPlanEndTime) { // 当前时间 在 开始 和结束之间 返回 2 未到访
                return VisitorStatusCode.NON_ARRIVAL.getCode();
            }
        } else {//有访问数据
            if (time > getPlanEndTime) { // 当前时间 在 开始 和结束之间     // 返回 5 已结束  执行访客签离删除
                rabbitTemplate.convertAndSend(VisitorBaseListener.DEFAULT_EXCAHNGE_NAME, VisitorBaseListener.DEL_VISITOR_EVENT, new DeleteVisitorInformation(el.getId(), el.getOrderId()));
                return VisitorStatusCode.FINISH.getCode();
            }
            if (time > getPlanStartTime && time < getPlanEndTime) { // 当前时间 在 开始 和结束之间 已到访
                return VisitorStatusCode.ARRIVED.getCode();
            }
        }
        return VisitorStatusCode.FINISH.getCode();
    }
}
