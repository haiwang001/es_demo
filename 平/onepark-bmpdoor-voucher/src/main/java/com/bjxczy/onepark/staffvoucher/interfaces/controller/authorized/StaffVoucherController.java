package com.bjxczy.onepark.staffvoucher.interfaces.controller.authorized;

import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.staffvoucher.pojo.dto.QueryStaffCertPageDTO;
import com.bjxczy.onepark.staffvoucher.service.StaffVoucherService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

;

/**
 * @author wangxiling
 * @date 2023/7/5 12:04
 * @description 凭证查询管理
 */
@ApiResourceController
@RequestMapping("/VoucherQuery")
@Log4j2
public class StaffVoucherController {

    @Resource
    private StaffVoucherService staffVoucherService;

    @GetMapping(value = "/page", name = "pc-凭证总览分页列表")
    public R<?> listPage(QueryStaffCertPageDTO dto) {
        return R.success(staffVoucherService.pageList(dto));
    }

    @PostMapping(value = "/export", name = "员工凭证查询导出")
    public void listExport(@RequestBody QueryStaffCertPageDTO dto, HttpServletResponse response) {
        staffVoucherService.listExport(response, dto);
    }

    @GetMapping(value = "/findPermission", name = "查询授权")
    public R<?> findStaffPermission(@RequestParam("staffId") String staffId) {
        return R.success(staffVoucherService.findStaffPermission(staffId));
    }
}
