package com.bjxczy.onepark.door.pojo.vo;


import lombok.Data;


/*
 *@Author wlw
 *@Date 2023/7/10 11:45
 */
@Data
public class ParticularsVo {
    private Integer id;
    /**
     * auth_log_id 授权记录id
     */
    private String authLogId;
    /**
     * device_id 设备id
     */
    private String deviceId;
    /**
     * device_name 设备名称
     */
    private String deviceName;
    /**
     *  space_name 设备区域空间名称：XXXX/XXX/XXX
     */
    private String spaceName;
    /**
     * 授权员工id staff_id
     */
    private String staffId;
    /**
     * 授权员工名称 staff_name
     */
    private String staffName;
    /**
     * 手机号 mobile
     */
    private String mobile;
    /**
     * 部门ID dept_id
     */
    private String deptId;
    /**
     * dept_name 部门名称：XXX公司/XX部门/XX
     */
    private String deptName;
    /**
     * sync_status  同步状态：0 - 下发中；1 - 下发成功；2 - 下发失败
     */
    private String syncStatus;
    /**
     * sync_status_text  同步状态描述
     */
    private String syncStatusText;
    /**
     * save_time 授权时间
     */
    private String saveTime;
}
