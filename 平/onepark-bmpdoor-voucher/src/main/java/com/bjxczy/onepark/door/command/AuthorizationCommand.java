package com.bjxczy.onepark.door.command;


import lombok.Data;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/7/4 10:41
 */
@Data
public class AuthorizationCommand {
    private String description; // 授权描述  授权
    private List<Integer> staffIds; //  员工id列  授权
    private List<String> facilityCodes; // 设备编码列 授权
}
