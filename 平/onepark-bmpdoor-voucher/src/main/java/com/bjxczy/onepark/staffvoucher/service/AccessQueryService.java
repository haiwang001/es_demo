package com.bjxczy.onepark.staffvoucher.service;

import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.staffvoucher.pojo.dto.AccessCardDto;

import javax.servlet.http.HttpServletResponse;

public interface AccessQueryService {
    R<?> accessCardLiat(AccessCardDto dto);

    Boolean exportExcel(HttpServletResponse response, String fileName, AccessCardDto dto);

    boolean isExistsAccessCard(String accessCode, Integer updateId);

    //Integer selectAccessCardByStaffId(Integer staffId);
}
