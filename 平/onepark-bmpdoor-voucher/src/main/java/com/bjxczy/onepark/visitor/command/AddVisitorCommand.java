package com.bjxczy.onepark.visitor.command;


import lombok.Data;

@Data
public class AddVisitorCommand {
    /**
     * 访客姓名          visitor_name
     */
    private String visitorName;
    /**
     * 访客手机号       visitor_mobile
     */
    private String visitorMobile;
    /**
     * 访客证件类型 visitor_id_card_type
     */
    private String visitorIdCardType;
    /**
     * 访客证件号       visitor_id_card
     */
    private String visitorIdCard;
    /**
     * 访客照片         visitor_photo
     */
    private String visitorPhoto;
    /**
     * 到访时间            visit_time  年月日
     */
    private String visitTime;
    /**
     * 结束时间            leave_time 年月日
     */
    private String leaveTime;
    /**
     * arrive_type 访问事由 0：入职，1：面试，2：送货，3：开会，4：出差，5：施工，6：其他原因
     */
    private Integer arriveType;
    /**
     * 接访人id      receptionist_id
     */
    private Integer receptionistId;
    /**
     *  访客备注 visitor_remark
     */
    private String visitorRemark;
    /**
     * 访问预园区id  area_id
     */
    private String areaId;
    /**
     * 楼id tenant_id
     */
    private String tenantId;
    /**
     * 楼层   space_id
     */
    private String spaceId;
    /**
     * 空间名称展示        space_name  xx/xx/xx
     */
    private String spaceName;
}
