package com.bjxczy.onepark.staffvoucher.service;

import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.staffvoucher.entity.AccessCard;
import com.bjxczy.onepark.staffvoucher.entity.StaffVoucher;
import com.bjxczy.onepark.staffvoucher.event.DelCertifyEvent;
import com.bjxczy.onepark.staffvoucher.pojo.dto.QueryStaffCertPageDTO;

import javax.servlet.http.HttpServletResponse;

public interface StaffVoucherService {

    /**
     * 订阅人脸服务， 新增人脸凭证
     * @param map
     * @throws Exception
     */
    void addCertify(FaceInfoTransmit map) throws Exception;

    /**
     * 订阅人脸服务， 修改人脸凭证信息
     * @param map
     */
    int updateCertify(FaceInfoTransmit map) throws Exception;

    /**
     * 订阅人脸服务， 删除人脸方法
     * @param map
     * @throws Exception
     */
    int deleteCertify(FaceInfoTransmit map) throws Exception;

    /**
     * 本地事件， 门禁卡删除
     * @param event
     */
    int deleteCertify(DelCertifyEvent event);

    /**
     * 本地事件， 门禁卡新增
     * @param accessCard 门禁卡信息
     * @param staff      员工信息
     * @throws Exception 异常信息
     */
    void addCertify(AccessCard accessCard, StaffInfoFine staff) throws Exception;

    /**
     * 本地方法,  查看凭证信息
     * @param dto
     * @return
     */
    Object pageList(QueryStaffCertPageDTO dto);

    /**
     * 本地方法；   导出员工凭证列表
     * @param response
     * @param dto
     */
    void listExport(HttpServletResponse response, QueryStaffCertPageDTO dto);

    /**
     * 本地方法；  鉴定员工权限
     * @param staffCode
     * @return
     */
    Object findStaffPermission(String staffId);


    /**
     * 本地方法； 通过员工ID获取员工凭证信息
     * @param staffId
     * @return
     */
    StaffVoucher selectStaffVoucherByStaffId(Integer staffId);
}
