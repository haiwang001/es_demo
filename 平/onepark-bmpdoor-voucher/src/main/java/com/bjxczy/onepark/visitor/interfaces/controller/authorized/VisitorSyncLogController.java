package com.bjxczy.onepark.visitor.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.visitor.service.VisitorSyncLogService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/*
 *@Author wlw
 *@Date 2023/6/20 17:31
 */
@Log4j2
@ApiResourceController
@RequestMapping("visitorSyncLog")
public class VisitorSyncLogController {
    @Resource
    private VisitorSyncLogService service;

    @ApiOperation("pc-访客授权状态查询")
    @SysLog(operationType = OperationType.SELECT, operation = "pc-访客授权状态查询")
    @GetMapping(value = "/accreditResult", name = "pc-访客授权状态查询")
    public R<?> accreditResult(String id) {
        return R.success(service.accreditResult(id));
    }
}
