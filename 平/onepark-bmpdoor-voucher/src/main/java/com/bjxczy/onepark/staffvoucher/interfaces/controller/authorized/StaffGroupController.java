package com.bjxczy.onepark.staffvoucher.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.staffvoucher.entity.StaffGroup;
import com.bjxczy.onepark.staffvoucher.service.StaffGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/*
 *@ClassName StaffGroupController
 *@Author 温良伟
 *@Date 2023/2/8 10:11
 *@Version 1.0
 *
 * 员工分组节点树管理
 */
@ApiResourceController
@RequestMapping("/group")
public class StaffGroupController {

    @Autowired
    private StaffGroupService staffGroupService;


    @GetMapping(value = "/list", name = "员工分组查询")
    public R<?> addStaffGroup()throws Exception  {
        return R.success(staffGroupService.getGroups());
    }

    /**
     * 新增分组
     * @param sg pid为-1表示根节点
     * @return
     */
    @PostMapping(value = "/add", name = "员工分组新增")
    public R<?> addStaffGroup(@RequestBody StaffGroup sg) throws Exception {
        return R.success(staffGroupService.addGroup(sg));
    }


    @PutMapping(value = "/update", name = "员工分组修改")
    public R<?> updateStaffGroup(@RequestBody StaffGroup sg) throws Exception {
        return R.success(staffGroupService.updateGroup(sg));
    }

    @DeleteMapping(value = "/delete/{id}", name = "员工分组删除")
    public R<?> deleteStaffGroup(@PathVariable("id") Integer id) throws Exception {
        return R.success(staffGroupService.deleteGroup(id));
    }


}
