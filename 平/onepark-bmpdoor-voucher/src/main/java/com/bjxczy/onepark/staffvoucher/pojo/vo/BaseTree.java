package com.bjxczy.onepark.staffvoucher.pojo.vo;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class BaseTree<T> {
	
	private T id;
	
	private String label;
	
	private List<BaseTree<T>> children;
	
	private Map<String, Object> payload;
	
	public BaseTree<T> putPayload(String k, Object v) {
		if (payload == null) {
			payload = new HashMap<>();
		}
		payload.put(k, v);
		return this;
	}

}
