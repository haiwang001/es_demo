package com.bjxczy.onepark.staffvoucher.pojo.vo;


import lombok.Data;

import java.util.ArrayList;

/*
 *@ClassName SpaceNodeVo
 *@Author 温良伟
 *@Date 2023/3/10 14:28
 *@Version 1.0
 */
@Data
public class SpaceNodeVo {
    private String id;
    private String pid;
    private String name;
    private String deviceName;
    private ArrayList<SpaceNodeVo> node = new ArrayList<>();
}
