package com.bjxczy.onepark.staffvoucher.pojo.dto;


import lombok.Data;

import java.util.List;

/*
*  *@Date 2023/2/8 14:12
 */
@Data
public class StaffAddDto {
    private String groupId;
    private List<Integer> staffList;
    /**
     * 操作人
     */
    private String operator;
}
