package com.bjxczy.onepark;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableConfigurationProperties
@EnableCaching
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
@EnableAsync
public class VoucherApplication {

    public static void main(String[] args) {
        SpringApplication.run(VoucherApplication.class, args);
    }

    /**
     * 解决Tomcat RFC7230以及Header状态码加上描述字段的问题
     */
    @Bean
    public ServletWebServerFactory webServerFactory() {
        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        factory.addConnectorCustomizers(connector -> {
            //设置解析描述符为true,例如Header 状态码为200,那么解析出来就是200 ok
            connector.setProperty("sendReasonPhrase", "true");
            //以下设置为解决RFC7230问题
            connector.setProperty("relaxedQueryChars", "|{}[](),/:;<=>?@[\\]{}\\");
            connector.setProperty("relaxedPathChars", "|{}[](),/:;<=>?@[\\]{}\\");
            //这个属性在tomcat为8.5的时候不生效
            connector.setProperty("rejectIllegalHeader", "false");
        });
        return factory;
    }


}
