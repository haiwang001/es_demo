package com.bjxczy.onepark.door.interfaces.listener.subscribe;


import com.alibaba.nacos.common.utils.UuidUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.core.rabbitmq.supports.DeviceBaseListener;
import com.bjxczy.onepark.common.constant.VoucherSysConst;
import com.bjxczy.onepark.common.model.device.DeviceInfoAll;
import com.bjxczy.onepark.common.model.device.SyncSubDeviceInfo;
import com.bjxczy.onepark.door.entity.DoorInfoPo;
import com.bjxczy.onepark.door.mapper.DoorInfoMapper;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;


@Component
@Slf4j
public class DeviceEventListener extends DeviceBaseListener {
    @Resource
    private DoorInfoMapper mapper;

    /**
     * 增加设备
     *
     * @param syncSubDeviceInfo
     * @param channel
     * @param message
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = ADD_DEVICE_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME))})
    @Override
    public void addDeviceEvent(SyncSubDeviceInfo syncSubDeviceInfo, Channel channel, Message message) {
        VoucherSysConst.DEVICE_TYPE.forEach(type -> {
            if (type.equals(syncSubDeviceInfo.getCategory())) {
                DoorInfoPo doorInfoPo = mapper.selectOne(new QueryWrapper<DoorInfoPo>()
                        .eq("device_code", syncSubDeviceInfo.getDeviceCode()));
                if (doorInfoPo == null) {
                    mapper.insert(setDoorInfo(type, syncSubDeviceInfo));
                }
            }
        });
    }

    /**
     * 修改设备事件
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = EDIT_DEVICE_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME))})
    @Override
    public void editDeviceEvent(SyncSubDeviceInfo Info, Channel channel, Message message) {
        try {
            DoorInfoPo doorInfoPo = mapper.selectOne(new QueryWrapper<DoorInfoPo>()
                    .eq("device_code", Info.getDeviceCode()));
            if (doorInfoPo!=null){
                doorInfoPo.setDeviceName(Info.getDeviceName());
                doorInfoPo.setAreaId(Info.getAreaId().toString());
                doorInfoPo.setTenantId(Info.getBuildingId());
                doorInfoPo.setSpaceId(Info.getSpaceId());
                doorInfoPo.setSpaceName(Info.getAreaPosition());
                mapper.updateById(doorInfoPo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除设备事件
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = REMOVE_DEVICE_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME))})
    @Override
    public void removeDeviceEvent(String deviceCode, Channel channel, Message message) {
        DoorInfoPo doorInfoPo = mapper.selectOne(new QueryWrapper<DoorInfoPo>()
                .eq("device_code", deviceCode));
        if (doorInfoPo != null) {
            mapper.deleteById(doorInfoPo);
        }
    }


    /**
     * 绑定
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = EDIT_DEVICE_SPACE,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME))})
    @Override
    public void editDeviceSpace(DeviceInfoAll deviceAll, Channel channel, Message message) {
        VoucherSysConst.DEVICE_TYPE.forEach(type -> {
            if (type.equals(deviceAll.getDeviceType())) { // 包含传入的设备类型 // 允许修改
                DoorInfoPo info = mapper.selectOne(new QueryWrapper<DoorInfoPo>()
                        .eq("device_code", deviceAll.getDeviceCode()));
                info.setAreaId(deviceAll.getAreaId().toString());
                info.setTenantId(deviceAll.getBuildingId());
                info.setSpaceId(deviceAll.getSpaceId());
                info.setSpaceName(deviceAll.getDevicePosition());
                mapper.updateById(info);
            }
        });
    }


    /**
     * 解绑
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = UNBIND_DEVICE_SPACE,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME))})
    @Override
    public void unbindDeviceSpace(String deviceCode, Channel channel, Message message) {
        mapper.unbindDeviceSpaceByDeviceCode(deviceCode);
    }

    public DoorInfoPo setDoorInfo(String type, SyncSubDeviceInfo syncSubDeviceInfo) {
        DoorInfoPo info = new DoorInfoPo();
        info.setId(UuidUtils.generateUuid());
        info.setDeviceCode(syncSubDeviceInfo.getDeviceCode());
        info.setDeviceType(syncSubDeviceInfo.getCategory());
        info.setDeviceName(syncSubDeviceInfo.getDeviceName());
        info.setPlatformType(type.equals(VoucherSysConst.DEVICE_TYPE_KINGOFHAN) ? "汉王" : "海康");
        info.setDirection(1);//默认进方向
        info.setCreateTime(new Date());
        info.setUpdateTime(new Date());
        info.setOperator("admin");// 暂无操作人
        info.setAreaId(syncSubDeviceInfo.getAreaId().toString());
        info.setTenantId(syncSubDeviceInfo.getBuildingId());
        info.setSpaceId(syncSubDeviceInfo.getSpaceId());
        info.setSpaceName(syncSubDeviceInfo.getAreaPosition());
        return info;
    }
}
