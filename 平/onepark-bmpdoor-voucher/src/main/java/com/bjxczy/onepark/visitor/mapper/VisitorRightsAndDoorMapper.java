package com.bjxczy.onepark.visitor.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/7/6 9:55
 */
@Mapper
public interface VisitorRightsAndDoorMapper {

    @Select("select privilege_group_id from v_visitor_rights_and_door where area_id = #{areaId}")
    List<String> setVisitorGroupByAreaId(@Param("areaId") String areaId);

    @Select("select  count(privilege_group_id) from v_visitor_rights_and_door where device_code = #{deviceId}")
    Integer selectVisitorRights(@Param("deviceId") String deviceId);
}
