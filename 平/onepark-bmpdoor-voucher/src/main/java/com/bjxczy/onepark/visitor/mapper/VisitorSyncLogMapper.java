package com.bjxczy.onepark.visitor.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorSyncLogPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/*
 *@Author wlw
 *@Date 2023/6/20 14:34
 */
@Mapper
public interface VisitorSyncLogMapper extends BaseMapper<VisitorSyncLogPo> {
    @Select("select  * from v_visitor_sync_log where visitor_id = #{id}")
    VisitorSyncLogPo selectByVisitorId(@Param("id") String id);
}
