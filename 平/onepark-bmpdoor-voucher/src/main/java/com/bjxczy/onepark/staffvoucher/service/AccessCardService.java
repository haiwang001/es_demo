package com.bjxczy.onepark.staffvoucher.service;

import com.bjxczy.onepark.staffvoucher.pojo.dto.command.CreateAccessCommand;
import com.bjxczy.onepark.staffvoucher.pojo.dto.command.RemoveAccessCommand;

/**
 * @Author: wangxiling
 * @Description: TODO                //类描述
 * @CreateTime: 2023-07-11  11:10  //时间
 */
public interface AccessCardService {

    Boolean createCard(CreateAccessCommand command);

    void delAccessCard(RemoveAccessCommand removeAccessCommand);

    Integer selectAccessCardByStaffId(Integer staffId);

}
