package com.bjxczy.onepark.visitor.pojo.vo;
import com.bjxczy.core.web.privacy.handler.IdCardMaskingHandler;
import com.bjxczy.core.web.privacy.handler.MaskingField;
import com.bjxczy.core.web.privacy.handler.MobileMaskingHandler;
import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/20 15:49
 */
@Data
public class VisitorInvitePageListVo {
    private String id;
    private String visitorName;
    @MaskingField(typeHandler = MobileMaskingHandler.class)
    private String visitorMobile;
    @MaskingField(typeHandler = IdCardMaskingHandler.class)
    private String visitorIdCard;
    private String visitorPhoto;
    private String visitTime;
    private String leaveTime;
    private String receptionistName;
    @MaskingField(typeHandler = MobileMaskingHandler.class)
    private String receptionistMobile;
    private String operator;
    private String arriveType;
    private String arriveStatus;
    private String visitorRemark;
    private String spaceName;
    private String receptionistDeptName;
}
