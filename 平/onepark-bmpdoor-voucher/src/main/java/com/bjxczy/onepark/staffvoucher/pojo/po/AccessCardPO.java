package com.bjxczy.onepark.staffvoucher.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bjxczy.core.web.base.UserOperator;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @author ：yubaowang
 * @date ：2022/12/27 10:45
 * 员工门禁卡表：access_card
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "access_card", autoResultMap = true)
public class AccessCardPO extends UserOperator {

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 员工ID
     */
    private Integer staffId;

    /**
     * 员工编号
     */
    private String employeeNo;

    /**
     * 员工姓名
     */
    @NotBlank(message = "员工姓名不能为空！")
    private String staffName;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 职级ID
     */
    private Integer rankId;

    /**
     * 职级名称
     */
    private String rankName;
    /**
     * 卡类型
     */
    private Integer comeFrom;
    /**
     * 门禁卡号
     */
    @NotBlank(message = "门禁卡号不能为空！")
    private String accessCode;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 操作人
     */
    private String operator;
    /**
     * 删除标记
     */
    private Integer delFlag;



}
