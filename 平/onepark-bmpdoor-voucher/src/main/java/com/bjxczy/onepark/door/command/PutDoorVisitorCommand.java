package com.bjxczy.onepark.door.command;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/7/6 16:28
 */
@Data
public class PutDoorVisitorCommand {
    private String deviceId;// 设备id
    /**
     * staff_permission 访客 (1-开，2-关)
     */
    private Integer visitPermission;
}
