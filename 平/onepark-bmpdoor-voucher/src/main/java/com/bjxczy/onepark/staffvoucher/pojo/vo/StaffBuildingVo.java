package com.bjxczy.onepark.staffvoucher.pojo.vo;


import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/*
 *@ClassName StaffBuildingVo
 *@Author 温良伟
 *@Date 2023/3/9 11:45
 *@Version 1.0
 */
@Data
public class StaffBuildingVo {


    private Integer areaId; // 区域id
    private String areaName; // 区域名字
    private Long buildingId; // 楼 编号或者名字
    private String buildingName; // 楼 编号或者名字
    @TableId
    private String layerId; //  具体某一层 编号或者名字
    private String layerName; //  具体某一层 编号或者名字

}
