package com.bjxczy.onepark.staffvoucher.service.impl;


import com.bjxczy.onepark.common.constant.VoucherSysConst;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.staffvoucher.entity.StaffGroup;
import com.bjxczy.onepark.staffvoucher.mapper.StaffGroupMapper;
import com.bjxczy.onepark.staffvoucher.service.StaffGroupService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Log4j2
@Service
public class StaffGroupServiceImpl implements StaffGroupService {

    @Autowired
    private StaffGroupMapper staffGroupMapper;

    @Override
    public List<StaffGroup> getGroups() {
        HashMap<String, Object> Map = new HashMap(3);
        Map.put("del_flag", 0);
        List<StaffGroup> staffGroups = staffGroupMapper.selectByMap(Map);
        List<StaffGroup> root = rootNode(-1, staffGroups);
        return root;
    }

    /**
     * @param root
     * @param staffGroups
     * @param tree
     * @return
     */
    private List<StaffGroup> rootNode(Integer root, List<StaffGroup> staffGroups) {
        List<StaffGroup> node = new ArrayList<>(); //根节点
        staffGroups.forEach(el -> {
            if (Objects.equals(el.getFatherId(), root)) {// 等于
                node.add(el); //添加根节点
            }
        });
        node.forEach(el -> {
            List<StaffGroup> list = rootNode(el.getId(), staffGroups);
            el.setChildren(list);
        });
        return node;
    }

    @Override
    public int deleteGroup(Integer id) throws Exception {
        HashMap<String, Object> Map = new HashMap(3);
        Map.put("del_flag", 0);
        List<StaffGroup> staffGroups = staffGroupMapper.selectByMap(Map);
        List<StaffGroup> root = rootNode(id, staffGroups);
        System.err.println(root);
        if (root.size() > 0) {//包含了子节点 无法删除
            throw new ResultException("!无法删除包含了子分组");
        }
        return staffGroupMapper.deleteById(id);
    }

    @Override
    public int updateGroup(StaffGroup sg) throws Exception {
        filtration(sg, true);//参数过滤
        return staffGroupMapper.updateById(sg);
    }

    /**
     * 新增分组
     * 需要判断是否是根节点
     * StaffGroupRepository
     *
     * @param sg
     * @return
     */
    @Override
    public int addGroup(StaffGroup sg) throws Exception {
        sg.setName(sg.getName().trim());
        filtration(sg, false);//参数过滤
        sg.setCreateTime(new Date());
        sg.setDelFlag(0);
        //具有父节点
        return staffGroupMapper.insert(sg);
    }

    /**
     * 条件过滤
     *
     * @param sg
     * @throws Exception
     */
    private void filtration(StaffGroup sg, boolean update) throws Exception {
        if (sg.getName() == null || "".equals(sg.getName()))
            throw new ResultException("该分组名称未填写或者未传入");
        if (!Objects.equals(sg.getFatherId(), VoucherSysConst.ROOT_NODE))//根节点不需要判断父节点
            if (isExistFather(sg))
                throw new ResultException("父节点不存在 fatherId=值不存在");
        if (update) {
            StaffGroup staffGroup = staffGroupMapper.selectById(sg.getId());
            if (staffGroup == null)
                throw new Exception("当前id记录不存在 修改失败");
            if (staffGroup.getName().equals(sg.getName()))
                throw new ResultException("修改的名称与原名称一致 修改失败");
        } else {
            if (isRepetition(sg))
                throw new ResultException("该分组下 已存在重复的分组名称");
        }
    }


    /**
     * 是否存在父节点
     *
     * @param sg
     * @return
     */
    private boolean isExistFather(StaffGroup sg) {
        return staffGroupMapper.selectById(sg.getFatherId()) == null;
    }

    /**
     * 同一节点不允许name重复
     *
     * @param sg father_id 节点  name名称
     * @return
     */
    private boolean isRepetition(StaffGroup sg) {
        HashMap<String, Object> Map = new HashMap(3);
        Map.put("father_id", sg.getFatherId());
        Map.put("name", sg.getName());
        Map.put("del_flag", 0);
        List<StaffGroup> staffGroups = staffGroupMapper.selectByMap(Map);
        return staffGroups.size() >= 1;
    }
}
