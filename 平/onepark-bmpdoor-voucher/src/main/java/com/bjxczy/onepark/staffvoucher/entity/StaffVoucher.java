package com.bjxczy.onepark.staffvoucher.entity;


import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author wangxiling
 * @date 2023/6/29 14:56
 * @description  凭证记录信息 无法删除 只能修改凭证数量  触发事件添加记录
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("staff_voucher")
public class StaffVoucher {

    /**
     * 记录id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 员工ID
     */
    private Integer staffId;

    /**
     * 记录员工编号 employee_no
     */
    @Excel(name = "员工编号", width = 18, replace = {"_null"})
    private String employeeNo;

    /**
     * 员工姓名 fld_name
     */
    @Excel(name = "员工姓名", width = 18, replace = {"_null"})
    private String staffName;

    /**
     * 员工手机号
     */
    @Excel(name = "手机号", width = 18, replace = {"_null"})
    private String mobile;

    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 员工部门名称 a/b/c dept_name
     */
    @Excel(name = "部门名称", width = 18, replace = {"_null"})
    private String deptName;

    /**
     * 职级ID
     */
    private Integer rankId;

    /**
     * 职级名称
     */
    private String rankName;

    /**
     * 人员类型
     */
    private Integer comeFrom;


    /**
     * 证件号 identify_card
     */
    @Excel(name = "证件号", width = 18, replace = {"_null"})
    private String identifyCard;

    /**
     * 人脸照片 不导出
     */
    private String faceImg;

    /**
     * 门禁卡号  access_code
     */
    @Excel(name = "门禁卡号", width = 18, replace = {"_null"})
    private String accessCode;

    /**
     *        voucher_cnt
     *        voucher_cnt
     */
    @Excel(name = "凭证数量", width = 18, replace = {"_null"})
    private Integer voucherCnt;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 是否删除
     */
    private Integer delFlag;



    // 导出专用字段，staff_voucher 表中没有此字段
    /**
     * 人员类型
     */
    @TableField(exist = false)
    private String staffType;

}