package com.bjxczy.onepark.staffvoucher.interfaces.feign;


import com.bjxczy.core.feign.client.voucher.BaseVoucherFeignClient;
import com.bjxczy.onepark.common.model.voucher.StaffVoucherInfo;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.staffvoucher.entity.StaffVoucher;
import com.bjxczy.onepark.staffvoucher.service.EsRecordService;
import com.bjxczy.onepark.staffvoucher.service.StaffVoucherService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @Author wlw
 * @Date 2023/5/6 18:24
 * 人脸上传到ES
 */
@Controller
@ResponseBody
public class FaceFileFeignController implements BaseVoucherFeignClient {

    @Resource
    private EsRecordService esService;

    @Autowired
    private StaffVoucherService staffVoucherService;


    @Override
    public R<?> upload(@RequestPart("file")MultipartFile multipartFile) throws Exception {
        return R.success(esService.fileUpload(multipartFile));
    }

    @Override
    public StaffVoucherInfo getByStaffId(@PathVariable("staffId")Integer staffId) {
        StaffVoucher staffVoucher = staffVoucherService.selectStaffVoucherByStaffId(staffId);
        StaffVoucherInfo staffVoucherInfo = new StaffVoucherInfo();
        if (staffVoucher!=null&&staffVoucher.getStaffId()!=null&&staffVoucher.getStaffId()!=0){
            BeanUtils.copyProperties(staffVoucher,staffVoucherInfo);
        }
        return staffVoucherInfo;
    }
}
