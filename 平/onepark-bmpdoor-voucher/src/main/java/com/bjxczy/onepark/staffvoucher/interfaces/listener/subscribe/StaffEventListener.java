package com.bjxczy.onepark.staffvoucher.interfaces.listener.subscribe;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.core.rabbitmq.supports.OrganizationBaseListener;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.RemoveStaffInfo;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.utils.StrUtils;
import com.bjxczy.onepark.staffvoucher.entity.AccessCard;
import com.bjxczy.onepark.staffvoucher.entity.StaffVoucher;
import com.bjxczy.onepark.staffvoucher.event.AddCertifyEvent;
import com.bjxczy.onepark.staffvoucher.event.DelCertifyEvent;
import com.bjxczy.onepark.staffvoucher.mapper.AccessCardMapper;
import com.bjxczy.onepark.staffvoucher.mapper.StaffVoucherMapper;
import com.bjxczy.onepark.staffvoucher.pojo.dto.command.RemoveAccessCommand;
import com.bjxczy.onepark.staffvoucher.pojo.po.AccessCardPO;
import com.bjxczy.onepark.staffvoucher.service.AccessCardService;
import com.bjxczy.onepark.staffvoucher.service.StaffVoucherService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author: wangxiling
 * @Description: 订阅员工管理服务
 * @CreateTime: 2023-07-03  10:52  //时间
 */
@Slf4j
@Component
public class StaffEventListener extends OrganizationBaseListener {
    @Autowired
    private StaffVoucherService staffVoucherService;
    @Resource
    private StaffVoucherMapper staffVoucherMapper;
    @Resource
    private AccessCardService accessCardService;
    @Resource
    private AccessCardMapper accessCardMapper;
    // 发布事件
    @Autowired
    private ApplicationEventPublisher publisher;

    @RabbitListener(bindings = {@QueueBinding(value = @Queue(name = "put_voucher_and_card_queue"), key = UPDATE_STAFF_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void updateStaffEventHandler(StaffInformation staffInfomation, Channel channel, Message message) {
        Integer staffId = staffInfomation.getId();
        List<DeptNameInfo> deptNameInfos = StrUtils.DeptNameInfo(staffInfomation.getDeptNames());
        List<AccessCardPO> accessCardPOS = accessCardMapper.selectList(new QueryWrapper<AccessCardPO>().eq("staff_id", staffId).eq("del_flag", 0));
        if (staffInfomation.getDimission() == 0) {// 在职职
            if (accessCardPOS != null && accessCardPOS.size() > 0) {
                // 修改卡
                AccessCardPO Card = new AccessCardPO();
                Card.setDeptId(staffInfomation.getOrganizationId());
                Card.setDeptName(deptNameInfos.get(0).getDeptName());
                Card.setComeFrom(staffInfomation.getComeFrom());
                Card.setMobile(staffInfomation.getMobile());
                Card.setRankId(staffInfomation.getRankId());
                Card.setEmployeeNo(staffInfomation.getEmployeeNo());
                Card.setRankName(staffInfomation.getRankName());
                Card.setStaffName(staffInfomation.getFldName());
                Card.setComeFrom(accessCardPOS.get(0).getComeFrom());
                Card.setUpdateTime(new Date());
                accessCardMapper.update(Card, new QueryWrapper<AccessCardPO>().eq("staff_id", staffId).eq("del_flag", 0));
            }
            // 修改凭证
            StaffVoucher voucher = new StaffVoucher();
            voucher.setDeptId(staffInfomation.getOrganizationId());
            voucher.setDeptName(deptNameInfos.get(0).getDeptName());
            voucher.setComeFrom(staffInfomation.getComeFrom());
            voucher.setMobile(staffInfomation.getMobile());
            voucher.setRankId(staffInfomation.getRankId());
            voucher.setEmployeeNo(staffInfomation.getEmployeeNo());
            voucher.setRankName(staffInfomation.getRankName());
            voucher.setStaffName(staffInfomation.getFldName());
            voucher.setIdentifyCard(staffInfomation.getIdentityCard());
            staffVoucherMapper.update(voucher, new QueryWrapper<StaffVoucher>().eq("staff_id", staffId).eq("del_flag", 0));
        }
        if (staffInfomation.getDimission() == 1) {// 离职
            if (accessCardPOS != null && accessCardPOS.size() > 0) {
                Integer integer = accessCardMapper.updateFlagById(accessCardPOS.get(0).getId());
                AccessCard accessCard = new AccessCard();
                BeanUtils.copyProperties(accessCardPOS.get(0), accessCard);
                if (integer > 0) {
                    publisher.publishEvent(new DelCertifyEvent(this, accessCard));
                }
            }
        }
        this.confirm(() -> true, channel, message);
    }

    /**
     * 删除员工门禁卡表数据， 删除员工凭证表门禁卡数据
     */
    @RabbitListener(bindings = {@QueueBinding(value = @Queue(name = "delete_voucher_and_card_queue"), key = REMOVE_STAFF_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void removeStaffEventHandler(RemoveStaffInfo RemoveStaffInfo, Channel channel, Message message) {
        Integer staffId = RemoveStaffInfo.getId();
        Integer accessCardId = accessCardService.selectAccessCardByStaffId(staffId);
        if (ObjectUtils.isEmpty(accessCardId)) {
            throw new ResultException("成功订阅员工更新时间..不存在该员工凭证卡");
        }
        accessCardService.delAccessCard(new RemoveAccessCommand(accessCardId));
        StaffVoucher staffVoucher = staffVoucherMapper.selectByStaffId(staffId);
        staffVoucherMapper.deleteById(staffVoucher.getId());
        this.confirm(() -> true, channel, message);
    }

    /**
     * 本地凭证新增事件订阅
     *
     * @param Event
     */
    @EventListener
    public void AddEventHandler(AddCertifyEvent Event) {
        try {
            staffVoucherService.addCertify(Event.getAccessCard(), Event.getStaff());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 本地凭证删除事件订阅
     *
     * @param Event
     */
    @EventListener
    public void removeEventHandler(DelCertifyEvent Event) {
        staffVoucherService.deleteCertify(Event);
    }

}
