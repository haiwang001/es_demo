package com.bjxczy.onepark.door.pojo.dto;


import com.bjxczy.onepark.staffvoucher.pojo.dto.BasePageDTO;
import lombok.Data;

/*
 *@ClassName AuthorizationRecordDto
 *@Author 温良伟
 *@Date 2023/3/8 17:12
 *@Version 1.0
 */
@Data
public class AuthorizationRecordDto extends BasePageDTO {
    /**
     * 授权描述
     */
    private String description;
    /**
     * 授权时间
     */
    private String date;
}
