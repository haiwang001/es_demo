package com.bjxczy.onepark.door.pojo.dto;


import com.bjxczy.onepark.staffvoucher.pojo.dto.BasePageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *@Author wlw
 *@Date 2023/7/10 13:35
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ParticularsDto extends BasePageDTO {
    /**
     * 授权记录id
     */
    private String id;
    /**
     * 授权状态
     */
    private String authorizationState;
}
