package com.bjxczy.onepark.staffvoucher.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.staffvoucher.entity.StaffNode;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffQueryListDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StaffNodeMapper extends BaseMapper<StaffNode> {
    @Select("select *  from staff_node sn  where del_flag = 0 and group_node_id = #{dto.groupId}\n" +
            "  and  if(#{dto.synthesize} != '',  sn.employee_no like concat('%',#{dto.synthesize},'%')\n" +
            "                         or sn.staff_name like concat('%',#{dto.synthesize},'%')\n" +
            "                          or sn.mobile like concat('%',#{dto.synthesize},'%'), 1=1 ) order by sn.id")
    List<StaffNode> queryList(@Param("dto") StaffQueryListDto dto);
}
