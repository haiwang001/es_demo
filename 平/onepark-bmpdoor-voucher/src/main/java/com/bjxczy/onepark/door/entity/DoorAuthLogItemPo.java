package com.bjxczy.onepark.door.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/*
 *@Author wlw
 *@Date 2023/7/5 9:32
 */
@Data
@TableName("d_door_auth_log_item")
public class DoorAuthLogItemPo {

    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * auth_log_id 授权记录id
     */
    private String authLogId;
    /**
     * device_id 设备id
     */
    private String deviceId;
    /**
     * device_name 设备名称
     */
    private String deviceName;
    /**
     *  space_name 设备区域空间名称：XXXX/XXX/XXX
     */
    private String spaceName;
    /**
     * 授权员工id staff_id
     */
    private String staffId;
    /**
     * 授权员工名称 staff_name
     */
    private String staffName;
    /**
     * 手机号 mobile
     */
    private String mobile;
    /**
     * 部门ID dept_id
     */
    private String deptId;
    /**
     * dept_name 部门名称：XXX公司/XX部门/XX
     */
    private Object deptName;
    /**
     * sync_status  同步状态：0 - 下发中；1 - 下发成功；2 - 下发失败
     */
    private Integer syncStatus;
    /**
     * sync_status_text  同步状态描述
     */
    private String syncStatusText;
    /**
     * 归属平台：（platformType） platform_type
     */
    private String platformType;
    /**
     * save_time 授权时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date saveTime;

    public DoorAuthLogItemPo(String authLogId, String deviceId, String deviceName, String spaceName, String staffId, String staffName, String mobile, String deptId, String deptName, Integer syncStatus, String syncStatusText, Date saveTime,String platformType) {
        this.authLogId = authLogId;
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.spaceName = spaceName;
        this.staffId = staffId;
        this.staffName = staffName;
        this.mobile = mobile;
        this.deptId = deptId;
        this.deptName = deptName;
        this.syncStatus = syncStatus;
        this.syncStatusText = syncStatusText;
        this.saveTime = saveTime;
        this.platformType = platformType;
    }
}
