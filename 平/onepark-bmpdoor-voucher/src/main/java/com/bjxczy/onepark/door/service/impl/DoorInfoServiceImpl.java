package com.bjxczy.onepark.door.service.impl;


import com.alibaba.nacos.common.utils.UuidUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.core.rabbitmq.event.door.AddStaffAuthDTO;
import com.bjxczy.core.rabbitmq.event.door.AuthorityDeviceDTO;
import com.bjxczy.core.rabbitmq.event.door.RemoveStaffAuthDTO;
import com.bjxczy.onepark.common.client.BmpQueryStaffServer;
import com.bjxczy.onepark.common.constant.enums.PlatformTypeCode;
import com.bjxczy.onepark.common.constant.enums.SyncStatusCode;
import com.bjxczy.onepark.common.event.RemoveAuthorityEvent;
import com.bjxczy.onepark.common.event.UpdateAuthorityEvent;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.utils.ExcelUtils;
import com.bjxczy.onepark.common.utils.StrUtils;
import com.bjxczy.onepark.door.command.*;
import com.bjxczy.onepark.door.entity.DoorAuthLogItemPo;
import com.bjxczy.onepark.door.entity.DoorAuthLogPo;
import com.bjxczy.onepark.door.entity.DoorAuthorizationPo;
import com.bjxczy.onepark.door.entity.DoorInfoPo;
import com.bjxczy.onepark.door.mapper.DoorAuthLogItemMapper;
import com.bjxczy.onepark.door.mapper.DoorAuthLogMapper;
import com.bjxczy.onepark.door.mapper.DoorAuthorizationMapper;
import com.bjxczy.onepark.door.mapper.DoorInfoMapper;
import com.bjxczy.onepark.door.pojo.dto.DoorPageDto;
import com.bjxczy.onepark.door.pojo.dto.LookPageDto;
import com.bjxczy.onepark.door.pojo.vo.DoorOverviewVo;
import com.bjxczy.onepark.door.pojo.vo.DoorPageVo;
import com.bjxczy.onepark.door.pojo.vo.LookPageVo;
import com.bjxczy.onepark.door.service.DoorInfoService;
import com.bjxczy.onepark.staffvoucher.entity.StaffVoucher;
import com.bjxczy.onepark.staffvoucher.mapper.StaffVoucherMapper;
import com.bjxczy.onepark.visitor.mapper.VisitorRightsAndDoorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.bjxczy.onepark.common.constant.DeviceTypes.HANWANG_TYPE_CODE;
import static com.bjxczy.onepark.common.constant.DeviceTypes.HIKVISION_TYPE_CODE;

/*
 *@Author wlw
 *@Date 2023/6/25 16:13
 */
@Service
public class DoorInfoServiceImpl extends
        ServiceImpl<DoorInfoMapper, DoorInfoPo> implements DoorInfoService {
    @Resource
    private DoorInfoMapper mapper;
    @Resource
    private DoorAuthLogMapper doorAuthLogMapper;
    @Resource
    private ApplicationEventPublisher publisher;
    @Resource
    private BmpQueryStaffServer bmpQueryStaffServer;
    @Resource
    private VisitorRightsAndDoorMapper visitorRightsAndDoorMapper;
    @Resource
    private DoorAuthorizationMapper doorAuthorizationMapper;
    @Resource
    private DoorAuthLogItemMapper doorAuthLogItemMapper;
    /**
     * 凭证模块
     */
    @Resource
    private StaffVoucherMapper voucherMapper;
    @Autowired
    private BmpQueryStaffServer StaffServer;

    @Override
    public IPage<DoorPageVo> pageList(DoorPageDto dto) {
        return mapper.pageList(dto.toPage(), dto);
    }

    @Override
    public IPage<LookPageVo> LookPage(LookPageDto dto) {
        return doorAuthorizationMapper.LookPage(dto.toPage(), dto);
    }

    @Override
    public DoorOverviewVo overview(String id) {
        List<DoorInfoPo> list = id != null ? lambdaQuery().eq(DoorInfoPo::getAreaId, id).list() : lambdaQuery().list();
        Map<Integer, List<DoorInfoPo>> collect = list.stream().collect(Collectors.groupingBy(DoorInfoPo::getOnlineStatus));
        return new DoorOverviewVo(list.size(), collect.get(1) == null ? 0 : collect.get(1).size(), collect.get(0) == null ? 0 : collect.get(0).size());
    }

    @Override
    public void lookExport(HttpServletResponse response, LookPageDto dto) {
        try {
            ExcelUtils.export(response,
                    "授权员工列表",
                    "授权员工列表",
                    doorAuthorizationMapper.lookExport(dto), LookPageVo.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Integer setVisitPermission(PutDoorVisitorCommand com) {
        DoorInfoPo one = lambdaQuery().eq(DoorInfoPo::getId, com.getDeviceId()).one();
        if (one == null) {
            return null;
        }
        if (one.getDeviceType().equals(HIKVISION_TYPE_CODE)) {
            if (visitorRightsAndDoorMapper.selectVisitorRights(one.getDeviceCode()) == 0) {
                throw new ResultException("此设备未分配访客权限组!");
            }
        }
        one.setVisitPermission(com.getVisitPermission());
        return mapper.updateById(one);
    }

    @Override
    public Integer setStaffPermission(PutDoorStaffCommand com) {
        DoorInfoPo one = lambdaQuery().eq(DoorInfoPo::getId, com.getDeviceId()).one();
        if (one == null) {
            return null;
        }
        one.setStaffPermission(com.getStaffPermission());
        return mapper.updateById(one);
    }


    @Override
    public Integer putDoorDirection(PutDoorDirectionCommand com) {
        DoorInfoPo one = lambdaQuery().eq(DoorInfoPo::getId, com.getDeviceId()).one();
        if (one == null) {
            return null;
        }
        one.setDirection(com.getDirection());
        return mapper.updateById(one);
    }

    /**
     * 授权多人多设备
     *
     * @param com
     * @param sysUser
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Integer authorization(AuthorizationCommand com, UserInformation sysUser) {
        // 授权批次业务id
        String doorAuthLogId = UuidUtils.generateUuid();
        // 授权门禁
        Authorization(com, doorAuthLogId);
        // 授权批次日志
        doorAuthLogMapper.insert(new DoorAuthLogPo(doorAuthLogId, com.getDescription(), sysUser.getStaffName(), new Date()));
        // 绑定凭证和设备
        bindingVoucherAndDevice(com, doorAuthLogId);
        return 1;
    }

    /**
     * 绑定设备和人入库下发记录
     *
     * @param com   授权参数
     * @param LogId 授权批次业务id
     */
    private void bindingVoucherAndDevice(AuthorizationCommand com, String LogId) {
        com.getStaffIds().forEach(el -> {
            com.getFacilityCodes().forEach(devid -> {
                StaffInformation staff = StaffServer.findById(el);
                List<DeptNameInfo> deptNameInfos = StrUtils.DeptNameInfo(staff.getDeptNames());
                StaffVoucher voucher = voucherMapper.selectOne(new QueryWrapper<StaffVoucher>().eq("staff_id", el));
                DoorInfoPo devId = mapper.selectOne(new QueryWrapper<DoorInfoPo>().eq("device_code", devid));
                // 绑定 设备和凭证
                DoorAuthorizationPo doorAuthorizationPo = new DoorAuthorizationPo(devId.getId(), staff.getId().toString(), new Date());
                // 授权 详情
                DoorAuthLogItemPo doorAuthLogItemPo = new DoorAuthLogItemPo(
                        LogId,
                        devId.getId(),
                        devId.getDeviceName(),
                        devId.getSpaceName(),
                        staff.getId().toString(),
                        staff.getFldName(),
                        staff.getMobile(),
                        deptNameInfos.get(0).getId().toString(),
                        deptNameInfos.get(0).getDeptName(),
                        SyncStatusCode.UNDER_WAY.getCode(),
                        SyncStatusCode.UNDER_WAY.getMsg(),
                        new Date(), getTypeByDeviceType(devId.getDeviceType()));
                if (voucher == null) {
                    doorAuthLogItemPo.setSyncStatus(SyncStatusCode.ERROR.getCode());
                    doorAuthLogItemPo.setSyncStatusText(SyncStatusCode.ERROR.getMsg() + ": 无凭证!");
                } else {
                    if (voucher.getFaceImg() == null) {
                        doorAuthLogItemPo.setSyncStatus(SyncStatusCode.ERROR.getCode());
                        doorAuthLogItemPo.setSyncStatusText(SyncStatusCode.ERROR.getMsg() + ": 无人脸凭证!");
                    }
                }
                doorAuthorizationMapper.insert(doorAuthorizationPo);
                doorAuthLogItemMapper.insert(doorAuthLogItemPo);
            });
        });
    }

    /**
     * 通过设备类型加入对应平台的标记
     *
     * @param deviceType
     * @return
     */
    private String getTypeByDeviceType(String deviceType) {
        return deviceType.equals(HIKVISION_TYPE_CODE) ?
                PlatformTypeCode.HIK.getMsg() :
                deviceType.equals(HANWANG_TYPE_CODE) ?
                        PlatformTypeCode.HANWNAG.getMsg() : "未知平台";
    }

    /**
     * 清除 1 人 1 设备
     *
     * @param com 删除参数
     * @return 删除凭证结果 1 成功 0失败
     */
    @Override
    public Integer clearAuthorization(DeleteAuthorizationCommand com) {
        // 清除授权
        clearHAuthorization(com);
        StaffVoucher voucher = voucherMapper.selectOne(new QueryWrapper<StaffVoucher>().eq("staff_id", com.getStaffId()));
        // 删除 凭证绑定
        return doorAuthorizationMapper.delete(new QueryWrapper<DoorAuthorizationPo>()
                .eq("device_id", com.getDeviceId())
                .eq("voucher_id", voucher.getId()));
    }

    /**
     * 凭证触发取消授权
     * 清除 单人 多设备
     *
     * @param staffId
     */
    @Override
    public void doorClearInteriorAuthorization(Integer staffId) {
        List<DoorInfoPo> devices = lambdaQuery().list();
        // 清除授权
        DeleteAuthorizationCommand hanwng = new DeleteAuthorizationCommand();
        hanwng.setStaffId(staffId);
        devices.forEach(el -> {
            hanwng.setDeviceId(el.getId());
            clearHAuthorization(hanwng);
        });
    }

    /**
     * 凭证触发授权
     * 授权多人 多设备
     *
     * @param staffId
     */
    @Override
    public void doorCreateInteriorAuthorization(Integer staffId) {
        List<DoorInfoPo> devices = lambdaQuery().in(DoorInfoPo::getStaffPermission, 1).list();
        if (devices == null) {
            return;
        }
        // 授权
        AuthorizationCommand hanwng = new AuthorizationCommand();
        hanwng.setStaffIds(Collections.singletonList(staffId));
        hanwng.setFacilityCodes(getDeviceCodes(devices));
        String authorizationId = staffId + "---" + getId(devices);
        Authorization(hanwng, authorizationId);
    }

    /**
     * 拼接 业务id
     *
     * @param collect
     * @return
     */
    private StringBuffer getId(List<DoorInfoPo> collect) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < collect.size(); i++) {
            if (collect.size() - 1 == i) { // 最后
                buffer.append(collect.get(i).getDeviceCode());
            } else {
                buffer.append(collect.get(i).getDeviceCode()).append("+++");
            }
        }
        return buffer;
    }

    /**
     * @param collect 设备列表
     * @return 设备编码列表
     */
    private List<String> getDeviceCodes(List<DoorInfoPo> collect) {
        List<String> list = new ArrayList<>();
        collect.forEach(el -> list.add(el.getDeviceCode()));
        return list;
    }

    /**
     * 清除员工权限
     *
     * @param com
     */
    private void clearHAuthorization(DeleteAuthorizationCommand com) {
        StaffInfoFine staff = bmpQueryStaffServer.getStaffById(com.getStaffId());
        DoorInfoPo info = lambdaQuery().in(DoorInfoPo::getId, com.getDeviceId()).one();
        String romId = com.getStaffId() + "---" + info.getDeviceCode();
        RemoveAuthorityEvent event = new RemoveAuthorityEvent(this, romId,
                Collections.singletonList(new AuthorityDeviceDTO(info.getDeviceCode(), info.getDeviceName(), info.getDeviceType())),
                Collections.singletonList(new RemoveStaffAuthDTO(com.getStaffId())));
        publisher.publishEvent(event);
    }


    /**
     * 多设备授权
     *
     * @param com           授权参数
     * @param doorAuthLogId 授权批次id
     */
    @Async
    public void Authorization(AuthorizationCommand com, String doorAuthLogId) {
        //设备信息
        List<DoorInfoPo> devices = lambdaQuery().in(DoorInfoPo::getDeviceCode, com.getFacilityCodes()).list();
        List<AuthorityDeviceDTO> adList = devices.stream().map(e ->
                new AuthorityDeviceDTO(e.getDeviceCode(), e.getDeviceName(), e.getDeviceType())).collect(Collectors.toList());
        // 员工信息
        List<AddStaffAuthDTO> asList = new ArrayList<>();
        com.getStaffIds().forEach(el -> { // 更换为凭证管理的信息
            List<StaffVoucher> staff_id = voucherMapper.selectList(new QueryWrapper<StaffVoucher>().eq("staff_id", el));
            if (staff_id != null) {
                if (staff_id.size() == 0) {
                    throw new ResultException("员工无凭证 无法授权!");
                }
                StaffVoucher e = staff_id.get(0);
                if (e != null && e.getFaceImg() != null) {
                    AddStaffAuthDTO atd = new AddStaffAuthDTO();
                    atd.setStaffId(e.getStaffId());
                    atd.setStaffName(e.getStaffName());
                    atd.setEmployeeNo(e.getEmployeeNo());
                    atd.setIdCard(e.getIdentifyCard());
                    atd.setAccessCode(e.getAccessCode() != null ? e.getAccessCode() : e.getMobile()); // TODO 替换为凭证管理门禁卡号
                    atd.setFaceUrl(e.getFaceImg());
                    atd.setUuid(String.valueOf(e.getId()));
                    asList.add(atd);
                }
            }
        });
        publisher.publishEvent(new UpdateAuthorityEvent(this, doorAuthLogId, adList, asList));
    }
}
