package com.bjxczy.onepark.visitor.entity.visitor;


import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * visitorSyncLog
 */

@Data
@TableName("v_visitor_invite_info")
public class VisitorInfoPo {
    /**
     * 主键id
     */
    private String id;

    /**
     * 访客姓名          visitor_name
     */
    private String visitorName;
    /**
     * 访客手机号       visitor_mobile
     */
    private String visitorMobile;
    /**
     * 访客证件类型 visitor_id_card_type
     */
    private String visitorIdCardType;
    /**
     * 访客证件号       visitor_id_card
     */
    private String visitorIdCard;
    /**
     * 访客照片         visitor_photo
     */
    private String visitorPhoto;
    /**
     * 到访时间            visit_time
     */
    private String visitTime;
    /**
     * 结束时间            leave_time
     */
    private String leaveTime;
    /**
     * 接访人id      receptionist_id
     */
    private Integer receptionistId;

    /**
     * create_time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * update_time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    private String operator;
    /**
     * del_flag
     */
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;
    /**
     * arrive_type 访问事由 0：入职，1：面试，2：送货，3：开会，4：出差，5：施工，6：其他原因
     */
    private Integer arriveType;
    /**
     * arrive_status 到访状态   1:未到访，2:已迟到，3:已到访，4:已取消，5:已结束
     */
    private Integer arriveStatus;
    /**
     *  访客备注 visitor_remark
     */
    private String visitorRemark;

    /**
     * 访问预园区id  area_id
     */
    private String areaId;
    /**
     * 楼id tenant_id
     */
    private String tenantId;
    /**
     * 楼层   space_id
     */
    private String spaceId;
    /**
     * 访客单位          visitor_work
     */
    private String visitorWork;
    /**
     * 访客性别       visitor_gender  1 男  2 女
     */
    private Integer visitorGender;
    /**
     * 接访人部门 receptionist_dept_name  xx/xx/xx
     */
    private String receptionistDeptName;

    /**
     * 空间名称展示        space_name  xx/xx/xx
     */
    private String spaceName;
    /**
     * order_id 用于访客签离/删除
     */
    private String orderId;

    public VisitorInfoPo() {

    }

    public VisitorInfoPo(String visitorName, String visitorMobile, String visitorIdCardType, String visitorIdCard, String visitorPhoto, String visitTime, String leaveTime, Integer receptionistId, Date createTime, Date updateTime, String operator, Integer delFlag, Integer arriveType, Integer arriveStatus, String visitorRemark) {
        this.visitorName = visitorName;
        this.visitorMobile = visitorMobile;
        this.visitorIdCardType = visitorIdCardType;
        this.visitorIdCard = visitorIdCard;
        this.visitorPhoto = visitorPhoto;
        this.visitTime = visitTime;
        this.leaveTime = leaveTime;
        this.receptionistId = receptionistId;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.operator = operator;
        this.delFlag = delFlag;
        this.arriveType = arriveType;
        this.arriveStatus = arriveStatus;
        this.visitorRemark = visitorRemark;
    }

    public VisitorInfoPo(String id, String orderId) {
        this.id = id;
        this.orderId = orderId;
    }
    public VisitorInfoPo(String id, Integer arriveStatus) {
        this.id = id;
        this.arriveStatus = arriveStatus;
    }
}
