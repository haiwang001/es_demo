package com.bjxczy.onepark.door.entity;


import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/*
 *@Author wlw
 *@Date 2023/6/25 15:31 门禁控制器
 */
@Data
@TableName("d_door_info")
public class DoorInfoPo implements Serializable {
    private static final long serialVersionUID = -4618982155939467009L;
    private String id;
    /**
     * device_code 设备编码
     */
    private String deviceCode;
    /**
     * device_type 设备类型
     */
    private String deviceType;
    /**
     * 设备名 device_name
     */
    private String deviceName;
    /**
     * online_status 在线状态 0不在线  1在线
     */
    private Integer onlineStatus;
    /**
     * platform_type 归属平台
     */
    private String platformType; //归属平台：字典（platformType
    /**
     * direction; 设备方向 (1-进，2-出)
     */
    private Integer direction;
    /**
     * staff_permission 员工权限 (1-开，2-关)
     */
    private Integer staffPermission;
    /**
     * visit_permission 访客权限 (1-开，2-关)
     */
    private Integer visitPermission;
    /**
     * last_data_time
     */
    private Date lastDataTime;
    /**
     * create_time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * update_time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    /**
     * operator
     */
    private String operator;
    /**
     * del_flag
     */
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;
    /**
     * 园区id  area_id
     */
    private String areaId;
    /**
     * 楼id tenant_id
     */
    private String tenantId;
    /**
     * 楼层   space_id
     */
    private String spaceId;
    /**
     * 空间名称展示        space_name  xx/xx/xx
     */
    private String spaceName;
    /**
     * 海康设备需要使用ip查询是否在线
     */
    private String ip;
}
