package com.bjxczy.onepark.staffvoucher.pojo.dto;


import lombok.Data;

import java.util.List;

/*
 *@Date 2023/2/8 16:24
 */
@Data
public class StaffDelDto {
    private String groupId;
    private List<Integer> staffIds;
}
