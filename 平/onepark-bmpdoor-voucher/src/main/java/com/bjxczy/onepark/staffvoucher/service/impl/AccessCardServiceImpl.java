package com.bjxczy.onepark.staffvoucher.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.onepark.common.client.BmpHikServiceFeign;
import com.bjxczy.onepark.common.client.BmpQueryStaffServer;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.utils.StrUtils;
import com.bjxczy.onepark.staffvoucher.entity.AccessCard;
import com.bjxczy.onepark.staffvoucher.event.AddCertifyEvent;
import com.bjxczy.onepark.staffvoucher.event.DelCertifyEvent;
import com.bjxczy.onepark.staffvoucher.mapper.AccessCardMapper;
import com.bjxczy.onepark.staffvoucher.pojo.dto.command.CreateAccessCommand;
import com.bjxczy.onepark.staffvoucher.pojo.dto.command.RemoveAccessCommand;
import com.bjxczy.onepark.staffvoucher.pojo.po.AccessCardPO;
import com.bjxczy.onepark.staffvoucher.service.AccessCardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Author: wangxiling
 * @CreateTime: 2023-07-11  11:10  //时间
 */
@Slf4j
@Service
public class AccessCardServiceImpl implements AccessCardService {
    @Autowired
    private BmpQueryStaffServer bmpQueryStaffServer;
    @Autowired
    private AccessCardMapper accessCardMapper;
    // 发布事件
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private BmpHikServiceFeign serviceFeign;

    @Override
    public Boolean createCard(CreateAccessCommand createAccessCommand) {
        if (createAccessCommand.getAccessCode().length() != 10) {
            throw new ResultException("卡号应该是10位纯数字!");
        }
        // 查询的用户数据信息
        StaffInfoFine staff = bmpQueryStaffServer.getStaffById(createAccessCommand.getStaffId());
        AccessCard accessCard = AccessCard.builder()
                .id(null)
                .staffId(createAccessCommand.getStaffId())
                .employeeNo(staff.getEmployeeNo())
                .staffName(staff.getFldName())
                .mobile(staff.getMobile())
                .deptId(staff.getOrganizationId())
                .rankId(staff.getRankId())
                .rankName(staff.getRankName())
                .comeFrom(createAccessCommand.getComeFrom())
                .accessCode(createAccessCommand.getAccessCode())
                .createTime(new Date())
                .updateTime(new Date())
                .operator(createAccessCommand.getOperator())
                .delFlag(0)
                .build();
        // 封装部门信息
        List<DeptNameInfo> deptNameInfo = StrUtils.DeptNameInfo(staff.getDeptNames());
        accessCard.setDeptName(deptNameInfo.get(0).getDeptName());
        // 添加凭证，判断凭证是否存在
        Boolean flag = this.saveAccessCard(accessCard);
        // 凭证添加成功，发布添加凭证事件
        if (flag) {
            publisher.publishEvent(new AddCertifyEvent(this, accessCard, staff));
        }
        return flag;
    }

    @Override
    public void delAccessCard(RemoveAccessCommand removeAccessCommand) {
        AccessCardPO accessCardPo = accessCardMapper.selectById(removeAccessCommand.getId());
        if (accessCardPo == null) {
            throw new ResultException("门禁卡不存在！");
        } else {
            Integer integer = accessCardMapper.updateFlagById(removeAccessCommand.getId());
            AccessCard accessCard = new AccessCard();
            BeanUtils.copyProperties(accessCardPo, accessCard);
            if (integer > 0) {
                publisher.publishEvent(new DelCertifyEvent(this, accessCard));
            }
        }
    }

    @Override
    public Integer selectAccessCardByStaffId(Integer staffId) {
        List<Integer> integers = accessCardMapper.selectAccessCardByStaffId(staffId);
        if (integers != null) {
            return integers.size() > 0 ? integers.get(0) : null;
        }
        return 0;
    }

    private boolean saveAccessCard(AccessCard accessCard) {
        AccessCardPO selectOne = accessCardMapper.selectOne(new QueryWrapper<AccessCardPO>()
                .eq("employee_no", accessCard.getEmployeeNo())
                .eq("del_flag", 0));
        if (selectOne != null) {
            throw new ResultException("员工已存在门禁卡！");
        }
        if (this.isExistsAccessCard(accessCard.getAccessCode())) {
            throw new ResultException("门禁卡号已存在！");
        }
        AccessCardPO accessCardPo = new AccessCardPO();
        BeanUtils.copyProperties(accessCard, accessCardPo);
        int insert = accessCardMapper.insert(accessCardPo);
        return insert > 0;
    }

    public boolean isExistsAccessCard(String accessCode) {
        List<AccessCardPO> list = accessCardMapper.selectAccessCode(accessCode);
        return list != null && list.size() > 0;
    }
}
