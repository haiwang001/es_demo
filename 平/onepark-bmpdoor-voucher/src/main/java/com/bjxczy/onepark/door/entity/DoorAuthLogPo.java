package com.bjxczy.onepark.door.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/*
门禁授权记录表
 *@Author wlw
 *@Date 2023/7/4 17:16
 */
@Data
@TableName("d_door_auth_log")
public class DoorAuthLogPo {
    /**
     * 记录id
     */
    private String id;
    /**
     * 描述
     */
    private String remark;
    /**
     * 操作人
     */
    private String operator;
    /**
     * 授权时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date saveTime;

    public DoorAuthLogPo(String id, String remark, String operator, Date save_time) {
        this.id = id;
        this.remark = remark;
        this.operator = operator;
        this.saveTime = save_time;
    }
}
