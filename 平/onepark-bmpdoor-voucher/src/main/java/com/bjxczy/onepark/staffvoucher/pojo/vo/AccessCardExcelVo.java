package com.bjxczy.onepark.staffvoucher.pojo.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

@Data
public class AccessCardExcelVo {
    /**
     * id
     */
    //private Integer id;
    /**
     * 员工ID
     */
    //private Integer staffId;
    /**
     * 员工编号
     */
    @Excel(name = "员工编号", width = 14, replace = {"_null"})
    private String employeeNo;
    /**
     * 员工姓名
     */
    @Excel(name = "姓名", width = 14, replace = {"_null"})
    private String staffName;
    /**
     * 手机号
     */
    @Excel(name = "手机号", width = 15, replace = {"_null"})
    private String mobile;
    /**
     * 部门
     */
    @Excel(name = "部门编号", width = 12, replace = {"_null"})
    private String deptId;
    /**
     * 部门
     */
    @Excel(name = "部门名称", width = 30, replace = {"_null"})
    private String deptName;
    /**
     * 门禁卡类型
     */
    @Excel(name = "门禁卡类型", width = 13, replace = {"_null"})
    private String accessTypeName;
    /**
     * 租户ID
     */
    //private String tenantId;
  /*  @Excel(name="局址",width=15,replace={"_null"})
    private String tsrName;*/
    /**
     * 门禁卡号
     */
    @Excel(name = "门禁卡号", width = 20, replace = {"_null"})
    private String accessCode;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 25, replace = {"_null"})
    private String createTime;
    /**
     *更新时间
     */
   /* @Excel(name="更新时间",width=15,replace={"_null"})
    private String updateTime;*/
    /**
     * 备注
     */
   /* @Excel(name = "备注", width = 20, replace = {"_null"})
    private String remark;*/
    /**
     * 操作人
     */
    @Excel(name = "操作人", width = 15, replace = {"_null"})
    private String operator;
    /**
     *是否删除
     */
    //private Integer delFlag;

    /**
     * 职级id
     */
    //private Integer rankId;

}
