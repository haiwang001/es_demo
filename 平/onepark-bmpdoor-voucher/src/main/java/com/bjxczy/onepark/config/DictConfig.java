package com.bjxczy.onepark.config;


import com.bjxczy.core.web.dict.DictService;
import com.bjxczy.core.web.dict.JdbcDictServiceImpl;
import com.bjxczy.onepark.common.constant.VoucherSysConst;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 *@Date 2023/1/31 15:11
 *@Version 1.0
 */
@Configuration
public class DictConfig {
    @Bean
    public DictService dictService() {
        // 字典表
        return new JdbcDictServiceImpl(VoucherSysConst.DICT_TABLE_NAME);
    }
}
