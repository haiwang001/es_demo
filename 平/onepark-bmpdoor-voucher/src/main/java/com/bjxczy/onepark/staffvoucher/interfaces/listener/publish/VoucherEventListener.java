package com.bjxczy.onepark.staffvoucher.interfaces.listener.publish;


import com.alibaba.nacos.common.utils.UuidUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.core.rabbitmq.supports.CardBaseListener;
import com.bjxczy.core.rabbitmq.supports.FaceBaseListener;
import com.bjxczy.core.rabbitmq.supports.OrganizationBaseListener;
import com.bjxczy.onepark.common.client.BmpHikServiceFeign;
import com.bjxczy.onepark.common.client.BmpQueryStaffServer;
import com.bjxczy.onepark.common.event.ClearInteriorAuthorizationEvent;
import com.bjxczy.onepark.common.event.CreateInteriorAuthorizationEvent;
import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.OrganizationInformation;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.voucher.CardInfoTransmit;
import com.bjxczy.onepark.common.utils.DateUtil;
import com.bjxczy.onepark.door.command.AuthorizationCommand;
import com.bjxczy.onepark.door.command.DeleteAuthorizationCommand;
import com.bjxczy.onepark.door.entity.DoorInfoPo;
import com.bjxczy.onepark.door.mapper.DoorInfoMapper;
import com.bjxczy.onepark.door.service.DoorInfoService;
import com.bjxczy.onepark.door.service.impl.DoorInfoServiceImpl;
import com.bjxczy.onepark.staffvoucher.event.AddCertifySyncEvent;
import com.bjxczy.onepark.staffvoucher.event.DelCertifySyncEvent;
import com.bjxczy.onepark.staffvoucher.event.PudCertifySyncEvent;
import com.google.common.collect.ImmutableList;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import java.util.List;

import static com.bjxczy.onepark.common.constant.DeviceTypes.HANWANG_TYPE_CODE;


/*
 *@Author wlw
 *@Date 2023/7/13 13:44
 */
@Component
public class VoucherEventListener {
    @Autowired
    private BmpQueryStaffServer StaffServer;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private BmpHikServiceFeign serviceFeign;
    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private DoorInfoServiceImpl doorInfoService;
    @Resource
    private DoorInfoMapper mapper;

    /**
     * 凭证 新增事件
     *
     * @param event
     */
    @EventListener
    public void AddCertifyLinkageHiKEvent(AddCertifySyncEvent event) {
        StaffInfoFine staff = StaffServer.getStaffById(event.getStaffId());
        if (staff == null) {
            return;
        }
        // 查看该员工组织是否存在  存在
        Boolean orgHik = null;
        try {
            orgHik = serviceFeign.isOrgHik(staff.getOrganizationId());
        } catch (RuntimeException e) {
            e.printStackTrace();
            return;
        }
        if (!orgHik) {
            DeptNameInfo deptNameInfo = staff.getDeptNames().get(staff.getDeptNames().size() - 1);
            OrganizationInformation information = new OrganizationInformation();
            information.setFldName(deptNameInfo.getDeptName());
            information.setId(staff.getOrganizationId());
            information.setParentId(-1);
            rabbitTemplate.convertAndSend(
                    OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME,
                    OrganizationBaseListener.CREATE_ORGANIZATION_EVENT,
                    information);
            DateUtil.await();
        }

        if (!serviceFeign.isStaffHik(event.getStaffId())) {
            StaffInformation information1 = new StaffInformation();
            information1.setId(event.getStaffId());
            information1.setFldName(staff.getFldName());
            information1.setOrganizationId(staff.getOrganizationId());
            information1.setMobile(staff.getMobile());
            information1.setUuid(UuidUtils.generateUuid());
            information1.setEmployeeNo(staff.getEmployeeNo());
            information1.setDeptNames(staff.getDeptNames());
            rabbitTemplate.convertAndSend(
                    OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME,
                    OrganizationBaseListener.CREATE_STAFF_EVENT,
                    information1);
            DateUtil.await();
        }
        // 开始同步 信息
        synchronizationMsg(event, staff);
    }

    /**
     * 同步 人脸照片/ic卡 和 单人授权
     *
     * @param event
     * @param staff
     */
    private void synchronizationMsg(AddCertifySyncEvent event, StaffInfoFine staff) {
        if (event.getFaceImg() != null) {
            FaceInfoTransmit faceBaseListener = new FaceInfoTransmit();
            faceBaseListener.setStaffId(event.getStaffId());
            faceBaseListener.setFaceImg(event.getFaceImg());
            rabbitTemplate.convertAndSend(
                    FaceBaseListener.DEFAULT_EXCAHNGE_NAME,
                    FaceBaseListener.ADD_HIK_FACE_EVENT,
                    faceBaseListener); // 添加人脸
            DateUtil.await(); //等待三秒
            // 开启授权
            publisher.publishEvent(new CreateInteriorAuthorizationEvent(this, event.getStaffId()));

        }
        // 默认1 ic卡 无联动其他卡类型
        rabbitTemplate.convertAndSend(
                CardBaseListener.DEFAULT_EXCAHNGE_NAME,
                CardBaseListener.ADD_HIK_CARD_EVENT,
                new CardInfoTransmit(event.getAccessCode(), event.getStaffId().toString(), "1"));

    }

    @EventListener
    public void PudCertifyLinkageHiKEvent(PudCertifySyncEvent event) {
        StaffInfoFine staff = StaffServer.getStaffById(event.getStaffId());
        if (staff == null) {
            return;
        }
        Boolean orgHik = serviceFeign.isOrgHik(staff.getOrganizationId());
        if (!orgHik) {
            DeptNameInfo deptNameInfo = staff.getDeptNames().get(staff.getDeptNames().size() - 1);
            OrganizationInformation information = new OrganizationInformation();
            information.setFldName(deptNameInfo.getDeptName());
            information.setId(staff.getOrganizationId());
            information.setParentId(-1);
            rabbitTemplate.convertAndSend(
                    OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME,
                    OrganizationBaseListener.CREATE_ORGANIZATION_EVENT,
                    information);
            DateUtil.await();
        }
        // 修改海康
        StaffInformation information1 = new StaffInformation();
        information1.setId(event.getStaffId());
        information1.setFldName(staff.getFldName());
        information1.setOrganizationId(staff.getOrganizationId());
        information1.setMobile(staff.getMobile());
        information1.setEmployeeNo(staff.getEmployeeNo());
        information1.setDeptNames(staff.getDeptNames());
        rabbitTemplate.convertAndSend(
                OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME,
                OrganizationBaseListener.UPDATE_STAFF_EVENT,
                information1);
        // 修改汉王 从新授权
        updateHangWang(event);
        if (event.getFaceImg() != null) {
            FaceInfoTransmit faceBaseListener = new FaceInfoTransmit();
            faceBaseListener.setStaffId(event.getStaffId());
            faceBaseListener.setFaceImg(event.getFaceImg());
            rabbitTemplate.convertAndSend(
                    FaceBaseListener.DEFAULT_EXCAHNGE_NAME,
                    FaceBaseListener.PUT_HIK_FACE_EVENT,
                    faceBaseListener);
        }
    }

    private void updateHangWang(PudCertifySyncEvent event) {
        List<String> list = mapper.selectListByType(HANWANG_TYPE_CODE);
        for (String s : list) {
            DeleteAuthorizationCommand command = new DeleteAuthorizationCommand();
            DoorInfoPo device_code = mapper.selectOne(new QueryWrapper<DoorInfoPo>().eq("device_code", s));
            command.setDeviceId(device_code.getId());
            command.setStaffId(event.getStaffId());
            doorInfoService.clearAuthorization(command);
        }
        DateUtil.await();
        AuthorizationCommand authorizationCommand = new AuthorizationCommand();
        authorizationCommand.setStaffIds(ImmutableList.of(event.getStaffId()));
        authorizationCommand.setFacilityCodes(mapper.selectListByType(HANWANG_TYPE_CODE));
        doorInfoService.Authorization(authorizationCommand, "123");
    }


    /**
     * 删错逻辑
     *
     * @param event
     */
    @EventListener
    public void DelCertifySyncEvent(DelCertifySyncEvent event) {
        StaffInfoFine staff = StaffServer.getStaffById(event.getStaffId());
        if (staff == null) {
            return;
        }
        publisher.publishEvent(new ClearInteriorAuthorizationEvent(this, event.getStaffId()));
        if (event.getFaceImg() != null) {
            FaceInfoTransmit faceBaseListener = new FaceInfoTransmit();
            faceBaseListener.setStaffId(event.getStaffId());
            rabbitTemplate.convertAndSend(
                    FaceBaseListener.DEFAULT_EXCAHNGE_NAME,
                    FaceBaseListener.DEL_HIK_FACE_EVENT,
                    faceBaseListener);
        }
        if (event.getAccessCode() != null) {
            // 默认ic卡 无联动其他卡类型
            rabbitTemplate.convertAndSend(CardBaseListener.DEFAULT_EXCAHNGE_NAME,
                    CardBaseListener.DEL_HIK_CARD_EVENT,
                    new CardInfoTransmit(event.getAccessCode(), event.getStaffId().toString(), "1"));
        }

    }
}
