package com.bjxczy.onepark.staffvoucher.pojo.vo;


import com.bjxczy.onepark.staffvoucher.entity.StaffVoucher;
import lombok.Data;

/**
 * 凭证管理列表展示
 */
@Data
public class StaffVoucherVO extends StaffVoucher {



}
