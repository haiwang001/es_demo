package com.bjxczy.onepark.common.event;


import com.bjxczy.core.rabbitmq.event.door.HikAuthorityPayload;
import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/*
 *@Author wlw
 *@Date 2023/7/4 16:26
 */
@Getter
@ToString
public class ClearHikDoorAuthorizationEvent extends ApplicationEvent {
    private static final long serialVersionUID = -5025514838587065627L;
    private HikAuthorityPayload payload;
    public ClearHikDoorAuthorizationEvent(Object source, HikAuthorityPayload payload) {
        super(source);
        this.payload = payload;
    }
}
