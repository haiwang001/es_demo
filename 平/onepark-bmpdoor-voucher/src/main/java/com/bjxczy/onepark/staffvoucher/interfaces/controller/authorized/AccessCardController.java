package com.bjxczy.onepark.staffvoucher.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.staffvoucher.pojo.dto.AccessCardDto;
import com.bjxczy.onepark.staffvoucher.pojo.dto.command.CreateAccessCommand;
import com.bjxczy.onepark.staffvoucher.pojo.dto.command.RemoveAccessCommand;
import com.bjxczy.onepark.staffvoucher.service.AccessCardService;
import com.bjxczy.onepark.staffvoucher.service.AccessQueryService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * 员工凭证管理
 */
@ApiResourceController
@RequestMapping("/access")
@Log4j2
public class AccessCardController {

    @Resource
    private AccessQueryService accessQueryService;

    @Resource
    private AccessCardService accessCardService;

    /**
     * 员工门禁卡管理查询,  页面主动查询<员工卡管理>
     * @return 员工卡信息列表
     */
    @GetMapping(value = "/card/list", name = "门禁卡管理查询")
    public R<?> cardList(AccessCardDto dto) {
        return accessQueryService.accessCardLiat(dto);
    }


    /**
     * 新增门禁卡；  页面新增按钮
     * @return 是否新增成功
     */
    @PostMapping(value = "/card/add", name = "新增门禁卡")
    public R<?> cardAdd(@RequestBody CreateAccessCommand command) {
        log.info("------------进入新增门禁卡---------------------------------------------------");
        return R.success(accessCardService.createCard(command));
    }

    /**
     * 删除门禁
     */
    @DeleteMapping(value = "/card/del/{id}", name = "删除门禁卡")
    public void cardDel(@PathVariable("id") Integer id) {
        log.info("------------进入删除门禁卡---------------------------------------------------");
        accessCardService.delAccessCard(new RemoveAccessCommand(id));
    }

    /**
     * 导出门禁卡信息
     * @param fileName 导出文件名称
     * @param dto      导出条件
     */
    @GetMapping(value = "/card/export", name = "员工门禁卡信息导出")
    public void exportExcel(HttpServletResponse response, String fileName, AccessCardDto dto) {
        Boolean isOk = accessQueryService.exportExcel(response, fileName, dto);
        log.info("------------导出" + fileName + "结果为" + isOk + "------------------------");
    }

}
