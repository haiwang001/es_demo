package com.bjxczy.onepark.staffvoucher.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.onepark.common.client.BmpQueryStaffServer;
import com.bjxczy.onepark.common.constant.VoucherSysConst;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.utils.ExcelUtils;
import com.bjxczy.onepark.common.utils.PageUtils;
import com.bjxczy.onepark.staffvoucher.entity.AccessCard;
import com.bjxczy.onepark.staffvoucher.entity.StaffVoucher;
import com.bjxczy.onepark.staffvoucher.event.AddCertifySyncEvent;
import com.bjxczy.onepark.staffvoucher.event.DelCertifyEvent;
import com.bjxczy.onepark.staffvoucher.event.DelCertifySyncEvent;
import com.bjxczy.onepark.staffvoucher.event.PudCertifySyncEvent;
import com.bjxczy.onepark.staffvoucher.mapper.AccessCardMapper;
import com.bjxczy.onepark.staffvoucher.mapper.StaffVoucherBuildingMapper;
import com.bjxczy.onepark.staffvoucher.mapper.StaffVoucherMapper;
import com.bjxczy.onepark.staffvoucher.pojo.dto.QueryStaffCertPageDTO;
import com.bjxczy.onepark.staffvoucher.pojo.vo.AuthorizationGuardVO;
import com.bjxczy.onepark.staffvoucher.pojo.vo.StaffBuildingVo;
import com.bjxczy.onepark.staffvoucher.pojo.vo.StaffVoucherVO;
import com.bjxczy.onepark.staffvoucher.pojo.vo.TreeNodeVo;
import com.bjxczy.onepark.staffvoucher.service.StaffVoucherService;
import com.google.common.collect.ImmutableList;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Log4j2
public class StaffVoucherServiceImpl implements StaffVoucherService {

    @Resource
    private StaffVoucherMapper staffVoucherMapper;

    @Resource
    private StaffVoucherBuildingMapper staffVoucherBuildingMapper;

    @Resource
    private AccessCardMapper accessCardMapper;
    @Autowired
    private ApplicationEventPublisher publisher;

    @Override
    public Object findStaffPermission(String staffId) {
        List<AuthorizationGuardVO> list = staffVoucherMapper.findStaffPermission(staffId);
        list.forEach(el -> {
            if (el.getSpaceId() != null) {//三级节点
                StaffBuildingVo or = staffVoucherBuildingMapper.getBuildingFloor(el.getSpaceId());
                el.setAreaName(or.getAreaName());
                el.setBuildingName(or.getBuildingName());
                el.setSpaceName(or.getLayerName());
            } else {
                if (el.getBuildingId() != null) {//二级节点
                    StaffBuildingVo or = staffVoucherBuildingMapper.getBuildingFloor(el.getBuildingId());
                    el.setBuildingName(or.getBuildingName());
                    el.setSpaceName(or.getLayerName());
                } else {
                    if (el.getAreaId() != null) {//一级节点
                        StaffBuildingVo or = staffVoucherBuildingMapper.getBuildingFloor(el.getAreaId());
                        el.setSpaceName(or.getLayerName());
                    }
                }
            }
        });
        // 先区域分组
        Map<String, List<AuthorizationGuardVO>> map = list.stream().collect(Collectors.groupingBy(AuthorizationGuardVO::getAreaName));
        ArrayList<TreeNodeVo> root = new ArrayList<>();
        map.forEach((k, v) -> {// 一级节点封装
            TreeNodeVo node = new TreeNodeVo();
            node.setName(k);
            v.forEach(el -> {
                if (el.getBuildingId() == null) {
                    node.setFacilityName(el.getName());
                    node.setNoChildren(false);
                }
            });
            if (!root.contains(node)) {
                root.add(node);
            }
        });
        List<AuthorizationGuardVO> del = new ArrayList<>();
        list.forEach(el -> {
            if (el.getBuildingId() == null) {
                del.add(el);
            }
        });
        list.removeAll(del); // 清除可能不存在的子集节点 否则 无法分组

        Map<String, List<AuthorizationGuardVO>> collect = list.stream().collect(Collectors.groupingBy(AuthorizationGuardVO::getBuildingName));
        root.forEach(j -> {
            if (j.isNoChildren()) {
                ArrayList<TreeNodeVo> tree = new ArrayList<>();
                collect.forEach((k, v) -> {
                    v.forEach(ve -> {
                        if (ve.getAreaName().equals(j.getName())) {
                            TreeNodeVo node = new TreeNodeVo();
                            node.setName(k);
                            if (ve.getSpaceId() == null) {
                                node.setFacilityName(ve.getName());
                                node.setNoChildren(true);
                            }
                            if (!tree.contains(node)) {
                                tree.add(node);
                            }
                        }
                    });
                });
                j.setChildren(tree);
            }
        });
        List<AuthorizationGuardVO> del2 = new ArrayList<>();
        list.forEach(el -> {
            if (el.getSpaceId() == null) {
                del2.add(el);
            }
        });
        list.removeAll(del2); // 清除可能不存在的子集节点 否则 无法分组

        Map<String, List<AuthorizationGuardVO>> collect1 = list.stream().collect(Collectors.groupingBy(AuthorizationGuardVO::getSpaceName));
        root.forEach(el -> {
            if (el.getChildren() != null) {
                el.getChildren().forEach(zel -> {
                    ArrayList<TreeNodeVo> tree = new ArrayList<>();
                    if (zel.isNoChildren()) {
                        collect1.forEach((k, v) -> {
                            v.forEach(vel -> {
                                if (vel.getAreaName().equals(el.getName()) && vel.getBuildingName().equals(zel.getName())) {
                                    TreeNodeVo node = new TreeNodeVo();
                                    node.setName(k);
                                    node.setNoChildren(true);
                                    node.setChildren(ImmutableList.of(new TreeNodeVo("设备名称 : " + vel.getName())));
                                    if (!tree.contains(node)) {
                                        tree.add(node);
                                    }
                                }
                            });
                        });
                        zel.setChildren(tree);
                    }
                });
            }
        });
        for (TreeNodeVo vo : root) {
            for (TreeNodeVo child : vo.getChildren()) {
                List<TreeNodeVo> lnode = new ArrayList<>();
                for (int i = 0; i < child.getChildren().size(); i++) {
                    Map<String, List<TreeNodeVo>> collect2 = child.getChildren().get(i).getChildren().stream().collect(Collectors.groupingBy(TreeNodeVo::getName));
                    for (String s : collect2.keySet()) {
                        lnode.addAll(collect2.get(s));
                    }
                    child.getChildren().get(i).setChildren(lnode);
                }
                Map<String, List<TreeNodeVo>> collect2 = child.getChildren().stream().collect(Collectors.groupingBy(TreeNodeVo::getName));
                child.getChildren().clear();
                for (String s : collect2.keySet()) {
                    TreeNodeVo treeNodeVo = collect2.get(s).get(0);
                    child.setChildren(ImmutableList.of(treeNodeVo));
                }
            }
        }
        return root;
    }

    @Override
    public StaffVoucher selectStaffVoucherByStaffId(Integer staffId) {
        return staffVoucherMapper.selectByStaffId(staffId);
    }


    @Override
    public void listExport(HttpServletResponse response, QueryStaffCertPageDTO dto) {
        List<StaffVoucherVO> list = staffVoucherMapper.staffVoucherPageList(dto);
        try {
            ExcelUtils.export(response,
                    "员工凭证列表导出",
                    "员工凭证列表导出",
                    list, StaffVoucherVO.class);
        } catch (IOException e) {
            throw new ResultException("员工凭证列表导出失败！");
        }
    }


    @Override
    public Object pageList(QueryStaffCertPageDTO dto) {
        List<StaffVoucherVO> list = staffVoucherMapper.staffVoucherPageList(dto);
        if (dto.getDeptId() != null) {
            List<StaffVoucherVO> list1 = new ArrayList<>(list.size());
            list.forEach(vel -> {
                accessCardMapper.selectByDepartmentId(dto.getDeptId()).forEach(id -> {
                    if (vel.getDeptId() != null) {
                        if (vel.getDeptId().equals(id)) {
                            list1.add(vel);
                        }
                    }
                });
            });
            list.clear();
            list = list1;
        }
        return PageUtils.Page(dto, list);
    }

    /**
     * 本地删除门禁卡凭证方法
     *
     * @param event
     * @return
     */
    @Override
    public int deleteCertify(DelCertifyEvent event) {
        publisher.publishEvent(new DelCertifySyncEvent(this, event.getAccessCard().getStaffId(), null, event.getAccessCard().getAccessCode()));
        Integer staffId = event.getAccessCard().getStaffId();
        StaffVoucher staffVoucher = staffVoucherMapper.selectByStaffId(staffId);
        if (staffVoucher != null) {
            if (staffVoucher.getVoucherCnt() != null) {
                if (staffVoucher.getVoucherCnt() > 0) {
                    Integer voucherCnt = staffVoucher.getVoucherCnt() - 1;
                    Integer integer = staffVoucherMapper.updateAccessCodeByStaffId(staffId, voucherCnt, null);
                    if (integer == 1) {
                        return integer;
                    }
                }
            }
        }
        return -1;
    }


    /**
     * 删除人脸凭证方法
     *
     * @param map 人脸信息
     * @return
     * @throws Exception
     */
    @Override
    public int deleteCertify(FaceInfoTransmit map) throws Exception {
        Integer staffId = map.getStaffId();
        StaffVoucher staffVoucher = staffVoucherMapper.selectByStaffId(staffId);
        if (staffVoucher != null) {
            if (staffVoucher.getVoucherCnt() != null) {
                if (staffVoucher.getVoucherCnt() > 0) {
                    Integer voucherNum = staffVoucher.getVoucherCnt() - 1;
                    Integer integer = staffVoucherMapper.deleteCertifyImgStaffId(staffId, voucherNum, null);
                    if (integer == 1) {
                        return integer;
                    }
                }
            }
        }
        return -1;
    }


    /**
     * 订阅人脸服务，修改人脸凭证信息
     *
     * @param map 人脸信息
     */
    @Override
    public int updateCertify(FaceInfoTransmit map) {
        StaffVoucher staffVoucher = new StaffVoucher();
        staffVoucher.setStaffId(map.getStaffId());
        staffVoucher.setFaceImg(map.getFaceImg());
        staffVoucher.setDeptId(map.getDeptId());
        staffVoucher.setDeptName(map.getDeptName());
        staffVoucher.setUpdateTime(new Date());
        List<StaffVoucher> staffid = staffVoucherMapper.selectList(new QueryWrapper<StaffVoucher>().eq("staff_id", map.getStaffId()));
        if (staffid != null) {
            staffVoucher.setVoucherCnt(staffid.get(0).getVoucherCnt());
        }
        return staffVoucherMapper.updateFaceImg(staffVoucher);
    }


    @Autowired
    BmpQueryStaffServer QueryStaffServer;

    /**
     * 订阅人脸服务，添加人脸数据
     *
     * @param map
     * @throws Exception
     */
    @Override
    public void addCertify(FaceInfoTransmit map) throws Exception {
        StaffVoucher staffVoucher = addStaffVoucher(null, null, map, "2");
        // 数据库查询出数据信息
        StaffVoucher staffVoucherPo = staffVoucherMapper.selectByStaffId(staffVoucher.getStaffId());
        if (staffVoucherPo == null) {
            staffVoucher.setCreateTime(new Date());
            staffVoucher.setVoucherCnt(VoucherSysConst.VOUCHER_DEFAULT_NUMBER);
            int insert = staffVoucherMapper.insert(staffVoucher);
            if (insert == 1) {
                log.info("凭证新增成功" + staffVoucher.getStaffName());
            }
        }
        if (staffVoucherPo != null) {
            staffVoucherPo.setVoucherCnt(staffVoucherPo.getVoucherCnt() == null ? 0 : staffVoucherPo.getVoucherCnt());
            if (staffVoucherPo.getVoucherCnt() >= VoucherSysConst.VOUCHER_MAXIMUM_NUMBER) {
                log.error("系统内部出现问题 联系管理员 员工凭证添加记录超过设定的阈值 -- {}", VoucherSysConst.VOUCHER_MAXIMUM_NUMBER);
                throw new Exception("系统内部出现问题 联系管理员 添加记录超过设定的阈值 " + VoucherSysConst.VOUCHER_MAXIMUM_NUMBER);
            }
            staffVoucher.setUpdateTime(new Date());
            // 更新凭证数
            staffVoucher.setVoucherCnt(staffVoucherPo.getVoucherCnt() + 1);
            int i =  staffVoucherMapper.update(staffVoucher,new QueryWrapper<StaffVoucher>().eq("staff_id",staffVoucher.getStaffId()));
            if (i == 1) {
                log.info("凭证数变更 -- {}", staffVoucher.getStaffName());
            }
        }
    }

    /**
     * 通过内部添加门禁 增加记录
     *
     * @param accessCard
     * @param staff
     * @throws Exception
     */
    @Override
    public void addCertify(AccessCard accessCard, StaffInfoFine staff) throws Exception {
        StaffVoucher staffVoucher = addStaffVoucher(accessCard, staff, null, "1");
        publisher.publishEvent(new AddCertifySyncEvent(this, accessCard.getStaffId(), null, accessCard.getAccessCode()));
        // 数据库查询出数据信息
        StaffVoucher staffVoucherPo = staffVoucherMapper.selectByStaffId(staffVoucher.getStaffId());
        // 添加员工门禁卡凭证
        if (ObjectUtils.isEmpty(staffVoucherPo)) {
            staffVoucher.setCreateTime(new Date());
            staffVoucher.setVoucherCnt(VoucherSysConst.VOUCHER_DEFAULT_NUMBER);
            int insert = staffVoucherMapper.insert(staffVoucher);
            if (insert == 1) {
                log.info("凭证新增成功" + staffVoucher.getStaffName());
            }
        } else {
            // 员工凭证表里有凭证记录
            if (!ObjectUtils.isEmpty(staffVoucherPo.getVoucherCnt())) {
                if (staffVoucherPo.getVoucherCnt() >= VoucherSysConst.VOUCHER_MAXIMUM_NUMBER) {
                    log.error("系统内部出现问题 联系管理员 员工凭证添加记录超过设定的阈值 -- {}", VoucherSysConst.VOUCHER_MAXIMUM_NUMBER);
                    throw new Exception("系统内部出现问题 联系管理员 添加记录超过设定的阈值 " + VoucherSysConst.VOUCHER_MAXIMUM_NUMBER);
                }
                staffVoucher.setUpdateTime(new Date());
                // 更新凭证数
                staffVoucher.setVoucherCnt(staffVoucherPo.getVoucherCnt() + 1);
                int i =  staffVoucherMapper.update(staffVoucher,new QueryWrapper<StaffVoucher>().eq("staff_id",staffVoucher.getStaffId()));
               // staffVoucherMapper.updateAccessCode(staffVoucher);
                if (i == 1) {
                    log.info("凭证数变更 -- {}", staffVoucher.getStaffName());
                }
            }
            log.info("----------------------------------通过内部添加门禁" + staffVoucher);
        }
        publisher.publishEvent(new PudCertifySyncEvent(this, accessCard.getStaffId(), null, accessCard.getAccessCode()));
    }

    /**
     * @param accessCard 员工卡实体类
     * @param staff      员工实体类
     * @param map        远程封装，人脸信息
     * @param msg        1：门禁卡；  2： 人脸信息
     * @return
     */
    private StaffVoucher addStaffVoucher(AccessCard accessCard, StaffInfoFine staff, FaceInfoTransmit map, String msg) {
        StaffInformation staff1 = null;
        StaffVoucher staffVoucher = null;
        if (map != null) {
            staff1 = QueryStaffServer.findById(map.getStaffId());
        }
        if (accessCard != null) {
            staff1 = QueryStaffServer.findById(accessCard.getStaffId());
        }
        System.err.println(staff1);
        if ("1".equals(msg)) {
            staffVoucher = new StaffVoucher();
            staffVoucher.setRankId(staff1.getRankId());
            staffVoucher.setRankName(staff1.getRankName());
            staffVoucher.setStaffId(accessCard.getStaffId());
            // 部门ID
            staffVoucher.setDeptId(accessCard.getDeptId());
            // 门禁卡号
            staffVoucher.setAccessCode(accessCard.getAccessCode());
            //部门名称
            staffVoucher.setDeptName(accessCard.getDeptName());
            //员工编号
            staffVoucher.setEmployeeNo(accessCard.getEmployeeNo());
            //证件号
            staffVoucher.setIdentifyCard(staff1.getIdentityCard());
            //员工名称
            staffVoucher.setStaffName(staff1.getFldName());
            staffVoucher.setMobile(staff1.getMobile());
            staffVoucher.setComeFrom(1);//默认内部员工
            staffVoucher.setDelFlag(0);
        }
        if ("2".equals(msg)) {
            staffVoucher = new StaffVoucher();
            staffVoucher.setRankId(staff1.getRankId());
            staffVoucher.setRankName(staff1.getRankName());
            staffVoucher.setStaffId(map.getStaffId());
            staffVoucher.setDeptId(map.getDeptId());
            staffVoucher.setFaceImg(map.getFaceImg());
            staffVoucher.setDeptName(map.getDeptName());//部门名称
            staffVoucher.setEmployeeNo(map.getEmployeeNo());//员工编号
            staffVoucher.setIdentifyCard(map.getIdentityCard());//证件号
            staffVoucher.setStaffName(map.getFldName());//员工名称
            staffVoucher.setMobile(map.getMobile());
            staffVoucher.setComeFrom(1);//默认内部员工
            staffVoucher.setDelFlag(0);
        }
        return staffVoucher;
    }
}
