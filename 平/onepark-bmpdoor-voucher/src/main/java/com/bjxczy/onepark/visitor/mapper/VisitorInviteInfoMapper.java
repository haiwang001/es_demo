package com.bjxczy.onepark.visitor.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.common.model.visitor.VisitorDtoInformation;
import com.bjxczy.onepark.common.model.visitor.VisitorInformation;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorInfoPo;
import com.bjxczy.onepark.visitor.pojo.dto.VisitorInviteDto;
import com.bjxczy.onepark.visitor.pojo.vo.VisitorInviteExcelVo;
import com.bjxczy.onepark.visitor.pojo.vo.VisitorInvitePageListVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/20 12:34
 */
@Mapper
public interface VisitorInviteInfoMapper extends BaseMapper<VisitorInfoPo> {
    String VisitorInvitePageListSql = "select a.id, a.visitor_name,a.visitor_photo, a.visitor_id_card, a.visitor_mobile,\n" +
            "a.visit_time,a.leave_time,a.visitor_remark,a.operator,a.receptionist_dept_name,\n" +
            "       d1.label arriveType,\n" +
            "       d.label arriveStatus,\n" +
            "       s.fld_name receptionistName,\n" +
            "       s.mobile receptionistMobile,\n" +
            "       b.name spaceName\n" +
            "from v_visitor_invite_info a\n" +
            "         left join voucher_dict d on a.arrive_status = d.value and d.group_code = 'arrive_status'\n" +
            "         left join voucher_dict d1 on a.arrive_type = d1.value and d1.group_code = 'arrive_type'\n" +
            "         left join organization.tbl_staff s on a.receptionist_id = s.id\n" +
            "         left join authority.tbl_area b on  a.area_id = b.id\n" +
            "         left join authority.tbl_building b1 on  a.tenant_id = b1.id\n" +
            "         left join authority.tbl_building_floor b2 on  a.space_id = b2.id\n" +
            "  where a.del_flag = 0\n" +
            "  and if(#{dto.visitorName}!='', a.visitor_name like CONCAT('%', #{dto.visitorName}, '%'), true)\n" +
            "  and if(#{dto.visitorMobile}!='', a.visitor_mobile like CONCAT('%', #{dto.visitorMobile}, '%'), true)\n" +
            "  and if(#{dto.visitorIdCard}!='', a.visitor_id_card like CONCAT('%', #{dto.visitorIdCard}, '%'), true)\n" +
            "  and if(#{dto.arriveStatus}!='', a.arrive_status like CONCAT('', #{dto.arriveStatus}, ''), true)\n" +
            "  and if(#{dto.visitTime}!='', DATE_FORMAT(a.visit_time, '%Y-%m-%d %H:%i:%S') >= #{dto.visitTime}, true)\n" +
            "  and if(#{dto.leaveTime}!='', #{dto.leaveTime} >= DATE_FORMAT(a.leave_time, '%Y-%m-%d %H:%i:%S'), true)\n" +
            "  group by a.id\n" +
            "order by a.create_time desc";

    @Select(VisitorInvitePageListSql)
    IPage<VisitorInvitePageListVo> VisitorInvitePageList(Page<Object> objectPage, @Param("dto") VisitorInviteDto dto);

    @Select(VisitorInvitePageListSql)
    List<VisitorInviteExcelVo> VisitorExportExcelList(@Param("dto") VisitorInviteDto dto);

    @Select("select a.visitor_name,a.visitor_mobile,a.receptionist_id,b.fld_name staffName,a.receptionist_dept_name,a.visit_time,a.leave_time\n" +
            "from v_visitor_invite_info a left join organization.tbl_staff b on a.receptionist_id=b.id\n" +
            "where a.del_flag = 0\n" +
            "  and (a.arrive_status != 4 and a.arrive_status != 5)\n" +
            "  and a.visitor_mobile = #{mobile} group by  visitor_mobile")
    VisitorInformation selectVisitorByMobile(@Param("mobile") String mobile);


    @Select("select a.order_id" +
            "from v_visitor_invite_info a\n" +
            "where a.del_flag = 0\n" +
            "  and (a.arrive_status != 4 and a.arrive_status != 5)\n" +
            "  and a.visitor_mobile = #{mobile} group by  visitor_mobile")
    String selectByMobile(@Param("mobile") String mobile);

    @Select("select count(id)\n" +
            "from v_visitor_invite_info\n" +
            "where del_flag = 0\n" +
            "  and visitor_name = #{po.visitorName}\n" +
            "  and visitor_mobile = #{po.visitorMobile}\n" +
            "  and visitor_id_card = #{po.visitorIdCard}\n" +
            "  and (arrive_status !=4 or arrive_status !=5)")
    int selectByName(@Param("po") VisitorInfoPo po);

    @Select("select count(id)\n" +
            "from v_visitor_invite_info\n" +
            "where del_flag = 0\n" +
            "  and visitor_name != #{po.visitorName}\n" +
            "  and (visitor_mobile = #{po.visitorMobile}\n" +
            "  or visitor_id_card = #{po.visitorIdCard})\n" +
            "  and (arrive_status =1 " +
            "  or arrive_status =2 " +
            "  or arrive_status =3)")
    int selectByMobileAndIdCard(@Param("po") VisitorInfoPo po);

    @Select("select v.visitor_name,\n" +
            "       v.visitor_mobile,\n" +
            "       v.visitor_work,\n" +
            "       v.receptionist_id,\n" +
            "       fld_name staffName,\n" +
            "       v.receptionist_dept_name,\n" +
            "       v.arrive_status,\n" +
            "       v.leave_time,\n" +
            "       v.visit_time\n" +
            "from v_visitor_invite_info v\n" +
            "         left join organization.tbl_staff on tbl_staff.id = v.receptionist_id\n" +
            "where v.del_flag = 0\n" +
            "  and v.arrive_status != 5\n" +
            "  and if(#{id}!='',v.area_id = #{id},true)\n" +
            "  and #{day} > v.visit_time\n" +
            "  and #{day} < v.leave_time\n")
    List<VisitorInformation> selectVisitorByVisitToday(@Param("day") String day,@Param("id") String id);

    @Select("select visitor_name name, visitor_type type, order_id id\n" +
            "from visitor_info\n" +
            "where del_flag = 0\n" +
            "  and order_id is not null\n" +
            "        group by order_id")
    List<VisitorDtoInformation> getVisitorAll();
}
