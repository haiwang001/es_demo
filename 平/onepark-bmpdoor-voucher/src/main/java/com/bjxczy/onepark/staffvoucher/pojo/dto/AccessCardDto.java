package com.bjxczy.onepark.staffvoucher.pojo.dto;

import lombok.Data;

import java.util.Date;

@Data
public class AccessCardDto extends BasePageDTO {
    /**
     * 合并查询字段
     */
    private String merge;
    /**
     * 员工门禁表ID
     */
    private Integer id;
    /**
     * 员工ID
     */
    private String staffId;
    /**
     * 员工编号
     */
    private String employeeNo;
    /**
     * 员工姓名
     */
    private String staffName;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 部门ID
     */
    // private Integer departmentId;
    private Integer deptId;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 职级ID
     */
    private Integer rankId;
    /**
     * 人员类型： 对应字典表voucher_dict中字段 group_code='comeFrom'
     */
    private Integer comeFrom;

    /**
     * 职级名称
     */
    private String rankName;

    /**
     * 租户ID(局址)
     */
    // private String tenantId;
    /**
     * 门禁卡号
     */
    private String accessCode;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     *更新时间
     */
    private Date updateTime;
    /**
     * 操作人  新加
     */
    private String operator;

    /**
     * 标记删除   新加
     */
    private Integer delFlag;

    /**
     * 门禁卡类型： 该字段已删除
     */
    // private Integer accessCardType;

    /**
     * 卡号和accessCode意义相同
     */
    // private String accessCard;

    /**
     * 描述： 该字段已删除
     */
    // private String remark;



}
