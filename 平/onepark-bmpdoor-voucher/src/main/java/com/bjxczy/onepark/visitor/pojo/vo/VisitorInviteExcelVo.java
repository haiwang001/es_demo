package com.bjxczy.onepark.visitor.pojo.vo;


import cn.afterturn.easypoi.excel.annotation.Excel;
import com.bjxczy.core.web.privacy.handler.IdCardMaskingHandler;
import com.bjxczy.core.web.privacy.handler.MaskingField;
import com.bjxczy.core.web.privacy.handler.MobileMaskingHandler;
import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/20 17:04
 */
@Data
public class VisitorInviteExcelVo {
    private String id;
    private String visitorPhoto;

    @Excel(name = "访客姓名", width = 18, replace = {"_null"})
    private String visitorName;

    @MaskingField(typeHandler = IdCardMaskingHandler.class)
    @Excel(name = "访客证件号", width = 25, replace = {"_null"})
    private String visitorIdCard;

    @MaskingField(typeHandler = MobileMaskingHandler.class)
    @Excel(name = "访客手机号", width = 18, replace = {"_null"})
    private String visitorMobile;
    @Excel(name = "接访人部门", width = 18, replace = {"_null"})
    private String receptionistDeptName;
    @Excel(name = "接访人", width = 18, replace = {"_null"})
    private String receptionistName;

    @MaskingField(typeHandler = MobileMaskingHandler.class)
    @Excel(name = "接访人手机", width = 18, replace = {"_null"})
    private String receptionistMobile;
    @Excel(name = "到访园区", width = 30, replace = {"_null"})
    private String spaceName;
    @Excel(name = "来访时间", width = 25, replace = {"_null"})
    private String visitTime;
    @Excel(name = "离开时间", width = 25, replace = {"_null"})
    private String leaveTime;
    @Excel(name = "到访事由", width = 15, replace = {"_null"})
    private String arriveType;
    @Excel(name = "到访详情说明", width = 23, replace = {"_null"})
    private String visitorRemark;
    @Excel(name = "到访状态", width = 15, replace = {"_null"})
    private String arriveStatus;
    @Excel(name = "操作人", width = 15, replace = {"_null"})
    private String operator;
}
