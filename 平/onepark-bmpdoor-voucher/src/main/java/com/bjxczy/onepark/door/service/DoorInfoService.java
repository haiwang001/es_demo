package com.bjxczy.onepark.door.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.door.command.*;
import com.bjxczy.onepark.door.pojo.dto.DoorPageDto;
import com.bjxczy.onepark.door.pojo.dto.LookPageDto;
import com.bjxczy.onepark.door.pojo.vo.DoorOverviewVo;
import com.bjxczy.onepark.door.pojo.vo.DoorPageVo;
import com.bjxczy.onepark.door.pojo.vo.LookPageVo;

import javax.servlet.http.HttpServletResponse;

public interface DoorInfoService {
    IPage<DoorPageVo> pageList(DoorPageDto dto);

    Integer authorization(AuthorizationCommand com, UserInformation sysUser);

    Integer clearAuthorization(DeleteAuthorizationCommand com);

    void doorCreateInteriorAuthorization(Integer staffId);

    void doorClearInteriorAuthorization(Integer staffId);

    Integer putDoorDirection(PutDoorDirectionCommand com);

    Integer setVisitPermission(PutDoorVisitorCommand com);

    Integer setStaffPermission(PutDoorStaffCommand com);

    IPage<LookPageVo> LookPage(LookPageDto dto);

    void lookExport(HttpServletResponse response, LookPageDto dto);

    DoorOverviewVo overview(String id);
}
