package com.bjxczy.onepark.staffvoucher.event;


import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEvent;

/*
 *@Author wlw
 *@Date 2023/7/13 13:30  修改凭证联动海康
 */
@Log4j2
@Data
public class PudCertifySyncEvent extends ApplicationEvent {
    private static final long serialVersionUID = -2134775381442624406L;
    private Integer staffId;
    private String faceImg;
    private String accessCode;

    public PudCertifySyncEvent(Object source, Integer staffId, String faceImg, String accessCode) {
        super(source);
        this.staffId = staffId;
        this.faceImg = faceImg;
        this.accessCode = accessCode;
    }
}
