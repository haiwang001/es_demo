package com.bjxczy.onepark.staffvoucher.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.staffvoucher.pojo.vo.StaffBuildingVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface StaffVoucherBuildingMapper extends BaseMapper<StaffBuildingVo> {
    @Select("select  a.id areaId,a.name areaName,b.id buildingId,b.name buildingName,c.id layerId,c.name layerName  from\n" +
            "        authority.tbl_building_floor c , authority.tbl_building b ,authority.tbl_area a\n" +
            " where c.tenant_id = b.id and b.area_id = a.id and c.id = #{addressId}")
    StaffBuildingVo getBuildingFloor(@Param("addressId") String addressId);

    @Select("select  a.id areaId,a.name areaName,b.id buildingId,b.name buildingName from\n" +
            "        authority.tbl_building b ,authority.tbl_area a\n" +
            " where  b.area_id = a.id and b.id = #{addressId}")
    StaffBuildingVo getBuilding(@Param("addressId") String spaceId);

    @Select("select  a.id areaId,a.name areaName  from\n" +
            "        authority.tbl_area a  where  a.id = #{addressId}")
    StaffBuildingVo getArea(@Param("addressId") String spaceId);


}
