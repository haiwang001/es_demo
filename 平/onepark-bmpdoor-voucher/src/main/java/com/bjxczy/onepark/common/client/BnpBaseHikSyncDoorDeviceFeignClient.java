package com.bjxczy.onepark.common.client;

import com.bjxczy.core.feign.client.door.BaseHikSyncDoorDeviceFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("hikvision-adapter")
public interface BnpBaseHikSyncDoorDeviceFeignClient extends BaseHikSyncDoorDeviceFeignClient {
}
