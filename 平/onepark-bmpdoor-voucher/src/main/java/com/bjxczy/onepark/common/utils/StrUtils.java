package com.bjxczy.onepark.common.utils;


import cn.hutool.core.util.IdcardUtil;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;

import java.util.List;
import java.util.regex.Pattern;

/*
 *@ClassName StringUtils
 *@Description TODO
 *@Author 温良伟
 *@Date 2023/1/19 15:16
 *@Version 1.0
 */
public class StrUtils {


    /**
     * 根据身份证号判断性别   只支持 15 和 18位 身份证号
     *
     * @param idNumber 身份证
     * @return -1识别失败  1 男  2 女
     */
    public static Integer getGenderByIdCard(String idNumber) throws IllegalArgumentException {
        if (IdcardUtil.isValidCard(idNumber)) {
            int gender = 0;
            if (idNumber.length() == 18) {
                //如果身份证号18位，取身份证号倒数第二位
                char c = idNumber.charAt(idNumber.length() - 2);
                gender = Integer.parseInt(String.valueOf(c));
            } else {
                //如果身份证号15位，取身份证号最后一位
                char c = idNumber.charAt(idNumber.length() - 1);
                gender = Integer.parseInt(String.valueOf(c));
            }
            if (gender % 2 == 1) {
                return 1;
            } else {
                return 2;
            }
        }
        return 1;//默认为男
    }

    public static boolean isIDCard(String idCard) {
        String regex = "\\d{17}[\\dXx]|\\d{15}";
        return Pattern.matches(regex, idCard);
    }

    /**
     * 计算身份证号码校验码是否正确
     */
    public static boolean isValidCheckDigit(String idCard) {
        if (!isIDCard(idCard)) {
            return false;
        }
        // 对于15位身份证号码，直接返回true
        if (idCard.length() == 15) {
            return true;
        }
        String[] factors = {"7", "9", "10", "5", "8", "4", "2", "1", "6", "3", "7", "9", "10", "5", "8", "4", "2"};
        String[] checkDigits = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};
        int sum = 0;
        for (int i = 0; i < 17; i++) {
            sum += (idCard.charAt(i) - '0') * Integer.parseInt(factors[i]);
        }
        int checkDigitIndex = sum % 11;
        String checkDigit = checkDigits[checkDigitIndex];
        return checkDigit.equalsIgnoreCase(idCard.substring(17));
    }

    /**
     * 验证身份证号码的有效性
     */
    public static boolean isValidIDCard(String idCard) {
        if (!isIDCard(idCard)) {
            return false;
        }
        return isValidCheckDigit(idCard);
    }

    public static void main(String[] args) {

        boolean validIDCard = isValidIDCard("522421195602195256");
        boolean validIDCard1 = isValidCheckDigit("522421195602195256");
        System.out.println(validIDCard);
        System.err.println(validIDCard1);



    }




    public static List<DeptNameInfo> DeptNameInfo(List<DeptNameInfo> list) {
        String str = "";
        for (int i = 0; i < list.size(); i++) {
            DeptNameInfo el = list.get(i);
            if (list.size() - 1 == i) {
                str = str + el.getDeptName();
                list.clear();
                DeptNameInfo deptNameInfo = new DeptNameInfo();
                deptNameInfo.setId(el.getId());
                deptNameInfo.setDeptName(str);
                list.add(deptNameInfo);
            } else {
                str = str + el.getDeptName() + "/";
            }
        }
        return list;
    }
}
