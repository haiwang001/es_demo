package com.bjxczy.onepark.common.event;


import com.bjxczy.core.rabbitmq.event.door.HikAuthorityPayload;
import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/*
 *@Author wlw
 *@Date 2023/7/4 16:26
 */
@Getter
@ToString
public class HikDoorAuthorizationEvent extends ApplicationEvent {
    private static final long serialVersionUID = 7832213816749731895L;
    private HikAuthorityPayload payload;
    public HikDoorAuthorizationEvent(Object source, HikAuthorityPayload payload) {
        super(source);
        this.payload = payload;
    }
}
