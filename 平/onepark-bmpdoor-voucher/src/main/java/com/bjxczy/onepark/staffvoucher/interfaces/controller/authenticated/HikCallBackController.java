package com.bjxczy.onepark.staffvoucher.interfaces.controller.authenticated;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.common.utils.StaffUtils;
import com.bjxczy.onepark.staffvoucher.service.EsRecordService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * @Author wlw
 * @Date 2023/4/4 15:49
 */
@ApiResourceController
@RequestMapping("/HikCallBack") //HikCallBack
@Log4j2
public class HikCallBackController {

    @Resource
    private EsRecordService esService;

    /**
     * 人脸上传 使用海康人脸评分接口
     *
     * @param file 参数文件
     * @return 返回文件存储地址
     */
    @PostMapping(value = "/upload", name = "人脸上传")
    public R<?> qualityJudge(@RequestParam MultipartFile file, HttpServletRequest request) throws Exception {
        String url = esService.fileUpload(file);
        return R.success(payload -> payload.set("url", url));
    }

    @PostMapping(value = "/base64String", name = "文件流转base64")
    public R<?> base64String(@RequestParam MultipartFile file) throws Exception {
        String url = esService.getBase64(file);
        return R.success(payload -> payload.set("url", url));
    }

    @GetMapping(value = "/urlToBase64", name = "文件路径转base64")
    public R<?> urlToBase64(String url) {
        return R.success(payload -> payload.set("base64", StaffUtils.getFileBase64String(url)));
    }



}
