package com.bjxczy.onepark.visitor.interfaces.listener.async;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.core.rabbitmq.rpc.RabbitAsyncComponent;
import com.bjxczy.core.rabbitmq.rpc.RabbitAsyncHandler;
import com.bjxczy.core.rabbitmq.supports.VisitorBaseListener;
import com.bjxczy.onepark.common.constant.enums.SyncStatusCode;
import com.bjxczy.onepark.common.constant.enums.VisitorStatusCode;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorInformation;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorInfoPo;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorSyncLogPo;
import com.bjxczy.onepark.visitor.mapper.VisitorInviteInfoMapper;
import com.bjxczy.onepark.visitor.mapper.VisitorSyncLogMapper;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/7/5 10:56
 */
@Component
@Log4j2
@RabbitAsyncComponent(
        value = "VisitorRabbitAsyncHandle", // 处理器KEY，同时作为Bean名称
        exchange = VisitorBaseListener.DEFAULT_EXCAHNGE_NAME, // 目的交换机（公共组件中定义）
        routingKey = VisitorBaseListener.CREATE_VISITOR_EVENT  // 目的路由（公共组件中定义）
)
public class VisitorRabbitAsyncHandle extends RabbitAsyncHandler {


    @Resource
    private VisitorInviteInfoMapper visitorInviteInfoMapper;
    @Resource
    private VisitorSyncLogMapper visitorSyncLogMapper;

    /**
     * 统一返回
     *
     * @param id      通过访客id 生成对应平台授权记录
     * @param message
     */
    @Override
    public void receive(Object id, Message message) {
        log.info("触发访客异步授权结果");
        CreateVisitorAndDoorVo visitor = (CreateVisitorAndDoorVo) this.messageConverter.fromMessage(message);
        List<VisitorSyncLogPo> logPos = visitorSyncLogMapper.selectList(new QueryWrapper<VisitorSyncLogPo>().eq("visitor_id", id).eq("platform_type", visitor.getType()));
        if (logPos.size() == 0) {
            if (visitor.getCode() == 0) {
                visitorInviteInfoMapper.updateById(new VisitorInfoPo(id.toString(), visitor.getMsg()));
                visitorSyncLogMapper.insert(new VisitorSyncLogPo(id.toString(), visitor.getType(), SyncStatusCode.SUCCESSFUL.getCode(), SyncStatusCode.SUCCESSFUL.getMsg()));
            } else {
                visitorInviteInfoMapper.updateById(new VisitorInfoPo(id.toString(), VisitorStatusCode.FINISH.getCode()));
                visitorSyncLogMapper.insert(new VisitorSyncLogPo(id.toString(), visitor.getType(), SyncStatusCode.ERROR.getCode(), visitor.getMsg()));
            }
        }
    }

}
