package com.bjxczy.onepark.common.constant;

public interface DeviceTypes {
	
	/**
	 * 汉王设备类型编码
	 */
	String HANWANG_TYPE_CODE = "KingOfHanFaceMachine";
	
	/**
	 * 海康设备类型编码
	 */
	String HIKVISION_TYPE_CODE = "HikFaceMachine";

}
