package com.bjxczy.onepark.staffvoucher.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/*
 *@Version 1.0
 * 节点树下的员工信息
 */
@Data
@TableName("staff_Node")
public class StaffNode implements Serializable {
    private static final long serialVersionUID = 1887059672105537760L;
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String groupNodeId;//分组节点
    private String employeeNo; //员工编号
    private String employeeId; //员工id  employee_id
    private String staffName; //员工名称
    private String mobile; //员工手机号
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 操作人
     */
    private String operator;
    /**
     * 创建时间  create_time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 0未删除1已删除  del_flag
     */
    @TableLogic(value = "0", delval = "1")
    private int delFlag;

}
