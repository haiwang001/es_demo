package com.bjxczy.onepark.common.event;

import java.util.List;

import org.springframework.context.ApplicationEvent;

import com.bjxczy.core.rabbitmq.event.door.AddStaffAuthDTO;
import com.bjxczy.core.rabbitmq.event.door.AuthorityDeviceDTO;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class UpdateAuthorityEvent extends ApplicationEvent {
    private static final long serialVersionUID = -8178937783094692445L;
    /**
     * 授权业务id
     */
    private String doorAuthLogId;
    /**
     * 授权设备ID List
     */
    private List<AuthorityDeviceDTO> doorList;

    /**
     * 授权员工ID List
     */
    private List<AddStaffAuthDTO> staffList;

    public UpdateAuthorityEvent(Object source, String doorAuthLogId, List<AuthorityDeviceDTO> doorList, List<AddStaffAuthDTO> staffList) {
        super(source);
        this.doorAuthLogId = doorAuthLogId;
        this.doorList = doorList;
        this.staffList = staffList;
    }
}
