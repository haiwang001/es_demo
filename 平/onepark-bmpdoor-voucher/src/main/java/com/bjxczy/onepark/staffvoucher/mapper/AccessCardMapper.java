package com.bjxczy.onepark.staffvoucher.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.staffvoucher.pojo.dto.AccessCardDto;
import com.bjxczy.onepark.staffvoucher.pojo.po.AccessCardPO;
import com.bjxczy.onepark.staffvoucher.pojo.vo.AccessCardVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AccessCardMapper extends BaseMapper<AccessCardPO> {

    List<AccessCardVo> accessCardList(@Param("dto") AccessCardDto dto);

    List<AccessCardVo> find(@Param("dto") AccessCardDto dto);

    List<Integer> selectAccessCardByStaffId(@Param("staffId") Integer staffId);

    @Select("select id from organization.tbl_organization where id = #{deptId} or parent_id = #{deptId}")
    List<Integer> selectByDepartmentId(@Param("deptId") Integer deptId);

    Integer updateFlagById(@Param("id") Integer id);

    List<AccessCardPO> selectAccessCode(@Param("accessCode") String accessCode);
}
