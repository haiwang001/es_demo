package com.bjxczy.onepark.staffvoucher.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.staffvoucher.entity.StaffGroup;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StaffGroupMapper extends BaseMapper<StaffGroup> {
}
