package com.bjxczy.onepark.common.constant.enums;

/**
 * @author jisen
 * @date 2020/2/14 10:50
 */
public enum ReturnCode {
    PARAMETERS_ERROR(105100001,"请求参数错误"),
    DEVICE_NOT_EXIST(105100003,"设备不存在"),
    DEVICE_OFFLINE(105100004,"设备不存在"),
    DEVICE_TIMEOUT(105100005,"设备请求超时"),
    DEVICE_PERMISSION_DENIED(105100006,"设备权限验证失败"),
    DEVICE_SHUTDOWN(105100008,"设备已关机"),
    COMMAND_EXECUTE_FAILE(105100009,"命令执行失败"),
    SERVICE_UNAVAILABLE(10200,"服务不可用"),
    SERVICE_READTIMEOUT(10201,"服务连接超时"),
    //设备相关
    DEVICE_LIST_ERROR(10300,"获取摄像头列表失败："),
    DEVICE_ONLINE_ERROR(10301,"获取在线状态失败："),
    DEVICE_GROUP_ERROR(10302,"未找到摄像头分组"),
    //流相关
    PLAYBACKURL_ERROE(10310,"获取回放流失败："),
    PREVIEWURL_ERROE(10311,"获取实时流失败："),
    //事件订阅
    EVENTSUBSCRIPTION_ERROR(10320,"事件订阅错误："),
    EVENTSUBSCRIPTION_VIEW_ERROR(10321,"事件查询错误："),
    EVENTUNSUBSCRIPTION_ERROR(10322,"退订事件发生错误："),
    //人脸布控
    FACE_CAPTURE_SEARCH_ERROR(10330,"以脸搜脸发生错误："),
    FACE_CAPTURE_SEARCH_LIST_ERROR(10331,"查询抓拍点信息时发生错误："),
    FACE_RECOGNITION_PLAN_ADD_ERROR(10332,"单个添加人脸识别计划时发生错误："),
    FACE_RECOGNITION_PLAN_DELETE_ERROR(10333,"删除人脸识别时发生错误："),
    FACE_RECOGNITION_PLAN_UPDATE_ERROR(10334,"修改人脸识别计划时发生错误："),
    FACE_RECOGNITION_PLAN_LIST_ERROR(10335,"查询人脸识别计划时发生错误："),
    FACE_RECOGNITION_PLAN_RESTART_ERROR(10336,"重新下发人脸识别计划时发生错误："),
    FACE_RECOGNITION_BLACK_LIST_ERROR(10337,"查询重点人脸识别失败："),
    FACE_RECOGNITION_WHITE_LIST_ERROR(10338,"查询陌生人脸识别失败："),
    FACE_RECOGNITION_HIGH_LIST_ERROR(10339,"查询高频人脸识别失败："),
    FACE_GROUP_ERROR(10340,"查询人脸分组时发生错误："),
    FACE_GROUP_ADD_ERROR(10341,"添加人脸分组时发生错误："),
    FACE_GROUP_COPY_ERROR(10342,"批量拷贝人脸分组时发生错误："),
    FACE_GROUP_UPDATE_ERROR(10343,"修改脸分组时发生错误："),
    FACE_GROUP_DELETE_ERROR(10344,"删除人脸分组时发生错误："),
    FACE_LIST_ERROR(10345,"查询人脸时发生错误："),
    FACE_ADD_ERROR(10346,"添加人脸时发生错误："),
    FACE_DELETE_ERROR(10347,"批量删除人脸时发生错误："),
    FACE_DOWNLOAD_ERROR(10349,"下载人脸图片时发生错误"),
    FACE_RECOGNITION_RESOURCE_ERROR(10348,"查询资源失败：");

    private int code;
    private String desc;

    ReturnCode(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }
    public String getDesc() {
        return desc;
    }


}
