package com.bjxczy.onepark.common.utils;

import com.alibaba.nacos.api.utils.StringUtils;
import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.net.URL;

public class StaffUtils {

    public static String formatFullMisCode(String misCode) {
        if (StringUtils.isBlank(misCode)) {
            return null;
        } else if (misCode.contains("E")) {
            return misCode;
        }
        return "E00" + misCode;
    }

    public static void main(String[] args) {
        try {
            String spec = "http://192.165.1.62:9881/group1/M00/00/20/wKUBPmQBksuAf9uPAADB7a2mjQk67.jpeg";
            String base64String02 = getFileBase64String(spec);
            System.err.println(base64String02);
        } catch (Exception e) {
        }
    }

    /**
     * 文件File类型转byte[]
     *
     * @param file
     * @return
     */
    private static byte[] fileToByte(File file) {
        byte[] fileBytes = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            fileBytes = new byte[(int) file.length()];
            fis.read(fileBytes);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileBytes;
    }


    /**
     * @param url 远端文件Url
     * @return File
     */
    public static String getFileBase64String(String url) {
        //对本地文件命名
        String fileName = url.substring(url.lastIndexOf("."), url.length());
        File file = null;
        URL urlfile;
        try {
            // 创建一个临时路径
            file = File.createTempFile("file", fileName);
            //下载
            urlfile = new URL(url);
            try (InputStream inStream = urlfile.openStream();
                 OutputStream os = new FileOutputStream(file);) {
                int bytesRead = 0;
                byte[] buffer = new byte[8192];
                while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
            }
        } catch (Exception ignored) {
        }
        return Base64.encodeBase64String(fileToByte(file));
    }


    public static File getFileBase64File(String url) {
        //对本地文件命名
        String fileName = url.substring(url.lastIndexOf("."), url.length());
        File file = null;
        URL urlfile;
        try {
            // 创建一个临时路径
            file = File.createTempFile("file", fileName);
            //下载
            urlfile = new URL(url);
            try (InputStream inStream = urlfile.openStream();
                 OutputStream os = new FileOutputStream(file);) {
                int bytesRead = 0;
                byte[] buffer = new byte[8192];
                while ((bytesRead = inStream.read(buffer, 0, 8192)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
            }
        } catch (Exception ignored) {
        }
        return file;
    }

}
