package com.bjxczy.onepark.door.interfaces.listener.async;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.core.rabbitmq.event.door.AuthorityResultPayload;
import com.bjxczy.core.rabbitmq.rpc.RabbitAsyncComponent;
import com.bjxczy.core.rabbitmq.rpc.RabbitAsyncHandler;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.onepark.common.constant.enums.SyncStatusCode;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.door.entity.DoorInfoPo;
import com.bjxczy.onepark.door.mapper.DoorAuthLogItemMapper;
import com.bjxczy.onepark.door.mapper.DoorInfoMapper;
import com.bjxczy.onepark.door.service.DoorInfoService;
import com.bjxczy.onepark.staffvoucher.entity.StaffVoucher;
import com.bjxczy.onepark.staffvoucher.mapper.StaffVoucherMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/*
 *@Author wlw  授权订阅 响应
 *@Date 2023/7/5 10:56
 */
@Log4j2
@Component
@RabbitAsyncComponent(
        value = "DoorAuthorityRabbitAsyncHandle", // 处理器KEY，同时作为Bean名称
        exchange = DoorMutualBaseListener.DEFAULT_EXCAHNGE_NAME, // 目的交换机（公共组件中定义）
        routingKey = DoorMutualBaseListener.UPDATE_DOOR_CTRL_AUTHORIZATION  // 目的路由（公共组件中定义）
)
public class DoorAuthorityRabbitAsyncHandle extends RabbitAsyncHandler {
    @Resource
    private DoorInfoMapper doorInfoMapper;
    @Resource
    private DoorAuthLogItemMapper doorAuthLogItemMapper;

    /**
     * 统一返回
     *
     * @param id
     * @param message
     */
    @Override
    public void receive(Object id, Message message) {
        Object o = this.messageConverter.fromMessage(message);
        if (o instanceof CreateVisitorAndDoorVo) {
            CreateVisitorAndDoorVo door = (CreateVisitorAndDoorVo) o;
            doorAuthLogItemMapper.updateByLogIdAndType(
                    door.getCode() == 0 ? SyncStatusCode.SUCCESSFUL.getCode() : SyncStatusCode.ERROR.getCode(),
                    door.getCode() == 0 ? SyncStatusCode.SUCCESSFUL.getMsg() : door.getMsg(),
                    door.getType(), id);
        }
        if (o instanceof AuthorityResultPayload) {
            AuthorityResultPayload door = (AuthorityResultPayload) o;
            DoorInfoPo doorInfoPo = doorInfoMapper.selectOne(new QueryWrapper<DoorInfoPo>().eq("device_code", door.getDeviceCode()));
            doorAuthLogItemMapper.updateByDeviceCodeAndStaffId(
                    door.getSuccess() ? SyncStatusCode.SUCCESSFUL.getCode() : SyncStatusCode.ERROR.getCode(),
                    door.getSuccess() ? SyncStatusCode.SUCCESSFUL.getMsg() : door.getMessage(), id, doorInfoPo.getId(), door.getStaffId().toString());
        }

    }
}
