package com.bjxczy.onepark.visitor.service.impl;


import cn.hutool.core.map.MapUtil;
import com.alibaba.nacos.common.utils.UuidUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.core.rabbitmq.supports.OrganizationBaseListener;
import com.bjxczy.core.rabbitmq.supports.VisitorBaseListener;
import com.bjxczy.core.web.privacy.MaskingOperation;
import com.bjxczy.core.web.privacy.PrivacyConfigService;
import com.bjxczy.core.web.privacy.handler.MaskingHandler;
import com.bjxczy.core.web.privacy.handler.MobileMaskingHandler;
import com.bjxczy.core.web.privacy.iterator.ListMaskingIterator;
import com.bjxczy.onepark.common.constant.enums.SyncStatusCode;
import com.bjxczy.onepark.common.constant.enums.VisitorStatusCode;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.OrganizationInformation;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.organization.StaffInformation;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorInformation;
import com.bjxczy.onepark.common.model.visitor.DeleteVisitorInformation;
import com.bjxczy.onepark.common.utils.*;
import com.bjxczy.onepark.visitor.command.AddVisitorCommand;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorInfoPo;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorSyncLogPo;
import com.bjxczy.onepark.visitor.interfaces.listener.async.VisitorRabbitAsyncHandle;
import com.bjxczy.onepark.visitor.mapper.VisitorInviteInfoMapper;
import com.bjxczy.onepark.visitor.mapper.VisitorRightsAndDoorMapper;
import com.bjxczy.onepark.visitor.mapper.VisitorSyncLogMapper;
import com.bjxczy.onepark.visitor.pojo.dto.VisitorInviteDto;
import com.bjxczy.onepark.visitor.pojo.vo.VisitorInviteExcelVo;
import com.bjxczy.onepark.visitor.pojo.vo.VisitorInvitePageListVo;
import com.bjxczy.onepark.common.client.*;
import com.bjxczy.onepark.visitor.service.VisitorInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Service
@Slf4j
public class VisitorInfoServiceImpl implements VisitorInfoService {
    @Resource
    private BmpQueryStaffServer bmpQueryStaffServer;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private VisitorRabbitAsyncHandle asyncHandler;
    @Resource
    private PrivacyConfigService privacyConfigService;
    @Resource
    private VisitorInviteInfoMapper visitorInviteInfoMapper;
    @Resource
    private VisitorRightsAndDoorMapper visitorRightsAndDoorMapper;
    @Autowired
    private BmpQueryStaffServer StaffServer;
    @Autowired
    private BmpHikServiceFeign serviceFeign;
    @Autowired
    private BmpHikVisitorFeignClient VisitorFeign;
    @Resource
    private VisitorSyncLogMapper visitorSyncLogMapper;
    @Override
    public void VisitorInfoExportExcel(HttpServletResponse response, VisitorInviteDto dto) {
        List<VisitorInviteExcelVo> visitorInvitePageListVos = visitorInviteInfoMapper.VisitorExportExcelList(dto);
        privacyConfigService.doMasking("bmpvoucher.visitor", MaskingOperation.EXPORT,
                MapUtil.builder(new HashMap<String, MaskingHandler>())
                        .put("visitorMobile", new MobileMaskingHandler())
                        .put("visitorIdCard", new MobileMaskingHandler())
                        .put("receptionistMobile", new MobileMaskingHandler()).build()
                , new ListMaskingIterator().set(visitorInvitePageListVos));
        try {
            ExcelUtils.export(response,
                    "访客邀约列表",
                    "访客邀约列表",
                    visitorInvitePageListVos, VisitorInviteExcelVo.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ResultException("访客信息列表下载失败!");
        }
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public Object addVisitor(AddVisitorCommand com, UserInformation sysUser) {
        VisitorInfoPo po = setVisitorInfoPo(com, sysUser);
        VisitorVerify(po);
        // 查询访客权限组
        List<String> list = visitorRightsAndDoorMapper.setVisitorGroupByAreaId(com.getAreaId());
        if (list == null || list.size() == 0) {
            throw new ResultException("该区域无访客权限!");
        }
        VisitorCreate(po, list); //
        return visitorInviteInfoMapper.insert(po);
    }

    /**
     * @param po
     * @param list 海康访客权限组
     */
    private void VisitorCreate(VisitorInfoPo po, List<String> list) {
        StaffInfoFine staff = StaffServer.getStaffById(po.getReceptionistId());
        if (staff == null) {
            return;
        }
        // 查看该员工组织是否存在  存在
        Boolean orgHik = serviceFeign.isOrgHik(staff.getOrganizationId());
        if (!orgHik) {
            DeptNameInfo deptNameInfo = staff.getDeptNames().get(staff.getDeptNames().size() - 1);
            OrganizationInformation information = new OrganizationInformation();
            information.setFldName(deptNameInfo.getDeptName());
            information.setId(staff.getOrganizationId());
            information.setParentId(-1);
            rabbitTemplate.convertAndSend(
                    OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME,
                    OrganizationBaseListener.CREATE_ORGANIZATION_EVENT,
                    information);
            DateUtil.await();
        }

        if (!serviceFeign.isStaffHik(po.getReceptionistId())) {
            StaffInformation information1 = new StaffInformation();
            information1.setId(po.getReceptionistId());
            information1.setFldName(staff.getFldName());
            information1.setOrganizationId(staff.getOrganizationId());
            information1.setMobile(staff.getMobile());
            information1.setUuid(UuidUtils.generateUuid());
            information1.setEmployeeNo(staff.getEmployeeNo());
            information1.setDeptNames(staff.getDeptNames());
            rabbitTemplate.convertAndSend(
                    OrganizationBaseListener.DEFAULT_EXCAHNGE_NAME,
                    OrganizationBaseListener.CREATE_STAFF_EVENT,
                    information1);
            DateUtil.await();
        }
        // 多平台授权 目前只有海康 需要其他平台 补充CreateVisitorInformation对应平台所需字段
        asyncHandler.sendMessage(po.getId(), new CreateVisitorInformation(
                po.getId(),
                po.getVisitorName(),
                po.getReceptionistId(),
                po.getVisitTime(),
                po.getLeaveTime(),
                po.getVisitorMobile(),
                po.getVisitorGender().toString(),
                po.getVisitorPhoto(),
                list));
    }



    /**
     * 访客数据 校验
     *
     * @param po
     */
    private void VisitorVerify(VisitorInfoPo po) {
        // 姓名相同  证件号 手机号相同 已经预约过
        if (visitorInviteInfoMapper.selectByName(po) > 0) {
            throw new ResultException("存在其他有效预约 无法预约!");
        } // 姓名不同 证件号 或者 手机号相同 未到访的访客 已结束已取消不在校验
        if (visitorInviteInfoMapper.selectByMobileAndIdCard(po) > 0) {
            throw new ResultException("证件号或者手机号和其他有效访客冲突!");
        }
        long time1 = DateUtil.parseDateTime(DateUtil.getYearaAndMonthAndDay() + " 00:00:00").getTime();//获取 开始时间毫秒值
        long time = DateUtil.parseDateTime(po.getVisitTime()).getTime();//获取 开始时间毫秒值
        long time2 = DateUtil.parseDateTime(po.getLeaveTime()).getTime();//获取 结束时间时间毫秒值
        if (time2 < time1) {
            throw new ResultException("离开时间需要超过现在时间!");
        }
        if (time >= time2) {
            throw new ResultException("离开时间需要大于到访时间!");
        }
        if (time < time1) {
            throw new ResultException("到访时间最早为今日!");
        }
    }

    private VisitorInfoPo setVisitorInfoPo(AddVisitorCommand com, UserInformation sysUser) {
        VisitorInfoPo po = new VisitorInfoPo(
                com.getVisitorName(),
                com.getVisitorMobile(),
                com.getVisitorIdCardType(),
                com.getVisitorIdCard(),
                com.getVisitorPhoto(),
                com.getVisitTime() + " 00:00:00",
                com.getLeaveTime() + " 23:59:59",
                com.getReceptionistId(),
                new Date(),
                new Date(),
                sysUser.getStaffName(),
                0,
                com.getArriveType(),
                1,
                com.getVisitorRemark()
        );
        po.setId(UuidUtils.generateUuid());
        StaffInfoFine staff = bmpQueryStaffServer.getStaffById(com.getReceptionistId());
        List<DeptNameInfo> deptNameInfos = StrUtils.DeptNameInfo(staff.getDeptNames());
        po.setReceptionistDeptName(deptNameInfos.get(0).getDeptName());
        po.setAreaId(com.getAreaId());
        po.setTenantId(com.getTenantId());
        po.setSpaceId(com.getSpaceId());
        po.setSpaceName(com.getSpaceName());
        po.setVisitorGender(StrUtils.getGenderByIdCard(com.getVisitorIdCard()));
        return po;
    }


    @Override
    public IPage<VisitorInvitePageListVo> VisitorInvitePageList(VisitorInviteDto dto, UserInformation sysUser) {
        dto.setVisitTime(dto.getVisitTime() != null ? dto.getVisitTime() + " 00:00:00" : "");
        dto.setLeaveTime(dto.getLeaveTime() != null ? dto.getLeaveTime() + " 23:59:59" : "");
        return visitorInviteInfoMapper.VisitorInvitePageList(dto.toPage(), dto);
    }

    @Override
    public Object delVisitorInfo(String id) {
        VisitorInfoPo visitorInfo = visitorInviteInfoMapper.selectById(id);
        if (visitorInfo.getArriveStatus().equals(VisitorStatusCode.HAVE_NOT_STARTED.getCode()) ||
                visitorInfo.getArriveStatus().equals(VisitorStatusCode.NON_ARRIVAL.getCode()) ||
                visitorInfo.getArriveStatus().equals(VisitorStatusCode.ARRIVED.getCode())) {
            rabbitTemplate.convertAndSend(VisitorBaseListener.DEFAULT_EXCAHNGE_NAME, VisitorBaseListener.DEL_VISITOR_EVENT, new DeleteVisitorInformation(visitorInfo.getId(), visitorInfo.getOrderId()));
        }
        return visitorInviteInfoMapper.deleteById(id);
    }

}
