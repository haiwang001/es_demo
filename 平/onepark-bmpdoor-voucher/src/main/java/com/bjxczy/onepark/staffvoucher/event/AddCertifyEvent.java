package com.bjxczy.onepark.staffvoucher.event;

import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.staffvoucher.entity.AccessCard;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEvent;

/*
 *@Date 2023/1/18 16:55
 */
@Log4j2
@Data
public class AddCertifyEvent extends ApplicationEvent {

    private static final long serialVersionUID = -8028455974280175527L;
    private AccessCard accessCard;
    private StaffInfoFine staff;


    public AddCertifyEvent(Object source, AccessCard accessCard, StaffInfoFine staff) {
        super(source);
        this.accessCard = accessCard;
        this.staff = staff;
    }

}
