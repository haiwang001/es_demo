package com.bjxczy.onepark.common.client;


import com.bjxczy.core.feign.client.user.BaseUserFeignClient;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("user-server")
public interface BmpUserFeignClient extends BaseUserFeignClient {
}
