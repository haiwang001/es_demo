package com.bjxczy.onepark.staffvoucher.pojo.dto.command;

import lombok.Data;

/**
 * @author ：yubaowang
 * @date ：2022/12/27 14:15
 */
@Data
public class RemoveAccessCommand {
    private Integer id;

    public RemoveAccessCommand(Integer id) {
        this.id = id;
    }
}
