package com.bjxczy.onepark.staffvoucher.pojo.vo;

import lombok.Data;

@Data
public class FloorVO {
	
	private Integer id;
	
	private String nodeName;
	
	private Integer parentId;

}
