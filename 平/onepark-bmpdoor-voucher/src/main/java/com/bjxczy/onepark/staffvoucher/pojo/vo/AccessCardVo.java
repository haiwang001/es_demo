package com.bjxczy.onepark.staffvoucher.pojo.vo;

import lombok.Data;

import java.util.Date;

@Data
public class AccessCardVo {

    /**
     * id
     */
    private Integer id;
    /**
     * 部门ID
     */
    private Integer deptId;

    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 员工编号
     */
    private String employeeNo;
    /**
     * 员工姓名
     */
    private String staffName;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 员工ID
     */
    private Integer staffId;
    /**
     * 职级ID
     */
    private Integer rankId;
    /**
     * 门禁卡号
     */
    private String accessCode;

    /**
     * 字典值
     */
    // private Integer comeFrom;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 操作人
     */
    private String operator;

    /**
     * 门禁卡类型
     */
    private String accessCardType;
}
