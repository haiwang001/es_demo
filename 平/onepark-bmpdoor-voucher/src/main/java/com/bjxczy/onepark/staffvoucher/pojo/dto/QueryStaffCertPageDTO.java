package com.bjxczy.onepark.staffvoucher.pojo.dto;

import lombok.Data;

@Data
public class QueryStaffCertPageDTO extends BasePageDTO {
    /**
     * 融合参数
     */
    private String merge;
    /**
     * 员工部门id
     */
    private Integer deptId;
    /**
     * 凭证
     */
    private String voucherType;
    /**
     * 凭证数量 voucher_num
     */
    private Integer voucherCnt;
}
