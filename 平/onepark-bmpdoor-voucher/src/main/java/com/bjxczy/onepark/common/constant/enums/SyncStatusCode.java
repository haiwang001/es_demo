package com.bjxczy.onepark.common.constant.enums;


/**
 * 发送授权状态 0 - 下发中；1 - 下发成功；2 - 下发失败
 */
public enum SyncStatusCode {

    UNDER_WAY(0, "下发中"),
    SUCCESSFUL(1, "下发成功"),
    ERROR(2, "下发失败");

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    SyncStatusCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
