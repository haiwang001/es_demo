package com.bjxczy.onepark.staffvoucher.pojo.dto.command;

import lombok.Data;

@Data
public class CreateAccessCommand {
    /**
     * 员工ID
     */
    private Integer staffId;
    /**
     * 备注
     */
    // private String remark;
    /**
     * 卡号码
     */
    private String accessCode;
    /**
     * 卡类型
     */
    private Integer comeFrom;
    /**
     * 操作人
     */
    private String operator;
}
