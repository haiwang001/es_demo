package com.bjxczy.onepark.door.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/7/12 10:58
 */
@Data
public class DoorOverviewVo {
    private Integer all;// 全部设备数
    private Integer onLine;//在线设备数
    private Integer noOnLine; //不在线设备

    public DoorOverviewVo(Integer all, Integer onLine, Integer noOnLine) {
        this.all = all;
        this.onLine = onLine;
        this.noOnLine = noOnLine;
    }
    /* map.put("All",list.size());
        map.put("Online",collect.get(1)==null?0:collect.get(1).

    size()); //
        map.put("NoOnline",collect.get(0)==null?0:collect.get(0).

    size()); // */
}
