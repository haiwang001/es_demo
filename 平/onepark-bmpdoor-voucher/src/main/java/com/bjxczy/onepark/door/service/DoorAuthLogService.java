package com.bjxczy.onepark.door.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.door.entity.DoorAuthLogPo;
import com.bjxczy.onepark.door.pojo.dto.AuthorizationRecordDto;
import com.bjxczy.onepark.door.pojo.dto.ParticularsDto;
import com.bjxczy.onepark.door.pojo.vo.ParticularsVo;

public interface DoorAuthLogService {
    IPage<DoorAuthLogPo> page(AuthorizationRecordDto dto);

    IPage<ParticularsVo> particulars(ParticularsDto dto);
}
