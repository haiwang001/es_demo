package com.bjxczy.onepark.staffvoucher.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/*
 *@Date 2023/2/8 10:14
 *@Version 1.0
 */
@Data
@TableName("staff_group")
public class StaffGroup implements Serializable {

    private static final long serialVersionUID = 5463942186183972522L;

    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 分组名称
     */
    @NotBlank(message = "分组名称不能为空！")
    private String name;

    /**
     * 父id
     */
    @NotBlank(message = "父id 不能为空！")
    private Integer fatherId;

    /**
     * 分组员工数组 id拼接
     */
    @TableField(exist = false)
    private List<StaffGroup> children;
    /**
     * 操作人
     */
    private String operator;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 0未删除1已删除
     */
    @TableLogic(value = "0", delval = "1")
    private int delFlag;
}
