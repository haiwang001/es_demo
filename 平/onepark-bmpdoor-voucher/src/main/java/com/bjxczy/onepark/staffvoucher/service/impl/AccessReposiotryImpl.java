package com.bjxczy.onepark.staffvoucher.service.impl;

import com.bjxczy.core.web.base.LogicDelete;
import com.bjxczy.core.web.base.MyBatisPlusRepositoryImpl;
import com.bjxczy.onepark.common.client.BmpQueryStaffServer;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.common.utils.DateTools;
import com.bjxczy.onepark.common.utils.ExcelUtils;
import com.bjxczy.onepark.common.utils.PageUtils;
import com.bjxczy.onepark.staffvoucher.entity.AccessCard;
import com.bjxczy.onepark.staffvoucher.mapper.AccessCardMapper;
import com.bjxczy.onepark.staffvoucher.mapper.AccessRepository;
import com.bjxczy.onepark.staffvoucher.pojo.dto.AccessCardDto;
import com.bjxczy.onepark.staffvoucher.pojo.po.AccessCardPO;
import com.bjxczy.onepark.staffvoucher.pojo.vo.AccessCardExcelVo;
import com.bjxczy.onepark.staffvoucher.pojo.vo.AccessCardVo;
import com.bjxczy.onepark.staffvoucher.service.AccessQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccessReposiotryImpl extends
        MyBatisPlusRepositoryImpl<AccessCardMapper, AccessCardPO, AccessCard>
        implements AccessQueryService, AccessRepository {

    @Autowired
    private BmpQueryStaffServer bmpQueryStaffServer;

    @Resource
    private AccessCardMapper accessCardMapper;


    /**
     * 门禁卡管理查询
     *
     * @param dto
     * @return
     */
    @Override
    public R<?> accessCardLiat(AccessCardDto dto) {
        // 查询出员工门禁卡列表，
        List<AccessCardVo> list = accessCardMapper.accessCardList(dto);
        if (dto.getDeptId() != null) {
            List<AccessCardVo> list1 = new ArrayList<>(list.size());
            list.forEach(vel -> {
                accessCardMapper.selectByDepartmentId(dto.getDeptId()).forEach(id -> {
                    if (vel.getDeptId() != null){
                        if (vel.getDeptId().equals(id)) {
                            list1.add(vel);
                        }
                    }
                });
            });
            list.clear();
            list = list1;
        }
        return R.success(PageUtils.Page(dto, list));
    }


    /**
     * 导出
     * @param response
     * @param fileName
     * @param dto
     * @return
     */
    @Override
    public Boolean exportExcel(HttpServletResponse response, String fileName, AccessCardDto dto) {
        String sheetName = "门禁卡列表";
        //查询数据并赋值给ExcelData
        List<AccessCardVo> exportDTOList = accessCardMapper.accessCardList(dto);
        List<AccessCardExcelVo> collect = exportDTOList.stream().map(item -> {
            AccessCardExcelVo exportVo = new AccessCardExcelVo();
            exportVo.setStaffName(item.getStaffName());
            exportVo.setEmployeeNo(item.getEmployeeNo());
            exportVo.setMobile(item.getMobile());
            exportVo.setDeptId(item.getDeptId().toString());
            exportVo.setDeptName(item.getDeptName());
            exportVo.setAccessCode(item.getAccessCode());
            exportVo.setCreateTime(DateTools.format(item.getCreateTime(), DateTools.FORMAT_01));
            exportVo.setOperator(item.getOperator());
            exportVo.setAccessTypeName(item.getAccessCardType());
            return exportVo;
        }).collect(Collectors.toList());
        try {
            if (ObjectUtils.isEmpty(fileName)){
                fileName = "员工门禁卡信息".trim();
            }
            ExcelUtils.export(response, fileName, sheetName, collect, AccessCardExcelVo.class);
            return true;
        } catch (Exception e) {
            throw new ResultException("门禁卡信息导出失败");
        }
    }


    @Override
    public boolean isExistsAccessCard(String accessCode, Integer updateId) {
        List<AccessCardPO> list = lambdaQuery().eq(AccessCardPO::getAccessCode, accessCode)
                .eq(LogicDelete::getDelFlag, 0)
                .ne(updateId != null, AccessCardPO::getId, updateId).list();
        if (list != null && list.size() > 0) {
            return true;
        }
        return false;
    }

}
