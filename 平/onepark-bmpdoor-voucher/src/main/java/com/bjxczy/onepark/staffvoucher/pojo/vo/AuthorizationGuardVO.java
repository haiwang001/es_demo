package com.bjxczy.onepark.staffvoucher.pojo.vo;


import lombok.Data;

/**
 *@ClassName AuthorizationGuardVO
 *@Author 温良伟
 *@Date 2023/3/10 13:26
 *@Version 1.0
 */
@Data
public class AuthorizationGuardVO {
    private String name; //设备名称
    private String areaId; //一级
    private String areaName; //一级
    private String buildingId; //二级
    private String buildingName; //二级
    private String spaceId; //三级
    private String spaceName; //三级
}
