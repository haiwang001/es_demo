package com.bjxczy.onepark.visitor.interfaces.listener.publish;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.core.rabbitmq.supports.VisitorBaseListener;
import com.bjxczy.onepark.common.constant.enums.SyncStatusCode;
import com.bjxczy.onepark.common.constant.enums.VisitorStatusCode;
import com.bjxczy.onepark.common.model.visitor.CreateVisitorAndDoorVo;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorInfoPo;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorSyncLogPo;
import com.bjxczy.onepark.visitor.mapper.VisitorInviteInfoMapper;
import com.bjxczy.onepark.visitor.mapper.VisitorSyncLogMapper;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/24 10:54
 */
@Log4j2
@Component
public class VisitorListener extends VisitorBaseListener {
    @Resource
    private VisitorInviteInfoMapper visitorInviteInfoMapper;
    @Resource
    private VisitorSyncLogMapper visitorSyncLogMapper;




    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "bmp_visitor_create_msg_event_queue", durable = "true"),  // 持久化队列
                    key = VisitorBaseListener.DEL_VISITOR_EVENT_MSG,
                    exchange = @Exchange(value = VisitorBaseListener.DEFAULT_EXCAHNGE_NAME) // 绑定的交换机
            )
    })
    @Override
    public void createVisitorMsg(CreateVisitorAndDoorVo visitor, Channel channel, Message message) {
        List<VisitorSyncLogPo> logPos = visitorSyncLogMapper.selectList(new QueryWrapper<VisitorSyncLogPo>().eq("visitor_id", visitor.getId()).eq("platform_type", visitor.getType()));
        System.err.println("授权结果 "+visitor);
        if (logPos.size() == 0) {
            if (visitor.getCode() == 0) {
                visitorInviteInfoMapper.updateById(new VisitorInfoPo(visitor.getId(), visitor.getMsg()));
                visitorSyncLogMapper.insert(new VisitorSyncLogPo(visitor.getId(), visitor.getType(), SyncStatusCode.SUCCESSFUL.getCode(), SyncStatusCode.SUCCESSFUL.getMsg()));
            } else {
                visitorInviteInfoMapper.updateById(new VisitorInfoPo(visitor.getId(), VisitorStatusCode.FINISH.getCode()));
                visitorSyncLogMapper.insert(new VisitorSyncLogPo(visitor.getId(), visitor.getType(), SyncStatusCode.ERROR.getCode(), visitor.getMsg()));
            }
        }
        this.confirm(() -> true, channel, message);
    }
}
