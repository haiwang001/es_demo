package com.bjxczy.onepark.door.command;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/7/4 16:09
 */
@Data
public class DeleteAuthorizationCommand {
    private Integer staffId; // 取消授权 员工id
    private String deviceId;// 设备id
}
