package com.bjxczy.onepark.staffvoucher.service;


import com.bjxczy.onepark.staffvoucher.entity.StaffGroup;

import java.util.List;

public interface StaffGroupService {
    int addGroup(StaffGroup sg) throws Exception ;

    int updateGroup(StaffGroup sg) throws Exception;

    int deleteGroup(Integer id) throws Exception;

    List<StaffGroup> getGroups() throws Exception;

}
