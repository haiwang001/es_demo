package com.bjxczy.onepark.staffvoucher.pojo.dto;


import lombok.Data;

/*
 *@Date 2023/2/8 17:08
 */
@Data
public class StaffQueryListDto extends BasePageDTO {
    private String groupId;
    private String synthesize;
}
