package com.bjxczy.onepark.visitor.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bjxczy.onepark.common.constant.enums.PlatformTypeCode;
import com.bjxczy.onepark.door.entity.DoorInfoPo;
import com.bjxczy.onepark.door.mapper.DoorInfoMapper;
import com.bjxczy.onepark.visitor.entity.visitor.VisitorSyncLogPo;
import com.bjxczy.onepark.visitor.mapper.VisitorSyncLogMapper;
import com.bjxczy.onepark.visitor.service.VisitorSyncLogService;
import com.google.common.collect.ImmutableBiMap;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/20 17:32
 */
@Service
public class VisitorSyncLogServiceImpl extends
        ServiceImpl<VisitorSyncLogMapper, VisitorSyncLogPo> implements VisitorSyncLogService {
    @Resource
    private VisitorSyncLogMapper mapper;

    @Override
    public Object accreditResult(String id) {
        List<VisitorSyncLogPo> list = lambdaQuery().eq(VisitorSyncLogPo::getVisitorId, id).list();
        list.forEach(el -> {
            if (el.getPlatformType().equals(PlatformTypeCode.HIK.getMsg())) {
                el.setPlatformType("海康");
            }
            if (el.getPlatformType().equals(PlatformTypeCode.HANWNAG.getMsg())) {
                el.setPlatformType("汉王");
            }
            if (el.getPlatformType().equals(PlatformTypeCode.DDS.getMsg())) {
                el.setPlatformType("DDS");
            }
        });
        return list;
    }
}
