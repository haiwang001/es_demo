package com.bjxczy.onepark.staffvoucher.pojo.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class StaffCertPermissionVO extends StaffVoucherVO {

	private List<BaseTree<Object>> permissionTree;
	
}
