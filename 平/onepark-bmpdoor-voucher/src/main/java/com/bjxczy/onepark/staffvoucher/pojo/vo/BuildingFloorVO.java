package com.bjxczy.onepark.staffvoucher.pojo.vo;

import lombok.Data;

@Data
public class BuildingFloorVO {
	
	private Integer id;
	
	private Integer parentId;
	
	private String nodeName;
	
	private String buildingId;
	
	private String buildingName;

}
