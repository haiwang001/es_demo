package com.bjxczy.onepark.visitor.interfaces.controller.authorized;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.core.web.api.ApiResourceController;

import com.bjxczy.core.web.log.OperationType;
import com.bjxczy.core.web.log.SysLog;
import com.bjxczy.core.web.privacy.MaskingOperation;
import com.bjxczy.core.web.privacy.PrivacyConfigService;
import com.bjxczy.core.web.privacy.ResponseMasking;
import com.bjxczy.core.web.privacy.iterator.IPageMaskingIterator;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.visitor.command.AddVisitorCommand;
import com.bjxczy.onepark.visitor.pojo.dto.VisitorInviteDto;
import com.bjxczy.onepark.visitor.service.VisitorInfoService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@Log4j2
@ApiResourceController
@RequestMapping("visitorInvite")
public class VisitorInviteController {
    @Resource
    private VisitorInfoService visitorInfoService;


    @ApiOperation("pc端增加访客")
    @SysLog(operationType = OperationType.INSERT, operation = "增加访客申请,访客姓名：${com.visitorName} 创建人：${user.username}")
    @PostMapping(value = "/add", name = "pc-访客邀约")
    public R<?> addVisitor(@RequestBody AddVisitorCommand com,
                           @LoginUser(isFull = true) UserInformation sysUser) {
        return R.success(visitorInfoService.addVisitor(com, sysUser));
    }

    @ResponseMasking(configId = "bmpvoucher.visitor", operation = MaskingOperation.QUERY, iterator = IPageMaskingIterator.class)
    @ApiOperation("pc-访客邀约分页列表")
    @GetMapping(value = "/page", name = "pc-访客邀约列表")
    public R<?> visitorInfoList(VisitorInviteDto dto, @LoginUser(isFull = true) UserInformation sysUser) {
        return R.success(visitorInfoService.VisitorInvitePageList(dto, sysUser));
    }

    @SysLog(operationType = OperationType.DELETE, operation = "删除访客：${id},操作人：${user.username}")
    @DeleteMapping(value = "del/{id}", name = "pc-删除访客")
    public R<?> delVisitorInfo(@PathVariable("id") String id, @LoginUser(isFull = true) UserInformation sysUser) {
        return R.success(visitorInfoService.delVisitorInfo(id));
    }

    @ApiOperation("pc-excel导出全量访客信息")
    @SysLog(operationType = OperationType.EXPORT, operation = "导出全量访客信息,操作人：${user.username}")
    @GetMapping(value = "/export", name = "pc-访客邀约列表-excel导出")
    public void VisitorInfoExportExcel(HttpServletResponse response, VisitorInviteDto dto) {
        visitorInfoService.VisitorInfoExportExcel(response, dto);
    }
}
