package com.bjxczy.onepark.staffvoucher.pojo.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * @author ：yubaowang
 * @description：TODO
 * @date ：2022/9/6 11:09
 */
@Data
public class AccessTempCardVo {
    private Integer id;
    @Excel(name = "局址", width = 18, replace = {"_null"})
    private String name;
    @Excel(name = "姓名", width = 18, replace = {"_null"})
    private String staffName;
    @Excel(name = "手机号", width = 18, replace = {"_null"})
    private String mobile;
    @Excel(name = "证件号", width = 18, replace = {"_null"})
    private String idCard;
    @Excel(name = "年龄", width = 18, replace = {"_null"})
    private Integer age;
    @Excel(name = "部门", width = 18, replace = {"_null"})
    private String accessCode;
    @Excel(name = "门禁卡号", width = 18, replace = {"_null"})
    private String companyName;
    @Excel(name = "移动接口人", width = 18, replace = {"_null"})
    private String contactStaffName;
    @Excel(name = "项目负责人姓名", width = 18, replace = {"_null"})
    private String pmName;
    @Excel(name = "项目负责人手机号", width = 18, replace = {"_null"})
    private String pmMobile;
    @Excel(name = "创建时间", width = 18, replace = {"_null"})
    private Date createTime;
    @Excel(name = "操作人", width = 18, replace = {"_null"})
    private String operator;
    @Excel(name = "备注", width = 18, replace = {"_null"})
    private String remark;
    private Integer contactStaffId;
    private Integer rankId;
    private String fldName;
    private Integer organizationId;
    private String tenantId;

}
