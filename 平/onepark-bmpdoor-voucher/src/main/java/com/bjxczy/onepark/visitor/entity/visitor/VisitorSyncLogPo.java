package com.bjxczy.onepark.visitor.entity.visitor;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("v_visitor_sync_log")
public class VisitorSyncLogPo {
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 访客id visitor_id
     */
    private String visitorId;
    /**
     * 归属平台：（platformType） platform_type
     */
    private String platformType;
    /**
     * sync_status 同步状态：0 - 下发中；1 - 下发成功；2 - 下发失败
     */
    private Integer syncStatus;
    /**
     * sync_status_text 同步状态描述
     */
    private String syncStatusText;


    public VisitorSyncLogPo(String visitorId,String platformType) {
        this.visitorId = visitorId;
        this.platformType = platformType;
    }

    public VisitorSyncLogPo(Integer syncStatus, String syncStatusText) {
        this.syncStatus = syncStatus;
        this.syncStatusText = syncStatusText;
    }

    public VisitorSyncLogPo(String visitorId, String platformType, Integer syncStatus, String syncStatusText) {
        this.visitorId = visitorId;
        this.platformType = platformType;
        this.syncStatus = syncStatus;
        this.syncStatusText = syncStatusText;
    }

    public VisitorSyncLogPo() {
    }

}