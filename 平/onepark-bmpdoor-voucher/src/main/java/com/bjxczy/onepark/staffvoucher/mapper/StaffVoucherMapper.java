package com.bjxczy.onepark.staffvoucher.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bjxczy.onepark.staffvoucher.entity.StaffVoucher;
import com.bjxczy.onepark.staffvoucher.pojo.dto.QueryStaffCertPageDTO;
import com.bjxczy.onepark.staffvoucher.pojo.vo.AuthorizationGuardVO;
import com.bjxczy.onepark.staffvoucher.pojo.vo.StaffVoucherVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StaffVoucherMapper extends BaseMapper<StaffVoucher> {

    /**
     * 通过员工ID获取凭证信息
     * @param staffId  员工ID
     * @return 员工信息
     */
    StaffVoucher selectByStaffId(@Param("staffId") Integer staffId);


    /**
     * 本地删除员工门禁卡
     * @param staffId     员工ID
     * @param voucherCnt  凭证数
     * @param faceImg     人脸图片
     * @return 1；成功， 0失败
     */
    Integer updateAccessCodeByStaffId(@Param("staffId") Integer staffId,
                                      @Param("voucherCnt") Integer voucherCnt,
                                      @Param("accessCode") String accessCode);

    /**
     * 订阅人脸服务；删除人脸信息
     * @param staffId     员工ID
     * @param voucherCnt  凭证数
     * @param faceImg     人脸图片
     * @return 1；成功， 0失败
     */
    Integer deleteCertifyImgStaffId(@Param("staffId") Integer staffId,
                                    @Param("voucherCnt") Integer voucherCnt,
                                    @Param("faceImg") String faceImg);


    /**
     * 通过员工ID， 更新人脸凭证信息
     * @param staffVoucher  凭证信息
     * @return
     */
    int updateFaceImg(@Param("staffVoucher") StaffVoucher staffVoucher);


    /**
     * 通过员工ID  更新门禁卡凭证信息
     * @param staffVoucher  凭证信息
     * @return
     */
    int updateAccessCode(@Param("staffVoucher") StaffVoucher staffVoucher);



    /**
     * 批量获取员工凭证信息
     * @param staffVoucher 员工凭证类
     * @return  员工凭证信息列表
     */
    // @Select("select * from evidence_record where mobile = #{staffVoucher.mobile} and fld_name = #{staffVoucher.fldName} group by id")
    // List<StaffVoucher> getEvidenceRecordByNameAndMobile(@Param("staffVoucher") StaffVoucher staffVoucher);


    /**
     * 本方法； 本地
     * @param staffVoucher 员工凭证类
     * @return 是否更新成功
     */
    // @Update("update staff_voucher set voucher_cnt = #{staffVoucher.voucherCnt},dept_id= #{staffVoucher.deptId},dept_name= #{staffVoucher.deptName}, access_code = #{staffVoucher.accessCode} where mobile = #{staffVoucher.mobile} and fld_name = #{staffVoucher.fldName}")
    // int updateVoucherNumAndAccessCode(@Param("staffVoucher") StaffVoucher staffVoucher);


    /**
     * 修改人脸信息
     * 通过员工姓名和编号一致才能修改图像
     *
     * @param img
     * @param fldName
     * @param employeeNo
     * @return
     */
    // @Update("update evidence_record set  img = #{img} where  fld_name = #{fldName} and employee_no = #{employeeNo}")
    // Integer updateCertifyImgByfldName(@Param("img") String img,@Param("fldName") String fldName,@Param("employeeNo") String employeeNo);




    // @Select("select * from staff_voucher where staff_name = #{fldName} group by id")
    // StaffVoucher getCertifyByfldName(@Param("fldName") String fldName);


//    @Select("select " +
//            "   evidence_record.*," +
//            "   a.label staffTypeLabel," +
//            "   b.fld_name rank_name\n" +
//            "from evidence_record, " +
//            "     voucher_dict a, " +
//            "     organization.tbl_rank b\n" +
//            "where true " +
//            "   and evidence_record.staff_type = a.value\n" +
//            "   and a.group_code ='comeFrom'\n" +
//            "   and if(#{dto.merge} != '', evidence_record.fld_name like concat('%', #{dto.merge}, '%') or\n" +
//            "                              evidence_record.mobile like concat('%', #{dto.merge}, '%') or\n" +
//            "                              evidence_record.employee_no like concat('%', #{dto.merge}, '%'), true)\n" +
//            "   and if(#{dto.voucherNum} is not null, evidence_record.voucher_num = #{dto.voucherNum}, true)\n" +
//            "   and if(#{dto.voucherType} = 'code', evidence_record.access_code != 'null', true)\n" +
//            "   and if(#{dto.voucherType} = 'img', evidence_record.img != 'null', true)\n" +
//            "group by evidence_record.id   " +
//            "order by evidence_record.create_time " +
//            "desc")
    List<StaffVoucherVO> staffVoucherPageList(@Param("dto") QueryStaffCertPageDTO dto);



    // @Delete("delete evidence_record where fld_name = #{record.fldName} and employee_no = #{record.employeeNo} and mobile = #{record.mobile}")
    // void deleteByName(@Param("evidenceRecord") StaffVoucher record);

    @Select("select g.device_name name, g.area_id, g.tenant_id buildingId, g.space_id\n" +
            "            from d_door_info g,\n" +
            "                 (select a.device_id\n" +
            "                  from d_door_auth_log_item a\n" +
            "                  where a.staff_id =#{staffId}) co\n" +
            "            where co.device_id = g.id group by g.id")
    List<AuthorizationGuardVO> findStaffPermission(@Param("staffId") String staffId);



}
