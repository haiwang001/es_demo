package com.bjxczy.onepark.staffvoucher.pojo.dto.command;

import lombok.Data;

import java.util.List;

/**
 * @author ：yubaowang
 * @date ：2022/12/27 13:51
 */
@Data
public class UpdateAccessCommand {
    private Integer id;

    private String fldName;

    private String mobile;

    private String uuid;

    private String gender;

    private String Email;

    private String employeeNo;

    private String identityCard;

    private List<String> photo;

    private Integer organizationId;

    private Integer positionId;

    private Integer rankId;

    private String remark;

    private String accessCode;

    private Integer accessCardType;

    private String operator;
}
