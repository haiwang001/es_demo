package com.bjxczy.onepark.config;


import com.bjxczy.core.rabbitmq.rpc.AbstractRabbitAsyncConfigure;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 *@Author wlw
 *@Date 2023/7/5 10:50
 */
@Configuration
public class VouCherRabbitAsyncConfig extends AbstractRabbitAsyncConfigure {

    @Bean
    public MessageConverter messageConverter() {
        return new SimpleMessageConverter();
    }

}