package com.bjxczy.onepark.staffvoucher.interfaces.listener.subscribe;


import com.bjxczy.core.rabbitmq.supports.FaceBaseListener;
import com.bjxczy.onepark.common.model.face.FaceInfoTransmit;
import com.bjxczy.onepark.staffvoucher.event.AddCertifySyncEvent;
import com.bjxczy.onepark.staffvoucher.event.DelCertifySyncEvent;
import com.bjxczy.onepark.staffvoucher.event.PudCertifySyncEvent;
import com.bjxczy.onepark.staffvoucher.service.StaffVoucherService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/**
 * @author wangxiling
 * @date 2023/7/11 15:56
 * @description 监听人脸事件
 */
@Slf4j
@Component
public class FaceEventListener extends FaceBaseListener {

    @Resource
    private StaffVoucherService staffVoucherService;
    @Autowired
    private ApplicationEventPublisher publisher;
    /**
     * 添加人脸
     * @param channel 通道
     * @param message 消息
     */
    @RabbitListener(bindings = {@QueueBinding(value = @Queue(),
            key = ADD_FACE_EVENT, exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void addFaceEvent(FaceInfoTransmit map, Channel channel, Message message) {
        try {
            staffVoucherService.addCertify(map);
            publisher.publishEvent(new AddCertifySyncEvent(this, map.getStaffId(), map.getFaceImg(), null));
            this.confirm(()->true,channel,message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 修改人脸
     * @param channel 通道
     * @param message 消息
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = PUT_FACE_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void putFaceEvent(FaceInfoTransmit map, Channel channel, Message message) {
        try {
            staffVoucherService.updateCertify(map);
            publisher.publishEvent(new PudCertifySyncEvent(this, map.getStaffId(), map.getFaceImg(), null));
            this.confirm(()->true,channel,message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除人脸
     * @param channel 通道
     * @param message 消息
     */
    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(),
            key = DEL_FACE_EVENT,
            exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME, type = ExchangeTypes.DIRECT))})
    @Override
    public void delFaceEvent(FaceInfoTransmit map, Channel channel, Message message) {
        publisher.publishEvent(new DelCertifySyncEvent(this, map.getStaffId(), "faceImg", null));
        try {
            staffVoucherService.deleteCertify(map);
            this.confirm(()->true,channel,message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
