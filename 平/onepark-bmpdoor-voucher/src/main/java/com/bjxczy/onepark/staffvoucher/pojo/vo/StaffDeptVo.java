package com.bjxczy.onepark.staffvoucher.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/4/18 16:47
 */
@Data
public class StaffDeptVo {
    private String id; // 部门id
    private String name; // 部门名
    private String fldName; // 员工姓名
    private String mobile; // 手机号
}
