package com.bjxczy.onepark.visitor.pojo.vo;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/6/20 11:50
 */
@Data
public class FaceAppointmentRegistrationVo {
    private int code;
    private String msg;

    public FaceAppointmentRegistrationVo(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
