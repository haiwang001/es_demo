package com.bjxczy.onepark.staffvoucher.pojo.vo;

import lombok.Data;

@Data
public class RecordVo {
    private String cameraIndexCode;
    private String beginTime;
    private String endTime;
    private long size;
    private long length;
    private String serviceId;
    private String fileName;
}
