package com.bjxczy.onepark.door.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.core.rabbitmq.event.door.AuthorityResultPayload;
import com.bjxczy.onepark.door.entity.DoorAuthLogItemPo;
import com.bjxczy.onepark.door.entity.DoorAuthLogPo;
import com.bjxczy.onepark.door.pojo.dto.ParticularsDto;
import com.bjxczy.onepark.door.pojo.vo.ParticularsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/*
 *@Author wlw
 *@Date 2023/7/10 10:32
 */
@Mapper
public interface DoorAuthLogItemMapper extends BaseMapper<DoorAuthLogItemPo> {
    @Select("SELECT a.id,\n" +
            "                   a.auth_log_id,\n" +
            "                   a.device_id,\n" +
            "                   a.device_name,\n" +
            "                   a.space_name,\n" +
            "                   a.staff_id,\n" +
            "                   a.staff_name,\n" +
            "                   a.mobile,\n" +
            "                   a.dept_id,\n" +
            "                   a.dept_name,\n" +
            "                   b.label sync_status,\n" +
            "                   a.sync_status_text,\n" +
            "                   a.save_time\n" +
            "            FROM d_door_auth_log_item a\n" +
            "left join voucher_dict b on b.value = a.sync_status and b.group_code = 'authorizationState'\n" +
            "            WHERE a.auth_log_id = #{dto.id} and if(#{dto.authorizationState} != '',sync_status = #{dto.authorizationState}, true)\n" +
            "            group by a.id")
    IPage<ParticularsVo> selectByLogId(Page<Object> objectPage, @Param("dto") ParticularsDto dto);

    @Update("update d_door_auth_log_item set sync_status = #{i} ,sync_status_text = #{s} where auth_log_id  = #{id} and platform_type = #{type} and sync_status = 0")
    void updateByLogIdAndType(@Param("i") int i, @Param("s") String s,
                              @Param("type") String type, @Param("id") Object id);


    @Update("update d_door_auth_log_item\n" +
            "set sync_status      = #{status},\n" +
            "    sync_status_text = #{text}\n" +
            "where auth_log_id = #{logid}\n" +
            "  and staff_id = #{staffId}\n" +
            "  and device_id = #{devid} and sync_status = 0")
    void updateByDeviceCodeAndStaffId(@Param("status") int status,
                                      @Param("text") String text,
                                      @Param("logid") Object logid,
                                      @Param("devid") String devid,
                                      @Param("staffId") String staffId);
}
