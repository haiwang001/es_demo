package com.bjxczy.onepark.door.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bjxczy.onepark.door.entity.DoorAuthLogPo;
import com.bjxczy.onepark.door.mapper.DoorAuthLogItemMapper;
import com.bjxczy.onepark.door.mapper.DoorAuthLogMapper;
import com.bjxczy.onepark.door.pojo.dto.AuthorizationRecordDto;
import com.bjxczy.onepark.door.pojo.dto.ParticularsDto;
import com.bjxczy.onepark.door.pojo.vo.ParticularsVo;
import com.bjxczy.onepark.door.service.DoorAuthLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/*
 *@Author wlw
 *@Date 2023/7/6 18:13
 */
@Service
public class DoorAuthLogServiceImpl implements DoorAuthLogService {
    @Resource
    private DoorAuthLogMapper mapper;
    @Resource
    private DoorAuthLogItemMapper doorAuthLogItemMapper;
    @Override
    public IPage<DoorAuthLogPo> page(AuthorizationRecordDto dto) {
        return mapper.pageList(dto.toPage(),dto);
    }

    @Override
    public IPage<ParticularsVo> particulars(ParticularsDto dto) {
        return doorAuthLogItemMapper.selectByLogId(dto.toPage(),dto);
    }
}
