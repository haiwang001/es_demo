package com.bjxczy.onepark.visitor.interfaces.listener.subscribe;

import com.bjxczy.core.rabbitmq.supports.WorkflowBaseListener;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.model.workflow.AddVisitsInfo;
import com.bjxczy.onepark.visitor.command.AddVisitorCommand;
import com.bjxczy.onepark.visitor.service.VisitorInfoService;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class WorkFlowListener extends WorkflowBaseListener {

    @Autowired
    private VisitorInfoService visitorInfoService;

    @RabbitListener(bindings = {@QueueBinding(
                    value = @Queue(name = "bmpworkflow_add_visits_queue", durable = "true"),
                    key = EOORDER_ADD_VISITS_INFO_EVENT,
                    exchange = @Exchange(value = DEFAULT_EXCAHNGE_NAME))})
    @Override
    public void addVisitsInfo(AddVisitsInfo addVisitsInfo, Channel channel, Message message) {
        UserInformation information = addVisitsInfo.getUserInformation();
        try {
            AddVisitorCommand command = new AddVisitorCommand();
            command.setVisitorName(addVisitsInfo.getName());
            command.setVisitorMobile(addVisitsInfo.getMobile());
            command.setVisitorIdCardType(addVisitsInfo.getCertificateType());
            command.setVisitorIdCard(addVisitsInfo.getCertificateNo());
            command.setVisitorPhoto(addVisitsInfo.getPhotoUrl());
            command.setVisitorRemark(addVisitsInfo.getRemark());
            command.setVisitTime(addVisitsInfo.getStartTime());
            command.setLeaveTime(addVisitsInfo.getEndTime());
            command.setArriveType(Integer.parseInt(addVisitsInfo.getVisitReason()));
            command.setReceptionistId(addVisitsInfo.getUserInformation().getStaffId());//接访人
            command.setAreaId(addVisitsInfo.getAreaId().toString());
            UserInformation userInformation = new UserInformation();
            userInformation.setStaffName(addVisitsInfo.getUserInformation().getStaffName());
            visitorInfoService.addVisitor(command, information);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
