package com.bjxczy.onepark.door.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/*
 *@Author wlw
     *@Date 2023/7/10 9:56  门禁设备 与 员工凭证的关联表
 */
@Data
@TableName("d_door_authorization")
public class DoorAuthorizationPo {
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * device_id 设备id
     */
    private String deviceId;
    /**
     * voucher_id 凭证id
     */
    private String voucherId;
    /**
     * 授权时间 save_time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date saveTime;
    /**
     * del_flag
     */
    @TableLogic(value = "0", delval = "1")
    private Integer delFlag;

    public DoorAuthorizationPo(String deviceId, String voucherId, Date saveTime) {
        this.deviceId = deviceId;
        this.voucherId = voucherId;
        this.saveTime = saveTime;
    }
}
