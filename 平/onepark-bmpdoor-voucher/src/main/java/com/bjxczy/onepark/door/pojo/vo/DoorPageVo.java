package com.bjxczy.onepark.door.pojo.vo;

import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/7/4 10:30
 */
@Data
public class DoorPageVo {
    private String id;
    /**
     * device_code 设备编码
     */
    private String deviceCode;
    private String platformType; //归属平台：字典（platformType
    /**
     * device_type 设备类型
     */
    private String deviceType;
    /**
     * 设备名 device_name
     */
    private String deviceName;
    /**
     * online_status 在线状态 0不在线  1在线
     */
    private String onlineStatus;
    /**
     * direction; 设备方向 (1-进，2-出)
     */
    private String direction;
    /**
     * staff_permission 员工权限 (1-开，2-关)
     */
    private Boolean staffPermission;
    /**
     * visit_permission 访客权限 (1-开，2-关)
     */
    private Boolean visitPermission;
    /**
     * 空间名称展示        space_name  xx/xx/xx
     */
    private String spaceName;
}
