package com.bjxczy.onepark.staffvoucher.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.staffvoucher.entity.AccessCard;
import com.bjxczy.onepark.staffvoucher.mapper.AccessCardMapper;
import com.bjxczy.onepark.staffvoucher.mapper.AccessRepository;
import com.bjxczy.onepark.staffvoucher.pojo.po.AccessCardPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author ：yubaowang
 * @date ：2022/12/27 10:05
 */
@Service
@Validated
public class AccessDomainService {

    @Resource
    private AccessCardMapper accessCardMapper;

    @Autowired
    private AccessRepository accessRepository;

    @Autowired
    private AccessQueryService accessQueryService;

    public Boolean saveAccessCard(@Valid AccessCard accessCard) {
        AccessCardPO selectOne = accessCardMapper.selectOne(new QueryWrapper<AccessCardPO>()
                .eq("staff_id",accessCard.getStaffId())
                .eq("employee_no",accessCard.getEmployeeNo())
                .eq("come_from",accessCard.getComeFrom())
                .eq("del_flag", 0));
        if (selectOne != null) {
            throw new ResultException("员工已存在门禁卡！");
        }
        if (accessQueryService.isExistsAccessCard(accessCard.getAccessCode(), null) ) {
            throw new ResultException("门禁卡号已存在！");
        }
        return accessRepository.saveModel(accessCard);
    }

    public Boolean delAccessCard(AccessCard accessCard) {
        boolean flag = false;
        Integer index = accessCardMapper.updateFlagById(accessCard.getId());
        if (index > 0) {
            flag = true;
        }
        return flag;
    }
}
