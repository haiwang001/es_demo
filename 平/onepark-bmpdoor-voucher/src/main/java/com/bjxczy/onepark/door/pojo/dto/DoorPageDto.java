package com.bjxczy.onepark.door.pojo.dto;


import com.bjxczy.onepark.staffvoucher.pojo.dto.BasePageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *@Author wlw
 *@Date 2023/7/4 9:57
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DoorPageDto extends BasePageDTO {
    /**
     * 设备名/设备编码 分页列表查询
     */
    private String mixTogether;
    /**
     * 区域id
     */
    private String areaId;
    /**
     * 楼宇id
     */
    private String buildingId;
    /**
     * 楼层id
     */
    private String spaceId;
    /**
     * 未绑定
     */
    private Boolean unbound;
}
