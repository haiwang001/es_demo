package com.bjxczy.onepark.common.event;

import java.util.List;

import org.springframework.context.ApplicationEvent;

import com.bjxczy.core.rabbitmq.event.door.AuthorityDeviceDTO;
import com.bjxczy.core.rabbitmq.event.door.RemoveStaffAuthDTO;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class RemoveAuthorityEvent extends ApplicationEvent {
    private static final long serialVersionUID = -5585591329840237963L;

    private String removeId;
    /**
     * 授权设备ID List
     */
    private List<AuthorityDeviceDTO> doorList;

    /**
     * 授权员工ID List
     */
    private List<RemoveStaffAuthDTO> staffList;

    public RemoveAuthorityEvent(Object source, String removeId, List<AuthorityDeviceDTO> doorList, List<RemoveStaffAuthDTO> staffList) {
        super(source);
        this.removeId = removeId;
        this.doorList = doorList;
        this.staffList = staffList;
    }
}
