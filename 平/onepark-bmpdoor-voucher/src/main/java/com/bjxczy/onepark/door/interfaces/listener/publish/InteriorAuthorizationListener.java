package com.bjxczy.onepark.door.interfaces.listener.publish;



import com.bjxczy.onepark.common.event.ClearInteriorAuthorizationEvent;
import com.bjxczy.onepark.common.event.CreateInteriorAuthorizationEvent;
import com.bjxczy.onepark.door.service.DoorInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/*
 *@Author wlw
 *@Date 2023/7/4 18:13
 */
@Component
public class InteriorAuthorizationListener {

    @Autowired
    private DoorInfoService service;

    /**
     * 设备授权
     *
     * @param event
     */
    @EventListener
    public void doorCreateInteriorAuthorizationEvent(CreateInteriorAuthorizationEvent event) {
        service.doorCreateInteriorAuthorization(event.getStaffId());
    }


    /**
     * 清除设备授权
     *
     * @param event
     */
    @EventListener
    public void doorClearInteriorAuthorizationEvent(ClearInteriorAuthorizationEvent event) {
        service.doorClearInteriorAuthorization(event.getStaffId());
    }
}
