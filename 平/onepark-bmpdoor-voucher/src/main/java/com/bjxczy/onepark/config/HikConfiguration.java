package com.bjxczy.onepark.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "hikvision")
public class HikConfiguration {
    private String host;
    private String appKey;
    private String appSecret;
    private String receipt;// 海康回调域名
    private String tagId;// 合作方标签  onlineNumber
    private Boolean rating;// 人脸评分开关
}
