package com.bjxczy.onepark.common.utils.img;


import com.bjxczy.onepark.common.utils.StaffUtils;


import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.http.entity.ContentType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import java.awt.*;
import java.awt.image.BufferedImage;

import java.awt.image.ColorModel;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

/*
 *@Author wlw
 *@Date 2023/5/11 9:40
 */
@Data
class ImageDTO {
    //文字内容
    private String text;
    //字体颜色和透明度
    private Color color;
    //字体和大小
    private Font font;
    //所在图片的x坐标
    private int x;
    //所在图片的y坐标
    private int y;
}

@Log4j2
public class ImgUtils {

    /**
     * main方法:
     *
     * @param args
     */
    public static void main(String[] args) {

        //=========================================自行发挥================================
        String srcImgPath = "http://192.165.1.62:9881/group1/M00/00/50/wKUBPmRYj1mAOWa5AAecu_xIPIk26.jpeg";    //源图片地址
        String tarImgPath = "C:/logs/666.jpg";   //目标图片的地址
        //==============================================================================
        //获取数据集合；
/*        ArrayList<ImageDTO> list = new ArrayList<>();
        list.add(createImageDTO("张三风",
                new Color(14, 138, 205, 255), new Font("微软雅黑", Font.PLAIN, 300), 0, 0));
        //操作图片:
        ImgUtils.writeImage(srcImgPath, tarImgPath, list);*/

        MultipartFile multipartFile = compressionByUrl(srcImgPath);
    }

    /**
     * 编辑图片,往指定位置添加文字
     *
     * @param srcImgPath    :源图片路径
     * @param targetImgPath :保存图片路径
     * @param list          :文字集合
     */
    public static void writeImage(String srcImgPath, String targetImgPath, ArrayList<ImageDTO> list) {
        FileOutputStream outImgStream = null;
        try {
            Image srcImg = ImageIO.read(StaffUtils.getFileBase64File(srcImgPath));//文件转化为图片
            int srcImgWidth = srcImg.getWidth(null);//获取图片的宽
            int srcImgHeight = srcImg.getHeight(null);//获取图片的高
            System.err.println("图片的宽 " + srcImgWidth);
            System.err.println("图片的高 " + srcImgHeight);
            //添加文字:
            BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufImg.createGraphics();
            g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
            for (ImageDTO imgDTO : list) {
                g.setColor(imgDTO.getColor());                                  //根据图片的背景设置水印颜色
                g.setFont(imgDTO.getFont());                                    //设置字体
                g.drawString(imgDTO.getText(), 510, (srcImgHeight + 396 / 2) / 2);   //画出水印
            }
            g.dispose();

            // 新地址
            outImgStream = new FileOutputStream(targetImgPath);

            ImageIO.write(bufImg, "jpg", outImgStream);
        } catch (Exception e) {
            log.error("==== 系统异常::{} ====", e);
        } finally {
            try {
                if (null != outImgStream) {
                    outImgStream.flush();
                    outImgStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 创建ImageDTO, 每一个对象,代表在该图片中要插入的一段文字内容:
     *
     * @param text  : 文本内容;
     * @param color : 字体颜色(前三位)和透明度(第4位,值越小,越透明);
     * @param font  : 字体的样式和字体大小;
     * @param x     : 当前字体在该图片位置的横坐标;
     * @param y     : 当前字体在该图片位置的纵坐标;
     * @return
     */
    private static ImageDTO createImageDTO(String text, Color color, Font font, int x, int y) {
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setText(text);
        imageDTO.setColor(color);
        imageDTO.setFont(font);
        imageDTO.setX(x);
        imageDTO.setY(y);
        return imageDTO;
    }


    /**
     * @param filePath 原图
     */
    public static MultipartFile compressionByUrl(String filePath) {
        try {
            File file = StaffUtils.getFileBase64File(filePath);
            return compression(file);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param multiFile 原图
     */
    public static MultipartFile compressionByMultipartFile(MultipartFile multiFile) {
        try {
            File file = MultipartFileToFile(multiFile);
            assert file != null;
            return compression(file);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static MultipartFile compression(File file) throws IOException {
        System.err.println("------>>" + file.length() / 1024);
        BufferedImage image = ImageIO.read(file);
        // 得到指定Format图片的writer（迭代器）
        Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpg");
        // 得到writer
        ImageWriter writer = iter.next();
        // 得到指定writer的输出参数设置(ImageWriteParam )
        ImageWriteParam iwp = writer.getDefaultWriteParam();
        // 设置可否压缩
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        // 设置压缩质量参数
        iwp.setCompressionQuality(0);
        iwp.setProgressiveMode(ImageWriteParam.MODE_DISABLED);
        ColorModel colorModel = ColorModel.getRGBdefault();
        // 指定压缩时使用的色彩模式
        iwp.setDestinationType(
                new javax.imageio.ImageTypeSpecifier(colorModel, colorModel.createCompatibleSampleModel(16, 16)));
        // 开始打包图片，写入byte[]
        // 取得内存输出流
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        IIOImage iIamge = new IIOImage(image, null, null);
        // 此处因为ImageWriter中用来接收write信息的output要求必须是ImageOutput
        // 通过ImageIo中的静态方法，得到byteArrayOutputStream的ImageOutput
        writer.setOutput(ImageIO.createImageOutputStream(byteArrayOutputStream));
        writer.write(null, iIamge, iwp);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        System.err.println("------>>" + bytes.length / 1024);
        // 字节数组 转 InputStream 流
        InputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        // InputStream 流 转  MultipartFile
        return new MockMultipartFile("file", file.getName(),
                ContentType.APPLICATION_OCTET_STREAM.toString(), byteArrayInputStream);

    }


    /**
     * @param multiFile 转换前
     * @return
     */
    public static File MultipartFileToFile(MultipartFile multiFile) {
        // 获取文件名
        String fileName = multiFile.getOriginalFilename();
        // 若须要防止生成的临时文件重复,能够在文件名后添加随机码
        try {
            assert fileName != null;
            File file = File.createTempFile(fileName, ".jpg");
            multiFile.transferTo(file);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param filePath 原图
     */
    public static MultipartFile compressionByUrl(String filePath, Float multiplier) {
        try {
            File file = StaffUtils.getFileBase64File(filePath);
            return compression(file, multiplier);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param multiFile 原图
     */
    public static MultipartFile compressionByMultipartFile(MultipartFile multiFile, Float multiplier) {
        try {
            File file = MultipartFileToFile(multiFile);
            assert file != null;
            return compression(file, multiplier);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static MultipartFile compression(File file, Float multiplier) throws IOException {
        BufferedImage image = ImageIO.read(file);
        // 得到指定Format图片的writer（迭代器）
        Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpg");
        // 得到writer
        ImageWriter writer = iter.next();
        // 得到指定writer的输出参数设置(ImageWriteParam )
        ImageWriteParam iwp = writer.getDefaultWriteParam();
        // 设置可否压缩
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        // 设置压缩质量参数
        iwp.setCompressionQuality(multiplier);
        iwp.setProgressiveMode(ImageWriteParam.MODE_DISABLED);
        ColorModel colorModel = ColorModel.getRGBdefault();
        // 指定压缩时使用的色彩模式
        iwp.setDestinationType(
                new javax.imageio.ImageTypeSpecifier(colorModel, colorModel.createCompatibleSampleModel(16, 16)));
        // 开始打包图片，写入byte[]
        // 取得内存输出流
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        IIOImage iIamge = new IIOImage(image, null, null);
        // 此处因为ImageWriter中用来接收write信息的output要求必须是ImageOutput
        // 通过ImageIo中的静态方法，得到byteArrayOutputStream的ImageOutput
        writer.setOutput(ImageIO.createImageOutputStream(byteArrayOutputStream));
        writer.write(null, iIamge, iwp);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        // 字节数组 转 InputStream 流
        InputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        // InputStream 流 转  MultipartFile
        return new MockMultipartFile("file", file.getName(),
                ContentType.APPLICATION_OCTET_STREAM.toString(), byteArrayInputStream);

    }
}
