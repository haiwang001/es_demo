package com.bjxczy.onepark.visitor.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.visitor.command.AddVisitorCommand;
import com.bjxczy.onepark.visitor.pojo.dto.VisitorInviteDto;
import com.bjxczy.onepark.visitor.pojo.vo.VisitorInvitePageListVo;

import javax.servlet.http.HttpServletResponse;


public interface VisitorInfoService {

    Object addVisitor(AddVisitorCommand com, UserInformation sysUser);

    IPage<VisitorInvitePageListVo> VisitorInvitePageList(VisitorInviteDto dto, UserInformation sysUser);

    Object delVisitorInfo(String id);

    void VisitorInfoExportExcel(HttpServletResponse response, VisitorInviteDto dto);
}

