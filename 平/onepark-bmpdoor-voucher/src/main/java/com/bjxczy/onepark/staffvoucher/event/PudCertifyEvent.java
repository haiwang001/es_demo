package com.bjxczy.onepark.staffvoucher.event;


import com.bjxczy.onepark.staffvoucher.entity.AccessCard;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEvent;


/*
 *@Date 2023/1/18 17:05
 */
@Log4j2
@Data
public class PudCertifyEvent extends ApplicationEvent {
    private static final long serialVersionUID = -5119661358075529542L;
    private com.bjxczy.onepark.staffvoucher.entity.AccessCard AccessCard;

    public PudCertifyEvent(Object source, AccessCard accessCard) {
        super(source);
        AccessCard = accessCard;
        log.info("-----------------------------------进员工凭证修改事件-----------------------------------------");
    }
}
