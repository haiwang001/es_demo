package com.bjxczy.onepark.staffvoucher.pojo.vo;


import lombok.Data;

@Data
public class CameraStatisticVo {
    private Integer cameraTotalCount;
    private Integer ortherType;
    private Integer cameraBallCount;
    private Integer cameraBarCount;
    private Integer cameraHalfBallCount;
    private Integer cameraOnlineCount;
    private Integer cameraOfflineCount;
    private Integer otherStatusCount;
    private Integer supportPTZ;


}
