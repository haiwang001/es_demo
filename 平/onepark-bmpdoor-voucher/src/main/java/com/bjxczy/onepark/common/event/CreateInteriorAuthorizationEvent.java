package com.bjxczy.onepark.common.event;


import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/*
增加凭证 添加权限
 *@Author wlw
 *@Date 2023/7/4 18:17
 */
@Getter
@ToString
public class CreateInteriorAuthorizationEvent extends ApplicationEvent {
    private static final long serialVersionUID = -2157480294059569771L;
    private Integer staffId;

    public CreateInteriorAuthorizationEvent(Object source, Integer staffId) {
        super(source);
        this.staffId = staffId;
    }
}
