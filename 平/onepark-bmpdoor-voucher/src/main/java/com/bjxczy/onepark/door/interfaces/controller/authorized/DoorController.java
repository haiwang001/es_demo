package com.bjxczy.onepark.door.interfaces.controller.authorized;


import com.bjxczy.core.web.api.ApiResourceController;
import com.bjxczy.onepark.common.annotation.LoginUser;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.resp.R;
import com.bjxczy.onepark.door.command.*;
import com.bjxczy.onepark.door.pojo.dto.DoorPageDto;
import com.bjxczy.onepark.door.pojo.dto.LookPageDto;
import com.bjxczy.onepark.door.service.DoorInfoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;


@Log4j2
@ApiResourceController
@RequestMapping("/door")
public class DoorController {
    @Resource
    private DoorInfoService service;

    /**
     * 授权
     */
    @PostMapping(value = "/authorization", name = "pc-门禁授权")
    public R<?> authorization(@RequestBody AuthorizationCommand com,
                              @LoginUser(isFull = true) UserInformation sysUser) {
        return R.success(service.authorization(com, sysUser));
    }

    @GetMapping(value = "/page", name = "pc-门禁设备")
    public R<?> pageList(DoorPageDto dto) {
        return R.success(service.pageList(dto));
    }

    @GetMapping(value = "/lookPage", name = "pc-查看授权")
    public R<?> LookPage(LookPageDto dto) {
        return R.success(service.LookPage(dto));
    }

    @GetMapping(value = "/lookExport", name = "pc-查看授权导出")
    public void lookExport(HttpServletResponse response, LookPageDto dto) {
        service.lookExport(response, dto);
    }

    @GetMapping(value = "/overview", name = "pc-数据概览-门禁概览")
    public R<?> overview(String id) {
        return R.success(service.overview(id));
    }

    @DeleteMapping(value = "/clearAuthorization", name = "pc-取消授权")
    public R<?> cancelAuthorization(@RequestBody DeleteAuthorizationCommand com) throws Exception {
        return R.success(service.clearAuthorization(com));
    }

    /**
     * 修改方向
     */
    @PutMapping(value = "/putDoorDirection", name = "pc-修改设备方向")
    public R<?> putDoorDirection(@RequestBody PutDoorDirectionCommand com) {
        return R.success(service.putDoorDirection(com));
    }

    /**
     * 设置访客权限
     */
    @PutMapping(value = "/setVisitPermission", name = "pc-修改访客权限")
    public R<?> setVisitPermission(@RequestBody PutDoorVisitorCommand com) throws Exception {
        return R.success(service.setVisitPermission(com));
    }

    /**
     * 设置员工权限
     */
    @PutMapping(value = "/setStaffPermission", name = "pc-修改员工权限")
    public R<?> setStaffPermission(@RequestBody PutDoorStaffCommand com) throws Exception {
        return R.success(service.setStaffPermission(com));
    }
}
