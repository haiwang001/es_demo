package com.bjxczy.onepark.staffvoucher.entity;

import com.bjxczy.core.web.base.AbstractAggregateRoot;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @author wangxiling
 * @date 2023/6/26 14:56
 * @description  门禁卡
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AccessCard extends AbstractAggregateRoot<AccessCard> {
    /**
     * ID
     */
    @Id
    private Integer id;
    /**
     * 员工ID
     */
    private Integer staffId;
    /**
     * 员工编号
     */
    private String employeeNo;
    /**
     * 员工姓名
     */
    @NotBlank(message = "员工姓名不能为空！")
    private String staffName;
    /**
     * 电话
     */
    private String mobile;
    /**
     * 部门ID
     */
    private Integer deptId;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 职级ID
     */
    private Integer rankId;
    /**
     * 职级名称
     */
    private String rankName;
    /**
     * 人员类型
     */
    private Integer comeFrom;
    /**
     * 门禁卡号
     */
    @NotBlank(message = "门禁卡号不能为空！")
    private String accessCode;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    /**
     * 操作人
     */
    private String operator;
    /**
     * 删除标记
     */
    private Integer delFlag;
}
