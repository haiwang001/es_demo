package com.bjxczy.onepark.door.interfaces.feign;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjxczy.onepark.door.entity.DoorInfoPo;
import com.bjxczy.onepark.door.mapper.DoorInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bjxczy.core.feign.client.door.BaseDoorDeviceFeignClient;
import com.bjxczy.onepark.common.model.door.DoorDeviceInformation;

import java.util.List;


@Controller
@ResponseBody
public class DoorDeviceFeignClient implements BaseDoorDeviceFeignClient {

	@Autowired
	private DoorInfoMapper mapper;

	@Override
	public DoorDeviceInformation getDoorInformation(String deviceCode) {
		DoorInfoPo device = mapper.selectOne(new QueryWrapper<DoorInfoPo>().eq("device_code", deviceCode));
		DoorDeviceInformation info = null;
		if (device != null) {
			info = new DoorDeviceInformation();
			info.setDeviceCode(device.getDeviceCode());
			info.setDeviceType(device.getDeviceType());
			info.setDeviceName(device.getDeviceName());
			info.setDirection(device.getDirection());
			info.setAreaId(String.valueOf(device.getAreaId()));
			info.setTenantId(device.getTenantId());
			info.setSpaceId(device.getSpaceId());
			info.setLocation(device.getSpaceName());
		}
		return info;
	}

	@Override
	public List<String> getDeviceCodeInformation(String inOut) {
		return mapper.getGoOutDeviceList(inOut);
	}

}
