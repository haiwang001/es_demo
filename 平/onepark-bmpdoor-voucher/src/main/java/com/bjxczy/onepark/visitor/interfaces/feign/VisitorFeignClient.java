package com.bjxczy.onepark.visitor.interfaces.feign;


import com.bjxczy.core.feign.client.visitor.BaseVisitorFeignClient;
import com.bjxczy.onepark.common.model.visitor.VisitorDtoInformation;
import com.bjxczy.onepark.common.model.visitor.VisitorInformation;
import com.bjxczy.onepark.visitor.mapper.VisitorInviteInfoMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/*
 *@Author wlw
 *@Date 2023/6/19 17:43
 */
@Controller
@ResponseBody
public class VisitorFeignClient implements BaseVisitorFeignClient {

    @Resource
    private VisitorInviteInfoMapper visitorInviteInfoMapper;

    @Override
    public VisitorInformation selectVisitorByMobile(String mobile) {
        return visitorInviteInfoMapper.selectVisitorByMobile(mobile);
    }

    /**
     * @param day 今日
     * @return 需要到访的访客 li 通过区域筛选
     */
    @Override
    public List<VisitorInformation> selectVisitorByVisitToday(String day,String id) {
        return visitorInviteInfoMapper.selectVisitorByVisitToday(day,id);
    }

    @Override
    public List<VisitorDtoInformation> selectAll() {
        return visitorInviteInfoMapper.getVisitorAll();
    }
}