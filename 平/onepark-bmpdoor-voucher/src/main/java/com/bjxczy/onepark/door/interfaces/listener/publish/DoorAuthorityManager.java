package com.bjxczy.onepark.door.interfaces.listener.publish;

import com.bjxczy.core.rabbitmq.event.door.AuthorityDeviceDTO;
import com.bjxczy.core.rabbitmq.event.door.RemoveAuthorityPayload;
import com.bjxczy.core.rabbitmq.event.door.UpdateAuthorityPayload;
import com.bjxczy.core.rabbitmq.supports.DoorMutualBaseListener;
import com.bjxczy.onepark.common.event.RemoveAuthorityEvent;
import com.bjxczy.onepark.common.event.UpdateAuthorityEvent;
import com.bjxczy.onepark.door.interfaces.listener.async.DeleteDoorAuthorityRabbitAsyncHandle;
import com.bjxczy.onepark.door.interfaces.listener.async.DoorAuthorityRabbitAsyncHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DoorAuthorityManager extends DoorMutualBaseListener {
    /**
     * 授权
     */
    @Autowired
    private DoorAuthorityRabbitAsyncHandle asyncHandler;
    /**
     * 取消授权
     */
    @Autowired
    private DeleteDoorAuthorityRabbitAsyncHandle delasyncHandler;
    /**
     * 发布更新设备授权事件 DoorAuthority
     *
     * @param event
     */
    @EventListener
    public void updateAuthorityEvent(UpdateAuthorityEvent event) {
        List<AuthorityDeviceDTO> flist = event.getDoorList();
        if (flist.isEmpty()) {
            return;
        }
        UpdateAuthorityPayload payload = new UpdateAuthorityPayload(flist, event.getStaffList());
        asyncHandler.sendMessage(event.getDoorAuthLogId(), payload);
    }

    /**
     * 发布删除设备授权事件
     *
     * @param event
     */
    @EventListener
    public void removeAuthorityEvent(RemoveAuthorityEvent event) {
        List<AuthorityDeviceDTO> flist = event.getDoorList();
        if (flist.isEmpty()) {
            return;
        }
        RemoveAuthorityPayload payload = new RemoveAuthorityPayload(flist, event.getStaffList());
        delasyncHandler.sendMessage(event.getRemoveId(), payload);
    }

}
