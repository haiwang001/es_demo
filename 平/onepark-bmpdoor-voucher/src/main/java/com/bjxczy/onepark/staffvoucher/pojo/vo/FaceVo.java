package com.bjxczy.onepark.staffvoucher.pojo.vo;


import lombok.Data;

import javax.validation.constraints.NotNull;

/*
 *@ClassName FaceVo
 *@Author 温良伟
 *@Date 2023/3/20 14:30
 *@Version 1.0
 */
@Data
public class FaceVo {

    /*
     * 员工编号
     */
    private String misCode;
    private String employeeNo;

    /**
     * 员工姓名
     */
    private String staffName;

    /**
     * 身份证号
     */
    @NotNull
    private String idCard;

    @NotNull
    private String faceImg;
}
