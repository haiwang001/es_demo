package com.bjxczy.onepark.staffvoucher.service.impl;


import com.bjxczy.onepark.common.client.BmpQueryStaffServer;
import com.bjxczy.onepark.common.exception.ResultException;
import com.bjxczy.onepark.common.model.organization.DeptNameInfo;
import com.bjxczy.onepark.common.model.organization.StaffInfoFine;
import com.bjxczy.onepark.common.model.user.UserInformation;
import com.bjxczy.onepark.common.utils.PageUtils;
import com.bjxczy.onepark.common.utils.StrUtils;
import com.bjxczy.onepark.staffvoucher.entity.StaffNode;
import com.bjxczy.onepark.staffvoucher.mapper.StaffNodeMapper;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffAddDto;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffDelDto;
import com.bjxczy.onepark.staffvoucher.pojo.dto.StaffQueryListDto;
import com.bjxczy.onepark.staffvoucher.service.StaffNodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class StaffNodeServiceImpl implements StaffNodeService {

    @Autowired
    private StaffNodeMapper staffNodeMapper;

    @Autowired
    private BmpQueryStaffServer bmpQueryStaffServer;


    @Override
    public Object getGroupNodeList(StaffQueryListDto dto) throws Exception {
        return PageUtils.Page(dto, staffNodeMapper.queryList(dto));
    }

    @Override
    public int DelGroupNodeStaff(StaffDelDto dto) throws Exception {
        int i = -1;
        for (Integer el : dto.getStaffIds()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("group_node_id", dto.getGroupId());
            map.put("id", el);
            i = staffNodeMapper.deleteByMap(map);
        }
        return i;
    }

    @Override
    public int addGroupNodeStaff(StaffAddDto dto, UserInformation sysUser) throws Exception {
        List<Integer> list = dto.getStaffList();
        int i = -1;
        for (Integer integer : list) {
            StaffInfoFine staffById = bmpQueryStaffServer.getStaffById(integer);
            if (staffById == null){
                throw new ResultException("找不到员工信息 添加失败");
            }
            if (whetherToRepeat(dto, staffById)){
                throw new ResultException("该分组员工信息重复 添加失败");
            }
            i = addGroupNode(dto, staffById,sysUser);
        }
        return i;
    }

    /**
     * 是否重复
     *
     * @param dto
     * @param staffById
     * @return
     */
    private boolean whetherToRepeat(StaffAddDto dto, StaffInfoFine staffById) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("employee_no", staffById.getEmployeeNo());
        map.put("staff_name", staffById.getFldName());
        map.put("group_node_id", dto.getGroupId());
        map.put("del_flag", 0);
        List<StaffNode> staffNodes = staffNodeMapper.selectByMap(map);
        return staffNodes.size() > 0;
    }

    /**
     * 添加节点
     *
     * @param dto
     * @param staff
     */
    private int addGroupNode(StaffAddDto dto, StaffInfoFine staff,UserInformation sysUser) {
        StaffNode Node = new StaffNode();
        Node.setCreateTime(new Date());
        Node.setDelFlag(0);
        Node.setOperator(dto.getOperator());
        Node.setGroupNodeId(dto.getGroupId());
        Node.setMobile(staff.getMobile());
        Node.setEmployeeNo(staff.getEmployeeNo());
        Node.setStaffName(staff.getFldName());
        Node.setEmployeeId(staff.getId().toString());
        List<DeptNameInfo> deptNameInfos = StrUtils.DeptNameInfo(staff.getDeptNames());
        Node.setDeptName(deptNameInfos.get(0).getDeptName());
        Node.setOperator(sysUser.getStaffName());
        return staffNodeMapper.insert(Node);
    }
}
