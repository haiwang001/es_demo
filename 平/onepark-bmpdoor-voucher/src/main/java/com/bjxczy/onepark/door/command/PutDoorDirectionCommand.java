package com.bjxczy.onepark.door.command;


import lombok.Data;

/*
 *@Author wlw
 *@Date 2023/7/6 15:53
 */
@Data
public class PutDoorDirectionCommand {
    private String deviceId;// 设备id
    private Integer direction;// 设备方向
}
