# 移动端 凭证 -API开发文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录 |
| --------- | ------ | ---------- | ------------ |
| v0.0.1    | 温良伟 | 2023.04.16 | 初始化文档   |


## 一、表结构

###  授权记录表  (authorization_record) 

| 数据库字段                 |   代码字段             | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ----------------------| ----------------------  | ---------------------- | ---------------- |
| recordId                 |  recordId             | varchar(255)            | 是                     | 主键ID 授权记录id          |
| description              |  description          | varchar(255)            | 是                     | 授权描述          |
| guard                    | guard	               | varchar(255)            | 是                     | 授权成功的设备名展示    |
| staff                    | staff	               | varchar(255)            | 是                     | 授权成功的员工名展示  |
| createTime               | createTime	           | varchar(255)            | 是                     | 授权描述      |
| operator                 | operator	           | varchar(255)            | 是                     | 操作人      |
| operator                 | operator	           | varchar(255)            | 是                     | 操作人      |


###  授权的员工信息  (authorization_staff) 

| 数据库字段                 |   代码字段             | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ----------------------| ----------------------  | ---------------------- | ---------------- |
| authId                   |  authId               | long                    | 是                     | 主键ID             |
| guardCode                |  guardCode            | varchar(255)            | 是                     | 绑定的设备编码       |
| staffCode                | staffCode	           | varchar(255)            | 是                     | 员工编码             |
| staffName                | staffName	           | varchar(255)            | 是                     | 员工名称            |
| staffId                  | staffId	           | number                  | 是                     | 员工id             |
| staffMobile              | staffMobile	       | varchar(255)            | 是                     | 员工手机号              |
| staffDepartmentId        | staffDepartmentId	   | number                  | 是                     | 部门id              |
| staffDepartment          | staffDepartment	   | varchar(255)            | 是                     | 部门展示              |
| comeFrom                 | comeFrom	           | number                  | 是                     | 内部1  外部2          |
| guardName                | guardName	           | varchar(255)            | 是                     | 设备名          |
| guardType                | guardType	           | varchar(255)            | 是                     | 设备类型         |
| guardDirection           | guardDirection	       | number                  | 是                     | 设备方向         |
| location                 | location     	       | varchar(255)            | 是                     | 空间展示         |
| createTime               | createTime     	   | varchar(255)            | 是                     | 授权时间         |
| operator                 | operator        	   | varchar(255)            | 是                     | 操作人         |
| delFlag                   | delFlag        	   | number                  | 是                     | 删除         |



###  取消授权记录 弃用 (cancel_record)

 
###  门禁控制器子节点  (e_guard_door) 

| 数据库字段                 |   代码字段              | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ----------------------| ----------------------  | ---------------------- | ---------------- |
| entranceGuard            |  entranceGuard        | varchar(255)            | 是                     | 门禁点编码            |
| entranceGuardName        |  entranceGuardName    | varchar(255)            | 是                     | 门禁点名字      |
| guardCode                |  guardCode	           | varchar(255)            | 是                     | 门禁控制器编码           |


###  门禁控制器子节点  (e_guard_door) 

| 数据库字段                 |   代码字段              | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ----------------------| ----------------------  | ---------------------- | ---------------- |
| entranceGuard            |  entranceGuard        | varchar(255)            | 是                     | 门禁点编码            |
| entranceGuardName        |  entranceGuardName    | varchar(255)            | 是                     | 门禁点名字      |
| guardCode                |  guardCode	           | varchar(255)            | 是                     | 门禁控制器编码           |

###  门禁控制器设备  (guard_info) 

| 数据库字段                 |   代码字段              | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ----------------------| ----------------------  | ---------------------- | ---------------- |
| guardId                  |  guardId              | long                    | 是                      |  设备id            |
| guardType                |  guardType            | varchar(255)            | 是                     | 设备类型  海康设备 汉王设备      |
| location                 |  location	           | varchar(255)            | 否                     | 设备位置展示           |
| guardName                |  guardName	           | varchar(255)            | 是                     | 设备名称           |
| primitiveName            |  primitiveName	       | varchar(255)            | 是                     | 设备原始名称           |
| guardCode                |  guardCode	           | varchar(255)            | 是                     | 设备编码           |
| guardLocation            |  guardLocation	       | varchar(255)            | 否                     | 设备位置  弃用           |
| isOnLine                 |  isOnLine	           | number                  | 是                     | 在线状态 0不在线  1在线           |
| guardBrand               |  guardBrand	       | varchar(255)            | 是                     | 品牌           |
| guardDirection           |  guardDirection	   |number                   | 是                     | 设备方向 (1-进，2-出)         |
| devicePosition           |  devicePosition	   |varchar(255)             | 否                     | 设备位置描述         |
| category                 |  category	           |varchar(255)             | 否                     | 设备类型描述         |
| staffPermission          |  staffPermission	   |number                   | 是                     |  员工权限 (1-开，2-关)      |
| visitorPermission        |  visitorPermission	   |number                   | 是                     |  访客权限 (1-开，2-关)      |
| areaId                   |  areaId	           |number                   | 否                     |  区域id      |
| buildingId               |  buildingId	       |varchar(255)             | 否                     |  楼id      |
| spaceId                  |  spaceId	           |varchar(255)             | 否                     |  楼层id      |
| createTime               |  createTime	       |varchar(255)             | 是                     |  操作时间      |
| operator                 |  operator	           |varchar(255)             | 是                     |  操作人      |
| tcpPort                  |  tcpPort	           |number                   | 否                     |  汉王连接端口      |
| udpPort                  |  udpPort	           |number                   | 否                     |  汉王连接端口      |
| heartbeatDate            |  heartbeatDate	       |date                     | 否                     |  心跳时间      |
| equipmentIp               |  equipmentIp	       |varchar(255)             | 否                     |  设备ip      |

###  员工卡  (access_card) 

| 数据库字段                 |   代码字段              | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ----------------------| ----------------------  | ---------------------- | ---------------- |
| id                       |  id                  | Integer                    | 是                      |  卡id            |
| fldName                  |  fldName             | String                     | 是                      |  员工名称           |
| mobile                   |  mobile              | String                     | 是                      |  员工手机号           |
| uuid                     |  uuid                | String                     | 是                      |  组织架构uuid           |
| gender                   |  gender              | String                     | 是                      |  组织架构员工性别            |
| email                    |  email               | String                     | 是                      |  组织架构邮件            |
| employeeNo               |  employeeNo          | String                     | 是                      |  组织架构员工编号            |
| identityCard             |  identityCard        | String                     | 是                      |  组织架构员工证件号            |
| hasAccount               |  hasAccount          | Integer                    | 是                      |  是否有账号            |
| organizationId           |  organizationId      | Integer                    | 是                      |  组织id            |
| positionId               |  positionId          | Integer                    | 是                      |  岗位id            |
| rankId                   |  rankId              | Integer                    | 是                      |  职级id            |
| remark                   |  remark              | String                     | 是                      |  备注            |
| accessCode               |  accessCode          | String                     | 是                      |  员工卡号            |
| accessCardType           |  accessCardType      | String                     | 是                      |  员工卡类型            |
| deptName                 |  deptName            | String                     | 是                      |  部门展示            |


###  访客临时卡  (access_temp_card)  弃用
###  设备身份校验  (device_auth_link)   弃用

###  授权记录详情  (every_time_staff)   
| 数据库字段                 |   代码字段              | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ----------------------| ----------------------  | ---------------------- | ---------------- |
| id                       |  id                  | Integer                    | 是                      |  主键            |
| recordId                 |  recordId            | String                     | 是                      |  授权记录id            |
| ip                       |  ip                  | String                     | 否                      |  设备ip            |
| staffCode                |  staffCode            | String                    | 是                      |  员工编码            |
| staffName                |  staffName            | String                    | 是                      |  员工名称            |
| devName                  |  devName              | String                    | 否                      |  设备名            |
| state                    |  state                | String                    | 是                      |  授权状态  fail /success            |
| stateLabel               |  stateLabel           | String                    | 是                      |  授权状态描述           |

###  员工凭证管理  (evidence_record)   
| 数据库字段                 |   代码字段              | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ---------------------- | ----------------------   | ---------------------- | ---------------- |
| id                       |  id                    | Integer                  | 是                      |  主键            |
| employeeNo               |  employeeNo            | String                   | 是                      |  员工编号            |
| fldName                  |  fldName               | String                   | 是                      |  员工名称            |
| mobile                   |  mobile                | String                   | 是                      |  员工手机号            |
| deptName                 |  deptName              | String                   | 是                      |  部门名称            |
| identityCard             |  identityCard          | String                   | 否                      |  证件号            |
| voucherNum               |  voucherNum            | Integer                  | 是                      |  当前凭证数            |
| accessCode               |  accessCode            | String                   | 否                      |  门禁卡号            |
| createTime               |  createTime            | String                   | 是                      |  创建时间            |
| deptId                   |  deptId                | String                   | 是                      |  部门id            |
| img                      |  img                   | String                   | 否                      |  人照片            |
| rankId                   |  rankId                | Integer                  | 否                      |  职级id            |
| rankId                   |  rankId                | Integer                  | 否                      |  职级id            |


###  员工分组  (staff_group)   
| 数据库字段                 |   代码字段              | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ---------------------- | ----------------------   | ---------------------- | ---------------- |
| id                       |  id                    | Integer                  | 是                      |  主键            |
| name                     |  name                  | String                   | 是                      |  分组名称            |
| fatherId                 |  fatherId              | Integer                  | 是                      |  父id            |
| operator                 |  operator              | String                   | 是                      |  操作人            |
| createTime               |  createTime            | String                   | 是                      |  创建时间            |
| delFlag                  |  delFlag               | Integer                  | 是                      |  删除标记 默认0            |


###  分组下的员工节点  (staff_Node)  

| 数据库字段                 |   代码字段              | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ---------------------- | ----------------------   | ---------------------- | ---------------- |
| id                       |  id                    | Integer                  | 是                      |  主键            |
| groupNodeId              |  groupNodeId           | String                   | 是                      |  分组节点id            |
| employeeNo               |  employeeNo            | String                   | 是                      |  员工编号            |
| employeeId               |  employeeId            | Integer                  | 是                      |  员工id            |
| staffName                |  staffName             | String                   | 是                      |  员工名称            |
| mobile                   |  mobile                | String                   | 是                      |  员工手机号            |
| deptName                 |  deptName              | String                   | 是                      |  部门名称      a/b/c      |
| operator                 |  operator              | String                   | 是                      |  操作人         |
| createTime               |  createTime            | String                   | 是                      |  创建时间         |
| delFlag                  |  delFlag               | String                   | 是                      |  删除标记         |

###  访客  (visitor_info)  

| 数据库字段                 |   代码字段              | 字段类型                 | 是否必填                | 描述              | 
| ----------------------   | ---------------------- | ----------------------   | ---------------------- | ---------------- |
| id                       |  id                    | Integer                  | 是                      |  主键            |
| operator                |  operator               | String                   | 是                      |  操作人            |
| staffId                 |  staffId                | String                   | 是                      |  接访人id            |
| staffName               |  staffName              | String                   | 是                      |  接访人姓名            |
| staffMobile             |  staffMobile            | String                   | 是                      |  接访人手机号           |
| staffBranch             |  staffBranch            | String                   | 是                      |  接访人部门           |
| staffBranch             |  staffBranch            | String                   | 是                      |  接访人部门           |
| visitorName             |  visitorName            | String                   | 是                      |  访客姓名           |
| idNumber                |  idNumber               | String                   | 是                      |  访客证件号           |
| cardType                |  cardType               | String                   | 是                      |  证件类型           |
| telephone               |  telephone              | String                   | 是                      |  访客手机号           |
| visitorPhoto            |  visitorPhoto           | String                   | 是                      |  访客照片           |
| arriveType              |  arriveType             | Integer                  | 是                      |  访问事由      0：入职，1：面试，2：送货，3：开会，4：出差，5：施工，6：其他原因      |
| planStartTime           |  planStartTime          | String                   | 是                      |  到访时间
| planEndTime             |  planEndTime            | String                   | 是                      |  离开时间
| arriveStatus            |  arriveStatus           | Integer                  | 是                      |  到访状态  Y 1:待到访，2:已迟到 ，3:已到访 ，4:已取消 ，5:已结束
| updater                 |  updater                | String                   | 是                      |  操作人
| ramark                  |  ramark                 | String                   | 是                      |  备注
| createTime              |  createTime             | date                     | 是                      |  创建时间
| updateTime              |  updateTime             | date                     | 是                      |  更新时间
| delFlag                 |  delFlag                | Integer                  | 是                      |  删除
| order_id                |  orderId                | String                   | 是                      |  访客在海康预约成功得到的id  order_id
| areaId                  |  areaId                 | String                   | 是                      |  访问预园区  
| hikCode                 |  hikCode                | String                   | 是                      |  海康的状态码   hik_code   0 成功  -1 失败  
| hikMsg                  |  hikMsg                 | String                   | 是                      |  授权海康描述
| hikMsg                  |  hikMsg                 | String                   | 是                      |  授权海康描述
| signTime                |  signTime               | date                     | 是                      |  签到时间      弃用
| companyName             |  companyName            | String                   | 是                      |  访客公司名称   弃用
| visitorType             |  visitorType            | String                   | 是                      |  访客类型       弃用
| accessCardNo            |  accessCardNo           | String                   | 是                      |  访客门禁卡     弃用
| respondentPn            |  respondentPn           | String                   | 是                      |  被访人手机号码  弃用
| plateNumber             |  plateNumber            | String                   | 是                      |  车牌号         弃用
| respondentId            |  respondentId           | String                   | 是                      |  被访人id       弃用
| prevention              |  prevention             | String                   | 是                      |  防疫要求        弃用
| floorId                 |  floorId                | String                   | 是                      |  楼层id        弃用
| floorName               |  floorName              | String                   | 是                      |  楼层名称       弃用
| tenantId                |  tenantId               | String                   | 是                      |  园区ID        弃用
## 接口

### 1、  移动端凭证

#### 1.1、 我的凭证 

| URL    | http://host:port/face/myCertificate |
| ------ | --------------------------------- |
| Method | get                               |

delMyCertificate
返回

| 参数           | 类型   | 说明     |    
| ------------- | ------ | -------- |
| gender       | string |  性别     |      
| faceImg       | string |  图像路径     |     
| idCard       | string |  证件号     |     
| idCardType       | string |  证件类型     |     
| accessCard       | string |  门禁卡号     |     


 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"gender": "男",
		"faceImg": "http://192.165.1.62:9881/group1/M00/00/2D/wKUBPmQ74_aAOur1AAPS9bvxeIo47.jpeg",
		"idCard": "130429199608017332",
		"idCardType": "身份证",
		"accessCard": null
	},
	"success": true
}
 ```

#### 1.2、  删除我的凭证 

| URL    | http://host:port/face/delMyCertificate |
| ------ | --------------------------------- |
| Method | del                               |

