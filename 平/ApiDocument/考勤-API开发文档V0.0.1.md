# 考勤 -API开发文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录 |
| --------- | ------ | ---------- | ------------ |
| v0.0.1    | 温良伟 | 2023.03.30 | 初始化文档   |

## 接口

### 1、  考勤

#### 1.1、 增加考勤规则模板 

| URL    | http://127.0.0.1:8009/OverView/guardOverview |
| ------ | --------------------------------- |
| Method | post                               |

 请求参数：

| 参数          | 类型   | 说明     |
| ------------- | ------ | -------- |
| rulesName        | string |  规则名称 |
| officeHours        | string |  起始早上打卡时间 |
| offDutyTime        | string |  下班时间 |
| weeks        | arr |  周出勤安排 |
| mark        | int  |  每周的id |
| display        | string  |  周展示 |
| isCheck        | boo  |  是否勾选 |
| ids        | arr  |  员工id |
| operator        | string  |  操作人 |


 ```json
{
    "rulesName":"测试研发部考勤规则",
    "officeHours":"09:00",
    "offDutyTime":"18:00", 
    "weeks":[
        {
                "mark":1,
                "display":"周一",
                "isCheck":true
        },
        {
                "mark":2,
                "display":"周二",
                "isCheck":true
        },
         {
                "mark":3,
                "display":"周三",
                "isCheck":true
        },
         {
                "mark":4,
                "display":"周四",
                "isCheck":true
        },
         {
                "mark":5,
                "display":"周五",
                "isCheck":true
        },
         {
                "mark":6,
                "display":"周六",
                "isCheck":false
        },
         {
                "mark":7,
                "display":"周日",
                "isCheck":false
        }
    ],
    "ids":[13156],
    "operator":"wlw"
}
 ```

返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| data          | number |     1 新增成功  其他数字失败     |
| exception       | exception |     异常      |
| message       | string |     信息      |
| success       | boo |     响应返回状态      |

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1,
	"success": true
}
 ```
 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"msg": "考勤名称相同 考勤添加失败",
		"code": 500
	},
	"success": true
}
 ```
 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"msg": "考勤员工已经在其他考勤规则中 考勤添加失败",
		"code": 500
	},
	"success": true
}
 ```

#### 1.2、 考勤规则列表查询 

| URL    | http://127.0.0.1:8009/attendanceRules/list |
| ------ | --------------------------------- |
| Method | get                               |

返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| pageNum          | number |     1      |
| pageSize       | number |     进入的人数      |
| rulesName       | number |     模板名称模糊查询      |


```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"recordsCount": 1,
		"pages": 1,
		"records": [
			{
				"rulesId": "20ce3859-359a-4287-b89d-27b13a50d61e",
				"peopleNumber": 1,
				"rulesName": "测试研发部考勤规则",
				"officeHours": "09:00",
				"offDutyTime": "18:00",
				"weeks": [
					{
						"rulesId": "20ce3859-359a-4287-b89d-27b13a50d61e",
						"mark": 1,
						"display": "周一",
						"isCheck": true
					},
					{
						"rulesId": "20ce3859-359a-4287-b89d-27b13a50d61e",
						"mark": 2,
						"display": "周二",
						"isCheck": true
					},
					{
						"rulesId": "20ce3859-359a-4287-b89d-27b13a50d61e",
						"mark": 3,
						"display": "周三",
						"isCheck": true
					},
					{
						"rulesId": "20ce3859-359a-4287-b89d-27b13a50d61e",
						"mark": 4,
						"display": "周四",
						"isCheck": true
					},
					{
						"rulesId": "20ce3859-359a-4287-b89d-27b13a50d61e",
						"mark": 5,
						"display": "周五",
						"isCheck": true
					},
					{
						"rulesId": "20ce3859-359a-4287-b89d-27b13a50d61e",
						"mark": 6,
						"display": "周六",
						"isCheck": false
					},
					{
						"rulesId": "20ce3859-359a-4287-b89d-27b13a50d61e",
						"mark": 7,
						"display": "周日",
						"isCheck": false
					}
				],
				"weeksLabel": "周一 - 周二 - 周三 - 周四 - 周五",
				"officeHoursLabel": "09:00 - 18:00",
				"ids": null,
				"operator": "wlw",
				"createTime": "2023-03-30 14:47:46",
				"delFlag": 0
			}
		],
		"currentRecordsCount": 1,
		"pageSize": 10,
		"pageNum": 1,
		"navigatePageNums": [
			1
		]
	},
	"success": true
}
```

#### 1.3、 考勤规则更新 

| URL    | http://127.0.0.1:8009/attendanceRules/update |
| ------ | --------------------------------- |
| Method | put                               |



返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| rulesId        | string |  考勤规则id |
| rulesName        | string |  规则名称 |
| officeHours        | string |  起始早上打卡时间 |
| offDutyTime        | string |  下班时间 |
| weeks        | arr |  周出勤安排 |
| mark        | int  |  每周的id |
| display        | string  |  周展示 |
| isCheck        | boo  |  是否勾选 |
| ids        | arr  |  员工id |
| operator        | string  |  操作人 |

```json
{
        "rulesId": "20ce3859-359a-4287-b89d-27b13a50d61e",
        "rulesName": "测试研发部考勤规则",
        "officeHours": "09:00",
        "offDutyTime": "17:00",
        "weeks": [
                {
                        "mark": 1,
                        "display": "周一",
                        "isCheck": true
                },
                {
                        "mark": 2,
                        "display": "周二",
                        "isCheck": true
                },
                {
                        "mark": 3,
                        "display": "周三",
                        "isCheck": true
                },
                {
                        "mark": 4,
                        "display": "周四",
                        "isCheck": true
                },
                {
                        "mark": 5,
                        "display": "周五",
                        "isCheck": true
                },
                {
                        "mark": 6,
                        "display": "周六",
                        "isCheck": true
                },
                {
                        "mark": 7,
                        "display": "周日",
                        "isCheck": true
                }
        ],
        "ids": [
                13159,1366,13555
        ],
        "operator": "wlw"
}
```

#### 1.4、 删除考勤模板

| URL    | http://127.0.0.1:8009/attendanceRules/delete/考勤模板id |
| ------ | --------------------------------- |
| Method | delete                               |


```json5
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": 1,
	"success": true
}
```


### 2、  考勤汇总

#### 1.1、 查询考勤列表 

| URL    | http://127.0.0.1:8009/attendanceSummary/list |
| ------ | --------------------------------- |
| Method | get                               |

请求的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| deptName        | string |  员工部门名 |
| rulesName        | string |  规则名称 |
| month          | string |  月份 |


返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| name        | string |  员工名 |
| employeeMark        | string |  员工标记 点查看传递 |
| deptName          | string |  部门名 |
| rulesName          | string |  考勤规则名 |

```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"recordsCount": 2,
		"pages": 1,
		"records": [
			{
				"name": "李军",
				"employeeMark": "1",
				"deptName": "研发2",
				"rulesName": "夏日考勤"
			},
			{
				"name": "张三",
				"employeeMark": "2",
				"deptName": "测试3",
				"rulesName": "夏日考勤"
			}
		],
		"currentRecordsCount": 2,
		"pageSize": 10,
		"pageNum": 1,
		"navigatePageNums": [
			1
		]
	},
	"success": true
}
```


#### 1.2、  某员工的考勤查询 

| URL    | http://127.0.0.1:8009/attendanceSummary/details |
| ------ | --------------------------------- |
| Method | get                               |

请求的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| employeeMark        | string |  员工唯一标记 |
| month               | string |  格式 yyyy-mm  月份 可以指定查询某年某月 不传默认当前年月 |


返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| normal        |   num |  正常出勤天数 |
| deletion        | num |  缺勤天数 |
| beLate          | num |  迟到天数 |
| leaveEarly      | num |  早退天数 |
| target          | num |  本月预计出勤天数 |
| days            | arr |  本月记录 |
| + time            | string |  记录时间点 |
| + beLate          | num |  迟到  1 正常  2 迟到 |
| + advance          | num |  迟到  1 正常  2 早退 |

```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"normal": 16,
		"deletion": 9,
		"beLate": 0,
		"days": [
			{
				"time": "2023-04-01",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-02",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-03",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-04",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-05",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-06",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-07",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-08",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-09",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-10",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-11",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-12",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-13",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-14",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-15",
				"beLate": 1,
				"advance": 1
			},
			{
				"time": "2023-04-16",
				"beLate": 1,
				"advance": 1
			}
		],
		"leaveEarly": 0,
		"target": 25
	},
	"success": true
}
```



#### 1.3、 考勤记录下载 

| URL    | http://127.0.0.1:8009/attendanceSummary/download |
| ------ | --------------------------------- |
| Method | get                               |

请求的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| deptName        | string |  员工部门名 |
| rulesName        | string |  规则名称 |
| month          | string |  月份 |