# 通行记录 数据概览 -API开发文档

| 版本/状态 | 责任人 | 修改日期   | 版本变更记录 |
| --------- | ------ | ---------- | ------------ |
| v0.0.1    | 温良伟 | 2023.03.24 | 初始化文档   |
| v0.0.2    | 温良伟 | 2023.06.44 | 大屏接口   |

## 接口

### 1、  汉王 门禁设备

#### 1.1、 门禁设备概览 

| URL    | http://127.0.0.1:8009/OverView/guardOverview |
| ------ | --------------------------------- |
| Method | get                               |

 请求参数：

| 参数          | 类型   | 说明     |
| ------------- | ------ | -------- |
| id        | string |  局址id |


返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| All          | number |     全部设备数      |
| NoOnline       | number |     不在线的设备数      |
| Online       | number |     在线设备数      |

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"All": 1,
		"NoOnline": 0,
		"Online": 1
	},
	"success": true
}
 ```
#### 1.2、 今日人流量统计 

| URL    | http://127.0.0.1:8009/OverView/toDayFlow |
| ------ | --------------------------------- |
| Method | get                               |

返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| count          | number |     同行人数      |
| enterCount       | number |     进入的人数      |
| goOutCount       | number |     出去的人数      |
| enterNode       | arr |     进入节点数据      |
| goOutNode       | arr |     出去节点数据      |

```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"enterNode": [
			{
				"x": "01:00",
				"y": 0
			},
			{
				"x": "02:00",
				"y": 0
			},
			{
				"x": "03:00",
				"y": 0
			},
			{
				"x": "04:00",
				"y": 0
			},
			{
				"x": "05:00",
				"y": 0
			},
			{
				"x": "06:00",
				"y": 0
			},
			{
				"x": "07:00",
				"y": 0
			},
			{
				"x": "08:00",
				"y": 0
			},
			{
				"x": "09:00",
				"y": 0
			},
			{
				"x": "10:00",
				"y": 1
			},
			{
				"x": "11:00",
				"y": 0
			},
			{
				"x": "12:00",
				"y": 0
			},
			{
				"x": "13:00",
				"y": 0
			},
			{
				"x": "14:00",
				"y": 0
			},
			{
				"x": "15:00",
				"y": 0
			},
			{
				"x": "16:00",
				"y": 0
			},
			{
				"x": "17:00",
				"y": 0
			},
			{
				"x": "18:00",
				"y": 0
			},
			{
				"x": "19:00",
				"y": 0
			},
			{
				"x": "20:00",
				"y": 0
			},
			{
				"x": "21:00",
				"y": 0
			},
			{
				"x": "22:00",
				"y": 0
			},
			{
				"x": "23:00",
				"y": 0
			},
			{
				"x": "00:00",
				"y": 0
			}
		],
		"count": 1,
		"enterCount": 1,
		"goOutCount": 0,
		"goOutNode": [
			{
				"x": "01:00",
				"y": 0
			},
			{
				"x": "02:00",
				"y": 0
			},
			{
				"x": "03:00",
				"y": 0
			},
			{
				"x": "04:00",
				"y": 0
			},
			{
				"x": "05:00",
				"y": 0
			},
			{
				"x": "06:00",
				"y": 0
			},
			{
				"x": "07:00",
				"y": 0
			},
			{
				"x": "08:00",
				"y": 0
			},
			{
				"x": "09:00",
				"y": 0
			},
			{
				"x": "10:00",
				"y": 0
			},
			{
				"x": "11:00",
				"y": 0
			},
			{
				"x": "12:00",
				"y": 0
			},
			{
				"x": "13:00",
				"y": 0
			},
			{
				"x": "14:00",
				"y": 0
			},
			{
				"x": "15:00",
				"y": 0
			},
			{
				"x": "16:00",
				"y": 0
			},
			{
				"x": "17:00",
				"y": 0
			},
			{
				"x": "18:00",
				"y": 0
			},
			{
				"x": "19:00",
				"y": 0
			},
			{
				"x": "20:00",
				"y": 0
			},
			{
				"x": "21:00",
				"y": 0
			},
			{
				"x": "22:00",
				"y": 0
			},
			{
				"x": "23:00",
				"y": 0
			},
			{
				"x": "00:00",
				"y": 0
			}
		]
	},
	"success": true
}
```

#### 1.3、 累计人流量统计 

| URL    | http://127.0.0.1:8009/OverView/accumulativeFlow |
| ------ | --------------------------------- |
| Method | get                               |



返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| count          | number |     同行人数      |
| enterCount       | number |     进入的人数      |
| goOutCount       | number |     出去的人数      |
| enterNode       | arr |     进入节点数据      |
| goOutNode       | arr |     出去节点数据      |

```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"enterNode": [
			{
				"x": "2023-03-27",
				"y": 1
			},
			{
				"x": "2023-03-26",
				"y": 0
			},
			{
				"x": "2023-03-25",
				"y": 0
			},
			{
				"x": "2023-03-24",
				"y": 0
			},
			{
				"x": "2023-03-23",
				"y": 0
			},
			{
				"x": "2023-03-22",
				"y": 0
			},
			{
				"x": "2023-03-21",
				"y": 0
			}
		],
		"count": 1,
		"enterCount": 1,
		"goOutCount": 0,
		"goOutNode": [
			{
				"x": "2023-03-27",
				"y": 0
			},
			{
				"x": "2023-03-26",
				"y": 0
			},
			{
				"x": "2023-03-25",
				"y": 0
			},
			{
				"x": "2023-03-24",
				"y": 0
			},
			{
				"x": "2023-03-23",
				"y": 0
			},
			{
				"x": "2023-03-22",
				"y": 0
			},
			{
				"x": "2023-03-21",
				"y": 0
			}
		]
	},
	"success": true
}
```

#### 1.4、 今日人数概览

| URL    | http://127.0.0.1:8009/OverView/numberOfPeople |
| ------ | --------------------------------- |
| Method | get                               |

请求参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| id          | String | 局址id      |

返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| datetime          | date |     当前记录时间:      |
| visitorCount          | num |     访客预约总人数      |
| temporaryVisitorCount          | num |     临时访客人数      |
| residentVisitorCount          | num |     常驻三方人数      |
| residentVisitorCount          | num |     内部员工人数      |
| discernCount          | num |     今日识别人数      |

```json5
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"datetime": "2023-03-27 10:40:29",
		"visitorCount": 16,
		"temporaryVisitorCount": 4,
		"residentVisitorCount": 5,
		"interiorCount": 5,
		"discernCount": 1
	},
	"success": true
}
```


### 1、  园区通行大屏接口

#### 1.1、 在园人数 

| URL    | http://127.0.0.1:8009/screen/parkArea |
| ------ | --------------------------------- |
| Method | get                               |

 请求参数：

| 参数           | 类型     | 说明     |
| ------------- | ------  | -------- |
| days          | num     | 当前 - 过去的 天数  |


返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| staff          | number |     员工数      |
| visitor        | number |     访客数      |
| proprietor     | number |     业主数      |
| In            | arr |   入场      |
| + x           | data |    yyyy-mm-dd HH 时间轴      |
| + y           | num |   数量      |
| Out           | arr |   出场  |
| + x           | data |    yyyy-mm-dd HH 时间轴      |
| + y           | num |   数量      |
 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"In": [
			{
				"x": "2023-06-14",
				"y": 0
			},
			{
				"x": "2023-06-13",
				"y": 6
			},
			{
				"x": "2023-06-12",
				"y": 0
			},
			{
				"x": "2023-06-11",
				"y": 0
			}
		],
		"Out": [
			{
				"x": "2023-06-14",
				"y": 0
			},
			{
				"x": "2023-06-13",
				"y": 0
			},
			{
				"x": "2023-06-12",
				"y": 0
			},
			{
				"x": "2023-06-11",
				"y": 0
			}
		],
		"staff": 2,
		"visitor": 1,
		"proprietor": 3
	},
	"success": true
}
 ```

#### 1.2、 今日访客 

| URL    | http://127.0.0.1:8009/screen/todayVisitor |
| ------ | --------------------------------- |
| Method | get                               |

 请求参数：

| 参数           | 类型     | 说明     |
| ------------- | ------  | -------- |
| day           | date     | 今日时间 yyyy-mm-dd  |


返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| visitor          | number |   访客姓名 手机号      |
| person           | number |   接访人姓名 手机号  |
| recordTime       | number |   入场开门时间      |

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": [
		{
			"visitor": "qq 18230201158",
			"person": "wlw 15664791649",
			"recordTime": "2023-06-13 16:59:16"
		}
	],
	"success": true
}
 ```

#### 1.3、 当日人员通行变化量

| URL    | http://127.0.0.1:8009/screen/todayChange |
| ------ | --------------------------------- |
| Method | get                               |

 请求参数：

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           | date     | 时间 yyyy-mm-dd  |
| id            | num      | 区域id           |


返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| In            | arr |   入场      |
| + x           | data |    yyyy-mm-dd HH 时间轴      |
| + y           | num |   数量      |
| Out           | arr |   出场  |
| + x           | data |    yyyy-mm-dd HH 时间轴      |
| + y           | num |   数量      |

 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"In": [
			{
				"x": "2023-06-15 14",
				"y": 0
			},
			{
				"x": "2023-06-15 13",
				"y": 0
			},
			{
				"x": "2023-06-15 12",
				"y": 0
			},
			{
				"x": "2023-06-15 11",
				"y": 1
			}
		],
		"Out": [
			{
				"x": "2023-06-15 14",
				"y": 0
			},
			{
				"x": "2023-06-15 13",
				"y": 0
			},
			{
				"x": "2023-06-15 12",
				"y": 0
			},
			{
				"x": "2023-06-15 11",
				"y": 0
			}
		]
	},
	"success": true
}
 ```
#### 1.4、 当日人员出入详情

| URL    | http://127.0.0.1:8009/screen/inOutDetails |
| ------ | --------------------------------- |
| Method | get                               |

 请求参数：

| 参数           | 类型      | 说明            |
| ------------- | -------  | --------------- |
| day           | date     | 时间 yyyy-mm-dd  |
| pageNum       | num      | 当前页码           |
| pageSize      | num      | 页容量           |


返回的参数： 

| 展示参数             | 类型 | 说明         |
| -------------------- | ---- | ------------ |
| recordsCount            | num |   总记录数      |
| pages            | num |   总记页数      |
| records            | arr |   数据列表      |
| currentRecordsCount    | num |   当前记录数      |
| navigatePageNums    | arr |   页码      |


 ```json 
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"recordsCount": 6,
		"pages": 2,
		"records": [
			{
				"index": 1,
				"carNumber": "",
				"type": "昌平中心东区-104楼",
				"time": "2023-06-13 16:59:17",
				"parkName": "入园"
			},
			{
				"index": 2,
				"carNumber": "qq",
				"type": "昌平中心东区-104楼",
				"time": "2023-06-13 16:59:16",
				"parkName": "入园"
			},
			{
				"index": 3,
				"carNumber": "wlw",
				"type": "昌平中心东区-104楼",
				"time": "2023-06-13 16:23:57",
				"parkName": "入园"
			},
			{
				"index": 4,
				"carNumber": "",
				"type": "昌平中心东区-101楼",
				"time": "2023-06-13 16:23:56",
				"parkName": "入园"
			},
			{
				"index": 5,
				"carNumber": "",
				"type": "昌平中心东区-101楼",
				"time": "2023-06-13 16:23:52",
				"parkName": "入园"
			}
		],
		"currentRecordsCount": 5,
		"pageSize": 5,
		"pageNum": 1,
		"navigatePageNums": [
			1,
			2
		]
	},
	"success": true
}
 ```
 #### 1.4、 今日访客滞留图表
 
 | URL    | http://127.0.0.1:8009/screen/retention |
 | ------ | --------------------------------- |
 | Method | get                               |
 
  请求参数：
 
 | 参数           | 类型      | 说明            |
 | ------------- | -------  | --------------- |
 | day           | date     | 时间 yyyy-mm-dd  |
 
 
 返回的参数： 
 
 | 展示参数             | 类型 | 说明         |
 | -------------------- | ---- | ------------ |
 | count                | num |    滞留访客总人数      |
 | duration             | num |    滞留访客平均时长 H单位      |
 | data                 | arr |    滞留访客      |


 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"count": 1,
		"duration": 47,
		"data": [
			{
				"visitor": "qq 18230201158",
				"person": "wlw 15664791649",
				"recordTime": "2023-06-13 16:59:16"
			}
		]
	},
	"success": true
}
 ```


 #### 1.5、 设备概览 
 
 | URL    | bmpvoucher/door/overview |
 | ------ | --------------------------------- |
 | Method | get                               |
 
  请求参数：
 
 | 参数           | 类型      | 说明            |
 | ------------- | -------  | --------------- |
 | id           | num     | 局址id  |
 
 
 返回的参数： 
 
 | 展示参数             | 类型 | 说明         |
 | -------------------- | ---- | ------------ |
 | all                   | num |    全部设备      |
 | onLine                   | num |    在线设备      |
 | noOnLine                   | num |    离线设备      |


 ```json
{
	"code": 0,
	"message": "请求成功！",
	"exception": null,
	"data": {
		"all": 3,
		"onLine": 3,
		"noOnLine": 0
	},
	"success": true
}
 ```

